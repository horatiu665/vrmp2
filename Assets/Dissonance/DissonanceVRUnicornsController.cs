using Dissonance.Integrations.UNet_LLAPI;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore.VRNetwork;
using VRCore.VRNetwork.Client;
using Random = UnityEngine.Random;

public class DissonanceVRUnicornsController : MonoBehaviour
{
    public bool server = false;
    UNetCommsNetwork dis;

    private void OnEnable()
    {
        dis = GetComponent<UNetCommsNetwork>();
    }

    IEnumerator Start()
    {
        bool connected = false;
        while (!connected)
        {
            yield return new WaitForSeconds(0.5f);

            if (server)
            {
                // dedicated cause the server is not also a client for us
                dis.InitializeAsServer();
                yield return 0;
                if (dis.ServerEnabled)
                {
                    connected = true;
                }
            }
            else
            {
                dis.InitializeAsClient(ClientNetSender.instance.serverIp);
                yield return 0;
                if (dis.ClientEnabled)
                {
                    connected = true;
                }
            }

        }
        yield break;
    }

}