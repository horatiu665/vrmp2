﻿using UnityEngine;

namespace Dissonance.Integrations.UNet_LLAPI.Demo
{
    [RequireComponent(typeof(DissonanceComms))]
    [RequireComponent(typeof(UNetCommsNetwork))]
    public class LlapiManager : MonoBehaviour
    {
        private UNetCommsNetwork _net;
        private DissonanceComms _comms;
        private string _serverIp = "127.0.0.1";

        void Start ()
        {
            _comms = GetComponent<DissonanceComms>();
            _net = GetComponent<UNetCommsNetwork>();
        }

        void OnGUI()
        {
            using (new GUILayout.AreaScope(new Rect(20, 20, 200, 200)))
            {
                if (_net.ServerEnabled && !_net.ClientEnabled)
                {
                    GUILayout.Label("Running Dedicated Server");

                    if (GUILayout.Button("Disconnect"))
                        _net.StopServer();
                }
                else if (_net.ServerEnabled)
                {
                    GUILayout.Label("Running server");

                    if (GUILayout.Button("Disconnect"))
                    {
                        _net.StopServer();
                        _net.StopClient();
                    }
                }
                else if (_net.ClientEnabled)
                {
                    GUILayout.Label("Connected to server at " + _net.ServerAddress);

                    if (GUILayout.Button("Disconnect"))
                        _net.StopClient();
                }

                if (!_net.ServerEnabled && !_net.ClientEnabled)
                {
                    if (GUILayout.Button("Create Server"))
                    {
                        _net.InitializeAsServer();
                    }

                    if (GUILayout.Button("Create Dedicated Server"))
                    {
                        _net.InitializeAsDedicatedServer();
                    }

                    GUILayout.Space(20);

                    _serverIp = GUILayout.TextField(_serverIp);
                    if (GUILayout.Button("Connect to Server"))
                    {
                        _net.InitializeAsClient(_serverIp);
                    }
                }
            }
        
            using (new GUILayout.AreaScope(new Rect(20, 140, 600, 500)))
            {
                foreach (var player in _comms.Players)
                    GUILayout.Label(string.Format("{0} - {1}", player.Name, player.IsSpeaking ? "(speaking)" : string.Empty));
            }
        }
    }
}
