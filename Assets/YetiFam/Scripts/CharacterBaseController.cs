﻿using Apex.WorldGeometry;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;


/// <summary>
/// Handles basic functionality, that can be used as API by AI as well as player controls as well as story moments and anything else.
/// </summary>
public class CharacterBaseController : MonoBehaviour
{
    private IkAnim _ikAnim;
    public IkAnim ikAnim
    {
        get
        {
            if (_ikAnim == null)
            {
                _ikAnim = GetComponent<IkAnim>();
            }
            return _ikAnim;
        }
    }

    [SerializeField]
    private Animator _anim;
    public Animator anim
    {
        get
        {
            if (_anim == null)
            {
                _anim = GetComponent<Animator>();
            }
            return _anim;
        }
    }

    private Rigidbody _rb;
    public Rigidbody rb
    {
        get
        {
            if (_rb == null)
            {
                _rb = GetComponent<Rigidbody>();
            }
            return _rb;
        }
    }

    [Header("Gravity stuff")]
    #region Gravity stuff variables
    [SerializeField]
    private bool _useFakeGravity;

    [SerializeField]
    private bool _grounded;

    [SerializeField]
    private float _groundedSphereCastRadius = 0f;

    private Vector3 _fallingVelocity = Vector3.zero;

    [SerializeField]
    private float _gravityMultiplier = 1f;

    internal void SetMoveTarget(object p)
    {
        throw new NotImplementedException();
    }

    public bool grounded
    {
        get { return _grounded; }
    }

    private Vector3 gravity
    {
        get { return Physics.gravity; }
    }

    #endregion

    [Header("Movement stuff low-level")]

    [SerializeField]
    private bool _useRootMotion = true;

    private Vector3 movementDir
    {
        get
        {
            if (_hasMoveTarget)
            {
                return _moveTargetWorldPos - transform.position;
            }
            return Vector3.zero;
        }
    }

    public AnimationCurve distToTargetToSpeed = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    public float accelSmoothness = 0.2f;
    float curSpeed = 0f;

    public Vector2 animSpeedRange01 = new Vector2(0, 20f);
    public Vector2 animSpeedRange12 = new Vector2(70f, 135f);

    [Header("Movement stuff high-level")]

    /// <summary>
    /// Body turning smoothness override for target movement
    /// </summary>
    [Tooltip("Body turning smoothness override for target movement")]
    [SerializeField]
    private AnimationCurve _distToTargetToBodyTurningSmoothness = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0.5f, 0, 0), new Keyframe(1, 0.1f, 0, 0) } };

    [SerializeField]
    private float _bodyTurningSmoothnessDefault = 0.1f;

    [SerializeField]
    private float _moveTargetMinDistance = 0.1f;

    public float slowdownDistance = 3f;

    private bool _hasMoveTarget = false;
    private Vector3 _moveTargetWorldPos;

    // calculated and cached when target is on
    float _targetDistance;

    [Header("Getting hit stuff")]

    [SerializeField]
    private float _health = 1;

    [SerializeField]
    private float _minHitVelocityForAnimation = 1f;

    [SerializeField]
    private Vector2 _hitVelocityAnimationRange = new Vector2(1, 5f);

    private RaycastHit _groundedHitInfo;

    public Vector3 position
    {
        get
        {
            return transform.position;
        }
    }

    [Header("Look at stuff")]
    public Transform lookAtTarget;
    public Vector3 lookAtTargetOffset;

    public float lookAtWeight = 1f;
    public float lookAtBodyWeight = 0.5f;
    float curLookWeight = 0f;

    public float lookSmoothness = 0.1f;

    public int debugFrames = 0;

    private Vector3 globalInitPos;

    /// <summary>
    /// Params: hit size, localHitDirection. Happens on collision.
    /// </summary>
    public static event Action<CharacterBaseController, float, Vector3> OnHitHappened;

    private void Awake()
    {
    }

    private void OnAnimatorIK(int layer)
    {
        if (lookAtTarget != null)
        {
            if (anim != null)
            {
                curLookWeight = Mathf.Lerp(curLookWeight, 1f, lookSmoothness);
                anim.SetLookAtPosition(lookAtTarget.position + lookAtTargetOffset);
                anim.SetLookAtWeight(lookAtWeight * curLookWeight, lookAtBodyWeight);
            }
            else
            {
                curLookWeight = Mathf.Lerp(curLookWeight, 0f, lookSmoothness);
                anim.SetLookAtWeight(lookAtWeight * curLookWeight, lookAtBodyWeight);
            }
        }
    }

    private void Update()
    {
    }

    private void FixedUpdate()
    {
        HandleGravity();
        HandleRotation();
        HandleLowLevelMove();
    }

    private void OnCollisionEnter(Collision c)
    {
        var rel = c.relativeVelocity.magnitude;
        if (rel > _minHitVelocityForAnimation)
        {
            var hitSize = Mathf.Clamp01(Mathf.InverseLerp(_hitVelocityAnimationRange.x, _hitVelocityAnimationRange.y, rel));
            // direction towards collider. contacts[0].normal could also work, if the physics system wasn't shit
            var hitDirection = c.collider.transform.position - transform.position;
            hitDirection.y = 0;
            var localHitDirection = transform.InverseTransformDirection(hitDirection);

            if (OnHitHappened != null)
            {
                OnHitHappened(this, hitSize, localHitDirection);
            }
        }
    }

    private bool Raycast(Ray ray, float maxDistance)
    {
        if (_groundedSphereCastRadius == 0)
        {
            return (Physics.Raycast(ray, maxDistance, Layers.terrain));
        }
        else
        {
            ray.origin += Vector3.up * _groundedSphereCastRadius;
            return Physics.SphereCast(ray, _groundedSphereCastRadius, maxDistance, Layers.terrain);
        }
    }

    private bool Raycast(Ray ray, float maxDistance, out RaycastHit hitInfo)
    {
        if (_groundedSphereCastRadius == 0)
        {
            return (Physics.Raycast(ray, out hitInfo, maxDistance, Layers.terrain));
        }
        else
        {
            return Physics.SphereCast(ray, _groundedSphereCastRadius, out hitInfo, maxDistance, Layers.terrain);
        }
    }

    private void HandleGravity()
    {
        if (_useFakeGravity)
        {
            // grounded, gravity stuff
            var ray = new Ray(transform.position + Vector3.up, Vector3.down);
            _grounded = Raycast(ray, 1f, out _groundedHitInfo);

            if (!_grounded)
            {
                RaycastHit hitInfo;
                // check if we can fall downwards with fallingVelocity
                if (!Raycast(ray, 1f + _fallingVelocity.magnitude * Time.fixedDeltaTime, out hitInfo))
                {
                    // apply velocity to position
                    transform.position += _fallingVelocity * Time.fixedDeltaTime;
                    // apply acceleration to velocity
                    _fallingVelocity += Physics.gravity * _gravityMultiplier * Time.fixedDeltaTime;
                }
                else
                {
                    // cannot fall because something in the way, just place us on the surface of the hit surface
                    transform.position = hitInfo.point;
                    _fallingVelocity = Vector3.zero;
                    // set grounding to true? maybe not because it's set before the  if(!_grounded)  above.
                }
            }
            else
            {
                _fallingVelocity = Vector3.zero;
            }
        }
        else
        {
            // just set info about groundedness
            var ray = new Ray(transform.position + Vector3.up, Vector3.down);
            _grounded = Raycast(ray, 1f, out _groundedHitInfo);

        }
        if (this.debugFrames > 0)
        {
            Debug.DrawLine(transform.position, _groundedHitInfo.point, Color.red, Time.fixedDeltaTime * debugFrames);
            Debug.DrawRay(_groundedHitInfo.point, _groundedHitInfo.normal * 100, Color.cyan, Time.fixedDeltaTime * debugFrames);
        }
    }

    private void HandleRotation()
    {
        // set rotation to where we are moving
        var e = movementDir;
        e = Vector3.ProjectOnPlane(e, Vector3.up);
        var bodyTurningSmoothness = _hasMoveTarget ? _distToTargetToBodyTurningSmoothness.Evaluate(_targetDistance) : _bodyTurningSmoothnessDefault;
        transform.forward = Vector3.Lerp(transform.forward, e, bodyTurningSmoothness);
    }

    private void HandleLowLevelMove()
    {
        var dirY0 = this.movementDir;
        dirY0.y = 0;
        var distToTarget = dirY0.magnitude;

        var dirNormalized = dirY0 / distToTarget;

        float curSpeedTarget = 1f;
        // we know how far the target is, so we can decide speed based on that
        curSpeedTarget *= distToTargetToSpeed.Evaluate(distToTarget);

        curSpeed = Mathf.Lerp(curSpeed, curSpeedTarget, accelSmoothness);

        // speed for animation
        // graph looks like this:
        //                 /
        //     ___________/
        //    /
        //   /
        var animSpeed = 0f;
        // less than first range max
        if (curSpeed < animSpeedRange01.y)
        {
            animSpeed = Mathf.InverseLerp(animSpeedRange01.x, animSpeedRange01.y, curSpeed);
        }
        // more than second range min
        else if (curSpeed > animSpeedRange12.x)
        {
            animSpeed = 1f + Mathf.InverseLerp(animSpeedRange12.x, animSpeedRange12.y, curSpeed);
        }
        // in between (1).
        else
        {
            animSpeed = 1f;
        }

        // if move dir more than move target min
        if (distToTarget > _moveTargetMinDistance)
        {
            if (!_useRootMotion)
            {
                if (!_grounded || _groundedHitInfo.normal == Vector3.up)
                {
                    transform.position += dirNormalized * curSpeed * Time.fixedDeltaTime;
                }
                else // if grounded
                {
                    // move parallel to ground heh
                    float downAmount = 1f - Vector3.Dot(_groundedHitInfo.normal, Vector3.up);
                    transform.position += dirNormalized * curSpeed  * Time.fixedDeltaTime;
                }
            }

            ikAnim.Anim_MoveForward(animSpeed);
            Anim_MoveForward(animSpeed);

        }
        else
        {
            StopMoving();
        }

    }

    private void Anim_MoveForward(float movementSpeedParam)
    {
        this.anim.SetFloat("Speed", movementSpeedParam);
    }
    
    public void SetMoveTarget(Vector3 worldPos)
    {
        _hasMoveTarget = true;
        _moveTargetWorldPos = worldPos;
    }

    public void StopMoving()
    {
        ikAnim.Anim_MoveForward(0f);
        Anim_MoveForward(0);
        _hasMoveTarget = false;
    }

    public void SetLookTarget(Transform target)
    {
        SetLookTarget(target, Vector3.zero);
    }

    public void SetLookTarget(Transform target, Vector3 offset)
    {
        this.lookAtTarget = target;
        this.lookAtTargetOffset = offset;
    }

    public void Attack()
    {
        if (anim != null)
        {
            anim.SetTrigger("Attack");
        }

        var pb = PlayerManager.GetLocalPlayer<Core.PlayerBase>();
        ToiletTeleporterManager.instance.TeleportPlayerToToilet(pb, 1);

        this.transform.position = OriginShiftManager.GlobalToLocalPos(globalInitPos);
    }
}