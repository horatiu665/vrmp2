﻿using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;

/// <summary>
/// Provides Animator-like and ragdoll functionality, for characters with an IK setup. 
///     Transfers the MirrorGuyData animations with a humanoid algorithm, using shoulder offset 
///     to rescale hands, hips offset to account for short characters/stumpy legs, and other such nonsense
/// Future task: Optimize human-animation to this ik animation
///     ...by applying the transformation to all animations used by the character, and saving as a new local-space animation that does not require so many calcs
/// 
/// TODO:
///     separate SetVectors() into new extendable class, 
///     so the IkAnim functionality has the generic functionality of setting animations and playback and shit,
///     but the SetVectors has the indiv. settings and interpretation params (opt. spider IK, etc)
/// </summary>
public class IkAnim : MonoBehaviour
{
    [Header("Animation settings")]
    public MirrorGuyAnimationShare playAnimation;

    public enum PlayState
    {
        None = 0,
        Play = 1,
        Anim = 2,
    }

    [EnumButtons]
    public PlayState playState = PlayState.Anim;

    float curPlaybackFrame = 0;
    float lastFrameAnimTime = 0f;

    public int playbackFrame
    {
        get
        {
            return (((int)curPlaybackFrame + playAnimation.data.Count) % playAnimation.data.Count);
        }
        set
        {
            curPlaybackFrame = value;
        }
    }

    public float playbackFPS = 90f;

    [SerializeField]
    private AnimationInterpreterBase _animationInterpreter;
    public AnimationInterpreterBase animationInterpreter
    {
        get
        {
            if (_animationInterpreter == null)
            {
                _animationInterpreter = GetComponent<AnimationInterpreterBase>();
            }
            return _animationInterpreter;
        }
    }

    // maybe we can delete this soon.
    private FullBodyBipedIK _ik;
    private FullBodyBipedIK ik
    {
        get
        {
            if (_ik == null)
            {
                _ik = GetComponent<FullBodyBipedIK>();
            }
            return _ik;
        }
    }

    private RagdollUtility _ragdollUtility;
    public RagdollUtility ragdollUtility
    {
        get
        {
            if (_ragdollUtility == null)
            {
                _ragdollUtility = GetComponent<RagdollUtility>();
            }
            return _ragdollUtility;
        }
    }

    private MirrorGuyRagdoll _mirrorGuyRagdoll;
    public MirrorGuyRagdoll mirrorGuyRagdoll
    {
        get
        {
            if (_mirrorGuyRagdoll == null)
            {
                _mirrorGuyRagdoll = GetComponent<MirrorGuyRagdoll>();
            }
            return _mirrorGuyRagdoll;
        }
    }

    [SerializeField]
    private Animator _ikAnimator;
    public Animator ikAnimator
    {
        get
        {
            if (_ikAnimator == null)
            {
                _ikAnimator = GetComponentsInChildren<Animator>(true).First(a => a.transform != transform);
            }
            return _ikAnimator;
        }
    }

    /// <summary>
    /// Sets this transform's position to the ikAnimator position, and the ikAnimator localPos to zero. This way the ikAnimator "applies root motion" to this object not just itself
    /// </summary>
    [Tooltip("Sets this transform's position to the ikAnimator position, and the ikAnimator localPos to zero. This way the ikAnimator applies root motion to this object not just itself")]
    public bool deferredRootMotion = true;

    [Header("Refs for IkAnimator")]
    [SerializeField]
    private HumanIkRefs humanIkRefs;

    private void OnValidate()
    {
        if (ikAnimator != null)
        {
        }
        if (animationInterpreter != null)
        {
        }
        if (ikAnimator != null)
        {
            humanIkRefs.Validate(ikAnimator.transform);
        }
        else
        {
            Debug.Log("[IkAnim] No IK Animator detected", gameObject);
        }
    }

    private void Reset()
    {
        if (ikAnimator == null)
        {
            Debug.Log("[IkAnim] No IK Animator detected");
        }
        if (ik == null)
        {
            _ik = gameObject.AddComponent<FullBodyBipedIK>();
        }
        if (ragdollUtility == null)
        {
            _ragdollUtility = gameObject.AddComponent<RagdollUtility>();
        }
        ragdollUtility.ik = ik;

        if (mirrorGuyRagdoll == null)
        {
            _mirrorGuyRagdoll = gameObject.AddComponent<MirrorGuyRagdoll>();
        }

        RagdollSetupBlind();

    }

    #region Ragdoll Setup
    private void RagdollSetupBlind()
    {

        // ragdoll setup
        var hips = ik.references.pelvis;
        CreateCapsule(ik.references.pelvis);
        CreateRb(ik.references.pelvis);

        CreateCapsule(ik.references.head);
        CreateRb(ik.references.head);
        CreateJoint(ik.references.head, hips);

        CreateCapsule(ik.references.leftUpperArm);
        CreateCapsule(ik.references.leftForearm);
        CreateRb(ik.references.leftUpperArm);
        CreateJoint(ik.references.leftUpperArm, hips);

        CreateCapsule(ik.references.leftThigh);
        CreateCapsule(ik.references.leftCalf);
        CreateRb(ik.references.leftThigh);
        CreateJoint(ik.references.leftThigh, hips);

        CreateCapsule(ik.references.rightUpperArm);
        CreateCapsule(ik.references.rightForearm);
        CreateRb(ik.references.rightUpperArm);
        CreateJoint(ik.references.rightUpperArm, hips);

        CreateCapsule(ik.references.rightThigh);
        CreateCapsule(ik.references.rightCalf);
        CreateRb(ik.references.rightThigh);
        CreateJoint(ik.references.rightThigh, hips);
    }

    private T GetOrAddComponent<T>(GameObject go) where T : UnityEngine.Component
    {
        var co = go.GetComponent<T>();
        if (co != null)
        {
            return co;
        }
        return go.AddComponent<T>();
    }

    private void CreateJoint(Transform t, Transform target)
    {
        var cj = GetOrAddComponent<ConfigurableJoint>(t.gameObject);
        cj.connectedBody = target.GetComponent<Rigidbody>();
        cj.anchor = Vector3.zero;
        cj.autoConfigureConnectedAnchor = true;

        cj.xMotion = ConfigurableJointMotion.Locked;
        cj.yMotion = ConfigurableJointMotion.Locked;
        cj.zMotion = ConfigurableJointMotion.Locked;
        cj.angularXMotion = ConfigurableJointMotion.Free;
        cj.angularYMotion = ConfigurableJointMotion.Free;
        cj.angularZMotion = ConfigurableJointMotion.Free;

        cj.lowAngularXLimit = new SoftJointLimit() { limit = -45 };
        cj.highAngularXLimit = new SoftJointLimit() { limit = 45 };
        cj.angularYLimit = new SoftJointLimit() { limit = 45 };
        cj.angularZLimit = new SoftJointLimit() { limit = 45 };

        cj.rotationDriveMode = RotationDriveMode.Slerp;
        cj.slerpDrive = new JointDrive() { positionSpring = 1, positionDamper = 1, maximumForce = 10 };
        cj.projectionMode = JointProjectionMode.PositionAndRotation;

    }

    private void CreateRb(Transform t)
    {
        var r = GetOrAddComponent<Rigidbody>(t.gameObject);
    }

    private void CreateCapsule(Transform t)
    {
        var c = GetOrAddComponent<CapsuleCollider>(t.gameObject);
        c.direction = 0;

        // init values? make better inits. use distance to next bone... or SOMETHING!
        if (c.height == 1f && c.radius == 0.5f)
        {
            c.radius = 0.05f;
            c.height = 0.36f;
        }

    }
    #endregion

    private void Update()
    {
        HandleAnimPlayback();
    }

    private void HandleAnimPlayback()
    {
        if (playState == PlayState.Play)
        {
            if (playAnimation != null)
            {
                ikAnimator.enabled = false;
                MirrorGuyData data;
                curPlaybackFrame += (Time.time - lastFrameAnimTime) / (1f / playbackFPS);
                lastFrameAnimTime = Time.time;
                data = playAnimation.data[playbackFrame];
                SetVectors(data);
            }
        }
        else if (playState == PlayState.Anim)
        {
            ikAnimator.enabled = true;
            // get ik state from the ik object animated independenlty by the ikAnimator
            MirrorGuyData data = humanIkRefs.GetDataFromAnimator(ikAnimator);
            SetVectors(data);
            if (deferredRootMotion && ikAnimator.applyRootMotion)
            {
                transform.position = ikAnimator.transform.position;
                transform.rotation = ikAnimator.transform.rotation;
                ikAnimator.transform.localPosition = Vector3.zero;
                ikAnimator.transform.localRotation = Quaternion.identity;
            }
        }
    }

    public void UpdatePlaybackFrame()
    {
        SetVectors(playAnimation.data[playbackFrame]);
    }

    internal void Anim_MoveForward(float movementSpeedParam)
    {
        if (enabled)
        {
            // play move fwd animation
            this.ikAnimator.SetFloat("Speed", movementSpeedParam);
        }
    }

    /// <summary>
    /// The opposite of <see cref="MirrorGuyData.GetFromPlayer()"/>
    /// sets the saved data to the IKs on the character.
    /// </summary>
    /// <example>
    /// Can set the data with various modifications, based on the actual character.
    ///     for example: shoulder/hand scaling for long hands, 
    ///     hipsOffset can be different for each leg when the legs are
    ///     far from each other like the stumpy characters, etc)
    /// We can even get leg data, duplicate/mirror/transform it 
    ///     and apply to 8 SPIDER legs instead of humanoid legs
    ///     in another version of this script, or when we decide to extend it ;)
    /// </example>
    /// <param name="data">Raw (saved) animation data from the player</param>
    public void SetVectors(MirrorGuyData data)
    {
        animationInterpreter.SetVectors(data);
    }

    /// <summary>
    /// Provides references to humanIK body parts, so the ikAnimator can animate those, while the CharacterIK interprets them for this individual character.
    /// </summary>
    [Serializable]
    public struct HumanIkRefs
    {
        public Transform head, handLeft, handRight, legLeft, legRight, hips;

        public bool AnyNull()
        {
            return head == null || handLeft == null || handRight == null || legLeft == null || legRight == null || hips == null;
        }

        public void AutoRef(Transform ikRoot)
        {
            head = ikRoot.Find("head");
            handLeft = ikRoot.Find("hand left");
            handRight = ikRoot.Find("hand right");
            hips = ikRoot.Find("hips");
            legLeft = ikRoot.Find("leg left");
            legRight = ikRoot.Find("leg right");
        }

        public void Validate(Transform ikRoot)
        {

            if (AnyNull())
            {
                AutoRef(ikRoot);
                if (AnyNull())
                {
                    // generate objs
                    GenerateRefs(ikRoot);
                }
            }
        }

        private void GenerateRefs(Transform ikRoot)
        {
            head = new GameObject("head").transform;
            head.SetParent(ikRoot);
            handLeft = new GameObject("hand left").transform;
            handLeft.SetParent(ikRoot);
            handRight = new GameObject("hand right").transform;
            handRight.SetParent(ikRoot);
            legLeft = new GameObject("leg left").transform;
            legLeft.SetParent(ikRoot);
            legRight = new GameObject("leg right").transform;
            legRight.SetParent(ikRoot);
            hips = new GameObject("hips").transform;
            hips.SetParent(ikRoot);
        }

        public MirrorGuyData GetDataFromAnimator(Animator ikAnimator)
        {
            return new MirrorGuyData()
            {
                headPos = head.localPosition,
                headRot = head.localRotation,
                handLeftPos = handLeft.localPosition,
                handLeftRot = handLeft.localRotation,
                handRightPos = handRight.localPosition,
                handRightRot = handRight.localRotation,
                legLeftPos = legLeft.localPosition,
                legLeftRot = legLeft.localRotation,
                legRightPos = legRight.localPosition,
                legRightRot = legRight.localRotation,
                hipsPos = hips.localPosition,
                hipsRot = hips.localRotation,
            };
        }
    }

}