﻿using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class AnimationInterpreterHumanoid : AnimationInterpreterBase
{
    private FullBodyBipedIK _ik;
    public FullBodyBipedIK ik
    {
        get
        {
            if (_ik == null)
            {
                _ik = GetComponent<FullBodyBipedIK>();
            }
            return _ik;
        }
    }

    [Header("IK Ref Body parts", order = 10)]
    public Transform ikLeft;
    public Transform ikRight, ikHead, ikLeftLeg, ikRightLeg, ikHips;

    [Header("Hand rotation offset 4 bad rig")]
    public Transform leftHandRotationOffset;
    public Transform rightHandRotationOffset;

    // can be set to two different objects, parented under ikHips, that are offset from ikHips to the left and right... if char is stumpy.
    // set to the same object or even the ikHips themselves if you don't want that.
    [Header("IK Ref hips offset for legs, set to hips when lazy")]
    public Transform hipsOffsetLeft;
    public Transform hipsOffsetRight;

    /// Should be set up just like the shoulder offset on the player upon recording. Both for recording and playback.
    [Header("IK Ref shoulder offset for hands Only for hands. Set to head if lazy, or actual left/right shoulders of rig when adventurous.")]
    public Transform shoulderOffsetLeft;
    public Transform shoulderOffsetRight;

    [Header("Offsets hips and head. Mostly for height/scale adjustments, legs and hips/head.")]
    public Transform ikBodyOffset;

    [Space(order = -1)]
    [Header("IK transfer parameters")]
    public bool useHipsOffset = false;
    public float heightAdjustHips = 0f;
    public float heightAdjustHead = 0f;

    public bool useShoulderOffset = true;
    /// <summary>
    /// applied to the raw animation data, before transfer to this character. scaling is done on the result of this operation
    /// </summary>
    public Vector3 playerShoulderPosOffset = new Vector3(0, -0.2f, 0f);
    public bool setShoulderUniformScale = true;
    public float shoulderUniformScale = shoulderUniScaleInvalidNumber;
    // just used for OnValidate()... -1 was not a good invalid number
    private const float shoulderUniScaleInvalidNumber = -1234567;

    private void OnValidate()
    {
        // set up references to ik transforms
        if (ikBodyOffset == null || ikHead == null || ikLeft == null || ikRight == null || ikLeftLeg == null || ikHips == null || ikRightLeg == null || shoulderOffsetLeft == null || shoulderOffsetRight == null || hipsOffsetLeft == null || hipsOffsetRight == null || leftHandRotationOffset == null || rightHandRotationOffset == null)
        {
            var ikParent = transform.GetChildren().First(t => t.name.Contains("CharacterIK"));
            if (ikBodyOffset == null)
            {
                ikBodyOffset = ikParent;
            }
            if (ikHips == null)
                ikHips = ikParent.GetChildren().First(t => t.name.Contains("body") || t.name.Contains("hips"));
            if (ikHead == null)
                ikHead = ikParent.GetChildren().First(t => t.name.Contains("head"));
            if (ikLeftLeg == null)
                ikLeftLeg = ikParent.GetChildren().First(t => t.name.Contains("left") && t.name.Contains("leg"));
            if (ikRightLeg == null)
                ikRightLeg = ikParent.GetChildren().First(t => t.name.Contains("right") && t.name.Contains("leg"));

            var shoulder = ikHead.GetChildren().First(t => t.name.Contains("shoulder"));
            // shoulder is child of head?... well yeah
            if (shoulderOffsetLeft == null)
            {
                shoulderOffsetLeft = ikHead.GetChildren().FirstOrDefault(t => t.name.Contains("left"));
                shoulderOffsetRight = ikHead.GetChildren().FirstOrDefault(t => t.name.Contains("right"));
            }
            if (shoulderOffsetLeft == null)
            {
                shoulderOffsetLeft = shoulderOffsetRight = shoulder;
            }

            // hands should not be child of shoulder, so we can choose to offset position/rotation or both or none...
            var handsParent = ikParent; // shoulder;
            if (ikLeft == null)
            {
                ikLeft = handsParent.GetChildren().FirstOrDefault(t => t.name.Contains("left") && !t.name.Contains("leg"));
                ikRight = handsParent.GetChildren().FirstOrDefault(t => t.name.Contains("right") && !t.name.Contains("leg"));
            }
            if (ikLeft == null)
            {
                handsParent = shoulder;
                ikLeft = handsParent.GetChildren().First(t => t.name.Contains("left") && !t.name.Contains("leg"));
                ikRight = handsParent.GetChildren().First(t => t.name.Contains("right") && !t.name.Contains("leg"));
            }
            if (hipsOffsetLeft == null)
            {
                hipsOffsetLeft = ikHips.GetChildren().FirstOrDefault(t => t.name.Contains("left"));
                hipsOffsetRight = ikHips.GetChildren().FirstOrDefault(t => t.name.Contains("right"));
            }
            if (hipsOffsetLeft == null)
            {
                hipsOffsetLeft = hipsOffsetRight = ikHips;
            }

            leftHandRotationOffset = ikLeft.GetChildren().FirstOrDefault(t => t.name.Contains("rot"));
            rightHandRotationOffset = ikRight.GetChildren().FirstOrDefault(t => t.name.Contains("rot"));
            // leave rotation offset as null, if an offset object is not found (rotation is just not applied this way)
            //if (leftHandRotationOffset == null)
            //{
            //    leftHandRotationOffset = ikLeft;//.GetChildren().FirstOrDefault(t => t.name.Contains("rot"));
            //    rightHandRotationOffset = ikRight;//.GetChildren().FirstOrDefault(t => t.name.Contains("rot"));
            //}
        }

        if (setShoulderUniformScale)
        {
            if (shoulderUniformScale == shoulderUniScaleInvalidNumber)
            {
                shoulderUniformScale = shoulderOffsetLeft.localScale.x;
            }
            else
            {
                shoulderOffsetLeft.localScale = shoulderOffsetRight.localScale = Vector3.one * shoulderUniformScale;
            }
        }

    }

    private void Reset()
    {
        if (ik == null)
        {
            _ik = gameObject.AddComponent<FullBodyBipedIK>();
        }

        OnValidate();

        shoulderUniformScale = shoulderOffsetLeft.localScale.x;

        // set up ik solver effectors and weights
        ik.solver.bodyEffector.target = ikHips;
        ik.solver.bodyEffector.positionWeight = 1f;
        ik.solver.leftHandEffector.target = ikLeft;
        ik.solver.leftHandEffector.positionWeight = 1f;
        ik.solver.rightHandEffector.target = ikRight;
        ik.solver.rightHandEffector.positionWeight = 1f;
        ik.solver.leftFootEffector.target = ikLeftLeg;
        ik.solver.leftFootEffector.positionWeight = 1f;
        ik.solver.rightFootEffector.target = ikRightLeg;
        ik.solver.rightFootEffector.positionWeight = 1f;
        var he = ikHead.GetComponent<FBBIKHeadEffector>();
        he.ik = ik;

    }

    private void Update()
    {
        HandleShoulderScaling();
    }


    private void HandleShoulderScaling()
    {
        if (setShoulderUniformScale)
        {
            shoulderOffsetLeft.localScale = shoulderOffsetRight.localScale = Vector3.one * shoulderUniformScale;
        }
    }


    /// <summary>
    /// Local shoulder data = shoulder child of head, hands are local position comapred to shoulder, shoulder can be scaled for chars with big/smaller hands than a human.
    /// </summary>
    /// <param name="data"></param>
    public override void SetVectors(MirrorGuyData data)
    {
        // head first
        var posHead = ikBodyOffset.TransformPoint(data.headPos);
        posHead.y += heightAdjustHead;
        var rotHead = ikBodyOffset.rotation * data.headRot;
        // order is important because we are setting local positions and shit.
        ikHead.position = posHead;
        ikHead.rotation = rotHead;

        // hands after head (w/ shoulder offset)
        var shoulderOffsetLeft = useShoulderOffset ? this.shoulderOffsetLeft : ikBodyOffset;
        var shoulderOffsetRight = useShoulderOffset ? this.shoulderOffsetRight : ikBodyOffset;
        var lHandPos = data.handLeftPos;
        var rHandPos = data.handRightPos;
        if (useShoulderOffset)
        {
            var shoulderOffsetPosition = ikHead.localPosition + playerShoulderPosOffset;

            lHandPos -= shoulderOffsetPosition;
            rHandPos -= shoulderOffsetPosition;
        }
        var posLeft = shoulderOffsetLeft.TransformPoint(lHandPos);
        var posRight = shoulderOffsetRight.TransformPoint(rHandPos);
        var rotLeft = ikBodyOffset.rotation * data.handLeftRot;
        var rotRight = ikBodyOffset.rotation * data.handRightRot;
        ikLeft.position = posLeft;
        ikLeft.rotation = rotLeft;
        if (leftHandRotationOffset != null)
        {
            ikLeft.rotation *= leftHandRotationOffset.localRotation;
        }
        ikRight.position = posRight;
        ikRight.rotation = rotRight;
        if (rightHandRotationOffset != null)
        {
            ikRight.rotation *= rightHandRotationOffset.localRotation;
        }

        // how to use hips offset depends on the character. can be used for stumpy legs, or just set to transform for no special transformation/scaling
        var hipsOffsetLeft = (useHipsOffset) ? this.hipsOffsetLeft : ikBodyOffset;
        var hipsOffsetRight = (useHipsOffset) ? this.hipsOffsetRight : ikBodyOffset;

        bool hipsValid = data.hipsPos != Vector3.zero;
        // set hips first
        Vector3 hipos = ikBodyOffset.TransformPoint(data.hipsPos);
        Quaternion hirot = ikBodyOffset.rotation * data.hipsRot;
        hipos.y += heightAdjustHips;
        if (hipsValid)
        {
            SetHipsActive(true);
            ikHips.position = hipos;
            ikHips.rotation = hirot;
        }
        else
        {
            SetHipsActive(false);
        }

        // set legs after hips
        bool leftLeg = data.legLeftPos != Vector3.zero;
        if (leftLeg)
        {
            Vector3 leg1 = hipsOffsetLeft.TransformPoint(data.legLeftPos);
            Quaternion reg1 = hipsOffsetLeft.rotation * data.legLeftRot;
            SetLegActive(true, true);
            ikLeftLeg.position = leg1;
            ikLeftLeg.rotation = reg1;
        }
        else
        {
            SetLegActive(true, false);
        }
        bool rightLeg = data.legRightPos != Vector3.zero;
        if (rightLeg)
        {
            Vector3 leg2 = hipsOffsetRight.TransformPoint(data.legRightPos);
            Quaternion reg2 = hipsOffsetRight.rotation * data.legRightRot;
            SetLegActive(false, true);
            ikRightLeg.position = leg2;
            ikRightLeg.rotation = reg2;
        }
        else
        {
            SetLegActive(false, false);
        }

    }

    private void SetLegActive(bool left, bool active)
    {
        ik.solver.SetEffectorWeights(left ? FullBodyBipedEffector.LeftFoot : FullBodyBipedEffector.RightFoot,
            active ? 1f : 0f, 0f);
    }

    private void SetHipsActive(bool active)
    {
        ik.solver.SetEffectorWeights(FullBodyBipedEffector.Body, active ? 1f : 0f, 0f);
    }

}