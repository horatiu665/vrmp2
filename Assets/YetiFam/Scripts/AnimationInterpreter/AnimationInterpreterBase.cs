﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using RootMotion.FinalIK;

/// <summary>
/// This component (and derivatives) purely interprets human-recorded animation data into character data.
/// For some it is a simple 1-to-1 transfer, with few params for scale and whatnot, but it can also be more fancy
/// </summary>
public abstract class AnimationInterpreterBase : MonoBehaviour
{
    /// <summary>
    /// The opposite of <see cref="MirrorGuyData.GetFromPlayer()"/>
    /// sets the saved data to the IKs on the character.
    /// </summary>
    /// <example>
    /// Can set the data with various modifications, based on the actual character.
    ///     for example: shoulder/hand scaling for long hands, 
    ///     hipsOffset can be different for each leg when the legs are
    ///     far from each other like the stumpy characters, etc)
    /// We can even get leg data, duplicate/mirror/transform it 
    ///     and apply to 8 SPIDER legs instead of humanoid legs
    ///     in another version of this script, or when we decide to extend it ;)
    /// </example>
    /// <param name="data">Raw (saved) animation data from the player</param>
    public abstract void SetVectors(MirrorGuyData data);
    
}
