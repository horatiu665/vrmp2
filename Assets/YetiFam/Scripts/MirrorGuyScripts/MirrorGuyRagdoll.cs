using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using RootMotion.FinalIK;
using System;

public class MirrorGuyRagdoll : MonoBehaviour
{
    [SerializeField]
    private MirrorGuyLimb[] _allLimbs;
    public MirrorGuyLimb[] allLimbs
    {
        get
        {
            if (_allLimbs == null || _allLimbs.Length == 0)
            {
                _allLimbs = GetComponentsInChildren<MirrorGuyLimb>();
            }
            return _allLimbs;
        }
    }

    [SerializeField]
    private Animator _animator;
    public Animator animator
    {
        get
        {
            if (_animator == null)
            {
                _animator = GetComponent<Animator>();
            }
            return _animator;
        }
    }

    [SerializeField]
    private RagdollUtility _ragdollUtility;
    public RagdollUtility ragdollUtility
    {
        get
        {
            if (_ragdollUtility == null)
            {
                _ragdollUtility = GetComponent<RagdollUtility>();
            }
            return _ragdollUtility;
        }
    }

    // same method as on ragdollUtility. DON'T SQUANDER
    public bool isRagdoll { get { return !allLimbs[0].r.isKinematic && !animator.enabled; } }

    public SmartSound soundOnDeath;

    /// <summary>
    /// If negative, never makes ragdoll.
    /// </summary>
    [Tooltip("If negative, never makes ragdoll.")]
    public float globalMinForceForRagdolls = -1;

    public float deathForceMultiplierHitLimb = 1f;
    public float deathForceMultiplierOtherLimbs = 0.2f;

    [Header("Keep track of who hit me")]
    public bool saveCollisionHistory = false;
    public List<GameObject> whoHitMeDebug = new List<GameObject>(100);

    private void OnValidate()
    {
        if (animator != null)
        {
        }
        if (ragdollUtility != null)
        {
        }
        if (allLimbs != null)
        {
        }
    }

    public void TeleOnCollisionEnter(MirrorGuyLimb limb, Collision collision)
    {
        if (!enabled)
        {
            return;
        }

        // only get hit by other rigidbodies (otherwise WE are the stupid rigidbody who hit a static mesh (like the ground))
        if (collision.rigidbody == null)
        {
            return;
        }

        if (saveCollisionHistory)
        {
            whoHitMeDebug.Add(collision.gameObject);
        }

        if (globalMinForceForRagdolls < 0)
        {
            return;
        }

        var mffk = limb.minForceForKill;
        var mSqr = mffk * mffk;
        var relSqr = collision.relativeVelocity.sqrMagnitude;

        if (relSqr > mSqr && relSqr > globalMinForceForRagdolls * globalMinForceForRagdolls)
        {
            if (!isRagdoll)
            {
                // react to collision
                // make sounds, enable ragdoll, attempt dodge, etc
                if (soundOnDeath != null)
                {
                    soundOnDeath.Play();
                }
                Ragdollify();

                ApplyDeathForce(limb, collision);
            }
        }
    }

    private void ApplyDeathForce(MirrorGuyLimb limb, Collision collision)
    {
        var force = collision.relativeVelocity;
        limb.r.AddForce(force * deathForceMultiplierHitLimb, ForceMode.Impulse);
        for (int i = 0; i < allLimbs.Length; i++)
        {
            if (allLimbs[i] != limb)
            {
                allLimbs[i].r.AddForce(force * deathForceMultiplierOtherLimbs, ForceMode.Impulse);
            }
        }
    }

    public void TeleOnCollisionExit(MirrorGuyLimb limb, Collision collision)
    {
        if (!enabled)
        {
            return;
        }

        // nothing happens on exit
    }

    [DebugButton]
    void Ragdollify()
    {
        ragdollUtility.EnableRagdoll();
    }

    [DebugButton]
    void UnRagdollify()
    {
        ragdollUtility.DisableRagdoll();
    }

    [DebugButton]
    void SetKinematic(bool isKine)
    {
        foreach (var l in allLimbs)
        {
            Debug.Log(l.name, l.gameObject);
            l.r.isKinematic = isKine;
        }

        Debug.Log("[MirrorGuyRagdoll] Set Kinematic count " + allLimbs.Length);
    }
}