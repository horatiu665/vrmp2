﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Passes collisions on to the parent ragdoll script, so stuff can happen there instead of here
/// </summary>
public class MirrorGuyLimb : MonoBehaviour
{
    [SerializeField]
    private MirrorGuyRagdoll _ragdoll;
    public MirrorGuyRagdoll ragdoll
    {
        get
        {
            if (_ragdoll == null)
            {
                _ragdoll = GetComponentInParent<MirrorGuyRagdoll>();
            }
            return _ragdoll;
        }
    }

    [SerializeField]
    private Rigidbody _r;
    public Rigidbody r
    {
        get
        {
            if (_r == null)
            {
                _r = GetComponent<Rigidbody>();
            }
            return _r;
        }
    }

    private void OnValidate()
    {
        if (ragdoll != null)
        {
        }
        if (r != null)
        {
        }
    }

    public float minForceForKill = 1f;

    private void OnCollisionEnter(Collision collision)
    {
        ragdoll.TeleOnCollisionEnter(this, collision);
    }

    private void OnCollisionExit(Collision collision)
    {
        ragdoll.TeleOnCollisionExit(this, collision);
    }
}