using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// The player offset = local playspace in the game. used for recording animations in a relative position
/// </summary>
public class MirrorGuyPlayerOffset : MonoBehaviour
{
    MirrorGuyTrackedObject playerRight { get { return MirrorGuyTrackedObject.playerRight; } }
    MirrorGuyTrackedObject playerHead { get { return MirrorGuyTrackedObject.playerHead; } }
    ArenaFeedback arenaFeedback;
    Collider c;

    public Text labelPlayerHeight;

    public static MirrorGuyPlayerOffset instance;
    private float arenaLookTime;
    private float arenaPressTime;

    [Header("Selection params")]
    public float selectLookTime = 0.2f;
    public float selectPressTime = 0.1f;

    [Header("Player normalization params: scales player so they record animations that are 1.71m tall ;)")]
    [SerializeField]
    private float playerHeightUponCalibration = 1.71f;
    private const float idealPlayerHeight = 1.71f;
    [SerializeField]
    private float headOffsetFromVivePos = 0.15f;
    private const string playerHeightPrefsId = "playerHeightCalibration";
    private float playerScaleMultiplier
    {
        get
        {
            // ideal height is (idealPlayerHeight) (1.71f) (horatiu's height ;))      .... corresponds to scale 1
            // player height is H (2m f.x)                      .... corresponds to scale X (~1.2 f.x)
            // so X = H / idealPlayerHeight
            return playerHeightUponCalibration / idealPlayerHeight;
        }
    }

    void Awake()
    {
        instance = this;
        arenaFeedback = GetComponentInChildren<ArenaFeedback>();
        c = arenaFeedback.GetComponent<Collider>();
    }

    private void OnEnable()
    {
        var savedPlayerHeight = PlayerPrefs.GetFloat(playerHeightPrefsId, 0f);
        if (savedPlayerHeight > 0f)
        {
            CalibratePlayer(savedPlayerHeight, false);
            PlayerScaleLabelReset();
        }
    }

    private void PlayerScaleLabelReset()
    {
        if (labelPlayerHeight != null)
        {
            labelPlayerHeight.text = playerHeightUponCalibration.ToString("F2") + "m" + "\nYour ~height";
        }
        else
        {
            Debug.LogWarning("[MirrorGuyPlayerOffset] Warning! Create a label here to give feedback to player for their height!", gameObject);
        }
    }

    private void Update()
    {
        Vector3 lookDir = playerHead.transform.forward;
        RaycastHit info;
        // if looking at some obj
        if (Physics.Raycast(playerHead.position, lookDir, out info, 1000f))
        {
            // if it's this collider
            if (info.collider == c)
            {
                // player is looking at us
                // better highlight.
                arenaLookTime += Time.deltaTime;
                if (arenaLookTime > selectLookTime)
                {
                    if (InputPressedCalibrate())
                    {
                        arenaPressTime += Time.deltaTime;
                        // feedback on arena start
                        arenaFeedback.DoFeedback();
                    }
                    else
                    {
                        arenaFeedback.DoPreFeedback();
                        arenaPressTime = 0f;
                    }
                }
                else
                {
                    arenaPressTime = 0;
                }
            }
            else
            {
                arenaLookTime = 0f;
                arenaPressTime = 0f;
            }
        }
        else
        {
            arenaLookTime = 0f;
            arenaPressTime = 0f;
        }

        if (arenaPressTime > selectPressTime)
        {
            // calibrate!
            CalibratePlayspace();
            arenaPressTime = 0f;
        }
    }

    [DebugButton]
    void ClearPlayerPrefs()
    {
        PlayerPrefs.DeleteKey(playerHeightPrefsId);
    }

    [DebugButton]
    public void CalibratePlayer()
    {
        // we take playerHead pos, compared to playerOffset position, and add headOffsetFromVivePos meters because that's how SteamVR calculates heights
        CalibratePlayer(playerHead.transform.position.y - transform.position.y + headOffsetFromVivePos, true);
    }

    private void CalibratePlayer(float playerHeadPositionY, bool savePlayerPrefs = true)
    {
        if (playerHeadPositionY > 0)
        {
            // local position is real height in meters.
            // we want that to be 2m for the sake of normalizing player heights and animations
            // so if real height is H ... corresponds to 1f scale
            // target height is    2m ... corresponds to X scale
            // x = 2 / H
            this.playerHeightUponCalibration = playerHeadPositionY;
            transform.localScale = Vector3.one * playerScaleMultiplier;
            PlayerScaleLabelReset();

            if (savePlayerPrefs)
            {
                PlayerPrefs.SetFloat(playerHeightPrefsId, playerHeadPositionY);
            }
        }
    }

    private void CalibratePlayspace()
    {
        var pos = playerHead.position;
        pos.y = playerHead.transform.root.position.y;
        transform.position = pos;

        transform.rotation = playerHead.rotation;
        transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
    }

    private bool InputPressedCalibrate()
    {
        return RightTouchpad();
    }

    private bool RightTouchpad()
    {
        return playerRight.controller.GetPress(ControllerWrapper.ButtonMask.Touchpad);
    }

}

