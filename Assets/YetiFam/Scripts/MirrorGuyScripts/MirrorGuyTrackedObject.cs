using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class MirrorGuyTrackedObject : MonoBehaviour
{
    public enum Type
    {
        Head,
        Left,
        Right,
        LeftLeg,
        RightLeg,
        Hips
    }
    public Type type;

    public static List<MirrorGuyTrackedObject> objects = new List<MirrorGuyTrackedObject>();

    public static MirrorGuyTrackedObject playerLeft, playerRight, playerHead, playerLeftLeg, playerRightLeg, playerHips;

    public ControllerWrapper controller { get; private set; }

    SteamVR_TrackedObject trackedObject;

    public int deviceId
    {
        get
        {
            if (type == Type.Head)
            {
                return (int)SteamVR_TrackedObject.EIndex.Hmd;
            }
            if (trackedObject == null)
            {
                return -1;
            }
            return (int)trackedObject.index;
        }
        set
        {
            SetTrackedObjectIndex(value);
        }
    }

    private LegMirror _legMirror;
    public LegMirror legMirror
    {
        get
        {
            if (_legMirror == null)
            {
                _legMirror = trackedObject.GetComponent<LegMirror>();
            }
            return _legMirror;
        }
    }

    public Transform positionOverride, rotationOverride;

    // makes it easier to find references. try it!
    public new Transform transform
    {
        get
        {
            return base.transform;
        }
    }

    public Vector3 position
    {
        get
        {
            if (positionOverride != null)
            {
                return positionOverride.position;
            }
            return base.transform.position;
        }
    }

    public Quaternion rotation
    {
        get
        {
            if (rotationOverride != null)
            {
                return rotationOverride.rotation;
            }
            return base.transform.rotation;
        }
    }

    /// <summary>
    /// Can be used for recording?
    /// if head or controllers, usually yes. If hips, depends on the existence of that trackedObject/vive tracker.
    /// If legs, same question. Can be simulated with some other method such as a mirror script.... or ragdolls ;)
    /// </summary>
    public bool isValid
    {
        get
        {
            if (type == Type.Head) // HMD is always valid. Because it doesn't have a trackedObject
            {
                return true;
            }
            if (legMirror != null)
            {
                return true;
            }
            return trackedObject != null && trackedObject.isValid;
        }
    }

    private void Awake()
    {
        controller = GetComponentInParent<ControllerWrapper>();
        trackedObject = GetComponentInParent<SteamVR_TrackedObject>();
        objects.Add(this);

    }

    private void Start()
    {
        var trackeds = objects;
        for (int i = 0; i < trackeds.Count; i++)
        {
            switch (trackeds[i].type)
            {
            case Type.Head:
                playerHead = trackeds[i];
                break;
            case Type.Left:
                playerLeft = trackeds[i];
                break;
            case Type.Right:
                playerRight = trackeds[i];
                break;
            case Type.LeftLeg:
                playerLeftLeg = trackeds[i];
                break;
            case Type.RightLeg:
                playerRightLeg = trackeds[i];
                break;
            case Type.Hips:
                playerHips = trackeds[i];
                break;
            }
        }

        if (trackedObject != null)
        {
            // get player prefs index
            trackedObject.index = (SteamVR_TrackedObject.EIndex)PlayerPrefs.GetInt(type.ToString() + "_trackedId", (int)trackedObject.index);
        }

    }

    [DebugButton]
    public void ToggleNextValidDeviceId()
    {
        for (int ii = (int)trackedObject.index + 1; ii < SteamVR.connected.Length + (int)trackedObject.index; ii++)
        {
            if (ii >= 0)
            {
                var i = ii % SteamVR.connected.Length;
                if (SteamVR.connected[i])
                {
                    SetTrackedObjectIndex(i);
                    return;
                }
            }
        }
    }

    private void SetTrackedObjectIndex(int index)
    {
        trackedObject.index = (SteamVR_TrackedObject.EIndex)index;
        // always save it in player prefs
        PlayerPrefs.SetInt(type.ToString() + "_trackedId", (int)trackedObject.index);
    }
}