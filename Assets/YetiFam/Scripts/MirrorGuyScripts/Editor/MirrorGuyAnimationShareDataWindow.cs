﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

public class MirrorGuyAnimationShareDataWindow : EditorWindow
{
    private MirrorGuyAnimationShare a;
    private YetiIkAnimRecord animRecord;

    public static MirrorGuyAnimationShareDataWindow Get()
    {
        var w = GetWindow<MirrorGuyAnimationShareDataWindow>(false, "MirrorGuyData", true);

        return w;
    }

    public static void Get(MirrorGuyAnimationShare a, YetiIkAnimRecord animRecord)
    {
        var w = Get();
        w.a = a;
        w.animRecord = animRecord;
    }

    private void OnGUI()
    {
        // draw animation curves for each of the data curves...

    }
}
