﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[CustomEditor(typeof(MirrorGuyAnimationShare), true)]
public class MirrorGuyAnimationShareEditor : Editor
{
    private MirrorGuyAnimationShare _a;
    public MirrorGuyAnimationShare a
    {
        get
        {
            if (_a == null)
            {
                _a = target as MirrorGuyAnimationShare;
            }
            return _a;
        }
    }

    private YetiIkAnimRecord _animRecord;
    public YetiIkAnimRecord animRecord
    {
        get
        {
            if (_animRecord == null)
            {
                _animRecord = FindObjectOfType<YetiIkAnimRecord>();
            }
            return _animRecord;
        }
    }

    private AnimationClip overwriteAnimation;

    private int seamlessFrames = 0;
    private float curFrameOnSlider = 0;

    private void OnEnable()
    {
        UnityEditor.EditorApplication.update += Update;
    }

    private void Update()
    {
        Repaint();
    }

    private void OnDisable()
    {
        UnityEditor.EditorApplication.update -= Update;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        if (a != null)
        {
            EditorGUILayout.Space();

            EditorGUILayout.IntField("Frame count", a.data.Count);

            if (!Application.isPlaying)
            {
                GUI.enabled = false;
            }

            var rect = EditorGUILayout.GetControlRect(false);
            var littleSpace = 6f;
            var littleRect = rect;

            // if anim != null, resize rect to fit frame view slider
            if (animRecord.playAnimation != null)
            {
                littleRect.width = 45;
                rect.width -= littleRect.width + littleSpace;
                rect.x += littleRect.width + littleSpace;
            }
            else
            {
                // center the button
                var oldWidth = littleRect.width;
                littleRect.width *= 0.8f;
                littleRect.x += (oldWidth - littleRect.width) / 2;
                littleRect.height *= 2.5f;
            }

            var playing = animRecord.state == YetiIkAnimRecord.AnimationState.Play && animRecord.playAnimation == a;

            var playButton = GUI.Button(littleRect, playing ? "||" : ">");
            if (playButton)
            {
                animRecord.playAnimation = a;
                if (!playing)
                {
                    animRecord.state = YetiIkAnimRecord.AnimationState.Play;
                }
                else
                {
                    animRecord.state = YetiIkAnimRecord.AnimationState.PlayPaused;
                }
            }

            if (animRecord.playAnimation == a)
            {
                var redZone = rect;
                // slider int box remains constant size
                var sliderIntBoxWidth = 55;
                redZone.width -= sliderIntBoxWidth;
                redZone.height = 6;
                redZone.y += 6;
                redZone.width /= 6;
                EditorGUI.DrawRect(redZone, new Color32(102, 124, 137, 255));
                redZone.x += redZone.width * 5;
                EditorGUI.DrawRect(redZone, new Color32(102, 124, 137, 255));
                curFrameOnSlider = EditorGUI.IntSlider(rect, (int)curFrameOnSlider, -(int)((a.data.Count - 1) * 0.25f), (int)((a.data.Count - 1) * 1.25f));

                // if playing, scroll normal positions
                if (curFrameOnSlider != (int)animRecord.curPlaybackFrame)
                {
                    if (playing)
                    {
                        curFrameOnSlider = (int)animRecord.curPlaybackFrame;
                    }
                    else
                    {
                        animRecord.curPlaybackFrame = (int)curFrameOnSlider;
                    }
                }

                //animRecord.curPlaybackFrame = EditorGUI.IntSlider(rect, animRecord.curPlaybackFrame, 0, a.data.Count - 1);
                animRecord.playbackFPS = EditorGUILayout.FloatField("Framerate", animRecord.playbackFPS);

                EditorGUILayout.BeginHorizontal();
                if (playing)
                {
                    GUI.enabled = false;
                }

                if (GUILayout.Button(new GUIContent("Del Before", "Deletes all frames from current frame to start of animation, excluding current")))
                {
                    a.DeleteFirst((int)animRecord.curPlaybackFrame);
                    animRecord.curPlaybackFrame = 0;
                }
                if (GUILayout.Button(new GUIContent("< Add", "Adds a number of frames that interpolate between beginning and end of the clip")))
                {
                    a.GenerateSeamlessFramesBefore(seamlessFrames);
                }

                var style = new GUIStyle(GUI.skin.textField);
                style.alignment = TextAnchor.MiddleCenter;
                style.overflow.top -= 2;
                style.overflow.bottom += 2;
                style.padding.top += 4;
                seamlessFrames = Mathf.Clamp(EditorGUILayout.IntField(seamlessFrames, style, GUILayout.MaxWidth(60)), 0, 100);


                if (GUILayout.Button(new GUIContent("Add >", "Adds a number of frames that interpolate between end and beginning of the clip")))
                {
                    a.GenerateSeamlessFrames(seamlessFrames);
                }
                if (GUILayout.Button(new GUIContent("Del After", "Deletes all frames from current frame to end of animation")))
                {
                    a.DeleteLast(a.data.Count - (int)animRecord.curPlaybackFrame - 1);
                }

                EditorGUILayout.EndHorizontal();

                //EditorGUILayout.BeginHorizontal();

                //if (GUILayout.Button("Add boomerang"))
                //{
                //    a.AddBoomerang();
                //}

                //EditorGUILayout.EndHorizontal();

                EditorGUILayout.LabelField("Dangerous controls", EditorStyles.boldLabel);

                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Root Motion", GUILayout.Width(64));
                if (GUILayout.Button("Del XZ"))
                {
                    Undo.RecordObject(a, "Delete XZ hips animation");
                    DeleteXZ(a);
                    Undo.IncrementCurrentGroup();
                }
                if (GUILayout.Button("Del Y"))
                {
                    Undo.RecordObject(a, "Delete Y hips animation");
                    DeleteY(a);
                    Undo.IncrementCurrentGroup();
                }
                EditorGUILayout.EndHorizontal();

                // animation curve editor....??? for editing the actual animation curves/data saved in the animation
                //if (GUILayout.Button("Edit Animation"))
                //{
                //    MirrorGuyAnimationShareDataWindow.Get(a, animRecord);
                //}


            }
            else // A DIFFERENT ONE IS PLAYING
            {
                if (animRecord.playAnimation != null)
                {
                    EditorGUI.ObjectField(rect, "Now Playing", animRecord.playAnimation, typeof(MirrorGuyAnimationShare), true);
                }
                //EditorGUILayout.GetControlRect();
                //EditorGUILayout.GetControlRect();
                //EditorGUILayout.GetControlRect();
            }

            // if app not playing
            GUI.enabled = true;

            EditorGUILayout.Space();

            if (GUILayout.Button("Save Clip"))
            {
                CreateClip();
            }

            EditorGUILayout.BeginHorizontal();
            overwriteAnimation = (AnimationClip)EditorGUILayout.ObjectField("Overwrite animation", overwriteAnimation, typeof(AnimationClip), false);
            if (GUILayout.Button("Overwrite Clip"))
            {
                CopyToClip(overwriteAnimation, animRecord.playbackFPS);
            }

            EditorGUILayout.EndHorizontal();


        }

        EditorGUILayout.Space();

        serializedObject.ApplyModifiedProperties();

    }

    // MOVE TO MIRRORGUYANIMATIONSHARE!!! NOT THE STUPID EDITOR
    private void DeleteY(MirrorGuyAnimationShare a)
    {
        var hipsAvg = a.data.Aggregate<MirrorGuyData, Vector3>(Vector3.zero, (s, m) => m.hipsPos);
        hipsAvg /= a.data.Count;
        for (int i = 0; i < a.data.Count; i++)
        {
            var data = a.data[i];
            data.hipsPos = new Vector3(data.hipsPos.x, hipsAvg.y, data.hipsPos.z);
            a.data[i] = data;
        }
    }

    // MOVE TO MIRRORGUYANIMATIONSHARE!!! NOT THE STUPID EDITOR
    private void DeleteXZ(MirrorGuyAnimationShare a)
    {
        var hipsAvg = a.data.Aggregate<MirrorGuyData, Vector3>(Vector3.zero, (s, m) => m.hipsPos);
        hipsAvg /= a.data.Count;
        for (int i = 0; i < a.data.Count; i++)
        {
            var data = a.data[i];

            // store hips delta and add it from all other animation values (acts as root motion)
            var deltaHipsMove = data.hipsPos;
            data.hipsPos = new Vector3(hipsAvg.x, data.hipsPos.y, hipsAvg.z);
            // new hips pos minus old hips pos is the delta.
            deltaHipsMove = data.hipsPos - deltaHipsMove;

            data.handLeftPos += deltaHipsMove;
            data.handRightPos += deltaHipsMove;
            data.headPos += deltaHipsMove;
            data.legLeftPos += deltaHipsMove;
            data.legRightPos += deltaHipsMove;

            a.data[i] = data;
        }
    }

    private const string csHandRight = "hand right";
    private const string csHandLeft = "hand left";
    private const string csHead = "head";
    private const string csHips = "hips";
    private const string csLegRight = "leg right";
    private const string csLegLeft = "leg left";


    public void CopyToClip(AnimationClip ani, float framerate)
    {
        // set undo for animation clip ani
        UnityEditor.Undo.RegisterCompleteObjectUndo(ani, "Copy MirrorGuy data to clip " + ani.name);

        ani.ClearCurves();

        // names for all ik body parts
        List<string> names = new List<string>()
        {
            csHandRight,
            csHandLeft,
            csHead,
            csHips,
            csLegRight,
            csLegLeft,
        };

        // create dictionary of animation curves, 7 for each body part (3 pos, 4 rot), plus the root motion.
        Dictionary<string, AnimationCurve> kurwa = new Dictionary<string, AnimationCurve>();
        foreach (var name in names)
        {
            kurwa.Add(name + "/localPosition.x", new AnimationCurve());
            kurwa.Add(name + "/localPosition.y", new AnimationCurve());
            kurwa.Add(name + "/localPosition.z", new AnimationCurve());
            kurwa.Add(name + "/localRotation.x", new AnimationCurve());
            kurwa.Add(name + "/localRotation.y", new AnimationCurve());
            kurwa.Add(name + "/localRotation.z", new AnimationCurve());
            kurwa.Add(name + "/localRotation.w", new AnimationCurve());
        }

        // for every recorded frame
        float frameDur = 1f / framerate;
        for (int i = 0; i < a.data.Count; i++)
        {
            var f = a.data[i];
            // set curves for each tracked obj
            foreach (var name in names)
            {
                Vector3 pos = Vector3.zero;
                Quaternion rot = Quaternion.identity;
                switch (name)
                {
                case csLegLeft:
                    pos = f.legLeftPos;
                    rot = f.legLeftRot;
                    break;
                case csLegRight:
                    pos = f.legRightPos;
                    rot = f.legRightRot;
                    break;
                case csHandLeft:
                    pos = f.handLeftPos;
                    rot = f.handLeftRot;
                    break;
                case csHandRight:
                    pos = f.handRightPos;
                    rot = f.handRightRot;
                    break;
                case csHead:
                    pos = f.headPos;
                    rot = f.headRot;
                    break;
                case csHips:
                    pos = f.hipsPos;
                    rot = f.hipsRot;
                    break;
                }

                if (pos == Vector3.zero && rot == Quaternion.identity)
                {
                    continue;
                }
                // keep it local space
                //pos = yetiTransform.TransformPoint(pos);
                //rot = yetiTransform.rotation * rot;
                kurwa[name + "/localPosition.x"].AddKey(i * frameDur, pos.x);
                kurwa[name + "/localPosition.y"].AddKey(i * frameDur, pos.y);
                kurwa[name + "/localPosition.z"].AddKey(i * frameDur, pos.z);
                kurwa[name + "/localRotation.x"].AddKey(i * frameDur, rot.x);
                kurwa[name + "/localRotation.y"].AddKey(i * frameDur, rot.y);
                kurwa[name + "/localRotation.z"].AddKey(i * frameDur, rot.z);
                kurwa[name + "/localRotation.w"].AddKey(i * frameDur, rot.w);
            }
        }

        // this is the root motion curve. by default applies the hips XZ movement. can/should be edited further in the unity animation editor.
        {
            var rootMotionCurveX = new AnimationCurve();
            var rootMotionCurveY = new AnimationCurve();
            var rootMotionCurveZ = new AnimationCurve();

            // add the first key at (0,0)
            rootMotionCurveX.AddKey(new Keyframe() { time = 0, value = 0 });
            rootMotionCurveZ.AddKey(new Keyframe() { time = 0, value = 0 });

            // add key the last XZ position of the hips.
            var lastTime = (a.data.Count - 1) * frameDur;
            rootMotionCurveX.AddKey(new Keyframe() { time = lastTime, value = a.data[a.data.Count - 1].hipsPos.x });
            rootMotionCurveZ.AddKey(new Keyframe() { time = lastTime, value = a.data[a.data.Count - 1].hipsPos.z });

            kurwa.Add("/localPosition.x", rootMotionCurveX);
            kurwa.Add("/localPosition.y", rootMotionCurveY);
            kurwa.Add("/localPosition.z", rootMotionCurveZ);
        }

        // finally, apply all curves to the animation clip
        foreach (var k in kurwa)
        {
            k.Value.postWrapMode = WrapMode.Loop;
            if (k.Value.keys.Length > 0)
            {
                // relative path is the first part of the string, uses transform.name and / to go down in the hierarchy. if root, there is no "/" so we just pass blank.
                var relativePath = k.Key.Contains("/") ? k.Key.Substring(0, k.Key.LastIndexOf("/")) : "";
                // property name is the last bit after the last "/", such as localPosition.x; but if there is no "/" we just take the whole thing.
                var propertyName = k.Key.Contains("/") ? k.Key.Substring(k.Key.LastIndexOf("/") + 1) : k.Key;
                ani.SetCurve(relativePath, typeof(Transform), propertyName, k.Value);
            }
        }

        var debug = "[MirrorGuyAnimationShare] Added " + kurwa.Count + " animation kurwes for animation " + ani.name;
        foreach (var k in kurwa)
        {
            debug += "\n" + k.Key + " - " + k.Value.length + " frames";
        }
        Debug.Log(debug);
    }

    public void ReadFromClip(AnimationClip ani, float framerate)
    {
        // TODOOOOOOOOOOOOOOOOOOOOOOOOOOOOOBIDOOBIDOO
    }

    [DebugButton]
    public void CreateClip()
    {
        AnimationClip newClip = new AnimationClip();
        a.folderPath = a.folderPath.Replace('\\', Path.DirectorySeparatorChar).Replace('/', Path.DirectorySeparatorChar);
        var path = Path.Combine(a.folderPath, a.transform.name + ".anim");
        var count = 1;
        while (File.Exists(path))
        {
            path = Path.Combine(a.folderPath, a.transform.name + " (" + count++ + ").anim");
        }

#if UNITY_EDITOR
        UnityEditor.AssetDatabase.CreateAsset(newClip, path);
        UnityEditor.AssetDatabase.SaveAssets();
#endif
        CopyToClip(newClip, animRecord.playbackFPS);

    }
}