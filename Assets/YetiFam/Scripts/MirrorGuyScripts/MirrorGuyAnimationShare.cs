using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

/// <summary>
/// Stores animation data, and provides functionality to manipulate it such as looping, boomeranging and smoothing, and any other filters should be implemented here.
/// </summary>
public class MirrorGuyAnimationShare : MonoBehaviour
{
    [HideInInspector]
    public List<MirrorGuyData> data;

    [Header("Clip creation")]
    public string folderPath = @"Assets\YetiFam\ik animations\anims";
    
    [DebugButton]
    public void GenerateSeamlessFrames(int framesAtEnd)
    {
        var first = data.First();
        var last = data.Last();
        for (int i = 1; i <= framesAtEnd; i++)
        {
            var inBetween = i / (float)(framesAtEnd);
            var newData = new MirrorGuyData()
            {
                handLeftPos = Vector3.Lerp(last.handLeftPos, first.handLeftPos, inBetween),
                handRightPos = Vector3.Lerp(last.handRightPos, first.handRightPos, inBetween),
                headPos = Vector3.Lerp(last.headPos, first.headPos, inBetween),
                handLeftRot = Quaternion.Slerp(last.handLeftRot, first.handLeftRot, inBetween),
                handRightRot = Quaternion.Slerp(last.handRightRot, first.handRightRot, inBetween),
                headRot = Quaternion.Slerp(last.headRot, first.headRot, inBetween),
                legLeftPos = Vector3.Lerp(last.legLeftPos, first.legLeftPos, inBetween),
                legRightPos = Vector3.Lerp(last.legRightPos, first.legRightPos, inBetween),
                legLeftRot = Quaternion.Slerp(last.legLeftRot, first.legLeftRot, inBetween),
                legRightRot = Quaternion.Slerp(last.legRightRot, first.legRightRot, inBetween),
                hipsPos = Vector3.Lerp(last.hipsPos, first.hipsPos, inBetween),
                hipsRot = Quaternion.Slerp(last.hipsRot, first.hipsRot, inBetween),

            };
            data.Add(newData);
        }
    }

    [DebugButton]
    public void GenerateSeamlessFramesBefore(int framesAtStart)
    {
        var first = data.First();
        var last = data.Last();
        for (int i = 1; i <= framesAtStart; i++)
        {
            // 0..1
            var inBetween = i / (float)(framesAtStart);
            var newData = new MirrorGuyData()
            {
                handLeftPos = Vector3.Lerp(first.handLeftPos, last.handLeftPos, inBetween),
                handRightPos = Vector3.Lerp(first.handRightPos, last.handRightPos, inBetween),
                headPos = Vector3.Lerp(first.headPos, last.headPos, inBetween),
                handLeftRot = Quaternion.Slerp(first.handLeftRot, last.handLeftRot, inBetween),
                handRightRot = Quaternion.Slerp(first.handRightRot, last.handRightRot, inBetween),
                headRot = Quaternion.Slerp(first.headRot, last.headRot, inBetween),
                legLeftPos = Vector3.Lerp(first.legLeftPos, last.legLeftPos, inBetween),
                legRightPos = Vector3.Lerp(first.legRightPos, last.legRightPos, inBetween),
                legLeftRot = Quaternion.Slerp(first.legLeftRot, last.legLeftRot, inBetween),
                legRightRot = Quaternion.Slerp(first.legRightRot, last.legRightRot, inBetween),
                hipsPos = Vector3.Lerp(first.hipsPos, last.hipsPos, inBetween),
                hipsRot = Quaternion.Slerp(first.hipsRot, last.hipsRot, inBetween),
            };
            data.Insert(0, newData);
        }
    }

    public void DeleteFirst(int count)
    {
        data.RemoveRange(0, count);
    }

    [DebugButton]
    public void DeleteLast(int count)
    {
        data.RemoveRange(data.Count - count, count);
    }

    public void AddBoomerang()
    {
        var frames = data.Count;
        for (int i = frames - 1; i >= 1; i--)
        {
            var newdata = new MirrorGuyData()
            {
                handLeftPos = data[i].handLeftPos,
                handRightPos = data[i].handRightPos,
                headPos = data[i].headPos,
                handLeftRot = data[i].handLeftRot,
                handRightRot = data[i].handRightRot,
                headRot = data[i].headRot,
                hipsPos = data[i].hipsPos,
                hipsRot = data[i].hipsRot,
                legLeftPos = data[i].legLeftPos,
                legRightPos = data[i].legRightPos,
                legLeftRot = data[i].legLeftRot,
                legRightRot = data[i].legRightRot,
            };
            data.Add(newdata);
        }
    }
}