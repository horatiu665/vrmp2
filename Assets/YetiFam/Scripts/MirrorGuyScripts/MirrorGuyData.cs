using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

/// <summary>
/// Structure containing one frame of an animation. All six tracked objects are recorded in local player space, and it is up to the ik animators to interpret this data for their own respective setup
/// for example: they can opt to perform shoulder-scaling = calculate hands position compared to (head pos) + (shoulder offset), and then scale the result (for long or stumpy hands)
/// or they can choose to take legs data, calculate compared to hips, and apply it four times with a rotation offset, to recreate a spider rig with 8 legs.
///     but the idea is, humanoid animations should always be compatible with all, and have a standard format
/// </summary>
[Serializable]
public struct MirrorGuyData
{
    static MirrorGuyTrackedObject playerLeft { get { return MirrorGuyTrackedObject.playerLeft; } }
    static MirrorGuyTrackedObject playerRight { get { return MirrorGuyTrackedObject.playerRight; } }
    static MirrorGuyTrackedObject playerHead { get { return MirrorGuyTrackedObject.playerHead; } }
    static MirrorGuyTrackedObject playerLeftLeg { get { return MirrorGuyTrackedObject.playerLeftLeg; } }
    static MirrorGuyTrackedObject playerRightLeg { get { return MirrorGuyTrackedObject.playerRightLeg; } }
    static MirrorGuyTrackedObject playerHips { get { return MirrorGuyTrackedObject.playerHips; } }
    static Transform playerOffset { get { return MirrorGuyPlayerOffset.instance.transform; } }

    // The actual data for each tracked object
    // left, right, head, leftleg, rightleg, hips.

    /// <summary>
    /// Left hand
    /// </summary>
    [HideInInspector]
    public Vector3 handLeftPos;
    /// <summary>
    /// Right hand
    /// </summary>
    [HideInInspector]
    public Vector3 handRightPos;
    /// <summary>
    /// Head
    /// </summary>
    [HideInInspector]
    public Vector3 headPos;
    /// <summary>
    /// Left leg
    /// </summary>
    [HideInInspector]
    public Vector3 legLeftPos;
    /// <summary>
    /// Right leg
    /// </summary>
    [HideInInspector]
    public Vector3 legRightPos;
    /// <summary>
    /// Hips
    /// </summary>
    [HideInInspector]
    public Vector3 hipsPos;

    [HideInInspector]
    public Quaternion handLeftRot;
    [HideInInspector]
    public Quaternion handRightRot;
    [HideInInspector]
    public Quaternion headRot;
    [HideInInspector]
    public Quaternion legLeftRot;
    [HideInInspector]
    public Quaternion legRightRot;
    [HideInInspector]
    public Quaternion hipsRot;

    public static MirrorGuyData GetFromPlayer()
    {
        if (AnyNull())
        {
            return new MirrorGuyData();
        }

        // Take care of hands and head
        var l1 = playerOffset.InverseTransformPoint(playerLeft.position);
        var l2 = playerOffset.InverseTransformPoint(playerRight.position);
        var r1 = Quaternion.Inverse(playerOffset.rotation) * playerLeft.rotation;
        var r2 = Quaternion.Inverse(playerOffset.rotation) * playerRight.rotation;
        // head
        var l3 = playerOffset.InverseTransformPoint(playerHead.position);
        var r3 = Quaternion.Inverse(playerOffset.rotation) * playerHead.rotation;

        // Take care of hips
        Vector3 hipos = Vector3.zero;
        Quaternion hirot = Quaternion.identity;
        if (playerHips.isValid)
        {
            hipos = playerOffset.InverseTransformPoint(playerHips.position);
            hirot = Quaternion.Inverse(playerOffset.rotation) * playerHips.rotation;
        }

        // Take care of legs
        Vector3 leg1 = Vector3.zero;
        Vector3 leg2 = Vector3.zero;
        Quaternion reg1 = Quaternion.identity;
        Quaternion reg2 = Quaternion.identity;

        if (playerLeftLeg.isValid)
        {
            leg1 = playerOffset.InverseTransformPoint(playerLeftLeg.position);
            reg1 = Quaternion.Inverse(playerOffset.rotation) * playerLeftLeg.rotation;
        }

        if (playerRightLeg.isValid)
        {
            leg2 = playerOffset.InverseTransformPoint(playerRightLeg.position);
            reg2 = Quaternion.Inverse(playerOffset.rotation) * playerRightLeg.rotation;
        }

        // Create data structure
        var data = new MirrorGuyData()
        {
            handLeftPos = l1,
            handRightPos = l2,
            headPos = l3,
            handLeftRot = r1,
            handRightRot = r2,
            headRot = r3,
            legLeftPos = leg1,
            legRightPos = leg2,
            legLeftRot = reg1,
            legRightRot = reg2,
            hipsPos = hipos,
            hipsRot = hirot,
        };

        return data;
    }

    private static bool AnyNull()
    {
        if (playerLeft == null || playerRight == null || playerHead == null || MirrorGuyPlayerOffset.instance == null)
        {
            return true;
        }
        return false;
    }
}
