using RootMotion.FinalIK;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;

public class YetiIkAnimRecord : MonoBehaviour
{
    [SerializeField]
    private IkAnim _ikAnim;
    public IkAnim ikAnim
    {
        get
        {
            if (_ikAnim == null)
            {
                _ikAnim = GetComponent<IkAnim>();
            }
            return _ikAnim;
        }
    }

    // some accessors for the editor
    public MirrorGuyAnimationShare playAnimation
    {
        get
        {
            return ikAnim.playAnimation;
        }
        set
        {
            ikAnim.playAnimation = value;
        }
    }

    public float curPlaybackFrame
    {
        get
        {
            return ikAnim.playbackFrame;
        }
        set
        {
            ikAnim.playbackFrame = (int)value;
        }
    }

    MirrorGuyTrackedObject playerLeft { get { return MirrorGuyTrackedObject.playerLeft; } }
    MirrorGuyTrackedObject playerRight { get { return MirrorGuyTrackedObject.playerRight; } }
    MirrorGuyTrackedObject playerHead { get { return MirrorGuyTrackedObject.playerHead; } }
    MirrorGuyTrackedObject playerLeftLeg { get { return MirrorGuyTrackedObject.playerLeftLeg; } }
    MirrorGuyTrackedObject playerRightLeg { get { return MirrorGuyTrackedObject.playerRightLeg; } }
    MirrorGuyTrackedObject playerHips { get { return MirrorGuyTrackedObject.playerHips; } }

    [Serializable]
    public class ToggleWeights
    {
        public bool activeLeft;[HideInInspector] public bool oldLeft;
        public bool activeRight;[HideInInspector] public bool oldRight;
        public bool activeHead;[HideInInspector] public bool oldHead;
        public bool activeLeftLeg;[HideInInspector] public bool oldLeftLeg;
        public bool activeRightLeg;[HideInInspector] public bool oldRightLeg;
        public bool activeHips;[HideInInspector] public bool oldHips;

        public void Validate(FullBodyBipedIK ik, FBBIKHeadEffector head = null)
        {
            if (activeHead != oldHead)
            {
                oldHead = activeHead;
                if (head != null)
                {
                    head.positionWeight = head.rotationWeight = activeHead ? 1f : 0f;
                }
                else
                {
                    Debug.Log("[ToggleWeightsValidate] Head reference missing");
                }
            }
            if (activeLeft != oldLeft)
            {
                oldLeft = activeLeft;
                ik.solver.SetEffectorWeights(FullBodyBipedEffector.LeftHand, activeLeft ? 1f : 0, activeLeft ? 1f : 0f);
            }
            if (activeLeftLeg != oldLeftLeg)
            {
                oldLeftLeg = activeLeftLeg;
                ik.solver.SetEffectorWeights(FullBodyBipedEffector.LeftFoot, activeLeftLeg ? 1f : 0, activeLeftLeg ? 1f : 0f);
            }
            if (activeRight != oldRight)
            {
                oldRight = activeRight;
                ik.solver.SetEffectorWeights(FullBodyBipedEffector.RightHand, activeRight ? 1f : 0, activeRight ? 1f : 0f);
            }
            if (activeRightLeg != oldRightLeg)
            {
                oldRightLeg = activeRightLeg;
                ik.solver.SetEffectorWeights(FullBodyBipedEffector.RightFoot, activeRightLeg ? 1f : 0, activeRightLeg ? 1f : 0f);
            }
            if (activeHips != oldHips)
            {
                oldHips = activeHips;
                ik.solver.SetEffectorWeights(FullBodyBipedEffector.Body, activeHips ? 1f : 0, activeHips ? 1f : 0f);
            }
        }
    }

    [Space]
    public ToggleWeights toggleWeights;

    // used mainly for ToggleWeights. useful in editor.
    private FullBodyBipedIK _ik;
    public FullBodyBipedIK ik
    {
        get
        {
            if (_ik == null)
            {
                _ik = GetComponent<FullBodyBipedIK>();
            }
            return _ik;
        }
    }

    public enum AnimationState
    {
        None,
        Live,
        Play,
        PlayPaused,
        Record
    }

    [EnumButtons]
    public AnimationState state;

    /// <summary>
    /// Can be used for statechangin, simpler than checking all states. Used by controller.touchpad.
    /// </summary>
    public bool toggleLivePlay;

    public bool record
    {
        get
        {
            return state == AnimationState.Record;
        }
        set
        {
            if (value)
            {
                state = AnimationState.Record;
            }
            else
            {
                state = AnimationState.Play;
            }
        }
    }

    MirrorGuyAnimationShare recordAnimation;

    /// accessor of <see cref="IkAnim.playbackFPS"/>
    public float playbackFPS
    {
        get
        {
            return ikAnim.playbackFPS;
        }
        set
        {
            ikAnim.playbackFPS = value;
        }
    }

    public GameObject recordGizmo;

    public string sanityCheckFolderPath = @"Assets\YetiFam\ik animations\anims";

    private void OnValidate()
    {
        if (ikAnim == null)
        {
            Debug.Log("No IkAnim detected");
        }

        if (recordGizmo == null)
        {
            var rg = transform.GetChildren().FirstOrDefault(t => t.name.Contains("record gizmo"));
            if (rg == null)
            {
                rg = GameObject.CreatePrimitive(PrimitiveType.Sphere).transform;
                DestroyImmediate(rg.GetComponent<Collider>());
                rg.name = "record gizmo";
                rg.SetParent(transform);
                rg.localScale = Vector3.one * 0.6f;
                rg.localPosition = Vector3.up * 3f;
                rg.GetComponent<Renderer>().receiveShadows = false;
            }
            recordGizmo = rg.gameObject;
        }
    }

    private void Reset()
    {
        OnValidate();
    }

    private void Update()
    {
        if (ik != null)
        {
            toggleWeights.Validate(ik);
        }

        if (RightTouchpadDown())
        {
            toggleLivePlay = true;
        }

        if (toggleLivePlay)
        {
            toggleLivePlay = false;
            // if it's play, make it live, anything else make it play
            state = state == AnimationState.Play ? AnimationState.Live : AnimationState.Play;
        }

        var justPressedRecord = (LeftTouchpadDown());
        if (justPressedRecord)
        {
            if (!record)
            {
                record = true;
                state = AnimationState.Record;
            }
            else
            {
                record = false;
                ikAnim.playAnimation = recordAnimation;
                recordAnimation = null;
                state = AnimationState.Play;

                // save animation too, as prefab, so we don't just press play mode and lose EVERYTHING LIKE JUST FUCKING HAPPENED
                SavePrefabSanityCheck(ikAnim.playAnimation);
            }
        }

        StateUpdate();

        HandleRecordGizmo();
    }

    private bool LeftTouchpadDown()
    {
        if (playerLeft != null)
        {
            if (playerLeft.controller != null)
            {
                return playerLeft.controller.GetPressDown(ControllerWrapper.ButtonMask.Touchpad);
            }
        }
        return false;
    }

    private bool RightTouchpadDown()
    {
        if (playerRight != null)
        {
            if (playerRight.controller != null)
            {
                return playerRight.controller.GetPressDown(ControllerWrapper.ButtonMask.Touchpad);
            }
        }
        return false;
    }

    private void HandleRecordGizmo()
    {
        recordGizmo.SetActive(record);
    }

    private void StateUpdate()
    {
        MirrorGuyData data;

        switch (state)
        {
        case AnimationState.None:
            // do nothing, as if component is deactivated ;)
            break;

        case AnimationState.Live:
            ikAnim.playState = IkAnim.PlayState.None;
            data = MirrorGuyData.GetFromPlayer();
            SetVectors(data);
            break;

        case AnimationState.Play:
            // let the ikAnim script play back the animation with the good in-game settings.
            ikAnim.playState = IkAnim.PlayState.Play;
            break;

        case AnimationState.PlayPaused:
            ikAnim.playState = IkAnim.PlayState.None;
            ikAnim.UpdatePlaybackFrame();
            break;

        case AnimationState.Record:
            // data direct from the player
            data = MirrorGuyData.GetFromPlayer();

            // data set to this character with various modifications
            SetVectors(data);
            if (recordAnimation == null)
            {
                NewRecordAnimation();
            }
            // original player data added as an animation (so it can be reused, when hands and legs are relative positions compared to shoulders/hips)
            recordAnimation.data.Add(data);
            break;
        }
    }

    private void NewRecordAnimation()
    {
        var newAnim = new GameObject("Anim " + Time.time.ToString("F0"), typeof(MirrorGuyAnimationShare)).GetComponent<MirrorGuyAnimationShare>();

        bool parentIsNewObj = true;
        if (ikAnim.playAnimation != null)
        {
            // in editor, check if the prefab type of the play animation is an instance (in the scene) or not (in the project/on disk)
#if UNITY_EDITOR
            {
                var prefabType = UnityEditor.PrefabUtility.GetPrefabType(ikAnim.playAnimation);
                switch (prefabType)
                {
                case UnityEditor.PrefabType.None:
                case UnityEditor.PrefabType.PrefabInstance:
                case UnityEditor.PrefabType.ModelPrefabInstance:
                case UnityEditor.PrefabType.MissingPrefabInstance:
                case UnityEditor.PrefabType.DisconnectedPrefabInstance:
                case UnityEditor.PrefabType.DisconnectedModelPrefabInstance:
                    // parent should be playAnimation parent
                    parentIsNewObj = false;
                    break;
                case UnityEditor.PrefabType.ModelPrefab:
                case UnityEditor.PrefabType.Prefab:
                    // parent should be new obj in scene.
                    parentIsNewObj = true;
                    break;
                }
            }
#endif
        }

        if (parentIsNewObj)
        {
            var pare = new GameObject("[AnimRec] " + transform.name);
            newAnim.transform.SetParent(pare.transform);
        }
        else
        {
            newAnim.transform.SetParent(ikAnim.playAnimation.transform.parent);
        }

        newAnim.data = new List<MirrorGuyData>(100);
        recordAnimation = newAnim;

    }

    /// <summary>
    /// WTF IS WRONG HERE??? DOESN'T WORK. PLS TEST
    /// quick n dirty function for saving prefabs at runtime whenever animation is recorded (sanity check location, can usually be deleted along with folder to preserve sanity).
    /// </summary>
    /// <param name="playAnimation"></param>
    private void SavePrefabSanityCheck(MirrorGuyAnimationShare playAnimation)
    {
#if UNITY_EDITOR
        // dir
        if (!System.IO.Directory.Exists(sanityCheckFolderPath))
        {
            System.IO.Directory.CreateDirectory(sanityCheckFolderPath);
        }
        var ext = ".prefab";
        int counter = 0;
        var path = System.IO.Path.Combine(sanityCheckFolderPath, playAnimation.name + (counter == 0 ? "" : counter.ToString()) + ext);
        while (System.IO.File.Exists(path))
        {
            counter++;
            path = System.IO.Path.Combine(sanityCheckFolderPath, playAnimation.name + (counter == 0 ? "" : counter.ToString()) + ext);
            if (counter >= 1000)
            {
                Debug.LogError("TOO MANY COUNTS");
                return;
            }
        }
        var finalName = playAnimation.name + (counter == 0 ? "" : counter.ToString()) + ext;
        path = path.Replace('\\', '/');
        // create prefab
        var prefabCreated = UnityEditor.PrefabUtility.CreatePrefab(path, playAnimation.gameObject);

#endif
    }

    private void SetVectors(MirrorGuyData data)
    {
        ikAnim.SetVectors(data);
    }

}