﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

/// <summary>
/// Tests the <see cref="CharacterBaseController"/>
/// </summary>
public class CharacterBaseControllerTP : MonoBehaviour
{
    [HelpBox("Test tool for CharacterBaseController. WASD move, click on screen to walk there.", HelpBoxType.Info)]
    public bool wasdControl = true;
    public bool goToClick = false;

    private CharacterBaseController _controller;
    public CharacterBaseController controller
    {
        get
        {
            if (_controller == null)
            {
                _controller = GetComponent<CharacterBaseController>();
            }
            return _controller;
        }
    }

    private Vector3 dir;
    private bool moving = false;

    public float minStepDistance = 3f;

    private Camera _mainCamera;

    public float speed = 10f;

    public Camera mainCamera
    {
        get
        {
            if (_mainCamera == null)
            {
                _mainCamera = Camera.main;
            }
            return _mainCamera;
        }
    }


    private void Update()
    {
        if (wasdControl)
        {
            if (MoveKey())
            {
                dir = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
                dir = mainCamera.transform.TransformDirection(dir);
                controller.SetMoveTarget(controller.transform.position + dir.normalized * this.speed);
                moving = true;
            }
            else if (moving)
            {
                moving = false;
                controller.SetMoveTarget(controller.transform.position + dir.normalized * minStepDistance);

            }

        }

        if (goToClick)
        {
            if (Input.GetMouseButtonDown(0))
            {
                var stw = mainCamera.ScreenPointToRay(Input.mousePosition);
                RaycastHit info;
                if (Physics.Raycast(stw, out info))
                {
                    var pos = info.point;
                    controller.SetMoveTarget(pos);
                }
            }
            if (Input.GetMouseButtonDown(1))
            {
                controller.StopMoving();
            }
        }
    }

    private bool MoveKey()
    {
        return Input.GetKey(KeyCode.W) || Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.S) || Input.GetKey(KeyCode.D);
    }
}