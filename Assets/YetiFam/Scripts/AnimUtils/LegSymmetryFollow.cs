using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LegSymmetryFollow : MonoBehaviour
{
    public Transform target;

    public Vector3 globalPosOffset;

    public float globalHeight = -1;

    public bool followRotation = false;
    public bool onlyY = false;

    public Vector3 localRotOffset;

    void Update()
    {
        transform.position = target.position + globalPosOffset;
        if (globalHeight != -1)
        {
            transform.position = new Vector3(transform.position.x, globalHeight, transform.position.z);
        }
        if (followRotation)
        {
            transform.rotation = Quaternion.Euler(localRotOffset) * target.rotation;
            if (onlyY)
            {
                transform.eulerAngles = new Vector3(0f, transform.eulerAngles.y, 0f);
            }
        }
    }

}