using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LegMirror : MonoBehaviour, IOriginShifter
{

    public Transform otherLeg;
    public Transform symmetryPivot;

    [Header("True = symmetry to point. False = mirror-like")]
    public bool pivotOrAxis = true;

    public bool controlWithTrigger = true;
    private Vector3 targetPos;

    [Range(0,1f)]
    public float smoothMovement = 0.1f;

    private void Update()
    {
        if (controlWithTrigger)
        {
            var c = MirrorGuyTrackedObject.playerRight.controller;
            if (c.GetPressDown(ControllerWrapper.ButtonMask.Trigger) || c.GetPressUp(ControllerWrapper.ButtonMask.Trigger))
            {
                pivotOrAxis = !pivotOrAxis;
            }
        }

        if (pivotOrAxis)
        {
            var otherOffset = otherLeg.position - symmetryPivot.position;
            targetPos = symmetryPivot.position - otherOffset;
        }
        else
        {
            //var otherOffset = otherLeg.position - symmetryPivot.position;
            var localVec = symmetryPivot.InverseTransformPoint(otherLeg.position);
            localVec.Scale(new Vector3(-1, 1f, 1f));
            targetPos = symmetryPivot.TransformPoint(localVec);
            //Vector3.Reflect(otherOffset, symmetryPivot.right);
        }

        transform.position = Vector3.Lerp(transform.position, targetPos, smoothMovement);
    }

    public void OnWorldMove(Vector3 originShiftDelta)
    {
        targetPos -= originShiftDelta;
    }
}