﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEditor;
using System;

[CanEditMultipleObjects]
[CustomEditor(typeof(IkAnim))]
public class IkAnimEditor : Editor
{
    static bool animFoldout = false;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        serializedObject.Update();

        if (targets.Length == 1)
        {
            SingleEditor();
        }
        else
        {
            MultiEditor();
        }

        serializedObject.ApplyModifiedProperties();
    }


    private void SingleEditor()
    {
        var t = target as IkAnim;
        if (t == null)
        {
            return;
        }

        // buttons for controlling animator and stuff
        animFoldout = EditorGUILayout.Foldout(animFoldout, "IK Animator Guetto Editor");
        if (animFoldout)
        {
            // enabled box
            t.ikAnimator.enabled = EditorGUILayout.ToggleLeft("IK Animator enabled", t.ikAnimator.enabled);

            // regular animator editor
            EditorGUI.indentLevel++;

            if (t.ikAnimator != null)
            {
                var animEditor = Editor.CreateEditor(t.ikAnimator);
                animEditor.OnInspectorGUI();
            }
            else
            {
                EditorGUILayout.LabelField("IK Animator null");
            }

            EditorGUI.indentLevel--;
        }

    }

    private void MultiEditor()
    {
        var ts = targets.Where(t => t is IkAnim).Select(t => t as IkAnim).ToArray();

        animFoldout = EditorGUILayout.Foldout(animFoldout, "IK Animators Guetto Editor");
        if (animFoldout)
        {
            // MULTI ENABLED BOX
            var allReallyEnabled = ts.All(i => i.ikAnimator.enabled);
            var someEnabled = ts.Any(i => i.ikAnimator.enabled);
            var noneEnabled = ts.All(i => !i.ikAnimator.enabled);
            if (someEnabled && !noneEnabled && !allReallyEnabled)
            {
                EditorGUI.showMixedValue = true;
            }
            EditorGUI.BeginChangeCheck();
            bool newIkAnimatorEnabled = EditorGUILayout.ToggleLeft("IK Animator Enabled", someEnabled);
            if (EditorGUI.EndChangeCheck())
            {
                for (int i = 0; i < ts.Length; i++)
                {
                    ts[i].ikAnimator.enabled = newIkAnimatorEnabled;
                }
            }
            EditorGUI.showMixedValue = false;


            // regular multi-edit animator box
            EditorGUI.indentLevel++;

            if (ts.All(t => t.ikAnimator != null))
            {
                var ikanimarray = ts.Select(t => t.ikAnimator).ToArray();

                var animEditor = Editor.CreateEditor(ikanimarray);
                animEditor.OnInspectorGUI();
            }
            else
            {
                EditorGUILayout.LabelField("Some IK Animators null");
            }

            EditorGUI.indentLevel--;
        }

    }

}
