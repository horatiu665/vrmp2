﻿namespace CleanMocapRig
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using VRCore;

    public class CleanMocapRigCalibration : MonoBehaviour
    {
        [Header("Consts")]
        public float deltaDecayLegs = 0.02f, deltaDecayHips = 0.01f;
        public float deltaThresholdLegs = 0.4f, deltaThresholdHips = 0.15f;

        public float shakeSumThreshold = 1f;

        public const float minAlpha = 0.05f;

        public int ignoreFrames = 3;

        [Header("privates")]
        private int changeStateFrame;
        public float shakeCurMax; // just preview


        [Header("States")]
        // interface for calibrating which controller/tracker is which
        [SerializeField, EnumButtons]
        private States state;

        private States prevState;

        public enum States
        {
            Inactive = 0,
            ShakeLeftLeg,
            ShakeRightLeg,
            ShakeHips,

        };

        public void SetState(States state)
        {
            this.state = state;
        }


        public List<float> trackedObjectPositionDeltas = new List<float>();
        public List<Vector3> trackedObjectOldPos = new List<Vector3>();

        // gizmos for all tracked objects
        bool gizmosShowing = false;

        public GameObject previewSelectionPrefab;
        public List<PreviewRenderModel> previewRenderModels = new List<PreviewRenderModel>();

        [Serializable]
        public class PreviewRenderModel
        {
            public SteamVR_TrackedObject trackedObj;
            public SteamVR_RenderModel renderModel;
            public Renderer selectionRenderer;
            public bool prevValidState;

            internal void SetSelectionPreview(float v)
            {
                selectionRenderer.material.SetFloat("_AlphaCutoff", Mathf.Clamp(v, minAlpha, 1f));
            }

            internal Vector3 GetPos()
            {
                SteamVR_Controller.Update();
                return trackedObj.transform.localPosition;
            }
        }

        private void OnEnable()
        {
            InitPreviewModels();
            ResetPositionDeltas();

            HideGizmos();
        }

        private void Update()
        {
            // first enable/disable gizmo shit.
            Update_ShowGizmoForAllTrackedControllers();

            // change of states means resetting deltas
            if (state != prevState)
            {
                prevState = state;
                changeStateFrame = Time.frameCount;

                // states changed. reset shit. every time the state changes.
                UpdatePositionDeltas();
                ResetPositionDeltas();
                return;
            }

            if (Time.frameCount - changeStateFrame < ignoreFrames)
            {
                // one frame has passed since the state change. 
                // update positions, this makes sure the devices don't jump from 0,0,0 to 10000 million position, so we get an instant assignment of the feet or whatever.
                UpdatePositionDeltas();
                ResetPositionDeltas();
                return;
            }

            // if not inactive, measure deltas and all.
            if (state != States.Inactive)
            {
                UpdatePositionDeltas();

                CheckAssignToMostMoved();
            }


        }

        private void Update_ShowGizmoForAllTrackedControllers()
        {
            if (state == States.Inactive && gizmosShowing)
            {
                HideGizmos();
            }
            else if (state != States.Inactive && !gizmosShowing)
            {
                InitPreviewModels();
                ShowGizmos();
            }

            // update preview also if a valid state changed for any of the tracked objects. only when showing gizmos.
            if (gizmosShowing)
            {
                for (int i = 0; i < previewRenderModels.Count; i++)
                {
                    var p = previewRenderModels[i];
                    if (p.trackedObj.isValid != p.prevValidState)
                    {
                        p.prevValidState = p.trackedObj.isValid;
                        if (p.trackedObj.isValid)
                        {
                            p.renderModel.SetDeviceIndex(i);
                        }
                    }
                }
            }
        }

        private void ShowGizmos()
        {
            // show gizmos
            gizmosShowing = true;

            for (int i = 0; i < previewRenderModels.Count; i++)
            {
                var rm = previewRenderModels[i];
                rm.trackedObj.gameObject.SetActive(true);
                rm.selectionRenderer.gameObject.SetActive(true);
                if (rm.trackedObj.isValid)
                {
                    rm.renderModel.SetDeviceIndex(i);
                }
            }
        }

        private void HideGizmos()
        {
            // hide gizmos
            gizmosShowing = false;

            // simply set gameobjects inactive to hide them
            for (int i = 0; i < previewRenderModels.Count; i++)
            {
                var rm = previewRenderModels[i];
                rm.trackedObj.gameObject.SetActive(false);
            }
        }

        [DebugButton]
        private void InitPreviewModels()
        {
            // generate SteamVR_TrackedObjects and SteamVR_RenderModels for each tracked object and set their IDs, this way they will show the correct meshes
            // disable/enable them every time we change state so they can reinitialize in case they were connected after startup
            if (previewRenderModels.Count != SteamVR.connected.Length)
            {
                for (int i = 0; i < SteamVR.connected.Length; i++)
                {
                    GeneratePreviewRenderModel(i);
                }
            }
        }

        /// <summary>
        /// Generates preview render models - a tracked object for each tracked object, along with render model and a selection preview gizmo that turns towards the head and fills up when calibrating that limb.
        /// </summary>
        /// <param name="i"></param>
        private void GeneratePreviewRenderModel(int i)
        {
            var tog = new GameObject("[gen] TrackedObject " + i);
            tog.transform.SetParent(transform);
            var to = tog.AddComponent<SteamVR_TrackedObject>();
            to.SetDeviceIndex(i);
            var rm = tog.AddComponent<SteamVR_RenderModel>();
            rm.updateDynamically = false;
            rm.createComponents = false;
            // special settings for head: do not show in VR
            if (to.index == SteamVR_TrackedObject.EIndex.Hmd)
            {
                tog.layer = LayerMask.NameToLayer("NotVisibleInVR");
            }
            var sel = Instantiate(previewSelectionPrefab, tog.transform.position, tog.transform.rotation, tog.transform).GetComponent<Renderer>();
            previewRenderModels.Add(new PreviewRenderModel()
            {
                trackedObj = to,
                renderModel = rm,
                selectionRenderer = sel,
            });
        }

        /// <summary>
        /// Keeps track of the movements of tracked objects, to be able to determine the most wobbly one and assign it when choosing a new calibrated target. 
        /// </summary>
        private void UpdatePositionDeltas()
        {
            // if state is legs, take the legs params
            float deltaThreshold = deltaThresholdLegs;
            float deltaDecay = deltaDecayLegs;
            if (state == States.ShakeHips)
            {
                deltaThreshold = deltaThresholdHips;
                deltaDecay = deltaDecayHips;
            }

            // decay position deltas. maybe there is smarter way of doing it
            for (int i = 0; i < trackedObjectPositionDeltas.Count; i++)
            {
                trackedObjectPositionDeltas[i] = Mathf.Clamp01(trackedObjectPositionDeltas[i] - deltaDecay);
            }

            // calculate total move positions over time for each device (not the assigned ones, but directly from steamvr)
            for (int i = 0; i < trackedObjectPositionDeltas.Count; i++)
            {
                // only valid devices 
                var p = (previewRenderModels[i]);
                if (p.trackedObj.isValid)
                {
                    // get tracked object position and find its delta
                    var pos = p.GetPos();
                    var delta = pos - trackedObjectOldPos[i];

                    // remap magnitude to a quadratic curve. divide by positionDeltaThreshold before squaring so the curve is steeper above the threshold instead of above 1.
                    var deltaMag = delta.magnitude / deltaThreshold;
                    deltaMag *= deltaMag;

                    trackedObjectPositionDeltas[i] += deltaMag;

                    trackedObjectOldPos[i] = pos;
                }
            }

            // set graphics for selection renderers
            for (int i = 0; i < previewRenderModels.Count; i++)
            {
                var p = previewRenderModels[i];
                p.SetSelectionPreview(trackedObjectPositionDeltas[i]);
            }
        }

        public void CheckAssignToMostMoved()
        {
            MirrorGuyTrackedObject target = null;
            switch (state)
            {
            case States.Inactive:
                break;
            case States.ShakeLeftLeg:
                target = MirrorGuyTrackedObject.playerLeftLeg;
                break;
            case States.ShakeRightLeg:
                target = MirrorGuyTrackedObject.playerRightLeg;
                break;
            case States.ShakeHips:
                target = MirrorGuyTrackedObject.playerHips;
                break;
            }

            var maxValue = trackedObjectPositionDeltas.Max();
            shakeCurMax = maxValue;
            if (maxValue > shakeSumThreshold)
            {
                var maxIndex = trackedObjectPositionDeltas.MaxIndex();

                if (target != null)
                {
                    target.deviceId = maxIndex;

                    // reset values
                    ResetPositionDeltas();
                    StopCalibration();
                }
            }

        }

        /// <summary>
        /// assign controls automatically based on body pose. leftmost/rightmost controller = easy because they have controllerwrappers, and leftmost leg, rightmost leg maybe easy too out of remaining devices (hips last)
        /// </summary>
        public void AutoAssignControllersBasedOnPose()
        {
            // get mirror guy devices
            var devices = MirrorGuyTrackedObject.objects;

            // set left leg as first ordered device
            var leftId = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.FarthestRight, Valve.VR.ETrackedDeviceClass.GenericTracker, (int)SteamVR_TrackedObject.EIndex.Hmd);

            // set right leg as last ordered device
            var rightId = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.FarthestRight, Valve.VR.ETrackedDeviceClass.GenericTracker, (int)SteamVR_TrackedObject.EIndex.Hmd);

            //MirrorGuyTrackedObject.playerHips.deviceId = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.First, Valve.VR.ETrackedDeviceClass.GenericTracker, MirrorGuyTrackedObject.playerLeftLeg.deviceId);

            var trackers = new List<int>();
            for (int i = 0; i < SteamVR.connected.Length; i++)
            {
                if (Valve.VR.OpenVR.System.GetTrackedDeviceClass((uint)i) == Valve.VR.ETrackedDeviceClass.GenericTracker)
                    trackers.Add(i);
            }

            var hipsId = trackers.FirstOrDefault(i => i != leftId && i != rightId);

            // all 3 trackers are different ids, can be assigned
            if (hipsId != leftId && leftId != rightId && rightId != hipsId)
            {
                MirrorGuyTrackedObject.playerHips.deviceId = hipsId;
                MirrorGuyTrackedObject.playerLeftLeg.deviceId = leftId;
                MirrorGuyTrackedObject.playerRightLeg.deviceId = rightId;
            }
            else
            {
                // ids are not different...
                Debug.Log("AutoCalibration failed!");
            }

        }

        public void ResetPositionDeltas()
        {
            // init lists
            if (trackedObjectPositionDeltas.Count != previewRenderModels.Count)
            {
                trackedObjectPositionDeltas = previewRenderModels.Select(b => 0f).ToList();
                trackedObjectOldPos = new Vector3[trackedObjectPositionDeltas.Count].ToList();
            }

            // set position deltas to zero
            for (int i = 0; i < trackedObjectPositionDeltas.Count; i++)
            {
                trackedObjectPositionDeltas[i] = 0;
            }
            for (int i = 0; i < trackedObjectOldPos.Count; i++)
            {
                trackedObjectOldPos[i] = previewRenderModels[i].GetPos();
            }
        }

        public void StopCalibration()
        {
            state = States.Inactive;
            ResetPositionDeltas();
        }

    }
}
