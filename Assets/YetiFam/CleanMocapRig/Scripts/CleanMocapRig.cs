﻿namespace CleanMocapRig
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    /// <summary>
    /// Workflow:
    /// Place this rig in the scene and (auto)assign the player camera rig. Ref head, hands [, hips] [, legs] and preview the adjustable mocap setup
    /// Mocap rig on the characters themselves is a separate object
    /// </summary>
    public class CleanMocapRig : MonoBehaviour
    {
        private CleanMocapRigCalibration _calibration;
        public CleanMocapRigCalibration calibration
        {
            get
            {
                if (_calibration == null)
                {
                    _calibration = GetComponent<CleanMocapRigCalibration>();
                }
                return _calibration;
            }
        }

        public bool fakeParentToPlayer = false;

        private void OnEnable()
        {
            if (fakeParentToPlayer)
            {
                var fp = gameObject.AddComponent<FakeParenting>();
                var cm = FindObjectOfType<SteamVR_ControllerManager>();
                if (cm != null)
                {
                    fp.target = cm.transform;
                    fp.position = true;
                    fp.rotation = true;
                    fp.keepInitWorldOffset = false;
                }

            }
        }

    }
}