﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class LookAtVrHead : MonoBehaviour
{
    Transform _playerHead;
    public Transform playerHead
    {
        get
        {
            if (_playerHead == null)
            {
                _playerHead = Camera.main.transform;
            }
            return _playerHead;
        }
    }

    void Update()
    {
        if (playerHead != null)
        {
            transform.LookAt(playerHead);
        }
    }

}