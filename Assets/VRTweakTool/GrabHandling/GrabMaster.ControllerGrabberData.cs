﻿namespace VRTweakTool
{
    using System;
    using UnityEngine;

    public partial class GrabMaster
    {
        // non serializable. generated in list upon access. [Serializable] just for the debug
        [Serializable]
        public class ControllerGrabberData
        {
            public GrabMaster master;
            public ControllerWrapper controllerWrapper;

            public bool GetPressDownButton()
            {
                return controllerWrapper.GetPressDown(ControllerWrapper.ButtonMask.Trigger);
            }

            public bool GetPressUpButton()
            {
                return controllerWrapper.GetPressUp(ControllerWrapper.ButtonMask.Trigger);
            }

            public Vector3 grabPoint
            {
                get
                {
                    if (grabGizmo != null)
                    {
                        return grabGizmo.position;
                    }
                    else
                    {
                        // transform point makes (example:) offset of (0,0,1) to be controller.pos + controller.forward.
                        return controllerWrapper.transform.TransformPoint(master.grabGizmoOffset);
                    }
                }
            }

            private Transform _grabGizmo;
            public Transform grabGizmo
            {
                get
                {
                    if (_grabGizmo == null && master.generateGrabGizmo)
                    {
                        var s = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        Destroy(s.GetComponent<Collider>());
                        _grabGizmo = s.transform;
                        _grabGizmo.SetParent(controllerWrapper.transform);
                        _grabGizmo.localPosition = master.grabGizmoOffset;
                        _grabGizmo.localRotation = Quaternion.identity;
                        _grabGizmo.localScale = Vector3.one * 0.01f; // 1cm diameter.
                    }
                    return _grabGizmo;
                }
            }

            /// <summary>
            /// a generated object that is "grabbed" when trying to punch stuff like buttons
            /// </summary>
            private DummyGrabbable _dummy;
            public DummyGrabbable dummy
            {
                get
                {
                    if (_dummy == null)
                    {
                        var dummyRes = Resources.Load<GameObject>("VRT_DummyGrabPrefab");
                        if (dummyRes != null)
                        {
                            _dummy = dummyRes.GetComponent<DummyGrabbable>();
                        }
                        // instantiate, etc...???

                    }
                    if (_dummy == null)
                    {
                        var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
                        go.layer = master.gameObject.layer;
                        go.transform.SetParent(controllerWrapper.transform);
                        go.transform.localScale = Vector3.one * 0.1f;
                        go.transform.position = grabPoint;
                        go.name = "[GEN] DUMMY for VRTweakTool";

                        //Destroy(go.GetComponent<Renderer>());
                        //Destroy(go.GetComponent<MeshFilter>());

                        var drb = go.AddComponent<Rigidbody>();
                        drb.isKinematic = false;
                        drb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;

                        var gb = go.AddComponent<DummyGrabbable>();
                        _dummy = gb;
                    }
                    return _dummy;
                }
            }

            // controlled by GrabDummy() and UngrabDummy()
            public bool isGrabbingDummy;

            public bool isGrabbing { get; private set; }
            private IHandleGrabbing _curGrabbed;
            public IHandleGrabbing curGrabbed
            {
                get
                {
                    return _curGrabbed;
                }
            }

            private IHandleGrabbing _prevGrabbed;
            public IHandleGrabbing prevGrabbed
            {
                get
                {
                    if (isGrabbing)
                    {
                        return _prevGrabbed;
                    }
                    else
                    {
                        return _curGrabbed;
                    }
                }
            }

            public bool isHighlighting { get; private set; }
            private IHandleGrabbing _curHighlighted;
            private IHandleGrabbing _prevHighlighted;
            public IHandleGrabbing curHighlighted
            {
                get
                {
                    if (isHighlighting)
                    {
                        return _curHighlighted;
                    }
                    else
                    {
                        return null;
                    }
                }
            }
            public IHandleGrabbing prevHighlighted
            {
                get
                {
                    if (isHighlighting)
                    {
                        return _prevHighlighted;
                    }
                    else
                    {
                        return _curHighlighted;
                    }
                }
            }

            public void Highlight(IHandleGrabbing obj)
            {
                _prevHighlighted = _curHighlighted;
                _curHighlighted = obj;
                isHighlighting = true;

                obj.OnHighlight(controllerWrapper);
            }

            public void Unhighlight()
            {
                if (isHighlighting)
                {
                    isHighlighting = false;

                    if (_curHighlighted != null)
                    {
                        _curHighlighted.OnUnhighlight(controllerWrapper);
                    }
                }

                // no null so we can get prev highlighted info
                //_curHighlighted = null;

            }

            public void Grab(IHandleGrabbing closestGrabber)
            {
                this._prevGrabbed = this._curGrabbed;
                this._curGrabbed = closestGrabber;
                isGrabbing = true;

                if (isHighlighting)
                {
                    Unhighlight();
                }

                closestGrabber.OnGrab(controllerWrapper);
            }

            public void Ungrab()
            {
                if (isGrabbing)
                {
                    this.isGrabbing = false;

                    _curGrabbed.OnUngrab(controllerWrapper);
                    // NO NULL for keeping prev grab info
                    //curGrabbed = null;
                }
            }
            
            internal void EnableDummy()
            {
                // dummy has an alternative grab method, so it can work simultaneous to other grabbed items. it is all up to the dummy
                dummy.transform.position = grabPoint;
                dummy.OnGrab(controllerWrapper);
                isGrabbingDummy = true;
            }

            internal void DisableDummy()
            {
                // alternative ungrab
                dummy.OnUngrab(controllerWrapper);
                isGrabbingDummy = false;
            }
        }

    }
}