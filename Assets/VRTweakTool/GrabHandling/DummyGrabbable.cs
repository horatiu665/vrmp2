﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    /// <summary>
    /// Replicates <see cref="GrabbableBase"/> but uses alternative grab joints to remain compatible with regular grabbables
    /// </summary>
    public class DummyGrabbable : MonoBehaviour // GrabbableBase
    {
        private Rigidbody _r;
        public Rigidbody r
        {
            get
            {
                if (_r == null)
                {
                    _r = GetComponent<Rigidbody>();
                }
                return _r;
            }
        }

        Dictionary<ControllerWrapper, Transform> dummyTransformAltGrabDict = new Dictionary<ControllerWrapper, Transform>();

        public void OnGrab(ControllerWrapper controllerWrapper)
        {
            gameObject.SetActive(true);

            if (!dummyTransformAltGrabDict.ContainsKey(controllerWrapper))
            {
                var t = new GameObject("[gen] dummy joint for punchable VRTweakTools").transform;
                t.SetParent(controllerWrapper.transform);
                t.localPosition = Vector3.zero;
                t.localRotation = Quaternion.identity;
                t.localScale = Vector3.one;
                dummyTransformAltGrabDict[controllerWrapper] = t;
            }

            JointGrabUtils.JointGrab(dummyTransformAltGrabDict[controllerWrapper], r);
        }

        public void OnUngrab(ControllerWrapper controllerWrapper)
        {
            if (dummyTransformAltGrabDict.ContainsKey(controllerWrapper))
            {
                JointGrabUtils.JointUngrab(dummyTransformAltGrabDict[controllerWrapper], r);

                gameObject.SetActive(false);
            }
        }
    }
}
