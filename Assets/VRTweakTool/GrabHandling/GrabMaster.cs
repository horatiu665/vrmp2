﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;

    public partial class GrabMaster : MonoBehaviour
    {
        private List<ControllerGrabberData> _controllers;
        public List<ControllerGrabberData> controllers
        {
            get
            {
                if (_controllers == null || _controllers.Count == 0)
                {
                    AutoControllerWrapperComponents();

                    _controllers = ControllerWrapper.allControllers.Select(cw => new ControllerGrabberData()
                    {
                        master = this,
                        controllerWrapper = cw,
                    }).ToList();

                    for (int i = 0; i < _controllers.Count; i++)
                    {
                        if (controllers[i].grabGizmo != null)
                        {
                            // just checking (creates gizmo if we use getter)
                        }
                    }
                }
                return _controllers;
            }
        }

        private static void AutoControllerWrapperComponents()
        {
            var scm = FindObjectOfType<SteamVR_ControllerManager>();
            if (scm != null)
            {
                if (scm.left != null)
                {
                    var cw = scm.left.GetComponent<ControllerWrapper>();
                    if (cw == null)
                    {
                        scm.left.AddComponent<ControllerWrapper>();
                    }
                }
                if (scm.right != null)
                {
                    var cw = scm.right.GetComponent<ControllerWrapper>();
                    if (cw == null)
                    {
                        scm.right.AddComponent<ControllerWrapper>();
                    }
                }
            }
        }

        [NonSerialized]
        private List<IHandleGrabbing> _grabbables;
        public List<IHandleGrabbing> grabbables
        {
            get
            {
                if (_grabbables == null)
                {
                    _grabbables = GetComponentsInChildren<IHandleGrabbing>(true).ToList();
                }
                return _grabbables;
            }
        }

        private List<IHandlePunching> _punchables;
        public List<IHandlePunching> punchables
        {
            get
            {
                if (_punchables == null)
                {
                    _punchables = GetComponentsInChildren<IHandlePunching>(true).ToList();
                }
                return _punchables;
            }
        }

        public bool punchGizmoOnHandNearPunchable = true;
        public bool generateGrabGizmo = true;
        public Vector3 grabGizmoOffset = new Vector3(0, -0.04f, 0.1f);

        // UPDATE RARE
        private float updateRareLast = 0f;
        public float updateRareFps = 10f;
        public float updateRareDeltaTime
        {
            get
            {
                return Time.time - updateRareLast;
            }
        }

        public void SetVisible(bool active)
        {
            foreach (var t in transform.GetChildren())
            {
                t.gameObject.SetActive(active);
            }
        }

        /// <summary>
        /// Allows adding grabbables at runtime, on their OnEnable
        /// </summary>
        /// <param name="grabbable"></param>
        public void AddGrabbable(GrabbableBase grabbable)
        {
            if (!grabbables.Contains(grabbable))
            {
                grabbables.Add(grabbable);
            }
        }

        /// <summary>
        /// Grabbable remove themselves on OnDisable or OnDestroy
        /// </summary>
        /// <param name="grabbable"></param>
        public void RemoveGrabbable(GrabbableBase grabbable)
        {
            grabbables.Remove(grabbable);
        }


        public void AddPunchable(IHandlePunching punchable)
        {
            if (!punchables.Contains(punchable))
            {
                punchables.Add(punchable);
            }
        }

        public void RemovePunchable(IHandlePunching punchable)
        {
            punchables.Remove(punchable);
        }


        // handle grabbing of controls via VRTweakTool.ControllerWrapper
        private void Update()
        {
            if (Time.time > updateRareLast + 1f / updateRareFps)
            {
                UpdateRare();
                updateRareLast = Time.time;
            }

            // when a controller presses trigger, try to do a grab
            for (int i = 0; i < controllers.Count; i++)
            {
                var c = controllers[i];
                if (c.GetPressDownButton())
                {
                    ControllerPressDown(c);
                }
                else if (c.GetPressUpButton())
                {
                    ControllerPressUp(c);
                }
            }
        }

        // highlights here
        private void UpdateRare()
        {
            for (int i = 0; i < controllers.Count; i++)
            {
                var c = controllers[i];

                // only do highlight stuff when not grabbing.
                if (!c.isGrabbing)
                {
                    // if punchable in range, create dummy puncher so we can punch. but still if we wanna grab highlighted shit, we should be able to do that.
                    if (IsPunchableInRange(c.grabPoint))
                    {
                        c.EnableDummy();
                    }
                    else
                    {
                        c.DisableDummy();
                    }

                    // find closest grabbable. this is expensive, which is why we only do it in rare update.
                    IHandleGrabbing closestGrabber = FindClosestGrabbable(c.grabPoint);

                    // if found a nearby grabber
                    if (closestGrabber != null)
                    {
                        // if we are not already highlighting the same obj, change.
                        if (!c.isHighlighting || (c.isHighlighting && c.curHighlighted != closestGrabber))
                        {
                            c.Unhighlight();
                            c.Highlight(closestGrabber);
                        }
                    }
                    else // didn't find nearby grabber
                    {
                        if (c.isHighlighting)
                        {
                            c.Unhighlight();
                        }
                    }

                }
                else
                {
                    c.DisableDummy();
                }
            }

        }

        private void ControllerPressDown(ControllerGrabberData c)
        {
            // only grab if not already grabbing something.
            if (!c.isGrabbing)
            {
                IHandleGrabbing closestGrabber = FindClosestGrabbable(c.grabPoint);

                if (closestGrabber != null)
                {
                    c.Grab(closestGrabber);
                }
                //else
                //{
                //    // close object not found. make dummy grabbed object which can be used to punch stuff!
                //    c.DummyGrab();
                //}
            }
        }

        private void ControllerPressUp(ControllerGrabberData c)
        {
            if (c.isGrabbing)
            {
                c.Ungrab();
            }
        }

        private IHandleGrabbing FindClosestGrabbable(Vector3 grabPoint)
        {
            var minSqrDist = float.MaxValue;
            Vector3 realClosestPoint = Vector3.zero;
            IHandleGrabbing closestGrabber = null;
            // check if someone was grabbed
            foreach (var g in grabbables)
            {
                var closestPoint = g.GetClosestPointToColliderSurface(grabPoint);
                var newSqrDist = (closestPoint - grabPoint).sqrMagnitude;
                if (g.maxGrabDistance > 0)
                {
                    // if it is close enough for the grab threshold
                    if (newSqrDist < g.maxGrabDistance * g.maxGrabDistance)
                    {
                        if (newSqrDist < minSqrDist)
                        {
                            minSqrDist = newSqrDist;
                            realClosestPoint = closestPoint;
                            closestGrabber = g;
                        }
                    }
                }
            }
            Debug.DrawLine(grabPoint, realClosestPoint, Color.red, 0.1f);

            return closestGrabber;
        }

        /// <summary>
        /// Returns true when some <see cref="IHandlePunching"/> is nearby and it can be punched...
        /// </summary>
        private bool IsPunchableInRange(Vector3 grabPoint)
        {
            foreach (var p in punchables)
            {
                if (p.IsInRange(grabPoint))
                {
                    return true;
                }
            }
            return false;
        }

        public ControllerGrabberData GetControllerGrabberData(ControllerWrapper controller)
        {
            for (int i = 0; i < controllers.Count; i++)
            {
                if (controllers[i].controllerWrapper == controller)
                {
                    return controllers[i];
                }
            }
            return null;
        }

    }
}