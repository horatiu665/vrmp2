﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    /// <summary>
    /// Can be grabbed by controllers, or by fake-grabbing in editor using the isGrabbed flag.
    /// If rigidbody, grabs with wobbly joint.
    /// If not, grabs with transform.position/fake parenting.
    /// </summary>
    public interface IHandleGrabbing
    {
        // some monobehaviour tools
        GameObject gameObject { get; }
        Transform transform { get; }
        Rigidbody rigidbody { get; }

        bool isHighlighted { get; }
        void OnHighlight(ControllerWrapper controller);
        void OnUnhighlight(ControllerWrapper controller);

        float maxGrabDistance { get; }
        bool isGrabbed { get; }
        void OnGrab(ControllerWrapper controller);
        void OnUngrab(ControllerWrapper controller);

        Vector3 GetClosestPointToColliderSurface(Vector3 pos);

    }
}
