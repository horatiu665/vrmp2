﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    
    public interface IHandlePunching
    {
        // some monobehaviour tools
        GameObject gameObject { get; }
        Transform transform { get; }

        // Points forward so we know in which direction the puncher should be active. maybe also sets distance...?
        Transform punchForwardDirection { get; }
        
        bool isHighlighted { get; }
        void OnHighlight(ControllerWrapper controller);
        void OnUnhighlight(ControllerWrapper controller);

        bool IsInRange(Vector3 grabPoint);
    }
}
