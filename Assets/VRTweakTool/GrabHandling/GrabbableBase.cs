﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    [SelectionBase]
    public class GrabbableBase : MonoBehaviour, IHandleGrabbing
    {
        private GrabMaster _master;
        public GrabMaster master
        {
            get
            {
                if (_master == null)
                {
                    _master = GetComponentInParent<GrabMaster>();
                }
                return _master;
            }
        }

        [Header("Grabbable Base params")]
        [SerializeField]
        private Collider[] _colliders;
        public Collider[] colliders
        {
            get
            {
                if (_colliders == null || _colliders.Length == 0 || _colliders.Any(c => c == null))
                {
                    _colliders = GetComponentsInChildren<Collider>().Where(c => c.GetComponentInParent<GrabbableBase>() == this).ToArray();
                }
                return _colliders;
            }
        }

        private Rigidbody _rigidbody;
        public new Rigidbody rigidbody
        {
            get
            {
                if (_rigidbody == null)
                {
                    _rigidbody = GetComponent<Rigidbody>();
                }
                return _rigidbody;
            }
        }

        /// <summary>
        /// Contains all current grabbers that are grabbing this obj using any method
        /// </summary>
        private HashSet<ControllerWrapper> whoGrabbin = new HashSet<ControllerWrapper>();

        /// <summary>
        /// Contains all current highlighters that are currently highlighting this obj
        /// </summary>
        private HashSet<ControllerWrapper> whoHighlightin = new HashSet<ControllerWrapper>();

        [SerializeField]
        private bool _isGrabbed;
        public bool isGrabbed
        {
            get
            {
                return _isGrabbed || whoGrabbin.Count > 0;
            }
        }

        [SerializeField]
        private float _maxGrabDistance = 0.25f;
        public float maxGrabDistance
        {
            get { return _maxGrabDistance; }
        }

        /// <summary>
        /// GrabMethod. Joint is the default, but sometimes one might prefer a direct move, when RBs should stay kinematic. When no rigidbody present, falls back to transform.move
        /// </summary>
        [Tooltip("GrabMethod. Joint is the default, but sometimes one might prefer a direct move, when RBs should stay kinematic. When no rigidbody present, falls back to transform.move")]
        public GrabMethod grabMethod = GrabMethod.RigidbodyJoint;

        /// <summary>
        /// AddSecondMaterial adds the outline shader material as a second mat on all renderers. Best suited for smooth normals. 
        /// ToggleRenderers passes through the renderers list and enables/disables them. You can have enabled and disabled renderers in the list, so you toggle f.x. between a red mesh and a blue mesh
        /// </summary>
        [Tooltip("AddSecondMaterial adds the outline shader material as a second mat on all renderers. Best suited for smooth normals." +
            " ToggleRenderers passes through the renderers list and enables/disables them. You can have enabled and disabled renderers in the list, so you toggle f.x. between a red mesh and a blue mesh")]
        public HighlightMethod highlightMethod = HighlightMethod.AddSecondMaterial;

        [SerializeField]
        private Renderer[] _highlightRenderers;
        public Renderer[] highlightRenderers
        {
            get
            {
                if (_highlightRenderers == null)
                {
                    _highlightRenderers = GetComponentsInChildren<Renderer>().Where(r => r.GetComponentInParent<GrabbableBase>() == this).ToArray();
                }
                return _highlightRenderers;
            }
        }

        [SerializeField]
        private bool _isHighlighted;
        public bool isHighlighted
        {
            get
            {
                return _isHighlighted || whoHighlightin.Count > 0;
            }
        }

        public event System.Action<ControllerWrapper> OnGrabEvent, OnUngrabEvent;

        protected virtual void OnValidate()
        {
            if (colliders != null)
            {
                // just ckeching
            }
            if (highlightRenderers != null)
            {
            }
        }

        protected virtual void OnEnable()
        {
            if (master != null)
            {
                master.AddGrabbable(this);
            }
        }

        protected virtual void OnDisable()
        {
            if (master != null)
            {
                master.RemoveGrabbable(this);
            }
        }

        public Vector3 GetClosestPointToColliderSurface(Vector3 pos)
        {
            return colliders.ClosestPointOnSurface(pos);
        }

        public virtual void OnHighlight(ControllerWrapper controllerWrapper)
        {
            if (!isHighlighted)
            {
                HighlightUtils.Highlight(this, highlightMethod);
            }
            whoHighlightin.Add(controllerWrapper);
        }

        public virtual void OnUnhighlight(ControllerWrapper controllerWrapper)
        {
            whoHighlightin.Remove(controllerWrapper);
            if (!isHighlighted)
            {
                HighlightUtils.Unhighlight(this, highlightMethod);
            }
        }

        // happens on grab by controller
        public virtual void OnGrab(ControllerWrapper controllerWrapper)
        {
            //Debug.Log(controllerWrapper.name + " grabbed " + name);
            if (rigidbody != null)
            {
                if (grabMethod == GrabMethod.RigidbodyJoint)
                {
                    // grab with a joint on controller
                    JointGrabUtils.JointGrab(controllerWrapper.transform, rigidbody);
                }
                else if (grabMethod == GrabMethod.RigidbodyKinematicMove)
                {
                    TransformGrabUtils.RigidbodyGrab(controllerWrapper, rigidbody);
                }
            }
            else
            {
                if (grabMethod == GrabMethod.TransformOnlyPosition)
                {
                    TransformGrabUtils.TransformGrab(controllerWrapper, transform, true, false);
                }
                else if (grabMethod == GrabMethod.transformOnlyRotation)
                {
                    TransformGrabUtils.TransformGrab(controllerWrapper, transform, false, true);
                }
                else if (grabMethod == GrabMethod.TransformMove)
                {
                    TransformGrabUtils.TransformGrab(controllerWrapper, transform);
                }
                else
                {
                    TransformGrabUtils.TransformGrab(controllerWrapper, transform);
                }
            }

            whoGrabbin.Add(controllerWrapper);

            if (OnGrabEvent != null)
            {
                OnGrabEvent(controllerWrapper);
            }
        }

        // happens on ungrab by controller
        public virtual void OnUngrab(ControllerWrapper controllerWrapper)
        {
            //Debug.Log(controllerWrapper.name + " released " + name);

            if (rigidbody != null)
            {
                JointGrabUtils.JointUngrab(controllerWrapper.transform, rigidbody);
            }
            else
            {
                TransformGrabUtils.TransformUngrab(controllerWrapper, transform);
            }

            whoGrabbin.Remove(controllerWrapper);

            if (OnUngrabEvent != null)
            {
                OnUngrabEvent(controllerWrapper);
            }
        }

    }

    public enum GrabMethod
    {
        RigidbodyJoint = 0,
        RigidbodyKinematicMove = 1,
        TransformMove = 100, // fallback for rigidbody methods, when no rigidbody exists
        TransformOnlyPosition = 101, // like transform move but only position, not rotation
        transformOnlyRotation = 102, // the weird brother of OnlyPosition
    }
}
