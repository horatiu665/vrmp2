﻿namespace VRTweakTool
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class ToggleRenderersUponGrabbing : MonoBehaviour
    {
        private GrabbableBase _grabbable;
        public GrabbableBase grabbable
        {
            get
            {
                if (_grabbable == null)
                {
                    _grabbable = GetComponent<GrabbableBase>();
                }
                return _grabbable;
            }
        }

        [SerializeField]
        private Renderer[] _renderers;
        public Renderer[] renderers
        {
            get
            {
                if (_renderers == null || _renderers.Length == 0)
                {
                    _renderers = GetComponentsInChildren<Renderer>();
                }
                return _renderers;
            }
        }

        [Header("When true, the initial setup is how it looks when grabbed, reverse ungrabbed.")]
        public bool initialStateIsGrabbed = true;

        private void OnValidate()
        {
            if (renderers != null)
            {
                // justnchckecnig
            }
        }

        private void OnEnable()
        {
            grabbable.OnGrabEvent += Grabbable_OnGrabEvent;
            grabbable.OnUngrabEvent += Grabbable_OnUngrabEvent;
            SetRenderers(initialStateIsGrabbed);
        }

        private void OnDisable()
        {
            grabbable.OnGrabEvent -= Grabbable_OnGrabEvent;
            grabbable.OnUngrabEvent -= Grabbable_OnUngrabEvent;
        }

        private void Grabbable_OnGrabEvent(ControllerWrapper obj)
        {
            SetRenderers(true);
        }

        private void Grabbable_OnUngrabEvent(ControllerWrapper obj)
        {
            SetRenderers(false);
        }

        private void SetRenderers(bool isGrabbed)
        {
            for (int i = 0; i < renderers.Length; i++)
            {
                renderers[i].enabled = isGrabbed ^ !initialStateIsGrabbed;
            }
        }

    }
}