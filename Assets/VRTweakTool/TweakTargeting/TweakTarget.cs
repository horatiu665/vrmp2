﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.Events;

    [Serializable]
    public class TweakTarget
    {
        // only one of these boys can be active at one time
        [SerializeField]
        private GameObject _goTarget;
        public GameObject goTarget
        {
            get
            {
                return _goTarget;
            }
            set
            {
                _cTarget = null;
                _goTarget = value;
            }
        }

        // only one of these boys can be active at one time
        [SerializeField]
        private Component _cTarget;
        public Component cTarget
        {
            get
            {
                return _cTarget;
            }
            set
            {
                _goTarget = null;
                _cTarget = value;
            }
        }

        private object autoTargetReference
        {
            get
            {
                if (goTarget != null)
                    return goTarget;
                return cTarget;
            }
        }

        [SerializeField]
        private List<ReflectionIdentifierData> _targetId;
        public List<ReflectionIdentifierData> targetId
        {
            get
            {
                if (_targetId == null)
                {
                    _targetId = new List<ReflectionIdentifierData>();
                }
                return _targetId;
            }
            private set
            {
                _targetId = value;
            }
        }

        // ref to last link in chain of references, so we do not waste cpu searching every time we change value
        private object quickndirty = null;

        // if quickndirty != null, this is the last index of the targetId. 
        private int quickndirtyIndex
        {
            get
            {
                return targetId.Count - 1;
            }
        }

        public bool allInstances = false;

        /// <summary>
        /// Generated from the goTarget
        /// </summary>
        [NonSerialized]
        private GenericMenuItem _menu;
        public GenericMenuItem menu
        {
            get
            {
                if (_menu == null)
                {
                    ResetMenu();
                }
                return _menu;
            }
        }

        public void ResetMenu()
        {
            _menu = new GenericMenuItem()
            {
                targetReference = autoTargetReference,
            };
        }

        public object GetValue()
        {
            // if quickndirty is not null, take that (it's a shortcut). otherwise, start from whichever is not null - goTarget or cTarget. and save quickndirty at second to last step, for future encounters
            object pointer = null;
            if (quickndirty != null)
            {
                pointer = quickndirty;
            }
            else if (goTarget != null)
            {
                pointer = (object)goTarget;
            }
            else if (cTarget != null)
            {
                pointer = (object)cTarget;
            }
            else
            {
                return null;
            }

            //Debug.Log("Q: " + quickndirty);
            //Debug.Log("G: " + goTarget);
            //Debug.Log("C: " + cTarget);
            //Debug.Log("P: " + pointer);
            int i = 0;
            // for shortcut
            if (quickndirty != null)
            {
                i = quickndirtyIndex;
            }
            if (pointer != null)
            {
                for (; i <= targetId.Count - 1; i++)
                {
                    if (targetId[i] != null && pointer != null)
                    {
                        // navigate through ref list
                        pointer = targetId[i].GetReference(pointer);

                        // might be null after stupid getreference in case ref was lost before. weird how object can just be null sometimes.
                        if (pointer == null)
                        {
                            Debug.LogError("[ERR] IT HAPPENED! pointer is null after GetReference(notnull). probably didn't find value or property or component...");
                            return null;
                        }

                        if (i == targetId.Count - 2)
                        {
                            // save second last pointer as the quick n dirty reference so we don't have to cycle thru all every time 
                            quickndirty = pointer;
                        }
                    }
                    else
                    {
                        // null. badd.
                        return null;
                    }
                }
                return pointer;
            }

            return null;
        }

        private void SetValues(object value)
        {
            // find references for all existing objects of the referenced type (only works on cTarget)
            if (cTarget != null)
            {
                // find all objects of cTarget type
                var type = cTarget.GetType();

                // this finds all objects of the type, but returns them as an array of UnityEngine.Object. But we need an array of (type), so we have to use Convert.ChangeType() as seen below.
                var all = GameObject.FindObjectsOfType(type);

                for (int a = 0; a < all.Length; a++)
                {
                    object pointer = Convert.ChangeType(all[a], type);
                    if (pointer != null)
                    {
                        int i = 0;
                        bool itWorks = true;
                        for (; i <= targetId.Count - 2; i++)
                        {
                            var t = targetId[i];
                            if (t != null)
                            {
                                pointer = t.GetReference(pointer);
                            }
                            else
                            {
                                itWorks = false;
                            }
                        }
                        if (itWorks)
                        {
                            targetId[i].SetValue(pointer, value);
                        }
                    }
                }
            }

        }

        public void SetValue(object value)
        {
            // if allInstances is true, set values on ALL objects, but also set value on target to make sure the value is ready for the next GetValue().
            if (allInstances && cTarget != null)
            {
                SetValues(value);
            }

            object pointer = null;
            if (quickndirty != null)
            {
                pointer = quickndirty;
            }
            else if (goTarget != null)
            {
                pointer = (object)goTarget;
            }
            else if (cTarget != null)
            {
                pointer = (object)cTarget;
            }
            else
            {
                return;
            }

            int i = 0;
            // for shortcut
            if (quickndirty != null)
            {
                i = quickndirtyIndex;
            }
            if (pointer != null)
            {
                for (; i <= targetId.Count - 2; i++)
                {
                    var t = targetId[i];
                    if (t != null)
                    {
                        pointer = t.GetReference(pointer);
                    }
                    else
                    {
                        // nulll!!!! bad
                        return;
                    }
                    // but only until n-1 because the last one is our value. we must overwrite that one with the value in SetValue ;)
                }

                // pointer is now the parent value. we can set value based on the type of reference we have...
                targetId[i].SetValue(pointer, value);

            }
        }
        
        public string GetLastWordInPath()
        {
            return FieldNameToCamelSpaces(targetId[targetId.Count - 1].path);
        }

        public string GetTextPath()
        {
            string path = "";
            // start from whichever is not null
            if (goTarget != null)
            {
                path += goTarget.name;
            }
            else if (cTarget != null)
            {
                path += cTarget.ToString();
            }
            else
            {
                path += "null";
            }
            // save first step in path
            path += ".";
            for (int i = 0; i < targetId.Count; i++)
            {
                var t = targetId[i];
                if (t != null)
                {
                    path += t.path;
                    if (i < targetId.Count - 1)
                    {
                        // dot between steps, like in programming
                        path += ".";
                    }
                }
                else
                {
                    return "Null Path";
                }
            }

            return path;
        }

        // TODO: make this extension of string or FieldInfo or something
        public static string FieldNameToCamelSpaces(string s)
        {
            var rs = Regex.Replace(s, "([a-z](?=[A-Z]|[0-9])|[A-Z](?=[A-Z][a-z]|[0-9])|[0-9](?=[^0-9]))", "$1 ");
            return rs;
        }

        /// <summary>
        /// Based on a selected menu item, sets the target for the tweaking... in a serializable way. );
        /// </summary>
        /// <param name="menuItem"></param>
        public void SetTarget(GenericMenuItem targetMenu)
        {
            quickndirty = null;

            // how do we get to the leaf...
            var target = targetMenu.targetReference;
            // ...from the earliest starting point that we can reference and serialize? like gameobject or component.

            targetId = new List<ReflectionIdentifierData>();

            // we have to find the first component, ancestor of target, that we can reference via serializable field. we start from the leaf.
            var pointer = targetMenu;
            bool failed = false;
            int montecarlo = 120;
            while (!GenericMenuItem.CanReferenceViaScript(pointer.targetType))
            {
                // if parent not available continue without adding new stuff to the list
                if (pointer.targetType != TargetReferenceTypes.Dummy)
                {
                    // add ref to the parent identifier so we can get from its parent to it.
                    // from this guy's parent to this guy: count-2 to count-1 for the last one since we cannot reference the last one. after this set pointer to the parent.
                    // then run while loop again.
                    // if parent cannot reference, save path to parent and move to its parent, to check again.
                    // then finally when count-3 can be referenced, we do not go into the while loop, and we saved refs from that guy to its child all the way to the final target. YEA
                    targetId.Insert(0, pointer.reflectionIdentifier.Clone());

                    // if pointer is null, we failed because we cannot go back from null to the reference.
                    if (pointer == null)
                    {
                        failed = true;
                        break;
                    }

                }

                // go to parent because this obj cannot be referenced.
                pointer = pointer.parentItem;

                // we took too long! montecarlo steps are here to prevent infinite while.
                if (montecarlo-- <= 0)
                {
                    failed = true;
                    break;
                }
            }

            if (!failed)
            {
                // we did not fail, meaning pointer is now a reference to something we can SERIALIZE!! WHOOOPTETEEEEE
                // SET INIT OBJECT
                if (pointer.targetType == TargetReferenceTypes.GameObject)
                {
                    goTarget = (GameObject)pointer.targetReference;
                }
                else if (pointer.IsComponent())
                {
                    cTarget = (Component)pointer.targetReference;
                }
                // so now we can say GETVALUE!! and start from goTarget or cTarget, and follow the targetId list until we find the field we are lookin for ;)
                Debug.Log("Saved reference to " + pointer.targetReference.ToString() + " of type " + pointer.targetReference.GetType().FullName);
                var debugtext = "";
                for (int i = 0; i < targetId.Count; i++)
                {
                    debugtext += targetId[i].type + " = " + targetId[i].path + "\n";
                }
                Debug.Log(debugtext);
            }
            else
            {
                // we could not find an ancestor to reference, we cannot reference this field to edit it...... :(((
                Debug.LogError("[TweakTarget] Cannot reference " + target + " from lack of serializable object in ancestry of desired parameter. " +
                    "\nReached " + pointer.GetFullDisplayString() + "\n" +
                    "Please select something that can be serialized by unity, like a position or a float from a component!");

            }
        }


        /// <summary>
        /// True when the target is set and we can tweak the value
        /// </summary>
        /// <returns></returns>
        public bool IsReady()
        {
            try
            {
                var newValue = GetValue();
                if (newValue != null)
                {
                    SetValue(newValue);
                }
                else // null value can be considered that it's not ready.
                {
                    return false;
                }
            }
            catch
            {
                // error is considered as not ready
                return false;
            }

            return true;
        }
    }

}
