﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using UnityEngine;
    using UnityEngine.Events;

    /// <summary>
    /// Can contain a list of items (then it is a category not a menu) OR it can contain a selectable item (so it is like a button) but not both
    /// </summary>
    public class GenericMenuItem
    {
        public GenericMenuItem parentItem;

        /// <summary>
        /// The object referenced by this menu item. Sometimes it is a gameobject, but it can be FieldInfo or other exotic stuff, or just a string.
        /// </summary>
        public object targetReference { get; set; }

        /// <summary>
        /// Can be used via reflection to get from parentItem to this object.
        /// </summary>
        private ReflectionIdentifierData _reflectionIdentifier;
        public ReflectionIdentifierData reflectionIdentifier
        {
            get
            {
                return _reflectionIdentifier;
            }
            private set
            {
                _reflectionIdentifier = value;
            }
        }

        /// <summary>
        /// If null, is set in Generate(). Tells us the type of the targetReference, for ease of drawing inspector and whatnot, and also for overview of what objects have been implemented
        /// </summary>
        public TargetReferenceTypes targetType;

        /// <summary>
        /// Cannot should not serialize. because we must regenerate when serialization is lost, due to having generic object targetReference.
        /// </summary>
        public bool isGenerated { get; private set; }

        /// <summary>
        /// Fields and other objects that belong to the targetData.targetReference
        /// </summary>
        private List<GenericMenuItem> _childItems;
        public List<GenericMenuItem> childItems
        {
            get
            {
                if (_childItems == null)
                {
                    _childItems = new List<GenericMenuItem>();
                }
                return _childItems;
            }
        }

        //public void GenerateRecursive(int recursiveCount = 0, object rootObject = null)
        //{
        //    rootObject = rootObject ?? this.targetReference;
        //    Generate(rootObject);
        //    if (recursiveCount > 0)
        //    {
        //        foreach (var i in childItems)
        //        {
        //            i.GenerateRecursive(recursiveCount--);
        //        }
        //    }
        //}

        /// <summary>
        /// Generates the childItems list based on components and fields for this rootObject.
        /// </summary>
        /// <param name="gameObjectRoot"></param>
        public void Generate()
        {
            if (targetReference != null)
            {
                var objType = targetReference.GetType();
                // if null type, determine type here (sometimes the type is set before so we should not do double the work
                if (targetType == TargetReferenceTypes.Null)
                {
                    if (targetReference is GameObject)
                    {
                        // for gameobjects we save a reference and create items for each component
                        targetType = TargetReferenceTypes.GameObject;
                    }
                    else if (CheckPrimitiveTypes(objType))
                    {
                        // assignment is done in the check func
                    }
                    else if (CheckVectorQuaternionTypes(objType))
                    {
                        // ass is done in check func
                    }
                    else if (CheckBuiltinTypes(objType))
                    {
                        // ass is done in check funk
                    }
                    else if (CheckBuiltinComponents(targetReference))
                    {
                        // ass check funk
                    }
                    else
                    {
                        // default type (just spam all fields and properties)
                        targetType = TargetReferenceTypes.Default;
                    }
                }

                if (targetType != TargetReferenceTypes.Null)
                {
                    GenerateWithType(targetReference, targetType);

                    isGenerated = true;
                }

            }

        }

        /// <summary>
        /// Returns true if the reference object can be serialized as type Component
        /// </summary>
        /// <returns></returns>
        public bool IsComponent()
        {
            switch (targetType)
            {
            case TargetReferenceTypes.ComponentRigidbody:
            case TargetReferenceTypes.ComponentCollider:
            case TargetReferenceTypes.ComponentTransform:
            case TargetReferenceTypes.ComponentGeneric:
                return true;
            default:
                return false;
            }
        }

        /// <summary>
        /// checks if rootObject.GetType() is one of the bool, int, string or other little primitive types.
        /// </summary>
        /// <param name="objType">cached for rootObject.GetType()</param>
        /// <returns>true when set to one of int, string, etc. primitive types</returns>
        private bool CheckPrimitiveTypes(Type objType)
        {
            switch (Type.GetTypeCode(objType))
            {
            case TypeCode.Boolean:
                targetType = TargetReferenceTypes.Bool;
                return true;
            case TypeCode.Byte:
            case TypeCode.SByte:
            case TypeCode.UInt16:
            case TypeCode.UInt32:
            case TypeCode.UInt64:
            case TypeCode.Int16:
            case TypeCode.Int32:
            case TypeCode.Int64:
                targetType = TargetReferenceTypes.Int_ish;
                return true;
            case TypeCode.Single:
                targetType = TargetReferenceTypes.Float;
                return true;
            case TypeCode.Double:
                targetType = TargetReferenceTypes.Double;
                return true;
            case TypeCode.Char:
                targetType = TargetReferenceTypes.Char;
                return true;
            case TypeCode.String:
                targetType = TargetReferenceTypes.String;
                return true;
            }

            if (objType.IsPrimitive)
            {
                targetType = TargetReferenceTypes.UnknownPrimitive;
                return true;
            }

            return false;
        }

        private bool CheckVectorQuaternionTypes(Type objType)
        {
            if (objType.Equals(typeof(Vector2)))
            {
                targetType = TargetReferenceTypes.Vector2;
                return true;
            }
            else if (objType.Equals(typeof(Vector3)))
            {
                targetType = TargetReferenceTypes.Vector3;
                return true;
            }
            else if (objType.Equals(typeof(Vector4)))
            {
                targetType = TargetReferenceTypes.Vector4;
                return true;
            }
            else if (objType.Equals(typeof(Vector2Int)))
            {
                targetType = TargetReferenceTypes.Vector2Int;
                return true;
            }
            else if (objType.Equals(typeof(Vector3Int)))
            {
                targetType = TargetReferenceTypes.Vector3Int;
                return true;
            }
            else if (objType.Equals(typeof(Quaternion)))
            {
                targetType = TargetReferenceTypes.Quaternion;
                return true;
            }

            return false;
        }

        private bool CheckBuiltinTypes(Type objType)
        {
            if (objType.Equals(typeof(AnimationCurve)))
            {
                targetType = TargetReferenceTypes.AnimationCurve;
                return true;
            }
            else if (objType.Equals(typeof(Material)))
            {
                targetType = TargetReferenceTypes.Material;
                return true;
            }
            else if (objType.Equals(typeof(PhysicMaterial)))
            {
                targetType = TargetReferenceTypes.PhysicMaterial;
                return true;
            }
            else if (objType.Equals(typeof(Mesh)))
            {
                targetType = TargetReferenceTypes.Mesh;
                return true;
            }

            return false;
        }

        /// <summary>
        /// Checks if rootObject is rigidbody or collider or other similar shit
        /// Sets targetType too if found.
        /// </summary>
        private bool CheckBuiltinComponents(object obj)
        {
            if (obj is Transform)
            {
                targetType = TargetReferenceTypes.ComponentTransform;
                return true;
            }
            else if (obj is Rigidbody)
            {
                targetType = TargetReferenceTypes.ComponentRigidbody;
                return true;
            }
            else if (obj is Collider)
            {
                targetType = TargetReferenceTypes.ComponentCollider;
                return true;
            }
            else if (obj is Component)
            {
                // for any components we don't know about we just say generic component
                targetType = TargetReferenceTypes.ComponentGeneric;
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Map targetType to GenerateXXX() function. Easier on the eyes, helps when implementing new shit - also optimizes in case we already know the type - we can skip the type setting functions.
        /// </summary>
        /// <param name="obj"></param>
        /// <param name="targetType"></param>
        private void GenerateWithType(object obj, TargetReferenceTypes targetType)
        {
            switch (targetType)
            {
            // nothing!
            case TargetReferenceTypes.Null:
                break;

            // start easy with GameObject
            case TargetReferenceTypes.GameObject:
                GenerateForGameObject((GameObject)obj);
                break;

            // dummy heh. generate error here.
            case TargetReferenceTypes.Dummy:
                DisplayValue(obj);
                break;

            // simple value display
            case TargetReferenceTypes.Bool:
            case TargetReferenceTypes.Int_ish:
            case TargetReferenceTypes.Float:
            case TargetReferenceTypes.Double:
            case TargetReferenceTypes.Char:
            case TargetReferenceTypes.String:
            case TargetReferenceTypes.UnknownPrimitive:
                DisplayValue(obj);
                break;

            case TargetReferenceTypes.AnimationCurve:
                GenerateForDefaultObject(obj);
                break;
            case TargetReferenceTypes.Material:
                GenerateForMaterial(obj);
                break;
            case TargetReferenceTypes.PhysicMaterial:
                GenerateForPhysicMaterial(obj);
                break;
            case TargetReferenceTypes.Mesh:
                GenerateForMesh(obj);
                break;

            case TargetReferenceTypes.Vector2:
            case TargetReferenceTypes.Vector2Int:
            case TargetReferenceTypes.Vector3:
            case TargetReferenceTypes.Vector3Int:
            case TargetReferenceTypes.Vector4:
            case TargetReferenceTypes.Quaternion:
                GenerateForVectorQuaternion(obj, targetType);
                break;

            // components
            case TargetReferenceTypes.ComponentRigidbody:
                GenerateForDefaultObject(obj);
                break;
            case TargetReferenceTypes.ComponentCollider:
                GenerateForDefaultObject(obj);
                break;
            case TargetReferenceTypes.ComponentTransform:
                GenerateForDefaultObject(obj);
                break;
            case TargetReferenceTypes.ComponentGeneric:
                GenerateForDefaultObject(obj);
                break;


            // Default object a.k.a. unknown
            case TargetReferenceTypes.Default:
                GenerateForDefaultObject(obj);
                break;

            default:
                // do nothing? no because we don't know what this object is so maybe it is useful to check it
                GenerateForDefaultObject(obj);
                break;
            }
        }

        private void GenerateForVectorQuaternion(object obj, TargetReferenceTypes targetType)
        {
            // find fields x, y, z and w depending which type
            var type = obj.GetType();
            var itemProp = type.GetProperty("Item");
            var indexes = itemProp.GetIndexParameters();
            var indexCount = 2;
            if (targetType == TargetReferenceTypes.Vector3 || targetType == TargetReferenceTypes.Vector3Int)
            {
                indexCount = 3;
            }
            else if (targetType == TargetReferenceTypes.Vector4 || targetType == TargetReferenceTypes.Quaternion)
            {
                indexCount = 4;
            }

            for (int i = 0; i < indexCount; i++)
            {
                var value = itemProp.GetValue(obj, new object[] { i });
                string name;
                switch (i)
                {
                case 0:
                    name = "x";
                    break;
                case 1:
                    name = "y";
                    break;
                case 2:
                    name = "z";
                    break;
                case 3:
                    name = "w";
                    break;
                default:
                    name = "error";
                    break;
                }
                GenerateMenuItem(this, value, ReflectionIdentifierData.Field(name), TargetReferenceTypes.Float);
            }

            if (targetType == TargetReferenceTypes.Quaternion)
            {
                //eulerangles too
                var name = "eulerAngles";
                var eulerProp = type.GetProperty(name);
                GenerateMenuItem(this, eulerProp.GetValue(obj, null), ReflectionIdentifierData.Property(name), TargetReferenceTypes.Vector3);
            }
        }

        /// <summary>
        /// Generates a childItem for each component that belongs to the gameObject. No properties, although tag and layer and stuff could be implemented
        /// </summary>
        /// <param name="rootObject"></param>
        private void GenerateForGameObject(GameObject rootObject)
        {
            // [TODO]: fields and props of GameObject: tag, layer, name, etc.
            //...

            if (rootObject != null)
            {
                var components = rootObject.GetComponents<Component>();
                foreach (var component in components)
                {
                    // create a new selectable item for each component, and generate those upon request
                    GenerateMenuItem(this, component, ReflectionIdentifierData.Component(component), TargetReferenceTypes.ComponentGeneric);
                }
            }
            else
            {
                // null menu
                GenerateMenuItem(this, "null", ReflectionIdentifierData.Dummy(this), TargetReferenceTypes.Dummy);
            }
        }

        private void GenerateForMaterial(object obj)
        {
            // generate a dummy for now, so it doesn't have to get material and LEAK it in the scene :/
            //GenerateMenuItem(this, obj.ToString() + " (material dummy)", ReflectionIdentifierData.Dummy(this), TargetReferenceTypes.Dummy);
            targetType = TargetReferenceTypes.Dummy;
            DisplayValue("Material (dummy)");
        }

        private void GenerateForPhysicMaterial(object obj)
        {
            //GenerateMenuItem(this, obj.ToString() + " (physics mat dummy)", ReflectionIdentifierData.Dummy(this), TargetReferenceTypes.Dummy);
            targetType = TargetReferenceTypes.Dummy;
            DisplayValue("PhysicMaterial (dummy)");
        }

        private void GenerateForMesh(object obj)
        {
            targetType = TargetReferenceTypes.Dummy;
            DisplayValue("Mesh (dummy)");
        }

        /// <summary>
        /// When we don't know the type we make children for all fields in case we find something useful.
        /// </summary>
        private void GenerateForDefaultObject(object defaultObject)
        {
            var type = defaultObject.GetType();
            string nam = type.Name;
            var allFields = GetFields(type);

            // Group fields by type.
            // extra grouping ideas: inherited fields, getter/setters for props ... etc
            foreach (var fieldGroupPrivatesPublics in allFields.GroupBy(fi => (fi.IsPrivate ? 1 : 0) + (fi.IsStatic ? 2 : 0)))
            {
                // add an extra layer based on public/private/static
                if (fieldGroupPrivatesPublics.Any())
                {
                    var first = fieldGroupPrivatesPublics.First();
                    var catName = "";
                    if (first != null)
                    {
                        if (first.IsStatic)
                        {
                            catName += "Static ";
                        }
                        if (first.IsPublic)
                        {
                            catName += "Public ";
                        }
                        else
                        {
                            catName += "Private ";
                        }
                    }
                    catName += "Fields";

                    var menuItemCategory = GenerateMenuItem(this, catName, ReflectionIdentifierData.Dummy(this), TargetReferenceTypes.Dummy);
                    foreach (var fieldGroup in fieldGroupPrivatesPublics.GroupBy(fi => fi.FieldType.Name))
                    {
                        // category, like field type (floats, ints, particle system modules, etc)
                        //AddMenuCategory(menu, componentName, fieldGroup.Key);
                        foreach (FieldInfo field in fieldGroup)
                        {
                            // add a menu item for the field. simply text (this was for the inspector)
                            //AddMenuItem(menu, componentName, field, tweakTarget.fieldName == field.Name);
                            object valueOfField;
                            TargetReferenceTypes valueOfFieldType = TargetReferenceTypes.Null;
                            try
                            {
                                valueOfField = field.GetValue(defaultObject);
                            }
                            catch (Exception e)
                            {
                                valueOfField = "[ERR] " + field.Name + " throws " + e.GetType().Name;
                                valueOfFieldType = TargetReferenceTypes.Dummy;
                            }
                            GenerateMenuItem(menuItemCategory, valueOfField, ReflectionIdentifierData.Field(field.Name), valueOfFieldType);
                        }
                    }
                }
            }

            var allProps = GetProperties(type);
            var allPropsGroupedByPublicPrivate = GetPropertiesGroupByPublicPrivate(allProps);
            foreach (var propGroupPrivatePublics in allPropsGroupedByPublicPrivate)
            {
                // add an extra layer based on get/set methods public/private/static.
                if (propGroupPrivatePublics.Any())
                {
                    var first = propGroupPrivatePublics.First();
                    var catName = GetCategoryName(first);
                    var menuItemCategory = GenerateMenuItem(this, catName, ReflectionIdentifierData.Dummy(this), TargetReferenceTypes.Dummy);

                    foreach (var propGroup in propGroupPrivatePublics.GroupBy(pi => pi.PropertyType.Name))
                    {
                        foreach (PropertyInfo prop in propGroup)
                        {
                            GeneratePropertyMenuItem(defaultObject, menuItemCategory, prop);
                        }
                    }
                }
            }

        }

        private static string GetCategoryName(PropertyInfo prop)
        {
            var catName = "";
            bool isStatic = false;
            if (prop != null)
            {
                var g = prop.GetGetMethod();
                var s = prop.GetSetMethod();
                if (g != null)
                {
                    if (g.IsStatic)
                    {
                        isStatic = true;
                    }
                }

                if (s != null)
                {
                    if (s.IsStatic)
                    {
                        isStatic = true;
                    }
                }

                if (isStatic)
                {
                    catName += "Static ";
                }

                if (g != null)
                {
                    if (g.IsPublic)
                    {
                        catName += "Public ";
                    }
                    else
                    {
                        catName += "Private ";
                    }
                    catName += "Get ";
                }

                if (s != null)
                {
                    if (s.IsPublic)
                    {
                        catName += "Public ";
                    }
                    else
                    {
                        catName += "Private ";
                    }
                    catName += "Set ";
                }
            }
            catName += "Props";
            return catName;
        }

        private static GenericMenuItem GenerateMenuItem(GenericMenuItem menuItemParent, object value, ReflectionIdentifierData reflectionId, TargetReferenceTypes targetType = TargetReferenceTypes.Null)
        {
            var gi = new GenericMenuItem()
            {
                parentItem = menuItemParent,
                targetReference = value,
                reflectionIdentifier = reflectionId,
                // no targetType cause we can figure it out in the Generate() function.
                targetType = targetType,
            };
            menuItemParent.childItems.Add(gi);
            return gi;
        }

        /// <summary>
        /// Grouped dealing with properties here. coming from GenerateForDefault
        /// </summary>
        private void GeneratePropertyMenuItem(object defaultObject, GenericMenuItem menuItemCategory, PropertyInfo prop)
        {
            if (prop == null)
            {
                return;
            }
            var indexParams = prop.GetIndexParameters();
            var indexParamCount = indexParams == null ? 0 : indexParams.Length;
            if (indexParamCount >= 1)
            {
                var errorMsg = "[WAR] Property " + prop.Name + " has " + indexParamCount + " index parameters";
                GenerateMenuItem(menuItemCategory, errorMsg, ReflectionIdentifierData.Dummy(menuItemCategory), TargetReferenceTypes.Dummy);
            }
            else
            {
                object objValue;
                TargetReferenceTypes type = TargetReferenceTypes.Null;
                try
                {
                    objValue = prop.GetValue(defaultObject, null);
                }
                catch (Exception e)
                {
                    // GetValue() returned some error. return an error message
                    objValue = "[ERR] " + prop.Name + " throws " + e.GetType().Name;
                    type = TargetReferenceTypes.Dummy;
                }
                GenerateMenuItem(menuItemCategory, objValue, ReflectionIdentifierData.Property(prop.Name), type);
            }
        }

        // sanity helper
        private IEnumerable<IGrouping<int, PropertyInfo>> GetPropertiesGroupByPublicPrivate(IEnumerable<PropertyInfo> allProps)
        {
            return allProps.GroupBy(pi =>
            {
                var g = pi.GetGetMethod(true);
                var s = pi.GetSetMethod(true);
                return
                    (g != null ? 16 +
                        (g.IsPublic ? 1 : 0) +
                        (g.IsStatic ? 2 : 0)
                    : 0) +
                    (s != null ? 32 +
                        (s.IsPublic ? 4 : 0) +
                        (s.IsStatic ? 8 : 0)
                    : 0);

            });
        }

        /// <summary>
        /// Happens when the referenced value can be displayed directly and used as a tweak target. Easiest example: float. displayed on a knob.
        /// Assumes the targetType is already set.
        /// </summary>
        /// <param name="obj"></param>
        private void DisplayValue(object obj)
        {
            targetReference = obj;
        }

        private static IEnumerable<FieldInfo> GetFields(Type type)
        {
            // get all fields and properties of obj
            var allFields = type.GetFields(bindingFlags).Where(fi =>
            {
                if (fi.GetCustomAttributes(typeof(ObsoleteAttribute), true).Length > 0)
                {
                    return false;
                }
                return true;
            });
            return allFields;
        }

        private static IEnumerable<PropertyInfo> GetProperties(Type type)
        {
            // same as GetFields but for properties
            var allProps = type.GetProperties(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static).Where(pi =>
            {
                if (pi.GetCustomAttributes(typeof(ObsoleteAttribute), true).Length > 0)
                {
                    return false;
                }
                return true;
            });
            return allProps;
        }

        public void ClearAllRecursive()
        {
            ClearAllChildrenRecursive();

            // clear item...?
            targetReference = null;
            targetType = TargetReferenceTypes.Null;
        }

        public void ClearAllChildrenRecursive()
        {
            foreach (var c in childItems)
            {
                c.ClearAllRecursive();
            }
            childItems.Clear();

        }

        /// <summary>
        /// Returns a string detailing this object. ideal for display in a big list menu
        /// </summary>
        public string GetFullDisplayString()
        {
            // we can still display fieldPath for null/dummies so we know what we're missing or what the problem is.
            string fieldPath = "";
            if (reflectionIdentifier != null)
            {
                fieldPath = reflectionIdentifier.path + " ";
            }

            // if dummy, show fieldPath and targetRef as string 
            if (targetType == TargetReferenceTypes.Dummy)
            {
                return fieldPath + targetReference as string;
            }

            // null target ref usually means problems
            if (targetReference == null)
            {
                return fieldPath + "Null " + targetType;
            }

            var actualType = targetReference.GetType();
            return fieldPath + "(" + actualType.Name + ") \t" + (isGenerated ? " (" + childItems.Count + ")" : "");
        }

        // Like GetDisplayString() but for in-game
        // public object GetDisplayObject(); 

        /// <summary>
        /// True when the targetReference can be referenced from a script and be serialized in that way
        /// </summary>
        /// <param name="targetType"></param>
        /// <returns></returns>
        public static bool CanReferenceViaScript(TargetReferenceTypes targetType)
        {
            switch (targetType)
            {
            case TargetReferenceTypes.Null:
            case TargetReferenceTypes.Dummy:
            case TargetReferenceTypes.Default:
            case TargetReferenceTypes.Bool:
            case TargetReferenceTypes.Int_ish:
            case TargetReferenceTypes.Float:
            case TargetReferenceTypes.Double:
            case TargetReferenceTypes.Char:
            case TargetReferenceTypes.String:
            case TargetReferenceTypes.UnknownPrimitive:
            case TargetReferenceTypes.Vector2:
            case TargetReferenceTypes.Vector3:
            case TargetReferenceTypes.Vector2Int:
            case TargetReferenceTypes.Vector3Int:
            case TargetReferenceTypes.Vector4:
            case TargetReferenceTypes.Quaternion:
                return false;
            case TargetReferenceTypes.GameObject:
            case TargetReferenceTypes.ComponentGeneric:
            case TargetReferenceTypes.ComponentRigidbody:
            case TargetReferenceTypes.ComponentCollider:
            case TargetReferenceTypes.ComponentTransform:
                return true;

            default:
                return false;
            }
        }

        private HashSet<TargetReferenceTypes> selectableTypes = new HashSet<TargetReferenceTypes>
        {
            TargetReferenceTypes.AnimationCurve,
        };

        private HashSet<TargetReferenceTypes> nonselectableTypes = new HashSet<TargetReferenceTypes>
        {
            TargetReferenceTypes.Null,
            TargetReferenceTypes.Dummy,
        };

        // easy access to binding flags when getting fields and props
        public const BindingFlags bindingFlags = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Static;

        public bool CanBeSelected()
        {
            if (!isGenerated)
            {
                return false;
            }

            if (nonselectableTypes.Contains(targetType))
            {
                return false;
            }

            // can be selected if it's a leaf. (but we only know that if it's generated)
            if (childItems.Count == 0 && isGenerated)
            {
                return true;
            }

            if (selectableTypes.Contains(targetType))
            {
                return true;
            }

            return false;
        }

    }

    public enum TargetReferenceTypes
    {
        // basic utility types
        Null = 0,
        Dummy = 1, // may contain error info, or just a string, or a troll message or ( ͡° ͜ʖ ͡°) 
        Default = 2,
        GameObject = 3,

        // primitives, known and unknown (with a little buffer)
        Bool = 4,
        Int_ish = 5, // contains byte, int64 and other int-like types too... lazyyyy
        Float = 6,
        Double = 7,

        Char = 8,
        String = 9,

        UnknownPrimitive = 20,

        Vector2 = 21,
        Vector3 = 22,
        Vector2Int = 23, // unity 2017.2 types.... wouldnt be surprised if they are gone some day
        Vector3Int = 24, // unity 2017.2 types.... wouldnt be surprised if they are gone some day
        Vector4 = 25,
        Quaternion = 26,

        // unity types with custom tweak controls
        AnimationCurve = 27,
        Material = 28,
        PhysicMaterial = 29,
        Mesh = 30,


        ComponentGeneric = 500,
        ComponentRigidbody,
        ComponentCollider,
        ComponentTransform,

        // other fucked up types go here as we discover them

    }

    // must be serializable. can be used across play/edit mode and in prefabs, to store references to tweakable targets
    [Serializable]
    public class ReflectionIdentifierData
    {
        public enum IdentifierTypes
        {
            Field,
            Property,
            Component,
            Dummy,
        }

        public IdentifierTypes type;
        public string path;

        /// <summary>
        /// Returns data that identifies a string
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static ReflectionIdentifierData Field(string name)
        {
            return new ReflectionIdentifierData()
            {
                path = name,
                type = IdentifierTypes.Field,
            };
        }

        public static ReflectionIdentifierData Property(string name)
        {
            return new ReflectionIdentifierData()
            {
                path = name,
                type = IdentifierTypes.Property,
            };
        }

        internal static ReflectionIdentifierData Component(Component component)
        {
            return new ReflectionIdentifierData()
            {
                path = component.GetType().Name,
                type = IdentifierTypes.Component,
            };
        }

        // ref to the parent ;)
        // perhaps we should return the reflectionidentifierdata of the parent??... FOOD FOR THOUGHTS motherfucker, food for thoughts
        internal static ReflectionIdentifierData Dummy(GenericMenuItem parent)
        {
            return new ReflectionIdentifierData()
            {
                path = "",
                type = IdentifierTypes.Dummy,
            };
        }

        /// <summary>
        /// Starting from the parent object, which can be a class or gameobject or whatever, finds a reference to the object of type type via the path path. 
        /// This way one can keep a reference to the nearest ancestor that can be serialized, and a serialized chain of references like this one.
        /// </summary>
        /// <param name="ancestorInstanceObject"></param>
        /// <returns></returns>
        public object GetReference(object ancestorInstanceObject)
        {
            switch (type)
            {
            case IdentifierTypes.Field:
                var obje = ancestorInstanceObject;
                var typee = obje.GetType();
                var field = typee.GetField(path, GenericMenuItem.bindingFlags);
                var valo = field.GetValue(ancestorInstanceObject);
                return valo; //return ancestorInstanceObject.GetType().GetField(path).GetValue(ancestorInstanceObject);
            case IdentifierTypes.Property:
                var obj = ancestorInstanceObject;
                var typ = obj.GetType();
                var prop = typ.GetProperty(path, GenericMenuItem.bindingFlags);
                var val = prop.GetValue(ancestorInstanceObject, null);
                return val; //ancestorInstanceObject.GetType().GetProperty(path).GetValue(ancestorInstanceObject, null);
            case IdentifierTypes.Component:
                if (ancestorInstanceObject != null)
                {
                    if (ancestorInstanceObject is GameObject)
                    {
                        var go = ((GameObject)ancestorInstanceObject);
                        return go.GetComponent(path);
                    }
                    else
                    {
                        Debug.LogError("[ERR] Cannot cast " + ancestorInstanceObject.GetType().FullName + " to GameObject!");
                    }
                }
                return null;
            case IdentifierTypes.Dummy:
                // cannot do shit with the dummy.
                return null;
            }
            return null;
        }

        /// <summary>
        /// The reverse of GetReference(ancestor) is SetValue(child), when setting the value on a chain of references (since we need the Field or Property type to set a value in the right way).
        /// </summary>
        /// <param name="value">the new value of the reference.</param>
        public void SetValue(object ancestor, object value)
        {
            switch (type)
            {
            case IdentifierTypes.Field:
                //ancestor.GetType().GetField(path).SetValue(ancestor, value);
                var obje = ancestor;
                var typee = obje.GetType();
                var field = typee.GetField(path, GenericMenuItem.bindingFlags);
                field.SetValue(ancestor, value);
                break;
            case IdentifierTypes.Property:
                //ancestor.GetType().GetProperty(path).SetValue(ancestor, value, null);
                var obj = ancestor;
                var typ = obj.GetType();
                var prop = typ.GetProperty(path, GenericMenuItem.bindingFlags);
                prop.SetValue(ancestor, value, null);
                break;
            case IdentifierTypes.Component:
            case IdentifierTypes.Dummy:
            default:
                // if we get to Component or Dummy it's broken - we can never add the value here :(
                return;
            }
        }

        public ReflectionIdentifierData Clone()
        {
            return new ReflectionIdentifierData()
            {
                path = this.path,
                type = this.type,
            };
        }

    }
}
