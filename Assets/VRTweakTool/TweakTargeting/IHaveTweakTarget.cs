﻿namespace VRTweakTool
{
    // can be used by tools to set tweak target externally.
    public interface IHaveTweakTarget
    {
        TweakTarget tweakTarget { get; }
    }
}