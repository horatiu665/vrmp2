﻿namespace VRTweakTool
{
    /// <summary>
    /// Enum of all field types that can be edited with any of the VRTweakTools, with explicit indexing.
    /// </summary>
    public enum AllFieldTypes
    {
        Float = 0,
        Int = 1,

    }

    /// <summary>
    /// Filtered list only valid for knobs that can only change one value.
    /// </summary>
    public enum FieldTypesForKnobs
    {
        Float = 0,
        Int = 1,
    }


}
