﻿namespace VRTweakTool
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Events;
    using UnityEngine.UI;
    using Random = UnityEngine.Random;

    public class ButtonHelper : MonoBehaviour
    {
        private Text _text;
        public Text text
        {
            get
            {
                if (_text == null)
                {
                    _text = GetComponentInChildren<Text>();
                }
                return _text;
            }
        }

        private Button _button;
        public Button button
        {
            get
            {
                if (_button == null)
                {
                    _button = GetComponentInChildren<Button>();
                }
                return _button;
            }
        }
        
        [DebugButton]
        public void SetTextTo(string s)
        {
            text.text = s;
        }
        
    }
}