﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEngine.Events;

    [Serializable]
    public class TextButtonEvent : UnityEvent<TextButton>
    {

    }

    /// <summary>
    /// Component added to root of button, that can help deal with text on that button, such as if it was a keyboard button.
    /// </summary>
    public class TextButton : MonoBehaviour
    {
        private Button _button;
        public Button button
        {
            get
            {
                if (_button == null)
                {
                    _button = GetComponentInChildren<Button>();
                }
                return _button;
            }
        }

        private UnityEngine.UI.Text _textComponent;
        public UnityEngine.UI.Text textComponent
        {
            get
            {
                if (_textComponent == null)
                {
                    _textComponent = button.GetComponentInChildren<UnityEngine.UI.Text>();
                }
                return _textComponent;
            }
        }

        public TextButtonEvent OnPress = new TextButtonEvent();

        [SerializeField]
        private string _text;
        public string text
        {
            get
            {
                return textComponent.text;
            }
            set
            {
                textComponent.text = value;
            }
        }

        public enum TextButtonTypes
        {
            Normal = 0, // outputs the textString
            Backspace, // removes a letter backwards (like on windows keyboards)
            Return, // writes "\n" new line
        }

        public TextButtonTypes buttonType = TextButtonTypes.Normal;

        private void OnValidate()
        {
            if (!string.IsNullOrEmpty(_text))
            {
                text = _text;
            }
        }

        private void OnEnable()
        {
            if (!string.IsNullOrEmpty(_text))
            {
                text = _text;
            }
            // keyboard buttons should not be onoff
            button.onOff = false;
            button.OnButtonOff.AddListener(OnButtonOff);
        }

        private void OnDisable()
        {
            button.OnButtonOff.RemoveListener(OnButtonOff);
        }

        private void OnButtonOff(Button button)
        {
            OnPress.Invoke(this);
        }
        
    }
}