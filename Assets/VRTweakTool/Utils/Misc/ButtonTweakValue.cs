﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class ButtonTweakValue : MonoBehaviour
    {
        public TweakTarget tweakTarget;

        private Button _button;
        public Button button
        {
            get
            {
                if (_button == null)
                {
                    _button = GetComponentInChildren<Button>();
                }
                return _button;
            }
        }

        private UnityEngine.UI.Text _textComponent;
        public UnityEngine.UI.Text textComponent
        {
            get
            {
                if (_textComponent == null)
                {
                    _textComponent = button.GetComponentInChildren<UnityEngine.UI.Text>();
                }
                return _textComponent;
            }
        }

        [SerializeField]
        private string _buttonLabel;
        public string buttonLabel
        {
            get
            {
                return textComponent.text;
            }
            set
            {
                textComponent.text = value;
            }
        }

        private void OnValidate()
        {
            // onoff always true, cause we are setting a bool
            button.onOff = true;
            if (!Application.isPlaying)
            {
                buttonLabel = _buttonLabel;
            }
        }

        private void OnEnable()
        {
            if (!string.IsNullOrEmpty(_buttonLabel))
            {
                buttonLabel = _buttonLabel;
            }

            button.OnButtonOn.AddListener(OnButtonOn);
            button.OnButtonOff.AddListener(OnButtonOff);

            // set button state to tweakTarget state
            button.SetButtonState((bool)tweakTarget.GetValue());

            UpdateLabel();
        }

        private void OnDisable()
        {
            button.OnButtonOn.RemoveListener(OnButtonOn);
            button.OnButtonOff.RemoveListener(OnButtonOff);
        }

        private void OnButtonOn(Button button)
        {
            tweakTarget.SetValue(true);
            UpdateLabel();
        }

        private void OnButtonOff(Button button)
        {
            tweakTarget.SetValue(false);
            UpdateLabel();
        }

        private void UpdateLabel()
        {
            this.buttonLabel = tweakTarget.GetLastWordInPath() + ": " + tweakTarget.GetValue().ToString();
        }

    }
}