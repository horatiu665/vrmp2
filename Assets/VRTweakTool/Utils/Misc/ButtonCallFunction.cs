﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Events;
    using Random = UnityEngine.Random;

    public class ButtonCallFunction : MonoBehaviour
    {
        private Button _button;
        public Button button
        {
            get
            {
                if (_button == null)
                {
                    _button = GetComponentInChildren<Button>();
                }
                return _button;
            }
        }

        private UnityEngine.UI.Text _textComponent;
        public UnityEngine.UI.Text textComponent
        {
            get
            {
                if (_textComponent == null)
                {
                    _textComponent = button.GetComponentInChildren<UnityEngine.UI.Text>();
                }
                return _textComponent;
            }
        }

        [SerializeField, TextArea]
        private string _buttonLabel;
        public string buttonLabel
        {
            get
            {
                return textComponent.text;
            }
            set
            {
                textComponent.text = value;
            }
        }

        public UnityEvent OnClick = new UnityEvent();

        private void OnValidate()
        {
            button.onOff = false;

            if (!Application.isPlaying)
            {
                buttonLabel = _buttonLabel;
            }
        }

        private void OnEnable()
        {
            if (!string.IsNullOrEmpty(_buttonLabel))
            {
                buttonLabel = _buttonLabel;
            }
            else
            {
                _buttonLabel = buttonLabel;
            }

            button.OnButtonOn.AddListener(OnButtonOn);

        }

        private void OnDisable()
        {
            button.OnButtonOn.RemoveListener(OnButtonOn);
        }

        private void OnButtonOn(Button button)
        {
            OnClick.Invoke();
        }

    }
}