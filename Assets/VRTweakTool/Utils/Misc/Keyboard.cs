﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class Keyboard : MonoBehaviour
    {
        [Header("Refs")]
        public UnityEngine.UI.Text label;

        [SerializeField]
        private List<TextButton> _buttons;
        public List<TextButton> buttons
        {
            get
            {
                if (_buttons == null || _buttons.Count == 0)
                {
                    _buttons = GetComponentsInChildren<TextButton>().ToList();
                }
                return _buttons;
            }
        }

        private void OnEnable()
        {
            for (int i = 0; i < buttons.Count; i++)
            {
                buttons[i].OnPress.AddListener(ButtonPressed);
            }
        }

        public string text;

        private void ButtonPressed(TextButton button)
        {
            switch (button.buttonType)
            {
            case TextButton.TextButtonTypes.Normal:
                text += button.text;
                break;
            case TextButton.TextButtonTypes.Backspace:
                if (text.Length > 0)
                {
                    text = text.Remove(text.Length - 1);
                }
                break;
            case TextButton.TextButtonTypes.Return:
                text += "\n";
                break;
            default:
                break;
            }

            UpdateLabel();
        }

        void UpdateLabel()
        {
            label.text = text;
            // show cursor...?

        }

    }
}