﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class ButtonSetValueConstant : MonoBehaviour
    {
        public TweakTarget tweakTarget;

        private Button _button;
        public Button button
        {
            get
            {
                if (_button == null)
                {
                    _button = GetComponentInChildren<Button>();
                }
                return _button;
            }
        }

        private UnityEngine.UI.Text _textComponent;
        public UnityEngine.UI.Text textComponent
        {
            get
            {
                if (_textComponent == null)
                {
                    _textComponent = button.GetComponentInChildren<UnityEngine.UI.Text>();
                }
                return _textComponent;
            }
        }

        [SerializeField]
        private string _buttonLabel;
        public string buttonLabel
        {
            get
            {
                return textComponent.text;
            }
            set
            {
                textComponent.text = value;
            }
        }

        [Header("String versions of type and value... if cannot be parsed, button does nothing")]
        public string typeToParse;
        public string valueToParse;

        [DebugButton]
        private void TryGetTypeAndValueFromTweakTarget()
        {
            var val = tweakTarget.GetValue();
            Type type = val.GetType();
            try
            {
                typeToParse = type.FullName;
                var testType = Type.GetType(typeToParse);
            }
            catch
            {
                Debug.Log("Cannot parse the value type for some reason.");
            }

            TypeConverter converter = TypeDescriptor.GetConverter(type);
            if (converter.CanConvertTo(typeof(string)))
            {
                valueToParse = converter.ConvertTo(val, typeof(string)) as string;
            }
            else
            {
                Debug.Log("Cannot convert type " + type.Name + " to string. Consider implementing attribute [TypeConverterAttribute] on the value type class, and inherit from TypeConverter to support the string conversion");
            }
        }

        [DebugButton]
        private void TrySetValueUsingFunkyParsedType()
        {
            try
            {
                OnButtonOn(null);
                Debug.Log("Value set successfully");
            }
            catch (Exception e)
            {
                Debug.Log("Did not work. Exception: " + e);
            }
        }

        private void OnValidate()
        {
            button.onOff = false;
            if (!Application.isPlaying)
            {
                buttonLabel = _buttonLabel;
            }
        }

        private void OnEnable()
        {
            if (!string.IsNullOrEmpty(_buttonLabel))
            {
                buttonLabel = _buttonLabel;
            }
            else
            {
                _buttonLabel = buttonLabel;
            }


            button.OnButtonOn.AddListener(OnButtonOn);
        }

        private void OnDisable()
        {
            button.OnButtonOn.RemoveListener(OnButtonOn);
        }

        private void OnButtonOn(Button button)
        {
            Type type = Type.GetType(typeToParse);
            TypeConverter converter = TypeDescriptor.GetConverter(type);
            var value = converter.ConvertFromString(valueToParse);
            tweakTarget.SetValue(value);
        }

    }
}