﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Events;
    using Random = UnityEngine.Random;

    public class Button : MonoBehaviour, IHandlePunching
    {
        private Rigidbody _r;
        public Rigidbody r
        {
            get
            {
                if (_r == null)
                {
                    _r = GetComponent<Rigidbody>();
                }
                return _r;
            }
        }

        private ConfigurableJoint _joint;
        public ConfigurableJoint joint
        {
            get
            {
                if (_joint == null)
                {
                    _joint = GetComponent<ConfigurableJoint>();
                }
                return _joint;
            }
        }

        public Transform punchForwardDirection
        {
            get
            {
                return buttonForward;
            }
        }

        [Header("Button Type")]
        public bool onOff = true;

        [Header("Params")]

        public float toggleDelta = 0.03f;
        public float extraPos = 0.005f;
        public float epsilon = 0.001f;

        public Vector2 forwardDummyDistance = new Vector2(-0.05f, 0.3f);
        public float sideDummyDistance = 0.3f;

        [SerializeField, ReadOnly]
        private float distanceBetweenOnOffPos;

        // if button is not pressed this is true. if is being presseed, is false until it returns to new toggle pos, then true again. then you can press it again, canToggle is false, until it returns to resting position.
        bool canToggle = true;
        bool isPressed = false;

        [Header("Drag and drop refs")]
        public Transform toggleOnPos;
        public Transform toggleOffPos;
        public Transform buttonForward;

        private ControllerWrapper lastControllerTouching;

        /// <summary>
        /// Invoked when the button is pressed. On happens when button pressed, or always when onOff is false. Off only happens when button is released.
        /// </summary>
        [Header("Events")]
        public ButtonEvent OnButtonOn = new ButtonEvent();
        public ButtonEvent OnButtonOff = new ButtonEvent();

        private void Reset()
        {
            buttonForward = transform.parent;
            toggleOnPos = new GameObject("toggleOnPos").transform;
            toggleOnPos.SetParent(transform.parent);
            toggleOnPos.transform.localPosition = Vector3.zero;
            toggleOnPos.transform.localRotation = transform.localRotation;
            toggleOffPos = new GameObject("toggleOffPos").transform;
            toggleOffPos.SetParent(transform.parent);
            toggleOffPos.transform.localPosition = Vector3.forward * -0.08599836f;
            toggleOffPos.transform.localRotation = transform.localRotation;

        }

        private void OnValidate()
        {
            distanceBetweenOnOffPos = (toggleOffPos.position - toggleOnPos.position).magnitude;
        }

        private void FixedUpdate()
        {
            // find cur position between min and max as a 0..1 param
            var dot = Vector3.Dot(transform.position - toggleOffPos.position, -buttonForward.forward);

            if (canToggle)
            {
                var nextStateIsPressed = onOff ? !isPressed : false;
                if (dot >= toggleDelta)
                {
                    SetButtonState(nextStateIsPressed);

                    // canToggle is false until distance returns to acceptable values
                    canToggle = false;

                    if (lastControllerTouching != null)
                    {
                        lastControllerTouching.VibrateRaw(1f);
                    }

                    if (isPressed || !onOff)
                    {
                        OnButtonOn.Invoke(this);
                    }
                    else
                    {
                        OnButtonOff.Invoke(this);
                    }
                }
            }
            else
            {
                // we are waiting for the joint to return back to its rest position before we allow it to toggle state again
                if (dot < (isPressed ? (toggleOnPos.position - toggleOffPos.position).magnitude : 0f) + epsilon)
                {
                    canToggle = true;
                }
            }

            LimitMovements(dot);
        }

        /// <summary>
        /// Sets button to pressed or un-pressed, with the whole joints setting and everything ***but does NOT call OnButtonOn/Off events!!!***
        /// </summary>
        /// <param name="pressed">pressed or depressed</param>
        public void SetButtonState(bool pressed)
        {
            // reset joint to toggleOn position set up in the scene
            var oldPos = joint.transform.position;
            var oldRot = joint.transform.rotation;
            joint.transform.position = (pressed ? toggleOnPos : toggleOffPos).position;
            joint.transform.rotation = (pressed ? toggleOnPos : toggleOffPos).rotation;

            // this resets the joint
            joint.anchor = joint.anchor;

            joint.transform.position = oldPos;
            joint.transform.rotation = oldRot;

            this.isPressed = pressed;
        }

        private void LateUpdate()
        {
            var dot = Vector3.Dot(transform.position - toggleOffPos.position, -buttonForward.forward);
            LimitMovements(dot);
        }

        // does not allow button to move outside of toggle range 
        private void LimitMovements(float dot)
        {
            if (dot < 0)
            {
                // move transform to a place where next frame it will no longer be frozen.
                transform.position = toggleOffPos.position;
                r.constraints = RigidbodyConstraints.FreezeAll;
            }
            else if (dot > toggleDelta + extraPos)
            {
                transform.position = (toggleOffPos.position - (buttonForward.forward * (toggleDelta + extraPos)) * 0.999f);
                r.constraints = RigidbodyConstraints.FreezeAll;

            }
            else
            {
                r.constraints = RigidbodyConstraints.FreezeRotation;
            }
        }

        private void OnCollisionEnter(Collision collision)
        {
            var ctrl = FindControllerInParent(collision.collider.transform);
            if (ctrl != null)
            {
                lastControllerTouching = ctrl;
                ctrl.VibrateRaw(0.4f);
            }
        }

        private ControllerWrapper FindControllerInParent(Transform transform)
        {
            for (int i = 0; i < ControllerWrapper.allControllers.Count; i++)
            {
                var c = ControllerWrapper.allControllers[i];
                if (c.transform == transform)
                {
                    return c;
                }
            }

            if (transform.parent != null)
            {
                return FindControllerInParent(transform.parent);
            }

            return null;
        }

        bool IHandlePunching.isHighlighted
        {
            get
            {
                return false;
            }
        }

        void IHandlePunching.OnHighlight(ControllerWrapper controller)
        {
            //throw new NotImplementedException();
        }

        void IHandlePunching.OnUnhighlight(ControllerWrapper controller)
        {
            //throw new NotImplementedException();
        }

        public bool IsInRange(Vector3 grabPoint)
        {
            // want it active only in front of the button. so dot product forward must be between 0 and 1 (from surface to 1m in front of the button) * frontDistance
            // however we also apply limit to the sides, so we don't go too far on the sides (that means magnitude of projected vector)
            var proj = Vector3.ProjectOnPlane(grabPoint - punchForwardDirection.position, punchForwardDirection.forward);
            var projMag = proj.magnitude;
            var dot = Vector3.Dot(grabPoint - punchForwardDirection.position, punchForwardDirection.forward);
            return dot >= forwardDummyDistance.x && dot <= forwardDummyDistance.y && projMag < sideDummyDistance;
        }
    }

    [System.Serializable]
    public class ButtonEvent : UnityEvent<Button>
    {

    }
}