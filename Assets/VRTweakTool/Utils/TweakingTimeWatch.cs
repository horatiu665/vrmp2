﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TweakingTimeWatch : MonoBehaviour
{
    private Renderer[] _renderers;
    public Renderer[] renderers
    {
        get
        {
            if (_renderers == null)
            {
                _renderers = GetComponentsInChildren<Renderer>();
            }
            return _renderers;
        }
    }

    public float transitionAngle = 15f;
    public float visibilityAngle = 45f;

    private float alpha;

    public void SetInterfaceActive(bool active)
    {
        // enable buttons and shit
        throw new System.NotImplementedException();
    }

    public void SetAlpha(float alpha)
    {
        this.alpha = alpha;
        if (alpha < 1)
        {
            SetInterfaceActive(false);
        }
        else
        {
            SetInterfaceActive(true);
        }

        // set actual material alpha
        for (int i = 0; i < renderers.Length; i++)
        {
            var c = renderers[i].material.color;
            c.a = alpha;
            renderers[i].material.color = c;
        }
    }

    void Update()
    {
        // calc view angle based on camera and watch.forward and set alpha based on that
        var angle = 25f;
        // angle smaller than visibiilty => alpha == 1. larger than transition + visi => alpha == 0. in between is linear
        var newalpha = 1f - Mathf.InverseLerp(visibilityAngle, visibilityAngle + transitionAngle, angle);

        if (newalpha != alpha)
        {
            SetAlpha(newalpha);
        }

    }
}