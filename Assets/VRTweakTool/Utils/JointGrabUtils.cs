﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public static class JointGrabUtils
    {
        private static Dictionary<Transform, ConfigurableJoint> handJointDict = new Dictionary<Transform, ConfigurableJoint>();

        public static void JointGrab(Transform controller, Rigidbody grabbedObj)
        {
            // controller has the joint, controller can be connected to only one grabbed object. but grabbed obj can be grabbed by any number of grabbers
            var joint = GetOrCreateJoint(controller);
            joint.connectedBody = grabbedObj;
        }

        /// <summary>
        /// Gets or creates a "hand" joint.
        /// </summary>
        /// <returns></returns>
        public static ConfigurableJoint GetOrCreateJoint(Transform controller)
        {
            ConfigurableJoint joint = handJointDict.ContainsKey(controller) ? handJointDict[controller] : null;

            if (joint == null)
            {
                var go = new GameObject("[Gen] VRTweakTool Grab Joint");
                go.transform.SetParent(controller.transform);
                go.transform.localPosition = Vector3.zero;
                go.transform.localRotation = Quaternion.identity;

                var rb = go.AddComponent<Rigidbody>();
                rb.constraints = RigidbodyConstraints.FreezeAll;
                rb.useGravity = false;

                joint = go.AddComponent<ConfigurableJoint>();
                SetDefaultHandJointSettings(joint);

                handJointDict[controller] = joint;
            }

            return joint;
        }

        /// <summary>
        /// Sets a range of 'magic' values on the given <see cref="ConfigurableJoint"/> in order to setup the joint as a 'hand joint' for VR use.
        /// </summary>
        /// <param name="joint">The joint.</param>
        public static void SetDefaultHandJointSettings(ConfigurableJoint joint)
        {
            joint.xMotion = joint.yMotion = joint.zMotion = ConfigurableJointMotion.Locked;
            joint.angularXMotion = joint.angularYMotion = joint.angularZMotion = ConfigurableJointMotion.Free;

            var positionDrive = new JointDrive()
            {
                positionSpring = 200000,
                positionDamper = 1,
            };
            joint.xDrive = joint.yDrive = joint.zDrive = positionDrive;

            joint.rotationDriveMode = RotationDriveMode.Slerp;
            joint.slerpDrive = new JointDrive()
            {
                positionSpring = 200000,
                positionDamper = 1,
                maximumForce = float.MaxValue
            };
        }

        public static void JointUngrab(Transform controller, Rigidbody grabbedObj)
        {
            var joint = GetOrCreateJoint(controller);
            joint.connectedBody = null;
        }
    }
}
