﻿// from Ivan Notaros
// https://github.com/nothke/MeshX/blob/master/Assets/Scripts/MeshX/MeshX.cs
// 17 10 2017
// but hijacked and deleted 95%

namespace MeshXtensions
{
    using UnityEngine;
    using System.Collections.Generic;

    public static class MeshX
    {

        // when u already have a Hlinerenderer mesh, same amount of points, just wanna update their pos
        public static void Quadstrip(Mesh mesh, Vector3[] points, Vector3 normal, float width)
        {
            if (points.Length < 2)
                return;

            Vector3[] vertices = new Vector3[points.Length * 2];

            Vector3 dir = Vector3.zero;

            for (int i = 0; i < points.Length; i++)
            {
                if (i != points.Length - 1)
                    dir = points[i + 1] - points[i];
                Vector3 right = Vector3.Cross(dir.normalized, normal);

                Vector3 p0 = points[i] + right * (width * 0.5f);
                Vector3 p1 = points[i] + right * -(width * 0.5f);

                vertices[i * 2] = p0;
                vertices[i * 2 + 1] = p1;
            }

            mesh.vertices = vertices;

        }

        // Ivan's cleaned up QuadStrip. when u just trying to make a damn line renderer
        public static Mesh Quadstrip(Vector3[] points, Vector3 normal, float width, float uvScale = 1)
        {
            if (points.Length < 2)
                return null;

            Vector3[] vertices = new Vector3[points.Length * 2];
            Vector3[] meshNormals = new Vector3[points.Length * 2];
            //Vector2[] uvs = new Vector2[points.Length * 2];
            int[] tris = new int[points.Length * 2 * 3];

            Vector3 dir = Vector3.zero;

            for (int i = 0; i < points.Length; i++)
            {
                if (i != points.Length - 1)
                    dir = points[i + 1] - points[i];
                Vector3 right = Vector3.Cross(dir.normalized, normal);

                Vector3 p0 = points[i] + right * (width * 0.5f);
                Vector3 p1 = points[i] + right * -(width * 0.5f);

                vertices[i * 2] = p0;
                vertices[i * 2 + 1] = p1;

                // normals
                meshNormals[i * 2] = meshNormals[i * 2 + 1] = normal;

                // uvs
                //uvs[i * 2] = new Vector2(0, i * uvScale);
                //uvs[i * 2 + 1] = new Vector2(1, i * uvScale);

                // triangles
                if (i != 0)
                {
                    int v = i * 2;

                    tris[v * 3] = v - 2;
                    tris[v * 3 + 1] = v;
                    tris[v * 3 + 2] = v - 1;

                    tris[v * 3 + 3] = v - 1;
                    tris[v * 3 + 4] = v;
                    tris[v * 3 + 5] = v + 1;
                }
            }

            Mesh m = new Mesh();
            m.vertices = vertices;
            m.normals = meshNormals;
            //m.uv = uvs;
            m.triangles = tris;

            m.MarkDynamic();
            return m;
        }

    }
}