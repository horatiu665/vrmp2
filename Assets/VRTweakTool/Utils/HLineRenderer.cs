﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using MeshXtensions;

    public class HLineRenderer : MonoBehaviour
    {
        private MeshFilter _meshFilter;
        public MeshFilter meshFilter
        {
            get
            {
                if (_meshFilter == null)
                {
                    _meshFilter = GetComponent<MeshFilter>();
                }
                return _meshFilter;
            }
        }

        private MeshRenderer _meshRenderer;
        public MeshRenderer meshRenderer
        {
            get
            {
                if (_meshRenderer == null)
                {
                    _meshRenderer = GetComponent<MeshRenderer>();
                }
                return _meshRenderer;
            }
        }

        public Vector3[] vertices = new Vector3[100];

        public float width = 0.1f;

        private int _positionCount;
        public int positionCount
        {
            get
            {
                return _positionCount;
            }
            set
            {
                _positionCount = value;
                if (vertices.Length != value)
                {
                    var newVert = new Vector3[_positionCount];

                    // no point in doing this cause we gonna regenerate anyways after changing position count
                    //// adds vertices positions into the new vert. if vertices is shorter, adds all available. if longer, last ones are lost
                    //for (int i = 0; i < Mathf.Min(vertices.Length, _positionCount); i++)
                    //{
                    //    newVert[i] = vertices[i];
                    //}

                    vertices = newVert;

                }
            }
        }

        public void GenerateMesh()
        {
            // normal must be in local space
            Vector3 normal = transform.InverseTransformDirection(-transform.forward);
            if (meshFilter.sharedMesh == null)
            {
                meshFilter.sharedMesh = MeshX.Quadstrip(vertices, normal, width);
            }
            else
            {
                // update vertices only
                MeshX.Quadstrip(meshFilter.sharedMesh, vertices, normal, width);
            }
        }

        public void SetPosition(int i, Vector3 pos)
        {
            vertices[i] = pos;
        }

    }
}
