﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public static class TransformGrabUtils
    {
        private static Dictionary<ControllerWrapper, VRT_FakeParenting> controllerGrabbedTransformDict = new Dictionary<ControllerWrapper, VRT_FakeParenting>();
        private static Dictionary<ControllerWrapper, VRT_FakeParentingRigidbody> controllerGrabbedTransformDictRb = new Dictionary<ControllerWrapper, VRT_FakeParentingRigidbody>();

        public static void TransformGrab(ControllerWrapper controller, Transform grabbedObj, bool pos = true, bool rot = true)
        {
            // get/add fake parenting component to controller
            VRT_FakeParenting fp;
            if (!controllerGrabbedTransformDict.TryGetValue(controller, out fp))
            {
                fp = controller.gameObject.AddComponent<VRT_FakeParenting>();
                controllerGrabbedTransformDict[controller] = fp;
            }

            // ungrab grabbedObj from all other controllers
            foreach (var c in controllerGrabbedTransformDict)
            {
                if (c.Value.fakeChild == grabbedObj)
                {
                    c.Value.SetFakeParenting(null);
                }
            }
            fp.SetFakeParenting(grabbedObj, pos, rot);
        }

        public static void TransformUngrab(ControllerWrapper controller, Transform grabbedObj)
        {
            VRT_FakeParenting fp;
            if (controllerGrabbedTransformDict.TryGetValue(controller, out fp))
            {
                fp.SetFakeParenting(null);
            }
        }


        public static void RigidbodyGrab(ControllerWrapper controller, Rigidbody grabbedObj)
        {
            // get/add fake parenting component to controller
            VRT_FakeParentingRigidbody fp;
            if (!controllerGrabbedTransformDictRb.TryGetValue(controller, out fp))
            {
                fp = controller.gameObject.AddComponent<VRT_FakeParentingRigidbody>();
                controllerGrabbedTransformDictRb[controller] = fp;
            }

            // ungrab grabbedObj from all other controllers
            foreach (var c in controllerGrabbedTransformDictRb)
            {
                if (c.Value.fakeChild == grabbedObj)
                {
                    c.Value.SetFakeParenting(null);
                }
            }
            fp.SetFakeParenting(grabbedObj);
        }

        public static void RigidbodyUngrab(ControllerWrapper controller, Rigidbody grabbedObj)
        {
            VRT_FakeParentingRigidbody fp;
            if (controllerGrabbedTransformDictRb.TryGetValue(controller, out fp))
            {
                fp.SetFakeParenting(null);
            }
        }


    }
}
