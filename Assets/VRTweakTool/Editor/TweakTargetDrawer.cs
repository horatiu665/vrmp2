﻿namespace VRTweakTool.Editor
{
    using System;
    using UnityEngine;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEditor;
    using System.Reflection;

    [CustomPropertyDrawer(typeof(TweakTarget))]
    public class TweakTargetDrawer : PropertyDrawer
    {
        const int rows = 4;

        private TweakTarget tweakTarget;
        private SerializedProperty serializedProperty;

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return base.GetPropertyHeight(property, label) * rows;
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            this.serializedProperty = property; // for the stupid menu event shit

            tweakTarget = (TweakTarget)fieldInfo.GetValue(property.serializedObject.targetObject);

            position.height /= rows;

            // draw label with the name of the tweak target
            EditorGUI.LabelField(position, label);
            position.y += position.height;

            // indent that shit
            EditorGUI.indentLevel++;

            // preview the goTarget and the cTarget. to see which is actually set. looks ugly, this is only for debug.
            //var halfRect = position;
            //halfRect.width /= 2;
            //EditorGUI.PropertyField(halfRect, property.FindPropertyRelative("_goTarget"));
            //halfRect.x += halfRect.width;
            //EditorGUI.PropertyField(halfRect, property.FindPropertyRelative("_cTarget"));
            //position.y += position.height;

            var clearTargetId = false;
            if (tweakTarget.goTarget != null)
            {
                var newObj = (GameObject)EditorGUI.ObjectField(position, "Game Object", tweakTarget.goTarget, typeof(GameObject), true);
                if (newObj != null && newObj != tweakTarget.goTarget)
                {
                    tweakTarget.goTarget = newObj;
                    // don't clear ID since we have only changed the target gameobject but maybe we have the same scripts on........... we can clear ID by selecting something else... or special button.
                    clearTargetId = false;
                }
            }
            else if (tweakTarget.cTarget != null)
            {
                var newObj = (Component)EditorGUI.ObjectField(position, "Component", tweakTarget.cTarget, typeof(Component), true);
                if (newObj != null && newObj != tweakTarget.cTarget)
                {
                    if (newObj.GetType() == tweakTarget.cTarget.GetType())
                    {
                        // same type! reassign but don't clear ID
                        tweakTarget.cTarget = newObj;
                        clearTargetId = false;
                    }
                    else if (newObj is Transform)
                    {
                        tweakTarget.cTarget = null;
                        tweakTarget.goTarget = ((Transform)newObj).gameObject;
                        clearTargetId = true;
                    }
                    else
                    {
                        tweakTarget.cTarget = newObj;
                        clearTargetId = true;

                    }
                }
            }
            else
            {
                // both null -> make obj field that accepts both hheheheh is that even possible? YES IT IS CAUSE I DID IT ;)
                var obj = EditorGUI.ObjectField(position, "GameObject/Component", null, typeof(UnityEngine.Object), true);
                if (obj != null)
                {
                    if (obj is GameObject)
                    {
                        tweakTarget.goTarget = obj as GameObject;
                    }
                    else if (obj is Transform)
                    {
                        tweakTarget.goTarget = ((Transform)obj).gameObject;
                    }
                    else if (obj is Component)
                    {
                        tweakTarget.cTarget = obj as Component;
                    }

                    // clear target id cause the target was null soooo doesn't make sense to keep the old string...
                    clearTargetId = true;

                }
            }

            if (clearTargetId)
            {
                // always clear targetId after a reassign
                tweakTarget.targetId.Clear();
            }

            position.y += position.height;

            var toggleAllRect = position;
            toggleAllRect.width = 24;
            toggleAllRect.x = position.xMax - toggleAllRect.width - 12;

            var toggleAllLabel = position;
            toggleAllLabel.width = 36;
            toggleAllLabel.x = position.xMax - toggleAllLabel.width - toggleAllRect.width - 12;

            var buttonRect = position;
            buttonRect.width -= toggleAllLabel.width + toggleAllRect.width + 12;

            // for multi-edit
            if (property.serializedObject.targetObjects.Length > 1)
            {
                // multi-button 
                if (GUI.Button(EditorGUI.IndentedRect(buttonRect), "Edit tweak TARGETS"))
                {
                    TweakTargetsListWindow.Get(property, fieldInfo);
                }
            }
            else
            {
                // single button
                if (GUI.Button(EditorGUI.IndentedRect(buttonRect), "Select Tweak Target"))
                {
                    TweakTargetMenuWindow.Get(tweakTarget, serializedProperty);
                    //GenerateMenu(tweakTarget).ShowAsContext();
                }
            }
            EditorGUI.LabelField(toggleAllLabel, "All");
            tweakTarget.allInstances = EditorGUI.Toggle(toggleAllRect, tweakTarget.allInstances);

            position.y += position.height;

            string text = TweakTargetMenuWindow.windowExists ? "Choosing target..." : tweakTarget.GetTextPath();
            EditorGUI.SelectableLabel(position, text);
            position.y += position.height;

            EditorGUI.indentLevel--;


            // WE ARE USING THE OF LATE UNSUPPORTED METHOD. 
            // we changed the target obj. properties by hand. 
            // so we need to tell unity's serializer to get that data and save it in the serialized data 
            // (opposite of setting serialized data into the real obj data).
            // check this thread for more sketchy info: https://forum.unity.com/threads/does-serializedobject-applymodifiedproperties-work-on-all-changes.463015/
            EditorUtility.SetDirty(property.serializedObject.targetObject);
        }

    }

    public class TweakTargetsListWindow : EditorWindow
    {
        public TweakTarget[] tweakTargets;

        // for use in retarded serializing (set dirrrrty)
        private SerializedProperty serializedProperty;

        private Vector2 scrollPos;

        private bool init = false;
        public static bool windowExists = false;

        public static TweakTargetsListWindow Get(SerializedProperty property, FieldInfo fieldInfo)
        {
            var w = GetWindow<TweakTargetsListWindow>(true);
            w.tweakTargets = property.serializedObject.targetObjects.Select(o => (TweakTarget)fieldInfo.GetValue(o)).Where(o => o != null).ToArray();
            w.serializedProperty = property;
            windowExists = true;
            return w;
        }

        private void OnDestroy()
        {
            windowExists = false;
        }

        private void OnGUI()
        {
            serializedProperty.serializedObject.Update();

            EditorGUI.BeginChangeCheck();
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            EditorGUILayout.LabelField("Tweak Targets Target ID list");

            EditorGUILayout.PropertyField(this.serializedProperty.FindPropertyRelative("_targetId"), true);

            if (GUILayout.Button("Done"))
            {
                if (serializedProperty != null)
                {
                    for (int i = 0; i < serializedProperty.serializedObject.targetObjects.Length; i++)
                    {
                        EditorUtility.SetDirty(serializedProperty.serializedObject.targetObjects[i]);
                    }
                }
                this.Close();
            }


            EditorGUILayout.EndScrollView();

            serializedProperty.serializedObject.ApplyModifiedProperties();
        }
    }

    public class TweakTargetMenuWindow : EditorWindow
    {
        public TweakTarget tweakTarget;

        // for use in retarded serializing (set dirrrrty)
        private SerializedProperty serializedProperty;

        private Vector2 scrollPos;
        private HashSet<GenericMenuItem> expandedItems = new HashSet<GenericMenuItem>();
        private bool init = false;
        public static bool windowExists = false;

        public static TweakTargetMenuWindow Get(TweakTarget tweakTarget, SerializedProperty serializedProperty)
        {
            var w = GetWindow<TweakTargetMenuWindow>(true);
            w.tweakTarget = tweakTarget;
            w.tweakTarget.ResetMenu();
            w.serializedProperty = serializedProperty;
            windowExists = true;
            return w;
        }

        private void OnDestroy()
        {
            windowExists = false;
        }

        private void OnGUI()
        {
            scrollPos = EditorGUILayout.BeginScrollView(scrollPos);
            EditorGUILayout.LabelField("Menu viewer");
            // auto expand shit based on recursion
            if (!init)
            {
                ExpandRecursive(tweakTarget.menu, 2);
                init = true;
            }
            DrawMenuItemsRecursive(tweakTarget.menu);

            EditorGUILayout.EndScrollView();
        }

        private void ExpandRecursive(GenericMenuItem menu, int recursions)
        {
            if (recursions <= 0)
            {
                return;
            }
            if (!menu.isGenerated)
            {
                menu.Generate();
            }
            expandedItems.Add(menu);
            foreach (var m in menu.childItems)
            {
                ExpandRecursive(m, recursions - 1);
            }
        }

        private void DrawMenuItemsRecursive(GenericMenuItem menu, int recursion = 25)
        {
            if (!menu.isGenerated)
            {
                menu.Generate();
            }
            string label = menu.GetFullDisplayString();

            // leaf?
            if (menu != null && menu.CanBeSelected())
            {

                EditorGUILayout.BeginHorizontal();
                {
                    // Label for selectable control
                    EditorGUILayout.LabelField(label);

                    // non-indented buttons and type info
                    var preIndentLevel = EditorGUI.indentLevel;
                    EditorGUI.indentLevel = 0;
                    if (GUILayout.Button("Select", GUILayout.Width(60)))
                    {
                        SelectTarget(menu);
                    }
                    EditorGUILayout.LabelField(menu.targetType.ToString(), GUILayout.Width(120));
                    EditorGUI.indentLevel = preIndentLevel;

                }
                EditorGUILayout.EndHorizontal();

            }
            else // not leaf.
            {
                var prevExpand = expandedItems.Contains(menu);
                var expand = EditorGUILayout.Foldout(prevExpand, label);
                if (!prevExpand && expand) // just pressed the button to contract
                {
                    expandedItems.Add(menu);
                }
                if (prevExpand && !expand) // just pressed button to contract
                {
                    expandedItems.Remove(menu);
                }
                if (expand)
                {
                    // global recursion limit.... stupid! but needs to be dealt with
                    if (recursion <= 0)
                    {
                        return;
                    }
                    if (menu.childItems != null && menu.childItems.Count > 0)
                    {
                        EditorGUI.indentLevel++;
                        foreach (var m in menu.childItems)
                        {
                            DrawMenuItemsRecursive(m, recursion - 1);
                        }
                        EditorGUI.indentLevel--;
                    }
                }
            }
        }

        private void SelectTarget(GenericMenuItem menu)
        {
            tweakTarget.SetTarget(menu);

            if (serializedProperty != null)
            {
                EditorUtility.SetDirty(serializedProperty.serializedObject.targetObject);
            }
            else
            {
                Debug.LogError("Did not serialize. you will lose changes. pls apply your prefab or fix it in some other way");
            }

            this.Close();
        }
    }
}
