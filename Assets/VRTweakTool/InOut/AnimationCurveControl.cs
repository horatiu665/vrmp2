﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;

    public class AnimationCurveControl : MonoBehaviour
    {
        public TweakTarget animationCurveTweak;

        [Header("Refs setup")]
        public GrabbableBase knobPrefab;
        public Transform tgHelper;

        [Header("Auto refs")]
        public List<AnimationCurveKnob> keyKnobs = new List<AnimationCurveKnob>();

        private GrabMaster _master;
        public GrabMaster master
        {
            get
            {
                if (_master == null)
                {
                    _master = GetComponentInParent<GrabMaster>();
                }
                return _master;
            }
        }

        public List<GrabMaster.ControllerGrabberData> controllers
        {
            get
            {
                return master.controllers;
            }
        }

        [SerializeField]
        AnimationCurve _animCurve = new AnimationCurve();
        // do not serialize. this is true when the _animCurve is sucecssfuly set as the tweak target.
        bool _animCurveFromTweak = false;
        AnimationCurve animCurve
        {
            get
            {
                if (!_animCurveFromTweak)
                {
                    if (animationCurveTweak.IsReady())
                    {
                        var tweakCurve = animationCurveTweak.GetValue() as AnimationCurve;
                        if (tweakCurve != null)
                        {
                            _animCurve = tweakCurve;
                            _animCurveFromTweak = true;
                        }
                    }
                }
                if (_animCurve == null)
                {
                    _animCurve = animationCurveTweak.GetValue() as AnimationCurve;
                }
                return _animCurve;

            }
        }

        [Header("New knob params")]
        public float newKnobDistance = 0.2f;
        public float grabPanelZDistance = 0.25f;

        [Header("Draw curve params")]
        public int resolution = 100;

        [SerializeField]
        private HLineRenderer _line;
        public HLineRenderer line
        {
            get
            {
                if (_line == null)
                {
                    _line = GetComponentInChildren<HLineRenderer>();
                }
                return _line;
            }
        }

        public float localZWhenOutOfRange = 0.05f;

        [Header("Resize curve params")]
        public GrabbableBase resizeHandleMax;
        public GrabbableBase resizeHandleMin;

        // these are the offsets of the handles compared to the canvasMin and canvasMax.
        private Vector3 maxHandleInitOffset;
        private Vector3 minHandleInitOffset;

        private Vector3 handleMaxOldPos, handleMinOldPos;

        public Vector2 canvasMin = new Vector2(-2, -2), canvasMax = new Vector2(2, 2);
        public Vector2 curveMin = new Vector2(0, 0), curveMax = new Vector2(1, 1);

        // when scaling/moving.
        Vector2 curveMaxPivot = new Vector2(1, 1), curveMinPivot = new Vector2(0, 0);

        [Header("Update rare params")]
        // UPDATE RARE
        public float updateRareFps = 10f;
        private float updateRareLast = 0f;
        public float updateRareDeltaTime
        {
            get
            {
                return Time.time - updateRareLast;
            }
        }

        Dictionary<ControllerWrapper, GameObject> gizmos = new Dictionary<ControllerWrapper, GameObject>();

        private void OnValidate()
        {
            if (line != null)
            {
                // just checkeingk
            }
            resolution = Mathf.Clamp(resolution, 10, 1000);
        }

        private void OnEnable()
        {
            // init orig pos for scaling cubes
            minHandleInitOffset = resizeHandleMin.transform.localPosition - (Vector3)canvasMin;
            maxHandleInitOffset = resizeHandleMax.transform.localPosition - (Vector3)canvasMax;
            resizeHandleMin.OnGrabEvent += ResizeHandleMin_OnGrabEvent;
            resizeHandleMax.OnGrabEvent += ResizeHandleMax_OnGrabEvent;
            resizeHandleMin.OnUngrabEvent += ResizeHandleMin_OnUngrabEvent;
            resizeHandleMax.OnUngrabEvent += ResizeHandleMax_OnUngrabEvent;

            // set anim limits based on min/max value of anim curve
            FindAnimCurveMinMaxBounds();

            ResetKnobs();
            UpdateCurve();
            DrawCurve();

        }

        private void FindAnimCurveMinMaxBounds()
        {
            Vector2 min = Vector2.one * float.MaxValue, max = Vector2.one * float.MinValue;
            for (int i = 0; i < animCurve.keys.Length; i++)
            {
                var k = (animCurve.keys[i]);
                if (k.time < min.x)
                {
                    min.x = k.time;
                }
                if (k.value < min.y)
                {
                    min.y = k.value;
                }
                if (k.time > max.x)
                {
                    max.x = k.time;
                }
                if (k.value > max.y)
                {
                    max.y = k.value;
                }
            }
            curveMin = min;
            curveMax = max;
        }

        /// <summary>
        /// Spawns and destroys knobs as needed. for the animation curve
        /// </summary>
        public void ResetKnobs()
        {
            if (animCurve != null)
            {
                var keys = animCurve.keys;
                for (int i = 0; i < keys.Length; i++)
                {
                    var k = keys[i];
                    if (keyKnobs.Count <= i)
                    {
                        AddNewKnob();
                    }

                    SetKnobPosition(keyKnobs[i], k);
                }
                for (int i = keys.Length; i < keyKnobs.Count; i++)
                {
                    DestroyKnob(keyKnobs[i]);//.SetActive(false);
                }
                keyKnobs.RemoveRange(keys.Length, keyKnobs.Count - keys.Length);
            }
        }

        private void Update()
        {
            if (Time.time > updateRareLast + 1f / updateRareFps)
            {
                UpdateRare();
                updateRareLast = Time.time;
            }

            var curveChanged = false;

            if (HandleResizeAndMove())
            {
                curveChanged = true;
            }

            // add/remove keyframes here!!!
            Update_CheckForGrabPanelSurface();

            if (resolution != line.positionCount)
            {
                curveChanged = true;
            }

            if (keyKnobs.Any(k => k.isActiveAndEnabled && (k.isGrabbed || k.inTangent.isGrabbed || k.outTangent.isGrabbed)))
            {
                UpdateCurve();
                curveChanged = true;
            }

            if (curveChanged)
            {
                DrawCurve();
            }

            //if (gg1 != null)
            //{
            //    DebugInGame.DrawVisibleGizmo(gg1, transform.TransformPoint(CurveToLocal(new Vector2(1, 1))), -transform.forward);
            //    DebugInGame.DrawVisibleGizmo(gg2, transform.TransformPoint(CurveToLocal(new Vector2(0, 0))), -transform.forward);
            //}
            //else
            //{
            //    gg1 = DebugInGame.DrawVisibleGizmo(Color.red);
            //    gg2 = DebugInGame.DrawVisibleGizmo(Color.red);
            //}
        }

        //GameObject gg1, gg2;

        // UN GRAB
        private void ResizeHandleMax_OnUngrabEvent(ControllerWrapper obj)
        {
            // this is a hidden position. // the gizmo can be grabbed from there too but it is mainly grabbable from the surface of the animcurve
            resizeHandleMax.transform.localPosition = (Vector3)canvasMax + maxHandleInitOffset;
            curveMaxPivot = curveMax;

        }

        // UN GRAB
        private void ResizeHandleMin_OnUngrabEvent(ControllerWrapper obj)
        {
            // this is a hidden position. // the gizmo can be grabbed from there too but it is mainly grabbable from the surface of the animcurve
            resizeHandleMin.transform.localPosition = (Vector3)canvasMin + minHandleInitOffset;
            curveMinPivot = curveMin;

        }

        // GRAB
        private void ResizeHandleMax_OnGrabEvent(ControllerWrapper obj)
        {
            handleMaxOldPos = resizeHandleMax.transform.localPosition;

            // we MUST set max pivot here, to the new handle position BEFORE the move.
            // but only if other hand is grabbed...??
            if (resizeHandleMin.isGrabbed)
            {
                curveMaxPivot = CurveValueOfCanvasPoint(newCanvasMin, canvasMax, newCanvasMax);
            }
        }


        // GRAB
        private void ResizeHandleMin_OnGrabEvent(ControllerWrapper obj)
        {
            handleMinOldPos = resizeHandleMin.transform.localPosition;
            if (resizeHandleMax.isGrabbed)
            {
                curveMaxPivot = CurveValueOfCanvasPoint(canvasMin, newCanvasMax, newCanvasMin);
            }

        }


        private Vector3 newCanvasMax
        {
            get { return resizeHandleMax.transform.localPosition - maxHandleInitOffset; }
        }
        private Vector3 newCanvasMin
        {
            get { return resizeHandleMin.transform.localPosition - minHandleInitOffset; }
        }

        private bool HandleResizeAndMove()
        {
            var gMax = resizeHandleMax.isGrabbed;
            var gMin = resizeHandleMin.isGrabbed;
            if (gMax || gMin)
            {
                // canvasMax is not the handle position, but the handle position minus a little offset.
                var newCanvasMax = resizeHandleMax.transform.localPosition - maxHandleInitOffset;
                var newCanvasMin = resizeHandleMin.transform.localPosition - minHandleInitOffset;

                // if both grabbed, resize curve. otherwise just move it
                if (gMax && gMin)
                {
                    ResizeCurveToNewCanvasSizes(newCanvasMin, newCanvasMax);
                }
                else if (gMax)
                {
                    Vector2 delta = resizeHandleMax.transform.localPosition - handleMaxOldPos;
                    handleMaxOldPos = resizeHandleMax.transform.localPosition;
                    Vector2 scale = new Vector2((curveMax.x - curveMin.x) / (canvasMax.x - canvasMin.x), (curveMax.y - curveMin.y) / (canvasMax.x - canvasMin.x));
                    delta.Scale(scale);
                    curveMax -= delta;
                    curveMin -= delta;
                }
                else if (gMin)
                {
                    Vector2 delta = resizeHandleMin.transform.localPosition - handleMinOldPos;
                    handleMinOldPos = resizeHandleMin.transform.localPosition;
                    Vector2 scale = new Vector2((curveMax.x - curveMin.x) / (canvasMax.x - canvasMin.x), (curveMax.y - curveMin.y) / (canvasMax.x - canvasMin.x));
                    delta.Scale(scale);
                    curveMax -= delta;
                    curveMin -= delta;
                }

                if (animCurve.keys.Length == keyKnobs.Count)
                {
                    for (int i = 0; i < animCurve.keys.Length; i++)
                    {
                        SetKnobPosition(keyKnobs[i], animCurve.keys[i]);
                    }
                }
            }
            else if (!gMax && !gMin)
            {
                // none grabbed, do nothing.
            }

            return gMax || gMin;
        }

        private Vector2 CurveValueOfCanvasPoint(Vector3 newCanvasMin, Vector3 newCanvasMax, Vector3 canvasPos)
        {
            var cmX = InverseLerpUnclamped(newCanvasMin.x, newCanvasMax.x, canvasPos.x);
            var cmY = InverseLerpUnclamped(newCanvasMin.y, newCanvasMax.y, canvasPos.y);
            return new Vector2(
                Mathf.LerpUnclamped(curveMinPivot.x, curveMaxPivot.x, cmX),
                Mathf.LerpUnclamped(curveMinPivot.y, curveMaxPivot.y, cmY)
                );
        }

        private void ResizeCurveToNewCanvasSizes(Vector3 newCanvasMin, Vector3 newCanvasMax)
        {
            // if           newCanvasMax                corresponds to curveMaxPivot in curve coords
            // and:         newCanvasMin                corresponds to curveMinPivot in curve coords
            // what do      canvasMax and canvasMin     correspond to in curve coords?
            // lerpylerp newparam = canvasMax based on newCanvasMax
            // lerpylerp curve between 0 and 1 with new param.

            // works, but replaced with function
            //var cmX = InverseLerpUnclamped(newCanvasMin.x, newCanvasMax.x, canvasMax.x);
            //var cmY = InverseLerpUnclamped(newCanvasMin.y, newCanvasMax.y, canvasMax.y);
            //Vector2 newCurveMax;
            //newCurveMax.x = Mathf.LerpUnclamped(curveMinPivot.x, curveMaxPivot.x, cmX);
            //newCurveMax.y = Mathf.LerpUnclamped(curveMinPivot.y, curveMaxPivot.y, cmY);
            var newCurveMax = CurveValueOfCanvasPoint(newCanvasMin, newCanvasMax, canvasMax);

            //var ciX = InverseLerpUnclamped(newCanvasMin.x, newCanvasMax.x, canvasMin.x);
            //var ciY = InverseLerpUnclamped(newCanvasMin.y, newCanvasMax.y, canvasMin.y);
            //Vector2 newCurveMin;
            //newCurveMin.x = Mathf.LerpUnclamped(curveMinPivot.x, curveMaxPivot.x, ciX);
            //newCurveMin.y = Mathf.LerpUnclamped(curveMinPivot.y, curveMaxPivot.y, ciY);
            var newCurveMin = CurveValueOfCanvasPoint(newCanvasMin, newCanvasMax, canvasMin);

            curveMax = newCurveMax;
            curveMin = newCurveMin;
        }

        private float InverseLerpUnclamped(float a, float b, float value)
        {
            return (value - a) / (b - a);
        }

        private void Update_CheckForGrabPanelSurface()
        {
            for (int i = 0; i < controllers.Count; i++)
            {
                var c = controllers[i];

                ////////////////////////////////////////================ NOTE!!!! ///////////////////////////////////////////
                ////////////////////////////////////////================ NOTE!!!! ///////////////////////////////////////////
                ////////////////////////////////////////================ NOTE!!!! ///////////////////////////////////////////
                // IF THE GetPressDown() and GetPressUp() happen in the same frame, this obj will be grabbed forever!! until the next Grab/Ungrab that is not in the same frame.
                if (c.GetPressDownButton())
                {
                    Vector3 closestPointOnCurve;
                    var canSpawnKnob = CheckSpawnKnob(c.grabPoint, out closestPointOnCurve);
                    if (canSpawnKnob)
                    {
                        if (c.isGrabbingDummy)
                        {
                            c.Ungrab();
                        }
                        if (!c.isGrabbing && !c.isHighlighting)
                        {
                            // we should add a knob and auto grab it. also we should add a keyframe on the curve, to avoid problems with knob count.
                            var k = AddNewKnob();
                            var curvePos = LocalToCurve(transform.InverseTransformPoint(closestPointOnCurve));
                            SetKnobPosition(k, curvePos.x, curvePos.y);

                            c.Grab(k);

                            animCurve.AddKey(GetKeyframe(k));

                        }
                    }
                    else
                    {

                        // we are not close to the curve.
                        // check if we are close to the surface so we can grab it and  move/scale it
                        var cGrabPoint = c.grabPoint;
                        var grabPointOnPlane = transform.InverseTransformPoint(cGrabPoint);
                        if (grabPointOnPlane.z <= grabPanelZDistance)
                        {
                            grabPointOnPlane.z = 0;
                            // put the point in the panel and then check again for distance
                            var gpp = transform.TransformPoint(CurveToLocal(LocalToCurve(grabPointOnPlane)));
                            if ((gpp - cGrabPoint).sqrMagnitude < grabPanelZDistance)
                            {
                                if (c.isGrabbingDummy)
                                {
                                    c.Ungrab();
                                }
                                if (!c.isGrabbing && !c.isHighlighting)
                                {
                                    // get the corner gizmo and auto grab it, and put it where the grabPointOnPlane is
                                    var cornerGizmo = GetCornerHandle(c.controllerWrapper, gpp);
                                    if (cornerGizmo != null)
                                    {
                                        // swap if necessary
                                        var maxLp = resizeHandleMax.transform.localPosition;
                                        var minLp = resizeHandleMin.transform.localPosition;
                                        if (minLp.x > maxLp.x || minLp.y > maxLp.y)
                                        {
                                            // swap them? if they are opposite sides?
                                            Debug.Log("[anim curve control] We should swap?");
                                        }
                                        c.Grab(cornerGizmo);
                                    }
                                }
                            }
                        }

                    }
                }
            }
        }

        // returns a corner handle for moving/resizing and positions it at the grabPointOnPlane before moving it. Automatically sets it as max or min depending on position of the other gizmo (if present)
        private GrabbableBase GetCornerHandle(ControllerWrapper controllerWrapper, Vector3 grabPointOnPlaneWorld)
        {
            // resize handle min or max, whichever is closer (and available for grabbing)
            GrabbableBase target;
            if (resizeHandleMax.isGrabbed)
            {
                if (resizeHandleMin.isGrabbed)
                {
                    // none can be grabbed cause they are TAKEN :(
                    return null;
                }
                target = resizeHandleMin;
            }
            else if (resizeHandleMin.isGrabbed)
            {
                target = resizeHandleMax;
            }
            else
            {
                // none are grabbed. grab nearest
                var dist1 = resizeHandleMax.transform.position - grabPointOnPlaneWorld;
                var dist2 = resizeHandleMin.transform.position - grabPointOnPlaneWorld;
                if (dist1.sqrMagnitude > dist2.sqrMagnitude)
                {
                    // dist1 further than dist2. return dist2
                    target = resizeHandleMin;
                }
                else
                {
                    target = resizeHandleMax;
                }
            }

            target.transform.position = grabPointOnPlaneWorld;
            return target;
        }

        void UpdateRare()
        {
            // here we only give feedback about what we are about to select. we want the actual spawning to happen based on Update() funcs, where we can get the actual input that frame.

            // if controller lurking around the curve, and then CLICKS, instead of grabbing the anim panel, create a new keyframe on the curve at that location and grab that.
            for (int i = 0; i < controllers.Count; i++)
            {
                var c = controllers[i];

                // only do this if not highlighting something else.
                if (!c.isGrabbing && !c.isHighlighting)
                {
                    // check if we are close enough to the actual curve to spawn a knob
                    Vector3 closest;
                    var canSpawnKnob = CheckSpawnKnob(c.grabPoint, out closest);
                    if (canSpawnKnob)
                    {
                        //if (!gizmos.ContainsKey(c.controllerWrapper))
                        //{
                        //    gizmos[c.controllerWrapper] = DebugInGame.DrawVisibleGizmo(Colors.Orange);
                        //}
                        //DebugInGame.DrawVisibleGizmo(gizmos[c.controllerWrapper], c.grabPoint, closest - c.grabPoint);
                    }
                    else
                    {
                        // we are not close to the curve.
                        // check if we are close to the surface so we can grab it and  move/scale it
                        var cGrabPoint = c.grabPoint;
                        var grabPointOnPlane = transform.InverseTransformPoint(cGrabPoint);
                        if (grabPointOnPlane.z <= grabPanelZDistance)
                        {
                            grabPointOnPlane.z = 0;
                            // put the point in the panel and then check again for distance
                            var gpp = transform.TransformPoint(CurveToLocal(LocalToCurve(grabPointOnPlane)));
                            if ((gpp - cGrabPoint).sqrMagnitude < grabPanelZDistance)
                            {
                                //// WE ARE CLOSE TO THE PANEL!!! show some damn feedback
                                //if (!gizmos.ContainsKey(c.controllerWrapper))
                                //{
                                //    gizmos[c.controllerWrapper] = DebugInGame.DrawVisibleGizmo(Colors.Orange);
                                //}
                                //DebugInGame.DrawVisibleGizmo(gizmos[c.controllerWrapper], cGrabPoint, gpp - cGrabPoint);
                            }
                        }

                    }
                }
            }

        }

        /// <summary>
        /// Compares grabPoint to curve and returns if there is a point on th curve close enough to the grabPoint. That point is output in the closestPointOnCurve out param.
        /// </summary>
        /// <param name="grabPoint">grab point to compare with curve for finding the closest point</param>
        /// <param name="closestPointOnCurve"> Returned position vector, in world space, if a closest position to the curve was indeed found.</param>
        /// <returns>true if a point was found</returns>
        private bool CheckSpawnKnob(Vector3 grabPoint, out Vector3 closestPointOnCurve)
        {
            // find closest point for this controller
            Vector3 curvePoint = FindClosestPointOnCurve(grabPoint);
            var dist = (curvePoint - grabPoint).magnitude;

            // compare with threshold
            if (dist < newKnobDistance)
            {
                closestPointOnCurve = curvePoint;
                return true;
            }

            closestPointOnCurve = Vector3.zero;
            return false;
        }

        /// <summary>
        /// Returns closest point on curve, in world space. because of grabPoint being in worldspace too.
        /// </summary>
        /// <param name="grabPoint">grab point in world space</param>
        /// <returns>WORLD space position, a point on the curve closest to grabPoint</returns>
        private Vector3 FindClosestPointOnCurve(Vector3 grabPoint)
        {
            // sample curve around grab point... some double or half type shit. luckily it is surjective/conjective/whichever one is on the X axis
            var grabPointLocalPos = transform.InverseTransformPoint(grabPoint);

            var minDist = float.MaxValue;
            Vector3 minPos = Vector3.one * float.MaxValue;

            // first check if grab point is even in the same area. if not, return bluff
            if (Mathf.Abs(grabPointLocalPos.z) > grabPanelZDistance)
            {
                return minPos;
            }

            // find nearest point on curve, sample using draw resolution
            var grabPointOnCurve = LocalToCurve(grabPointLocalPos);
            for (int i = 0; i < resolution; i++)
            {
                var t = i / ((float)resolution - 1);
                var pos = new Vector2(t, animCurve.Evaluate(t));
                var dist = (grabPointOnCurve - pos).magnitude;
                if (dist < minDist)
                {
                    minDist = dist;
                    minPos = pos;
                }
            }

            var ctl = CurveToLocal(minPos);
            ctl.z = 0;
            return transform.TransformPoint(ctl);
        }

        /// <summary>
        /// Draws curve on line renderer, using resolution. Does not care about knobs, only about curve.
        /// </summary>
        public void DrawCurve()
        {
            if (line.positionCount != resolution)
            {
                line.positionCount = resolution;
            }
            var curve = animCurve;
            for (int i = 0; i < resolution; i++)
            {
                var t = i / ((float)resolution - 1);
                var tt = Mathf.Lerp(curveMin.x, curveMax.x, t);
                Vector3 posi = new Vector3(tt, curve.Evaluate(tt), 0f);
                posi = CurveToLocal(posi);
                line.SetPosition(i, posi);
            }

            line.GenerateMesh();
        }

        /// <summary>
        /// Returns Z = 0 when in range, Z = 0.05 when out of range of anim window. 
        /// Transform a vector from curve coordinates (time, value), into the local coords to draw on the makeshift animation curve editor.
        /// </summary>
        /// <param name="curvePos"></param>
        /// <returns></returns>
        private Vector3 CurveToLocal(Vector2 curvePos)
        {
            var cx = Mathf.InverseLerp(curveMin.x, curveMax.x, curvePos.x);
            var cy = Mathf.InverseLerp(curveMin.y, curveMax.y, curvePos.y);
            var x = Mathf.Lerp(canvasMin.x, canvasMax.x, cx);
            var y = Mathf.Lerp(canvasMin.y, canvasMax.y, cy);

            // place behind canvas when out of range heheheh
            var z = (curvePos.x >= curveMax.x || curvePos.x <= curveMin.x || curvePos.y >= curveMax.y || curvePos.y <= curveMin.y) ? localZWhenOutOfRange : 0f;
            return new Vector3(x, y, z);
        }

        /// <summary>
        /// Transform a vector from canvas coordinates (whatever local position on the in-game makeshift animation curve editor) into normalized 0..1 nice coords.
        /// </summary>
        /// <param name="canvasPos"></param>
        /// <returns></returns>
        private Vector2 LocalToCurve(Vector3 canvasPos)
        {
            var x = Mathf.InverseLerp(canvasMin.x, canvasMax.x, canvasPos.x);
            var y = Mathf.InverseLerp(canvasMin.y, canvasMax.y, canvasPos.y);
            var cx = Mathf.Lerp(curveMin.x, curveMax.x, x);
            var cy = Mathf.Lerp(curveMin.y, curveMax.y, y);
            return new Vector2(cx, cy);
        }

        /// <summary>
        /// 
        /// </summary>
        public void UpdateCurve()
        {
            if (animCurve != null)
            {
                // allocation!!! :(( refactor for performanceeeee
                var keys = animCurve.keys;
                if (keys.Length != keyKnobs.Count)
                {
                    ResetKnobs();
                }

                ReorderKeyKnobs();

                float prevTime = float.MinValue;
                for (int i = 0; i < keyKnobs.Count; i++)
                {
                    var knob = keyKnobs[i];

                    var keyframe = GetKeyframe(knob);

                    // if keyframe is not ordered properly (impossible since we just sorted), or is too close
                    var offset = 0.001f;
                    while (keyframe.time <= prevTime || IsTimeTooClose(keyframe.time, prevTime))
                    {
                        // offset the time a bit so it's not so close to the prev.
                        keyframe.time += offset;

                        // increase offset size in case we must repeat this a few times...
                        offset *= 1.2f;
                    }

                    prevTime = keyframe.time;

                    keys[i] = keyframe;
                }

                // keys array should be goodnow, add it again to animcurve. even though animcurve might want to remove some keyframes, this way we force it not to.
                animCurve.keys = keys;

            }
        }

        #region UpdateCurve helpers
        /// <summary>
        /// maybe the most fool proof option is to have a dummy animation curve where we test the times by actually adding them as keyframes and seeing if it works...... 
        /// fucking moronic solution but uses unity's weapons against itself. 
        /// </summary>
        private bool IsTimeTooClose(float time, float prevTime)
        {
            return Mathf.Abs(time - prevTime) < 0.001f;
        }

        private void DestroyKnob(GrabbableBase knob)
        {
            Debug.Log("About to destroy a fucknig knob");
            // should be replaced with pooling and shit
            Destroy(knob.gameObject);
        }

        // when animCurve reorders its keyframes
        private void ReorderKeyKnobs()
        {
            // List.Sort() IS AN UNSTABLE SORT!!! EQUAL ELEMENTS MIGHT BE REORDERED. WE DO NOT WANT THAT SHIT. Linq.OrderBy would be a stable sort, but it reassigns shit so we do not want that.
            //keyKnobs.Sort(KeyKnobComparer);
            Quicksort(keyKnobs, 0, keyKnobs.Count - 1);
        }

        public void Quicksort<T>(List<T> elements, int left, int right) where T : IHandleGrabbing
        {
            int i = left, j = right;
            int pivotIndex = (left + right) / 2;
            T pivot = elements[pivotIndex];

            while (i <= j)
            {
                while (KeyKnobComparer(elements[i], pivot, i, pivotIndex) < 0)
                {
                    i++;
                }

                while (KeyKnobComparer(elements[j], pivot, j, pivotIndex) > 0)
                {
                    j--;
                }

                if (i <= j)
                {
                    // Swap
                    T tmp = elements[i];
                    elements[i] = elements[j];
                    elements[j] = tmp;

                    i++;
                    j--;
                }
            }

            // Recursive calls
            if (left < j)
            {
                Quicksort(elements, left, j);
            }

            if (i < right)
            {
                Quicksort(elements, i, right);
            }
        }

        // for reordering
        private int KeyKnobComparer(IHandleGrabbing a, IHandleGrabbing b, int aindex, int bindex)
        {
            return a.transform.localPosition.x < b.transform.localPosition.x ? -1 : a.transform.localPosition.x > b.transform.localPosition.x ? 1 : aindex.CompareTo(bindex);
        }
        #endregion

        // gets a keyframe from a knob. uses local position and converts to curve pos.
        private Keyframe GetKeyframe(AnimationCurveKnob knob)
        {
            var pos = LocalToCurve(knob.transform.localPosition);
            var time = pos.x;
            var val = pos.y;

            return new Keyframe()
            {
                time = time,
                value = val,

                // we get the display angle from the tangent knob, so we must multiply by tangentRatio to get the real angle. (trial and error
                inTangent = knob.GetInTangent() * tangentRatio,
                outTangent = knob.GetOutTangent() * tangentRatio,
            };
        }

        // if the curve space is scaled nonuniformly, the tangent angles no longer reflect the slope but have to be multiplied by the nonuniform scale ratio.
        private float tangentRatio
        {
            get
            {
                // y/x is the nonuniform scaling ratio. curveMax.x - curveMin.x should always be not null.
                return (curveMax.y - curveMin.y) / (curveMax.x - curveMin.x);
            }
        }

        private void SetKnobPosition(AnimationCurveKnob knob, Keyframe keyframe)
        {
            SetKnobPosition(knob, keyframe.time, keyframe.value, keyframe.inTangent, keyframe.outTangent);
        }

        private void SetKnobPosition(AnimationCurveKnob knob, float time, float value)
        {
            var pos = CurveToLocal(new Vector2(time, value));
            knob.transform.localPosition = pos;
            // do not change tangents here if we didn't ask for it
        }

        private void SetKnobPosition(AnimationCurveKnob knob, float time, float value, float inTangent, float outTangent)
        {
            var pos = CurveToLocal(new Vector2(time, value));
            knob.transform.localPosition = pos;

            // we must divide by tangentRatio to get the display angle from the real angle
            knob.SetInTangent(inTangent / tangentRatio);
            knob.SetOutTangent(outTangent / tangentRatio);
        }

        // spawns a new knob and returns it for the ResetKnobs function
        private AnimationCurveKnob AddNewKnob()
        {
            var gb = Instantiate(knobPrefab, transform.position, transform.rotation, transform).GetComponent<AnimationCurveKnob>();
            keyKnobs.Add(gb);
            return gb;
        }
    }
}