﻿namespace VRTweakTool
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;
    using Random = UnityEngine.Random;

    public class Label : MonoBehaviour
    {
        public TweakTarget outputTarget;

        [SerializeField]
        private Text _text;
        public Text text
        {
            get
            {
                if (_text == null)
                {
                    _text = GetComponentInChildren<Text>();
                }
                return _text;
            }
        }

        private void Update()
        {
            text.text = outputTarget.GetValue().ToString();

        }
    }
}