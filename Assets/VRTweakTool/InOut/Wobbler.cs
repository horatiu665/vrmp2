﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.Events;

    public class Wobbler : GrabbableBase
    {
        [Header("Reflection params")]
        public TweakTarget tweakTarget;
        public FieldTypesForKnobs fieldType;

        public UnityEngine.UI.Text labelText;
        public UnityEngine.UI.Text valueText;

        [Header("Graphics setup")]
        // knob graphics shit
        [SerializeField]
        private Transform _knobGraphics;
        public Transform knobGraphics
        {
            get
            {
                // generate here
                if (_knobGraphics == null)
                {
                    _knobGraphics = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                    _knobGraphics.SetParent(transform);
                    _knobGraphics.localScale = Vector3.one * 0.05f;
                    _knobGraphics.localPosition = Vector3.zero;
                    _knobGraphics.localRotation = Quaternion.identity;
                }
                return _knobGraphics;
            }
        }

        public bool onlyWhenGrabbed = false;

        public float wobbliness = 0;

        private Vector3 oldKnobPos;
        private Vector3 knobPos
        {
            get
            {
                return knobGraphics.transform.position;
            }
        }
        public Vector3 deltaWobble;

        // must become invisible to user
        public AnimationCurve deltaPosToDeltaValue = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        public override void OnGrab(ControllerWrapper controllerWrapper)
        {
            base.OnGrab(controllerWrapper);
            oldKnobPos = knobPos;

            // for the shits, update label here. and on start.
            labelText.text = tweakTarget.GetLastWordInPath();
        }

        private void Start()
        {
            labelText.text = tweakTarget.GetLastWordInPath();
            valueText.text = tweakTarget.GetValue().ToString();
        }

        private void Update()
        {
            if (!onlyWhenGrabbed || (onlyWhenGrabbed && isGrabbed))
            {
                deltaWobble = knobPos - oldKnobPos;
                oldKnobPos = knobPos;

                var dd = deltaPosToDeltaValue.Evaluate(deltaWobble.magnitude);
                ApplyDelta(dd);
            }
        }

        private void ApplyDelta(float dd)
        {
            switch (fieldType)
            {
            case FieldTypesForKnobs.Float:
                ApplyDeltaFloat(dd);
                break;
            case FieldTypesForKnobs.Int:
                ApplyDeltaInt(dd);
                break;
            default:
                break;
            }
        }

        private void ApplyDeltaInt(float dd)
        {
            var value = (int)tweakTarget.GetValue();
            value += (int)dd;
            tweakTarget.SetValue(value);

            if (valueText != null)
            {
                valueText.text = value.ToString();
            }
        }

        private void ApplyDeltaFloat(float dd)
        {
            var value = (float)tweakTarget.GetValue();
            value += dd;
            tweakTarget.SetValue(value);

            if (valueText != null)
                valueText.text = value.ToString();
        }
    }
}
