﻿namespace VRTweakTool
{
    using System.Collections.Generic;
    using System.Collections;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    public class AnimationCurveKnobTangent : GrabbableBase
    {
        private GrabMaster.ControllerGrabberData c = null;

        public override void OnGrab(ControllerWrapper controllerWrapper)
        {
            base.OnGrab(controllerWrapper);
            c = master.GetControllerGrabberData(controllerWrapper);

        }

        public override void OnUngrab(ControllerWrapper controllerWrapper)
        {
            base.OnUngrab(controllerWrapper);
            c = null;
        }

        private void LateUpdate()
        {
            if (isGrabbed)
            {
                if (c != null)
                {
                    transform.localPosition = Vector3.zero;
                    var up = -transform.parent.forward;
                    var gp = c.grabPoint;
                    // in same plane
                    var gpOnPlane = transform.position + Vector3.ProjectOnPlane(gp - transform.position, up);
                    transform.LookAt(gpOnPlane, up);
                }
            }
        }

    }
}