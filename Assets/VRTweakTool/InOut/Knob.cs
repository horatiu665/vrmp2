﻿namespace VRTweakTool
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Text;
    using System.Text.RegularExpressions;
    using UnityEngine;
    using UnityEngine.Events;

    public class Knob : MonoBehaviour
    {
        [Header("Reflection params")]
        [SerializeField]
        private TweakTarget _tweakTarget;
        public TweakTarget tweakTarget
        {
            get
            {
                return _tweakTarget;
            }
            private set
            {
                _tweakTarget = value;
            }
        }
        public FieldTypesForKnobs fieldType;

        public UnityEngine.UI.Text labelText;
        public UnityEngine.UI.Text valueText;

        [Header("Graphics setup")]
        // knob graphics shit = this is what turns
        [SerializeField]
        private GrabbableBase _knobToTurn;
        public GrabbableBase knobToTurn
        {
            get
            {
                // generate here - this might be too fucked up to generate with cubes.
                if (_knobToTurn == null)
                {
                    var trans = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
                    trans.SetParent(transform);
                    trans.localScale = Vector3.one * 0.05f;
                    trans.localPosition = Vector3.zero;
                    trans.localRotation = Quaternion.identity;
                    _knobToTurn = trans.gameObject.AddComponent<GrabbableBase>();
                }
                return _knobToTurn;
            }
        }

        private float _oldAngle;
        public float knobAngle
        {
            get
            {
                return knobToTurn.transform.localEulerAngles.z;
            }
        }

        private ControllerWrapper controllerGrabbin;
        private Vector3 controllerPosOnKnobGrab;

        // must become invisible to user
        public AnimationCurve angleToDeltaMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 2, 0) } };
        public float angleDeltaMax = 180f;
        public float angleToDeltaValueMax = 1f;

        public AnimationCurve posToDeltaMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 2, 0) } };
        public float posToDeltaMax = 10f;
        public float posToDeltaValueMax = 1f;

        [Header("Global magnitude adapts to tweaking")]
        /// algorithm
        ///     when hand moves at first, we are tweaking values around 1-100.
        ///     but if we are moving hand slowly, we want to tweak values smaller closer to 1 than to 100.
        ///     therefore we must reduce the global tweak magnitude to be around 0.1-10.
        ///         then if we still move hand slowly, we must reduce even further, to 0.01-1,
        ///         and then even more to 0.001-0.1, 
        ///     until hand starts moving faster, which means we want to tweak values that are larger.
        public float globalTweakExponent = 1f;

        public Vector2 globalExponentRange = new Vector2(-7, 7);

        public float globalTweakBase = 3.5f;

        [Range(0, 1f)]
        public float globalTweakSmoothness = 0.03f;

        public float globalTweakParam = 0f;

        public float actualTweak;

        private void Start()
        {
            labelText.text = tweakTarget.GetLastWordInPath();
            //cannot do it on Start. breaks shit. maybe.
            valueText.text = tweakTarget.GetValue().ToString();

        }

        private void OnEnable()
        {
            knobToTurn.OnGrabEvent += KnobToTurn_OnGrabEvent;
            knobToTurn.OnUngrabEvent += KnobToTurn_OnUngrabEvent;
        }

        private void OnDisable()
        {
            knobToTurn.OnGrabEvent -= KnobToTurn_OnGrabEvent;
            knobToTurn.OnUngrabEvent -= KnobToTurn_OnUngrabEvent;
        }

        private void KnobToTurn_OnUngrabEvent(ControllerWrapper controller)
        {
        }

        private void KnobToTurn_OnGrabEvent(ControllerWrapper controller)
        {
            _oldAngle = knobAngle;
            // for the shits, update label here. and on start.
            labelText.text = tweakTarget.GetLastWordInPath();

            controllerGrabbin = controller;
            // inverse transform point from controller.pos into knob coords.
            controllerPosOnKnobGrab = transform.InverseTransformPoint(controller.transform.position);

            // reset exponent to one less magnitude than the current value. this way when grabbing, the value won't jump around so much.
            var val = (float)tweakTarget.GetValue();
            if (val > 0)
            {
                globalTweakExponent = Mathf.Clamp(Mathf.Log10(Mathf.Abs(val)) / 10f, globalExponentRange.x, globalExponentRange.y);
            }
        }

        private void Update()
        {
            if (knobToTurn.isGrabbed)
            {
                // TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO
                // use angleDelta and the new pos to rot thing to rotate the knob with the same increments of value, so value zero/init is always knob.rotation.init
                // TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO TODO

                var angleDelta = knobAngle - _oldAngle;
                while (angleDelta > 180)
                {
                    angleDelta -= 360;
                }
                while (angleDelta < -180)
                {
                    angleDelta += 360;
                }
                _oldAngle = knobAngle;

                // angleDeltaMax is in units per sec. Time.deltatime * angleDeltaMax is in units per frame.
                var angleToValue = angleToDeltaMapping.Evaluate(Mathf.Abs(angleDelta) / (angleDeltaMax * Time.deltaTime)) * angleToDeltaValueMax * Mathf.Sign(angleDelta);

                // TURN FAKELY AFTER APPLYING FIRST ROTATION, TO MAKE SURE JOINT ROTATES WITH PHYSICS, EVEN IF THIS SHIT DOESN'T WORK

                // turn knob by moving controller left-down/right-up
                // controllerDelta in knob coordinates
                var controllerDelta = transform.InverseTransformPoint(controllerGrabbin.transform.position) - controllerPosOnKnobGrab;
                controllerPosOnKnobGrab = transform.InverseTransformPoint(controllerGrabbin.transform.position);

                // postodeltamax is in units per second. multi by time.deltatime to find the units per frame
                controllerDelta = controllerDelta / (posToDeltaMax * Time.deltaTime);

                // turn positive X into positive rotation, positive Y same.
                var posToRot = (controllerDelta.x + controllerDelta.y);
                posToRot = posToDeltaMapping.Evaluate(Mathf.Abs(posToRot)) * posToDeltaValueMax * Mathf.Sign(posToRot);

                var le = knobToTurn.transform.localEulerAngles;
                le.z += posToRot;

                // neutralize joint pushes
                var j = JointGrabUtils.GetOrCreateJoint(controllerGrabbin.transform);
                var oldb = j.connectedBody;
                j.connectedBody = null;
                knobToTurn.transform.localEulerAngles = le;
                j.connectedBody = oldb;

                var newTweak = (angleToValue + posToRot);

                actualTweak = newTweak * Mathf.Pow(globalTweakBase, globalTweakExponent);
                ApplyDelta(actualTweak);

                // smoothly updates to the latest delta tweaked
                globalTweakParam = Mathf.Lerp(globalTweakParam, actualTweak, globalTweakSmoothness);
                if (globalTweakParam != 0)
                {
                    globalTweakExponent = Mathf.Clamp(Mathf.Lerp(globalTweakExponent, Mathf.Log10(Mathf.Abs(globalTweakParam)), globalTweakSmoothness), globalExponentRange.x, globalExponentRange.y);
                }
            }
        }

        private void ApplyDelta(float dd)
        {
            switch (fieldType)
            {
            case FieldTypesForKnobs.Float:
                ApplyDeltaFloat(dd);
                break;
            case FieldTypesForKnobs.Int:
                ApplyDeltaInt(dd);
                break;
            default:
                break;
            }
        }

        private void ApplyDeltaInt(float dd)
        {
            var value = (int)tweakTarget.GetValue();
            value += (int)dd;
            tweakTarget.SetValue(value);

            valueText.text = value.ToString();
        }

        private void ApplyDeltaFloat(float dd)
        {
            var value = (float)tweakTarget.GetValue();
            value += dd;
            tweakTarget.SetValue(value);

            valueText.text = value.ToString();
        }
    }
}
