﻿namespace VRTweakTool
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;
    using Random = UnityEngine.Random;

    public class LabelNumber : MonoBehaviour
    {
        public TweakTarget outputTarget;

        [SerializeField]
        private Text _text;
        public Text text
        {
            get
            {
                if (_text == null)
                {
                    _text = GetComponentInChildren<Text>();
                }
                return _text;
            }
        }

        [Header("Works for numeric types like float, int, etc.")]
        public string format = "F3";

        private void Update()
        {
            text.text = ((float)outputTarget.GetValue()).ToString(format);
        }
    }
}