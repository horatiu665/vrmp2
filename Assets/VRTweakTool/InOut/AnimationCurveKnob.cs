﻿namespace VRTweakTool
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    /// <summary>
    /// Animation curve knobs. They each have a couple of tangent controls. Knobs cannot simply be rotated, instead tangent controls must be used for tangents, because they can be more accurate
    /// Flow: when knob is released, tangents are auto activated. When anything else is grabbed (by the same controller!?), they are deactivated.
    /// </summary>
    public class AnimationCurveKnob : GrabbableBase
    {
        private AnimationCurveControl _acControl;
        public AnimationCurveControl acControl
        {
            get
            {
                if (_acControl == null)
                {
                    _acControl = GetComponentInParent<AnimationCurveControl>();
                }
                return _acControl;
            }
        }

        ControllerWrapper lastGrabController;

        [Header("Refs setup")]
        public AnimationCurveKnobTangent inTangent;
        public AnimationCurveKnobTangent outTangent;


        [SerializeField, ReadOnly]
        private bool _visibleTangents;
        public bool visibleTangents
        {
            get
            {
                return _visibleTangents;
            }
        }

        public bool tangentHiddenOnStart = false;

        protected override void OnEnable()
        {
            base.OnEnable();
            if (tangentHiddenOnStart)
            {
                TangentsVisibility(false);
            }
        }

        private void Update()
        {
            if (visibleTangents)
            {
                if (lastGrabController != null)
                {
                    // check controllers on master to get data about our last grab controller
                    for (int i = 0; i < master.controllers.Count; i++)
                    {
                        var c = master.controllers[i];
                        if (c.controllerWrapper == lastGrabController)
                        {
                            // if our last grabber is grabbing anything (including ourselves, but excluding the tangents), hide tangents and clear reference.
                            if (c.isGrabbing && (c.curGrabbed != (IHandleGrabbing)inTangent) && (c.curGrabbed != (IHandleGrabbing)outTangent))
                            {
                                TangentsVisibility(false);
                                lastGrabController = null;
                            }
                        }
                    }
                }
            }
        }

        private void LateUpdate()
        {
            LimitMovement();
        }

        private void LimitMovement()
        {
            var lp = transform.localPosition;
            lp.z = 0;
            lp.x = Mathf.Clamp(lp.x, acControl.canvasMin.x, acControl.canvasMax.x);
            lp.y = Mathf.Clamp(lp.y, acControl.canvasMin.y, acControl.canvasMax.y);
            transform.localPosition = lp;

            transform.localRotation = Quaternion.identity;
        }

        private void TangentsVisibility(bool visible)
        {
            _visibleTangents = visible;
            inTangent.gameObject.SetActive(visible);
            outTangent.gameObject.SetActive(visible);
        }

        public override void OnUngrab(ControllerWrapper controllerWrapper)
        {
            base.OnUngrab(controllerWrapper);
            TangentsVisibility(true);
            lastGrabController = controllerWrapper;

            acControl.UpdateCurve();
            acControl.DrawCurve();
        }

        public float GetInTangent()
        {
            var fw = inTangent.transform.forward;
            var signAngle = Vector3.SignedAngle(transform.right, fw, transform.forward);
            return Mathf.Tan(signAngle * Mathf.Deg2Rad);
        }

        public float GetOutTangent()
        {
            var fw = outTangent.transform.forward;
            var signAngle = Vector3.SignedAngle(transform.right, fw, transform.forward);
            return Mathf.Tan(signAngle * Mathf.Deg2Rad);
        }

        Transform tgHelper
        {
            get
            {
                return acControl.tgHelper;
            }
        }

        public void SetInTangent(float t)
        {
            var lea = tgHelper.localEulerAngles;
            lea.z = Mathf.Atan(t) * Mathf.Rad2Deg;
            tgHelper.localEulerAngles = lea;
            inTangent.transform.LookAt(inTangent.transform.position - tgHelper.right, -transform.forward);
        }

        public void SetOutTangent(float t)
        {
            var lea = tgHelper.localEulerAngles;
            lea.z = Mathf.Atan(t) * Mathf.Rad2Deg;
            tgHelper.localEulerAngles = lea;
            outTangent.transform.LookAt(outTangent.transform.position + tgHelper.right, -transform.forward);
        }

    }
}