﻿/* Copyright © 2014 Apex Software. All rights reserved. */
using Apex.Editor.Versioning;

[assembly: ApexProductAttribute("Apex Path", "2.4.2.1", ProductType.Product)]