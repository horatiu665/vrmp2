Shader "RomanticBendy" {
    Properties {
        _Color ("Color", Color) = (1,0.5951557,0.1397059,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _LightSteps ("LightSteps", Float ) = 5
        _ShadowLightSteps ("Shadow light steps", Float) = 2
        _MinLight ("MinLight", Float ) = 0.3
        _MaxLight ("MaxLight", Float ) = 1
		
		// These values are global now. Manipulated in RomanticShaderManager
		/*_FlatRadius ("Flat Radius", Float) = 3000
		_FalloffRadius ("Falloff", Float) = 1000
		_Offset ("Offset", Float) = 14*/
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityInstancing.cginc"
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            // INSTANCING
            #pragma multi_compile_instancing
            #pragma target 3.0
            uniform float _LightSteps;
            uniform float _ShadowLightSteps;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;

            uniform float _MinLight;
            uniform float _MaxLight;

			// global
			uniform float _FlatRadius, _FalloffRadius, _Offset;

            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID // INSTANCING
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
				LIGHTING_COORDS(2, 3)
				UNITY_FOG_COORDS(4)
                UNITY_VERTEX_INPUT_INSTANCE_ID // INSTANCING
			};

             // INSTANCING
            // UNITY_INSTANCING_CBUFFER_START(MyProperties)
            //     UNITY_DEFINE_INSTANCED_PROP(float4, _Color)
            // UNITY_INSTANCING_CBUFFER_END
           
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
				
                UNITY_SETUP_INSTANCE_ID(v);
                UNITY_TRANSFER_INSTANCE_ID(v, o); // necessary only if you want to access instanced properties in the fragment Shader.

				// set to clip pos
				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float cameraToObject = distance(_WorldSpaceCameraPos, worldPos);
                cameraToObject = clamp((cameraToObject - _FlatRadius), 0.0, 99999999);
                float dist = cameraToObject / _FalloffRadius;
                worldPos -= float4(0, dist * dist * _Offset, 0, 0);
                float4 localOffsetPos = mul(unity_WorldToObject, worldPos);
                o.pos = UnityObjectToClipPos(localOffsetPos);
                 
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                UNITY_SETUP_INSTANCE_ID(i); // necessary only if any instanced properties are going to be accessed in the fragment Shader.
                i.normalDir = normalize(i.normalDir);
				float3 normalDirection = i.normalDir;
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
				float lightIntensity = 
                    (
                        _MinLight + 
                        ( 
                            (
                                clamp(
                                    round(
                                        (
                                            dot(normalDirection,lightDirection)*0.5+0.5
                                        )
                                        //* round(attenuation)
                                        * _LightSteps
                                    )
                                    - (1 - round(attenuation)) * _ShadowLightSteps
                                    , 0, _LightSteps
                                )
                            )
                            / _LightSteps
                            * (_MaxLight - _MinLight) 
                        )
                    );
                
                //float4 color = UNITY_ACCESS_INSTANCED_PROP(_Color);
                float4 color = _Color;
                float3 emissive = _MainTex_var.rgb * color * lightIntensity;
                float3 finalColor = emissive;

				fixed4 finalRGBA = fixed4(finalColor, 1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
