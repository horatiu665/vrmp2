// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// hacked based on the standard shader so it can receive ambient light
// HACKED AGAIN SO IT DOESN'T  because that looked like shit

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "RomanticBendySnow" {
    Properties {
		_BrightColor ("Bright Color", Color) = (1,0.5951557,0.1397059,1)
        _DarkColor ("Dark Color", Color) = (0.5, 0.35, 0.07, 1)

		_MainTex ("MainTex", 2D) = "white" {}
        _LightSteps ("LightSteps", Float ) = 5
        _ShadowLightSteps ("Shadow light steps", Float) = 3
        _MinLight ("MinLight", Float ) = 0.3
        _MaxLight ("MaxLight", Float ) = 1

        _FlatLightSteps ("Flat Light Steps", Float) = 2
        _FlatColorLimit ("Flat Color Limit", Float) = 0.95
		// These values are global now. Manipulated in RomanticShaderManager
		/*_FlatRadius ("Flat Radius", Float) = 3000
		_FalloffRadius ("Falloff", Float) = 1000
		_Offset ("Offset", Float) = 14*/

    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            CULL OFF
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            //#pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _LightSteps;
            uniform float _ShadowLightSteps;
            uniform float4 _BrightColor;
            uniform float4 _DarkColor;
            uniform float4 _FlatSnowColor, _FlatSnowShadow;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _MinLight;
            uniform float _MaxLight;
            uniform float _FlatLightSteps;
            uniform float _FlatColorLimit;

			// global
			uniform float _FlatRadius, _FalloffRadius, _Offset;

            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
				LIGHTING_COORDS(2, 3)
				UNITY_FOG_COORDS(4)
			};
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
				
				// set to clip pos
				float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float cameraToObject = distance(_WorldSpaceCameraPos, worldPos);
                cameraToObject = clamp((cameraToObject - _FlatRadius), 0.0, 99999999);
                float dist = cameraToObject / _FalloffRadius;
                worldPos -= float4(0, dist * dist * _Offset, 0, 0);
                float4 localOffsetPos = mul(unity_WorldToObject, worldPos);
                o.pos = UnityObjectToClipPos(localOffsetPos);

                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
				float3 normalDirection = i.normalDir;
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);

                // 0 to 1, if the surface is horizontal.
                float horizontality = dot(normalDirection, float3(0,1,0)) * 0.5 + 0.5;
                // =0 if smaller than _flatcolorlimit, 1 if bigger.
                horizontality = ceil(horizontality - _FlatColorLimit);
                
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i); // 0 to 1. amount of projected shadow. round(attenuation) is 0 for shadow or 1 for no shadow
                float3 horizontalColor = lerp(_FlatSnowShadow, _FlatSnowColor, round(attenuation));
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
				float lightIntensity = 
					(
						_MinLight + 
						( 
							(
                                clamp(
    								round(
    									(
                                            dot(normalDirection,lightDirection)*0.5+0.5
                                        )
    									//* round(attenuation)
    									* _LightSteps
    								)
                                    - (1 - round(attenuation)) * _ShadowLightSteps
                                    - (1 - horizontality) * _FlatLightSteps
                                    , 0, _LightSteps
                                )
                            )
                            / _LightSteps
							* (_MaxLight - _MinLight) 
						)
					);
				
				float3 emissive = _MainTex_var.rgb * lerp(_DarkColor, _BrightColor, lightIntensity);
				float3 finalColor = emissive;

				fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
