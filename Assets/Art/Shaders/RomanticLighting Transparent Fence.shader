// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// hacked based on the standard shader so it can receive ambient light
// HACKED AGAIN SO IT DOESN'T  because that looked like shit

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Shader created with Shader Forge v1.30 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.30;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:9361,x:33240,y:32700,varname:node_9361,prsc:2|normal-528-OUT,emission-2348-OUT;n:type:ShaderForge.SFN_Round,id:3999,x:31998,y:32880,varname:node_3999,prsc:2|IN-9587-OUT;n:type:ShaderForge.SFN_Multiply,id:9587,x:31824,y:32880,varname:node_9587,prsc:2|A-2588-OUT,B-2325-OUT;n:type:ShaderForge.SFN_Divide,id:1516,x:32204,y:32880,varname:node_1516,prsc:2|A-3999-OUT,B-2325-OUT;n:type:ShaderForge.SFN_Dot,id:1060,x:31261,y:32754,varname:node_1060,prsc:2,dt:0|A-606-OUT,B-3368-OUT;n:type:ShaderForge.SFN_ValueProperty,id:589,x:31824,y:33126,ptovrint:False,ptlb:LightSteps,ptin:_LightSteps,varname:node_589,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:5;n:type:ShaderForge.SFN_Color,id:8257,x:32180,y:32622,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_5181,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0.5951557,c3:0.1397059,c4:1;n:type:ShaderForge.SFN_Tex2d,id:7873,x:32180,y:32445,ptovrint:False,ptlb:MainTex,ptin:_MainTex,varname:node_6626,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Multiply,id:64,x:32495,y:32605,varname:node_64,prsc:2|A-7873-RGB,B-8257-RGB;n:type:ShaderForge.SFN_Multiply,id:2348,x:32934,y:32790,varname:node_2348,prsc:2|A-64-OUT,B-4253-OUT;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:4253,x:32489,y:32881,varname:node_4253,prsc:2|IN-1516-OUT,IMIN-1265-OUT,IMAX-8618-OUT,OMIN-7485-OUT,OMAX-4621-OUT;n:type:ShaderForge.SFN_Vector1,id:1265,x:32489,y:33016,varname:node_1265,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:8618,x:32489,y:33069,varname:node_8618,prsc:2,v1:1;n:type:ShaderForge.SFN_ValueProperty,id:7485,x:32489,y:33155,ptovrint:False,ptlb:MinLight,ptin:_MinLight,varname:node_7485,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_ValueProperty,id:4621,x:32489,y:33240,ptovrint:False,ptlb:MaxLight,ptin:_MaxLight,varname:node_4621,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_RemapRange,id:6340,x:31450,y:32769,varname:node_6340,prsc:2,frmn:1,frmx:-1,tomn:1,tomx:0|IN-1060-OUT;n:type:ShaderForge.SFN_Relay,id:2325,x:32027,y:33114,varname:node_2325,prsc:2|IN-589-OUT;n:type:ShaderForge.SFN_LightVector,id:3368,x:31047,y:32823,varname:node_3368,prsc:2;n:type:ShaderForge.SFN_NormalVector,id:606,x:31047,y:32648,prsc:2,pt:True;n:type:ShaderForge.SFN_Round,id:5971,x:31442,y:32975,varname:node_5971,prsc:2|IN-2044-OUT;n:type:ShaderForge.SFN_Multiply,id:2588,x:31632,y:32853,varname:node_2588,prsc:2|A-6340-OUT,B-5971-OUT;n:type:ShaderForge.SFN_Tex2d,id:6619,x:32512,y:32305,ptovrint:False,ptlb:NormalMap,ptin:_NormalMap,varname:node_6619,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:3,isnm:True;n:type:ShaderForge.SFN_Multiply,id:2754,x:32905,y:32305,varname:node_2754,prsc:2|A-915-OUT,B-1920-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1920,x:32711,y:32501,ptovrint:False,ptlb:Normal,ptin:_Normal,varname:node_1920,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_ComponentMask,id:915,x:32711,y:32305,varname:node_915,prsc:2,cc1:0,cc2:1,cc3:-1,cc4:-1|IN-6619-RGB;n:type:ShaderForge.SFN_Append,id:528,x:32905,y:32464,varname:node_528,prsc:2|A-2754-OUT,B-6619-B;n:type:ShaderForge.SFN_LightAttenuation,id:2044,x:31271,y:32975,varname:node_2044,prsc:2;proporder:8257-7873-6619-1920-589-7485-4621;pass:END;sub:END;*/

Shader "RomanticLighting Transparent Fence" {
    Properties {
        _Color ("Color", Color) = (1,0.5951557,0.1397059,1)
        _MainTex ("MainTex", 2D) = "white" {}
        _LightSteps ("LightSteps", Float ) = 5
        _MinLight ("MinLight", Float ) = 0.3
        _MaxLight ("MaxLight", Float ) = 1
		//_AmbienteAmount ("Ambient Amount", Float ) = 0.75
			_Cutoff ("Cutoff", Float) = 1
    }
    SubShader {
        Tags {
			"Queue" = "Transparent"
			"IgnoreProjector" = "False"
			"RenderType" = "TransparentCutout"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
			Blend SrcAlpha OneMinusSrcAlpha
			//Cull Off

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float _LightSteps;
            uniform float4 _Color;
            uniform sampler2D _MainTex; uniform float4 _MainTex_ST;
            uniform float _MinLight;
            uniform float _MaxLight;
            uniform float _Cutoff;

			//uniform float _AmbienteAmount;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
				float4 color : COLOR;
				float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
				LIGHTING_COORDS(5, 6)
					UNITY_FOG_COORDS(7)
					//float3 ambientShit : TEXCOORD8;
					float4 color : COLOR;
			};
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
				o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                o.pos = UnityObjectToClipPos(v.vertex );
				//o.ambientShit = ShadeSH9(half4(o.normalDir, 1));
                UNITY_TRANSFER_FOG(o,o.pos);
				TRANSFER_VERTEX_TO_FRAGMENT(o)
				o.color = v.color;
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
				float3 normalDirection = i.normalDir;
				float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float4 _MainTex_var = tex2D(_MainTex,TRANSFORM_TEX(i.uv0, _MainTex));
				float3 color = (_MainTex_var.rgb*_Color.rgb);
                float3 emissive = 
					color 
					* (_MinLight 
						+ ( 
							(round(
									((
										(dot(normalDirection,lightDirection)*0.5+0.5)
										* round(attenuation)) * _LightSteps)
									)
								/ _LightSteps)
							* (_MaxLight - _MinLight) 
						)
					);
				float3 finalColor = emissive * i.color;
                //finalColor *= i.ambientShit * _AmbienteAmount + (1 - _AmbienteAmount);

				fixed4 finalRGBA = fixed4(finalColor*_MainTex_var.a, _MainTex_var.a * i.color.a);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
              
				clip(finalRGBA.a - _Cutoff);
				
				return finalRGBA;
				//return float4(/*length(i.normalDir) - */length(i.bitangentDir), 0, 0, 1);
            }
            ENDCG
        }
    }
    FallBack "Transparent/Cutout/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
