﻿Shader "Skybox/Skybox On Cube" {
	Properties{
		_Cube("Environment Map", Cube) = "white" {}
		_OffsetY("Y Offset", Float) = 0
		_Rotation("Rotation", Range(0,360)) = 0
	}

	SubShader{
		Tags{ "Queue" = "Geometry" }

		Pass{
			ZWrite Off
			Cull Off

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"
			
			// User-specified uniforms
			samplerCUBE _Cube;
			float _OffsetY;
			float _Rotation;

			struct vertexInput {
				float4 vertex : POSITION;
			};
			struct vertexOutput {
				float4 pos : SV_POSITION;
				float3 viewDir : TEXCOORD1;
			};

			vertexOutput vert(vertexInput input)
			{
				vertexOutput output;

				float4x4 modelMatrix = unity_ObjectToWorld;

				//		Y Rotation matrix
				float deg2rad = 3.14159265359 / 180;
				float4x4 yRotMatrix = {
					{ cos(-_Rotation * deg2rad), 0, sin(-_Rotation * deg2rad), 0 },
					{ 0, 1, 0, 0 },
					{ -sin(-_Rotation * deg2rad), 0, cos(-_Rotation * deg2rad), 0 },
					{ 0, 0, 0, 1 }
				};

				float3 viewDir = mul(modelMatrix, input.vertex).xyz
					- _WorldSpaceCameraPos;

				output.viewDir = mul(yRotMatrix, viewDir + float4(0, _OffsetY, 0, 1));
				output.pos = UnityObjectToClipPos(input.vertex);

				return output;
			}

			float4 frag(vertexOutput input) : COLOR
			{
				return texCUBE(_Cube, input.viewDir);
			}

			ENDCG
		}
	}
}