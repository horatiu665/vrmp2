// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_World2Object' with 'unity_WorldToObject'

// Shader created with Shader Forge v1.26 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.26;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-7605-OUT,voffset-6832-OUT;n:type:ShaderForge.SFN_Tex2d,id:3830,x:32115,y:32379,ptovrint:False,ptlb:Palette Texture,ptin:_PaletteTexture,varname:node_3830,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:4236,x:31608,y:32802,varname:node_4236,prsc:2,uv:1;n:type:ShaderForge.SFN_Time,id:5493,x:30781,y:32857,varname:node_5493,prsc:2;n:type:ShaderForge.SFN_Sin,id:6080,x:31099,y:32870,varname:node_6080,prsc:2|IN-5493-T;n:type:ShaderForge.SFN_Vector3,id:2217,x:31897,y:32756,varname:node_2217,prsc:2,v1:0,v2:1,v3:0;n:type:ShaderForge.SFN_Multiply,id:5793,x:31873,y:32963,varname:node_5793,prsc:2|A-4236-V,B-2663-OUT;n:type:ShaderForge.SFN_Multiply,id:7781,x:32081,y:33034,varname:node_7781,prsc:2|A-1393-XYZ,B-5793-OUT;n:type:ShaderForge.SFN_Abs,id:8161,x:31287,y:32870,varname:node_8161,prsc:2|IN-6080-OUT;n:type:ShaderForge.SFN_ValueProperty,id:6826,x:31899,y:33228,ptovrint:False,ptlb:Movement Strength,ptin:_MovementStrength,varname:node_6826,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Multiply,id:6832,x:32182,y:33183,varname:node_6832,prsc:2|A-6826-OUT,B-7781-OUT;n:type:ShaderForge.SFN_Sin,id:8597,x:31099,y:33020,varname:node_8597,prsc:2|IN-5493-TDB;n:type:ShaderForge.SFN_Abs,id:1733,x:31287,y:33050,varname:node_1733,prsc:2|IN-8597-OUT;n:type:ShaderForge.SFN_Add,id:2084,x:31484,y:33015,varname:node_2084,prsc:2|A-8161-OUT,B-1733-OUT,C-6287-OUT;n:type:ShaderForge.SFN_Sin,id:6287,x:31084,y:33179,varname:node_6287,prsc:2|IN-1230-OUT;n:type:ShaderForge.SFN_RemapRange,id:2663,x:31642,y:33126,varname:node_2663,prsc:2,frmn:0,frmx:2,tomn:0,tomx:1|IN-2084-OUT;n:type:ShaderForge.SFN_Add,id:1230,x:30843,y:33179,varname:node_1230,prsc:2|A-5493-T,B-8734-OUT;n:type:ShaderForge.SFN_Divide,id:8734,x:30650,y:33267,varname:node_8734,prsc:2|A-5493-TTR,B-5057-OUT;n:type:ShaderForge.SFN_ValueProperty,id:5057,x:30509,y:33402,ptovrint:False,ptlb:Time Divisor,ptin:_TimeDivisor,varname:node_5057,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:6;n:type:ShaderForge.SFN_Add,id:9477,x:31133,y:32546,varname:node_9477,prsc:2|A-7935-OUT,B-5493-T;n:type:ShaderForge.SFN_Vector1,id:7935,x:30874,y:32524,varname:node_7935,prsc:2,v1:506;n:type:ShaderForge.SFN_Sin,id:9165,x:31368,y:32546,varname:node_9165,prsc:2|IN-9477-OUT;n:type:ShaderForge.SFN_Abs,id:5788,x:31626,y:32538,varname:node_5788,prsc:2|IN-9165-OUT;n:type:ShaderForge.SFN_Multiply,id:7605,x:32390,y:32565,varname:node_7605,prsc:2|A-3830-RGB,B-6891-OUT;n:type:ShaderForge.SFN_RemapRange,id:6891,x:31943,y:32536,varname:node_6891,prsc:2,frmn:0,frmx:1,tomn:1,tomx:1.1|IN-2827-OUT;n:type:ShaderForge.SFN_Add,id:2827,x:31780,y:32389,varname:node_2827,prsc:2|A-8597-OUT,B-5788-OUT;n:type:ShaderForge.SFN_Transform,id:1393,x:32081,y:32818,varname:node_1393,prsc:2,tffrom:0,tfto:1|IN-2217-OUT;proporder:3830-6826-5057;pass:END;sub:END;*/

Shader "Skijump/Fire" {
    Properties {
        _PaletteTexture ("Palette Texture", 2D) = "white" {}
        _MovementStrength ("Movement Strength", Float ) = 1
        _TimeDivisor ("Time Divisor", Float ) = 6
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform sampler2D _PaletteTexture; uniform float4 _PaletteTexture_ST;
            uniform float _MovementStrength;
            uniform float _TimeDivisor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float2 uv1 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.uv1 = v.texcoord1;
                float4 node_5493 = _Time + _TimeEditor;
                float node_8597 = sin(node_5493.b);
                v.vertex.xyz += (_MovementStrength*(mul( unity_WorldToObject, float4(float3(0,1,0),0) ).xyz.rgb*(o.uv1.g*((abs(sin(node_5493.g))+abs(node_8597)+sin((node_5493.g+(node_5493.a/_TimeDivisor))))*0.5+0.0))));
                o.pos = UnityObjectToClipPos(v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _PaletteTexture_var = tex2D(_PaletteTexture,TRANSFORM_TEX(i.uv0, _PaletteTexture));
                float4 node_5493 = _Time + _TimeEditor;
                float node_8597 = sin(node_5493.b);
                float3 emissive = (_PaletteTexture_var.rgb*((node_8597+abs(sin((506.0+node_5493.g))))*0.1+1.0));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma exclude_renderers gles3 metal d3d11_9x xbox360 xboxone ps3 ps4 psp2 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            uniform float _MovementStrength;
            uniform float _TimeDivisor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord1 : TEXCOORD1;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv1 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv1 = v.texcoord1;
                float4 node_5493 = _Time + _TimeEditor;
                float node_8597 = sin(node_5493.b);
                v.vertex.xyz += (_MovementStrength*(mul( unity_WorldToObject, float4(float3(0,1,0),0) ).xyz.rgb*(o.uv1.g*((abs(sin(node_5493.g))+abs(node_8597)+sin((node_5493.g+(node_5493.a/_TimeDivisor))))*0.5+0.0))));
                o.pos = UnityObjectToClipPos(v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    //CustomEditor "ShaderForgeMaterialInspector"
}
