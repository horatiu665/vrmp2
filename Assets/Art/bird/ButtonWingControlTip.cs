﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ButtonWingControlTip : MonoBehaviour
{
    [SerializeField]
    private ShoulderForwardController _sfc;
    public ShoulderForwardController sfc
    {
        get
        {
            if (_sfc == null)
            {
                _sfc = GetComponent<ShoulderForwardController>();
            }
            return _sfc;
        }
    }

    public Transform toRotate;
    Quaternion   initRot;
    public Transform clickedValue;

    public bool left;
    public InputVR.ButtonMask axis;

    private IEnumerator Start()
    {
        yield return 0;
        toRotate = sfc.lastFeather;
        initRot = toRotate.localRotation;
    }

    void Update()
    {
        if (toRotate != null)
        {
            var l = InputVR.GetAxis(left, axis).x;
            toRotate.localRotation = Quaternion.Slerp(initRot, clickedValue.localRotation, l);
        }
    }
}