﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ShoulderForwardController : MonoBehaviour
{
    [SerializeField]
    private ShoulderFeathers _sf;
    public ShoulderFeathers sf
    {
        get
        {
            if (_sf == null)
            {
                _sf = GetComponent<ShoulderFeathers>();
            }
            return _sf;
        }
    }

    [SerializeField]
    private ShoulderFeathers _childSF;
    public ShoulderFeathers childSF
    {
        get
        {
            if (_childSF == null)
            {
                _childSF = transform.GetChild(0).GetComponent<ShoulderFeathers>();
            }
            return _childSF;
        }
    }

    List<Transform> feathers = new List<Transform>();

    public Transform firstFeather, lastFeather;

    public AnimationCurve rotCurve = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    private void Start()
    {
        feathers = sf.spawned.Select(g => g.transform).ToList();
        if (feathers.Count > 0)
        {
            firstFeather = feathers[0];
            feathers.RemoveAt(0);
        }
        if (childSF != null)
        {
            if (childSF.spawned.Count > 0)
            {
                lastFeather = childSF.spawned[0].transform;
            }
        }
        else
        {
            lastFeather = feathers.Last();
            feathers.RemoveAt(feathers.Count - 1);

        }
    }

    void Update()
    {
        for (int i = 0; i < feathers.Count; i++)
        {
            var f = feathers[i];
            var t = i / (float)(feathers.Count);
            Quaternion rot = Quaternion.Lerp(firstFeather.rotation, lastFeather.rotation, rotCurve.Evaluate(t));
            f.transform.rotation = rot;
        }
    }
}