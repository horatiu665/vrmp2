﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ButtonWingControl : MonoBehaviour
{
    public Transform toRotate;
    Quaternion initRot;
    public Transform clickedValue;

    public bool left;
    public InputVR.ButtonMask axis;

    private void Awake()
    {
        initRot = toRotate.localRotation;
    }

    void Update()
    {
        var l = InputVR.GetAxis(left, axis).x;
        toRotate.localRotation = Quaternion.Slerp(initRot, clickedValue.localRotation, l);
    }
}