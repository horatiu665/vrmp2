﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class ShoulderFeathers : MonoBehaviour
{
    public GameObject prefab;
    public float dist = 0.1f;
    public int count = 4;
    public Vector3 dir = new Vector3(1, 0, 0);
    public bool spawnButton;
    public bool clearButton;
    public List<GameObject> spawned = new List<GameObject>();
    public AnimationCurve scale = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
    public float scaleMulti = 1f;

    public Vector3 rotationOffset = new Vector3(0, 180, 0);

    private void Awake()
    {
        Spawn();
    }

    [DebugButton]
    private void Clear()
    {
        for (int i = 0; i < spawned.Count; i++)
        {
            if (spawned[i] != null)
            {
                this.DestroySafe(spawned[i].gameObject);
            }
        }
        spawned.Clear();
    }

    [DebugButton]
    private void Spawn()
    {
        Clear();
        for (int i = 0; i < count; i++)
        {
            var t = i / (float)count;
            var pp = Instantiate(prefab, transform.position + transform.TransformDirection(dir) * dist * t, transform.rotation, transform);
            pp.transform.localScale = scale.Evaluate(t) * Vector3.one * scaleMulti;
            pp.transform.Rotate(rotationOffset);
            spawned.Add(pp.gameObject);
        }
    }


}