using Slope;
using UnityEngine;
using VRCore;

public class SlopePrefabProvider : SingletonMonoBehaviour<SlopePrefabProvider>
{
    [SerializeField]
    private GameObject newSlopePrefab = null;

    public bool spawnOnStart = true;

    private void Start()
    {
        if (spawnOnStart)
        {
            Instantiate(newSlopePrefab);
        }
    }

    public SlopeManager GetNewSlope(SlopeManager oldSlope, Node startNode)
    {
        var newSlope = GetNewSlope(oldSlope, startNode.transform.position, startNode.transform.rotation);

        newSlope.startNode = startNode;

        return newSlope;
    }

    public SlopeManager GetNewSlope(SlopeManager oldSlope, Vector3 position, Quaternion rotation)
    {
        var newSlope = Instantiate(newSlopePrefab).GetComponent<SlopeManager>();
        newSlope.transform.position = position;
        newSlope.transform.rotation = rotation;

        // randomize new slope parameters here
        /// blablabla

        return newSlope;
    }
}