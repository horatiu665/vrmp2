using UnityEngine;
using System.Collections;
using Core;
using Slope;
using VRCore;

public class InfiniteSlopeTrigger : MonoBehaviour
{
    public event System.Action TriggerEnter;

    private bool _youOnlyCreateOnce = false;

    private SlopeManager _slope;

    public SlopeManager slope
    {
        get
        {
            if (_slope == null)
            {
                _slope = GetComponentInParent<SlopeManager>();
            }
            return _slope;
        }
    }

    private SlopeManager newSlopeInstance;

    public int nextSlopeRandomSeed;

    public void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            if (other.attachedRigidbody.GetComponent<IPlayer>() != null)
            {
                // spawn slope now! we found the player nearby
                if (TriggerEnter != null)
                {
                    TriggerEnter();
                }
                Destroy(this);
                InfiniteSlopeSpawner_TriggerEnter();
            }
        }
    }


    private void InfiniteSlopeSpawner_TriggerEnter()
    {
        // trigger was hit, time to make a level if we haven't already
        if (!_youOnlyCreateOnce)
        {
            _youOnlyCreateOnce = true;
            SpawnNewSlope().GenerateAll();
        }
    }

    public void ClearNewSlope()
    {
        if (newSlopeInstance != null)
        {
            newSlopeInstance.ClearAll();
            if (Application.isEditor)
            {
                DestroyImmediate(newSlopeInstance.gameObject);
            }
            else
            {
                Destroy(newSlopeInstance.gameObject);
            }

            newSlopeInstance = null;
        }
    }

    private SlopeManager SpawnNewSlope()
    {
        ClearNewSlope();
        newSlopeInstance = SlopePrefabProvider.instance.GetNewSlope(slope, slope.goalNode);
        newSlopeInstance.SetSeedByPrevSlope(nextSlopeRandomSeed);
        return newSlopeInstance;
    }

    [DebugButton]
    private void SpawnNewSlopeManually()
    {
        SpawnNewSlope().GenerateAll();
    }

    [DebugButton]
    private void ClearSpawnedSlope()
    {
        ClearNewSlope();
    }

    private void OnEnable()
    {
        _slope = GetComponentInParent<SlopeManager>();
    }
}
