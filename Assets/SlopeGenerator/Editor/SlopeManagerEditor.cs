namespace Slope.Editor
{
    using System.IO;
    using System.Linq;
    using UnityEditor;
    using UnityEditorInternal;
    using UnityEngine;
    using VRCore.Editor;

    [CustomEditor(typeof(SlopeManager))]
    public sealed class SlopeManagerEditor : Editor
    {
        private SerializedProperty _stringSeed;
        private SerializedProperty _keepSeed;
        private SerializedProperty _currentSeed;
        private SerializedProperty _nodes;

        private MonoScript _script;
        private SlopeManager _manager;
        private ReorderableList filterDisplayList;

        private void OnEnable()
        {
            _stringSeed = this.FindProperty("_stringSeed");
            _keepSeed = this.FindProperty("_keepSeed");
            _currentSeed = this.FindProperty("_currentSeed");

            _nodes = this.FindProperty("_nodes");

            _manager = this.target as SlopeManager;
            _script = MonoScript.FromMonoBehaviour(_manager);

            // reorder list on start.
            InitFilterList();
        }

        private void InitFilterList()
        {
            // init reorderable list with ref to serialized object and its field. true true represents that the list is reorderable, and false false that we don't want the +- buttons at the bottom
            filterDisplayList = new ReorderableList(_manager.filters, typeof(ISlopeFilter), true, true, false, false);

            filterDisplayList.headerHeight = 36;
            filterDisplayList.drawHeaderCallback = DrawFilterListHeaderCallback;
            filterDisplayList.drawElementCallback = DrawFilterListElementCallback;
            filterDisplayList.onReorderCallback = ReorderFilterListCallback;
        }

        /// <summary>
        /// Reordering the list manually triggers a recalculation of the .order parameters in the filters.
        /// The algorithm is: if a filter is dragged to either end of the list,
        /// it is assumed to be necessary that its order is respected, so the order value is put as the adjacent value +- 1.
        /// However, if a filter is dragged in between two elements, there are two cases:
        /// if the elements are equal, the moved filter should have the same value as them;
        /// but if they are distinct, the moved filter should have their average value.
        /// If that means it is equal to either one, it has to shift the entire list by 1 so it makes some room for itself.
        ///
        /// TODO: make the reordered values "round", so have a preference for not reassigning the value 0,
        /// and choosing values such as 5, 10, 100, -20, -50, rather than 3, 7, 21 or other primes.
        /// </summary>
        /// <param name="list"></param>
        private void ReorderFilterListCallback(ReorderableList list)
        {
            serializedObject.Update();

            // the filter with the wrong filter.order is the currently selected one: filters[list.index].
            var filters = _manager.filters;
            var n = filters.Count;

            if (filters.Count > 1)
            {
                // edge cases:
                if (list.index == 0)
                {
                    filters[0].order = filters[1].order - 1;
                }
                else if (list.index == n - 1)
                {
                    filters[n - 1].order = filters[n - 2].order + 1;
                }
                else
                {
                    var w = list.index;
                    // the unordered filter is in the middle of the list.
                    // give it the value of average between the two neighbors. then deal with consequences
                    filters[w].order = (filters[w - 1].order + filters[w + 1].order) / 2;

                    // if filters' new neighbors are different, move them apart. if they are the same don't do anything, we wanted the filter to fit in here.
                    if (filters[w - 1].order != filters[w + 1].order)
                    {
                        if (filters[w].order == filters[w - 1].order)
                        {
                            for (int i = w - 1; i >= 0; i--)
                            {
                                filters[i].order -= 1;
                            }
                        }
                        else if (filters[w].order == filters[w + 1].order)
                        {
                            for (int i = w + 1; i < filters.Count; i++)
                            {
                                filters[i].order += 1;
                            }
                        }
                    }
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawFilterListHeaderCallback(Rect rect)
        {
            // CLONED FROM DrawFilterListElementCallback. UPDATE BOTH BEFORE SAVING!!!
            var applyButtonWidth = 70;
            var xButtonWidth = 24;
            var genTimeLabelWidth = 60;
            var buttonsOffset = 36;

            serializedObject.Update();
            EditorGUI.LabelField(rect, "Filters", EditorStyles.boldLabel);
            var bgRect = new Rect(rect.x - 5, rect.y + 16, rect.width + 9, rect.height - 16);
            var darkGrey = 0.375f;
            EditorGUI.DrawRect(bgRect, new Color(darkGrey, darkGrey, darkGrey));
            var toggleRect = new Rect(rect.x + 14, rect.y + 16, 16, 16);

            var toggleGraphicsEnabled = _manager.filters.Any(f => !f.enabled);
            // if someone presses the toggle like a button
            if (EditorGUI.Toggle(toggleRect, GUIContent.none, !toggleGraphicsEnabled) == toggleGraphicsEnabled)
            {
                // if any filter is disabled, enable all first.
                if (toggleGraphicsEnabled)
                {
                    // enable all
                    EnableAllFilters(true);
                }
                else
                {
                    EnableAllFilters(false);
                }

            }

            var genTimeLabelRect = toggleRect;
            genTimeLabelRect.x = rect.width - applyButtonWidth - xButtonWidth - genTimeLabelWidth*2 + buttonsOffset - 14;
            genTimeLabelRect.width = genTimeLabelWidth*2;
            var totalExecTime = _manager.generationTime;
            // draw label with sum of all generation times
            GUI.skin.label.alignment = TextAnchor.MiddleRight;
            GUI.Label(genTimeLabelRect, "Gen in " + totalExecTime.ToString("F0") + "ms");
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;

            serializedObject.ApplyModifiedProperties();
        }

        private void EnableAllFilters(bool enabled)
        {
            for (int i = 0; i < _manager.filters.Count; i++)
            {
                var thisFilter = _manager.filters[i];
                var thisFilterMonobehaviour = (MonoBehaviour)thisFilter;
                thisFilterMonobehaviour.enabled = enabled;
                // if filters are enabled, set their gameobject to active.
                if (enabled)
                {
                    if (!thisFilterMonobehaviour.gameObject.activeInHierarchy)
                    {
                        thisFilterMonobehaviour.gameObject.SetActive(true);
                    }
                }
            }
        }

        private void DrawFilterListElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            serializedObject.Update();

            var toggleWidth = 26;
            var orderWidth = 29;
            var orderToTextGap = 2;
            var applyButtonWidth = 70;
            var xButtonWidth = 24;
            var genTimeLabelWidth = 60;
            var buttonsOffset = 36;

            if (_manager.filters.Count > index)
            {
                var thisFilter = _manager.filters[index];
                var thisFilterMonobehaviour = (MonoBehaviour)thisFilter;
                // cannot nullcheck an interface apparently!
                if (thisFilterMonobehaviour != null)
                {
                    // Enabled toggle box
                    var curRect = rect;
                    curRect.width = toggleWidth;
                    // set script enabled to exactly the value of filter.enabled
                    thisFilterMonobehaviour.enabled = EditorGUI.Toggle(curRect, GUIContent.none, thisFilterMonobehaviour.enabled);

                    // too buggy for now. 
                    //// set gameObject.active to true if script is enabled and the gameObject isn't. but leave it false if script was disabled, whatever
                    //if (thisFilterMonobehaviour.enabled && !thisFilterMonobehaviour.gameObject.activeInHierarchy)
                    //{
                    //    thisFilterMonobehaviour.gameObject.SetActive(true);
                    //}

                    // Order int field
                    curRect.x += curRect.width;
                    curRect.width = orderWidth;
                    thisFilter.order = EditorGUI.DelayedIntField(curRect, GUIContent.none, thisFilter.order);
                    // when changing the order, all surrounding nodes should change their values
                    // in case they interfere with the new order. the node should not instead jump around the list so you lose it

                    // Filter name
                    curRect.x += curRect.width + orderToTextGap;
                    var labelX = curRect.x;

                    // Label to show duration of generation
                    curRect.x = rect.width - applyButtonWidth - xButtonWidth - genTimeLabelWidth + buttonsOffset;
                    curRect.width = genTimeLabelWidth;

                    GUI.skin.label.alignment = TextAnchor.MiddleRight;
                    GUI.Label(curRect, thisFilter.generationFrames + "/" + thisFilter.generationTime.ToString("F0") + "ms");
                    GUI.skin.label.alignment = TextAnchor.MiddleLeft;

                    // Button for applying/reapplying filter (also shows w/ colors if the filter is applied or not)
                    curRect.x = rect.width - applyButtonWidth - xButtonWidth + buttonsOffset;
                    curRect.width = applyButtonWidth;
                    if (GUIButtonColored(thisFilter, curRect, thisFilter.isApplied ? "Reapply" : "Apply"))
                    {
                        ApplyFilterFancy(thisFilter);
                    }

                    // Button for clearing filter (X)
                    curRect.x = rect.width - xButtonWidth + buttonsOffset;
                    curRect.width = xButtonWidth;
                    if (GUI.Button(curRect, "X"))
                    {
                        ClearFilterFancy(thisFilter);
                    }

                    curRect.x = labelX;
                    // width is what remains from label.left until buttons left
                    curRect.width = rect.width - curRect.x - applyButtonWidth - genTimeLabelWidth - xButtonWidth + buttonsOffset;

                    string filterName = thisFilterMonobehaviour.gameObject.name + " / " + thisFilter.ToString();

                    GUI.Label(curRect, filterName);

                    // draw thisFilter's filter editor below. ghetto mode but at least it's usable.
                    if (isActive)
                    {
                        EditorGUILayout.Separator();
                        EditorGUILayout.LabelField("Ghetto editor for " + filterName, EditorStyles.boldLabel);
                        var theEditor = Editor.CreateEditor(thisFilterMonobehaviour);
                        theEditor.OnInspectorGUI();
                        EditorGUILayout.Separator();
                    }
                }
                else
                {
                    // one of the elements is null. fuck!
                    _manager.SortFilters();
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        public override void OnInspectorGUI()
        {
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", _script, typeof(MonoScript), false);
            GUI.enabled = true;

            EditorGUILayout.Separator();

            // ============================== BUTTONS ==============================

            if (GUILayout.Button("Generate All"))
            {
                _manager.GenerateAll();
            }

            if (GUILayout.Button("Clear All"))
            {
                _manager.ClearAll();
            }

            if (filterDisplayList != null)
            {
                filterDisplayList.DoLayoutList();
            }
            else
            {
                Debug.LogError("NullRef: The ReorderableList in the " + _manager.name + " SlopeManager inspector was null", _manager.gameObject);
            }

            // ========================= SAVE MESH =========================

            if (GUILayout.Button("Destroy All Generated Children"))
            {
                _manager.DestroyAllGenChildren();
            }

            EditorGUILayout.Separator();

            if (GUILayout.Button("Save Mesh"))
            {
                if (_manager.mesh == null || _manager.mesh.vertexCount == 0)
                {
                    EditorUtility.DisplayDialog("No Generated Mesh", "Please make sure that you have generated mesh prior to attempting to save it.", "Ok");
                }
                else
                {
                    var meshName = EditorUtility.SaveFilePanelInProject("Slope Mesh Name", "Slope Mesh", "asset", "Choose the name and location of the generated mesh asset.");
                    EditorHelpers.CreateOrUpdateAsset(_manager.mesh, Path.GetFileNameWithoutExtension(meshName), Path.GetDirectoryName(meshName));
                }
            }

            EditorGUILayout.Separator();

            this.serializedObject.Update();

            // ============================== CREATE OPTIONS ==============================
            EditorHelpers.Section("Create Options");

            EditorGUILayout.PropertyField(_keepSeed);
            // ALWAYS SHOW the seed. in case we want to keep it
            EditorGUILayout.PropertyField(_currentSeed);
            EditorGUILayout.PropertyField(_stringSeed);

            EditorHelpers.Section("Debugging");

            GUI.enabled = false;

            EditorGUILayout.PropertyField(_nodes, true);

            GUI.enabled = true;

            this.serializedObject.ApplyModifiedProperties();

        }

        private void ApplyFilterFancy(ISlopeFilter thisFilter)
        {
            // algo: clear all filters after this one, clear self, apply all unapplied previous filters, apply self. do not disturb the filters with equal order.
            var filters = _manager.filters;

            // which filters to clear? just the ones after self. doesn't matter if they're applied or not, due to manual meddling.
            var clearFilters = filters.Where(f => f.order > thisFilter.order).ToList();

            // clear all filters with order after self, except equal priority
            for (int j = clearFilters.Count - 1; j >= 0; j--)
            {
                clearFilters[j].Clear();
            }
            thisFilter.Clear();

            var applyFilters = filters.Where(f => f.order < thisFilter.order && !f.isApplied && f.enabled).ToList();
            // add this filter to the end of the list
            applyFilters.Add(thisFilter);

            // apply this filter only.
            for (int j = 0; j < applyFilters.Count; j++)
            {
                applyFilters[j].Apply(_manager);
            }
        }

        private void ClearFilterFancy(ISlopeFilter thisFilter)
        {
            // which filters to clear?
            // only filters whose order is larger than this one.
            var clearFilters = _manager.filters.Where(f => f.order > thisFilter.order).ToList();

            // clear all filters with order after self, except equal priority
            for (int j = clearFilters.Count - 1; j >= 0; j--)
            {
                clearFilters[j].Clear();
            }
            thisFilter.Clear();

        }

        private bool GUILayoutButtonColored(ISlopeFilter filter, string buttonText, float buttonWidth)
        {
            GUI.color = filter.isApplied ? Color.green : Color.red;
            var buttonResult = GUILayout.Button(buttonText, GUILayout.Width(buttonWidth));
            GUI.color = Color.white;
            return buttonResult;
        }

        private bool GUIButtonColored(ISlopeFilter filter, Rect rect, string buttonText)
        {
            GUI.color = filter.isApplied ? Color.green : Color.red;
            var buttonResult = GUI.Button(rect, buttonText);
            GUI.color = Color.white;
            return buttonResult;
        }
    }
}