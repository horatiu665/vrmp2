namespace Slope.Editor
{
    using Helpers.Editor;
    using UnityEditor;
    using UnityEngine;

    [CanEditMultipleObjects]
    [CustomEditor(typeof(SlopeFilter), true)]
    public class SlopeFilterEditor : Editor
    {
        private SlopeFilter _filter;

        private void OnEnable()
        {
            _filter = this.target as SlopeFilter;
        }

        public override void OnInspectorGUI()
        {
            // buttons for apply and clear, then the rest of the controls

            EditorGUILayout.Separator();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Apply"))
            {
                _filter.Apply(_filter.slope);
            }
            if (GUILayout.Button("Clear + Apply"))
            {
                _filter.Clear();
                _filter.Apply(_filter.slope);
            }
            if (GUILayout.Button("Clear"))
            {
                _filter.Clear();
            }
            GUI.color = Color.green;
            if (GUILayout.Button("GenAll", GUILayout.Width(60)))
            {
                _filter.slope.GenerateAll();
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            EditorGUILayout.Separator();

            base.OnInspectorGUI();
        }
    }
}