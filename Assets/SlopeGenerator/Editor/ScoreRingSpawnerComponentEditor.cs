namespace Slope.Editor
{
    using UnityEditor;
    using UnityEngine;
    using VRCore.Editor;

    [CustomEditor(typeof(ScoreRingSpawnerComponent))]
    public class ScoreRingSpawnerComponentEditor : Editor
    {
        private SerializedProperty _scoreRingPrefab, _parent, _dropVector, _ballisticCurve, _ringCountRange, _ringDistanceRange, _rings;
        private ScoreRingSpawnerComponent _component;
        private MonoScript _script;

        private void OnEnable()
        {
            _scoreRingPrefab = this.FindProperty("_scoreRingPrefab");
            _parent = this.FindProperty("_parent");
            _dropVector = this.FindProperty("_dropVector");
            _ballisticCurve = this.FindProperty("_ballisticCurve");
            _ringCountRange = this.FindProperty("_ringCountRange");
            _ringDistanceRange = this.FindProperty("_ringDistanceRange");
            _rings = this.FindProperty("_rings");

            _component = (ScoreRingSpawnerComponent)this.target;
            _script = MonoScript.FromMonoBehaviour(_component);
        }

        public override void OnInspectorGUI()
        {
            this.serializedObject.Update();

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", _script, typeof(MonoScript), false);
            GUI.enabled = true;

            EditorGUILayout.PropertyField(_scoreRingPrefab);
            EditorGUILayout.PropertyField(_parent);
            EditorGUILayout.PropertyField(_dropVector);
            EditorGUILayout.PropertyField(_ballisticCurve);
            EditorGUILayout.PropertyField(_ringCountRange);
            EditorGUILayout.PropertyField(_ringDistanceRange);

            EditorGUILayout.Separator();

            EditorGUILayout.PropertyField(_rings, true);

            this.serializedObject.ApplyModifiedProperties();
            EditorGUILayout.Separator();

            if (GUILayout.Button("Clear Rings"))
            {
                _component.Clear();
            }

            if (GUILayout.Button("Generate Score Rings"))
            {
                _component.SpawnScoreRings();
            }
        }
    }
}