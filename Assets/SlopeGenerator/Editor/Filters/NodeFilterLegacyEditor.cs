using Slope;
using UnityEditor;
using UnityEngine;
using VRCore.Editor;

[System.Obsolete]
[CustomEditor(typeof(NodeFilterLegacy))]
public class NodeFilterLegacyEditor : Editor
{
    private SerializedProperty _createSplines;
    private SerializedProperty _createSeparation;
    private SerializedProperty _splineSeparationIterations;
    private SerializedProperty _splineParameters;
    private SerializedProperty _startParameters;
    private SerializedProperty _separationParameters;
    private SerializedProperty _straightParameters;
    private SerializedProperty _jumpCurveParameters;
    private SerializedProperty _jumpParameters;
    private SerializedProperty _lanesParameters;
    private SerializedProperty _goalParameters;

    private MonoScript _script;
    private NodeFilterLegacy _filter;

    private void OnEnable()
    {
        _createSplines = this.FindProperty("_createSplines");
        _createSeparation = this.FindProperty("_createSeparation");
        _splineSeparationIterations = this.FindProperty("_splineSeparationIterations");
        _splineParameters = this.FindProperty("_splineParameters");
        _startParameters = this.FindProperty("_startParameters");
        _separationParameters = this.FindProperty("_separationParameters");
        _straightParameters = this.FindProperty("_straightParameters");
        _jumpCurveParameters = this.FindProperty("_jumpCurveParameters");
        _jumpParameters = this.FindProperty("_jumpParameters");
        _lanesParameters = this.FindProperty("_lanesParameters");
        _goalParameters = this.FindProperty("_goalParameters");

        _filter = this.target as NodeFilterLegacy;
        _script = MonoScript.FromMonoBehaviour(_filter);
    }

    public override void OnInspectorGUI()
    {
        GUI.enabled = false;
        EditorGUILayout.ObjectField("Script", _script, typeof(MonoScript), false);
        GUI.enabled = true;

        // order
        _filter.order = EditorGUILayout.IntField("Order", _filter.order);

        EditorGUILayout.Separator();

        // ============================== buttons like on SlopeFilterEditor ==============================

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Apply"))
        {
            _filter.Apply(_filter.slope);
        }
        if (GUILayout.Button("Clear + Apply"))
        {
            _filter.Clear();
            _filter.Apply(_filter.slope);
        }
        if (GUILayout.Button("Clear"))
        {
            _filter.Clear();
        }
        GUILayout.EndHorizontal();

        EditorGUILayout.Separator();

        // ============================== interface like on old SlopeManager ==============================

        this.serializedObject.Update();
        // ============================== CREATE OPTIONS ==============================
        EditorHelpers.Section("Create Options");

        EditorGUILayout.PropertyField(_createSplines);
        EditorGUILayout.PropertyField(_createSeparation);

        // ============================== SPLINES ==============================
        EditorHelpers.Section("Splines");

        EditorGUILayout.PropertyField(_splineSeparationIterations);
        EditorGUILayout.PropertyField(_splineParameters, true);

        // ============================== START ==============================
        EditorHelpers.Section("Start");

        EditorGUILayout.PropertyField(_startParameters, true);
        EditorGUILayout.PropertyField(_separationParameters, true);

        // ============================== STRAIGHT ==============================
        EditorHelpers.Section("Straight");

        EditorGUILayout.PropertyField(_straightParameters, true);

        // ============================== BLABLA ==============================
        EditorHelpers.Section("JumpCurves");

        EditorGUILayout.PropertyField(_jumpCurveParameters, true);

        EditorHelpers.Section("Jump");

        EditorGUILayout.PropertyField(_jumpParameters, true);

        EditorHelpers.Section("Secondary Lanes");

        EditorGUILayout.PropertyField(_lanesParameters, true);

        EditorHelpers.Section("Goal");

        EditorGUILayout.PropertyField(_goalParameters, true);

        this.serializedObject.ApplyModifiedProperties();
    }
}