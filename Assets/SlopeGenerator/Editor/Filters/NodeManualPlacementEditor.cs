﻿namespace Slope.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(NodeManualPlacement))]
    public sealed class NodeManualPlacementEditor : Editor
    {
        private NodeManualPlacement _filter;
        private bool _isPlacing;
        private Camera _cam;

        private void OnEnable()
        {
            _filter = (NodeManualPlacement)this.target;
            _cam = Camera.current;
        }

        private void OnSceneGUI()
        {
            if (IsKey(EventType.KeyUp, KeyCode.Delete))
            {
                DeleteLast();
                Event.current.Use();
            }

            if (!_isPlacing)
            {
                if (IsKey(EventType.KeyUp, KeyCode.Space))
                {
                    StartPlacing();
                    this.Repaint();
                    Event.current.Use();
                }

                return;
            }

            if (IsKey(EventType.KeyUp, KeyCode.Space))
            {
                NewNode();
                Event.current.Use();
            }
            else if (IsKey(EventType.KeyUp, KeyCode.Escape))
            {
                StopPlacing();
                Event.current.Use();
            }

            if (_filter.parentNode != null)
            {
                var camPos = GetCameraPosition();
                var pos = _filter.GetValidPosition(camPos);
                Handles.color = Color.magenta;
                Handles.DrawSolidDisc(pos, Vector3.up, 1f);
            }

            this.Repaint();
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            EditorGUILayout.Separator();
            if (_isPlacing)
            {
                if (_cam == null)
                {
                    EditorGUILayout.HelpBox("Could not find a camera! Cannot place new nodes without a camera. Make sure the scene view is focused. ", MessageType.Error);
                }
                else
                {
                    EditorGUILayout.HelpBox("Place new nodes by pressing spacebar, stop by pressing escape.", MessageType.Info);

                    var camPos = GetCameraPosition();
                    GUILayout.Label("Camera position: " + camPos.ToString() + (_filter.parentNode != null ? "\nNode position: " + _filter.GetValidPosition(camPos).ToString() : string.Empty));

                    GUI.color = Color.green;
                    if (GUILayout.Button("Create node (Spacebar)"))
                    {
                        NewNode();
                    }
                }

                GUILayout.BeginHorizontal();

                GUI.color = Color.gray;
                if (GUILayout.Button("Stop (Escape)"))
                {
                    StopPlacing();
                }

                GUI.color = Color.red;
                if (GUILayout.Button("Delete last (delete)"))
                {
                    DeleteLast();
                }
                GUILayout.EndHorizontal();
            }
            else
            {
                GUILayout.BeginHorizontal();
                GUI.color = Color.green;
                if (GUILayout.Button("New Node (Spacebar)"))
                {
                    StartPlacing();
                }

                GUI.color = Color.yellow;
                if (GUILayout.Button("Delete last (delete)"))
                {
                    DeleteLast();
                }
                GUILayout.EndHorizontal();
            }

            GUI.color = Color.red;
            EditorGUILayout.Separator();
            if (GUILayout.Button("Delete All Manually Placed Nodes"))
            {
                _filter.DeleteAll();
                EditorUtility.SetDirty(_filter);
            }
        }

        private void NewNode()
        {
            _filter.NewNode(GetCameraPosition());
            EditorUtility.SetDirty(_filter);
        }

        private void DeleteLast()
        {
            _filter.DeleteLast();
            EditorUtility.SetDirty(_filter);
        }

        private void StartPlacing()
        {
            _isPlacing = true;
            EditorApplication.update += OnUpdate;

            if (SceneView.sceneViews.Count > 0)
            {
                ((SceneView)SceneView.sceneViews[0]).Focus();
            }

            if (Camera.current != null)
            {
                _cam = Camera.current;
            }
        }

        private void StopPlacing()
        {
            // stop placing
            _isPlacing = false;
            EditorApplication.update -= OnUpdate;
            OnUpdate();
        }

        private Vector3 GetCameraPosition()
        {
            if (_cam == null)
            {
                _cam = Camera.current;
                if (_cam == null)
                {
                    return Vector3.zero;
                }
            }

            return _cam.transform.position;
        }

        private bool IsMouse(EventType type, int button)
        {
            var evt = Event.current;
            return evt.isMouse && evt.type == type && evt.button == button;
        }

        private bool IsKey(EventType type, KeyCode keyCode)
        {
            return Event.current.isKey && Event.current.type == type && Event.current.keyCode == keyCode;
        }

        private void OnUpdate()
        {
            if (Camera.current != null)
            {
                _cam = Camera.current;
            }
        }
    }
}