namespace Slope.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(ScoreRingTargetSpawnerComponent))]
    public class ScoreRingTargetSpawnerComponentEditor : Editor
    {
        private ScoreRingTargetSpawnerComponent _component;

        private void OnEnable()
        {
            _component = (ScoreRingTargetSpawnerComponent)this.target;
        }

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();

            if (GUILayout.Button("Clear Rings"))
            {
                _component.Clear();
            }

            if (GUILayout.Button("Generate Score Rings"))
            {
                _component.SpawnScoreRings();
            }
        }
    }
}