﻿namespace Slope
{
    using UnityEngine;

    public class LineSegment
    {
        public Node n1;
        public Node n2;

        public LineSegment(Node n1, Node n2)
        {
            this.n1 = n1;
            this.n2 = n2;
        }

        public Vector3 p1
        {
            get
            {
                return n1.transform.position;
            }
        }

        public Vector3 p2
        {
            get
            {
                return n2.transform.position;
            }
        }

        public Vector3 vector
        {
            get
            {
                return p2 - p1;
            }
        }

        public Vector3 GetPositionFromZ(float z)
        {
            var vector = this.vector;

            var fraction = (z - this.p1.z) / vector.z;

            var y = fraction * vector.y + p1.y;
            var x = fraction * vector.x + p1.x;

            return new Vector3(x, y, z);
        }
    }
}