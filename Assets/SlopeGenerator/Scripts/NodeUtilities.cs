namespace Slope
{
    using System.Collections.Generic;
    using Apex;
    using Helpers;
    using UnityEngine;

    /// <summary>
    /// Static helper class providing common node functionality for the <see cref="ISlopeFilter"/>s.
    /// </summary>
    public static class NodeUtilities
    {
        /// <summary>
        /// Action for nodes traversal that gives the index of the node and the node.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="node">The node.</param>
        public delegate void NodeTraverselAction(int index, Node node);

        /// <summary>
        /// Action for nodes traversal where the previous node is also needed.
        /// </summary>
        /// <param name="index">The index.</param>
        /// <param name="prevNode">The previous node.</param>
        /// <param name="node">The node.</param>
        public delegate void PreviousNodeTraversalAction(int index, Node prevNode, Node node);

        /// <summary>
        /// Calculates the node direction from the given parent's forward and the specified turn, drop and length <see cref="SmartRange"/>s are used to get random values, optionally reversing the turn direction based on a coin flip.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="turn">The turn.</param>
        /// <param name="drop">The drop.</param>
        /// <param name="length">The length.</param>
        /// <param name="turnFlipCoin">if set to <c>true</c> reverses the turn direction based on a coin flip.</param>
        /// <returns></returns>
        public static Vector3 CalculateNodeDirection(Node parent, SmartRange turn, SmartRange drop, SmartRange length, bool turnFlipCoin)
        {
            return CalculateNodeDirection(parent, turn.value, drop.value, length.value, turnFlipCoin);
        }

        /// <summary>
        /// Calculates the node direction from the given parent's forward and the specified turn, drop and length, optionally reversing the turn direction based on a coin flip.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="turn">The turn.</param>
        /// <param name="drop">The drop.</param>
        /// <param name="length">The length.</param>
        /// <param name="turnFlipCoin">if set to <c>true</c> reverses the turn direction based on a coin flip.</param>
        /// <returns></returns>
        public static Vector3 CalculateNodeDirection(Node parent, float turn, float drop, float length, bool turnFlipCoin)
        {
            var t = turn;
            if (turnFlipCoin)
            {
                // turn in a random direction based on the turn parameter, using flip coin
                t *= Utilities.FlipCoin();
            }

            return CalculateNodeDirection(parent, t, drop, length);
        }

        /// <summary>
        /// Calculates the node direction from the given parent's forward and the specified turn, drop and length.
        /// </summary>
        /// <param name="parent">The parent.</param>
        /// <param name="turn">The turn.</param>
        /// <param name="drop">The drop.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static Vector3 CalculateNodeDirection(Node parent, float turn, float drop, float length)
        {
            var forward = parent.transform.forward.OnlyXZ().normalized;
            return CalculateNodeDirection(forward, turn, drop, length);
        }

        /// <summary>
        /// Calculates the node direction based on the given forward, turn, drop and length.
        /// </summary>
        /// <param name="forward">The forward.</param>
        /// <param name="turn">The turn.</param>
        /// <param name="drop">The drop.</param>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        public static Vector3 CalculateNodeDirection(Vector3 forward, float turn, float drop, float length)
        {
            var forwardDir = Quaternion.Euler(0f, turn, 0f) * forward;
            var ortho = Quaternion.Euler(0f, 90f, 0f) * forwardDir;

            var nodeDir = Quaternion.AngleAxis(drop, -ortho) * forwardDir;
            return nodeDir.normalized * length;
        }

        /// <summary>
        /// Creates a child node with the designated <see cref="NodeType"/> of the designated parent, with the specified width and depth coming from the given <see cref="SmartRange"/>s, additionally setting the specified position.
        /// </summary>
        /// <param name="slope">The slope.</param>
        /// <param name="type">The type.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="width">The width.</param>
        /// <param name="depth">The depth.</param>
        /// <param name="position">The position.</param>
        /// <returns></returns>
        public static Node CreateChildNode(SlopeManager slope, NodeType type, Node parent, SmartRange width, SmartRange depth, Vector3 position)
        {
            return CreateChildNode(slope, type, parent, new Vector2(width.value, width.value), new Vector2(depth.value, depth.value), position);
        }

        /// <summary>
        /// Creates a child node with the designated <see cref="NodeType"/> of the designated parent, with the specified width and depth coming from the given <see cref="SmartRange"/>s.
        /// </summary>
        /// <param name="slope">The slope.</param>
        /// <param name="type">The type.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="width">The width.</param>
        /// <param name="depth">The depth.</param>
        /// <returns></returns>
        public static Node CreateChildNode(SlopeManager slope, NodeType type, Node parent, SmartRange width, SmartRange depth)
        {
            return CreateChildNode(slope, type, parent, new Vector2(width.value, width.value), new Vector2(depth.value, depth.value));
        }

        /// <summary>
        /// Creates a child node with the designated <see cref="NodeType"/> of the designated parent, with the specified width, depth and position.
        /// </summary>
        /// <param name="slope">The slope.</param>
        /// <param name="type">The type.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="width">The width.</param>
        /// <param name="depth">The depth.</param>
        /// <param name="position">The position.</param>
        /// <returns></returns>
        public static Node CreateChildNode(SlopeManager slope, NodeType type, Node parent, Vector2 width, Vector2 depth, Vector3 position)
        {
            var node = CreateChildNode(slope, type, parent, width, depth);
            node.transform.position = position;
            return node;
        }

        /// <summary>
        /// Creates a child node with the designated <see cref="NodeType"/> of the designated parent, with the specified width and depth.
        /// </summary>
        /// <param name="slope">The slope.</param>
        /// <param name="type">The type.</param>
        /// <param name="parent">The parent.</param>
        /// <param name="width">The width.</param>
        /// <param name="depth">The depth.</param>
        /// <returns></returns>
        public static Node CreateChildNode(SlopeManager slope, NodeType type, Node parent, Vector2 width, Vector2 depth)
        {
            var node = CreateChildNode(slope, type, parent);
            node.width = width;
            node.depth = depth;
            return node;
        }

        /// <summary>
        /// Creates a node with the designated <see cref="NodeType"/> and adds it as a child of another node, also adding it to the slope's list.
        /// </summary>
        /// <param name="slope">The slope.</param>
        /// <param name="type">The type.</param>
        /// <param name="parent">The parent.</param>
        /// <returns></returns>
        public static Node CreateChildNode(SlopeManager slope, NodeType type, Node parent)
        {
            var node = CreateNode(slope, type);
            node.parents.Add(parent);
            parent.children.Add(node);

            return node;
        }

        /// <summary>
        /// Creates a node with the designated <see cref="NodeType"/> and childs it under the slope, and adds the node to the slope's nodes list.
        /// </summary>
        /// <param name="slope">The slope.</param>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Node CreateNode(SlopeManager slope, NodeType type)
        {
            var node = CreateNode(type);
            node.slope = slope;
            node.transform.SetParent(slope.nodesParent);
            slope.nodes.Add(node);
            return node;
        }

        /// <summary>
        /// Creates a node and adds the Node component with the designated <see cref="NodeType"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public static Node CreateNode(NodeType type)
        {
            var go = new GameObject(type.ToString());

            var node = go.AddComponent<Node>();
            node.nodeType = type;

            return node;
        }

        /*
         *  Traversal methods from parent-to-parent.
         */

        /// <summary>
        /// Iterate through all ancestors of the last node. The specified action is executed for each node, providing access to the index (not necessarily list index) and current node.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="action">The action.</param>
        public static void ForEachParent(this List<Node> nodes, NodeTraverselAction action)
        {
            var count = nodes.Count;
            if (count == 0)
            {
                return;
            }

            ForEachParent(nodes[count - 1], action);
        }

        /// <summary>
        /// Iterate through all ancestors of the given start node. The specified action is executed for each node, providing access to the index (not necessarily list index) and current node.
        /// </summary>
        /// <param name="startNode">The start node.</param>
        /// <param name="action">The action.</param>
        public static void ForEachParent(this Node startNode, NodeTraverselAction action)
        {
            if (startNode.parents.Count == 0)
            {
                return;
            }

            var index = 0;
            action(index++, startNode);

            var queue = new Queue<Node>(startNode.parents);
            while (queue.Count > 0)
            {
                var parent = queue.Dequeue();
                action(index++, parent);

                var parentCount = parent.parents.Count;
                for (int i = 0; i < parentCount; i++)
                {
                    queue.Enqueue(parent.parents[i]);
                }
            }
        }

        /*
         *  Traversal methods from parent-to-parent with the previous node provided.
         */

        /// <summary>
        /// Iterate through all ancestors of the last node. The specified action is executed for each node, providing access to the index (not necessarily list index), previous node and current node.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="action">The action.</param>
        public static void ForEachParent(this List<Node> nodes, PreviousNodeTraversalAction action)
        {
            var count = nodes.Count;
            if (count < 2)
            {
                return;
            }

            // When using the previous node traversal action, start at the second last node, since the very last node will then be the "prevNode"
            ForEachParent(nodes[count - 2], action);
        }

        /// <summary>
        /// Iterate through all ancestors of the given start node. The specified action is executed for each node, providing access to the index (not necessarily list index), previous node and current node.
        /// </summary>
        /// <param name="startNode">The start node.</param>
        /// <param name="action">The action.</param>
        public static void ForEachParent(this Node startNode, PreviousNodeTraversalAction action)
        {
            if (startNode.parents.Count == 0)
            {
                return;
            }

            var index = 0;
            action(index++, startNode.children.Count > 0 ? startNode.children[0] : null, startNode); // TODO: multiple children?

            var queue = new Queue<Node>(startNode.parents);
            while (queue.Count > 0)
            {
                var parent = queue.Dequeue();
                action(index++, startNode, parent);

                var parentCount = parent.parents.Count;
                for (int i = 0; i < parentCount; i++)
                {
                    queue.Enqueue(parent.parents[i]);
                }

                startNode = parent;
            }
        }

        /*
         *  Traversal methods from child-to-child.
         */

        /// <summary>
        /// Iterate through all descendent nodes of the first node. The specified action is executed for each node, providing access to the index (not necessarily list index) and current node.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="action">The action.</param>
        public static void ForEachChild(this List<Node> nodes, NodeTraverselAction action)
        {
            ForEachChild(nodes, 0, action);
        }

        /// <summary>
        /// Iterate through all descendent nodes of the node at index 'start'. The specified action is executed for each node, providing access to the index (not necessarily list index) and current node.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="action">The action.</param>
        /// <param name="start">The start.</param>
        public static void ForEachChild(this List<Node> nodes, int start, NodeTraverselAction action)
        {
            var count = nodes.Count;
            if (count == 0)
            {
                return;
            }

            var s = Mathf.Max(0, start);
            if (s >= count)
            {
                return;
            }

            ForEachChild(nodes[s], action);
        }

        /// <summary>
        /// Iterate through all descendents of the given start node. The specified action is executed for each node, providing access to the index (not necessarily list index) and current node.
        /// </summary>
        /// <param name="startNode">The start node.</param>
        /// <param name="action">The action.</param>
        public static void ForEachChild(this Node startNode, NodeTraverselAction action)
        {
            if (startNode.children.Count == 0)
            {
                return;
            }

            var index = 0;
            action(index++, startNode);

            var queue = new Queue<Node>(startNode.children);
            while (queue.Count > 0)
            {
                var child = queue.Dequeue();
                action(index++, child);

                var childCount = child.children.Count;
                for (int i = 0; i < childCount; i++)
                {
                    queue.Enqueue(child.children[i]);
                }
            }
        }

        /*
         *  Traversal methods from child-to-child with the previous node provided.
         */

        /// <summary>
        /// Iterate through all descendent nodes in the list, starting at the first one. The specified action is executed for each node, providing access to the index (not necessarily list index), previous node and current node.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="action">The action.</param>
        public static void ForEachChild(this List<Node> nodes, PreviousNodeTraversalAction action)
        {
            ForEachChild(nodes, 0, action);
        }

        /// <summary>
        /// Iterate through all descendent nodes of the node at index 'start'. The specified action is executed for each node, providing access to the index (not necessarily list index), previous node and current node.
        /// </summary>
        /// <param name="nodes">The nodes.</param>
        /// <param name="action">The action.</param>
        /// <param name="start">The start.</param>
        public static void ForEachChild(this List<Node> nodes, int start, PreviousNodeTraversalAction action)
        {
            var count = nodes.Count;
            if (count == 0)
            {
                return;
            }

            var s = Mathf.Max(0, start);
            if (s >= count)
            {
                return;
            }

            ForEachChild(nodes[s + 1], action);
        }
        
        /// <summary>
        /// Iterate through all descendents of the given start node. The specified action is executed for each node, providing access to the index (not necessarily list index), previous node and current node.
        /// </summary>
        /// <param name="startNode">The start node.</param>
        /// <param name="action">The action.</param>
        public static void ForEachChild(this Node startNode, PreviousNodeTraversalAction action)
        {
            if (startNode.children.Count == 0)
            {
                return;
            }

            var index = 0;
            action(index++, startNode.parents.Count > 0 ? startNode.parents[0] : null, startNode); // TODO: multiple parents?

            var queue = new Queue<Node>(startNode.children);
            while (queue.Count > 0)
            {
                var child = queue.Dequeue();
                action(index++, startNode, child);

                var childCount = child.children.Count;
                for (int i = 0; i < childCount; i++)
                {
                    queue.Enqueue(child.children[i]);
                }

                startNode = child;
            }
        }
    }
}