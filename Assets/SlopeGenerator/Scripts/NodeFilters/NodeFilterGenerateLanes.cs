namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;

    public sealed class NodeFilterGenerateLanes : SlopeFilter
    {
        [Space]
        [SerializeField]
        private int _startNodeIndexOffset = 4;

        [SerializeField]
        private int _endNodeIndexOffset = 4;

        [SerializeField]
        private SmartRange _laneNodeLength = new SmartRange(5f, 10f);

        [SerializeField]
        private SmartRange _turnsCount = new SmartRange(1f, 2f);

        [SerializeField]
        private SmartRange _clearance = new SmartRange(40f, 50f);

        [SerializeField]
        private SmartRange _width = new SmartRange(10f, 20f);

        [SerializeField]
        private SmartRange _depth = new SmartRange(10f, 20f);

        [Space]
        [SerializeField]
        private NodeType[] _acceptedNodeTypes = new NodeType[]
        {
            NodeType.Straight
        };

        private HashSet<NodeType> _acceptedNodes;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (slope.nodes.Count == 0)
            {
                Debug.LogError(this.ToString() + " cannot operate with no nodes!");
                return;
            }

            _acceptedNodes = new HashSet<NodeType>(_acceptedNodeTypes, new NodeTypeComparer());

            Node splitNode = null, mergeNode = null;

            var nodes = slope.nodes;
            var count = nodes.Count;
            var start = Mathf.Max(0, _startNodeIndexOffset);
            var end = Mathf.Min(count, count - _endNodeIndexOffset);

            var nextIndex = 0;
            nodes.ForEachChild(start, (index, node) =>
            {
                if (index >= end || nextIndex >= index)
                {
                    return;
                }

                if (!_acceptedNodes.Contains(node.nodeType))
                {
                    return;
                }

                if (splitNode == null)
                {
                    splitNode = node;
                    nextIndex = index + _laneNodeLength.intValue;
                    return;
                }

                mergeNode = node;
                nextIndex = count;
            });

            if (splitNode == null)
            {
                Debug.LogError(this.ToString() + " could not identify any potential split nodes.");
                return;
            }

            if (mergeNode == null)
            {
                Debug.LogError(this.ToString() + " could not find a merge node within the designated lane node length");
                return;
            }

            CreateLaneNodes(slope, splitNode, mergeNode);

            DoneApplying();
        }

        private void CreateLaneNodes(SlopeManager slope, Node splitNode, Node mergeNode)
        {
            splitNode.nodeType = NodeType.Split;
            splitNode.UpdateName();

            mergeNode.nodeType = NodeType.Merge;
            mergeNode.UpdateName();

            var splitNodePos = splitNode.transform.position;
            var flipDir = Utilities.FlipCoin();

            var dir = mergeNode.transform.position - splitNodePos;
            var orthoDir = new Vector3(dir.z, 0f, -dir.x);
            var orthoDirNorm = orthoDir.normalized;

            var turnsCount = _turnsCount.value;
            var avgDirPerTurn = dir / (turnsCount + 1f);

            var parentNode = splitNode;

            for (int i = 0; i < turnsCount; i++)
            {
                var clearance = _clearance.value;
                var dirFraction = avgDirPerTurn * (i + 1f);
                var turnPos = splitNodePos + dirFraction + orthoDirNorm * clearance * flipDir;
                var dirFromParentToTurn = turnPos - parentNode.transform.position;
                var childNode = NodeUtilities.CreateChildNode(slope, NodeType.Straight, parentNode, _width, _depth, parentNode.transform.position + dirFromParentToTurn);

                parentNode.UpdateLookDir();
                parentNode = childNode;

                // flip the direction for the next turn
                flipDir *= -1;
            }

            // the last node should merge with the main lane
            parentNode.children.Add(mergeNode);
            parentNode.UpdateLookDir();

            mergeNode.parents.Add(parentNode);
        }

        public override void Clear()
        {
            base.Clear();
            this.slope.ClearNodes();

            DoneClearing();
        }
    }
}