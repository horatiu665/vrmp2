namespace Slope
{
    using Helpers;
    using UnityEngine;

    /// <summary>
    /// This filter should make "Straight" sections of nodes, with parameters to affect the shape of the generated nodes.
    /// </summary>
    public class NodeFilterStraight : SlopeFilter
    {
        [SerializeField]
        private bool _makeStartNodeIfNull = true;

        [SerializeField]
        private SmartRange _nodeCount = new SmartRange(10f);

        [SerializeField]
        private bool _relativeRotation = true;

        [SerializeField]
        private SmartRange _drop = new SmartRange(-10f, -30f);

        [SerializeField]
        private SmartRange _length = new SmartRange(20f, 40f);

        [SerializeField]
        private SmartRange _turn = new SmartRange(0f, 0f);

        [SerializeField]
        private bool _turnFlipCoin = true;

        [SerializeField]
        private SmartRange _width = new SmartRange(10f);

        [SerializeField]
        private SmartRange _depth = new SmartRange(10f);

        /// <summary>
        /// multiplies the drop across the generated nodes in the filter. example use: multiply by linear 0 to 1 to drop gradually.
        /// </summary>
        //[SerializeField]
        //private AnimationCurve _dropMultiplierAcross = AnimationCurve.Linear(0, 1, 1, 1);

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            int nodeCount = _nodeCount.intValue;

            Node parent;
            if (slope.nodes.Count > 0)
            {
                parent = slope.nodes[slope.nodes.Count - 1];
            }
            else
            {
                parent = NodeUtilities.CreateNode(slope, _makeStartNodeIfNull ? NodeType.Start : NodeType.Straight);
                parent.transform.position = slope.nodesParent.position;
                parent.transform.rotation = slope.nodesParent.rotation;

                parent.width = new Vector2(_width.value, _width.value);
                parent.depth = new Vector2(_depth.value, _depth.value);

                nodeCount -= 1;
            }

            for (int i = 0; i < nodeCount; i++)
            {
                // generate node and put it at the end of node list
                var localDir = NodeUtilities.CalculateNodeDirection(parent, _turn, _drop, _length, _turnFlipCoin);
                var newNode = NodeUtilities.CreateChildNode(slope, NodeType.Straight, parent, _width, _depth, parent.transform.position + localDir);

                //parent should look towards the child
                parent.UpdateLookDir();

                if (_relativeRotation)
                {
                    newNode.transform.rotation = parent.transform.rotation;
                }

                // use new node as parent for next node
                parent = newNode;
            }

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();
            slope.ClearNodes();

            DoneClearing();
        }
    }
}