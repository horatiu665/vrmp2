namespace Slope
{
    using Helpers;
    using UnityEngine;

    /// <summary>
    /// This filter should make "Straight" sections of nodes, with parameters to affect the shape of the generated nodes.
    /// </summary>
    public class NodeFilterGoal : SlopeFilter
    {
        [SerializeField]
        private SmartRange _drop = new SmartRange(0);

        [SerializeField]
        private SmartRange _length = new SmartRange(20);

        [SerializeField]
        private SmartRange _turn = new SmartRange(0);

        [SerializeField]
        private bool _turnFlipCoin = true;

        [SerializeField]
        private SmartRange _width = new SmartRange(10);

        [SerializeField]
        private SmartRange _depth = new SmartRange(10);

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            // slope.nodes cannot be empty.... because it's the damn goal filter!
            if (slope.nodes.Count == 0)
            {
                Debug.LogError(this.ToString() + " NodeFilterGoal Error: Slope nodes are empty!!!");
                return;
            }

            var parent = slope.nodes[slope.nodes.Count - 1];

            // generate node and put it at the end of node list
            var localDir = NodeUtilities.CalculateNodeDirection(parent, _turn, _drop, _length, _turnFlipCoin);
            var newNode = NodeUtilities.CreateChildNode(slope, NodeType.Goal, parent, _width, _depth, parent.transform.position + localDir);

            // parent should look towards the child
            parent.UpdateLookDir();

            // rotate goal node like the parent node so we don't get weird shapes at the end.
            newNode.transform.rotation = parent.transform.rotation;

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            DoneClearing();
        }
    }
}