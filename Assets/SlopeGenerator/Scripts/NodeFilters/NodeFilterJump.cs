namespace Slope
{
    using Helpers;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    public sealed class NodeFilterJump : SlopeFilter
    {
        [Header("General")]
        [SerializeField]
        private bool _turnFlipCoin = true;

        [Header("Ramp")]
        [SerializeField]
        private bool _usePreviousNodeDrop = false;

        [SerializeField]
        private AnimationCurve _rampCurve = new AnimationCurve(new Keyframe(0f, 1f, 0f, 0f), new Keyframe(0.85f, 1f, 0f, 0f), new Keyframe(1f, 1.125f, -1f, 0f));

        [SerializeField]
        private SmartRange _rampSizeLength = new SmartRange(50f, 60f);

        [SerializeField]
        private SmartRange _rampSizeHeight = new SmartRange(50f);

        [SerializeField]
        private SmartRange _rampSegmentLength = new SmartRange(10f);

        [SerializeField]
        private SmartRange _rampWidth = new SmartRange(10f);

        [SerializeField]
        private SmartRange _rampDepth = new SmartRange(10f);

        [Header("Jump")]
        [SerializeField, FormerlySerializedAs("_jumpDrop")]
        private SmartRange _jumpDropAngle = new SmartRange(-45);

        [SerializeField]
        private SmartRange _jumpLength = new SmartRange(30f, 50f);

        [SerializeField]
        private SmartRange _jumpTurn = new SmartRange(0f);

        [SerializeField]
        private SmartRange _jumpWidth = new SmartRange(10);

        [SerializeField]
        private SmartRange _jumpDepth = new SmartRange(10);

        [Header("Landing")]
        [SerializeField, FormerlySerializedAs("_landingDrop")]
        private SmartRange _landingDropAngle = new SmartRange(0f);

        [SerializeField]
        private SmartRange _landingLength = new SmartRange(10f);

        [SerializeField]
        private SmartRange _landingTurn = new SmartRange(0f);

        [SerializeField]
        private SmartRange _landingWidth = new SmartRange(20f);

        [SerializeField]
        private SmartRange _landingDepth = new SmartRange(10);

        [Header("Straight")]
        [SerializeField]
        private bool _addStraightNodeBefore = true;

        [SerializeField]
        private bool _addStraightNodeAfter = true;

        [SerializeField]
        private SmartRange _straightWidth = new SmartRange(20f);

        [SerializeField]
        private SmartRange _straightDepth = new SmartRange(20f);

        [SerializeField]
        private SmartRange _straightTurn = new SmartRange(0f);

        [SerializeField]
        private SmartRange _straightDrop = new SmartRange(-10f);

        [SerializeField]
        private SmartRange _straightLength = new SmartRange(20f, 30f);

        [SerializeField]
        private bool _straightTurnFlipCoin = true;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            Node parent = null;
            if (slope.nodes.Count > 0)
            {
                parent = slope.nodes.Last();
            }
            else
            {
                parent = NodeUtilities.CreateNode(slope, NodeType.Start);
                parent.transform.position = slope.nodesParent.position;
                parent.transform.rotation = slope.nodesParent.rotation;

                parent.width = new Vector2(_rampWidth.value, _rampWidth.value);
                parent.depth = new Vector2(_rampDepth.value, _rampDepth.value);
            }

            if (_addStraightNodeBefore)
            {
                var position = parent.transform.position + NodeUtilities.CalculateNodeDirection(parent, _straightTurn, _straightDrop, _straightLength, _straightTurnFlipCoin);
                parent = NodeUtilities.CreateChildNode(slope, NodeType.PreRamp, parent, _straightWidth, _straightDepth, position);
            }
            else
            {
                parent.nodeType = NodeType.PreRamp;
                parent.UpdateName();
            }

            GenerateJump(slope, parent);

            if (_addStraightNodeAfter)
            {
                parent = slope.nodes.Last();
                var position = parent.transform.position + NodeUtilities.CalculateNodeDirection(parent, _straightTurn, _straightDrop, _straightLength, _straightTurnFlipCoin);
                NodeUtilities.CreateChildNode(slope, NodeType.Straight, parent, _straightWidth, _straightDepth, position);
            }

            DoneApplying();
        }

        private Node GenerateJump(SlopeManager slope, Node parentNode)
        {
            var rampHeight = _rampSizeHeight.value;
            var rampLength = _rampSizeLength.value;

            var parentPos = parentNode.transform.position;

            Vector3 dir;
            if (_usePreviousNodeDrop)
            {
                dir = parentNode.transform.forward;
            }
            else
            {
                dir = parentNode.transform.forward;
                dir.y = 0f;
                dir.Normalize();
            }

            var endPos = parentPos + dir * rampLength;
            var dirToEnd = endPos - parentPos;

            var dirToEndXZ = dirToEnd;
            dirToEnd.y = 0;
            var dirToEndMag = dirToEnd.magnitude;

            var segmentLength = _rampSegmentLength.value;
            var countNodes = dirToEndMag / segmentLength;

            for (int i = 0; i < countNodes; i++)
            {
                var rampLengthToCurrentNode = segmentLength * (i + 1);
                var fractionOfTotalRampLength = rampLengthToCurrentNode / dirToEndMag;
                var y = (1f - _rampCurve.Evaluate(fractionOfTotalRampLength)) * rampHeight;
                var nodePosition = parentPos + (dirToEndXZ * fractionOfTotalRampLength) + (Vector3.down * y);

                var node = NodeUtilities.CreateChildNode(slope, NodeType.Ramp, parentNode, _rampWidth, _rampDepth, nodePosition);

                parentNode.UpdateLookDir();
                parentNode = node;
            }

            parentNode.nodeType = NodeType.Jump;
            parentNode.width = new Vector2(_jumpWidth.value, _jumpWidth.value);
            parentNode.depth = new Vector2(_jumpDepth.value, _jumpDepth.value);
            parentNode.UpdateName();
            parentNode.transform.rotation = parentNode.parents[0].transform.rotation;

            return GenerateLandingNode(slope, parentNode);
        }

        private Node GenerateLandingNode(SlopeManager slope, Node parentNode)
        {
            var localDir = NodeUtilities.CalculateNodeDirection(parentNode, _jumpTurn, _jumpDropAngle, _jumpLength, _turnFlipCoin);
            var node = NodeUtilities.CreateChildNode(slope, NodeType.Landing, parentNode, _landingWidth, _landingDepth, parentNode.transform.position + localDir);

            parentNode.UpdateLookDir();
            node.transform.rotation = parentNode.transform.rotation;

            // another node for the landing params
            parentNode = node;
            node = NodeUtilities.CreateChildNode(slope, NodeType.Straight, parentNode, _landingWidth, _landingDepth);
            localDir = NodeUtilities.CalculateNodeDirection(parentNode, _landingTurn, _landingDropAngle, _landingLength, _turnFlipCoin);
            node.transform.position = parentNode.transform.position + localDir;

            parentNode.UpdateLookDir();
            node.transform.rotation = parentNode.transform.rotation;

            return node;
        }

        public override void Clear()
        {
            base.Clear();

            DoneClearing();
        }
    }
}