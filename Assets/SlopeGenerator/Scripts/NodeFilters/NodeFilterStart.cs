namespace Slope
{
    using Helpers;
    using UnityEngine;

    public sealed class NodeFilterStart : SlopeFilter
    {
        [SerializeField]
        private SmartRange _width = new SmartRange(20f);

        [SerializeField]
        private SmartRange _depth = new SmartRange(10f);

        [SerializeField]
        private SmartRange _turn = new SmartRange(0);

        [SerializeField]
        private SmartRange _drop = new SmartRange(0f);

        [SerializeField]
        private SmartRange _length = new SmartRange(20f);

        [SerializeField]
        private bool _turnFlipCoin = false;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var startNode = NodeUtilities.CreateNode(slope, NodeType.Start);

            var pos = slope.nodesParent.position;
            var rot = slope.nodesParent.rotation;

            // if slope has more than just the new startNode, make startNode a child of the last node in the slope and position it there. 
            if (slope.nodes.Count > 1)
            {
                pos = slope.nodes[slope.nodes.Count - 2].transform.position;
                rot = slope.nodes[slope.nodes.Count - 2].transform.rotation;
                startNode.parents.Add(slope.nodes[slope.nodes.Count - 2]);
                slope.nodes[slope.nodes.Count - 2].children.Add(startNode);
            }

            startNode.transform.position = pos;
            startNode.transform.rotation = rot;

            startNode.width = new Vector2(_width.value, _width.value);
            startNode.depth = new Vector2(_depth.value, _depth.value);

            var localDir = NodeUtilities.CalculateNodeDirection(startNode, _turn, _drop, _length, _turnFlipCoin);
            var child = NodeUtilities.CreateChildNode(slope, NodeType.Straight, startNode, _width, _depth, startNode.transform.position + localDir);

            startNode.UpdateLookDir();
            child.transform.rotation = startNode.transform.rotation;


            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();
            this.slope.ClearNodes();

            DoneClearing();
        }
    }
}