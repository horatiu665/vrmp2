namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public sealed class NodeFilterSplines : SlopeFilter
    {
        [Header("Splines")]
        [SerializeField]
        private int _splineSeparationIterations = 1;

        [SerializeField]
        private float[] _distanceThresholdPerIteration = new float[] { 10f };

        [SerializeField]
        private float _ruggedness = 0f;

        [Tooltip("Limits the distance to parent and grandchild vectors when splining, to avoid overshooting when having large difference between adjacent node distances. Zero for ignoring the effect.")]
        [SerializeField]
        private float _maxSplineTangentDist = 0;

        [SerializeField]
        private NodeType[] _splinableNodeTypes = new NodeType[]
        {
            NodeType.Start,
            NodeType.Straight,
            NodeType.Split,
            NodeType.Merge,
            NodeType.Goal
        };

        [Header("Separation")]
        [SerializeField]
        private bool _applySeparation = true;

        [SerializeField]
        private float _minimumSeparation = 10f;

        [Header("Debugging")]
        [SerializeField, ReadOnly]
        private List<Node> _splineNodes = new List<Node>();

        private HashSet<NodeType> _allowedNodeTypes;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (_distanceThresholdPerIteration.Length == 0 || _distanceThresholdPerIteration.Length < _splineSeparationIterations)
            {
                Debug.LogError(this.ToString() + " must have at least one distance threshold per iteration set - per spline separation iteration, cannot function otherwise.");
                return;
            }

            _allowedNodeTypes = new HashSet<NodeType>(_splinableNodeTypes, new NodeTypeComparer());

            for (int i = 0; i < _splineSeparationIterations; i++)
            {
                CreateSplineNodes(slope);

                if (_applySeparation)
                {
                    SeparateNodes(slope);
                }
            }

            DoneApplying();
        }

        private void CreateSplineNodes(SlopeManager slope)
        {
            var nodes = slope.nodes;

            for (int i = 0; i < _distanceThresholdPerIteration.Length; i++)
            {
                var threshold = _distanceThresholdPerIteration[i];
                var splinables = new List<Node[]>();

                var nodesCount = nodes.Count;
                for (int j = 0; j < nodesCount; j++)
                {
                    var splines = GetSplinables(nodes[j]);
                    if (splines == null)
                    {
                        continue;
                    }

                    splinables.AddRange(splines);
                }

                var splinablesCount = splinables.Count;
                for (int j = 0; j < splinablesCount; j++)
                {
                    CreateSplinesFromSplinables(slope, splinables[j], threshold, _ruggedness);
                }
            }
        }

        private void CreateSplinesFromSplinables(SlopeManager slope, Node[] splinables, float threshold, float ruggedness)
        {
            var node = splinables[1];
            var child = splinables[2];

            var dist = (node.transform.position - child.transform.position).magnitude;
            if (dist < threshold)
            {
                return;
            }

            var divisions = Mathf.FloorToInt(dist / threshold);
            var parent = splinables[0];
            var grandchild = splinables[3];
            CreateSplines(slope, parent.transform.position, grandchild.transform.position, node, child, divisions);
        }

        private void CreateSplines(SlopeManager slope, Vector3 parentPos, Vector3 grandchildPos, Node node, Node child, int divisions)
        {
            var step = 1f / divisions;

            // first spline's parent is the node, but each subseq. parent is the newly created spline
            var splineParent = node;
            for (float t = step; t < 1f; t += step)
            {
                // get position on spline curve
                Vector3 pos;
                if (_maxSplineTangentDist == 0)
                {
                    pos = Utilities.ReturnCatmullRom(t, parentPos, node.transform.position, child.transform.position, grandchildPos);
                }
                else
                {
                    var grandChildDist = (grandchildPos - child.transform.position).magnitude;
                    var grandChildAdjusted = child.transform.position + (grandchildPos - child.transform.position).normalized * Mathf.Min(grandChildDist, _maxSplineTangentDist);
                    var parentDist = (parentPos - node.transform.position).magnitude;
                    var parentPosAdjusted = node.transform.position + (parentPos - node.transform.position).normalized * Mathf.Min(parentDist, _maxSplineTangentDist);
                    pos = Utilities.ReturnCatmullRom(t, parentPosAdjusted, node.transform.position, child.transform.position, grandChildAdjusted);
                }

                if (_ruggedness > 0f)
                {
                    pos.y += Random.Range(-_ruggedness, _ruggedness);
                }

                // save ref. to new node, to use in next node calculation
                splineParent = CreateSplineNode(slope, node, splineParent, child, pos, t);
            }
        }

        private Node CreateSplineNode(SlopeManager slope, Node firstNode, Node parentNode, Node childNode, Vector3 position, float lerpFactor)
        {
            var splineNode = NodeUtilities.CreateNode(NodeType.Straight);
            splineNode.slope = slope;
            splineNode.transform.SetParent(slope.nodesParent, true);
            splineNode.name = string.Concat(splineNode.name, " (Spline)");
            splineNode.transform.position = position;
            splineNode.depth = Vector2.Lerp(firstNode.depth, childNode.depth, Mathf.SmoothStep(0, 1f, lerpFactor));
            splineNode.width = Vector2.Lerp(firstNode.width, childNode.width, Mathf.SmoothStep(0, 1f, lerpFactor));
            splineNode.parents.Add(parentNode);
            splineNode.children.Add(childNode);
            splineNode.UpdateLookDir();

            parentNode.children.Remove(childNode);
            parentNode.children.Add(splineNode);
            parentNode.UpdateLookDir();

            childNode.parents.Remove(parentNode);
            childNode.parents.Add(splineNode);

            _splineNodes.Add(splineNode);

            var parentIndex = slope.nodes.IndexOf(parentNode) + 1;
            slope.nodes.Insert(parentIndex, splineNode);
            return splineNode;
        }

        private List<Node[]> GetSplinables(Node node)
        {
            if (node.parents.Count == 0)
            {
                return null;
            }

            var count = node.children.Count;
            if (count == 0)
            {
                return null;
            }

            var parent = node.parents[0];
            var list = new List<Node[]>(count);
            for (int i = 0; i < count; i++)
            {
                var child = node.children[i];
                var spline = GetSplines(parent, node, child);

                if (spline != null)
                {
                    list.Add(spline);
                }
            }

            return list;
        }

        private Node[] GetSplines(Node parent, Node node, Node child)
        {
            if (child.children.Count == 0)
            {
                return null;
            }

            if (!_allowedNodeTypes.Contains(node.nodeType))
            {
                return null;
            }

            //if (!_allowedNodeTypes.Contains(child.nodeType))
            //{
            //    return null;
            //}

            return new Node[] { parent, node, child, child.children[0] };
        }

        private void SeparateNodes(SlopeManager slope)
        {
            var lineSegments = CreateLineSegments(slope);
            var count = lineSegments.Count;
            for (int i = 0; i < count; i++)
            {
                var lineSegment = lineSegments[i];
                var separationVector = GetSeparationVector(lineSegment, lineSegments, _minimumSeparation);
                TransformNodeAndChildren(lineSegment.n1, separationVector);
            }
        }

        private void TransformNodeAndChildren(Node node, Vector3 direction)
        {
            node.transform.position += direction;

            if (direction == Vector3.zero)
            {
                return;
            }

            var children = node.children;
            var count = children.Count;

            for (int i = 0; i < count; i++)
            {
                TransformNodeAndChildren(children[i], direction);
            }
        }

        private List<LineSegment> CreateLineSegments(SlopeManager slope)
        {
            var nodes = slope.nodes;
            var count = nodes.Count;

            var list = new List<LineSegment>(count);
            for (int i = 0; i < count; i++)
            {
                var node = nodes[i];
                var children = node.children;
                for (int j = 0; j < children.Count; j++)
                {
                    var child = children[j];
                    var ls = new LineSegment(node, child);
                    list.Add(ls);
                }
            }

            return list;
        }

        private Vector3 GetSeparationVector(LineSegment n1, List<LineSegment> lineSegments, float minSeparation)
        {
            var totalSeparationVector = Vector3.zero;
            var count = lineSegments.Count;
            var countVectors = 0;

            for (int j = 0; j < count; j++)
            {
                var n2 = lineSegments[j];
                if (n2.Equals(n1) || n2.p1 == n1.p1 || n2.p1 == n1.p2)
                {
                    continue;
                }

                var i1 = new Vector2();
                var v11 = new Vector2(n1.p1.x, n1.p1.z);
                var v12 = new Vector2(n1.p2.x, n1.p2.z);
                var v21 = new Vector2(n2.p1.x, n2.p1.z);
                var v22 = new Vector2(n2.p2.x, n2.p2.z);

                var IsIntersecting = Utilities.LineSegmentsIntersect(
                    v11,
                    v12,
                    v21,
                    v22,
                    out i1);

                if (!IsIntersecting || i1.Equals(v11) || i1.Equals(v12) || i1.Equals(v21) || i1.Equals(v22))
                {
                    continue;
                }

                var intersection = new Vector3(i1.x, 0f, i1.y);
                var z = intersection.z;
                var p1 = n1.GetPositionFromZ(z);
                var p2 = n2.GetPositionFromZ(z);

                //if n1 is on top, don't move it, as we only move downwards
                if (p1.y > p2.y)
                {
                    continue;
                }

                var dir = p1 - p2;
                var dist = dir.magnitude;
                if (dist > minSeparation)
                {
                    continue;
                }

                var separationVector = dir.normalized * (minSeparation - dist);
                totalSeparationVector += separationVector;
                countVectors++;
            }

            if (countVectors > 1)
            {
                totalSeparationVector /= countVectors;
            }

            return totalSeparationVector;
        }

        public override void Clear()
        {
            base.Clear();
            var count = _splineNodes.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                var node = _splineNodes[i];
                if (node == null || node.Equals(null))
                {
                    continue;
                }

                if (node.parents.Count > 1 || node.children.Count > 1)
                {
                    Debug.LogWarning(this.ToString() + " cannot remove nodes that have more than one child or parent, conflicting node == " + node.ToString());
                    continue;
                }

                var parent = node.parents[0];
                var child = node.children[0];

                parent.children.Remove(node);
                parent.children.Add(child);

                child.parents.Remove(node);
                child.parents.Add(parent);

                this.DestroySafe(node.gameObject);
            }

            _splineNodes.Clear();

            DoneClearing();
        }
    }
}