namespace Slope
{
    using Helpers;
    using UnityEngine;
    using VRCore;

    public sealed class NodeFilterSymmetricalSplit : SlopeFilter
    {
        [Header("Split")]
        [SerializeField]
        private SmartRange _nodesCount = new SmartRange(10f, 20f);

        [SerializeField]
        private AnimationCurve _splitTurnCurve = AnimationCurve.EaseInOut(0f, 1f, 1f, 0f);

        [SerializeField]
        private SmartRange _ruggedness = new SmartRange(-5f, 5f);

        [SerializeField]
        private SmartRange _drop = new SmartRange(-10f, -30f);

        [SerializeField]
        private SmartRange _length = new SmartRange(20f, 40f);

        [SerializeField]
        private SmartRange _turn = new SmartRange(20f, 40f);

        [SerializeField]
        private SmartRange _width = new SmartRange(10f);

        [SerializeField]
        private SmartRange _depth = new SmartRange(10f);

        [Header("Extra Nodes")]
        [SerializeField]
        private bool _createStraightNodeBefore = true;

        [SerializeField]
        private bool _createStraightNodeAfter = true;

        [SerializeField]
        private bool _turnFlipCoin = true;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (slope.nodes.Count == 0)
            {
                Debug.LogError(this.ToString() + " cannot operate when the slope has no nodes!");
                return;
            }

            var parent = slope.nodes.Last();
            if (_createStraightNodeBefore)
            {
                var dir = NodeUtilities.CalculateNodeDirection(parent, _turn, _drop, _length, _turnFlipCoin);
                var start = NodeUtilities.CreateChildNode(slope, NodeType.Split, parent, _width, _depth, parent.transform.position + dir);
                parent = start;
            }
            else
            {
                parent.nodeType = NodeType.Split;
                parent.UpdateName();
            }

            var negParent = parent;
            var posParent = parent;

            var nodeCount = _nodesCount.intValue;
            for (int i = 0; i < nodeCount; i++)
            {
                var factor = i / (float)nodeCount;
                var turn = _turn.value * _splitTurnCurve.Evaluate(factor);
                var drop = _drop.value;
                var length = _length.value;
                var turnFactor = factor < 0.5f ? -1f : 1f;

                var negDir = NodeUtilities.CalculateNodeDirection(negParent, (turn * turnFactor) + _ruggedness.value, drop, length + _ruggedness.value);
                var negNode = NodeUtilities.CreateChildNode(slope, NodeType.Straight, negParent, _width, _depth, negParent.transform.position + negDir);

                negParent.UpdateLookDir();
                negParent = negNode;

                var posDir = NodeUtilities.CalculateNodeDirection(posParent, (turn * -turnFactor) + _ruggedness.value, drop, length + _ruggedness.value);
                var posNode = NodeUtilities.CreateChildNode(slope, NodeType.Straight, posParent, _width, _depth, posParent.transform.position + posDir);

                posParent.UpdateLookDir();
                posParent = posNode;
            }

            var startPos = negParent.transform.position + ((posParent.transform.position - negParent.transform.position) * 0.5f);
            startPos.y = Mathf.Min(negParent.transform.position.y, posParent.transform.position.y) + _drop.value;
            var forward = (negParent.transform.forward + posParent.transform.forward) * _length.value;
            var mergeNode = NodeUtilities.CreateChildNode(slope, NodeType.Merge, negParent, _width, _depth, startPos + forward);

            mergeNode.parents.Add(posParent);
            posParent.children.Add(mergeNode);
            posParent.UpdateLookDir();

            if (_createStraightNodeAfter)
            {
                var dir = NodeUtilities.CalculateNodeDirection(mergeNode, _turn, _drop, _length, _turnFlipCoin);
                NodeUtilities.CreateChildNode(slope, NodeType.Straight, mergeNode, _depth, _width, mergeNode.transform.position + dir);
            }

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();
            this.slope.ClearNodes();

            DoneClearing();
        }
    }
}