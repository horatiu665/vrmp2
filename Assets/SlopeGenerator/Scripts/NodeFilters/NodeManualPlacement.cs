﻿namespace Slope
{
    using System.Collections.Generic;
    using Apex;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public sealed class NodeManualPlacement : MonoBehaviour
    {
        [Space]
        [SerializeField, Tooltip("Created nodes will use this node as a parent. If none is set, the last node in the list will be used.")]
        private Node _parentNode = null;

        [Space]
        [SerializeField, Tooltip("Maximum distance from the parent node.")]
        private float _maxDistance = 20f;

        [Header("Node Attributes")]
        [SerializeField]
        private NodeType _nodeType = NodeType.Straight;

        [SerializeField]
        private SmartRange _width = new SmartRange(20f, 30f);

        [SerializeField]
        private SmartRange _depth = new SmartRange(20f, 30f);

        [Header("Random Factors")]
        [SerializeField]
        private SmartRange _drop = new SmartRange(-5f, 0f);

        [SerializeField]
        private SmartRange _length = new SmartRange(-5f, 5f);

        [SerializeField]
        private SmartRange _turn = new SmartRange(-5f, 5f);

        [Header("Debugging")]
        [SerializeField, ReadOnly]
        private List<Node> _nodes = new List<Node>();

        private SlopeManager _slope;

        public SlopeManager slope
        {
            get
            {
                if (_slope == null)
                {
                    _slope = this.GetComponentInParent<SlopeManager>();
                }

                return _slope;
            }
        }

        public Node parentNode
        {
            get
            {
                if (_parentNode == null || _parentNode.Equals(null))
                {
                    _parentNode = this.slope.nodes.Last();
                }

                return _parentNode;
            }
        }

        public void DeleteLast()
        {
            var currentParent = this.parentNode;
            if (currentParent == null)
            {
                Debug.LogWarning(this.ToString() + " no nodes to delete");
                return;
            }

            if (currentParent.parents.Count > 0)
            {
                _parentNode = currentParent.parents[0];
            }
            else
            {
                _parentNode = null;
            }

            _nodes.Remove(currentParent);
            this.DestroySafe(currentParent.gameObject);
        }

        public void DeleteAll()
        {
            var count = _nodes.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                this.DestroySafe(_nodes[i].gameObject);
            }

            _nodes.Clear();
            _parentNode = null;
        }

        public void NewNode(Vector3 position)
        {
            var slope = this.slope;
            if (slope.nodes.Count == 0)
            {
                var startNode = NodeUtilities.CreateNode(slope, NodeType.Start);
                startNode.transform.position = position;
                _parentNode = startNode;
                _nodes.Add(startNode);
                return;
            }

            position = GetValidPosition(position);
            var parent = this.parentNode;

            var positionDir = (position - parent.transform.position);
            var localDir = NodeUtilities.CalculateNodeDirection(positionDir, _turn.value, _drop.value, positionDir.OnlyXZ().magnitude + _length.value);
            var finalPos = parent.transform.position + localDir;
            var newNode = NodeUtilities.CreateChildNode(slope, _nodeType, parent, _width, _depth, finalPos);
            newNode.UpdateName();
            parent.UpdateLookDir();

            if (parent.children.Count > 1)
            {
                parent.nodeType = NodeType.Split;
                parent.UpdateName();
            }

            _nodes.Add(newNode);
            _parentNode = newNode;
        }

        public Vector3 GetValidPosition(Vector3 position)
        {
            var parent = this.parentNode;
            var dir = (position - parent.transform.position);
            var magnitude = dir.magnitude;
            if (magnitude > _maxDistance)
            {
                return parent.transform.position + ((dir / magnitude) * _maxDistance);
            }

            return position;
        }
    }
}