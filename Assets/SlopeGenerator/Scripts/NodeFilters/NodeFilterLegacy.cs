namespace Slope
{
    using System.Collections.Generic;
    using System.Linq;
    using Helpers;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    /// <summary>
    /// Provides the oldschool functionality of the SlopeManager, spawning all the nodes with all the parameters.
    /// </summary>
    [System.Obsolete]
    public class NodeFilterLegacy : SlopeFilter
    {
        [SerializeField, FormerlySerializedAs("createSplines")]
        private bool _createSplines = false;

        [SerializeField, FormerlySerializedAs("createSeparation")]
        private bool _createSeparation = true;

        [SerializeField, FormerlySerializedAs("splineSeparationIterations")]
        private int _splineSeparationIterations = 1;

        [SerializeField, FormerlySerializedAs("splineParameters")]
        private SplineParameters _splineParameters = null;

        //start
        [SerializeField, FormerlySerializedAs("startParameters")]
        private StartParameters _startParameters = null;

        [SerializeField, FormerlySerializedAs("separationParameters")]
        private SeparationParameters _separationParameters = null;

        //straight
        [SerializeField, FormerlySerializedAs("straightParameters")]
        private List<StraightParameters> _straightParameters = new List<StraightParameters>();

        //jump
        [SerializeField]
        private List<JumpCurveParameters> _jumpCurveParameters = new List<JumpCurveParameters>();

        //jump
        [SerializeField, FormerlySerializedAs("jumpParameters")]
        private List<JumpParameters> _jumpParameters = new List<JumpParameters>();

        //secondary lanes
        [SerializeField, FormerlySerializedAs("lanesParameters")]
        private List<LanesParameters> _lanesParameters = new List<LanesParameters>();

        [SerializeField, FormerlySerializedAs("goalParameters")]
        private GoalParameters _goalParameters = null;

        private List<Node> nodes
        {
            get
            {
                return slope.nodes;
            }
        }

        [SerializeField, ReadOnly]
        private Node _goalNode = null;

        [SerializeField, ReadOnly]
        private Node _startNode = null;

        public GoalParameters goalParameters
        {
            get { return _goalParameters; }
        }

        public List<StraightParameters> straightParameters
        {
            get { return _straightParameters; }
            set { _straightParameters = value; }
        }

        public StartParameters startParameters
        {
            get { return _startParameters; }
        }

        public Transform nodesParent
        {
            get
            {
                return slope.nodesParent;
            }
        }

        public Node goalNode
        {
            get { return _goalNode; }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            GenerateNodes();

            DoneApplying();
        }

        #region Apply

        public void GenerateNodes()
        {
            nodesParent.localPosition = Vector3.zero;
            nodesParent.localRotation = Quaternion.identity;

            var parentNode = GenerateStartNode();

            var slopeNodes = GenerateSlopeNodeList();

            GenerateJumpCurveParamsList(slopeNodes);
            GenerateJumpParamsList(slopeNodes);

            var count = slopeNodes.Count;

            for (int i = 0; i < count; i++)
            {
                var slopeNode = slopeNodes[i];
                var index = slopeNode.parameterIndex;

                switch (slopeNode.nodeType)
                {
                    case NodeType.Straight:
                    parentNode = GenerateStraightNode(parentNode, _straightParameters[index]);
                    break;

                    case NodeType.Jump:
                    parentNode = GenerateJump(parentNode, _jumpParameters[index]);
                    break;

                    case NodeType.JumpCurve:
                    parentNode = GenerateJumpCurve(parentNode, _jumpCurveParameters[index]);
                    break;
                }
            }

            GenerateGoalNode(parentNode);
            GenerateLanes();

            for (int i = 0; i < _splineSeparationIterations; i++)
            {
                if (_createSplines)
                {
                    CreateSplineNodes();
                }

                if (_createSeparation)
                {
                    SeparateNodes();
                }
            }
        }

        private void GenerateJumpParamsList(List<SlopeNode> slopeNodes)
        {
            List<int> indexesAlreadyTurned = new List<int>();
            var countJumpParameters = _jumpParameters.Count;
            for (int i = 0; i < countJumpParameters; i++)
            {
                var jumpParams = _jumpParameters[i];

                for (int j = 0; j < jumpParams.countJumps; j++)
                {
                    var index = Mathf.Clamp(Random.Range(jumpParams.minNodeIndex, jumpParams.maxNodeIndex), 1, slopeNodes.Count - 1);

                    // do not add the same index more than once
                    // but we are too lazy to properly check so we use MonteCarlo
                    int maxIterations = 10;
                    while (slopeNodes[index].nodeType == NodeType.JumpCurve && maxIterations-- > 0)
                    {
                        index = Mathf.Clamp(Random.Range(jumpParams.minNodeIndex, jumpParams.maxNodeIndex), 1, slopeNodes.Count - 1);
                    }
                    indexesAlreadyTurned.Add(index);

                    var jumpNode = slopeNodes[index];
                    jumpNode.nodeType = NodeType.Jump;
                    jumpNode.parameterIndex = i;
                    //print("Turning node " + index + " into a JUMP (no curve) node");
                }
            }
        }

        private void GenerateJumpCurveParamsList(List<SlopeNode> slopeNodes)
        {
            List<int> indexesAlreadyTurned = new List<int>();
            var countJumpCurveParameters = _jumpCurveParameters.Count;
            for (int i = 0; i < countJumpCurveParameters; i++)
            {
                var jumpCurveParams = _jumpCurveParameters[i];

                for (int j = 0; j < jumpCurveParams.countJumps; j++)
                {
                    var index = Mathf.Clamp(Random.Range(jumpCurveParams.minNodeIndex, jumpCurveParams.maxNodeIndex), 1, slopeNodes.Count - 1);

                    // do not add the same index more than once
                    // but we are too lazy to properly check so we use MonteCarlo
                    int maxIterations = 15;
                    int closestNodeIndex = -1;
                    while (maxIterations-- > 0 && Mathf.Abs(index - closestNodeIndex) <= jumpCurveParams.minNodesBetween)
                    {
                        index = Mathf.Clamp(Random.Range(jumpCurveParams.minNodeIndex, jumpCurveParams.maxNodeIndex), 1, slopeNodes.Count - 1);
                        // find closest node and save index in closestNode;
                        for (int k = 0; k < indexesAlreadyTurned.Count; k++)
                        {
                            if (Mathf.Abs(indexesAlreadyTurned[k] - index) < Mathf.Abs(index - closestNodeIndex))
                            {
                                closestNodeIndex = indexesAlreadyTurned[k];
                            }
                        }
                    }
                    indexesAlreadyTurned.Add(index);

                    var jumpCurveNode = slopeNodes[index];
                    jumpCurveNode.nodeType = NodeType.JumpCurve;
                    jumpCurveNode.parameterIndex = i;
                    //print("Turning node " + index + " into a jump CURVE node");
                }
            }
        }

        private List<SlopeNode> GenerateSlopeNodeList()
        {
            var count = _straightParameters.Count;
            var list = new List<SlopeNode>();
            for (int i = 0; i < count; i++)
            {
                var s = _straightParameters[i];

                var countNodes = Random.Range(s.nodesMin, s.nodesMax);

                for (int j = 0; j < countNodes; j++)
                {
                    list.Add(new SlopeNode(NodeType.Straight, i));
                }
            }

            return list;
        }

        private Node GenerateJump(Node parentNode, JumpParameters j)
        {
            Node node;

            foreach (var rampSection in j.ramp)
            {
                parentNode.nodeType = NodeType.Ramp;
                node = CreateChildNode(parentNode, nodes, rampSection, NodeType.None); // Node type is set later through methods, so just set None for now
                parentNode.transform.LookAt(node.transform.position);
                parentNode = node;
            }

            node = GenerateJumpNode(parentNode, j.jump);
            node = GenerateLandingNode(parentNode, j.landing);

            ////jump
            //parentNode.nodeType = NodeType.Jump;
            //node = CreateChildNode(parentNode, _nodes, j.jump);
            //parentNode.transform.LookAt(node.transform.position);
            //parentNode = node;

            ////landing
            //parentNode.nodeType = NodeType.Landing;
            //node = CreateChildNode(parentNode, _nodes, j.landing);
            //parentNode.transform.LookAt(node.transform.position);
            //node.nodeType = NodeType.Straight;

            return node;
        }

        private Node GenerateJumpCurve(Node parentNode, JumpCurveParameters j)
        {
            var rampHeight = j.rampSizeHeight.Random();
            var rampLength = j.rampSizeLength.Random();

            var parentPos = parentNode.transform.position;

            Vector3 dir;
            if (j.usePreviousNodeDrop)
            {
                dir = parentNode.transform.forward;
            }
            else
            {
                dir = parentNode.transform.forward;
                dir.y = 0;
                dir.Normalize();
            }
            //var dirNorm = dir.normalized;

            var endPos = parentPos + dir * rampLength;
            var dirToEnd = endPos - parentPos;

            var dirToEndXZ = dirToEnd;
            dirToEnd.y = 0;
            var dirToEndMag = dirToEnd.magnitude;

            var segmentLength = j.rampCurveSegmentLength;
            var countNodes = dirToEndMag / segmentLength;

            for (int i = 0; i < countNodes; i++)
            {
                var rampLengthToCurrentNode = segmentLength * (i + 1);
                var fractionOfTotalRampLength = rampLengthToCurrentNode / dirToEndMag;

                var y = (1 - j.rampCurve.Evaluate(fractionOfTotalRampLength)) * rampHeight;

                var nodePosition = parentPos + dirToEndXZ * fractionOfTotalRampLength + Vector3.down * y;

                parentNode.nodeType = NodeType.Ramp;
                var node = CreateChildNode(parentNode, nodes, nodePosition, j.widthSpan, j.depthSpan, NodeType.None); // Node type is set later through methods, so just set None for now
                parentNode.transform.LookAt(node.transform.position);
                parentNode = node;
            }

            parentNode = GenerateJumpNode(parentNode, j.jump);
            parentNode = GenerateLandingNode(parentNode, j.landing);

            return parentNode;
        }

        private Node GenerateJumpNode(Node parentNode, SectionParameters s)
        {
            //jump
            parentNode.nodeType = NodeType.Jump;
            var node = CreateChildNode(parentNode, nodes, s, NodeType.None); // Node type is set later through methods, so just set None for now
            parentNode.transform.LookAt(node.transform.position);
            return node;
        }

        private Node GenerateLandingNode(Node parentNode, SectionParameters s)
        {
            //landing
            parentNode.nodeType = NodeType.Landing;
            var node = CreateChildNode(parentNode, nodes, s, NodeType.Straight);
            parentNode.transform.LookAt(node.transform.position);

            return node;
        }

        private Node GenerateStraightNode(Node parentNode, StraightParameters p)
        {
            var childNode = CreateChildNode(parentNode, nodes, p, NodeType.Straight);
            var p1 = childNode.transform.position;
            parentNode.transform.LookAt(p1);
            return childNode;
        }

        private void GenerateGoalNode(Node parentNode)
        {
            if (_goalParameters.usePreviousNode)
            {
                var dir = Vector3.zero;
                // same orientation as the direction formed by it and its parent node(s)
                if (parentNode.parents.Count == 1)
                {
                    dir = parentNode.transform.position - parentNode.parents[0].transform.position;
                }
                else
                {
                    for (int i = 0; i < parentNode.parents.Count; i++)
                    {
                        var ppos = parentNode.parents[i].transform.position;
                        var toNode = parentNode.transform.position - ppos;
                        dir += toNode;
                    }
                }

                _goalNode = CreateChildNode(parentNode, dir.normalized, parentNode.width, parentNode.depth, NodeType.Goal);
                nodes.Add(_goalNode);
                goalNode.transform.LookAt(goalNode.transform.position + dir);
            }
            else
            {
                _goalNode = CreateChildNode(parentNode, nodes, _goalParameters, NodeType.Goal);
                var p1 = goalNode.transform.position;
                parentNode.transform.LookAt(p1);
            }
        }

        /// <summary>
        /// Can be used before generating the slope to set the start node based on another node (so the new slope will integrate seamlessly with another slope or structure)
        /// </summary>
        /// <param name="oldNode">A node which the new one will be based on (identical to)</param>
        public void SetStartNode(Node oldNode)
        {
            startParameters.copyNode = oldNode;
        }

        private void JakesFunctionForRandomizingWidthAndDepth(Node node, Vector2 widthSpan, Vector2 depthSpan)
        {
            node.width = JakesFunctionForTwoRandomValues(widthSpan);
            node.depth = JakesFunctionForTwoRandomValues(depthSpan);
        }

        private Vector2 JakesFunctionForTwoRandomValues(Vector2 span)
        {
            return new Vector2(span.Random(), span.Random());
        }

        private Node GenerateStartNode()
        {
            if (startParameters.copyNode != null)
            {
                // create start node based on other node
                _startNode = CreateBasicNode(startParameters.copyNode.transform.position, NodeType.Start);
                _startNode.transform.rotation = startParameters.copyNode.transform.rotation;
                // we don't want random! we want the same!
                //JakesFunctionForRandomizingWidthAndDepth(_startNode, startParameters.copyNode.width, startParameters.copyNode.depth);
                //_startNode.UpdateVertices(startParameters.copyNode.width, startParameters.copyNode.depth);
                _startNode.width = startParameters.copyNode.width;
                _startNode.depth = startParameters.copyNode.depth;
                _startNode.nodeType = NodeType.Start;
                nodes.Add(_startNode);

                _startNode.parents.Add(startParameters.copyNode);

                var dir = _startNode.transform.forward;
                var startBandLength = _splineParameters.distanceThresholdPerIteration[0];
                var childNode = CreateChildNode(_startNode, dir * startBandLength, _startNode.width, _startNode.depth, NodeType.Straight);
                nodes.Add(childNode);
                childNode.transform.LookAt(childNode.transform.position + dir);

                return childNode;
            }
            else
            {
                _startNode = CreateBasicNode(Vector3.zero, NodeType.Start);
                _startNode.transform.localPosition = Vector3.zero;
                _startNode.transform.localRotation = Quaternion.identity;

                JakesFunctionForRandomizingWidthAndDepth(_startNode, startParameters.widthSpan, startParameters.depthSpan);
                //_startNode.UpdateVertices(startParameters.widthSpan, startParameters.depthSpan);
                nodes.Add(_startNode);

                var childNode = CreateChildNode(_startNode, nodes, startParameters, NodeType.Straight);
                var p1 = childNode.transform.position;
                _startNode.transform.LookAt(p1);

                return childNode;
            }
        }

        private void GenerateLanes()
        {
            var lanes = new List<Queue<Node>>();

            //generate the lanes
            foreach (var laneParams in _lanesParameters)
            {
                var countLanes = laneParams.countLanes;

                for (int i = 0; i < countLanes; i++)
                {
                    var lane = GenerateOneLane(laneParams);

                    if (lane == null)
                    {
                        Debug.Log("lane == null");
                    }
                    else
                    {
                        lanes.Add(lane);
                    }
                }
            }

            //add the lanes to node graphs
            var count = lanes.Count;

            for (int i = 0; i < count; i++)
            {
                var lane = lanes[i];
                var nodeIndex = 0;
                while (lane.Count > 0)
                {
                    var node = lane.Dequeue();
                    node.name = "lane_" + i + "_" + nodeIndex++;
                    nodes.Add(node);
                }
            }
        }

        private Queue<Node> GenerateOneLane(LanesParameters l)
        {
            int splitNodeIndex = -1;
            int mergeNodeIndex = -1;
            Node splitNode = null;
            Node mergeNode = null;

            int monteCarloMaxIterations = 10;
            while (mergeNodeIndex <= splitNodeIndex && monteCarloMaxIterations-- > 0)
            {
                splitNode = GetSplitNode(l);

                if (splitNode == null)
                {
                    return null;
                }

                mergeNode = GetMergeNode(l);

                if (mergeNode == null)
                {
                    return null;
                }

                splitNodeIndex = nodes.IndexOf(splitNode);
                mergeNodeIndex = nodes.IndexOf(mergeNode);
            }

            var lane = CreateLaneNodes(splitNode, mergeNode, l);

            return lane;
        }

        private Node GetSplitNode(LanesParameters l)
        {
            var potentialSplitNodes = new List<Node>();

            for (int i = l.splitMin; i < l.splitMax + 1; i++)
            {
                var node = nodes[i];

                if (node.nodeType != NodeType.Straight)
                {
                    continue;
                }

                potentialSplitNodes.Add(node);
            }

            if (potentialSplitNodes.Count == 0)
            {
                Debug.Log("potentialSplitNodes.Count == 0");
                return null;
            }

            var splitIdx = Random.Range(0, potentialSplitNodes.Count);
            var splitNode = potentialSplitNodes[splitIdx];

            return splitNode;
        }

        private Node GetMergeNode(LanesParameters l)
        {
            var potentialMergeNodes = new List<Node>();

            for (int i = l.mergeMin; i < l.mergeMax + 1; i++)
            {
                var node = nodes[i];

                if (node.nodeType != NodeType.Straight)
                {
                    continue;
                }

                potentialMergeNodes.Add(node);
            }

            if (potentialMergeNodes.Count == 0)
            {
                Debug.Log("potentialMergeNodes.Count == 0");
                return null;
            }

            var mergeIdx = UnityEngine.Random.Range(0, potentialMergeNodes.Count);
            var mergeNode = potentialMergeNodes[mergeIdx];

            return mergeNode;
        }

        private Queue<Node> CreateLaneNodes(Node splitNode, Node mergeNode, LanesParameters l)
        {
            splitNode.nodeType = NodeType.Split;

            //var startIdx = _nodes.IndexOf(splitNode);
            //var endIdx = _nodes.IndexOf(mergeNode);

            //Debug.Log("startIdx == " + startIdx + " endIdx == " + endIdx);

            var splitNodePos = splitNode.transform.position;

            var lane = new Queue<Node>();

            var flipDir = Utilities.FlipCoin();

            var dir = mergeNode.transform.position - splitNode.transform.position;
            var orthoDir = Quaternion.Euler(0f, 90f, 0f) * dir;
            orthoDir.y = 0f;
            var orthoDirNorm = orthoDir.normalized;

            var countTurns = Random.Range(l.countTurnsMin, l.countTurnsMax);

            var averageDirPerTurn = dir / (countTurns + 1f);

            var parentNode = splitNode;

            var clearanceSpan = l.clearanceSpan;

            for (int i = 0; i < countTurns; i++)
            {
                var clearance = Random.Range(clearanceSpan.x, clearanceSpan.y);
                var dirFraction = averageDirPerTurn * (i + 1f);
                var turnPos = splitNodePos + dirFraction + orthoDirNorm * clearance * flipDir;
                var dirFromParentToTurn = turnPos - parentNode.transform.position;
                var childNode = CreateChildNode(parentNode, dirFromParentToTurn, l.widthSpan, l.depthSpan, NodeType.Straight);
                parentNode = childNode;
                lane.Enqueue(parentNode);

                //flip the direction for the next turn
                flipDir *= -1;
            }

            //the last node should merge with the main lane
            parentNode.children.Add(mergeNode);
            mergeNode.nodeType = NodeType.Merge;
            mergeNode.parents.Add(parentNode);

            return lane;
        }

        private Node CreateChildNode(Node parentNode, List<Node> nodes, SectionParameters p, NodeType type)
        {
            var localDir = CreateLocalDir(parentNode, p);
            var childNode = CreateChildNode(parentNode, localDir, p.widthSpan, p.depthSpan, type);
            nodes.Add(childNode);
            return childNode;
        }

        private Node CreateChildNode(Node parentNode, List<Node> nodes, Vector3 position, Vector2 widthSpan, Vector2 depthSpan, NodeType type)
        {
            var localDir = position - parentNode.transform.position;
            var childNode = CreateChildNode(parentNode, localDir, widthSpan, depthSpan, type);
            nodes.Add(childNode);
            return childNode;
        }

        private Node CreateChildNode(Node parentNode, Vector3 localDir, Vector2 widthSpan, Vector2 depthSpan, NodeType type)
        {
            var childNode = CreateBasicNode(GetChildPosition(parentNode, localDir), type);

            JakesFunctionForRandomizingWidthAndDepth(childNode, widthSpan, depthSpan);
            //childNode.UpdateVertices(widthSpan, depthSpan);
            childNode.parents.Add(parentNode);
            parentNode.children.Add(childNode);
            parentNode.UpdateLookDir();

            // this makes the slope turning become relative rather than absolute, so if we turn the start point, the whole slope follows. however the slope will have a new shape.
            childNode.transform.rotation = parentNode.transform.rotation;

            return childNode;
        }

        private Node CreateSplineNode(Node firstNode, Node parentNode, Node childNode, Vector3 position, float lerpFactor)
        {
            var splineNode = CreateBasicNode(position, NodeType.Straight);
            splineNode.depth = Vector2.Lerp(firstNode.depth, childNode.depth, Mathf.SmoothStep(0, 1f, lerpFactor));
            splineNode.width = Vector2.Lerp(firstNode.width, childNode.width, Mathf.SmoothStep(0, 1f, lerpFactor));
            splineNode.parents.Add(parentNode);
            splineNode.children.Add(childNode);
            splineNode.UpdateLookDir();

            parentNode.children.Remove(childNode);
            parentNode.children.Add(splineNode);
            parentNode.UpdateLookDir();

            childNode.parents.Remove(parentNode);
            childNode.parents.Add(splineNode);

            var parentIndex = nodes.IndexOf(parentNode);
            nodes.Insert(parentIndex + 1, splineNode);

            return splineNode;
        }

        private Node CreateBasicNode(Vector3 position, NodeType type)
        {
            var go = new GameObject();
            go.transform.position = position;
            go.transform.SetParent(this.nodesParent, true);

            var node = go.AddComponent<Node>();
            node.nodeType = type;

            return node;
        }

        private Vector3 GetChildPosition(Node parentNode, Vector3 localDir)
        {
            var parentpos = parentNode.transform.position;
            return parentpos + localDir;
        }

        private Vector3 CreateLocalDir(Node parentNode, SectionParameters p)
        {
            var parentFwd = parentNode.transform.forward;

            var localDir = parentFwd;

            localDir.y = 0;
            localDir.Normalize();

            var dir = 1f;

            if (p.randomFlipTurnDirection)
            {
                dir = Utilities.FlipCoin();
            }

            var turn = UnityEngine.Random.Range(p.turnSpan.x, p.turnSpan.y) * dir;
            localDir = Quaternion.Euler(0, turn, 0) * localDir;

            var drop = UnityEngine.Random.Range(p.dropSpan.x, p.dropSpan.y);
            var ortho = Quaternion.Euler(0, 90, 0) * localDir;
            localDir = Quaternion.AngleAxis(drop, -ortho) * localDir;

            var length = UnityEngine.Random.Range(p.lengthSpan.x, p.lengthSpan.y);
            localDir = localDir.normalized * length;

            return localDir;
        }

        public void CreateSplineNodes()
        {
            var thresholdPerIteration = _splineParameters.distanceThresholdPerIteration;
            var count = thresholdPerIteration.Count;
            if (count == 0)
            {
                return;
            }

            var nodesToSpline = nodes.ToList();

            for (int i = 0; i < count; i++)
            {
                var threshold = thresholdPerIteration[i];

                var splinables = new List<List<Node>>();

                foreach (var node in nodesToSpline)
                {
                    //CreateSplineNode(node, threshold, splineParameters.ruggedness);
                    var splines = GetSplinables(node);

                    if (splines == null)
                    {
                        continue;
                    }

                    foreach (var spline in splines)
                    {
                        splinables.Add(spline);
                    }
                }

                foreach (var splinable in splinables)
                {
                    CreateSplinesFromSplinables(splinable, threshold, _splineParameters.ruggedness);
                }
            }
        }

        /// <summary>
        /// Go through each child to check if it is splinable
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        private List<List<Node>> GetSplinables(Node node)
        {
            if (node.parents.Count == 0)
            {
                return null;
            }

            var countChildren = node.children.Count;
            if (countChildren == 0)
            {
                return null;
            }

            var parent = node.parents[0];
            var list = new List<List<Node>>();
            for (int i = 0; i < countChildren; i++)
            {
                var child = node.children[i];
                var spline = GetSplines(parent, node, child);

                if (spline != null)
                {
                    list.Add(spline);
                }
            }

            return list;
        }

        private List<Node> GetSplines(Node parent, Node node, Node child)
        {
            if (child.children.Count == 0)
            {
                return null;
            }

            if (!IsSplinableNodeType(node.nodeType))
            {
                return null;
            }

            if (!IsSplinableNodeType(child.nodeType))
            {
                return null;
            }

            var grandChild = child.children[0];
            return new List<Node> { parent, node, child, grandChild };
        }

        private void CreateSplinesFromSplinables(List<Node> splinables, float threshold, float ruggedness)
        {
            var parent = splinables[0];
            var node1 = splinables[1];
            var child = splinables[2];
            var grandchild = splinables[3];

            var dist = (node1.transform.position - child.transform.position).magnitude;

            if (dist < threshold)
            {
                return;
            }

            var divisions = Mathf.FloorToInt(dist / threshold);

            CreateSplines(parent.transform.position, grandchild.transform.position, node1, child, divisions, ruggedness);
        }

        private void CreateSplines(Vector3 parentPos, Vector3 grandchildPos, Node node, Node child, int divisions, float ruggedness)
        {
            var step = 1f / (divisions);

            // first spline's parent is the node, but each subseq. parent is the newly created spline
            var splineParent = node;

            for (float t = step; t < 1f; t += step)
            {
                // get position on spline curve
                var pos = Utilities.ReturnCatmullRom(t, parentPos, node.transform.position, child.transform.position, grandchildPos);
                pos.y += Random.Range(-ruggedness, ruggedness);
                // save ref. to new node, to use in next node calculation
                splineParent = CreateSplineNode(node, splineParent, child, pos, t);
            }
        }

        private bool IsSplinableNodeType(NodeType nodeType)
        {
            if (nodeType == NodeType.Straight ||
                nodeType == NodeType.Start ||
                nodeType == NodeType.Goal ||
                nodeType == NodeType.Merge ||
                nodeType == NodeType.Split)
            {
                return true;
            }

            return false;
        }

        public void SeparateNodes()
        {
            //Debug.Log("SeparateNodes");

            var s = _separationParameters;
            var minSeparation = s.minSeparation;

            var lineSegments = CreateLineSegments(nodes);
            var count = lineSegments.Count;
            for (int i = 0; i < count; i++)
            {
                var lineSegment = lineSegments[i];
                var separationVector = GetSeparationVector(lineSegment, lineSegments, minSeparation);
                TransformNodeAndChildren(lineSegment.n1, separationVector);
            }
        }

        private void TransformNodeAndChildren(Node node, Vector3 direction)
        {
            node.transform.position += direction;

            var children = node.children;
            var count = children.Count;
            for (int i = 0; i < count; i++)
            {
                var child = children[i];
                TransformNodeAndChildren(child, direction);
            }
        }

        private Vector3 GetSeparationVector(LineSegment n1, List<LineSegment> lineSegments, float minSeparation)
        {
            var count = lineSegments.Count;
            var totalSeparationVector = Vector3.zero;
            var countVectors = 0;

            for (int j = 0; j < count; j++)
            {
                var n2 = lineSegments[j];

                if (n2.Equals(n1))
                {
                    continue;
                }

                if (n2.p1 == n1.p1)
                {
                    continue;
                }

                if (n2.p1 == n1.p2)
                {
                    continue;
                }

                var i1 = new Vector2();

                var v11 = new Vector2(n1.p1.x, n1.p1.z);
                var v12 = new Vector2(n1.p2.x, n1.p2.z);
                var v21 = new Vector2(n2.p1.x, n2.p1.z);
                var v22 = new Vector2(n2.p2.x, n2.p2.z);

                var IsIntersecting = Utilities.LineSegmentsIntersect(
                    v11,
                    v12,
                    v21,
                    v22,
                    out i1);

                if (!IsIntersecting)
                {
                    continue;
                }

                if (i1.Equals(v11))
                {
                    continue;
                }

                if (i1.Equals(v12))
                {
                    continue;
                }

                if (i1.Equals(v21))
                {
                    continue;
                }

                if (i1.Equals(v22))
                {
                    continue;
                }

                var intersection = new Vector3(i1.x, 0f, i1.y);
                var z = intersection.z;
                var p1 = n1.GetPositionFromZ(z);
                var p2 = n2.GetPositionFromZ(z);

                //if n1 is on top, don't move it, as we only move downwards
                if (p1.y > p2.y)
                {
                    continue;
                }

                var dir = p1 - p2;

                var dist = dir.magnitude;

                if (dist > minSeparation)
                {
                    continue;
                }

                var separationVector = dir.normalized * (minSeparation - dist);
                totalSeparationVector += separationVector;
                countVectors++;
            }

            if (countVectors > 0)
            {
                totalSeparationVector /= countVectors;
            }

            return totalSeparationVector;
        }

        private List<LineSegment> CreateLineSegments(List<Node> nodes)
        {
            var count = nodes.Count;

            var list = new List<LineSegment>();

            for (int i = 0; i < count; i++)
            {
                var node = nodes[i];
                //var nodePos = node.transform.position;
                //var separationVector = Vector3.zero;

                var children = node.children;
                for (int j = 0; j < children.Count; j++)
                {
                    var child = children[j];
                    var ls = new LineSegment(node, child);
                    list.Add(ls);
                }
            }

            return list;
        }

        private void AddNumberToNodeNames()
        {
            var nodes = this.nodes;
            var count = this.nodes.Count;
            for (int i = 0; i < count; i++)
            {
                nodes[i].name = string.Concat(nodes[i].name, " ", i.ToString());
            };
        }

        private void ReorderNodesInSceneHierarchy()
        {
            var count = nodes.Count;
            for (int i = 0; i < count; i++)
            {
                nodes[i].transform.SetAsLastSibling();
            }
        }

        #endregion Apply

        public override void Clear()
        {
            base.Clear();

            // nodes are cleared in the SlopeManager in this case, because there is no way to determine if the nodes have been affected by any other filters.
            slope.ClearNodes();

            DoneClearing();
        }
    }
}