namespace Slope
{
    using System.Collections.Generic;
    using Apex;
    using Helpers;
    using UnityEngine;

    public sealed class NodeFilterGenerateParallel : SlopeFilter
    {
        [Header("General")]
        [SerializeField]
        private NodeType[] _acceptableNodeTypes = new NodeType[]
        {
            NodeType.Straight,
        };

        [SerializeField]
        private int _startNodeIndexOffset = 1;

        [SerializeField]
        private int _endNodeIndexOffset = 1;

        [SerializeField]
        private SmartRange _slopeNodeLength = new SmartRange(5, 20);

        [SerializeField]
        private int _maxSafePositionAttempts = 5000;

        [Header("Splitting")]
        [SerializeField]
        private float _splitChancePerNode = 0.1f;

        [SerializeField]
        private SmartRange _splitTurn = new SmartRange(40f, 60f);

        [SerializeField]
        private SmartRange _splitLength = new SmartRange(40f, 60f);

        [Header("Parallel Nodes")]
        [SerializeField]
        private float _minDistanceToOtherNodes = 10f;

        [SerializeField]
        private float _maxDistanceToMergeNode = 30f;

        [SerializeField]
        private SmartRange _drop = new SmartRange(-10f, -30f);

        [SerializeField]
        private SmartRange _length = new SmartRange(20f, 40f);

        [SerializeField]
        private SmartRange _turn = new SmartRange(0f, 0f);

        [SerializeField]
        private SmartRange _width = new SmartRange(10f);

        [SerializeField]
        private SmartRange _depth = new SmartRange(10f);

        [SerializeField]
        private bool _turnFlipCoin = true;

        private HashSet<NodeType> _acceptedNodes;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (slope.nodes.Count == 0)
            {
                Debug.LogError(this.ToString() + " cannot operate when the slope has no nodes!");
                return;
            }

            _acceptedNodes = new HashSet<NodeType>(_acceptableNodeTypes, new NodeTypeComparer());
            Node startNode = null;

            var nodes = slope.nodes;
            var count = nodes.Count;
            var start = Mathf.Max(0, _startNodeIndexOffset);
            var end = Mathf.Min(count, count - _endNodeIndexOffset);
            for (int i = start; i < end; i++)
            {
                var node = nodes[i];
                if (!_acceptedNodes.Contains(node.nodeType))
                {
                    continue;
                }

                if (Random.value > _splitChancePerNode)
                {
                    continue;
                }

                node.nodeType = NodeType.Split;
                node.UpdateName();
                startNode = node;
                break;
            }

            var parentNode = startNode;
            var slopeLength = _slopeNodeLength.intValue;
            for (int i = 0; i < slopeLength; i++)
            {
                var position = GetSafePosition(slope, parentNode, i == 0);
                if (!position.HasValue)
                {
                    Debug.LogWarning(this.ToString() + " parallel slope failed since could not find a safe position with current parameters");
                    return;
                }

                var newNode = NodeUtilities.CreateChildNode(slope, NodeType.Straight, parentNode, _width, _depth, position.Value);

                parentNode.UpdateLookDir();
                parentNode = newNode;
            }

            var endNode = GetNearestNotParent(slope, parentNode);
            if (endNode == null)
            {
                Debug.LogError(this.ToString() + " could not find a suitable end node for parallel, resulting in a dead end");
                return;
            }

            var dir = (endNode.transform.position - parentNode.transform.position);
            var distance = dir.magnitude;
            if (distance > _maxDistanceToMergeNode)
            {
                var dirNormXZ = (dir / distance).OnlyXZ();
                var newNodes = Mathf.CeilToInt(distance / _maxDistanceToMergeNode);
                var maxLength = distance / Mathf.Min(2f, newNodes);
                var maxDrop = Mathf.Abs(dir.y) / Mathf.Min(2f, newNodes);
                for (int i = 0; i < newNodes; i++)
                {
                    var factor = i / Mathf.Min(2f, newNodes);
                    var length = Mathf.Min(maxLength, _length.value);
                    var parentDir = parentNode.transform.forward * length;
                    var straightDir = NodeUtilities.CalculateNodeDirection(dirNormXZ, _turn.value, Mathf.Min(maxDrop, _drop.value), length);

                    var pos = parentNode.transform.position + ((straightDir * factor) + (parentDir * (1f - factor)));
                    var node = NodeUtilities.CreateChildNode(slope, NodeType.Straight, parentNode, _width, _depth, pos);

                    parentNode.UpdateLookDir();
                    parentNode = node;
                }

                endNode = GetNearestNotParent(slope, parentNode);
                if (endNode == null)
                {
                    Debug.LogError(this.ToString() + " could not find a suitable end node for parallel, resulting in a dead end");
                    return;
                }
            }

            endNode.nodeType = NodeType.Merge;
            endNode.UpdateName();
            endNode.parents.Add(parentNode);

            parentNode.children.Add(endNode);
            parentNode.UpdateLookDir();

            DoneApplying();
        }

        private Node GetNearestNotParent(SlopeManager slope, Node parentNode)
        {
            var position = parentNode.transform.position + (parentNode.transform.forward * _length.value);
            var shortest = float.MaxValue;
            Node nearest = null;

            parentNode.ForEachChild((index, node) =>
            {
                if (!_acceptedNodes.Contains(node.nodeType))
                {
                    return;
                }

                if (node.transform.position.y >= position.y)
                {
                    // the nearest not parent node must be at a lower Y
                    return;
                }

                // check parents and children
                var parentsCount = node.parents.Count;
                for (int p = 0; p < parentsCount; p++)
                {
                    if (!_acceptedNodes.Contains(node.parents[p].nodeType))
                    {
                        return;
                    }
                }

                var childCount = node.children.Count;
                for (int c = 0; c < childCount; c++)
                {
                    if (!_acceptedNodes.Contains(node.children[c].nodeType))
                    {
                        return;
                    }
                }

                var distanceSqr = (node.transform.position - position).sqrMagnitude;
                if (distanceSqr < shortest)
                {
                    shortest = distanceSqr;
                    nearest = node;
                }
            });

            return nearest;
        }

        private Vector3? GetSafePosition(SlopeManager slope, Node parentNode, bool first)
        {
            Vector3 position;
            var attempts = 0;
            do
            {
                position = parentNode.transform.position + NodeUtilities.CalculateNodeDirection(parentNode, first ? _splitTurn : _turn, _drop, first ? _splitLength : _length, _turnFlipCoin);

                if (attempts++ > _maxSafePositionAttempts)
                {
                    return null;
                }
            } while (!IsClear(slope, parentNode, position));

            return position;
        }

        private bool IsClear(SlopeManager slope, Node parent, Vector3 position)
        {
            var thresholdSqr = _minDistanceToOtherNodes * _minDistanceToOtherNodes;
            var result = true;
            parent.ForEachChild((index, node) =>
            {
                if (!result)
                {
                    return;
                }

                var distanceSqr = (node.transform.position - position).sqrMagnitude;
                if (distanceSqr < thresholdSqr)
                {
                    result = false;
                }

                if (distanceSqr < (node.depth.x * node.depth.x) || distanceSqr < (node.depth.y * node.depth.y))
                {
                    result = false;
                }

                distanceSqr = (node.transform.position - position).OnlyXZ().sqrMagnitude;
                if (distanceSqr < (node.width.x * node.width.x) || distanceSqr < (node.width.y * node.width.y))
                {
                    result = false;
                }
            });

            return result;
        }
    }
}