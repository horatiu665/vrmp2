namespace Slope
{
    using System;
    using UnityEngine;
    using VRCore;
    using Random = UnityEngine.Random;

    public class SlopeFilter : MonoBehaviour, ISlopeFilter
    {
        [SerializeField]
        private int _order;

        [SerializeField, ReadOnly]
        private bool _isApplied;

        UnityEngine.Random.State _randomStateOnApply;

        private SlopeManager _slope;

        private float _generationTime;
        private DateTime _applyTime;

        public void DoneApplying()
        {
            var ms = DateTime.Now.Subtract(_applyTime).Milliseconds;
            _generationTime = ms;
            _generationFrames = 1;
        }

        public void DoneClearing()
        {
            var ms = DateTime.Now.Subtract(_applyTime).Milliseconds;
            _generationTime = ms;
            _generationFrames = 1;
        }

        private int _generationFrames;


        public SlopeManager slope
        {
            get
            {
                if (_slope == null)
                {
                    _slope = this.GetComponentInParent<SlopeManager>();
                }

                return _slope;
            }
        }

        public new bool enabled
        {
            get
            {
                // this shit makes the script disabled once the gameObject is disabled and enabled again. we should probably fix that.......
                return base.enabled && gameObject.activeInHierarchy;
            }
        }

        public int order
        {
            get { return _order; }
            set { _order = value; }
        }

        public bool isApplied
        {
            get
            {
                return _isApplied;
            }

            set
            {
                _isApplied = value;
            }
        }

        public float generationTime
        {
            get
            {
                return _generationTime;
            }

            set
            {
                this._generationTime = value;
            }
        }

        public int generationFrames
        {
            get
            {
                return _generationFrames;
            }

            set
            {
                this._generationFrames = value;
            }
        }
        public override string ToString()
        {
            return GetType().Name;
        }

        protected virtual void OnEnable()
        {
            slope.SortFilters();
        }

        public virtual void Apply(SlopeManager slope)
        {
            _slope = slope;
            if (!isApplied)
            {
                isApplied = true;
                _randomStateOnApply = Random.state;
            }
            _applyTime = System.DateTime.Now;
        }

        public virtual void Clear()
        {
            if (isApplied)
            {
                isApplied = false;
                Random.state = _randomStateOnApply;
            }
            _applyTime = DateTime.Now;
        }
    }
}