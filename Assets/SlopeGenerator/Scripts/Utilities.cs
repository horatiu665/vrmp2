﻿namespace Slope
{
    using System.Collections.Generic;
    using UnityEngine;

    public static class Utilities
    {
        public static int FlipCoin()
        {
            if (Random.value > 0.5f)
            {
                return 1;
            }
            else
            {
                return -1;
            }
        }

        public static bool ClosestPointsOnTwoLines(out Vector3 closestPointLine1, out Vector3 closestPointLine2, Vector3 linePoint1, Vector3 lineVec1, Vector3 linePoint2, Vector3 lineVec2)
        {
            closestPointLine1 = Vector3.zero;
            closestPointLine2 = Vector3.zero;

            float a = Vector3.Dot(lineVec1, lineVec1);
            float b = Vector3.Dot(lineVec1, lineVec2);
            float e = Vector3.Dot(lineVec2, lineVec2);

            float d = a * e - b * b;

            //lines are not parallel
            if (d != 0.0f)
            {
                Vector3 r = linePoint1 - linePoint2;
                float c = Vector3.Dot(lineVec1, r);
                float f = Vector3.Dot(lineVec2, r);

                float s = (b * f - c * e) / d;
                float t = (a * f - c * b) / d;

                closestPointLine1 = linePoint1 + lineVec1 * s;
                closestPointLine2 = linePoint2 + lineVec2 * t;

                return true;
            }
            else
            {
                return false;
            }
        }

        public static Vector3 ReturnCatmullRom(float t, Vector3 p0, Vector3 p1, Vector3 p2, Vector3 p3)
        {
            Vector3 a = 0.5f * (2f * p1);
            Vector3 b = 0.5f * (p2 - p0);
            Vector3 c = 0.5f * (2f * p0 - 5f * p1 + 4f * p2 - p3);
            Vector3 d = 0.5f * (-p0 + 3f * p1 - 3f * p2 + p3);
            return a + (b * t) + (c * t * t) + (d * t * t * t);
        }

        public static Vector3? LineIntersectionXZPoint(Vector3 ps1, Vector3 pe1, Vector3 ps2, Vector3 pe2)
        {
            // Get A,B,C of first line - points : ps1 to pe1
            float A1 = pe1.z - ps1.z;
            float B1 = ps1.x - pe1.x;
            float C1 = A1 * ps1.x + B1 * ps1.z;

            // Get A,B,C of second line - points : ps2 to pe2
            float A2 = pe2.z - ps2.z;
            float B2 = ps2.x - pe2.x;
            float C2 = A2 * ps2.x + B2 * ps2.z;

            // Get delta and check if the lines are parallel
            float delta = A1 * B2 - A2 * B1;
            if (delta == 0)
            {
                return null;
            }

            // now return the Vector2 intersection point
            return new Vector3(
                (B2 * C1 - B1 * C2) / delta,
                0f,
                (A1 * C2 - A2 * C1) / delta
            );
        }

        internal static bool LineSegmentsIntersect(Vector2 p, Vector2 p2, Vector2 q, Vector2 q2, out Vector2 intersection, bool considerCollinearOverlapAsIntersect = false)
        {
            intersection = new Vector2();

            var r = p2 - p;
            var s = q2 - q;
            var rxs = r.Cross(s);
            var qpxr = (q - p).Cross(r);

            // If r x s = 0 and (q - p) x r = 0, then the two lines are collinear.
            if (rxs.IsZero() && qpxr.IsZero())
            {
                // 1. If either  0 <= (q - p) * r <= r * r or 0 <= (p - q) * s <= * s
                // then the two lines are overlapping,
                if (considerCollinearOverlapAsIntersect)
                    if ((0 <= (q - p).Multiply(r) && (q - p).Multiply(r) <= r.Multiply(r)) || (0 <= (p - q).Multiply(s) && (p - q).Multiply(s) <= s.Multiply(s)))
                        return true;

                // 2. If neither 0 <= (q - p) * r = r * r nor 0 <= (p - q) * s <= s * s
                // then the two lines are collinear but disjoint.
                // No need to implement this expression, as it follows from the expression above.
                return false;
            }

            // 3. If r x s = 0 and (q - p) x r != 0, then the two lines are parallel and non-intersecting.
            if (rxs.IsZero() && !qpxr.IsZero())
                return false;

            // t = (q - p) x s / (r x s)
            var t = (q - p).Cross(s) / rxs;

            // u = (q - p) x r / (r x s)
            var u = (q - p).Cross(r) / rxs;

            // 4. If r x s != 0 and 0 <= t <= 1 and 0 <= u <= 1
            // the two line segments meet at the point p + t r = q + u s.
            if (!rxs.IsZero() && (0 <= t && t <= 1) && (0 <= u && u <= 1))
            {
                // We can calculate the intersection point using either t or u.
                intersection = p + t * r;

                // An intersection was found.
                return true;
            }

            // 5. Otherwise, the two line segments are not parallel but do not intersect.
            return false;
        }

        // TODO: SHOULD NOT BE USED!!! SHOULD BE FACTORED OUT!! NOT A STRUCT!
        [System.Obsolete("Please do not use! Make do with a normal Vector2 instead.")]
        internal class VectorXZ
        {
            public float X;
            public float Z;

            // Constructors.
            public VectorXZ(float x, float z) { X = x; Z = z; }

            public VectorXZ() : this(float.NaN, float.NaN)
            {
            }

            public static VectorXZ operator -(VectorXZ v, VectorXZ w)
            {
                return new VectorXZ(v.X - w.X, v.Z - w.Z);
            }

            public static VectorXZ operator +(VectorXZ v, VectorXZ w)
            {
                return new VectorXZ(v.X + w.X, v.Z + w.Z);
            }

            public static float operator *(VectorXZ v, VectorXZ w)
            {
                return v.X * w.X + v.Z * w.Z;
            }

            public static VectorXZ operator *(VectorXZ v, float mult)
            {
                return new VectorXZ(v.X * mult, v.Z * mult);
            }

            public static VectorXZ operator *(float mult, VectorXZ v)
            {
                return new VectorXZ(v.X * mult, v.Z * mult);
            }

            public float Cross(VectorXZ v)
            {
                return X * v.Z - Z * v.X;
            }

            public override bool Equals(object obj)
            {
                var v = (VectorXZ)obj;
                return (X - v.X).IsZero() && (Z - v.Z).IsZero();
            }

            public override int GetHashCode()
            {
                return this.X.GetHashCode() ^ this.Z.GetHashCode() << 2;
            }
        }

        public static Node CreateNode(Vector3 position, Vector3 direction, NodeType type, Transform nodesParent, Vector2 width, Vector2 depth, Node parent)
        {
            var go = new GameObject();
            go.transform.position = position;
            go.transform.rotation = Quaternion.LookRotation(direction, Vector3.up);
            go.transform.SetParent(nodesParent, true);

            var node = go.AddComponent<Node>();
            node.nodeType = type;
            node.width = width;
            node.depth = depth;
            node.parents.Add(parent);

            return node;
        }

        public static int GetRandomNodeFromList(List<Node> nodes, Vector2 minMaxSpan, NodeType[] acceptableNodeTypes)
        {
            return GetRandomNodeFromList(nodes, (int)minMaxSpan.x, (int)minMaxSpan.y, acceptableNodeTypes);
        }

        public static int GetRandomNodeFromList(List<Node> nodes, int min, int max, NodeType[] acceptableNodeTypes)
        {
            var potentialNodes = new List<Node>();
            var countNodes = nodes.Count;

            for (int i = min; i < max + 1; i++)
            {
                //ensire we exit the loop if for some reason we have reached the end of the list of nodes
                if (i >= countNodes)
                {
                    break;
                }

                var potentialNode = nodes[i];

                for (int j = 0; j < acceptableNodeTypes.Length; j++)
                {
                    var acceptableNodeType = acceptableNodeTypes[j];
                    if (potentialNode.nodeType.Equals(acceptableNodeType))
                    {
                        potentialNodes.Add(potentialNode);
                        break;
                    }
                }
            }

            var countPotentialNodes = potentialNodes.Count;

            if (countPotentialNodes == 0)
            {
                return -1;
            }

            var index = Random.Range(0, countPotentialNodes);
            var selectedNode = potentialNodes[index];
            return nodes.IndexOf(selectedNode);
        }
    }
}