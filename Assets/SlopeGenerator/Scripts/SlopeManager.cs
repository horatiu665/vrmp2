namespace Slope
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;
    using Random = UnityEngine.Random;

    public sealed class SlopeManager : MonoBehaviour
    {
        #region private Fields...

        // ======================================= RANDOM SEED STUFF =======================================
        [SerializeField, FormerlySerializedAs("useRandomSeed")]
        private bool _keepSeed = true;

        [Tooltip("String seeds are converted through a hash function based on java.String.hashCode() into an int, which is then used as the currentSeed\n\nThis allows us to make more recognizable seeds by using strings rather than numbers.")]
        [SerializeField]
        private string _stringSeed = string.Empty;

        [SerializeField, FormerlySerializedAs("randomSeed")]
        private int _currentSeed = 0;

        private bool seededByPrevSlope = false;

        // ======================================= NODES AND OTHER SLOPE INFO =======================================

        [SerializeField]
        private List<Node> _nodes = new List<Node>();

        [SerializeField]
        private Node _goalNode = null;

        [SerializeField]
        private Node _startNode = null;

        [SerializeField]
        private MeshFilter _meshFilter;

        // used for grouping all nodes. SlopeManager shuold handle all the low level node generating stuff, node filters should be high level node processing
        [SerializeField]
        private Transform _nodesParent;

        // used for grouping all filters. NO SPAWNED OBJECTS SHOULD BE HERE!
        [SerializeField]
        private Transform _filtersParent;

        // used for grouping all spawned objects. NO FILTERS SHOULD BE HERE! because they will be deleted on ClearAll()
        [SerializeField]
        private Transform _generatedParent;

        [SerializeField]
        private List<ISlopeFilter> _filters;

        // in millis
        private float _generationTime;

        #endregion private Fields...

        #region public Properties, getters, setters

        public List<ISlopeFilter> filters
        {
            get
            {
                if (_filters == null)
                {
                    _filters = new List<ISlopeFilter>();
                    SortFilters();
                }
                return _filters;
            }
        }

        // used for grouping all nodes. This should be parented under generatedParent. SlopeManager shuold handle all the low level node generating stuff, node filters should be high level node processing
        public Transform nodesParent
        {
            get
            {
                if (_nodesParent == null)
                {
                    _nodesParent = new GameObject("Nodes").transform;
                    _nodesParent.SetParent(generatedParent);
                    _nodesParent.localPosition = Vector3.zero;
                    _nodesParent.localRotation = Quaternion.identity;
                }

                return _nodesParent;
            }
        }

        // used for grouping all filters. THIS SHOULD BE PARENTED UNDER SLOPE ITSELF! NO SPAWNED OBJECTS SHOULD BE HERE!
        public Transform filtersParent
        {
            get
            {
                if (_filtersParent == null)
                {
                    _filtersParent = new GameObject("Filters").transform;
                    _filtersParent.SetParent(this.transform);
                    _filtersParent.localPosition = Vector3.zero;
                    _filtersParent.localRotation = Quaternion.identity;
                }
                return _filtersParent;
            }
        }

        // used for grouping all spawned objects. NO FILTERS SHOULD BE HERE! because they will be deleted on ClearAll()
        public Transform generatedParent
        {
            get
            {
                if (_generatedParent == null)
                {
                    _generatedParent = new GameObject("Generated").transform;
                    _generatedParent.SetParent(this.transform);
                    _generatedParent.localPosition = Vector3.zero;
                    _generatedParent.localRotation = Quaternion.identity;
                }
                return _generatedParent;
            }
        }

        public Mesh mesh
        {
            get
            {
                if (_meshFilter == null)
                {
                    _meshFilter = this.GetComponent<MeshFilter>();
                    if (_meshFilter == null)
                    {
                        return null;
                    }
                }

                return _meshFilter.sharedMesh ?? _meshFilter.mesh;
            }
        }

        public Node startNode
        {
            get
            {
                return _startNode;
            }
            set
            {
                _startNode = value;
            }
        }

        public Node goalNode
        {
            get
            {
                if (_goalNode == null)
                {
                    if (_nodes.Any())
                    {
                        _goalNode = _nodes.First(n => n.nodeType == NodeType.Goal);
                    }
                }
                return _goalNode;
            }
        }

        public List<Node> nodes
        {
            get { return _nodes; }
        }

        // in millis
        public float generationTime
        {
            get
            {
                return _generationTime;// _filters.Sum(f => (f.enabled && f.isApplied) ? f.generationTime : 0);
            }
        }

        #endregion public Properties, getters, setters

        public void SetSeedByPrevSlope(int nextSlopeRandomSeed)
        {
            _currentSeed = nextSlopeRandomSeed;
            seededByPrevSlope = true;
        }

        /// <summary>
        /// 1-button-does-all for generating the entire slope as if from scratch.
        /// </summary>
        public void GenerateAll()
        {
            ClearAll();

            if (seededByPrevSlope)
            {
                Random.InitState(_currentSeed);
            }
            else
            {
                // generate seed, or keep seed
                if (_keepSeed)
                {
                    _currentSeed = EncodeRandomSeed(_stringSeed);
                    Random.InitState(_currentSeed);
                }
                else
                {
                    _currentSeed = (int)System.DateTime.Now.ToBinary();
                    _stringSeed = DecodeRandomSeed(_currentSeed);
                    Random.InitState(_currentSeed);
                }
            }

            ApplyFilters();

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            AddNumberToNodeNames();

            // only when not playing so we don't do it at runtime for no reason
            if (!Application.isPlaying)
            {
                ReorderNodesInSceneHierarchy();
            }

#endif
        }

        /// <summary>
        /// Gets filters on the current object and in the filters children hierarchy, but not in other children of this object.
        /// </summary>
        public void SortFilters()
        {
            // clear instead of reassign list so it keeps the reference in the inspector.
            filters.Clear();

            // add from self
            var unorderedList = GetComponents<ISlopeFilter>().ToList();
            // add using GetComponentsInChildren from filtersParent

            unorderedList.AddRange(filtersParent.GetComponentsInChildren<ISlopeFilter>(true));

            // THIS SORT REORDERS ELEMENTS WITH THE SAME ORDER. THIS SHOULD NOT HAPPEN, THEY SHOULD KEEP THEIR ORDER.
            // PLEASE MAKE SURE IT DOES THAT BEFORE CHANGING THE LINQ ORDERBY() BELOW.....!!!!
            //_filters.AddRange(unorderedList);
            //_filters.Sort((a, b) => { return a.order.CompareTo(b.order); });

            // add the orderBY result directly without ToList()
            filters.AddRange(unorderedList.OrderBy(f => f.order));
        }

        // this is equivalent to GetComponentsInChildren. but perhaps more optimized? probably not tho
        private List<ISlopeFilter> FindFiltersInChildrenRecursive(Transform pointer)
        {
            var newList = new List<ISlopeFilter>();
            newList.AddRange(pointer.GetComponents<ISlopeFilter>());
            for (int i = 0; i < pointer.childCount; i++)
            {
                newList.AddRange(FindFiltersInChildrenRecursive(pointer.GetChild(i)));
            }
            return newList;
        }

        public void ApplyFilters()
        {
            // filters are sorted in the ClearFilters() function.
            ClearFilters();

            var initTime = System.DateTime.Now;
            for (int i = 0; i < filters.Count; i++)
            {
                if (!filters[i].enabled)
                {
                    continue;
                }

                filters[i].Apply(this);
            }
            _generationTime = System.DateTime.Now.Subtract(initTime).Milliseconds;
        }

        public void DestroyAllGenChildren()
        {
            ClearAll();

            var tr = generatedParent;
            while (tr.childCount > 0)
            {
                this.DestroySafe(tr.GetChild(0).gameObject);
            }
        }

        public void ClearAll()
        {
            var initTime = System.DateTime.Now;

            ClearNodes();

            if (_nodesParent != null)
            {
                this.DestroySafe(_nodesParent.gameObject);
                _nodesParent = null;
            }

            ClearFilters();
            _generationTime = System.DateTime.Now.Subtract(initTime).Milliseconds;

        }

        public void ClearNodes()
        {
            var count = _nodes.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                var node = _nodes[i];
                if (node != null && !node.Equals(null))
                {
                    this.DestroySafe(node.gameObject);
                }
            }

            _nodes.Clear();
        }

        public void ClearFilters()
        {
            // TODO: optimize this. do not GetComponentsInChildren() every time we clear all the filters......:(
            SortFilters();

            for (int i = 0; i < filters.Count; i++)
            {
                // clear all filters, regardless if they are enabled or disabled, and only if isApplied is true (meaning they have been applied before).
                if (filters[i].isApplied)
                {
                    filters[i].Clear();
                }
            }
        }

        private int EncodeRandomSeed(string seed)
        {
            // if only numbers are used, convert that to an actual number

            int intSeed = 0;
            if (int.TryParse(seed, out intSeed))
            {
                return intSeed;
            }

            intSeed = 0;
            // ported java.String.hashCode() which apparently is used in minecraft so it's gotta be good
            int factor = 1;
            for (int i = seed.Length - 1; i >= 0; i--)
            {
                intSeed += seed[i] * factor;
                factor *= 31;
            }

            return intSeed;
        }

        private string DecodeRandomSeed(int intSeed)
        {
            return intSeed.ToString();
        }

        private void AddNumberToNodeNames()
        {
            for (int i = 0; i < _nodes.Count; i++)
            {
                _nodes[i].name = string.Concat(i.ToString(), " ", _nodes[i].name);
            };
        }

        private void ReorderNodesInSceneHierarchy()
        {
            var count = _nodes.Count;
            for (int i = 0; i < count; i++)
            {
                _nodes[i].transform.SetAsLastSibling();
            }
        }
    }
}