using Slope;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using Core;
using VRCore;

public class SlopeDestructor : MonoBehaviour
{
    private SlopeManager _slope;

    public SlopeManager slope
    {
        get
        {
            if (_slope == null)
            {
                _slope = this.GetComponentInParent<SlopeManager>();
            }

            return _slope;
        }
    }

    private Transform player;

    public float minDistBeforeDelete = 1000f;

    int deleteChecks = 0;

    public float farawaySecondsBeforeDelete = 5f;

    private void Awake()
    {
    }

    private void Start()
    {
        // THIS IS SHIT. PLS FIX
        player = SpawnManagerPreviewable.instance.transform;
        StartCoroutine(TestForPlayerDistance());
    }

    private IEnumerator TestForPlayerDistance()
    {
        while (true)
        {
            yield return new WaitForSeconds(1f);

            float sqrMinDist;

            var sqrDistToSlopeRoot = sqrMinDist = (player.position - slope.transform.position).sqrMagnitude;
            if (slope.goalNode != null)
            {
                var sqrDistToSlopeGoalNode = (player.position - slope.goalNode.transform.position).sqrMagnitude;
                sqrMinDist = sqrDistToSlopeRoot > sqrDistToSlopeGoalNode ? sqrDistToSlopeGoalNode : sqrDistToSlopeRoot;
            }

            if (sqrMinDist > minDistBeforeDelete * minDistBeforeDelete)
            {
                deleteChecks++;
            }
            else
            {
                deleteChecks = 0;
            }

            if (deleteChecks > farawaySecondsBeforeDelete)
            {
                StartCoroutine(StartDestroying());
                yield break;
            }
        }
    }

    IEnumerator StartDestroying()
    {
        slope.ClearAll();
        yield return 0;

        Destroy(slope.gameObject);
        yield break;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = new Color(0.7f, 0, 0, 0.4f);
        Gizmos.DrawWireSphere(slope.transform.position, minDistBeforeDelete);
        if (slope.goalNode != null)
        {
            Gizmos.DrawWireSphere(slope.goalNode.transform.position, minDistBeforeDelete);
        }

    }
}