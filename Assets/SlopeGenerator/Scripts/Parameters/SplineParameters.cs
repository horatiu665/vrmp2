﻿namespace Slope
{
    using System;
    using System.Collections.Generic;
    [Serializable]
    public class SplineParameters
    {
        public float ruggedness;
        public List<float> distanceThresholdPerIteration = new List<float>();
    }
}
