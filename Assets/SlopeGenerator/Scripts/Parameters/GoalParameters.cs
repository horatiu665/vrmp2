﻿namespace Slope
{
    using System;

    [Serializable]
    public class GoalParameters :  SectionParameters
    {
        public bool usePreviousNode = false;
    }
}
