﻿namespace Slope
{
    using System;

    [Serializable]
    public class StraightParameters :  SectionParameters
    {
        public int nodesMin;
        public int nodesMax;
    }
}
