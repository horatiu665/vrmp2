﻿namespace Slope
{
    using System;

    [Serializable]
    public class SplitParameters
    {
        public float chanceSplit;
        public SectionParameters leftSplit;
        public SectionParameters rightSplit;
    }
}
