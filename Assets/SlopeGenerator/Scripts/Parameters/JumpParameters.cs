﻿namespace Slope
{
    using System;
    using System.Collections.Generic;

    [Serializable]
    public class JumpParameters
    {
        public int countJumps;
        public int minNodeIndex;
        public int maxNodeIndex;
        public List<SectionParameters> ramp = new List<SectionParameters>();
        public SectionParameters jump;
        public SectionParameters landing;
    }
}