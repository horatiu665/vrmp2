﻿namespace Slope
{
    using System;
    using Helpers;
    using UnityEngine;

    [Serializable]
    public class SectionParameters
    {
        //[MinMax]
        public Vector2 dropSpan;

        //[MinMax]
        public Vector2 lengthSpan;

        //[MinMax(min = 0f, max = 90f)]
        public Vector2 turnSpan;

        //[MinMax]
        public Vector2 widthSpan;

        //[MinMax]
        public Vector2 depthSpan;

        public bool randomFlipTurnDirection;
    }
}