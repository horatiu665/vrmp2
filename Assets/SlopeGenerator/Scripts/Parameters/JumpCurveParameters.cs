﻿namespace Slope
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;

    [Serializable]
    public class JumpCurveParameters
    {
        public bool usePreviousNodeDrop = true;
        public int countJumps = 0;
        public int minNodeIndex;
        public int maxNodeIndex;
        public int minNodesBetween = 0;
        public AnimationCurve rampCurve = new AnimationCurve();
        public float rampCurveSegmentLength;
        public Vector2 rampSizeLength;
        public Vector2 rampSizeHeight;

        //[MinMax]
        public Vector2 widthSpan;
        //[MinMax]
        public Vector2 depthSpan;

        public SectionParameters jump;
        public SectionParameters landing;
    }
}