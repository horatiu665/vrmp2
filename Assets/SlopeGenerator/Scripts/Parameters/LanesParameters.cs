﻿namespace Slope
{
    using System;
    using Helpers;
    using UnityEngine;

    [Serializable]
    public class LanesParameters
    {
        public int countLanes;
        public int splitMin;
        public int splitMax;
        public int mergeMin;
        public int mergeMax;
        public int countTurnsMin;
        public int countTurnsMax;

        //[MinMax]
        public Vector2 clearanceSpan;

        //[MinMax]
        public Vector2 widthSpan;

        //[MinMax]
        public Vector2 depthSpan;
    }
}
