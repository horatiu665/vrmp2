﻿namespace Slope
{
    using System;
    using Helpers;
    using UnityEngine;

    [Serializable]
    public class SeparationParameters
    {
        public float minSeparation;

        //the min and max limits of drops
        //[MinMax]
        public Vector2 dropSpan;
    }
}