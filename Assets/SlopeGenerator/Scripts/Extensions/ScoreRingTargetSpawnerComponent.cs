namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    [DisallowMultipleComponent]
    public sealed class ScoreRingTargetSpawnerComponent : MonoBehaviour
    {
        [SerializeField]
        private GameObject _scoreRingPrefab = null;

        [SerializeField]
        private GameObject _parent = null;

        [SerializeField]
        private Transform _target = null;

        [Space]
        [SerializeField]
        private AnimationCurve _curve = new AnimationCurve(new Keyframe(0, 0, 0, 3), new Keyframe(0.5f, 1, 0, 0), new Keyframe(1, 0, -3, 0));

        [SerializeField]
        private float _curveScale = 50f;

        [SerializeField]
        private float _startOffset = 6f;

        [SerializeField]
        private int _ringCount = 10;

        [Space]
        [SerializeField, ReadOnly]
        private List<GameObject> _rings = new List<GameObject>();

        public void SpawnScoreRings()
        {
            Clear();

            if (_target == null)
            {
                Debug.LogWarning(this.ToString() + " is missing a target Transform reference!");
                return;
            }

            var position = this.transform.position + (this.transform.forward * _startOffset);
            for (int i = 0; i < _ringCount; i++)
            {
                var t = i / (float)_ringCount;
                var pos = Vector3.Lerp(position, _target.position, t) + (Vector3.up * _curve.Evaluate(t) * _curveScale);
                var rot = i > 0 ? Quaternion.LookRotation(pos - _rings[i - 1].transform.position, Vector3.up) : this.transform.rotation;
                SpawnRing(pos, rot);
            }

            if (_rings.Count > 1)
            {
                // Finally rotate the first ring to face the second one, if there is more than one ring in total
                _rings[0].transform.rotation = Quaternion.LookRotation(_rings[1].transform.position - position, Vector3.up);
            }
        }

        private void SpawnRing(Vector3 position, Quaternion rotation)
        {
            _rings.Add(this.InstantiateSafe(_scoreRingPrefab, position, rotation, _parent != null ? _parent.transform : this.transform));
        }

        public void Clear()
        {
            var count = _rings.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                this.DestroySafe(_rings[i]);
            }

            _rings.Clear();
        }

        public void SetTarget(Transform target)
        {
            _target = target;
        }
    }
}