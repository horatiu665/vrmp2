namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    [DisallowMultipleComponent]
    public sealed class ScoreRingSpawnerComponent : MonoBehaviour
    {
        [SerializeField]
        private GameObject _scoreRingPrefab = null;

        [SerializeField]
        private GameObject _parent = null;

        [SerializeField]
        private Vector3 _dropVector = Physics.gravity;

        [SerializeField]
        private AnimationCurve _ballisticCurve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        [SerializeField, MinMax]
        private Vector2 _ringCountRange = new Vector2(6, 10);

        [SerializeField, MinMax]
        private Vector2 _ringDistanceRange = new Vector2(4f, 6f);

        [SerializeField, ReadOnly]
        private List<GameObject> _rings = new List<GameObject>();

        public void SpawnScoreRings()
        {
            Clear();

            // The first ring spawns at the position of this game object
            var position = this.transform.position;
            var rotation = this.transform.rotation;

            SpawnRing(position, rotation);

            var direction = this.transform.forward;
            var count = _ringCountRange.RandomInt();
            for (int i = 1; i < count; i++)
            {
                var factor = i / (count - 1f);
                var drop = _ballisticCurve.Evaluate(factor) * _dropVector;
                var pos = (position + (direction * i * _ringDistanceRange.Random())) + drop;

                var rot = Quaternion.LookRotation(pos - _rings[i - 1].transform.position, Vector3.up);
                SpawnRing(pos, rot);
            }
        }

        public void Clear()
        {
            var count = _rings.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                this.DestroySafe(_rings[i]);
            }

            _rings.Clear();
        }

        private void SpawnRing(Vector3 position, Quaternion rotation)
        {
            _rings.Add(this.InstantiateSafe(_scoreRingPrefab, position, rotation, _parent != null ? _parent.transform : this.transform));
        }
    }
}