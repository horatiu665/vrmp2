﻿namespace Slope
{
    using UnityEngine;

    public static class Extensions
    {
        private const float Epsilon = 0.0000000001f;

        public static bool IsZero(this float d)
        {
            return Mathf.Abs(d) < Epsilon;
        }

        public static float Cross(this Vector2 vector, Vector2 other)
        {
            return vector.x * other.y - vector.y * other.x;
        }

        public static float Multiply(this Vector2 vector, Vector2 other)
        {
            return vector.x * other.x + vector.y * other.y;
        }

        public static GameObject FindChild(this Component c, string childName)
        {
            var child = c.transform.Find(childName);
            return child != null ? child.gameObject : null;
        }

        public static GameObject FindChild(this GameObject go, string childName)
        {
            var child = go.transform.Find(childName);
            return child != null ? child.gameObject : null;
        }
    }
}