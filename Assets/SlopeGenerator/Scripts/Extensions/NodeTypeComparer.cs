namespace Slope
{
    using System.Collections.Generic;

    public sealed class NodeTypeComparer : IEqualityComparer<NodeType>
    {
        public bool Equals(NodeType x, NodeType y)
        {
            return x == y;
        }

        public int GetHashCode(NodeType obj)
        {
            return (int)obj;
        }
    }
}