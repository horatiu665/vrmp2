namespace Slope
{
    public interface ISlopeFilter
    {
        string ToString();

        bool enabled { get; }

        int order { get; set; }

        bool isApplied { get; set; }

        // how long for the filter to be applied?
        float generationTime { get; set; }

        // how many frames for the filter to be applied?
        int generationFrames { get; set; }

        void Apply(SlopeManager slope);

        void Clear();
    }
}