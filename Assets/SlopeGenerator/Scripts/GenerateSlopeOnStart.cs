namespace Slope
{
    using UnityEngine;

    public class GenerateSlopeOnStart : MonoBehaviour
    {
        private void Start()
        {
            var manager = this.GetComponent<SlopeManager>();
            if (manager == null)
            {
                Debug.LogWarning(this.ToString() + " could not generate slope, since there is no SlopeManager on this game object.");
                return;
            }

            manager.ClearAll();
            manager.GenerateAll();
        }
    }
}