namespace Slope
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    [ExecuteInEditMode]
    public sealed class Node : MonoBehaviour
    {
        [SerializeField, ReadOnly(runtimeOnly = true)]
        private NodeType _nodeType;

        [Space]
        [SerializeField, FormerlySerializedAs("parents")]
        private List<Node> _parents = new List<Node>();

        [Space]
        [SerializeField, FormerlySerializedAs("children")]
        private List<Node> _children = new List<Node>();

        [Header("Debugging")]
        [ReadOnly]
        [SerializeField]
        private Vector2 _width;

        [ReadOnly]
        [SerializeField]
        private Vector2 _depth;

#if UNITY_EDITOR

        [Space]
        [SerializeField, Range(0.1f, 10f)]
        private float _sphereRadius = 0.5f;

        [SerializeField]
        private Color _sphereColor = Color.yellow;

#endif

        private SlopeManager _slope;

        public NodeType nodeType
        {
            get { return _nodeType; }
            set { _nodeType = value; }
        }

        public SlopeManager slope
        {
            get
            {
                if (_slope == null)
                {
                    _slope = this.GetComponentInParent<SlopeManager>();
                }

                return _slope;
            }

            set { _slope = value; }
        }

        public List<Node> parents
        {
            get { return _parents; }
        }

        public List<Node> children
        {
            get { return _children; }
        }

        /// <summary>
        /// Gets or sets how far left and right the slope extends at this point (can be assymetrical).
        /// </summary>
        /// <value>
        /// The width.
        /// </value>
        public Vector2 width
        {
            get { return _width; }
            set { _width = value; }
        }

        /// <summary>
        /// Gets or sets how far left and right the BOTTOM OF THE slope extends at this point (DUE TO LEGACY CONCERNS: Y is left, X is right).
        /// </summary>
        /// <value>
        /// The depth.
        /// </value>
        public Vector2 depth
        {
            get { return _depth; }
            set { _depth = value; }
        }

        /// <summary>
        /// Calculates and returns the right vertex based on transform.position and width.
        /// </summary>
        /// <value>
        /// The right vertex.
        /// </value>
        public Vector3 rightVertex
        {
            get
            {
                return this.transform.right * width.x + this.transform.position;
            }
        }

        /// <summary>
        /// Calculates and returns the left vertex based on transform.position and width.
        /// </summary>
        /// <value>
        /// The left vertex.
        /// </value>
        public Vector3 leftVertex
        {
            get
            {
                return this.transform.right * -width.y + this.transform.position;
            }
        }

        /// <summary>
        /// Calculates and returns the right lower vertex based on transform.position and width and depth.
        /// </summary>
        /// <value>
        /// The right lower vertex.
        /// </value>
        public Vector3 rightLowerVertex
        {
            get
            {
                return this.transform.right * width.x + this.transform.position - this.transform.up * depth.x;
            }
        }

        /// <summary>
        /// Calculates and returns the left lower vertex based on transform.position and width and depth.
        /// </summary>
        /// <value>
        /// The left lower vertex.
        /// </value>
        public Vector3 leftLowerVertex
        {
            get
            {
                return this.transform.right * -width.y + this.transform.position - this.transform.up * depth.y;
            }
        }

        /// <summary>
        /// Equivalent to LookAt (average pos of children). Use after setting children!
        /// </summary>
        public void UpdateLookDir()
        {
            var count = _children.Count;
            if (count == 0)
            {
                return;
            }

            var pos = _children[0].transform.position;
            if (count > 1)
            {
                for (int i = 1; i < count; i++)
                {
                    pos += (_children[i].transform.position - _children[i - 1].transform.position) * 0.5f;
                }
            }

            this.transform.LookAt(pos);
        }

        public void UpdateName()
        {
            this.name = _nodeType.ToString();
        }

        private void OnDestroy()
        {
            if (this.slope != null)
            {
                this.slope.nodes.Remove(this);
            }

            var parentsCount = _parents.Count;
            for (int i = parentsCount - 1; i >= 0; i--)
            {
                _parents[i].children.Remove(this);
                _parents.RemoveAt(i);
            }

            var childCount = _parents.Count;
            for (int i = childCount - 1; i >= 0; i--)
            {
                _children[i].parents.Remove(this);
                _children.RemoveAt(i);
            }
        }

#if UNITY_EDITOR

        private void OnDrawGizmos()
        {
            Gizmos.color = _sphereColor;
            Gizmos.DrawSphere(this.transform.position, _sphereRadius);
            Gizmos.DrawSphere(this.leftVertex, _sphereRadius * 0.5f);
            Gizmos.DrawSphere(this.rightVertex, _sphereRadius * 0.5f);
            Gizmos.DrawLine(this.leftVertex, this.rightVertex);

            var countChildren = _children.Count;

            Gizmos.color = Color.cyan;
            for (int i = 0; i < countChildren; i++)
            {
                Gizmos.DrawLine(this.transform.position, children[i].transform.position);
            }
        }

#endif
    }
}