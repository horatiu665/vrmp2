namespace Slope
{
    public class SlopeNode
    {
        public NodeType nodeType;
        public int parameterIndex;

        public SlopeNode(NodeType nodeType, int parameterIndex)
        {
            this.nodeType = nodeType;
            this.parameterIndex = parameterIndex;
        }
    }
}