namespace Slope
{
    using System.Collections.Generic;
    using Core.Level;
    using Helpers;
    using UnityEngine;
    using VRCore;

    /// <summary>
    /// Creates <see cref="ScoreRingTargetSpawnerComponent"/>s at every jump in the slope.
    /// </summary>
    /// <seealso cref="Slope.SlopeFilter" />
    public class FilterSpawnScoreRingTargetSpawners : SlopeFilter
    {
        [SerializeField]
        private GameObject _spawnScorerPrefab = null;

        [SerializeField, ReadOnly]
        private Transform _parent;

        [SerializeField]
        private float _forwardOffset = 6f;

        [SerializeField]
        private float _upOffset = 2f;

        [SerializeField]
        private bool _spawnAtTrampolines = true;

        [SerializeField, ReadOnly]
        private List<GameObject> _scoreRingSpawners = new List<GameObject>();

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("ScoreRingSpawners").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (_spawnScorerPrefab.GetComponent<ScoreRingTargetSpawnerComponent>() == null)
            {
                Debug.LogError(this.ToString() + " Prefab (" + _spawnScorerPrefab.ToString() + ") does not have a ScoreRingTargetSpawnerComponent, which is a requirement!");
                return;
            }

            var nodes = slope.nodes;
            if (_spawnAtTrampolines)
            {
                // Either we spawn at trampolines...
                var trampolines = FindObjectsOfType<JumpTrigger>();
                for (int i = 0; i < trampolines.Length; i++)
                {
                    var tr = trampolines[i];

                    var position = tr.transform.position + (tr.jumpDirection * _forwardOffset) + (tr.upDirection * _upOffset);
                    var rotation = Quaternion.LookRotation(tr.jumpDirection);

                    var nearestNodeIdx = GetNearestJumpNodeIndex(slope, tr.transform.position);
                    var targetPosition = nodes[nearestNodeIdx + 1].transform.position;

                    SpawnAt(targetPosition, position, rotation);
                }
            }
            else
            {
                // ... or we spawn at nodes.
                var count = nodes.Count;
                nodes.ForEachChild(1, (index, prevNode, node) =>
                {
                    if (index >= count - 2)
                    {
                        return;
                    }

                    if (node.nodeType != NodeType.Jump && node.nodeType != NodeType.JumpCurve)
                    {
                        return;
                    }

                    var center = node.transform.position;
                    var forward = (center - prevNode.transform.position);
                    var rotation = Quaternion.LookRotation(node.transform.up);
                    var position = center + (forward.normalized * _forwardOffset);

                    var nextNode = node.children[0]; // TODO: multiple children
                    SpawnAt(nextNode.transform.position, position, rotation);
                });
            }

            DoneApplying();
        }

        private int GetNearestJumpNodeIndex(SlopeManager slope, Vector3 position)
        {
            int nearest = -1;
            var shortest = float.MaxValue;

            var nodes = slope.nodes;
            var count = nodes.Count;
            for (int i = 1; i < count - 1; i++)
            {
                if (nodes[i].nodeType == NodeType.Jump || nodes[i].nodeType == NodeType.JumpCurve)
                {
                    var distance = (nodes[i].transform.position - position).sqrMagnitude;
                    if (distance < shortest)
                    {
                        shortest = distance;
                        nearest = i;
                    }
                }
            }

            return nearest;
        }

        private void SpawnAt(Vector3 targetPosition, Vector3 position, Quaternion rotation)
        {
            var spawnerParent = new GameObject(string.Concat("Spawner ", (_scoreRingSpawners.Count + 1).ToString()));
            spawnerParent.transform.position = position;
            spawnerParent.transform.SetParent(parent, true);
            _scoreRingSpawners.Add(spawnerParent);

            var go = this.InstantiateSafe(_spawnScorerPrefab, position, rotation, spawnerParent.transform);

            var target = new GameObject(string.Concat(go.name, " (Target)"));
            target.transform.position = targetPosition;
            target.transform.SetParent(spawnerParent.transform, true);

            var comp = go.GetComponent<ScoreRingTargetSpawnerComponent>();

            comp.SetTarget(target.transform);
        }

        public override void Clear()
        {
            base.Clear();
            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _scoreRingSpawners.Clear();

            DoneClearing();
        }
    }
}