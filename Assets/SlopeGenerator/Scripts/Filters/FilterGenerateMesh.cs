namespace Slope
{
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    public class FilterGenerateMesh : SlopeFilter
    {
        [SerializeField, FormerlySerializedAs("slopeMaterial")]
        private Material _slopeMaterial = null;

        [SerializeField]
        private PhysicMaterial _physicsMaterial = null;

        [SerializeField, HideInInspector]
        private MeshFilter _meshFilter;

        [SerializeField, HideInInspector]
        private MeshRenderer _meshRenderer;

        [SerializeField, HideInInspector]
        private MeshCollider _meshCollider;

        //[SerializeField]
        //private bool _smoothNormals = true;

        [SerializeField]
        private bool _removeStartFaces = false;

        [SerializeField]
        private bool _removeEndFaces = false;

        [SerializeField]
        private bool _createBottomFaces = true;

        public Mesh generatedMesh
        {
            get
            {
                if (_meshFilter == null)
                {
                    _meshFilter = slope.GetComponent<MeshFilter>();
                    if (_meshFilter == null)
                    {
                        return null;
                    }
                }

                return _meshFilter.sharedMesh ?? _meshFilter.mesh;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            //if (slope.nodes.Count == 0)
            //{
            //    Debug.LogError(this.ToString() + " cannot generate mesh when there are no slope nodes!");
            //    return;
            //}

            // nodes need to be at zero when generating....

            // set nodes to global zero so the mesh can be generated in local space
            slope.nodesParent.position = Vector3.zero;
            slope.nodesParent.rotation = Quaternion.identity;

            GenerateMesh(slope.nodes);

            // set nodesParent to local zero, so we bring all the nodes where the mesh is. therefore mesh coordinates have been created optimally without unnnecessary calculations, but we still keep nodes in world space so we can use them.
            slope.nodesParent.localPosition = Vector3.zero;
            slope.nodesParent.localRotation = Quaternion.identity;

            DoneApplying();
        }

        #region public void GenerateMesh()

        public void GenerateMesh(List<Node> nodes)
        {
            ClearMesh();

            _meshRenderer = slope.GetOrAddComponent<MeshRenderer>();
            _meshFilter = slope.GetOrAddComponent<MeshFilter>();
            _meshCollider = slope.GetOrAddComponent<MeshCollider>();

            _meshRenderer.sharedMaterial = _slopeMaterial;


            if (_physicsMaterial != null)
            {
                _meshCollider.sharedMaterial = _physicsMaterial;
            }

            var countNodes = nodes.Count;

            //Debug.Log("countNodes == " + countNodes);

            var vertices = new List<Vector3>();
            var triangles = new List<int>();
            var dicIndicesByTriangleTopBottom = new Dictionary<Vector3, int>();
            var dicIndicesSides = new Dictionary<Vector3, int>();

            nodes.ForEachChild((index, node) =>
            {
                GenerateNodeMesh(node, vertices, triangles, dicIndicesByTriangleTopBottom, dicIndicesSides);
            });

            var m = new Mesh();

            //Debug.Log("vertices == " + vertices.Count);
            //Debug.Log("triangles == " + triangles.Count);

            m.vertices = vertices.ToArray();
            m.triangles = triangles.ToArray();
            //if (_smoothNormals)
            //{
            m.RecalculateNormals();
            //}
            m.RecalculateBounds();
            ;

            _meshCollider.sharedMesh = _meshFilter.sharedMesh = m;
            Debug.Log(this.ToString() + " Generated new Slope mesh with " + countNodes.ToString() + " nodes, " + vertices.Count.ToString() + " vertices and " + triangles.Count.ToString() + " triangles");
        }

        private void GenerateNodeMesh(Node node, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesByTriangle, Dictionary<Vector3, int> dicIndicesSides)
        {
            var children = node.children;

            switch (node.nodeType)
            {
            case NodeType.Straight:
                {
                    if (node.children.Count == 1)
                    {
                        var child = node.children[0];
                        if (child.nodeType == NodeType.Merge)
                        {
                            CreateMergeMesh(child, vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                            break;
                        }

                        CreateStraightMesh(node, child, vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                    }

                    break;
                }

            case NodeType.PreRamp:
            case NodeType.Ramp:
            case NodeType.Landing:
            case NodeType.Merge:
                {
                    CreateStraightMesh(node, children[0], vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                    break;
                }

            case NodeType.Start:
                {
                    CreateStraightMesh(node, children[0], vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                    if (_removeStartFaces)
                    {
                        // remove side face between goal and start node, so normals are aligned with previous mesh!
                        RemoveSideFromStart(node, children[0], vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                    }

                    break;
                }

            case NodeType.Jump:
                {
                    // skip the mesh if we are a jump node
                    break;
                }

            case NodeType.Split:
                {
                    CreateSplitMesh(node, vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                    break;
                }

            case NodeType.Goal:
                {
                    // remove triangles at the end of the slope, to remove normals seam with the next slope that will be generated
                    if (_removeEndFaces)
                    {
                        RemoveSideFromGoal(node.parents[0], node, vertices, triangles, dicIndicesByTriangle, dicIndicesSides);
                    }

                    break;
                }
            }
        }

        private void CreateStraightMesh(Node parent, Node child, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesByTriangle, Dictionary<Vector3, int> dicIndicesSides)
        {
            var parentFwd = parent.transform.forward;
            var childFwd = child.transform.forward;

            var angle = Vector3.Angle(parentFwd, childFwd);

            var v0 = parent.rightVertex;
            var v1 = parent.leftVertex;

            var v2 = child.leftVertex;
            var v3 = child.rightVertex;

            if (angle > 90f)
            {
                v2 = child.rightVertex;
                v3 = child.leftVertex;
            }

            // upward facing triangles
            GenerateTriangle(v0, v1, v2, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v0, v2, v3, vertices, triangles, dicIndicesByTriangle);

            if (_createBottomFaces)
            {

                // downward facing triangles
                var v0d = parent.rightLowerVertex;
                var v1d = parent.leftLowerVertex;
                var v2d = child.leftLowerVertex;
                var v3d = child.rightLowerVertex;

                if (angle > 90f)
                {
                    v2d = child.rightLowerVertex;
                    v3d = child.leftLowerVertex;
                }

                GenerateTriangle(v2d, v1d, v0d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v3d, v2d, v0d, vertices, triangles, dicIndicesByTriangle);

                // this makes triangles on the inside of the mesh (when sides are not common with top and bottom so normals look nice)
                var v = new Vector3[] { v0, v1, v2, v3 };
                var vd = new Vector3[] { v0d, v1d, v2d, v3d };
                GenerateSideVertices(v, vd, vertices, triangles, dicIndicesSides);
            }

        }

        private void RemoveSideFromStart(Node parent, Node child, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesByTriangle, Dictionary<Vector3, int> dicIndicesSides)
        {
            if (_createBottomFaces)
            {
                var v0 = parent.rightVertex;
                var v1 = parent.leftVertex;
                var v0d = parent.rightLowerVertex;
                var v1d = parent.leftLowerVertex;
                ZeroTriangles(v0, v0d, v1d, v1, vertices, triangles, dicIndicesSides);
                ZeroTriangles(v1, v1d, v0d, v0, vertices, triangles, dicIndicesSides);

            }
        }

        private void RemoveSideFromGoal(Node goalParent, Node goal, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesByTriangle, Dictionary<Vector3, int> dicIndicesSides)
        {
            if (_createBottomFaces)
            {
                var v0 = goal.rightVertex;
                var v1 = goal.leftVertex;
                var v0d = goal.rightLowerVertex;
                var v1d = goal.leftLowerVertex;
                ZeroTriangles(v0, v0d, v1d, v1, vertices, triangles, dicIndicesSides);
                ZeroTriangles(v1, v1d, v0d, v0, vertices, triangles, dicIndicesSides);

            }
        }

        private void CreateSplitMesh(Node parent, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesByTriangle, Dictionary<Vector3, int> dicIndicesSides)
        {

            var parentPos = parent.transform.position;

            var children = parent.children.ToList();

            //var dirChild0 = children[0].transform.position - parent.transform.position;
            //var dirChild1= children[1].transform.position - parent.transform.position;

            var leftMost = GetLeftMostNode(children, parent);

            //var dirLeftMost = leftMost.transform.position - parentPos;

            children.Remove(leftMost);

            var rightMost = children[0];

            var child0 = leftMost;
            var child1 = rightMost;
            var v0 = parent.rightVertex;
            var v1 = parent.leftVertex;

            var v2 = child0.leftVertex;
            var v3 = child0.rightVertex;

            var v5 = child1.leftVertex;
            var v6 = child1.rightVertex;

            var v4 = (parentPos + child0.transform.position + child1.transform.position) / 3f;

            var v = new Vector3[] { v0, v1, v2, v3, v4, v5, v6 };

            // middle vertex
            GenerateTriangle(v0, v1, v4, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v1, v2, v4, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v2, v3, v4, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v4, v5, v6, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v4, v6, v0, vertices, triangles, dicIndicesByTriangle);

            if (_createBottomFaces)
            {
                var v0d = parent.rightLowerVertex;
                var v1d = parent.leftLowerVertex;
                var v2d = child0.leftLowerVertex;
                var v3d = child0.rightLowerVertex;
                var v4d = v4 + Vector3.down * (leftMost.depth.y + rightMost.depth.x) * 0.5f;
                var v5d = child1.leftLowerVertex;
                var v6d = child1.rightLowerVertex;

                var vd = new Vector3[] { v0d, v1d, v2d, v3d, v4d, v5d, v6d };

                // generate bottom triangles
                GenerateTriangle(v4d, v1d, v0d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v4d, v2d, v1d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v4d, v3d, v2d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v6d, v5d, v4d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v0d, v6d, v4d, vertices, triangles, dicIndicesByTriangle);

                GenerateSideVertices(v, vd, vertices, triangles, dicIndicesSides);
            }

        }

        private void CreateMergeMesh(Node node, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesByTriangle, Dictionary<Vector3, int> dicIndicesSides)
        {

            var nodePos = node.transform.position;

            var parents = node.parents.ToList();

            //var dirParentd0 = parents[0].transform.position - node.transform.position;
            //var dirParentd1 = parents[1].transform.position - node.transform.position;
            var leftMost = GetLeftMostNode(parents, node);
            //var dirLeftMost = leftMost.transform.position - nodePos;

            parents.Remove(leftMost);

            var rightMost = parents[0];

            var parent0 = leftMost;
            var parent1 = rightMost;
            var parent0Pos = parent0.transform.position;
            var parent1Pos = parent1.transform.position;

            var v0 = node.rightVertex;
            var v1 = node.leftVertex;

            var v2 = parent0.leftVertex;
            var v3 = parent0.rightVertex;

            var v5 = parent1.leftVertex;
            var v6 = parent1.rightVertex;

            var v4 = (nodePos + parent0Pos + parent1Pos) / 3f;

            var lowestParentY = Mathf.Min(parent0Pos.y, parent1Pos.y);

            var y = (lowestParentY + nodePos.y) * 0.5f;
            v4.y = y;

            var v = new Vector3[7] { v6, v5, v4, v3, v2, v1, v0 };

            //top triangles
            GenerateTriangle(v4, v1, v0, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v4, v2, v1, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v4, v3, v2, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v6, v5, v4, vertices, triangles, dicIndicesByTriangle);
            GenerateTriangle(v0, v6, v4, vertices, triangles, dicIndicesByTriangle);

            var down = Vector3.down;

            if (_createBottomFaces)
            {
                var v0d = node.rightLowerVertex;
                var v1d = node.leftLowerVertex;
                var v2d = parent0.leftLowerVertex;
                var v3d = parent0.rightLowerVertex;
                var v4d = v4 + down * (leftMost.depth.y + rightMost.depth.x) * 0.5f;
                var v5d = parent1.leftLowerVertex;
                var v6d = parent1.rightLowerVertex;

                var vd = new Vector3[7] { v6d, v5d, v4d, v3d, v2d, v1d, v0d };

                //bottom triangles
                GenerateTriangle(v0d, v1d, v4d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v1d, v2d, v4d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v2d, v3d, v4d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v4d, v5d, v6d, vertices, triangles, dicIndicesByTriangle);
                GenerateTriangle(v4d, v6d, v0d, vertices, triangles, dicIndicesByTriangle);

                GenerateMergeSideVertices(v, vd, vertices, triangles, dicIndicesSides);

            }

        }

        private void GenerateMergeSideVertices(Vector3[] v, Vector3[] vd, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesSides)
        {
            //generate sides
            for (int i = 0; i < v.Length; i++)
            {
                //int i = 0;
                int j = i + 1;

                if (i == v.Length - 1)
                {
                    j = 0;
                }

                GenerateTriangle(v[i], vd[i], vd[j], vertices, triangles, dicIndicesSides);
                GenerateTriangle(v[i], vd[j], v[j], vertices, triangles, dicIndicesSides);
            }
        }

        private void GenerateSideVertices(Vector3[] v, Vector3[] vd, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndicesSides)
        {
            //generate sides
            for (int i = 0; i < v.Length; i++)
            {
                //int i = 0;
                int j = i + 1;

                if (i == v.Length - 1)
                {
                    j = 0;
                }

                // if all corners of current side are contained in dictionary, it means the current side has been drawn before (no matter in which order).
                if (dicIndicesSides.ContainsKey(v[i]) && dicIndicesSides.ContainsKey(v[j]) && dicIndicesSides.ContainsKey(vd[i]) && dicIndicesSides.ContainsKey(vd[j]))
                {
                    if (!ZeroTriangles(v[i], vd[i], vd[j], v[j], vertices, triangles, dicIndicesSides))
                    {
                        GenerateTriangle(v[i], vd[i], vd[j], vertices, triangles, dicIndicesSides);
                        GenerateTriangle(v[i], vd[j], v[j], vertices, triangles, dicIndicesSides);
                    }
                }
                else
                {
                    GenerateTriangle(v[i], vd[i], vd[j], vertices, triangles, dicIndicesSides);
                    GenerateTriangle(v[i], vd[j], v[j], vertices, triangles, dicIndicesSides);
                }
            }
        }

        private Node GetLeftMostNode(List<Node> children, Node parent)
        {
            var minX = float.MaxValue;
            Node leftMost = null;

            for (int i = 0; i < children.Count; i++)
            {
                var child = children[i];
                var dirToChild = child.transform.position - parent.transform.position;
                var localDirToChild = parent.transform.InverseTransformDirection(dirToChild);

                if (localDirToChild.x < minX)
                {
                    minX = localDirToChild.x;
                    leftMost = child;
                }
            }

            return leftMost;
        }

        private bool ZeroTriangles(Vector3 v0, Vector3 vd0, Vector3 vd1, Vector3 v1, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndices)
        {
            var i0 = dicIndices[v0];
            var id0 = dicIndices[vd0];
            var id1 = dicIndices[vd1];
            var i1 = dicIndices[v1];

            bool erase, erasedSomething = false;
            for (int i = 0; i < triangles.Count; i += 3)
            {
                // if triangles[i] and [i+1] and [i+2] are equal to any viable permutation of i/id, erase the triangles
                erase = false;
                if (triangles[i] == i0)
                {
                    if (triangles[i + 1] == id0 && triangles[i + 2] == id1)
                    {
                        erase = true;
                    }
                    else if (triangles[i + 1] == id1 && triangles[i + 2] == i1)
                    {
                        erase = true;
                    }
                }
                else if (triangles[i] == i1)
                {
                    if (triangles[i + 1] == id1 && triangles[i + 2] == id0)
                    {
                        erase = true;
                    }
                    else if (triangles[i + 1] == id0 && triangles[i + 2] == i0)
                    {
                        erase = true;
                    }
                }
                if (erase)
                {
                    erasedSomething = true;
                    triangles[i] = triangles[i + 1] = triangles[i + 2] = 0;
                }
            }
            return erasedSomething;
        }

        private void GenerateTriangle(Vector3 v0, Vector3 v1, Vector3 v2, List<Vector3> vertices, List<int> triangles, Dictionary<Vector3, int> dicIndices)
        {
            var i0 = GetVertexIndex(v0, vertices, dicIndices);
            var i1 = GetVertexIndex(v1, vertices, dicIndices);
            var i2 = GetVertexIndex(v2, vertices, dicIndices);

            triangles.Add(i0);
            triangles.Add(i1);
            triangles.Add(i2);

            //var surfaceNormal = Vector3.Cross(v1 - v0, v2 - v0);

            //var y = surfaceNormal.y;

            //if (!faceUp)
            //{
            //    y = -y;
            //}

            //if (surfaceNormal.y > 0)
            //{
            //}
            //else
            //{
            //    triangles.Add(i2);
            //    triangles.Add(i1);
            //    triangles.Add(i0);
            //}
        }

        private int GetVertexIndex(Vector3 vertex, List<Vector3> vertices, Dictionary<Vector3, int> dicIndices)
        {
            int idx = -1;

            if (dicIndices.TryGetValue(vertex, out idx))
            {
                return idx;
            }
            else
            {
                idx = vertices.Count;
                vertices.Add(vertex);
                dicIndices.Add(vertex, idx);
                return idx;
            }
        }

        #endregion public void GenerateMesh()

        public override void Clear()
        {
            base.Clear();
            ClearMesh();

            DoneClearing();
        }

        private void ClearMesh()
        {
            _meshFilter = _meshFilter ?? (_meshFilter = this.GetComponent<MeshFilter>());

            if (_meshFilter != null)
            {
                if (_meshFilter.sharedMesh != null)
                {
                    _meshFilter.sharedMesh.Clear(false);
                }

#if !UNITY_EDITOR

                if (_meshFilter.mesh != null)
                {
                    _meshFilter.mesh.Clear(false);
                }

#endif

                _meshFilter.sharedMesh = _meshFilter.mesh = null;
            }

            _meshCollider = _meshCollider ?? (_meshCollider = this.GetComponent<MeshCollider>());
            if (_meshCollider != null)
            {
                if (_meshCollider.sharedMesh != null)
                {
                    _meshCollider.sharedMesh.Clear(false);
                }

                _meshCollider.sharedMesh = null;
            }
        }
    }
}