namespace Slope
{
    using Apex;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Random = UnityEngine.Random;

    [ExecuteInEditMode]
    public class FilterSpawnForest : SlopeFilter
    {
        [Header("Spawn/Despawn Params")]

        [SerializeField]
        private bool _useSpawnManager = true;

        [SerializeField]
        private VRPrefabType[] _prefabsToSpawn = new VRPrefabType[0];

        [SerializeField]
        private GameObject[] _treePrefabs = new GameObject[0];


        [Header("Coroutine params")]
        [SerializeField]
        private int _maxTreesPerFrame = 100;
        private int _treesThisFrame = 0;

        [Header("Sampling params")]
        [SerializeField]
        private float _sampleDistance = 1f;

        [SerializeField]
        private float _sampleRandomOffset = 1f;

        [SerializeField]
        private float _spawnNoiseThreshold = 0.5f;

        [SerializeField]
        private FastNoise noise = new FastNoise();

        [Header("Piste params")]
        [SerializeField]
        private bool _spawnOnPiste = true;

        [SerializeField]
        private bool _spawnOffPiste = false;

        [SerializeField]
        private float _offPisteWidthMultiplier = 1f;

        [SerializeField]
        private float _offPisteVerticalOffsetUnits = 0f;


        [Header("Basic params")]
        [SerializeField]
        private LayerMask _terrainLayer = 0;

        [SerializeField]
        private LayerMask _avoidLayer = 0;

        [SerializeField]
        private int _startNodeIndexOffset = 0;

        [SerializeField]
        private int _endNodeIndexOffset = 0;

        [SerializeField]
        private NodeType[] _ignoredNodeTypes = new NodeType[]
        {
            NodeType.Jump,
            NodeType.JumpCurve
        };

        [SerializeField, MinMax, Tooltip("Randomizes the scale of spawned game objects between the min and max values set here.")]
        private Vector2 _randomScale = Vector2.one;

        [SerializeField, ReadOnly]
        private List<GameObject> _spawnedTrees = new List<GameObject>();

        [SerializeField, ReadOnly]
        private Transform _parent = null;

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("Forest of " + transform.name).transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        private Random.State _multiFrameRandomState;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            StopCoroutine("ApplyMultiFrames");
            StartCoroutine("ApplyMultiFrames", slope);

            DoneApplying();
        }

        private IEnumerator ApplyMultiFrames(SlopeManager slope)
        {
            var nodes = slope.nodes;
            var count = nodes.Count;
            var start = Mathf.Max(1, _startNodeIndexOffset);
            var end = Mathf.Min(count, count - _endNodeIndexOffset);
            _treesThisFrame = 0;

            if (count == 0)
            {
                yield break;
            }

            var index = 0;
            var node = nodes[start];

            // enqueue the first node. then enqueue all its children. for every child, enqueue its children too. and so we go through the entire tree.
            var queue = new Queue<Node>();
            queue.Enqueue(node);
            while (queue.Count > 0)
            {
                node = queue.Dequeue();
                var prevNode = node.parents[0];

                if (index++ >= end)
                {
                    break;
                }

                //////// vvvvvvvvvvvvvv ACTION GOES HERE vvvvvvvvvvvvvv /////////////
                //action(index++, startNode, child);

                if (!_ignoredNodeTypes.Contains(node.nodeType) && !_ignoredNodeTypes.Contains(prevNode.nodeType))
                {
                    Vector3 prevLeftVertex = prevNode.leftVertex;
                    Vector3 prevRightVertex = prevNode.rightVertex;
                    Vector3 leftVertex = node.leftVertex;
                    Vector3 rightVertex = node.rightVertex;
                    var nodeDelta = node.transform.position - prevNode.transform.position;

                    if (_spawnOnPiste)
                    {
                        // spawn objects between prevNode and node
                        Spawn(slope, prevLeftVertex, prevRightVertex, leftVertex, rightVertex, nodeDelta);
                        if (_treesThisFrame >= _maxTreesPerFrame)
                        {
                            _treesThisFrame = 0;
                            _multiFrameRandomState = Random.state;
                            yield return 0;
                            base.generationFrames++;
                            Random.state = _multiFrameRandomState;
                        }
                    }

                    if (_spawnOffPiste)
                    {
                        // two spawn cycles: from left to middle, then middle to right

                        // parent offset right
                        var por = (prevRightVertex - prevLeftVertex) * _offPisteWidthMultiplier;
                        // child offset right
                        var cor = (rightVertex - leftVertex) * _offPisteWidthMultiplier;
                        // vertical offset
                        var vo = Vector3.up * _offPisteVerticalOffsetUnits;

                        Spawn(slope, prevLeftVertex - por + vo, prevLeftVertex, leftVertex - cor + vo, leftVertex, nodeDelta);
                        if (_treesThisFrame >= _maxTreesPerFrame)
                        {
                            _treesThisFrame = 0;
                            _multiFrameRandomState = Random.state;
                            yield return 0;
                            base.generationFrames++;
                            Random.state = _multiFrameRandomState;
                        }

                        Spawn(slope, prevRightVertex, prevRightVertex + por + vo, rightVertex, rightVertex + cor + vo, nodeDelta);
                        if (_treesThisFrame >= _maxTreesPerFrame)
                        {
                            _treesThisFrame = 0;
                            _multiFrameRandomState = Random.state;
                            yield return 0;
                            base.generationFrames++;
                            Random.state = _multiFrameRandomState;
                        }
                    }

                }

                //////// ^^^^^^^^^^^^^^ ACTION GOES HERE ^^^^^^^^^^^^^^ /////////////

                var childCount = node.children.Count;
                for (int i = 0; i < childCount; i++)
                {
                    queue.Enqueue(node.children[i]);
                }

            } // end while

            yield break;
        }

        private void Spawn(SlopeManager slope, Vector3 prevLeftVertex, Vector3 prevRightVertex, Vector3 leftVertex, Vector3 rightVertex, Vector3 nodeDelta)
        {
            var mag = nodeDelta.magnitude;

            Vector3 pos;

            int xStepsStart = (int)((prevLeftVertex - prevRightVertex).magnitude / _sampleDistance);
            int xStepsEnd = (int)((leftVertex - rightVertex).magnitude / _sampleDistance);
            int zSteps = (int)(mag / _sampleDistance);
            float zIncrement = 1f / zSteps;

            // sample positions on a grid from prevNode to node, in steps of _sampleDistance + Random(_sampleRandomOffset)
            for (float z = 0; z < 1f; z += zIncrement)
            {
                for (float x = 0; x < 1f; x += 1f / Mathf.Lerp(xStepsStart, xStepsEnd, z))
                {
                    // sampled position, using the normalized sampling parameters X and Z
                    pos = Vector3.Lerp(
                       Vector3.Lerp(prevLeftVertex, prevRightVertex, x),
                       Vector3.Lerp(leftVertex, rightVertex, x),
                       z) +
                       new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)) * _sampleRandomOffset
                       ;

                    // check if viable via raycasts
                    RaycastHit hit;
                    // if we hit something from the avoid layer (such as other trees), do not place a tree here.
                    if (_avoidLayer != 0)
                    {
                        if (Physics.Raycast(pos + (Vector3.up * mag * 0.5f), Vector3.down, out hit, mag, _avoidLayer))
                        {
                            continue;
                        }
                    }

                    // if we do not hit the terrain layer, do not place a tree here.
                    if (Physics.Raycast(pos + (Vector3.up * mag * 0.5f), Vector3.down, out hit, mag, _terrainLayer))
                    {
                        pos = hit.point;
                    }
                    else
                    {
                        continue;
                    }

                    // is pos viable based on noise?
                    var noiseValue = noise.GetNoise(pos.x, pos.z);
                    if (noiseValue > _spawnNoiseThreshold)
                    {
                        // spawn one tree at pos
                        var r = SpawnTree(pos, Quaternion.Euler(0, Random.Range(0, 360f), 0));
                        r.transform.localScale *= _randomScale.Random();

                        //Debug.DrawRay(pos, Vector3.up * 10, Color.red, 7f);

                        _treesThisFrame++;

                    }
                    else
                    {
                        //Debug.DrawRay(pos, Vector3.up * 10, Color.yellow, 7f);
                    }

                }
            }

        }

        private GameObject SpawnTree(Vector3 position, Quaternion rotation)
        {
            if (_useSpawnManager && Application.isPlaying)
            {
                var prefabToSpawn = _prefabsToSpawn.Random();
                var tree = FoggyForestPoolManager.instance.Spawn(prefabToSpawn, position, rotation);
                tree.transform.SetParent(parent);
                _spawnedTrees.Add(tree.gameObject);
                return tree.gameObject;
            }
            else
            {
                var go = this.InstantiateSafe(_treePrefabs.Random(), position, rotation, parent);
                _spawnedTrees.Add(go);
                return go;
            }
        }

        public override void Clear()
        {
            StopCoroutine("ApplyMultiFrames");

            base.Clear();

            for (int i = 0; i < _spawnedTrees.Count; i++)
            {
                DespawnTree(_spawnedTrees[i]);
            }

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _spawnedTrees.Clear();

            DoneClearing();
        }

        private void DespawnTree(GameObject tree)
        {
            if (_useSpawnManager && Application.isPlaying)
            {
                FoggyForestPoolManager.instance.Return(tree.GetComponent<IVRPrefab>());
            }
            else
            {
                this.DestroySafe(tree);
            }
        }
    }
}