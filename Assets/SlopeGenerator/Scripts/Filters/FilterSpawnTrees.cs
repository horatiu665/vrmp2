namespace Slope
{
    using System.Collections.Generic;
    using UnityEngine;

    [System.Obsolete]
    public class FilterSpawnTrees : SlopeFilter
    {
        public GameObject treePrefab;

        [Header("Spawn params")]
        public float chancePerNode = 0.1f;

        public bool orientToNode = false;

        [Tooltip("X from 0 to 1 represents random input, Y from 0 to 1, returns lerp pos between the left and right vertices of the node")]
        public AnimationCurve positionDistribution = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [HideInInspector]
        [SerializeField]
        private List<GameObject> spawnedTrees = new List<GameObject>();

        [HideInInspector]
        [SerializeField]
        private Transform _parent;

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject(treePrefab.name + "s").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            // spawn trees along nodes
            for (int i = 0; i < slope.nodes.Count; i++)
            {
                if (Random.Range(0, 1f) < chancePerNode)
                {
                    var pos = Vector3.Lerp(slope.nodes[i].leftVertex, slope.nodes[i].rightVertex, positionDistribution.Evaluate(Random.Range(0, 1f)));
                    GameObject t = Instantiate(treePrefab, pos, Quaternion.identity) as GameObject;
                    if (orientToNode)
                    {
                        t.transform.up = slope.nodes[i].transform.up;
                    }
                    spawnedTrees.Add(t);
                    t.transform.SetParent(parent);
                }
            }

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            for (int i = 0; i < spawnedTrees.Count; i++)
            {
                if (spawnedTrees[i] != null)
                {
                    DestroyImmediate(spawnedTrees[i].gameObject);
                }
            }
            spawnedTrees.Clear();

            if (_parent != null)
            {
                DestroyImmediate(_parent.gameObject);
                _parent = null;
            }

            DoneClearing();
        }
    }
}