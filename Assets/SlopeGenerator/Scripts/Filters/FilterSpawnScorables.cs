namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterSpawnScorables : SlopeFilter
    {
        [SerializeField]
        private GameObject[] _scorablePrefabs = new GameObject[0];

        [SerializeField]
        private GameObject _campfirePrefab = null;

        [Space]
        [SerializeField]
        private LayerMask _terrainLayer = 0;

        [SerializeField]
        private int _startNodeIndexOffset = 2;

        [SerializeField]
        private int _endNodeIndexOffset = 2;

        [SerializeField, MinMax, Tooltip("How many clusters to generate.")]
        private Vector2 _clustersCount = new Vector2(1, 4);

        [SerializeField, MinMax, Tooltip("How many scorables there are per cluster.")]
        private Vector2 _clusterSizeRange = new Vector2(1, 15);

        [SerializeField, Tooltip("The maximum angle between two nodes, before that stretch is considered viable for spawning.")]
        private float _maxNodeDeltaAngle = 30f;

        [SerializeField]
        private bool _alignToNode = true;

        [SerializeField, Range(0, 100), Tooltip("The maximum iterations through the list of nodes allowed (to ensure enough clusters).")]
        private int _maxIterations = 10;

        [SerializeField, Range(0, 10000)]
        private int _maxClusterPlacementAttempts = 1000;

        [SerializeField]
        private int _minNodeSeparation = 3;

        [SerializeField, MinMax, Tooltip("Randomizes the scale of spawned game objects between the min and max values set here.")]
        private Vector2 _randomScale = Vector2.one;

        [SerializeField, MinMax]
        private Vector2 _randomTowardsEdgePercentage = new Vector2(0f, 0.5f);

        [SerializeField, MinMax]
        private Vector2 _campfireSpawnChance = new Vector2(0f, 0.5f);

        [SerializeField]
        private NodeType[] _ignoredNodeTypes = new NodeType[]
        {
            NodeType.PreRamp,
            NodeType.Ramp,
            NodeType.Jump,
            NodeType.JumpCurve
        };

        [Space]
        [SerializeField, ReadOnly]
        private int _clusters;

        [SerializeField, ReadOnly]
        private Transform _parent;

        [SerializeField, ReadOnly]
        private List<GameObject> _scorables = new List<GameObject>();

        private HashSet<NodeType> _ignoredNodes;

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("Scorables").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            _ignoredNodes = new HashSet<NodeType>(_ignoredNodeTypes, new NodeTypeComparer());

            _clusters = 0;
            var counter = 0;
            var target = _clustersCount.RandomInt();
            while (counter++ < _maxIterations && _clusters <= target)
            {
                SpawnClusters(slope);
            }

            DoneApplying();
        }

        private void SpawnClusters(SlopeManager slope)
        {
            var nodes = slope.nodes;
            var start = Mathf.Max(0, _startNodeIndexOffset);
            var end = Mathf.Min(nodes.Count, nodes.Count - _endNodeIndexOffset);

            var nextIndex = 0;
            nodes.ForEachChild(start, (index, prevNode, node) =>
            {
                if (index >= end || index < nextIndex)
                {
                    return;
                }

                if (_ignoredNodes.Contains(node.nodeType) || _ignoredNodes.Contains(prevNode.nodeType))
                {
                    // do not place at ignored node types
                    return;
                }

                if (_clusters >= _clustersCount.y)
                {
                    // enough clusters spawned
                    return;
                }

                var angle = Vector3.Angle(node.transform.forward, prevNode.transform.forward);
                if (angle > _maxNodeDeltaAngle)
                {
                    return;
                }

                CreateCluster(node, prevNode, _scorablePrefabs.Random());
                nextIndex = index + _minNodeSeparation; // try to avoid clustering at nearby nodes
            });
        }

        private void CreateCluster(Node node, Node prevNode, GameObject prefab)
        {
            var dir = (node.transform.position - prevNode.transform.position);
            var mag = dir.magnitude;
            var halfMag = mag * 0.5f;
            var center = prevNode.transform.position + (dir * 0.5f);

            var edgeFactor = _randomTowardsEdgePercentage.Random();
            if (edgeFactor > 0f)
            {
                var edgeDir = Utilities.FlipCoin() == 1 ? (prevNode.leftVertex - prevNode.transform.position) : (prevNode.rightVertex - prevNode.transform.position);
                var edgeOffset = edgeDir * edgeFactor;
                center += edgeOffset;
            }

            var clusterSize = _clusterSizeRange.RandomInt();

            var clusterParent = new GameObject(string.Concat("Cluster ", ((1 + _clusters++)).ToString(), " (", clusterSize.ToString(), " ", prefab.name, ")"));
            clusterParent.transform.position = center;
            clusterParent.transform.SetParent(parent.transform, true);

            if (_campfirePrefab != null)
            {
                var campfireChance = Mathf.Clamp01(_campfireSpawnChance.Random());
                if (campfireChance > 0f && Random.value < campfireChance)
                {
                    var rot = Quaternion.Slerp(prevNode.transform.rotation, node.transform.rotation, 0.5f) * Quaternion.Euler(-90f, 0f, 0f); // TODO: "Magic" rotation due to fireplace prefab requiring -90 on X rotation
                    this.InstantiateSafe(_campfirePrefab, center, rot, clusterParent.transform);
                }
            }

            var currentAttempts = 0;
            var currentSize = 0;
            while (currentSize < clusterSize && currentAttempts++ < _maxClusterPlacementAttempts)
            {
                var pos = center + (Random.onUnitSphere * halfMag);
                pos.y = center.y;
                var origin = pos + (Vector3.up * halfMag);

                RaycastHit hit;
                if (Physics.Raycast(origin, Vector3.down, out hit, mag, _terrainLayer))
                {
                    var rot = _alignToNode ?
                        Quaternion.Slerp(prevNode.transform.rotation, node.transform.rotation, (pos - node.transform.position).magnitude / mag) :
                        Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);

                    var go = this.InstantiateSafe(prefab, hit.point, rot, clusterParent.transform);
                    _scorables.Add(go);
                    currentSize++;

                    var scale = _randomScale.Random();
                    if (scale != 1f)
                    {
                        go.transform.localScale *= scale;
                    }
                }
            }
        }

        public override void Clear()
        {
            base.Clear();

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _clusters = 0;
            _scorables.Clear();

            DoneClearing();
        }
    }
}