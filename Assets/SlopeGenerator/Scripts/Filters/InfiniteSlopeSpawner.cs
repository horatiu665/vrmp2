namespace Slope
{
    using System.Linq;
    using UnityEngine;
    using VRCore;

    [ExecuteInEditMode]
    public class InfiniteSlopeSpawner : SlopeFilter
    {
        /// <summary>
        /// this prefab will be placed at the end of the slope to generate a new slope when the player is nearby
        /// </summary>
        public GameObject infiniteSlopeTriggerPrefab;

        [HideInInspector]
        [SerializeField]
        private InfiniteSlopeTrigger infiniteSlopeTriggerInst;

        [HideInInspector]
        [SerializeField]
        private Transform _parent;

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject(infiniteSlopeTriggerPrefab.name + "s").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            // create a spawner at the end of the slope. when player is nearby, generate another slope there.

            Node node = slope.nodes.First(n => n.nodeType == NodeType.Goal);

            var pos = Vector3.Lerp(node.leftVertex, node.rightVertex, 0.5f);
            GameObject t = this.InstantiateSafe(infiniteSlopeTriggerPrefab, pos, Quaternion.identity) as GameObject;
            //var euler = node.transform.eulerAngles;

            infiniteSlopeTriggerInst = t.GetComponent<InfiniteSlopeTrigger>();
            t.transform.SetParent(parent);

            infiniteSlopeTriggerInst.nextSlopeRandomSeed = Random.Range(int.MinValue, int.MaxValue);

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            if (infiniteSlopeTriggerInst != null)
            {
                infiniteSlopeTriggerInst.ClearNewSlope();
                DestroyImmediate(infiniteSlopeTriggerInst.gameObject);
            }

            if (_parent != null)
            {
                DestroyImmediate(_parent.gameObject);
                _parent = null;
            }

            DoneClearing();
        }
    }
}