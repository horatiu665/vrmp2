namespace Slope
{
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterSpawnParallelSlope : SlopeFilter
    {
#pragma warning disable 0649

        [Header("Spawn params")]
        [SerializeField, MinMax]
        private Vector2 verticalOffset;

        [SerializeField, MinMax]
        private Vector2 horizontalOffset;

        [SerializeField, MinMax]
        private Vector2 startNodeSpan;

        [SerializeField, MinMax]
        private Vector2 deltaEndNode;

        [SerializeField, MinMax]
        private Vector2 endNodeSpan;

        [SerializeField, Tooltip("The chance of the parallel slope merging back into the main slope versus ending in a dead end")]
        private float chanceOfMerging;

        [SerializeField, Tooltip("The chance of the parallel slope splitting out from main slope versus starting as a dead end")]
        private float chanceOfSplit;

        [SerializeField, Tooltip("When TRUE ensures the slope - especially the when merging back to the main slope - has a negative drop all the way through")]
        private bool forceNegativeDrop;

#pragma warning restore 0649
        private bool _shouldMerge;
        private bool _shouldSplit;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (Random.value < chanceOfMerging)
            {
                _shouldMerge = true;
            }
            else
            {
                _shouldMerge = false;
            }

            if (Random.value < chanceOfSplit)
            {
                _shouldSplit = true;
                //as we have both a start and an end node we can now start creating the parallel slope
            }
            else
            {
                _shouldSplit = false;
            }

            var nodes = slope.nodes;

            var horizontalOffset = this.horizontalOffset.Random();
            var verticalOffset = this.verticalOffset.Random();

            //start node can only be split out from the the list of acceptable node types
            var startNodeIndex = Utilities.GetRandomNodeFromList(nodes, this.startNodeSpan, new NodeType[] { NodeType.Straight });

            if (startNodeIndex == -1)
            {
                Debug.Log(this + " failed: no available start node");
                return;
            }

            var startNode = nodes[startNodeIndex];

            var startNodePosition = startNode.transform.position;

            if (_shouldSplit)
            {
                startNode.nodeType = NodeType.Split;
            }
            else
            {
                startNode = null;
            }

            //the end node will be offset by a random amount of nodes down the node graph, as the start node, it can only merge with acceptable node types
            var delta = deltaEndNode.RandomInt() + startNodeIndex;
            var deltaEndNodeSpan = new Vector2(endNodeSpan.x + delta, endNodeSpan.y + delta);
            var endNodeIndex = Utilities.GetRandomNodeFromList(nodes, deltaEndNodeSpan, new NodeType[] { NodeType.Straight });

            if (endNodeIndex == -1)
            {
                Debug.Log(this + " failed: no available end node");
                return;
            }

            var endNode = nodes[endNodeIndex];
            var endNodePosition = endNode.transform.position;
            var parentNode = startNode;
            var countNodes = endNodeIndex - startNodeIndex;

            var deltaVerticalDrop = 0f;
            if (forceNegativeDrop)
            {
                deltaVerticalDrop = Delta(verticalOffset, startNodePosition, endNodePosition, countNodes);
            }

            int j = -1;

            //offset the nodes in between the start and end nodes
            for (int i = startNodeIndex + 1; i < endNodeIndex; i++)
            {
                j++;
                var node = nodes[i];
                var pos = node.transform.position + node.transform.right * horizontalOffset + node.transform.up * (verticalOffset + (j * deltaVerticalDrop));
                var parallelNode = Utilities.CreateNode(pos, node.transform.forward, NodeType.Straight, slope.nodesParent, node.width, node.depth, parentNode);
                slope.nodes.Add(parallelNode);
                if (parentNode != null)
                {
                    parentNode.children.Add(parallelNode);
                }
                parentNode = parallelNode;
                Debug.DrawLine(node.transform.position, pos, Color.red, 20f);
                //todo: consider setting the direction of the parent node here
            }

            if (_shouldMerge)
            {
                parentNode.children.Add(endNode);
                endNode.nodeType = NodeType.Merge;
            }
            else
            {
                endNode.nodeType = NodeType.Straight;
            }

            endNode.parents.Add(parentNode);

            DoneApplying();
        }

        private float Delta(float currentVerticalOffset, Vector3 startNodePosition, Vector3 endNodePosition, int countNodes)
        {
            //var realDrop = (endNodePosition - startNodePosition).y;
            //var desiredParallelDrop = realDrop - currentVerticalOffset;
            var delta = currentVerticalOffset / countNodes;
            delta *= -1f;
            return delta;
        }
    }
}