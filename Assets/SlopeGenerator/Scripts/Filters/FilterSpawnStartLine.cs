namespace Slope
{
    using Apex;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterSpawnStartLine : SlopeFilter
    {
        [SerializeField, Tooltip("The start line prefab to be used")]
        private GameObject _startLinePrefab = null;

        [SerializeField, ReadOnly]
        private GameObject _startLine;

        [SerializeField]
        private string _poleLeftName = "PoleLeft";

        [SerializeField]
        private string _poleRightName = "PoleRight";

        [SerializeField]
        private string _flagName = "FlagSizer";

        [SerializeField, Range(0f, 100f), Tooltip("The margin from the side of the slope to the poles")]
        private float _margin = 2f;

        private GameObject _leftPole, _rightPole, _flag;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (_startLinePrefab == null)
            {
                Debug.LogError(this.ToString() + " is missing a StartLinePrefab reference!");
                return;
            }

            if (_startLine == null || _startLine.Equals(null))
            {
                // create and cache the start line, as we will always only need one
                _startLine = this.InstantiateSafe(_startLinePrefab, Vector3.zero, Quaternion.identity, slope.generatedParent);
                _startLine.transform.localPosition = Vector3.zero;
                _startLine.transform.localRotation = Quaternion.identity;
            }

            _startLine.SetActive(true);
            AssignParts();

            var nodes = slope.nodes;
            var done = false;
            nodes.ForEachChild((index, prevNode, node) =>
            {
                if (done)
                {
                    return;
                }

                if (prevNode.nodeType != NodeType.Start)
                {
                    return;
                }

                var nextNode = node.children[0]; // TODO: multiple children?
                var p1 = node.transform.position;
                var p2 = nextNode.transform.position;
                PlaceAndSize(p2 - p1, node.leftVertex, node.rightVertex);
                done = true;
            });

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            if (_startLine != null)
            {
                this.DestroySafe(_startLine);
                _startLine = null;
            }

            DoneClearing();
        }

        private void AssignParts()
        {
            _leftPole = _startLine.FindChild(_poleLeftName);
            if (_leftPole == null)
            {
                Debug.LogError(this.ToString() + " could not find a left pole named == " + _poleLeftName);
            }

            _rightPole = _startLine.FindChild(_poleRightName);
            if (_rightPole == null)
            {
                Debug.LogError(this.ToString() + " could not find a right pole named == " + _poleRightName);
            }

            _flag = _startLine.FindChild(_flagName);
            if (_flag == null)
            {
                Debug.LogError(this.ToString() + " could not find a flag named == " + _flagName);
            }
        }

        private void PlaceAndSize(Vector3 forward, Vector3 leftPosition, Vector3 rightPosition)
        {
            var dirLR = rightPosition - leftPosition;
            var dirLRNorm = dirLR.normalized;
            var flagPos = leftPosition + (dirLR * 0.5f);

            // Place the 'flag' at the middle between the two vertices
            _startLine.transform.position = flagPos;

            var leftPolePos = leftPosition + dirLRNorm * _margin;
            var rightPolePos = rightPosition - dirLRNorm * _margin;

            var localScale = _flag.transform.localScale;
            localScale.x = (leftPolePos - rightPolePos).magnitude;
            _flag.transform.localScale = localScale;

            //set the rotation to match the skiing direction
            _startLine.transform.rotation = Quaternion.LookRotation(forward.OnlyXZ());

            _leftPole.transform.position = leftPolePos;
            _rightPole.transform.position = rightPolePos;
        }
    }
}