namespace Slope
{
    using System.Collections.Generic;
    using UnityEngine;
    using VRCore;

    public class FilterExecuteScoreRingTargets : SlopeFilter
    {
        [SerializeField, Tooltip("Whether to use FindObjectsOfType for identifying ScoreRingSpawnerComponents in the scene, or not (false).")]
        private bool _autoFindScoreRingSpawnerTargets = true;

        [SerializeField, Tooltip("If not auto finding ScoreRingSpawnerComponents, this array must be populated with the relevant components.")]
        private ScoreRingTargetSpawnerComponent[] _scoreRingSpawnerTargets = new ScoreRingTargetSpawnerComponent[0];

        [Space]
        [SerializeField, ReadOnly]
        private List<ScoreRingTargetSpawnerComponent> _spawners = new List<ScoreRingTargetSpawnerComponent>();

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var spawners = _scoreRingSpawnerTargets;
            if (_autoFindScoreRingSpawnerTargets)
            {
                spawners = FindObjectsOfType<ScoreRingTargetSpawnerComponent>();
            }

            if (spawners.Length == 0)
            {
                Debug.LogWarning(this.ToString() + " cannot operate without any ScoreRingSpawnerComponents. Either set it to 'Auto Find Score Ring Spawners' or manually assign the relevant objects.");
                return;
            }

            for (int i = 0; i < spawners.Length; i++)
            {
                spawners[i].SpawnScoreRings();
            }

            _spawners.Clear();
            _spawners.AddRange(spawners);

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            var count = _spawners.Count;
            for (int i = 0; i < count; i++)
            {
                _spawners[i].Clear();
            }

            _spawners.Clear();

            DoneClearing();
        }
    }
}