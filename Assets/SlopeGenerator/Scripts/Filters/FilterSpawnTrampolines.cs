namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterSpawnTrampolines : SlopeFilter
    {
        [SerializeField]
        private GameObject _trampolinePrefab = null;

        [SerializeField, Range(0f, 1000f)]
        private float _backMargin = 2f;

        [SerializeField, ReadOnly]
        private Transform _parent;

        [SerializeField, ReadOnly]
        private List<GameObject> _trampolines = new List<GameObject>();

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("Trampolines").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var nodes = slope.nodes;
            var count = nodes.Count;
            nodes.ForEachChild(1, (index, prevNode, node) =>
            {
                if (index >= count - 2)
                {
                    return;
                }

                if (node.nodeType != NodeType.Jump && node.nodeType != NodeType.JumpCurve)
                {
                    return;
                }

                var left = node.leftVertex;
                var right = node.rightVertex;
                var lr = right - left;
                var center = left + (lr * 0.5f);

                var forward = (node.transform.position - prevNode.transform.position);
                var rotation = Quaternion.LookRotation(forward, Vector3.up);
                var position = center - (forward.normalized * _backMargin);
                _trampolines.Add(this.InstantiateSafe(_trampolinePrefab, position, rotation, parent));
            });

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _trampolines.Clear();

            DoneClearing();
        }
    }
}