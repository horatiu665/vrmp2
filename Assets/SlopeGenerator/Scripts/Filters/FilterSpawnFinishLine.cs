namespace Slope
{
    using Apex;
    using Helpers;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    public class FilterSpawnFinishLine : SlopeFilter
    {
        [Tooltip("The finish line prefab to be used"), SerializeField, FormerlySerializedAs("finishLinePrefab")]
        private GameObject _finishLinePrefab = null;

        [SerializeField, ReadOnly]
        private GameObject _finishLine;

        [SerializeField]
        private string _poleLeftName = "PoleLeft";

        [SerializeField]
        private string _poleRightName = "PoleRight";

        [SerializeField]
        private string _flagName = "FlagSizer";

        [SerializeField, Range(0f, 100f), FormerlySerializedAs("margin")]
        private float _margin = 2f;

        private GameObject _leftPole, _rightPole, _flag;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (_finishLine == null)
            {
                // Create and cache the finish line, as we will always only need one
                _finishLine = this.InstantiateSafe(_finishLinePrefab.gameObject, Vector3.zero, Quaternion.identity, slope.generatedParent);
            }

            _finishLine.SetActive(true);
            AssignFinishLineParts();
            FindAndAssignGoalNode(slope);

            DoneApplying();
        }

        private void FindAndAssignGoalNode(SlopeManager slope)
        {
            var nodes = slope.nodes;

            // Start fromt the bottom and go through all nodes
            var done = false;
            nodes.ForEachParent((index, prevNode, node) =>
            {
                if (done)
                {
                    return;
                }

                if (prevNode.nodeType != NodeType.Goal)
                {
                    return;
                }

                var p1 = node.transform.position;
                var p2 = prevNode.transform.position;
                PlaceAndSize(p2 - p1, node.leftVertex, node.rightVertex);
                done = true;
            });
        }

        private void AssignFinishLineParts()
        {
            _leftPole = _finishLine.FindChild(_poleLeftName);
            if (_leftPole == null)
            {
                Debug.LogError(this.ToString() + " could not find a left pole named == " + _poleLeftName);
            }

            _rightPole = _finishLine.FindChild(_poleRightName);
            if (_rightPole == null)
            {
                Debug.LogError(this.ToString() + " could not find a right pole named == " + _poleRightName);
            }

            _flag = _finishLine.FindChild(_flagName);
            if (_flag == null)
            {
                Debug.LogError(this.ToString() + " could not find a flag named == " + _flagName);
            }
        }

        private void PlaceAndSize(Vector3 forward, Vector3 leftPosition, Vector3 rightPosition)
        {
            var dirLR = rightPosition - leftPosition;
            var dirLRNorm = dirLR.normalized;
            var flagPos = leftPosition + (dirLR * 0.5f);

            // Place the 'flag' at the middle between the two vertices
            _finishLine.transform.position = flagPos;

            var leftPolePos = leftPosition + dirLRNorm * _margin;
            var rightPolePos = rightPosition - dirLRNorm * _margin;

            var localScale = _flag.transform.localScale;
            localScale.x = (leftPolePos - rightPolePos).magnitude;
            _flag.transform.localScale = localScale;

            //set the rotation to match the skiing direction
            _finishLine.transform.rotation = Quaternion.LookRotation(forward.OnlyXZ());

            _leftPole.transform.position = leftPolePos;
            _rightPole.transform.position = rightPolePos;
        }

        public override void Clear()
        {
            base.Clear();

            if (_finishLine != null)
            {
                this.DestroySafe(_finishLine);
                _finishLine = null;
            }

            DoneClearing();
        }
    }
}