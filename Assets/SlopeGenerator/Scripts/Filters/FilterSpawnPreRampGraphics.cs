namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public sealed class FilterSpawnPreRampGraphics : SlopeFilter
    {
        [SerializeField]
        private GameObject _prefab = null;

        [SerializeField]
        private LayerMask _terrainLayer = 0;

        [SerializeField]
        private float _addYPosition = 0.1f;

        [SerializeField]
        private int _addToNodesBefore = 3;

        [SerializeField]
        private float _spacing = 15f;

        [SerializeField]
        private int _maxRampGraphicsPerRamp = 1;

        [SerializeField]
        private NodeType[] _allowedNodeTypes = new NodeType[]
        {
            NodeType.PreRamp,
            NodeType.Ramp
        };

        [Space]
        [SerializeField, ReadOnly]
        private Transform _parent;

        [SerializeField, ReadOnly]
        private List<GameObject> _spawnedPrefabs = new List<GameObject>();

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("PreRamp Graphics").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var allowedNodeTypes = new HashSet<NodeType>(_allowedNodeTypes, new NodeTypeComparer());

            Node startNode = null;
            var nodes = slope.nodes;

            nodes.ForEachChild(_addToNodesBefore, (index, node) =>
            {
                if (!allowedNodeTypes.Contains(node.nodeType))
                {
                    if (startNode != null)
                    {
                        SpawnGraphics(startNode, node);
                        startNode = null;
                    }
                }
                else if (startNode == null)
                {
                    node.ForEachParent((jindex, prevNode) =>
                    {
                        if (startNode != null || prevNode == node)
                        {
                            return;
                        }

                        if (allowedNodeTypes.Contains(prevNode.nodeType))
                        {
                            // only spawn at disallowed node types!
                            return;
                        }

                        startNode = prevNode;
                    });
                }
            });

            DoneApplying();
        }

        private void SpawnGraphics(Node startNode, Node endNode)
        {
            // spawn at the starting node
            var startPos = startNode.transform.position;
            var dir = (endNode.transform.position - startPos);
            var mag = dir.magnitude;
            var dirNorm = dir / mag;

            var steps = Mathf.RoundToInt(mag / _spacing);
            if (_maxRampGraphicsPerRamp > 0)
            {
                steps = Mathf.Min(_maxRampGraphicsPerRamp, steps);
            }

            for (int i = 0; i < steps; i++)
            {
                var pos = startPos + (dirNorm * (_spacing * i));
                RaycastHit hit;
                if (!Physics.Raycast(pos + (Vector3.up * mag * 0.5f), Vector3.down, out hit, mag, _terrainLayer))
                {
                    Debug.LogWarning(this.ToString() + " raycast did not hit terrain at pos == " + pos.ToString());
                    continue;
                }

                var go = Spawn(hit.point, Quaternion.identity);
                go.transform.up = hit.normal;
            }
        }

        private GameObject Spawn(Vector3 position, Quaternion rotation)
        {
            var pos = position + new Vector3(0f, _addYPosition, 0f);
            var go = this.InstantiateSafe(_prefab, pos, rotation, parent);
            _spawnedPrefabs.Add(go);

            return go;
        }

        public override void Clear()
        {
            base.Clear();

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _spawnedPrefabs.Clear();

            DoneClearing();
        }
    }
}