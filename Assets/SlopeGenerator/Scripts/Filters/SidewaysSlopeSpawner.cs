namespace Slope
{
    using System.Linq;
    using UnityEngine;

    [ExecuteInEditMode]
    public class SidewaysSlopeSpawner : SlopeFilter
    {
        /// <summary>
        /// prefab will be spawned at some random node, and the slope will continue sideways from there on, effectively splitting into a new direction
        /// </summary>
        public GameObject slopeGeneratorPrefab;

        [HideInInspector]
        [SerializeField]
        private GameObject slopeGeneratorInstance;

        [HideInInspector]
        [SerializeField]
        private Transform _parent;

        private bool youOnlyCreateOnce = false;

        [SerializeField]
        private bool spawnAnotherSlopeManually = false;

        private SlopeManager newSlopeInstance;

        public float chancePerSlope = 0.5f;

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject(slopeGeneratorPrefab.name + "s").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (Random.Range(0, 1f) < chancePerSlope)
            {
                // create a spawner at some node on the side of the slope. when player is nearby, generate another slope there.

                int straightNodes = slope.nodes.Where(n => n.nodeType == NodeType.Straight).Count();
                int which = Random.Range(0, straightNodes);
                int c = 0;
                for (int i = 0; i < slope.nodes.Count; i++)
                {
                    if (slope.nodes[i].nodeType == NodeType.Straight)
                    {
                        if (c == which)
                        {
                            // found it

                            var node = slope.nodes[i].transform;
                            GameObject t = Instantiate(slopeGeneratorPrefab, node.position, Quaternion.LookRotation(node.right * Utilities.FlipCoin())) as GameObject;
                            //var euler = node.transform.eulerAngles;

                            slopeGeneratorInstance = t;
                            t.transform.SetParent(parent);

                            t.GetComponent<InfiniteSlopeTrigger>().TriggerEnter += InfiniteSlopeSpawner_TriggerEnter;

                            break;
                        }
                        else
                        {
                            c++;
                        }
                    }
                }
            }

            DoneApplying();
        }

        private void InfiniteSlopeSpawner_TriggerEnter()
        {
            // trigger was hit, time to make a level if we haven't already
            if (!youOnlyCreateOnce)
            {
                youOnlyCreateOnce = true;
                SpawnNewSlope();
            }
        }

        private SlopeManager SpawnNewSlope()
        {
            ClearNewSlope();
            newSlopeInstance = SlopePrefabProvider.instance.GetNewSlope(slope, slopeGeneratorInstance.transform.position, slopeGeneratorInstance.transform.rotation);
            return newSlopeInstance;
        }

        private void ClearNewSlope()
        {
            if (newSlopeInstance != null)
            {
                newSlopeInstance.ClearAll();
                if (Application.isEditor)
                {
                    DestroyImmediate(newSlopeInstance.gameObject);
                }
                else
                {
                    Destroy(newSlopeInstance.gameObject);
                }

                newSlopeInstance = null;
            }
        }

        public override void Clear()
        {
            base.Clear();

            ClearNewSlope();

            if (slopeGeneratorInstance != null)
            {
                DestroyImmediate(slopeGeneratorInstance.gameObject);
            }

            if (_parent != null)
            {
                DestroyImmediate(_parent.gameObject);
                _parent = null;
            }

            DoneClearing();
        }

        private void Update()
        {
            if (spawnAnotherSlopeManually)
            {
                spawnAnotherSlopeManually = false;
                SpawnNewSlope().GenerateAll();
            }
        }
    }
}