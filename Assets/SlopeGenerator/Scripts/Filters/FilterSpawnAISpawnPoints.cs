namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterSpawnAISpawnPoints : SlopeFilter
    {
        [SerializeField]
        private GameObject _aiSpawnPointPrefab = null;

        [SerializeField]
        private int _minNodeDistance = 10;

        [SerializeField]
        private int _startNodeIndexOffset = 2;

        [SerializeField]
        private int _endNodeIndexOffset = 2;

        [SerializeField, Range(0f, 1000f)]
        private float _edgeMargin = 3f;

        [SerializeField, Range(0f, 100f)]
        private float _addYPos = 1f;

        [Space]
        [SerializeField, ReadOnly]
        private Transform _parent;

        [SerializeField, ReadOnly]
        private List<GameObject> _aiSpawnPoints = new List<GameObject>();

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("AI Spawn Points").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var nodes = slope.nodes;
            var count = nodes.Count;
            var start = Mathf.Max(0, _startNodeIndexOffset);
            var end = Mathf.Min(count - 1, count - _endNodeIndexOffset);

            var nextIndex = 0;
            nodes.ForEachChild(start, (index, node, nextNode) =>
            {
                if (index >= end || index < nextIndex)
                {
                    return;
                }

                if (node.nodeType == NodeType.Jump || node.nodeType == NodeType.JumpCurve)
                {
                    // do not place at jump nodes
                    return;
                }

                var dir = (nextNode.transform.position - node.transform.position);
                var rotation = Quaternion.LookRotation(dir, Vector3.up);

                Vector3 pos;
                if (Random.value > 0.5f)
                {
                    // switch between right and left vertex now and then
                    pos = node.leftVertex + (node.rightVertex - node.leftVertex).normalized * _edgeMargin;
                }
                else
                {
                    pos = node.rightVertex + (node.leftVertex - node.rightVertex).normalized * _edgeMargin;
                }

                _aiSpawnPoints.Add(this.InstantiateSafe(_aiSpawnPointPrefab, pos + new Vector3(0f, _addYPos, 0f), rotation, parent));
                nextIndex = index + _minNodeDistance;
            });

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _aiSpawnPoints.Clear();

            DoneClearing();
        }
    }
}