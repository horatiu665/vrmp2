namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterSpawnCheckpoints : SlopeFilter
    {
        [SerializeField]
        private GameObject _checkpointPrefab = null;

        [Space]
        [SerializeField, Range(0, 1000)]
        private int _minNodeDistance = 5;

        [SerializeField]
        private int _startNodeIndexOffset = 2;

        [SerializeField]
        private int _endNodeIndexOffset = 2;

        [SerializeField]
        private NodeType[] _ignoredNodeTypes = new NodeType[]
        {
            NodeType.Start,
            NodeType.Ramp,
            NodeType.Jump,
            NodeType.JumpCurve,
            NodeType.Landing,
            NodeType.Goal,
        };

        [Space]
        [SerializeField, ReadOnly]
        private Transform _parent;

        [SerializeField, ReadOnly]
        private List<GameObject> _checkpoints = new List<GameObject>();

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("Checkpoints").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var ignoredNodeTypes = new HashSet<NodeType>(_ignoredNodeTypes, new NodeTypeComparer());

            var nodes = slope.nodes;
            var count = nodes.Count;
            var start = Mathf.Max(1, _startNodeIndexOffset);
            var end = Mathf.Min(count, count - _endNodeIndexOffset);

            var nextIndex = 0;
            nodes.ForEachChild(start, (index, prevNode, node) =>
            {
                if (index >= end || index < nextIndex)
                {
                    return;
                }

                if (ignoredNodeTypes.Contains(node.nodeType))
                {
                    // do not place at ignored nodes
                    return;
                }

                var dir = (node.transform.position - prevNode.transform.position);
                var rotation = Quaternion.LookRotation(dir, Vector3.up);

                _checkpoints.Add(this.InstantiateSafe(_checkpointPrefab, node.transform.position, rotation, parent));
                nextIndex = index + _minNodeDistance;
            });

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _checkpoints.Clear();

            DoneClearing();
        }
    }
}