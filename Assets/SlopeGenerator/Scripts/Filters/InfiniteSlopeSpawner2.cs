namespace Slope
{
    using System.Linq;
    using UnityEngine;

    [ExecuteInEditMode]
    public class InfiniteSlopeSpawner2 : SlopeFilter
    {
        public SlopeManager newSlope;

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            // if player is far enough away, instantiates newSlope with adapted parameters at the end of this slope



            DoneApplying();
        }
    }
}