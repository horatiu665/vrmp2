namespace Slope
{
    using System.Collections.Generic;
    using Apex;
    using Helpers;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    public class FilterSpawnTreesAdvanced : SlopeFilter
    {
        [SerializeField]
        private GameObject[] _treePrefabs = new GameObject[0];

        [Header("Spawn params")]
        [SerializeField]
        private LayerMask _terrainLayer = 0;

        [SerializeField]
        private LayerMask _avoidLayer = 0;

        [SerializeField]
        private float _marginToEdge = 0.5f;

        [SerializeField]
        private float _chancePerNode = 0.65f;

        [SerializeField, MinMax]
        private Vector2 _treeDistanceRange = new Vector2(3f, 5f);

        [SerializeField, MinMax]
        private Vector2 _randomPosRange = new Vector2(2f, 5f);

        [SerializeField]
        private int _startNodeIndexOffset = 0;

        [SerializeField]
        private int _endNodeIndexOffset = 0;

        [SerializeField, Range(0, 10000)]
        private int _maxSpawnAttempts = 100;

        [SerializeField]
        private NodeType[] _ignoredNodeTypes = new NodeType[]
        {
            NodeType.Jump,
            NodeType.JumpCurve
        };

        [SerializeField, MinMax, Tooltip("Randomizes the scale of spawned game objects between the min and max values set here.")]
        private Vector2 _randomScale = Vector2.one;

        [Space]
        [SerializeField, ReadOnly]
        private Transform _parent = null;

        [SerializeField, ReadOnly]
        private List<GameObject> _spawnedTrees = new List<GameObject>();

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject(_treePrefabs[0].name + "s").transform;
                    _parent.SetParent(slope.generatedParent, true);
                }
                return _parent;
            }
        }

        public override string ToString()
        {
            return base.ToString() + " " + _treePrefabs[0].name;
        }

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            if (_treePrefabs.Length == 0)
            {
                Debug.LogError(this.ToString() + " is missing at least one tree prefab!");
                return;
            }

            var ignoredNodeTypes = new HashSet<NodeType>(_ignoredNodeTypes, new NodeTypeComparer());

            // spawn trees along nodes
            var nodes = slope.nodes;
            var count = nodes.Count;
            var start = Mathf.Max(1, _startNodeIndexOffset);
            var end = Mathf.Min(count, count - _endNodeIndexOffset);

            nodes.ForEachChild(start, (index, prevNode, node) =>
            {
                if (index >= end)
                {
                    return;
                }

                if (ignoredNodeTypes.Contains(node.nodeType) || ignoredNodeTypes.Contains(prevNode.nodeType))
                {
                    return;
                }

                if (Random.value < _chancePerNode)
                {
                    SpawnTrees(prevNode, node, SideType.Left);
                }

                if (Random.value < _chancePerNode)
                {
                    SpawnTrees(prevNode, node, SideType.Right);
                }
            });

            DoneApplying();
        }

        private void SpawnTrees(Node prevNode, Node node, SideType sideType)
        {
            var startPos = Vector3.zero;
            var marginDir = Vector3.zero;
            var dir = Vector3.zero;
            if (sideType == SideType.Left)
            {
                startPos = prevNode.leftVertex;
                dir = (node.leftVertex - startPos);
                marginDir = (prevNode.rightVertex - startPos).OnlyXZ().normalized;
            }
            else
            {
                startPos = prevNode.rightVertex;
                dir = (node.rightVertex - startPos);
                marginDir = (prevNode.leftVertex - startPos).OnlyXZ().normalized;
            }

            var mag = dir.magnitude;
            var dirNormalized = dir / mag;
            var marginOffset = marginDir * _marginToEdge;

            var attempts = 0;
            var remainingDistance = mag;
            while (remainingDistance > _treeDistanceRange.x && attempts++ < _maxSpawnAttempts)
            {
                var distance = _treeDistanceRange.Random();
                remainingDistance -= distance;

                var pos = startPos + marginOffset + (dirNormalized * (mag - remainingDistance)) + (marginDir * _randomPosRange.Random());

                RaycastHit hit;
                // if we hit something from the avoid layer (such as other trees), do not place a tree here.
                if (_avoidLayer != 0)
                {
                    if (Physics.Raycast(pos + (Vector3.up * mag * 0.5f), Vector3.down, out hit, mag, _avoidLayer))
                    {
                        continue;
                    }
                }

                // if we hit the terrain layer, we are allowed to place a tree here.
                if (Physics.Raycast(pos + (Vector3.up * mag * 0.5f), Vector3.down, out hit, mag, _terrainLayer))
                {
                    var rot = Quaternion.Euler(0f, Random.Range(0f, 360f), 0f);
                    var go = SpawnTree(hit.point + (marginOffset * 0.5f), rot);

                    var scale = _randomScale.Random();
                    if (scale != 1f)
                    {
                        go.transform.localScale *= scale;
                    }
                }
                else
                {
                    // add the distance once again to 're-iterate'
                    remainingDistance += distance;
                }
            }
        }

        private GameObject SpawnTree(Vector3 position, Quaternion rotation)
        {
            var go = this.InstantiateSafe(_treePrefabs.Random(), position, rotation, parent);
            _spawnedTrees.Add(go);
            return go;
        }

        public override void Clear()
        {
            base.Clear();

            if (_parent != null)
            {
                this.DestroySafe(_parent.gameObject);
                _parent = null;
            }

            _spawnedTrees.Clear();

            DoneClearing();
        }
    }
}