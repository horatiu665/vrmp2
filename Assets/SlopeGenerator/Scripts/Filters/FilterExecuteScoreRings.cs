namespace Slope
{
    using System.Collections.Generic;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public class FilterExecuteScoreRings : SlopeFilter
    {
        [SerializeField, Tooltip("Whether to use FindObjectsOfType for identifying ScoreRingSpawnerComponents in the scene, or not (false).")]
        private bool _autoFindScoreRingSpawners = true;

        [SerializeField, Tooltip("If not auto finding ScoreRingSpawnerComponents, this array must be populated with the relevant components.")]
        private ScoreRingSpawnerComponent[] _scoreRingSpawners = new ScoreRingSpawnerComponent[0];

        [Space]
        [SerializeField, ReadOnly]
        private List<ScoreRingSpawnerComponent> _spawners = new List<ScoreRingSpawnerComponent>();

        public override void Apply(SlopeManager slope)
        {
            base.Apply(slope);

            var spawners = _scoreRingSpawners;
            if (_autoFindScoreRingSpawners)
            {
                spawners = FindObjectsOfType<ScoreRingSpawnerComponent>();
            }

            if (spawners.Length == 0)
            {
                Debug.LogWarning(this.ToString() + " cannot operate without any ScoreRingSpawnerComponents. Either set it to 'Auto Find Score Ring Spawners' or manually assign the relevant objects.");
                return;
            }

            for (int i = 0; i < spawners.Length; i++)
            {
                spawners[i].SpawnScoreRings();
            }

            _spawners.Clear();
            _spawners.AddRange(spawners);

            DoneApplying();
        }

        public override void Clear()
        {
            base.Clear();

            var count = _spawners.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                _spawners[i].Clear();
                this.DestroySafe(_spawners[i].gameObject);
            }

            _spawners.Clear();

            DoneClearing();
        }
    }
}