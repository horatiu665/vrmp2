﻿namespace Slope
{
    public enum NodeType
    {
        None,
        Straight,
        Split,
        Merge,
        PreRamp,
        Ramp,
        Jump,
        JumpCurve,
        Landing,
        Start,
        Goal
    }
}
