
namespace Slope
{
    using UnityEngine;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    public class SlopeParameters
    {
        public List<Node> nodes;
        public Transform nodesParent;
        
        public SlopeParameters(List<Node> nodes, Transform nodeParent)
        {
            this.nodes = nodes;
            this.nodesParent = nodeParent;
        }
    }
}
