﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PhysicsHiddenAssSettings : MonoBehaviour
{
    public float maxAngularVelocity = 27;
    float _maxAngularVelocity = 27;

    public int solverIterations = 6;
    private int _solverIterations = 6;

    public int solverVelocityIterations = 1;
    private int _solverVelocityIterations = 1;

    public bool onUpdate = false;

    private Rigidbody _r;
    public Rigidbody r
    {
        get
        {
            if (_r == null)
            {
                _r = GetComponent<Rigidbody>();
            }
            return _r;
        }
    }

    private void Start()
    {
        SetChangedValues();
    }

    private void Update()
    {
        if (!onUpdate)
        {
            return;
        }
        SetChangedValues();
    }

    private void SetChangedValues()
    {
        if (_maxAngularVelocity != maxAngularVelocity)
        {
            _maxAngularVelocity = maxAngularVelocity;
            r.maxAngularVelocity = maxAngularVelocity;
        }
        if (_solverIterations != solverIterations)
        {
            _solverIterations = solverIterations;
            r.solverIterations = solverIterations;
        }
        if (_solverVelocityIterations != solverVelocityIterations)
        {
            _solverVelocityIterations = solverVelocityIterations;
            r.solverVelocityIterations = solverVelocityIterations;
        }
    }
}