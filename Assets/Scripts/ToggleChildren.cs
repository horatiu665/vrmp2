﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ToggleChildren : MonoBehaviour
{

    private List<Transform> _children;
    public List<Transform> children
    {
        get
        {
            if (_children == null || _children.Count != transform.childCount)
            {
                _children = transform.GetChildren();
            }
            return _children;
        }
    }

    private void OnEnable()
    {
        if (children.Count(t => t.gameObject.activeSelf) > 1)
        {
            var firstEnabled = children.First(t => t.gameObject.activeSelf);
            DisableAll();
            firstEnabled.gameObject.SetActive(true);
        }
    }

    [DebugButton]
    public void DisableAll()
    {
        for (int i = 0; i < children.Count; i++)
        {
            children[i].gameObject.SetActive(false);
        }
    }

    [DebugButton]
    public void ToggleNextChild()
    {
        for (int i = 0; i < children.Count; i++)
        {
            // first active child, just set the next one true and return.
            if (children[i].gameObject.activeSelf)
            {
                children[i].gameObject.SetActive(false);
                children[(i + 1) % children.Count].gameObject.SetActive(true);
                return;
            }
        }

        children[0].gameObject.SetActive(true);
    }
}