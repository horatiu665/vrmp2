﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class DancePoint : MonoBehaviour
{
    public enum Types
    {
        NormalDistance,
    }

    public int id = 0;

    [SerializeField]
    private DancePoint _otherPoint;
    public DancePoint otherPoint
    {
        get
        {
            if (_otherPoint == null)
            {
                _otherPoint = FindObjectsOfType<DancePoint>().First(d => d.id == this.id && d != this);
            }
            return _otherPoint;
        }
    }

    public Vector3 deltaMove { get; private set; }
    Vector3 lastPos;

    public AnimationCurve pointsPerMoveDifference = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
    public float moveDifMax = 1f;

    public AnimationCurve pointsPerSpeedMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    public AnimationCurve emitRate = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    public float pointsMulti = 1f;

    [SerializeField]
    private ParticleSystem _ps;
    public ParticleSystem ps
    {
        get
        {
            if (_ps == null)
            {
                _ps = GetComponentInChildren<ParticleSystem>();
            }
            return _ps;
        }
    }


    [DebugButton]
    void RandomizeId()
    {
        id = Random.Range(0, 10000);
    }

    private void Update()
    {
        if (otherPoint != null)
        {
            // calculate points that the duo gets for this pair of dance points
            // by eval. the difference between their delta moves. (similar speed = more points... higher speed = more points ;))
            var moveDif = deltaMove.magnitude - otherPoint.deltaMove.magnitude;
            var ppmd = pointsPerMoveDifference.Evaluate(moveDif / moveDifMax);

            var pps = pointsPerSpeedMultiplier.Evaluate(deltaMove.magnitude);

            var totalPoints = pps * ppmd;

            DanceContest.instance.UpdateTotalScore(totalPoints * pointsMulti);

            var e = ps.emission;
            e.rateOverTime = new ParticleSystem.MinMaxCurve(emitRate.Evaluate(totalPoints));
        }

        deltaMove = transform.position - lastPos;
        lastPos = transform.position;

    }

}