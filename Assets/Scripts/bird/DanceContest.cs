﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using VRCore.VRNetwork;
using Random = UnityEngine.Random;

public class DanceContest : MonoBehaviour
{
    private static DanceContest _instance;
    public static DanceContest instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DanceContest>();
            }
            return _instance;
        }
    }

    public float contestDuration = 30f;
    float curTime;
    bool contestStarted = false;

    public float totalScore { get; private set; }

    public void UpdateTotalScore(float delta)
    {
        if (contestStarted)
        {
            totalScore += delta;
        }
    }

    public Text textScore;
    public Text textTimer;
    public Text vrScore;
    public Text vrTimer;

    public static event System.Action OnStartContest;

    public void StartContest()
    {
        contestStarted = true;
        totalScore = 0;
        HandleScoresFeedback();
        curTime = 0;

        if (OnStartContest != null)
        {
            OnStartContest();
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            StartContest();
        }

        if (contestStarted)
        {
            HandleScoresFeedback();

            curTime += Time.deltaTime;

            if (curTime >= contestDuration)
            {
                Finish();
            }

            //HandleNetScore();
        }
    }

    //private void HandleNetScore()
    //{
    //    if (NetServices.isNetworked && NetServices.isClient)
    //    {
    //        if (contestStarted)
    //        {
    //        }
    //    }
    //}

    private void HandleScoresFeedback()
    {
        vrTimer.text =
        textTimer.text = (contestDuration - curTime).ToString("F0");
        vrScore.text =
        textScore.text = totalScore.ToString("F0");
    }

    private void Finish()
    {
        contestStarted = false;
        // add team to leaderboards....??
        // if winners, create particles and shit
        // if losers, create rain cloud or something
        vrTimer.text =
        textTimer.text = "FINAL SCORE";
    }
}