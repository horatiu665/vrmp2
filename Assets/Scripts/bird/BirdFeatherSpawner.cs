﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class BirdFeatherSpawner : MonoBehaviour
{
    public Transform topRotation;
    public Transform bottomRotation;

    public List<GameObject> feathers = new List<GameObject>();

    public AnimationCurve bendAngle = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
    
    private void Update()
    {
        // calc bend angle for each feather, rotate it in local space.
        //var angleBendEachLeaf = Vector3.Angle(transform.forward, transform.parent.forward) / bendAngleMultiplier;

        for (int i = 0; i < feathers.Count; i++)
        {
            var f = feathers[i];
            var t = i / (float)(feathers.Count);

            f.transform.rotation = Quaternion.Slerp(bottomRotation.rotation, topRotation.rotation, bendAngle.Evaluate(t));

        }
    }
}