﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class DanceScore : MonoBehaviour
{
    public float score { get; private set; }

    private void OnEnable()
    {
        DanceContest.OnStartContest += DanceContest_OnStartContest;
    }

    private void DanceContest_OnStartContest()
    {
        score = 0;
    }

    private void OnDisable()
    {
        DanceContest.OnStartContest -= DanceContest_OnStartContest;
    }

    public void UpdateScore(float delta)
    {
        score += delta;
    }
}