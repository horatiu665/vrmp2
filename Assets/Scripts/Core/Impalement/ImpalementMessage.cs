namespace Core
{
    using UnityEngine;
    using VRCore.VRNetwork;

    public class ImpalementMessage : DataMessage
    {
        public ImpalementMessage(byte[] buffer) : base(buffer)
        {
        }

        public ImpalementMessage() : base(NetMessageType.Impalement)
        {
        }

        public ImpalementType impalementType
        {
            get;
            set;
        }

        public int netId
        {
            get;
            set;
        }

        public ImpalementType otherType
        {
            get;
            set;
        }

        public int otherId
        {
            get;
            set;
        }

        public Vector3 position
        {
            get;
            set;
        }

        public Quaternion rotation
        {
            get;
            set;
        }

        // Local position used so we assume that we can use impreicse vector3
        public Vector3 anchor
        {
            get;
            set;
        }

        public bool hardSound
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return 37; // impalement type (1) + net id (4) + other type (1) + other id (4) + precise position (12) + imprecise rotation (8) + imprecise anchor position (6) + hard sound (1)
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write((byte)this.impalementType);
            s.Write(this.netId);
            s.Write((byte)this.otherType);
            s.Write(this.otherId);
            s.WriteExact(this.position);
            s.Write(this.rotation);
            s.Write(this.anchor);
            s.Write(this.hardSound);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.impalementType = (ImpalementType)s.ReadByte();
            this.netId = s.ReadInt();
            this.otherType = (ImpalementType)s.ReadByte();
            this.otherId = s.ReadInt();
            this.position = s.ReadExactVector3();
            this.rotation = s.ReadQuaternion();
            this.anchor = s.ReadVector3();
            this.hardSound = s.ReadBool();
        }
    }
}