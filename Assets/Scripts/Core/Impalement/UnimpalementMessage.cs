namespace Core
{
    using VRCore.VRNetwork;

    public sealed class UnimpalementMessage : DataMessage
    {
        public UnimpalementMessage(byte[] buffer) : base(buffer)
        {
        }

        public UnimpalementMessage() : base(NetMessageType.Unimpalement)
        {
        }

        public ImpalementType impalementType
        {
            get;
            set;
        }

        public int netId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return 5; // net id (4) + impalement type (1)
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write((byte)this.impalementType);
            s.Write(this.netId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.impalementType = (ImpalementType)s.ReadByte();
            this.netId = s.ReadInt();
        }
    }
}