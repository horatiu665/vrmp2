namespace Core
{
    using UnityEngine;

    public interface ICanImpale
    {
        Transform transform { get; }

        Rigidbody rigidbody { get; }

        Collider[] activeColliders { get; }

        Vector3 prevPos { get; }

        Quaternion prevRot { get; }

        ImpaleParams impaleParameters { get; }
    }
}