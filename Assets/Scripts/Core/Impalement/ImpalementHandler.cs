namespace Core
{
    using Apex.WorldGeometry;
    using Level;
    using UnityEngine;
    using UnityEngine.Networking;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Server;

    public static class ImpalementHandler
    {
        /// <summary>
        /// only called on client
        /// </summary>
        public static void FixClientImpalement(ICanImpale impaler, Rigidbody other, Vector3 position, Quaternion rotation, Vector3 anchor, bool hardSound)
        {
            var ip = impaler.impaleParameters;
            if (ip.impaleJoint != null)
            {
                //Debug.Log(impaler + " removing joint from impaler " + ip.impaleJoint.connectedBody, impaler.transform);
                Object.Destroy(ip.impaleJoint);
            }

            if (other != null)
            {
                impaler.transform.position = other.transform.TransformPoint(position);
                impaler.transform.rotation = other.transform.rotation * rotation;
            }
            else
            {
                impaler.transform.position = position;
                impaler.transform.rotation = rotation;
            }

            ip.impaleJoint = impaler.transform.AddComponent<ConfigurableJoint>();
            InitJoint(ip, impaler.rigidbody, anchor, other);

            // disable scripts
            var count = ip.disabledWhileImpaled.Count;
            for (int i = 0; i < count; i++)
            {
                ip.disabledWhileImpaled[i].enabled = false;
            }

            if (ip.disableCollidersAfterImpale)
            {
                var activeColliders = impaler.activeColliders;
                for (int i = 0; i < activeColliders.Length; i++)
                {
                    activeColliders[i].enabled = false;
                }
            }

            if (ip.disableImpaleCollider)
            {
                if (ip.impaleCollider != null)
                {
                    ip.impaleCollider.enabled = false;
                }
            }

            if (hardSound)
            {
                if (ip.soundImpaleHard != null)
                {
                    ip.soundImpaleHard.Play();
                }
            }
            else
            {
                if (ip.soundImpaleSoft != null)
                {
                    ip.soundImpaleSoft.Play();
                }
            }

            //Debug.Log(impaler + " correct impalement against other == " + other, impaler.transform);
        }

        /// <summary>
        /// Impales, plays appropriate sounds on client/singleplayer and sends message on server.
        /// </summary>
        public static void Impale(ICanImpale impaler, Collider otherCollider, Vector3 relativeVelocity, Vector3 impalePoint)
        {
            var ip = impaler.impaleParameters;
            if (ip.impaleJoint != null)
            {
                return;
            }

            if (!Layers.InLayer(otherCollider.gameObject, ip.impaleOnlyLayers))
            {
                return;
            }

            if (relativeVelocity.sqrMagnitude < ip.minImpaleForce * ip.minImpaleForce)
            {
                return;
            }

            var impalingEquippable = impaler.transform.GetComponent<IVREquippable>();
            if (impalingEquippable != null)
            {
                // An equippable is the impaler
                if (impalingEquippable.isEquipped)
                {
                    if (otherCollider.attachedRigidbody != null)
                    {
                        // the impaling equippable is actually equipped => check if it is equipped by the player who is also the target 
                        var impaledPlayer = otherCollider.attachedRigidbody.GetComponentInParent<IVRPlayer>();
                        if (impaledPlayer != null)
                        {
                            if (impaledPlayer == impalingEquippable.player)
                            {
                                // If the equipped impaling equippable's player is the target player, disallow the impalement
                                return;
                            }
                        }
                    }
                }
            }

            var dummyObjectForSmartRotations = VRHelper.dummy.transform;
            var impaleDir = ip.impaleForward;
            var transform = impaler.transform;
            var rb = impaler.rigidbody;

            transform.position = impaler.prevPos;
            transform.rotation = impaler.prevRot;

            // position transform such that the impale tip is at the impale point. use the global dummy
            var oldPa = transform.parent;
            dummyObjectForSmartRotations.position = impaleDir.position;
            dummyObjectForSmartRotations.rotation = impaleDir.rotation;
            transform.SetParent(dummyObjectForSmartRotations);
            // move arrow by impaleOffset towards -normal direction (into object)
            dummyObjectForSmartRotations.position = impalePoint + impaleDir.forward * ip.impaleOffset;
            transform.SetParent(oldPa);
            dummyObjectForSmartRotations.gameObject.SetActive(false);

            //// if other object is a ball, put the ball at the arrow tip. looks funny in SelfieTennis. no balls in Utopia tho, this needs rethinking.
            //if (otherCollider != null && otherCollider.tag == "Ball")
            //{
            //    if (otherCollider.attachedRigidbody != null)
            //    {
            //        otherCollider.attachedRigidbody.transform.position = ip.arrowTip.position;
            //    }
            //}

            ip.impaleJoint = transform.AddComponent<ConfigurableJoint>();
            InitJoint(ip, rb, rb.transform.InverseTransformPoint(impalePoint), otherCollider.attachedRigidbody);

            // set vels just below break force
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, ip.impaleJoint.breakForce * 0.9f);
            rb.angularVelocity = Vector3.ClampMagnitude(rb.angularVelocity, ip.impaleJoint.breakTorque * 0.9f);

            // add force to other object?
            CoroutineHelper.instance.StartCoroutine(pTween.WaitFixedFrames(1, () =>
            {
                if (otherCollider != null && otherCollider.attachedRigidbody != null)
                {
                    otherCollider.attachedRigidbody.velocity = impaleDir.forward * relativeVelocity.magnitude * ip.impaleForceMultiplierForOtherObj;
                }
            }));

            // disable scripts
            var count = ip.disabledWhileImpaled.Count;
            for (int i = 0; i < count; i++)
            {
                ip.disabledWhileImpaled[i].enabled = false;
            }

            if (ip.disableCollidersAfterImpale)
            {
                var activeColliders = impaler.activeColliders;
                for (int i = 0; i < activeColliders.Length; i++)
                {
                    activeColliders[i].enabled = false;
                }
            }

            if (ip.disableImpaleCollider)
            {
                if (ip.impaleCollider != null)
                {
                    ip.impaleCollider.enabled = false;
                }
            }

            // only play sounds immediately in single player. they are also played in ImpaleCorrect
            if (!NetServices.isNetworked)
            {
                if (relativeVelocity.sqrMagnitude > ip.hardImpaleForce * ip.hardImpaleForce)
                {
                    if (ip.soundImpaleHard != null)
                    {
                        ip.soundImpaleHard.Play();
                    }
                }
                else
                {
                    if (ip.soundImpaleSoft != null)
                    {
                        ip.soundImpaleSoft.Play();
                    }
                }
            }
            else if (NetServices.isServer)
            {
                // send message only on server
                var msg = MessagePool.Get<ImpalementMessage>();

                int id;
                ImpalementType type;
                GetIdAndType(transform, out id, out type);
                msg.netId = id;
                msg.impalementType = type;

                int otherId;
                ImpalementType otherType;
                GetIdAndType(otherCollider.attachedRigidbody != null ? otherCollider.attachedRigidbody.transform : null, out otherId, out otherType);
                msg.otherId = otherId;
                msg.otherType = otherType;

                var orb = otherCollider.attachedRigidbody;
                if (orb == null)
                {
                    msg.position = transform.position;
                    msg.rotation = transform.rotation;
                }
                else
                {
                    msg.position = orb.transform.InverseTransformPoint(transform.position);
                    msg.rotation = Quaternion.Inverse(orb.transform.rotation) * transform.rotation;
                }

                msg.anchor = ip.impaleJoint.anchor;
                msg.hardSound = relativeVelocity.sqrMagnitude > (ip.hardImpaleForce * ip.hardImpaleForce);

                ServerNetSender.instance.SendToAll(msg, QosType.ReliableSequenced);
                MessagePool.Return(msg);

                //Debug.Log(impaler.ToString() + " impale against other == " + otherCollider, impaler.transform);
            }
        }

        internal static void Reimpale(ICanImpale arrow)
        {
            var ip = arrow.impaleParameters;
            if (ip.impaleJoint != null)
            {
                ip.impaleJoint.connectedAnchor = ip.impaleJoint.connectedAnchor;
            }

        }

        private static void GetIdAndType(Transform transform, out int id, out ImpalementType type)
        {
            if (transform != null)
            {
                var otherEquippable = transform.GetComponent<INetEquippable>();
                if (otherEquippable != null)
                {
                    id = otherEquippable.netId;
                    type = ImpalementType.Equippable;
                    return;
                }

                var otherSyncRB = transform.GetComponent<RigidbodySyncComponent>();
                if (otherSyncRB != null)
                {
                    id = otherSyncRB.syncId;
                    type = ImpalementType.SyncedRigidbody;
                    return;
                }

                var otherPlayer = transform.GetComponent<INetPlayer>();
                if (otherPlayer != null)
                {
                    id = otherPlayer.netId;
                    type = ImpalementType.Player;
                    return;
                }
            }

            id = -1;
            type = ImpalementType.Ground;
        }

        private static void InitJoint(ImpaleParams ip, Rigidbody rb, Vector3 anchor, Rigidbody otherBody)
        {
            var j = ip.impaleJoint;
            j.xMotion = ConfigurableJointMotion.Free;
            j.yMotion = ConfigurableJointMotion.Free;
            j.zMotion = ConfigurableJointMotion.Free;

            // can be null, in which case it will be connected to the world (maybe?)
            j.connectedBody = otherBody;
            j.anchor = anchor;
            // this is intentional! trying to fix an elusive bug
            j.connectedAnchor = j.connectedAnchor;

            var pDrive = j.xDrive;
            pDrive.positionSpring = ip.jointPosSpringStrength;
            pDrive.positionDamper = 1;
            j.xDrive = j.yDrive = j.zDrive = pDrive;

            j.rotationDriveMode = RotationDriveMode.Slerp;

            var s = j.slerpDrive;
            s.positionDamper = 1f;
            s.positionSpring = ip.jointSpringStrength;
            s.maximumForce = ip.jointMaxSpring;
            j.slerpDrive = s;

            j.breakForce = ip.breakForce;
            j.breakTorque = ip.breakTorque;

            j.enablePreprocessing = true;
            j.projectionMode = JointProjectionMode.PositionAndRotation;
        }

        public static void Unimpale(ICanImpale impaler, bool noSend = false)
        {
            var ip = impaler.impaleParameters;

            var sendMessage = false;
            if (ip.impaleJoint != null)
            {
                Object.Destroy(ip.impaleJoint);
                sendMessage = true;
            }

            // reenable scripts
            for (int i = 0; i < ip.disabledWhileImpaled.Count; i++)
            {
                ip.disabledWhileImpaled[i].enabled = true;
            }

            if (ip.disableCollidersAfterImpale)
            {
                var activeColliders = impaler.activeColliders;
                for (int i = 0; i < activeColliders.Length; i++)
                {
                    activeColliders[i].enabled = true;
                }
            }

            if (ip.disableImpaleCollider)
            {
                if (ip.impaleCollider != null)
                {
                    ip.impaleCollider.enabled = true;
                }
            }

            var rb = impaler.rigidbody;
            rb.ResetCenterOfMass();
            rb.ResetInertiaTensor();

            if (!noSend && sendMessage && NetServices.isServer)
            {
                var msg = MessagePool.Get<UnimpalementMessage>();

                int id;
                ImpalementType type;
                GetIdAndType(impaler.transform, out id, out type);
                msg.netId = id;
                msg.impalementType = type;

                ServerNetSender.instance.SendToAll(msg, QosType.ReliableSequenced);
                MessagePool.Return(msg);
            }
        }
    }
}