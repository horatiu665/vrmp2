namespace Core
{
    using System.Collections.Generic;
    using UnityEngine;

    [System.Serializable]
    public class ImpaleParams
    {
        /// <summary>
        /// if not null, only impales when this collider is involved in the collision
        /// </summary>
        [Tooltip("if not null, only impales when this collider is involved in the collision")]
        public Collider impaleCollider;

        /// <summary>
        /// If not null, only impales if the relative velocity is in the direction of this transform.forward
        /// </summary>
        [Tooltip("If not null, only impales if the relative velocity is in the direction of this transform.forward")]
        public Transform impaleForward;

        /// <summary>
        /// Moves object by impaleOffset amount in the direction of the arrow tip (or impaleForward if not null), prior to making the joint
        /// </summary>
        [Tooltip("Moves object by impaleOffset amount in the direction of the arrow tip (or impaleForward if not null), prior to making the joint")]
        public float impaleOffset = 0.05f;

        /// <summary>
        /// Hand push is when the hand position is away from the grab point because the rigidbody cannot go through what we are pushing into
        /// </summary>
        [Tooltip("Hand push is when the hand position is away from the grab point because the rigidbody cannot go through what we are pushing into")]
        public float handPushMaxDistance = 0.35f;

        /// <summary>
        /// The ratio of handpush sufficient for un-impaling
        /// </summary>
        [Tooltip("The ratio of handpush sufficient for un-impaling")]
        public float handPushForUnimpale = -0.5f;

        [Header("Conditions")]
        public bool impaleOnlyWhileFlying = false;

        public bool disableCollidersAfterImpale = false;

        public bool disableImpaleCollider = false;

        public float minImpaleForce = 1f;

        // default to everything
        public LayerMask impaleOnlyLayers = -1;

        [Header("Joint")]

        /// <summary>
        /// Position drive spring - a very big value, making it equivalent to locking the positions on all axes, but without the glitching...
        /// </summary>
        [Tooltip("Position drive spring - a very big value, making it equivalent to locking the positions on all axes, but without the glitching...")]
        public float jointPosSpringStrength = 10000000f;
        public float jointSpringStrength = 1000f;
        public float jointMaxSpring = 100000f;

        public float breakForce = float.PositiveInfinity, breakTorque = float.PositiveInfinity;

        [Header("Effects")]
        public float impaleForceMultiplierForOtherObj = 0.2f;

        public List<MonoBehaviour> disabledWhileImpaled = new List<MonoBehaviour>();

        [Space]
        public float hardImpaleForce = 7f;

        public SmartSound soundImpaleSoft, soundImpaleHard;

        [HideInInspector]
        public ConfigurableJoint impaleJoint = null;
    }
}