namespace Core.Score
{
    using Apex;

    public interface IScore : IPooledComponent, IEnumTyped
    {
        ScoreType type { get; set; }

        string scoreText { set; }

        bool paused { get; set; }

        float height { get; }
    }
}