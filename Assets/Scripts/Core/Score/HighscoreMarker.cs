﻿using Core;
using Core.Score;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using VRCore.VRNetwork;
using VRCore.VRNetwork.Client;
using Random = UnityEngine.Random;

public class HighscoreMarker : MonoBehaviour
{
    private static HighscoreMarker _instance;
    public static HighscoreMarker instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<HighscoreMarker>();
            }
            return _instance;
        }
    }

    [Header("Uses prefabType of this object to spawn it")]
    [Header("TODO: POOL THE HIGHSCORE POLES :*")]
    public JumpHighscoreFlag jumpHighscoreFlagPrefab;

    private void OnEnable()
    {
        ScorePointsManager.OnScoreAdded += ScorePointsManager_OnScoreAdded;
    }

    private void OnDisable()
    {
        ScorePointsManager.OnScoreAdded -= ScorePointsManager_OnScoreAdded;
    }

    private void ScorePointsManager_OnScoreAdded(ScorePointsManager.ScoreAddedEventArgs data)
    {
        if (data.scoreType == Core.Score.ScoreType.JumpScore)
        {
            var pos = data.player.position;
            if (data.player.isLocal)
            {
                CreateHighscorePole(pos, data.lastScoreAdded, data.scoreType, data.player, data.isGlobalHighscore);
            }

            // HACK HACK HACK HACK HACK
            if (NetServices.isNetworked)
            {
                // only local client should send the highscore pole message to others... dunno yet about other scores ;)
                if (NetServices.isClient && data.player.isLocal)
                {
                    var msg = MessagePool.Get<HighscorePoleMessage>();
                    msg.netId = data.player.netId;
                    msg.position = pos;
                    msg.scoreType = data.scoreType;
                    msg.score = data.lastScoreAdded;

                    ClientNetSender.instance.Send(msg, UnityEngine.Networking.QosType.Reliable);

                    MessagePool.Return(msg);

                }
            }

        }
    }

    public void CreateHighscorePole(Vector3 pos, float score, ScoreType scoreType, IPlayer player, bool isGlobalHighscore)
    {
        // create pole
        // we don't want this because it can be on a slope in the air!
        //pos.y = MountainHeightProviderSingleton.SampleHeight(pos);
        var pole = VRPrefabManager.instance.Spawn<JumpHighscoreFlag>(jumpHighscoreFlagPrefab.prefabType, pos, Quaternion.identity);
        var scoreText = Mathf.Round(score) + "m";
        pole.SetHighscoreFirstTime(scoreText, scoreType, score, player, isGlobalHighscore);
        pole.transform.SetParent(transform);
    }
}