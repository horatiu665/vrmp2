namespace Core.Score
{
    using System.Collections.Generic;

    public sealed class ScoreTypeComparer : IEqualityComparer<ScoreType>
    {
        public bool Equals(ScoreType x, ScoreType y)
        {
            return x == y;
        }

        public int GetHashCode(ScoreType obj)
        {
            return (int)obj;
        }
    }
}