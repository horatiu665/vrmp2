namespace Core.Score
{
    using Apex.WorldGeometry;

    public interface IScoreAwarder : IPositioned
    {
        ScoreType type { get; }

        ScoreMode mode { get; }

        int scoreToAward { get; }
        
    }
}