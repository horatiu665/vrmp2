namespace Core.Score
{
    using System;
    using Apex;
    using UnityEngine;
    using VRCore;

    /// <summary> 
    /// component on the score object in the sky
    /// </summary>
	public sealed class ScoreComponent : PooledComponentBase, IScore
    {
        IPlayer _player;
        IPlayer player
        {
            get
            {
                if (_player == null)
                {
                    _player = PlayerManager.GetLocalPlayer<IPlayer>();
                }
                return _player;
            }
        }

        [Header("Settings")]
        [SerializeField]
        private float _moveUpSpeed = 100f;

        [SerializeField]
        private float _movingDuration = 3f;

        [SerializeField]
        private float _scalingDuration = 1.5f;

        [Header("References")]
        [SerializeField]
        private TextMesh _textMesh;

        [Header("Manual setup")]
        [SerializeField]
        private float _visibleHeight = 70;
        public float height
        {
            get
            {
                return _visibleHeight;
            }
        }

        [Header("Debugging")]
        [SerializeField, ReadOnly]
        private float _enabledTime;

        [SerializeField, ReadOnly]
        private bool _paused;

        private ScoreType _type;
        private Vector3 _startScale;

        public ScoreType type
        {
            get { return _type; }
            set { _type = value; }
        }

        public Enum entityIdentifier
        {
            get { return _type; }
        }

        public string scoreText
        {
            set
            {
                _textMesh.text = value;
            }
        }

        public bool paused
        {
            get
            {
                return _paused;
            }

            set
            {
                _paused = value;

                if (!value)
                {
                    // when unpausing, save the time it happened so that moving and scaling happens after that
                    _enabledTime = Time.timeSinceLevelLoad;
                }
            }
        }

        private void OnEnable()
        {
            if (_textMesh == null)
            {
                _textMesh = this.GetComponentInChildren<TextMesh>();
                if (_textMesh == null)
                {
                    Debug.LogError(this.ToString() + " could not find a TextMesh!");
                }
            }

            _startScale = this.transform.localScale;
            _enabledTime = Time.timeSinceLevelLoad;
        }

        private void OnDisable()
        {
            this.transform.localScale = _startScale;
        }

        private void Update()
        {
            if (player != null)
            {
                // always turn to face the camera "billboard-style"
                this.transform.LookAt(player.head);
            }

            if (_paused)
            {
                return;
            }

            this.transform.position += Vector3.up * _moveUpSpeed * Time.deltaTime;

            var time = Time.timeSinceLevelLoad;
            var timeElapsed = time - _enabledTime;
            // if time btw moving and moving + scaling
            if (timeElapsed <= _movingDuration + _scalingDuration && timeElapsed > _movingDuration)
            {
                // scale down after the moving duration, during the scaling duration
                var scale = Mathf.SmoothStep(1f, 0f, Mathf.InverseLerp(_movingDuration, _movingDuration + _scalingDuration, timeElapsed));
                this.transform.localScale = _startScale * scale;
            }
            else if (timeElapsed > _movingDuration + _scalingDuration)
            {
                // return this score to the pool after moving and scaling
                ScoreManager.instance.Return(this);
            }
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(transform.position + Vector3.up * height / 2, height * Vector3.down);

        }
    }
}