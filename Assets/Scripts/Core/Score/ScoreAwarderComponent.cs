namespace Core.Score
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Apex.WorldGeometry;
    using Level;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Score")]
    public sealed class ScoreAwarderComponent : MonoBehaviour, IScoreAwarder
    {
        // keeps track of players that got this score awarder. to prevent duplicate score from same obj
        private HashSet<IPlayer> _players;
        public HashSet<IPlayer> players
        {
            get
            {
                if (_players == null)
                {
                    _players = new HashSet<IPlayer>();
                }
                return _players;
            }
        }

        [SerializeField]
        private ScoreType _type = ScoreType.None;

        [SerializeField]
        private ScoreMode _mode = ScoreMode.DisableScoreAwarder;

        [SerializeField, Range(0, 30f), Tooltip("The minimum amount of seconds which must pass before this score awarder component awards any scores again.")]
        private float _minCollisionDelay = 5f;

        [SerializeField]
        private int _scoreToAward = 25;

        [SerializeField]
        private float _respawnTime = 15f;

        [SerializeField, ReadOnly(runtimeOnly = true)]
        private float _fromBehindAngle = 89f;

        [SerializeField]
        private bool _onlyFromBehind = false;

        [SerializeField]
        private AudioSource _audio;

        private float _nextAllowedCollision;
        private float _collisionAngle;

        public ScoreType type
        {
            get { return _type; }
        }

        public ScoreMode mode
        {
            get { return _mode; }
        }

        public int scoreToAward
        {
            get { return _scoreToAward; }
        }

        public Vector3 position
        {
            get { return this.transform.position; }
            set { this.transform.position = value; }
        }

        private void OnValidate()
        {
            _audio = this.GetOrAddComponent<AudioSource>();
        }

        private void OnEnable()
        {
            _collisionAngle = Mathf.Cos(((360f - _fromBehindAngle) * 0.5f) * Mathf.Deg2Rad);
        }

        private void OnCollisionEnter(Collision collision)
        {
            OnTriggerEnter(collision.collider);
        }

        private void OnTriggerEnter(Collider collider)
        {
            // Triggers get called on disabled components, so we manually check here...
            if (!this.enabled)
            {
                return;
            }
            
            // check for the right type of gameobject (non-player, layer)
            Rigidbody rb = null;
            var go = collider.gameObject;
            if (collider.attachedRigidbody != null)
            {
                rb = collider.attachedRigidbody;
                go = rb.gameObject;
            }

            if (ReferenceEquals(this.gameObject, go) || !Layers.InLayer(go, Layers.players))
            {
                // not a player or AI skier - or collided with own game object
                return;
            }

            var skier = rb.GetPlayer<IPlayer>();
            if (skier == null)
            {
                Debug.LogWarning(this.ToString() + " collided with " + go.ToString() + ", but found no SkIVRPlayer on the game object");
                return;
            }

            // check for duplicate score award
            if (_mode == ScoreMode.OncePerPlayer)
            {
                if (players.Contains(skier))
                {
                    return;
                }

                players.Add(skier);
            }

            // check for delay
            var time = Time.time;
            if (time < _nextAllowedCollision)
            {
                return;
            }

            // check for hit direction (for gates and such)
            if (_onlyFromBehind)
            {
                var intersectPoint = this.position + ((this.position - skier.position) * 0.5f);
                var localIntersection = this.transform.InverseTransformPoint(intersectPoint);
                var dot = Vector3.Dot(skier.transform.forward, this.transform.forward);
                if (dot < _collisionAngle || localIntersection.z < 0f)
                {
                    return;
                }
            }

            // check for game mode
            if (!GameModeManager.instance.IsScoreAllowed(_type))
            {
                // This game mode does not allow this type of score
                return;
            }

            _nextAllowedCollision = Time.time + _minCollisionDelay;

            // add the score value to the points manager
            ScorePointsManager.instance.AddScore(skier, _type, _scoreToAward);
            //skier.AddScore(_type, _scoreToAward);

            _audio.Play();

            //skier.RegisterScoreAwarder(this);

            // Instantiate visual score effects for players, not for AI
            var score = ScoreManager.instance.Get(_type, _scoreToAward.ToString(), skier, this.position);
            
            switch (_mode)
            {
            case ScoreMode.None:
            case ScoreMode.OncePerPlayer:
                {
                    break;
                }

            case ScoreMode.DisableScoreAwarder:
                {
                    this.enabled = false;
                    break;
                }

            case ScoreMode.Deactivate:
                {
                    this.gameObject.SetActive(false);
                    break;
                }

            case ScoreMode.Delete:
                {
                    this.enabled = false;
                    Destroy(this.gameObject, 0.1f);
                    break;
                }

            case ScoreMode.Respawn:
                {
                    CoroutineHelper.instance.StartCoroutine(DelayedActivate());
                    this.gameObject.SetActive(false);
                    break;
                }
                
            default:
                {
                    Debug.LogWarning(this.ToString() + " unsupported ScoreMode used == " + _mode.ToString());
                    break;
                }
            }
        }
        
        private IEnumerator DelayedActivate()
        {
            yield return new WaitForSeconds(_respawnTime);
            this.gameObject.SetActive(true);
        }
    }
}