namespace Core.Score
{
    [Apex.PooledTypeIdentifier]
    public enum ScoreType
    {
        None = 0,
        HitTree = 1,
        JumpScore = 6,
        SlalomGatesCompleted = 7,
        SlalomCompletionTime = 10,
        SlalomGate = 11,
        SlalomLevelStart = 12,
    }

}