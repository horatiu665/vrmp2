﻿using UnityEngine;
using System.Collections;
using Core;
using Core.Score;
using VRCore;

public class ScoreTextSimpleScoreType : ScoreTextBase
{
    public ScoreType scoreType;

    public enum ShowScoreMethods
    {
        Total,
        Last
    }

    public ShowScoreMethods showScoreMethod = ShowScoreMethods.Total;

    public bool onlyLocalPlayer = false;

    private void OnEnable()
    {
        ScorePointsManager.OnScoreAdded += ScorePointsManager_OnScoreAdded;
    }

    private void OnDisable()
    {
        ScorePointsManager.OnScoreAdded -= ScorePointsManager_OnScoreAdded;
    }

    private void ScorePointsManager_OnScoreAdded(ScorePointsManager.ScoreAddedEventArgs data)
    {
        if (!onlyLocalPlayer || (onlyLocalPlayer && data.player.isLocal))
        {
            if (scoreType == data.scoreType)
            {
                if (showScoreMethod == ShowScoreMethods.Total)
                {
                    SetScoreValue(Mathf.RoundToInt(data.scoreTypeData.totalScore));
                }
                else if (showScoreMethod == ShowScoreMethods.Last)
                {
                    SetScoreValue(Mathf.RoundToInt(data.lastScoreAdded));
                }
            }
        }
    }

}
