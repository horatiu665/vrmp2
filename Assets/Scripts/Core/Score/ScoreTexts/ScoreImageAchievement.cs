﻿using Core.Score;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class ScoreImageAchievement : MonoBehaviour
{
    private Image _image;
    public Image image
    {
        get
        {
            if (_image == null)
            {
                _image = GetComponent<Image>();
            }
            return _image;
        }
    }

    public bool disabledOnStart = true;

    public ScoreType scoreType;

    public void SetScoreAchieved(bool achieved)
    {
        image.enabled = achieved;
    }

    private void OnEnable()
    {
        ScorePointsManager.OnScoreAdded += ScorePointsManager_OnScoreAdded;
        if (disabledOnStart)
        {
            image.enabled = false;
        }
    }

    private void OnDisable()
    {
        ScorePointsManager.OnScoreAdded -= ScorePointsManager_OnScoreAdded;
    }

    private void ScorePointsManager_OnScoreAdded(ScorePointsManager.ScoreAddedEventArgs data)
    {
        if (scoreType == data.scoreType && data.player.isLocal)
        {
            SetScoreAchieved(true);
        }
    }
}
