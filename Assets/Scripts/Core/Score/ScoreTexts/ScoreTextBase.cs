﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using VRCore;
using Random = UnityEngine.Random;

public class ScoreTextBase : MonoBehaviour
{
    private Text _text;
    public Text text
    {
        get
        {
            if (_text == null)
            {
                _text = GetComponent<Text>();
            }
            return _text;
        }
    }

    public virtual void SetScoreValue(string scoreText)
    {
        text.text = scoreText;
    }

    public virtual void SetScoreValue(int score)
    {
        text.text = score.ToString();
    }

}