﻿using UnityEngine;
using System.Collections;
using Core;
using Core.Score;
using VRCore;
using System;

public class ScoreTextSlalomTime : ScoreTextBase
{
    private void OnEnable()
    {
        ScorePointsManager.OnScoreAdded += ScorePointsManager_OnScoreAdded;
    }

    private void OnDisable()
    {
        ScorePointsManager.OnScoreAdded -= ScorePointsManager_OnScoreAdded;
    }

    private void ScorePointsManager_OnScoreAdded(ScorePointsManager.ScoreAddedEventArgs data)
    {
        if (data.player.isLocal)
        {
            if (data.scoreType == ScoreType.SlalomCompletionTime)
            {
                float completionTimeSeconds = data.lastScoreAdded;
                string s = SlalomManager.GenerateTimeString(completionTimeSeconds);
                SetScoreValue(s);
            }
        }
    }

}
