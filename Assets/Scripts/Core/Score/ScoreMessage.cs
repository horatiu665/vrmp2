namespace Core.Score
{
    public class ScoreMessage
    {
        public ScoreType scoreType = ScoreType.None;
        public IPlayer player = null;
        public int amount = 0;

        public ScoreMessage()
        {
        }
    }
}