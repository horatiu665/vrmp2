namespace Core.Score
{
    using System;
    using UnityEngine;

    [Serializable]
    public sealed class ScoreSetup
    {
        public ScoreType type;
        public GameObject prefab;
        
        public int preallocation = 2;
    }
}