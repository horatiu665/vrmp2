﻿namespace Core
{
    using Core.Score;
    using UnityEngine;
    using VRCore.VRNetwork;

    /// <summary>
    /// Highscore pole from which player (server to client, otherwise we have connectionId). data about score type and score value, and position of the pole.
    /// </summary>
    public class HighscorePoleMessage : NetIdMessageBase
    {
        public HighscorePoleMessage(byte[] buffer) : base(buffer)
        {
        }

        public HighscorePoleMessage() : base(NetMessageType.HighscorePole)
        {
        }

        public Vector3 position // 12 low precision
        {
            get;
            set;
        }

        public ScoreType scoreType // 1
        {
            get;
            set;
        }

        public float score // 4
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 17; // base + position 12 + scoretype 1 + score 4
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.WriteExact(position);
            s.Write((byte)scoreType);
            s.Write(score);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.position = s.ReadExactVector3();
            this.scoreType = (ScoreType)s.ReadByte();
            this.score = s.ReadFloat();
        }
    }
}