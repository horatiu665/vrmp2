﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using UnityEditor;

[CustomEditor(typeof(ScorePointsManager))]
public class ScorePointsManagerEditor : Editor
{
    private ScorePointsManager _spMan;
    public ScorePointsManager spMan
    {
        get
        {
            if (_spMan == null)
            {
                _spMan = target as ScorePointsManager;
            }
            return _spMan;
        }
    }

    List<bool> foldouts = new List<bool>();
    int foldCount = 0;

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        foldCount = 0;

        // dictionary. points for each player
        var ps = spMan.editorPlayerScores;

        EditorGUILayout.LabelField("Player scores", EditorStyles.boldLabel);

        EditorGUI.indentLevel++;
        if (!Application.isPlaying)
        {
            EditorGUILayout.LabelField("only in play mode");
        }
        else if (ps.Count == 0)
        {
            EditorGUILayout.LabelField("none");

        }

        foreach (var p in ps)
        {
            var playerName = p.Key.name;
            if (Fold(playerName + " scores"))
            {
                EditorGUI.indentLevel++;

                foreach (var k in p.Value)
                {
                    if (Fold(k.Key.ToString() + " (total = " + k.Value.totalScore + ")"))
                    {
                        EditorGUI.indentLevel++;
                        var size = EditorGUILayout.IntField("Size", k.Value.scoreList.Count);
                        if (size != k.Value.scoreList.Count)
                        {
                            k.Value.scoreList.RemoveRange(size, k.Value.scoreList.Count - size);
                        }

                        for (int i = 0; i < k.Value.scoreList.Count; i++)
                        {
                            EditorGUILayout.FloatField("Element " + i, k.Value.scoreList[i]);
                        }

                        EditorGUI.indentLevel--;
                    }
                }

                EditorGUI.indentLevel--;
            }

        }

        EditorGUI.indentLevel--;


    }


    bool Fold(string label)
    {
        if (foldouts.Count <= this.foldCount)
        {
            foldouts.Add(false);
        }
        foldouts[foldCount] = EditorGUILayout.Foldout(foldouts[foldCount], label);
        return foldouts[foldCount++];
    }
}