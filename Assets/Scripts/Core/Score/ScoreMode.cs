namespace Core.Score
{
    public enum ScoreMode
    {
        /// <summary>
        /// Just award a score and do nothing else
        /// </summary>
        None = 0,

        /// <summary>
        /// Records the player and ensures that the player can only score once per score awarder component.
        /// </summary>
        OncePerPlayer = 1,

        /// <summary>
        /// Disable this score awarder component, disabling further scores from being awarded for this game object.
        /// </summary>
        DisableScoreAwarder = 2,

        /// <summary>
        /// Deactivate this score awarder game object after giving the first score.
        /// </summary>
        Deactivate = 3,

        /// <summary>
        /// Destroy this score awarder game object after giving the first score.
        /// </summary>
        Delete = 4,

        /// <summary>
        /// Deactivate and then reactivate this score awarder game object after a set amount of time.
        /// </summary>
        Respawn = 5,
    }
}