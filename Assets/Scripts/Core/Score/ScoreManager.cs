namespace Core.Score
{
    using Apex;
    using UnityEngine;
    using VRCore;

    // manages the flying score objects
    public sealed class ScoreManager : SingletonMonoBehaviour<ScoreManager>
    {
        [SerializeField]
        private float _backProjection = 5f;

        [SerializeField]
        private float _forwardProjection = 1000f;

        [SerializeField]
        private float _yOffset = 0f;

        [SerializeField]
        private bool showDebug = false;

        public IScore Get(ScoreType type, string scoreText, IPlayer player, Vector3 triggerPosition, IScore underScore = null)
        {
            Vector3 pos;
            if (underScore != null)
            {
                pos = underScore.transform.position + Vector3.down * underScore.height;
            }
            else
            {
                pos = GetProjectedPosition(player, triggerPosition);
            }

            // get the score element and set its score
            var scoreUI = PoolManager.Get<IScore>(type, pos, Quaternion.identity);
            scoreUI.scoreText = scoreText;
            scoreUI.type = type;
            scoreUI.transform.SetParent(player.transform, true);

            return scoreUI;
        }

        /// <summary>
        /// find the right position to spawn a flying score at, depending on the player and the trigger position
        /// </summary>
        /// <param name="player"></param>
        /// <param name="triggerPosition"></param>
        /// <returns></returns>
        private Vector3 GetProjectedPosition(IPlayer player, Vector3 triggerPosition)
        {
            var playerPos = player.transform.position;
            var posProjected = playerPos - (player.skis.forward * _backProjection);
            var pos = playerPos + ((triggerPosition - posProjected).normalized * _forwardProjection);
            pos.y = posProjected.y + _yOffset;

            if (showDebug)
            {
                Debug.DrawRay(playerPos, Vector3.up, Color.red, 3f);
                Debug.DrawRay(posProjected, Vector3.up, Color.red + Color.green * 0.5f, 3f);
                Debug.DrawLine(playerPos, pos, Color.yellow, 3f);
                Debug.DrawLine(playerPos, posProjected, Color.yellow, 3f);
            }

            return pos;
        }

        public void Return(IScore score)
        {
            score.transform.SetParent(this.transform, true);
            score.Recycle(score.entityIdentifier);
        }

    }

}
