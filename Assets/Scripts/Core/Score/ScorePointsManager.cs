﻿using UnityEngine;
using System.Collections;
using VRCore;
using Core;
using System;
using System.Collections.Generic;
using Core.Score;
using VRCore.VRNetwork;
using Apex.Services;
using System.Linq;

public sealed class ScorePointsManager : SingletonMonoBehaviour<ScorePointsManager>
{
    // this only exists for tablet.
    private ScoreMessage _msg;

    private Dictionary<IPlayer, Dictionary<ScoreType, ScoreTypeData>> playerScores = new Dictionary<IPlayer, Dictionary<ScoreType, ScoreTypeData>>();
    public Dictionary<IPlayer, Dictionary<ScoreType, ScoreTypeData>> editorPlayerScores
    {
        get
        {
            return playerScores;
        }
    }

    public Dictionary<ScoreType, GlobalHighscoreData> maxPlayerScores = new Dictionary<ScoreType, GlobalHighscoreData>();

    public delegate void ScoreAddedDelegate(ScoreAddedEventArgs data);

    /// <summary>
    /// Fired whenever a score is added for ANY player. Usually local machine or server will add scores, but network will tell clients about other player's scores.
    /// </summary>
    public static event ScoreAddedDelegate OnScoreAdded;

    public class ScoreAddedEventArgs
    {
        public IPlayer player;
        public ScoreType scoreType;
        public float lastScoreAdded;
        public ScoreTypeData scoreTypeData;
        public bool isGlobalHighscore;

    }

    protected override void Awake()
    {
        base.Awake();

        _msg = new ScoreMessage();

    }

    public ScoreAddedEventArgs AddScore(IPlayer player, ScoreType scoreType, float score)
    {
        if (!playerScores.ContainsKey(player))
        {
            playerScores[player] = new Dictionary<ScoreType, ScoreTypeData>();
        }

        if (!playerScores[player].ContainsKey(scoreType))
        {
            playerScores[player][scoreType] = new ScoreTypeData();
        }

        var std = playerScores[player][scoreType];
        std.scoreList.Add(score);
        if (std.maxScore < score)
        {
            std.maxScore = score;
        }
        std.totalScore += score;

        // ============== FIND MAX SCORE OF ALL PLAYERS ==============
        // max of all scoreList max values for all players in their scoreType lists.
        //var maxScoreOfAllPlayersForThisScoreType = playerScores.Values.Max(playerScoreDict => playerScoreDict[scoreType].scoreList.Max());
        float maxScoreOfAllPlayersForThisScoreType = -1;
        IPlayer whoIsBest = null;
        foreach (var playerData in playerScores)
        {
            if (playerData.Value != null)
            {
                // find max score. not necessary to create new score data struct if nonexistent yet.
                if (playerData.Value.ContainsKey(scoreType))
                {
                    var playerScoreTypeData = playerData.Value[scoreType];
                    var maxPlayerScore = playerScoreTypeData.maxScore;
                    if (maxPlayerScore > maxScoreOfAllPlayersForThisScoreType)
                    {
                        maxScoreOfAllPlayersForThisScoreType = maxPlayerScore;
                        whoIsBest = playerData.Key;
                    }
                }

            }
        }

        // if we have a winner
        // THIS IS BROKEN BECAUSE A PLAYER MIGHT DROP OUT AND STILL BE IN THE LEAD :(
        if (whoIsBest != null)
        {
            // whoIsBest has the top score, and that score is maxScoreOfAllPlayersForThisScoreType
            if (!maxPlayerScores.ContainsKey(scoreType))
            {
                maxPlayerScores[scoreType] = new GlobalHighscoreData();
            }
            maxPlayerScores[scoreType].maxScore = maxScoreOfAllPlayersForThisScoreType;
            maxPlayerScores[scoreType].player = whoIsBest;
        }

        // ============== END FIND MAX SCORE OF ALL PLAYERS ==============

        var eventArgs = new ScoreAddedEventArgs()
        {
            player = player,
            scoreType = scoreType,
            lastScoreAdded = score,
            scoreTypeData = std,
            isGlobalHighscore = (score == maxScoreOfAllPlayersForThisScoreType),

        };

        if (OnScoreAdded != null)
        {
            // perf? should we cache the eventargs? maybe negligible?
            OnScoreAdded(eventArgs);
        }

        return eventArgs;
    }

    // only keeping this while tablet exists.
    public IList<float> GetPlayerScore(IPlayer player, ScoreType scoreType)
    {
        if (!playerScores.ContainsKey(player))
        {
            playerScores[player] = new Dictionary<ScoreType, ScoreTypeData>();
        }

        if (!playerScores[player].ContainsKey(scoreType))
        {
            playerScores[player][scoreType] = new ScoreTypeData();
        }

        return playerScores[player][scoreType].scoreList;
    }

    // only keeping this while tablet exists.
    public float GetPlayerTotalScore(IPlayer player)
    {
        if (!playerScores.ContainsKey(player))
        {
            playerScores[player] = new Dictionary<ScoreType, ScoreTypeData>();
        }

        return playerScores[player].Sum(s => s.Value.totalScore);
    }

    private void SaveToFile()
    {
        // LOLOLOLOLOL UNIMPLEMENTED
    }

    private void LoadFromFile()
    {
        // the opposite of SaveToFile()
        // UNIMPLEMENTED LOLOLOLOLOL
    }

    public class ScoreTypeData
    {
        public List<float> scoreList = new List<float>();
        public float totalScore = 0;
        public float maxScore = float.MinValue;
    }

    public class GlobalHighscoreData
    {
        public float maxScore;
        public IPlayer player;
    }
}
