namespace Core
{
    using UnityEngine;
    using UnityEngine.UI;
    using VRCore;

    public sealed class OnScreenTextUIHandler : SingletonMonoBehaviour<OnScreenTextUIHandler>
    {
        [SerializeField]
        private Text _textField = null;

        [SerializeField]
        private Color _defaultTextColor = Color.white;

        [Header("Fading")]
        [SerializeField]
        private bool _fadeOut = true;

        [SerializeField]
        private float _fadeTime = 8f;

        [Header("Start")]
        [SerializeField]
        private string _startText = "LOADING...";

        public bool fadeOut
        {
            get { return _fadeOut; }
            set { _fadeOut = value; }
        }

        public float fadeTime
        {
            get { return _fadeTime; }
            set { _fadeTime = value; }
        }

        protected override void Awake()
        {
            base.Awake();

            _textField = _textField ?? this.GetComponentInChildren<Text>(true);
            _textField.gameObject.SetActive(false);
        }

        private void Start()
        {
            if (!string.IsNullOrEmpty(_startText))
            {
                ShowText(_startText);
            }
        }

        public void ShowText(string text)
        {
            ShowText(text, _defaultTextColor);
        }

        public void ShowText(string text, Color color)
        {
            if (string.IsNullOrEmpty(text))
            {
                return;
            }

            if (!_textField.IsActive())
            {
                _textField.gameObject.SetActive(true);
            }

            _textField.color = color.ChangeAlpha(1f);
            _textField.text += "\n" + text;

            if (_fadeOut)
            {
                _textField.CrossFadeAlpha(0f, _fadeTime, true);
            }

            StartCoroutine(pTween.Wait(_fadeTime, () =>
            {
                _textField.text = _textField.text.Substring(Mathf.Max(0, _textField.text.IndexOf("\n")));
            }));
        }
    }
}