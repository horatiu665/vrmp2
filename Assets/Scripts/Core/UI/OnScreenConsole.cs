﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Linq;

public class OnScreenConsole : MonoBehaviour
{
    public static OnScreenConsole instance;

    private List<LogMessage> messages = new List<LogMessage>();

    public float logMessageDuration = 5;

    private Text text;
    private ControllerWrapper[] controllers;

    void Start()
    {
        text = GetComponent<Text>();
        instance = this;
        controllers = FindObjectsOfType<ControllerWrapper>();
    }

    public static void Log(string message)
    {
        if (instance != null) {
            instance.messages.Add(new LogMessage(message, instance.logMessageDuration));
        }
    }

    public ControllerWrapper.ButtonMask buttonToHoldConsole = ControllerWrapper.ButtonMask.Touchpad;

    void Update()
    {
        if (controllers.Any(c => c.GetPress(buttonToHoldConsole)))
            return;

        List<LogMessage> toDelete = new List<LogMessage>();
        foreach (var m in messages) {
            if (m.realTimeToDelete < Time.realtimeSinceStartup) {
                toDelete.Add(m);
            }
        }
        for (int i = 0; i < toDelete.Count; i++) {
            messages.Remove(toDelete[i]);
        }

        // after removing stuff, write it to the text
        string onScreen = "";
        foreach (var m in messages) {
            onScreen += m.message + "\n";
        }
        text.text = onScreen;
    }

    class LogMessage
    {
        public string message;
        public float realTimeToDelete;
        public LogMessage(string message, float duration)
        {
            this.message = message;
            this.realTimeToDelete = Time.realtimeSinceStartup + duration;
        }
    }
}
