using System.Linq;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    public static UIManager inst;

    [SerializeField]
    private float _minDistance = 3f;

    private UIMovement[] _uiMovements = new UIMovement[0];
    private Vector3 _oldInput;

    //private float allLastInterfaceVisibilityTime
    //{
    //    set
    //    {
    //        for (int i = 0; i < _uiMovements.Length; i++)
    //        {
    //            _uiMovements[i].lastInterfaceVisibilityTime = value;
    //        }
    //    }
    //}

    private void Awake()
    {
        inst = this;
        _uiMovements = GetComponentsInChildren<UIMovement>(true);
    }

    private void Start()
    {
        _oldInput = Input.mousePosition;

        if (Application.isEditor)
            Cursor.visible = true;
        else
            Cursor.visible = false;
    }

    private void Update()
    {
        bool mouseMoved = false;
        var delta = Input.mousePosition - _oldInput;
        if (delta.sqrMagnitude < _minDistance * _minDistance)
        {
            // distance smaller than minimum = start counting down
        }
        else
        {
            // distance larger than minimum => mouse moved a lot. time to make interface appear.
            _oldInput = Input.mousePosition;

            mouseMoved = true;

            Cursor.visible = true;
        }

        //bool globalAnyInputFields = false;

        // mouse position. Reverse y, so 0 is at top and 100% is at bottom.
        var mousePos = Input.mousePosition;
        mousePos.y = Screen.height - mousePos.y;

        for (int i = 0; i < _uiMovements.Length; i++)
        {
            var ui = _uiMovements[i];

            if (mouseMoved && ui.visibleOnMouseMove)
            {
                ui.lastInterfaceVisibilityTime = Time.time;
            }

            // set visibility for all when an input field is active
            bool anyFocusedFields = ui.inputFieldsToKeepInterfaceOn.Any(infield => infield.isFocused);
            if (anyFocusedFields)
            {
                ui.lastInterfaceVisibilityTime = Time.time;
                //globalAnyInputFields = true;
            }

            // if mouse position is within an interface's rectangle, set that one to be visible.
            var re = ui.keepOnRect;
            if (mousePos.x > re.x * Screen.width
                && mousePos.x <= (re.x + re.width) * Screen.width
                && mousePos.y > (re.y) * Screen.height
                && mousePos.y < (re.y + re.height) * Screen.height)
            {
                ui.lastInterfaceVisibilityTime = Time.time;
            }

            // set visibility based on the last interface visibility time.
            if (Time.time - ui.lastInterfaceVisibilityTime < ui.maxTimeForInterfaceVisible)
            {
                if (!ui.interfaceVisible)
                {
                    ui.SetInterfaceVisibility(true);
                }
            }
            else if (Time.time > ui.firstVisibilityDuration)
            {
                if (ui.interfaceVisible)
                {
                    ui.SetInterfaceVisibility(false);
                }
            }
        }

        //HandleCheats(globalAnyInputFields);
    }

    //private void HandleCheats(bool anyFocusedInputFields)
    //{
    //    bool cheatsOn = !anyFocusedInputFields;
    //    //GlobalCheatManager.inst.SetCheats(cheatsOn);
    //}
}