using System.Collections;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// Used to move interfaces in and out of the main screen.
/// For example, the tweet overlay, or the local highscore leaderboard edit menu.
/// </summary>
public class UIMovement : MonoBehaviour
{
    [SerializeField]
    private Transform _interfaceTransform = null, _finalIT = null, _initIT = null;

    [SerializeField]
    private float _animDuration = 0.5f;

    [SerializeField]
    private GameObject[] _onlyShowWhenVisible;

    /// <summary>
    /// keeps interface on while typing. also deactivates cheats.
    /// </summary>
    [SerializeField]
    private InputField[] _inputFieldsToKeepInterfaceOn = new InputField[0];

    // default, bottom left corner.
    public Rect keepOnRect = new Rect(0, 0.55f, 0.68f, 0.45f);

    public float firstVisibilityDuration = 3f;

    public float maxTimeForInterfaceVisible = 1f;

    public bool visibleOnMouseMove = true;

    [SerializeField]
    private bool visibleOnStart = true;

    [SerializeField]
    private bool locked = false;

    [HideInInspector]
    internal float lastInterfaceVisibilityTime = float.MinValue;

    public InputField[] inputFieldsToKeepInterfaceOn
    {
        get { return _inputFieldsToKeepInterfaceOn; }
    }

    public bool interfaceVisible
    {
        get;
        private set;
    }

    private void Awake()
    {
        _inputFieldsToKeepInterfaceOn = GetComponentsInChildren<InputField>(true);
    }

    private void Start()
    {
        interfaceVisible = visibleOnStart;
        if (visibleOnStart)
        {
            lastInterfaceVisibilityTime = Time.time;
        }

        StartCoroutine(InterfaceVisibilityCor(interfaceVisible, 0f));
    }

    public void SetInterfaceVisibility(bool interfaceVisible)
    {
        if (!locked)
        {
            this.interfaceVisible = interfaceVisible;

            StopAllCoroutines();
            if (isActiveAndEnabled)
            {
                StartCoroutine(InterfaceVisibilityCor(interfaceVisible));
            }
        }
    }

    private IEnumerator InterfaceVisibilityCor(bool interfaceVisible, float animDuration = -1)
    {
        if (animDuration == -1)
        {
            animDuration = this._animDuration;
        }

        foreach (var o in _onlyShowWhenVisible)
        {
            o.SetActive(interfaceVisible);
        }

        var startPos = _interfaceTransform.position;
        yield return StartCoroutine(pTween.To(animDuration, 0, 1, t =>
        {
            _interfaceTransform.position = Vector3.Lerp(startPos,
                (interfaceVisible ? _finalIT : _initIT).position, Mathf.SmoothStep(0, 1, t));
        }));
    }
}