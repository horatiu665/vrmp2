using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using VRCore.VRNetwork;
using Random = UnityEngine.Random;
using VRCore.VRNetwork.Server;

public class CoalEquippable : NetEquippableSpawnableBase, IHaveThrowMultipliers
{
    [Header("Exploding coals")]
    [SerializeField]
    [Range(0, 1f)]
    private float _explosionChanceOnFirstThrowCollision = 0.1f;

    [SerializeField]
    private float _minForceForFirstCollisionExplosion = 1f;


    [SerializeField]
    [Range(0, 1f)]
    private float _explosionChanceOnOtherCollisions = 0;

    [SerializeField]
    private float _minForceForOtherCollisionExplosion = 3f;

    [SerializeField]
    private VRPrefabType _explosionPrefabType;

    [Header("Throw params")]
    [SerializeField]
    private float _velocityMultiplier = 2f;

    [SerializeField]
    private float _angularVelocityMultiplier = 1f;

    private bool _explosionFirstThrow = false;

    public float angularVelocityMultiplier
    {
        get
        {
            return _angularVelocityMultiplier;
        }
    }

    public float velocityMultiplier
    {
        get
        {
            return _velocityMultiplier;
        }
    }

    public bool _destroySelfAfterExplosion = true;

    public override void OnEquipped(IVRHand hand)
    {
        base.OnEquipped(hand);
        _explosionFirstThrow = true;

    }

    void OnCollisionEnter(Collision c)
    {
        // spawn explosion ONLY ON THE SERVER or SINGLEPLAYER
        if (NetServices.isServer || !NetServices.isNetworked)
        {
            if (c.collider.attachedRigidbody != null)
            {
                var player = c.collider.attachedRigidbody.GetComponentInParent<IVRPlayer>();
                if (player == _player)
                {
                    // if the hit player is the one throwing / shooting => ignore
                    return;
                }
            }

            if (_explosionFirstThrow)
            {
                _explosionFirstThrow = false;
                if (_explosionChanceOnFirstThrowCollision > 0)
                {
                    if (c.relativeVelocity.sqrMagnitude > _minForceForFirstCollisionExplosion * _minForceForFirstCollisionExplosion)
                    {
                        if (Random.value <= _explosionChanceOnFirstThrowCollision)
                        {
                            ServerExplode();
                        }
                    }
                }
            }
            else
            {
                if (_explosionChanceOnOtherCollisions > 0)
                {
                    if (c.relativeVelocity.sqrMagnitude > _minForceForOtherCollisionExplosion * _minForceForOtherCollisionExplosion)
                    {
                        if (Random.value <= _explosionChanceOnOtherCollisions)
                        {
                            ServerExplode();
                        }
                    }
                }
            }
        }
    }

    private void ServerExplode()
    {
        // spawn an explosion at that location
        VRPrefabManager.instance.Spawn(_explosionPrefabType, transform.position, Quaternion.identity);

        if (NetServices.isServer)
        {
            // tell everybody else to also spawn explosion
            var msg = MessagePool.Get<SpawnExplosionMessage>();
            msg.position = transform.position;
            msg.prefabType = _explosionPrefabType;
            ServerNetSender.instance.SendToAll(msg, UnityEngine.Networking.QosType.Reliable);
            MessagePool.Return(msg);
        }

        // make sure we are not still equipped
        if (this.isEquipped)
        {
            this.hand.Unequip(this, Vector3.zero, Vector3.zero);
        }

        DestroyCorrect(gameObject);
    }

    private void DestroyCorrect(GameObject go)
    {
        var manager = NetEntityDestructionManager.instance;
        if (manager != null)
        {
            var netEntity = go.GetComponent<INetEntity>();
            if (netEntity != null)
            {
                manager.DestroyNetEntity(netEntity);
            }
        }
        else
        {
            Destroy(go);
        }
    }
}