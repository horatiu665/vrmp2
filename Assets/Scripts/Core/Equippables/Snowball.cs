namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    [RequireComponent(typeof(Rigidbody), typeof(SphereCollider))]
    public sealed class Snowball : NetEquippableSpawnableBase, IHaveThrowMultipliers
    {
        [Header("Rolling")]
        [SerializeField, Range(0.0001f, 1f)]
        private float _sizeVelocityFactor = 0.05f;

        [SerializeField, Range(0.01f, 10f)]
        private float _sizeToMass = 1.5f;

        [SerializeField, Range(1f, 1000f)]
        private float _maxSize = 5f;

        [SerializeField, Range(0.01f, 0.99f)]
        private float _vibration = 0.05f;

        [Header("Multipliers")]
        [SerializeField, Range(0.1f, 100f)]
        private float _velocityMultiplier = 2f;

        [SerializeField, Range(0.1f, 100f)]
        private float _angularVelocityMultiplier = 1f;

        private float _size, _originalSize;
        private float _colliderRadius;
        private Vector3 _lastPos;

        public float velocityMultiplier
        {
            get { return _velocityMultiplier; }
        }

        public float angularVelocityMultiplier
        {
            get { return _angularVelocityMultiplier; }
        }

        protected override void Awake()
        {
            base.Awake();
            _colliderRadius = this.GetComponent<SphereCollider>().radius;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        
            if (_originalSize == 0f)
            {
                _originalSize = this.transform.localScale.x;
            }

            this.transform.localScale = Vector3.one * _originalSize;
            _size = _originalSize;
            _rb.mass = _size * _sizeToMass;
            _lastPos = this.position;
        }

        private void Update()
        {
            if (_size >= _maxSize)
            {
                // big enough
                return;
            }

            if (Physics.Raycast(this.position, Vector3.down, _colliderRadius * _size * 1.5f, Layers.terrain))
            {
                // increase size gradually as a function of velocity, when grounded
                var deltaPos = (_lastPos - this.position);

                _size += deltaPos.magnitude * _sizeVelocityFactor;
                this.transform.localScale = Vector3.one * _size;
                _rb.mass = _size * _sizeToMass;

                if (_isEquipped)
                {
                    if (_hand.controller != null)
                    {
                        _hand.controller.Vibrate(_vibration * _size);
                    }
                }
            }

            _lastPos = this.position;
        }
    }
}