namespace Core
{
    using VRCore;
    using VRCore.VRNetwork;

    public sealed class BowEquippable : NetEquippableSpawnableBase
    {
        private Bow _bow;

        protected override void Awake()
        {
            base.Awake();
            _bow = GetComponent<Bow>();
        }

        public override void OnEquipped(IVRHand hand)
        {
            base.OnEquipped(hand);
            _bow.Grab(hand);
        }

        public override void OnUnequipped(IVRHand hand)
        {
            base.OnUnequipped(hand);
            _bow.Ungrab();
        }
    }
}