﻿using Apex.WorldGeometry;
using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;

public class SkiStickMovement2 : MonoBehaviour
{
    #region Vars: Refs

    [Header("Refs")]

    /// <summary>
    /// Represents the forward direction of the ski stick(from the base towards the tip). Defaults to this.transform
    /// </summary>
    [SerializeField, Tooltip("Represents the forward direction of the ski stick (from the base towards the tip). Defaults to this.transform")]
    private Transform _skiStickForward;

    /// <summary>
    /// Represents the grip point along the ski stick. Difference between hand position and grip position will determine the amount of "push" into the ground. Defaults to this.transform
    /// </summary>
    [SerializeField, Tooltip("Represents the grip point along the ski stick. Difference between hand position and grip position will determine the amount of \"push\" into the ground. Defaults to this.transform")]
    private Transform _skiStickGrip;

    #endregion Vars: Refs

    #region Vars: Hand delta to final force multiplier

    [Header("Hand Delta to final force")]
    [SerializeField]
    private AnimationCurve _localHandDeltaToForce = AnimationCurve.Linear(0, 0, 1, 1);

    [SerializeField]
    private float _maxHandDelta = 1f;

    [Header("HandPush delta to final force")]
    [SerializeField]
    private AnimationCurve _localHandPushDeltaToForce = AnimationCurve.EaseInOut(0, 0, 1, 1);

    [SerializeField]
    private float _maxHandPushDelta = 10f;

    #endregion Vars: Hand delta to final force multiplier

    #region Vars: HandPush: dist between hand and stick grip

    [Header("HandPush: dist between hand and stick grip when pushing into ground)")]
    /// <summary>
    /// This curve maps the amount of push applied to the stick towards the ground to a value multiplied with the final stick force applied, incl. friction that can stop the player
    /// </summary>
    [SerializeField, Tooltip("This curve maps the amount of push applied to the stick towards the ground to a value multiplied with the final stick force applied, incl. friction that can stop the player")]
    private AnimationCurve _handPushStickForceMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private float _downwardPushMaxDistance = 0.35f;

    #endregion Vars: HandPush: dist between hand and stick grip

    #region Vars: Ski stick tip movement mapping (results in tip speed in m/s)

    [SerializeField, Header("Ski stick tip movement mapping (results in tip speed in m/s)")]
    private AnimationCurve _skiStickForceMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 1, 0) } };

    [SerializeField]
    private float _maxDeltaSpeed = 10f;

    [SerializeField]
    private float _skiStickForceMultiplier = 1f;

    #endregion Vars: Ski stick tip movement mapping (results in tip speed in m/s)

    #region Vars: Stick angle to ground

    [Header("Stick angle to ground (results in 90deg angle percent, from -1 to 1)")]
    /// <summary>
    /// Ski stick angle will affect the amount of force added when pushing against ground.
    /// Intended result: when stick tip points toward movement vector, it applies higher force than when tip points away from movement
    /// </summary>
    [SerializeField, Tooltip("Ski stick angle will affect the amount of force added when pushing against ground. Intended result: when stick tip points toward movement vector, it applies higher force than when tip points away from movement")]
    private AnimationCurve _skiStickAngleToForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(-1, 0, 0, 0), new Keyframe(1, 1, 1, 0) } };

    [SerializeField]
    private Vector2 _skiStickAngleForceRange = new Vector2(0.3f, 1f);

    #endregion Vars: Stick angle to ground

    #region Time-based non-accelerating

    [Header("Time to Brake and Time to FinalForce")]

    /// <summary>
    /// Time since grounded controls Drag vs FinalForce. So if we start applying the brakes, we don't apply the forces anymore
    /// </summary>
    [SerializeField, Tooltip("Time since grounded controls Drag vs FinalForce. So if we start applying the brakes, we don't apply the forces anymore")]
    private AnimationCurve _timeSinceGroundedToBrakes = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private Vector2 _timeSinceGroundedToBrakesRange = new Vector2(0, 0.5f);

    [SerializeField]
    private AnimationCurve _timeSinceGroundedToFinalForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 0, 1, 0) } };

    [SerializeField]
    private Vector2 _timeToFinalForceRange = new Vector2(1, 2);

    /// <summary>
    /// Maps linear time to the X axis of the curve above - min and max time become 0 and 1 respectively. Private var used in above calculation.
    /// </summary>
    private float _groundedTime = 0f;

    #endregion Time-based non-accelerating

    #region Vars: Braking

    [Header("Braking. Based on HandPush and playerSpeed")]
    [SerializeField]
    private AnimationCurve _handPushToDrag = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    /// <summary>
    /// Reduces the player velocity by this percent of his speed per second
    /// </summary>
    [SerializeField]
    private AnimationCurve _playerSpeedToDrag = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private Vector2 _playerSpeedRangeForDrag = new Vector2(1, 10f);

    [SerializeField]
    private Vector2 _playerSpeedToDragRange = new Vector2(0, 1f);

    public SmartSound soundBrake;

    public AnimationCurve soundBrakeVolumePlayerSpeed = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0), new Keyframe(2, 0f, 0, 0) } };

    /// <summary>
    /// multiplies the final value of drag (from all the above curves)
    /// </summary>
    [SerializeField, Tooltip("Multiplies the final value of drag (from all the above curves)")]
    private float _finalDragMultiplier = 1f;

    #endregion Vars: Braking

    #region Vars: PlayerSpeed to Final Force

    [Header("Final force multiplier based on playerSpeed")]
    /// <summary>
    /// Map the player speed to a final multiplier, to counter the high drag when the player is standing still
    /// </summary>
    [SerializeField, Tooltip("Map the player speed to a final multiplier, to counter the high drag when the player is standing still")]
    private AnimationCurve _playerSpeedToFinalForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private float _finalForceMaxPlayerSpeed = 10f;

    [SerializeField]
    private float _finalForceMaxForce = 1f;

    /// <summary>
    /// Projects final force to surface plane so the player cannot jump up or go through the ground
    /// </summary>
    [SerializeField, Tooltip("Projects final force to surface plane so the player cannot jump up or go through the ground")]
    private bool _onlyOnSurfacePlane = true;

    #endregion Vars: PlayerSpeed to Final Force

    #region Vars: Limit per ski direction

    [Header("Limit per ski direction components")]
    [SerializeField]
    private bool _limitPerDirectionComponent = true;

    [SerializeField]
    private float _compForward = 1f;

    [SerializeField]
    private AnimationCurve _playerVelocityToCompSide = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(5, 0, 0, 0) } };

    [SerializeField]
    private float _compSide = 1f;

    [SerializeField]
    private AnimationCurve _playerVelocityToCompBack = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(20, 0, 0, 0) } };

    [SerializeField]
    private float _compBack = 1f;

    [Header("1 means only view direction, 0 means any direction")]
    [SerializeField]
    private AnimationCurve _playerVelocityToCompViewDir = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [Header("Max push force when not impaled")]
    [SerializeField]
    private float _maxForceClamp = 4f;

    #endregion Vars: Limit per ski direction

    #region Vars: Vibration

    [Header("Vibration. Based on final added speed. Also OnCollisionEnter")]
    [SerializeField]
    private AnimationCurve _vibrationFromSpeed = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private float _vibrationFromSpeedMaxSpeed = 1f;

    /// <summary>
    /// Only adds vibration if player is moving. Even though sticks will add drag also when player is not moving.
    /// </summary>
    [SerializeField, Tooltip("Only adds vibration if player is moving. Even though sticks will add drag also when player is not moving.")]
    private AnimationCurve _playerSpeedToDragToVibration = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private AnimationCurve _dragToVibration = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private float _vibrationFromDragMax = 1f;

    [SerializeField]
    private float _vibrationOnCollision = 0.75f;

    #endregion Vars: Vibration

    #region Vars: Impale mechanic

    [Header("Impale mechanic")]
    [SerializeField]
    private float _impaleHandPush = 0.5f;

    private bool _stickPenetrated;

    // should be low because the stick unpenetrates as soon as we move away from its impale joint
    [SerializeField]
    private float _maxPlayerSpeedForPenetrate = 2.5f;

    private Arrow _arrow;
    public Arrow arrow
    {
        get
        {
            if (_arrow == null)
            {
                _arrow = GetComponent<Arrow>();
            }
            return _arrow;
        }
    }

    [SerializeField]
    private float _minEquipTimeForPenetration = 0.06667f;

    [SerializeField]
    private float _stickAngleForPenetration = -0.3f;

    [SerializeField]
    private float _handPushForDepenetration = -0.5f;

    [SerializeField]
    private float _vibrationOnPenetration = 0.8f;

    [SerializeField]
    private AnimationCurve _penetratedForceMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [SerializeField]
    private float _penetratedPushForceMultiplier = 2.5f;

    [SerializeField]
    private float _penetratedPushMaxForce = 3.5f;

    private float _equipTime;

    public SmartSound soundExpale;

    #endregion


    [SerializeField]
    private float _finalForceDebugScale = 10f;

    #region refs and privates

    private SkiStick _skiStick;
    public SkiStick skiStick
    {
        get
        {
            if (_skiStick == null)
            {
                _skiStick = GetComponent<SkiStick>();
            }
            return _skiStick;
        }
    }


    private GameObject gizmo;

    // about ski stick tip
    private Vector3 _lastSkiPos, _deltaSkiPos;
    private Vector3 _skiCollisionPoint;
    private Vector3 _lastPlayerPos;
    private Vector3 _groundedNormal;
    private Collider _groundedCollider;
    private bool _grounded;
    private float _oldHandPush;
    private Vector3 _oldHandPos;

    private Vector3 skierPos
    {
        get
        {
            return skiStick.skier.position;
        }
    }

    private Vector3 handPosition
    {
        get
        {
            return skiStick.hand.position;
        }
    }

    private Vector3 handForward
    {
        get
        {
            return skiStick.hand.transform.forward;
        }
    }

    private float headSideParam
    {
        get
        {
            return skiStick.skier.headSteeringParam;
        }
    }

    private void Vibrate(float vib)
    {
        if (skiStick != null && skiStick.hand != null && skiStick.hand.controller != null)
        {
            skiStick.hand.controller.Vibrate(vib);
        }
    }

    #endregion



    private bool SkierNotNullAndLocal()
    {
        return skiStick != null && skiStick.skier != null && skiStick.skier.isLocal;
    }

    private void Reset()
    {
        _skiStickForward = transform;
        _skiStickGrip = transform;
    }

    private void OnEnable()
    {
        if (_skiStickForward == null)
        {
            _skiStickForward = this.transform;
        }

        if (_skiStickGrip == null)
        {
            _skiStickGrip = this.transform;
        }

        skiStick.OnEquipEvent += SkiStick_OnEquipEvent;
        skiStick.OnUnequipEvent += SkiStick_OnUnequipEvent;

    }

    private void OnDisable()
    {
        skiStick.OnEquipEvent -= SkiStick_OnEquipEvent;
        skiStick.OnUnequipEvent -= SkiStick_OnUnequipEvent;

    }

    private void SkiStick_OnEquipEvent(VRCore.IVRHand hand)
    {
        _equipTime = Time.time;

        if (_stickPenetrated)
        {
            UndoGroundPenetration();

        }

        if (arrow != null && arrow.canImpale)
        {
            arrow.Unimpale();
        }

    }

    private void SkiStick_OnUnequipEvent(VRCore.IVRHand hand)
    {
        if (_stickPenetrated)
        {
            // nothing actually
        }

        if (soundBrake != null)
        {
            soundBrake.audio.volume = 0;
        }
    }

    private void FixedUpdate()
    {
        Vector3 deltaPlayerPos = Vector3.zero;
        var skierNotNullAndLocal = SkierNotNullAndLocal();

        if (skierNotNullAndLocal)
        {
            deltaPlayerPos = skierPos - _lastPlayerPos;
        }

        // delta pos of ski stick since last physics frame
        _deltaSkiPos = transform.TransformPoint(_skiCollisionPoint) - _lastSkiPos;

        if (arrow != null)
        {
            _stickPenetrated = arrow.ip.impaleJoint != null;
        }

        // if ski stick grounded
        if (_grounded)
        {
            if (skierNotNullAndLocal)
            {
                // vvv======= MULTI-USEFUL =======vvv
                var playerSpeed = deltaPlayerPos.magnitude / Time.fixedDeltaTime;
                var skiForward = skiStick.skier.skis;

                // vvvvvvvvvvvvvvv====================== AMOUNT OF PUSH INTO GROUND ======================vvvvvvvvvvvvvvv
                // We want to apply more force, the more the sticks are pushed into the ground
                // The collision system does not work that way, so there is no value such as "potential energy" or whatever that we can use
                // but we do know the vector from ski stick grip to tip, and where the hands are at this moment,
                // and the difference between the hand and grip positions, along the stick forward axis, while the stick collides with the ground,
                // can tell us how much we push the stick towards the ground.
                var gripToHandDistance = Vector3.Dot(handPosition - _skiStickGrip.position, _skiStickForward.forward);
                var handPushLinear = gripToHandDistance / _downwardPushMaxDistance;
                var handPushLinearAbs = Mathf.Abs(handPushLinear);
                // gripToHandDistance is the amount of push. handPushStickForce is the mapped value representing "push force" that we can use to apply friction and also modulate the final ski stick added force. Multiplies at the end.
                var handPushStickForce = _handPushStickForceMapping.Evaluate(handPushLinearAbs);

                // vvvvvvvvvvvvvvv====================== HAND _PUSH_ DELTA TO FORCE ======================vvvvvvvvvvvvvvv

                var handPushDelta = handPushLinear - _oldHandPush;
                _oldHandPush = handPushLinear;
                var localHandPushDeltaMultiplier = _localHandPushDeltaToForce.Evaluate(handPushDelta / Time.fixedDeltaTime / _maxHandPushDelta);

                // vvvvvvvvvvvvvvv====================== HAND DELTA TO FORCE ======================vvvvvvvvvvvvvvv

                var handDelta = skiStick.hand.localPosition - _oldHandPos;
                _oldHandPos = skiStick.hand.localPosition;
                var localHandDeltaMultiplier = _localHandDeltaToForce.Evaluate(handDelta.magnitude / Time.fixedDeltaTime / _maxHandDelta);

                if (!_stickPenetrated)
                {
                    // vvvvvvvvvvvvvvv====================== SKI STICK DELTA TO FORCE ======================vvvvvvvvvvvvvvv
                    // calculate magnitude of delta in units/sec (minus player delta so it's local stick velocity)
                    var deltaMag = (_deltaSkiPos.magnitude - deltaPlayerPos.magnitude) / Time.fixedDeltaTime;

                    // negative force, opposite direction of ski stick tip movement
                    var mappedDeltaForce = -_skiStickForceMapping.Evaluate(deltaMag / _maxDeltaSpeed) * _skiStickForceMultiplier;

                    // vvvvvvvvvvvvvvv====================== ANGLE OF STICK INTO GROUND ======================vvvvvvvvvvvvvvv
                    // adjust force based on angle of ski stick towards movement direction.
                    // ski stick angle matters only in relation to player velocity on slope.
                    // when player is going fast down the slope and the ski stick is angled backwards, it does not provide much friction.
                    // Even if the stick is pushed backwards in the above scenario, the stick will still not be considered to add force, since the player is going fast down the slope
                    var skiStickTipGlobalVelocity = _deltaSkiPos;
                    // if we project the ski stick forward vector on the global velocity, we can find if the stick is angled towards or against the movement
                    var stickTowardsMovement = Vector3.Dot(_skiStickForward.forward, skiStickTipGlobalVelocity.normalized);
                    // use this info. to map a multiplier, so when the stick is against the movement it has more impact to the velocity
                    var stickAngleMultiplier = Mathf.Lerp(_skiStickAngleForceRange.x, _skiStickAngleForceRange.y, _skiStickAngleToForce.Evaluate(stickTowardsMovement));


                    // vvvvvvvvvvvvvvv====================== BRAKE: decide if we brake or we accelerate based on time ======================vvvvvvvvvvvvvvv
                    // time
                    var timeSinceGrounded = Time.time - _groundedTime;
                    var timeBrakeParam = Mathf.InverseLerp(_timeSinceGroundedToBrakesRange.x, _timeSinceGroundedToBrakesRange.y, timeSinceGrounded);
                    var brakeMultiplier = _timeSinceGroundedToBrakes.Evaluate(timeBrakeParam);

                    var timeFinalForceParam = Mathf.InverseLerp(_timeToFinalForceRange.x, _timeToFinalForceRange.y, timeSinceGrounded);
                    var timeFinalForceMultiplier = _timeSinceGroundedToFinalForce.Evaluate(timeFinalForceParam);


                    // vvvvvvvvvvvvvvv====================== GROUND PENETRATION. BASED ON HAND PUSH AND TIME AND ANGLE ======================vvvvvvvvvvvvvvv
                    if (handPushLinear >= _impaleHandPush
                        && (Time.time - _equipTime) > _minEquipTimeForPenetration
                        && stickTowardsMovement > _stickAngleForPenetration
                        && playerSpeed < _maxPlayerSpeedForPenetrate
                        )
                    {
                        // impale stick in ground and activate secondary ski stick mechanic
                        DoGroundPenetration();
                    }

                    // vvvvvvvvvvvvvvv====================== BRAKE: APPLY FRICTION (DRAG) TO PLAYER ======================vvvvvvvvvvvvvvv
                    var playerSpeedParam = Mathf.InverseLerp(_playerSpeedRangeForDrag.x, _playerSpeedRangeForDrag.y, playerSpeed);
                    // map linear(raw) handPush to handPushDragMultiplier
                    var handPushDragMultiplier = _handPushToDrag.Evaluate(handPushLinear);
                    // player speed to drag
                    var psToDrag = Mathf.Lerp(_playerSpeedToDragRange.x, _playerSpeedToDragRange.y, _playerSpeedToDrag.Evaluate(playerSpeedParam));
                    // final applied drag is a factor of:
                    // 1. handPush to drag
                    // 2. playerSpeed to drag
                    // 3. timeSinceGround to drag
                    // 4. final multiplier
                    var stickDrag01 = handPushDragMultiplier * psToDrag * brakeMultiplier;
                    skiStick.skier.AddDragNextFrame(stickDrag01 * _finalDragMultiplier);

                    // vvvvvvvvvvvvvvv====================== playerSpeed TO FINAL FORCE ======================vvvvvvvvvvvvvvv
                    var playerSpeedMultiplier = _playerSpeedToFinalForce.Evaluate(playerSpeed / _finalForceMaxPlayerSpeed) * _finalForceMaxForce;

                    // vvvvvvvvvvvvvvv====================== FINAL FORCE ======================vvvvvvvvvvvvvvv
                    // can be negative because of angle
                    var finalForceParam = localHandDeltaMultiplier * localHandPushDeltaMultiplier * stickAngleMultiplier * playerSpeedMultiplier;
                    var finalForceMag = mappedDeltaForce * handPushStickForce * finalForceParam;
                    var finalForce = _deltaSkiPos.normalized * finalForceMag;

                    if (_onlyOnSurfacePlane)
                    {
                        finalForce = Vector3.ProjectOnPlane(finalForce, _groundedNormal);
                        finalForceMag = finalForce.magnitude;
                    }

                    // vvvvvvvvvvvvvvv====================== LIMIT PER SKI DIRECTION ======================vvvvvvvvvvvvvvv
                    // use multipliers for each component of the final force (split into the ski direction forward, side and back)
                    if (_limitPerDirectionComponent)
                    {
                        var fwComponent = Vector3.Project(finalForce, skiForward.forward);
                        var sideComp = Vector3.Project(finalForce, skiForward.right);
                        // we assume these are the only components because we do not allow forces along the normal of the collision surface (slope surface)
                        var fwOrBack = Vector3.Dot(fwComponent, skiForward.forward) > 0 ? true : false;
                        var realCompSide = _playerVelocityToCompSide.Evaluate(playerSpeed) * _compSide;
                        var realCompBack = _playerVelocityToCompBack.Evaluate(playerSpeed) * _compBack;
                        finalForce = (fwComponent * (fwOrBack ? _compForward : realCompBack)) + (sideComp * realCompSide);

                        var lookDir = skiStick.skier.head.forward;
                        var lookDirProjected = Vector3.Project(finalForce, lookDir);
                        var lookDirNonProjected = (finalForce - lookDirProjected);
                        if (Vector3.Dot(finalForce, lookDir) < 0)
                        {
                            // the whole final force is either side or back compared to view direction
                            finalForce = (1f - _playerVelocityToCompViewDir.Evaluate(playerSpeed)) * finalForce;
                        }
                        else
                        {
                            // limit finalForce to the vector projected on the view direction, plus only a fraction of the non-projected (which also contains any negative amount of the projected)
                            finalForce = lookDirProjected + (1f - _playerVelocityToCompViewDir.Evaluate(playerSpeed)) * lookDirNonProjected;
                        }
                        finalForceMag = finalForce.magnitude;
                    }

                    // vvvvvvvvvvvvvvv====================== ADD VIBRATION ======================vvvvvvvvvvvvvvv
                    var vibParam = Mathf.Abs(finalForceMag);
                    var vib = _vibrationFromSpeed.Evaluate(vibParam / _vibrationFromSpeedMaxSpeed);
                    vib += _playerSpeedToDragToVibration.Evaluate(playerSpeed) * _dragToVibration.Evaluate(stickDrag01) * _vibrationFromDragMax;
                    Vibrate(vib);

                    finalForce = Vector3.ClampMagnitude(finalForce, _maxForceClamp);

                    // apply force in the deltaPos direction with calculated magnitude
                    skiStick.skier.AddForce(this, finalForce);

                    //GraphVRDebug.instance.SetValue(finalForce.magnitude / _finalForceDebugScale, Color.red);

                    // vvvvvvvvvvvvvvv====================== BRAKING SOUNDS ======================vvvvvvvvvvvvvvv

                    var soundBrakeVolume = Mathf.Clamp01(handPushLinear);
                    soundBrakeVolume *= soundBrakeVolumePlayerSpeed.Evaluate(playerSpeed);
                    if (soundBrake != null)
                    {
                        if (!soundBrake.audio.isPlaying)
                        {
                            soundBrake.audio.Play();
                        }
                        soundBrake.audio.volume = soundBrakeVolume;
                    }
                }
                else
                {
                    // if handpush is less than zero, unimpale cause we are trying to lift the stick
                    if (handPushLinear < _handPushForDepenetration)
                    {
                        // un-penetrate! pull-out! (this should happen for all arrows btw)
                        UndoGroundPenetration();
                    }

                    //var fwdVector = _skiStickForward.forward;
                    var fwdVector = -handForward;
                    // use hand push as intensity, but use fwdVector for direction of push. we cannot control ski stick rotation easily, but our hands w/ controllers are more intuitive for setting our intended rotation
                    Vector3 pushIntoGround = Mathf.Max(0, handPushLinear) * fwdVector;
                    pushIntoGround = -Vector3.ProjectOnPlane(pushIntoGround, _groundedNormal);
                    // now it's on the ground and we must remap the magnitude
                    var pigMag = pushIntoGround.magnitude;
                    if (pigMag != 0)
                    {
                        var pigMappedForce = _penetratedForceMapping.Evaluate(pigMag);
                        pushIntoGround = pushIntoGround / pigMag;

                        var finalForceMag = Mathf.Min(_penetratedPushMaxForce, _penetratedPushForceMultiplier * localHandDeltaMultiplier * localHandPushDeltaMultiplier * pigMappedForce);
                        var finalForce = pushIntoGround * finalForceMag;

                        // vvvvvvvvvvvvvvv====================== LIMIT PER SKI DIRECTION ======================vvvvvvvvvvvvvvv
                        // use multipliers for each component of the final force (split into the ski direction forward, side and back)
                        if (_limitPerDirectionComponent)
                        {
                            var fwComponent = Vector3.Project(finalForce, skiForward.forward);
                            var sideComp = Vector3.Project(finalForce, skiForward.right);
                            // we assume these are the only components because we do not allow forces along the normal of the collision surface (slope surface)
                            var fwOrBack = Vector3.Dot(fwComponent, skiForward.forward) > 0 ? true : false;
                            var realCompSide = _playerVelocityToCompSide.Evaluate(playerSpeed) * _compSide;
                            var realCompBack = _playerVelocityToCompBack.Evaluate(playerSpeed) * _compBack;
                            finalForce = (fwComponent * (fwOrBack ? _compForward : realCompBack)) + (sideComp * realCompSide);

                            var lookDir = skiStick.skier.head.forward;
                            var lookDirProjected = Vector3.Project(finalForce, lookDir);
                            var lookDirNonProjected = (finalForce - lookDirProjected);
                            if (Vector3.Dot(finalForce, lookDir) < 0)
                            {
                                // the whole final force is either side or back compared to view direction
                                finalForce = (1f - _playerVelocityToCompViewDir.Evaluate(playerSpeed)) * finalForce;
                            }
                            else
                            {
                                // limit finalForce to the vector projected on the view direction, plus only a fraction of the non-projected (which also contains any negative amount of the projected)
                                finalForce = lookDirProjected + (1f - _playerVelocityToCompViewDir.Evaluate(playerSpeed)) * lookDirNonProjected;
                            }
                            finalForceMag = finalForce.magnitude;
                        }

                        skiStick.skier.AddForce(this, finalForce);

                        //GraphVRDebug.instance.SetValue(finalForce.magnitude / _finalForceDebugScale, Colors.Orange);

                        if (soundBrake != null)
                        {
                            if (soundBrake.audio.isPlaying)
                            {
                                soundBrake.audio.Stop();
                            }
                            soundBrake.audio.volume = 0f;
                        }
                    }
                }
            }
        }

        _lastSkiPos = transform.TransformPoint(_skiCollisionPoint);

        if (skierNotNullAndLocal)
        {
            _lastPlayerPos = skierPos;
        }
    }

    private void DoGroundPenetration()
    {
        _stickPenetrated = true;

        // create joint at tip of ski stick.
        // move tip a bit under ground (auto)
        // impale sound (auto?)
        if (arrow != null)
        {
            var j = arrow.ip.impaleJoint;
            if (j == null)
            {
                // hack impale
                ImpalementHandler.Impale(arrow, _groundedCollider, arrow.ip.impaleForward.forward * (arrow.ip.minImpaleForce + 1), arrow.ip.impaleForward.position);

                // create snow splat particle effect

            }

            arrow.SetImpaleJointWeakness(true);
        }

        // vibrate
        Vibrate(_vibrationOnPenetration);

    }

    private void UndoGroundPenetration()
    {
        _stickPenetrated = false;

        if (arrow != null && arrow.ip.impaleJoint != null)
        {
            arrow.Unimpale();

            if (soundExpale != null)
            {
                soundExpale.Play();
            }
        }

    }

    public void OnCollisionEnter(Collision collision)
    {
        // if colliding with terrain
        if (Layers.InLayer(collision.gameObject, Layers.terrain))
        {
            _grounded = true;
            _groundedNormal = collision.contacts[0].normal;
            _groundedCollider = collision.collider;
            _groundedTime = Time.time;

            var p = collision.contacts[0].point;

            _skiCollisionPoint = transform.InverseTransformPoint(p);

            if (skiStick.isEquipped)
            {
                Vibrate(_vibrationOnCollision);
            }

        }
    }

    public void OnCollisionExit(Collision collision)
    {
        // if exiting collision with terrain
        if (Layers.InLayer(collision.gameObject, Layers.terrain))
        {
            _grounded = false;
            if (soundBrake != null)
            {
                soundBrake.audio.volume = 0;
            }
        }
    }
}