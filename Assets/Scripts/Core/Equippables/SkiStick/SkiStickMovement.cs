namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    /// <summary>
    /// enabled when ski should be moving player, in SkiStickH.cs
    /// </summary>
    [RequireComponent(typeof(SkiStick))]
    public sealed class SkiStickMovement : MonoBehaviour
    {
        #region Vars: Refs

        [Header("Refs")]

        /// <summary>
        /// Represents the forward direction of the ski stick(from the base towards the tip). Defaults to this.transform
        /// </summary>
        [SerializeField, FormerlySerializedAs("skiStickForward"), Tooltip("Represents the forward direction of the ski stick (from the base towards the tip). Defaults to this.transform")]
        private Transform _skiStickForward;

        /// <summary>
        /// Represents the grip point along the ski stick. Difference between hand position and grip position will determine the amount of "push" into the ground. Defaults to this.transform
        /// </summary>
        [SerializeField, FormerlySerializedAs("skiStickGrip"), Tooltip("Represents the grip point along the ski stick. Difference between hand position and grip position will determine the amount of \"push\" into the ground. Defaults to this.transform")]
        private Transform _skiStickGrip;

        #endregion Vars: Refs

        #region Vars: Hand delta to final force multiplier

        [Header("Hand delta to final force")]
        [SerializeField]
        private AnimationCurve _localHandDeltaToForce = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [SerializeField]
        private float _maxHandDelta = 1f;

        #endregion Vars: Hand delta to final force multiplier

        #region Vars: Ski stick tip movement mapping (results in tip speed in m/s)

        [SerializeField, FormerlySerializedAs("skiStickForceMapping"), Header("Ski stick tip movement mapping (results in tip speed in m/s)")]
        private AnimationCurve _skiStickForceMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 1, 0) } };

        [SerializeField, FormerlySerializedAs("maxDeltaSpeed")]
        private float _maxDeltaSpeed = 10f;

        [SerializeField, FormerlySerializedAs("skiStickForceMultiplier")]
        private float _skiStickForceMultiplier = 1f;

        #endregion Vars: Ski stick tip movement mapping (results in tip speed in m/s)

        #region Vars: Stick angle to ground

        [Header("Stick angle to ground (results in 90deg angle percent, from -1 to 1)")]
        /// <summary>
        /// Ski stick angle will affect the amount of force added when pushing against ground.
        /// Intended result: when stick tip points toward movement vector, it applies higher force than when tip points away from movement
        /// </summary>
        [SerializeField, FormerlySerializedAs("skiStickAngleToForce"), Tooltip("Ski stick angle will affect the amount of force added when pushing against ground. Intended result: when stick tip points toward movement vector, it applies higher force than when tip points away from movement")]
        private AnimationCurve _skiStickAngleToForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(-1, 0, 0, 0), new Keyframe(1, 1, 1, 0) } };

        [SerializeField, FormerlySerializedAs("skiStickAngleToForce"), MinMax]
        private Vector2 _skiStickAngleForceRange = new Vector2(0.3f, 1f);

        #endregion Vars: Stick angle to ground

        #region Vars: HandPush: dist between hand and stick grip

        [Header("HandPush: dist between hand and stick grip when pushing into ground)")]
        /// <summary>
        /// This curve maps the amount of push applied to the stick towards the ground to a value multiplied with the final stick force applied, incl. friction that can stop the player
        /// </summary>
        [SerializeField, FormerlySerializedAs("handPushStickForceMapping"), Tooltip("This curve maps the amount of push applied to the stick towards the ground to a value multiplied with the final stick force applied, incl. friction that can stop the player")]
        private AnimationCurve _handPushStickForceMapping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("downwardPushMaxDistance")]
        private float _downwardPushMaxDistance = 0.25f;

        [SerializeField, FormerlySerializedAs("alsoUseRaycastHandPush")]
        private bool _alsoUseRaycastHandPush = false;

        /// <summary>
        /// Uses raycast starting from controller position into controller direction to calculate alternative handpush that should remove some of the shit physics
        /// </summary>
        [SerializeField, FormerlySerializedAs("raycastHandPushMultiplier"), Tooltip("Uses raycast starting from controller position into controller direction to calculate alternative handpush that should remove some of the shit physics")]
        private AnimationCurve _raycastHandPushMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("maxRaycastHandPush")]
        private float _maxRaycastHandPush = 0.5f;

        #endregion Vars: HandPush: dist between hand and stick grip

        #region Vars: PlayerSpeed to Final Force

        [Header("Final force multiplier based on playerSpeed")]
        /// <summary>
        /// Map the player speed to a final multiplier, to counter the high drag when the player is standing still
        /// </summary>
        [SerializeField, FormerlySerializedAs("playerSpeedToFinalForce"), Tooltip("Map the player speed to a final multiplier, to counter the high drag when the player is standing still")]
        private AnimationCurve _playerSpeedToFinalForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("finalForceMaxPlayerSpeed")]
        private float _finalForceMaxPlayerSpeed = 10f;

        [SerializeField, FormerlySerializedAs("finalForceMaxForce")]
        private float _finalForceMaxForce = 1f;

        /// <summary>
        /// Projects final force to surface plane so the player cannot jump up or go through the ground
        /// </summary>
        [SerializeField, FormerlySerializedAs("onlyOnSurfacePlane"), Tooltip("Projects final force to surface plane so the player cannot jump up or go through the ground")]
        private bool _onlyOnSurfacePlane = true;

        #endregion Vars: PlayerSpeed to Final Force

        #region Vars: Limit per ski direction

        [Header("Limit per ski direction components")]
        [SerializeField]
        private bool _limitPerDirectionComponent = true;

        [SerializeField]
        private float _compForward = 1f;

        [SerializeField]
        private AnimationCurve _playerVelocityToCompSide = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(5, 0, 0, 0) } };

        [SerializeField]
        private float _compSide = 0f;

        [SerializeField]
        private AnimationCurve _playerVelocityToCompBack = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(20, 0, 0, 0) } };

        [SerializeField]
        private float _compBack = 0.3333f;

        #endregion Vars: Limit per ski direction

        #region Time-based non-accelerating

        [Header("Time to Brake and Time to FinalForce")]

        /// <summary>
        /// Time since grounded controls Drag vs FinalForce. So if we start applying the brakes, we don't apply the forces anymore
        /// </summary>
        [SerializeField, FormerlySerializedAs("timeSinceGroundedToBrakes"), Tooltip("Time since grounded controls Drag vs FinalForce. So if we start applying the brakes, we don't apply the forces anymore")]
        private AnimationCurve _timeSinceGroundedToBrakes = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("timeSinceGroundedToBrakesRange"), MinMax]
        private Vector2 _timeSinceGroundedToBrakesRange = new Vector2(0, 0.5f);

        [SerializeField, FormerlySerializedAs("timeSinceGroundedToFinalForce")]
        private AnimationCurve _timeSinceGroundedToFinalForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 0, 1, 0) } };

        [SerializeField, FormerlySerializedAs("timeToFinalForceRange"), MinMax]
        private Vector2 _timeToFinalForceRange = new Vector2(1, 2);

        /// <summary>
        /// Maps linear time to the X axis of the curve above - min and max time become 0 and 1 respectively. Private var used in above calculation.
        /// </summary>
        private float _groundedTime = 0f;

        #endregion Time-based non-accelerating

        #region Vars: Braking

        [Header("Braking. Based on HandPush and playerSpeed")]
        [SerializeField, FormerlySerializedAs("handPushToDrag")]
        private AnimationCurve _handPushToDrag = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        /// <summary>
        /// Reduces the player velocity by this percent of his speed per second
        /// </summary>
        [SerializeField, FormerlySerializedAs("playerSpeedToDrag")]
        private AnimationCurve _playerSpeedToDrag = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("playerSpeedRangeForDrag"), MinMax]
        private Vector2 _playerSpeedRangeForDrag = new Vector2(1, 10f);

        [SerializeField, FormerlySerializedAs("playerSpeedToDragRange"), MinMax]
        private Vector2 _playerSpeedToDragRange = new Vector2(0, 1f);

        /// <summary>
        /// multiplies the final value of drag (from all the above curves)
        /// </summary>
        [SerializeField, FormerlySerializedAs("finalDragMultiplier"), Tooltip("Multiplies the final value of drag (from all the above curves)")]
        private float _finalDragMultiplier = 1f;

        #endregion Vars: Braking

        #region Vars: MaxVelocity adjust

        [Header("MaxVelocity Adjust")]
        [SerializeField]
        private AnimationCurve _brakeToMaxVelocity = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField]
        private float _brakeToMaxVelAmount = -20f;

        [SerializeField]
        private AnimationCurve _forceToMaxVelocity = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField]
        private float _forceToMaxVelAmount = 30f;

        #endregion

        #region Vars: Steering

        [Header("Ski Stick Steering")]

        /// <summary>
        /// If false, bypasses calculations used for steering (for this section)
        /// </summary>
        [SerializeField, FormerlySerializedAs("useSteering"), Tooltip("If false, bypasses calculations used for steering (for this section)")]
        private bool _useSteering = false;

        /// <summary>
        /// Multiplier for steering, based on ski stick left/right compared to sticks. Input uses same calculation as headPos01 for head steering.
        /// </summary>
        [SerializeField, FormerlySerializedAs("skiStickSideToSteering"), Tooltip("Multiplier for steering, based on ski stick left/right compared to sticks. Input uses same calculation as headPos01 for head steering.")]
        private AnimationCurve _skiStickSideToSteering = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("maxStickSidePos")]
        private float _maxStickSidePos = 0.3f;

        /// <summary>
        /// Takes head position 01 from the ski player steering component, and maps that with a curve to a multiplier that applies stick steering.
        /// </summary>
        [SerializeField, FormerlySerializedAs("headPosToSteering"), Tooltip("Takes head position 01 from the ski player steering component, and maps that with a curve to a multiplier that applies stick steering.")]
        private AnimationCurve _headPosToSteering = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("headPosToSteeringRange"), MinMax]
        private Vector2 _headPosToSteeringRange = new Vector2(0, 1f);

        [SerializeField, FormerlySerializedAs("drag01ToSteeringMultiplier")]
        private float _drag01ToSteeringMultiplier = 10f;

        #endregion Vars: Steering

        #region Vars: Vibration

        [Header("Vibration. Based on final added speed. Also OnCollisionEnter")]
        [SerializeField, FormerlySerializedAs("vibrationFromSpeed")]
        private AnimationCurve _vibrationFromSpeed = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("vibrationFromSpeedMaxSpeed")]
        private float _vibrationFromSpeedMaxSpeed = 1f;

        /// <summary>
        /// Only adds vibration if player is moving. Even though sticks will add drag also when player is not moving.
        /// </summary>
        [SerializeField, FormerlySerializedAs("playerSpeedToDragToVibration"), Tooltip("Only adds vibration if player is moving. Even though sticks will add drag also when player is not moving.")]
        private AnimationCurve _playerSpeedToDragToVibration = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("dragToVibration")]
        private AnimationCurve _dragToVibration = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField, FormerlySerializedAs("vibrationFromDragMax")]
        private float _vibrationFromDragMax = 1f;

        [SerializeField, FormerlySerializedAs("vibrationOnCollision")]
        private float _vibrationOnCollision = 0.5f;

        #endregion Vars: Vibration

        ////#region Vars: Debug

        ////[Header("Debug")]
        ////public DebugShower showDebug;

        ////[System.Serializable]
        ////public class DebugShower
        ////{
        ////    public bool enabled = true;
        ////    public bool finalForce;
        ////    public bool printRawTipSpeedPerSecond;
        ////    public bool skierVelocity;
        ////    public bool stickAngle;
        ////    public bool handPush;
        ////    public bool raycastHandPush;
        ////    public bool drag;
        ////}

        ////#endregion Vars: Debug

        #region Vars: privates

        private SkiStick _skiStick;

        // about ski stick tip
        private Vector3 _lastSkiPos, _deltaSkiPos;

        private Vector3 _skiCollisionPoint;
        private Vector3 _lastPlayerPos;

        private Vector3 _groundedNormal;
        private bool _grounded;

        private Vector3 _oldLocalHandPosition;
        

        #endregion Vars: privates

        #region properties and funcs for using both grab methods

        private Vector3 skierPos
        {
            get
            {
                return _skiStick.skier.position;
            }
        }

        private Vector3 handPosition
        {
            get
            {
                return _skiStick.hand.position;
            }
        }

        private Vector3 handForward
        {
            get
            {
                return _skiStick.hand.transform.forward;
            }
        }

        private float headSideParam
        {
            get
            {
                return _skiStick.skier.headSteeringParam;
            }
        }

        private void Vibrate(float vib)
        {
            if (_skiStick != null && _skiStick.hand != null && _skiStick.hand.controller != null)
            {
                _skiStick.hand.controller.Vibrate(vib);
            }
        }

        #endregion properties and funcs for using both grab methods

        private bool SkierNotNullAndLocal()
        {
            return _skiStick != null && _skiStick.skier != null && _skiStick.skier.isLocal;
        }

        private void Reset()
        {
            _skiStickForward = transform;
            _skiStickGrip = transform;
        }

        private void OnEnable()
        {
            if (_skiStickForward == null)
            {
                _skiStickForward = this.transform;
            }

            if (_skiStickGrip == null)
            {
                _skiStickGrip = this.transform;
            }

            _skiStick = this.GetComponent<SkiStick>();
        }

        private void FixedUpdate()
        {
            Vector3 deltaPlayerPos = Vector3.zero;
            if (SkierNotNullAndLocal())
            {
                deltaPlayerPos = skierPos - _lastPlayerPos;
            }

            // delta pos of ski stick since last physics frame
            _deltaSkiPos = transform.TransformPoint(_skiCollisionPoint) - _lastSkiPos;

            // if ski stick is on the ground, add the delta pos to the player pos - move him one to one as if ski stick was a Google Maps finger scrolling on the map
            if (_grounded)
            {
                if (SkierNotNullAndLocal())
                {
                    // vvvvvvvvvvvvvvv====================== HAND DELTA TO FORCE ======================vvvvvvvvvvvvvvv
                    var localHandDelta = _skiStick.hand.localPosition - _oldLocalHandPosition;
                    // in units / s
                    var localHandDeltaMultiplier = _localHandDeltaToForce.Evaluate(localHandDelta.magnitude / Time.fixedDeltaTime / _maxHandDelta);
                    _oldLocalHandPosition = _skiStick.hand.localPosition;

                    // vvvvvvvvvvvvvvv====================== AMOUNT OF PUSH INTO GROUND ======================vvvvvvvvvvvvvvv
                    // We want to apply more force, the more the sticks are pushed into the ground
                    // The collision system does not work that way, so there is no value such as "potential energy" or whatever that we can use
                    // but we do know the vector from ski stick grip to tip, and where the hands are at this moment,
                    // and the difference between the hand and grip positions, along the stick forward axis, while the stick collides with the ground,
                    // can tell us how much we push the stick towards the ground.
                    var gripToHandDistance = Mathf.Abs(Vector3.Dot(handPosition - _skiStickGrip.position, _skiStickForward.forward));
                    var handPushLinear = gripToHandDistance / _downwardPushMaxDistance;
                    // gripToHandDistance is the amount of push. handPushStickForce is the mapped value representing "push force" that we can use to apply friction and also modulate the final ski stick added force. Multiplies at the end.
                    var handPushStickForce = _handPushStickForceMapping.Evaluate(handPushLinear);

                    float alternativeHandPushMultiplier = 1f;
                    if (_alsoUseRaycastHandPush)
                    {
                        var alternativeHandPush = 0f;
                        RaycastHit info;
                        if (Physics.Raycast(handPosition, -handForward, out info, 1f, Layers.terrain))
                        {
                            alternativeHandPush = 1 - (info.point - handPosition).magnitude;
                        }

                        alternativeHandPushMultiplier = _raycastHandPushMultiplier.Evaluate(alternativeHandPush / _maxRaycastHandPush);
                    }

                    // vvvvvvvvvvvvvvv====================== SKI STICK DELTA TO FORCE ======================vvvvvvvvvvvvvvv
                    // calculate magnitude of delta in units/sec (minus player delta so it's local stick velocity)
                    var deltaMag = (_deltaSkiPos.magnitude - deltaPlayerPos.magnitude) / Time.fixedDeltaTime;

                    // negative force, opposite direction of ski stick tip movement
                    var mappedDeltaForce = -_skiStickForceMapping.Evaluate(deltaMag / _maxDeltaSpeed) * _skiStickForceMultiplier;

                    // vvvvvvvvvvvvvvv====================== ANGLE OF STICK INTO GROUND ======================vvvvvvvvvvvvvvv
                    // adjust force based on angle of ski stick towards movement direction.
                    // ski stick angle matters only in relation to player velocity on slope.
                    // when player is going fast down the slope and the ski stick is angled backwards, it does not provide much friction.
                    // Even if the stick is pushed backwards in the above scenario, the stick will still not be considered to add force, since the player is going fast down the slope
                    var skiStickTipGlobalVelocity = _deltaSkiPos;
                    // if we project the ski stick forward vector on the global velocity, we can find if the stick is angled towards or against the movement
                    var stickTowardsMovement = Vector3.Dot(_skiStickForward.forward, skiStickTipGlobalVelocity.normalized);
                    // use this info. to map a multiplier, so when the stick is against the movement it has more impact to the velocity
                    var stickAngleMultiplier = Mathf.Lerp(_skiStickAngleForceRange.x, _skiStickAngleForceRange.y, _skiStickAngleToForce.Evaluate(stickTowardsMovement));

                    // vvvvvvvvvvvvvvv====================== BRAKE: decide if we brake or we accelerate based on time ======================vvvvvvvvvvvvvvv
                    // time
                    var timeSinceGrounded = Time.time - _groundedTime;
                    var timeBrakeParam = Mathf.InverseLerp(_timeSinceGroundedToBrakesRange.x, _timeSinceGroundedToBrakesRange.y, timeSinceGrounded);
                    var brakeMultiplier = _timeSinceGroundedToBrakes.Evaluate(timeBrakeParam);

                    var timeFinalForceParam = Mathf.InverseLerp(_timeToFinalForceRange.x, _timeToFinalForceRange.y, timeSinceGrounded);
                    var timeFinalForceMultiplier = _timeSinceGroundedToFinalForce.Evaluate(timeFinalForceParam);

                    // vvvvvvvvvvvvvvv====================== BRAKE: APPLY FRICTION (DRAG) TO PLAYER ======================vvvvvvvvvvvvvvv
                    var playerSpeed = deltaPlayerPos.magnitude / Time.fixedDeltaTime;
                    var playerSpeedParam = Mathf.InverseLerp(_playerSpeedRangeForDrag.x, _playerSpeedRangeForDrag.y, playerSpeed);
                    // map linear(raw) handPush to handPushDragMultiplier
                    var handPushDragMultiplier = _handPushToDrag.Evaluate(handPushLinear);
                    // player speed to drag
                    var psToDrag = Mathf.Lerp(_playerSpeedToDragRange.x, _playerSpeedToDragRange.y, _playerSpeedToDrag.Evaluate(playerSpeedParam));
                    // final applied drag is a factor of:
                    // 1. handPush to drag
                    // 2. playerSpeed to drag
                    // 3. timeSinceGround to drag
                    // 4. final multiplier
                    var stickDrag01 = handPushDragMultiplier * psToDrag * brakeMultiplier;
                    _skiStick.skier.AddDragNextFrame(stickDrag01 * _finalDragMultiplier);

                    var skiForward = _skiStick.skier.skis;

                    // vvvvvvvvvvvvvvv====================== STEERING: augment head steering ======================vvvvvvvvvvvvvvv
                    if (_useSteering)
                    {
                        // steering uses the drag parameter used for braking, but additionally takes headPos01 into account,
                        // so the sticks augment the head steering by factors related to the sticks.

                        // this is the stick position left/right compared to the skis, using same calculation as the head is doing for the steering
                        var stickSideParam = Vector3.Dot(_skiStick.skier.head.position - skiForward.position, skiForward.right) / _maxStickSidePos;
                        // now the side param is -1..1 meaning left..right
                        // are we on the same side of the skis as the head? or are we on the opposite side?
                        stickSideParam *= (Mathf.Sign(headSideParam) == Mathf.Sign(stickSideParam)) ? 1 : -1;

                        // this does not take into account if the stick is in the opposite direction than the head. actually it simply uses the same curve to apply reverse force in that case.
                        var stickSideMultiplier = _skiStickSideToSteering.Evaluate(Mathf.Abs(stickSideParam)) * Mathf.Sign(stickSideParam);

                        var headPosMultiplier = Mathf.Lerp(_headPosToSteeringRange.x, _headPosToSteeringRange.y, _headPosToSteering.Evaluate(Mathf.Abs(headSideParam)));

                        var skiStickSteeringMapped = stickSideMultiplier * stickDrag01 * _drag01ToSteeringMultiplier * headPosMultiplier;

                        // add force sideways. it will be applied on top of the other force.
                        _skiStick.skier.AddForce(this, skiForward.right * skiStickSteeringMapped);
                    }

                    // vvvvvvvvvvvvvvv====================== playerSpeed TO FINAL FORCE ======================vvvvvvvvvvvvvvv
                    var playerSpeedMultiplier = _playerSpeedToFinalForce.Evaluate(playerSpeed / _finalForceMaxPlayerSpeed) * _finalForceMaxForce;

                    // vvvvvvvvvvvvvvv====================== FINAL FORCE ======================vvvvvvvvvvvvvvv
                    // can be negative because of angle
                    var finalForceParam = localHandDeltaMultiplier * stickAngleMultiplier * playerSpeedMultiplier * alternativeHandPushMultiplier * timeFinalForceMultiplier;
                    var finalForceMag = mappedDeltaForce * handPushStickForce * finalForceParam;
                    var finalForce = _deltaSkiPos.normalized * finalForceMag;

                    if (_onlyOnSurfacePlane)
                    {
                        finalForce = Vector3.ProjectOnPlane(finalForce, _groundedNormal);
                        finalForceMag = finalForce.magnitude;
                    }

                    // vvvvvvvvvvvvvvv====================== ADJUST MAX VELOCITY BASED ON SKI STICK BOOST AND BREAK ======================vvvvvvvvvvvvvvv
                    var maxVelBreakAmount = _brakeToMaxVelocity.Evaluate(brakeMultiplier * handPushLinear) * _brakeToMaxVelAmount;
                    var maxVelAccelAmount = _forceToMaxVelocity.Evaluate(finalForceParam * handPushLinear) * _forceToMaxVelAmount;

                    _skiStick.skier.velocityController.IncreaseMaxVelocity((maxVelBreakAmount + maxVelAccelAmount) * Time.fixedDeltaTime);

                    // vvvvvvvvvvvvvvv====================== LIMIT PER SKI DIRECTION ======================vvvvvvvvvvvvvvv
                    // use multipliers for each component of the final force (split into the ski direction forward, side and back)
                    if (_limitPerDirectionComponent)
                    {
                        var fwComponent = Vector3.Project(finalForce, skiForward.forward);
                        var sideComp = Vector3.Project(finalForce, skiForward.right);
                        // we assume these are the only components because we do not allow forces along the normal of the collision surface (slope surface)
                        var fwOrBack = Vector3.Dot(fwComponent, skiForward.forward) > 0 ? true : false;
                        var realCompSide = _playerVelocityToCompSide.Evaluate(playerSpeed) * _compSide;
                        var realCompBack = _playerVelocityToCompBack.Evaluate(playerSpeed) * _compBack;
                        finalForce = (fwComponent * (fwOrBack ? _compForward : realCompBack)) + (sideComp * realCompSide);
                        finalForceMag = finalForce.magnitude;
                    }

                    // vvvvvvvvvvvvvvv====================== ADD VIBRATION ======================vvvvvvvvvvvvvvv
                    var vibParam = Mathf.Abs(finalForceMag);
                    var vib = _vibrationFromSpeed.Evaluate(vibParam / _vibrationFromSpeedMaxSpeed);
                    vib += _playerSpeedToDragToVibration.Evaluate(playerSpeed) * _dragToVibration.Evaluate(stickDrag01) * _vibrationFromDragMax;
                    Vibrate(vib);

                    // vvvvvvvvvvvvvvv====================== DEBUG ======================vvvvvvvvvvvvvvv
                    //if (showDebug.enabled)
                    //{
                    //    var tipPos = transform.TransformPoint(_skiCollisionPoint);

                    //    if (showDebug.printRawTipSpeedPerSecond)
                    //    {
                    //        Debug.Log(deltaMag.ToString("F2"), gameObject);
                    //        Debug.DrawRay(tipPos, Vector3.up * deltaMag, Color.green, 1f);
                    //    }

                    //    if (showDebug.finalForce)
                    //    {
                    //        Debug.DrawRay(tipPos, finalForce, Color.red, 1f);
                    //        Debug.DrawRay(tipPos, Mathf.Abs(finalForceMag) * Vector3.up, Color.red, 1f);
                    //    }

                    //    if (showDebug.handPush)
                    //    {
                    //        Debug.DrawRay(tipPos, Vector3.up * handPushStickForce, Color.yellow, 1f);
                    //    }

                    //    if (showDebug.raycastHandPush)
                    //    {
                    //        Debug.DrawRay(tipPos, Vector3.up * alternativeHandPushMultiplier, new Color(1, 0.6f, 0.1f), 1f);
                    //    }

                    //    if (showDebug.drag)
                    //    {
                    //        Debug.DrawRay(tipPos + Vector3.right * 0.004f, Vector3.up * stickDrag01, Color.black, 1f);
                    //    }

                    //    if (showDebug.skierVelocity)
                    //    {
                    //        Debug.DrawRay(skierPos, skierRigidbody.velocity, Color.green, 1f);
                    //    }

                    //    if (showDebug.stickAngle)
                    //    {
                    //        Debug.DrawRay(tipPos, Vector3.up * Mathf.Abs(stickTowardsMovement), stickTowardsMovement > 0 ? Color.green : Color.red, 1f);
                    //    }
                    //}

                    // apply force in the deltaPos direction with calculated magnitude
                    _skiStick.skier.AddForce(this, finalForce);
                }
            }

            _lastSkiPos = transform.TransformPoint(_skiCollisionPoint);

            if (SkierNotNullAndLocal())
            {
                _lastPlayerPos = skierPos;
            }
        }

        public void OnCollisionEnter(Collision collision)
        {
            if (!enabled)
            {
                return;
            }

            // if colliding with terrain
            if (Layers.InLayer(collision.gameObject, Layers.terrain))
            {
                _grounded = true;
                _groundedNormal = collision.contacts[0].normal;
                _groundedTime = Time.time;

                var p = collision.contacts[0].point;

                _skiCollisionPoint = transform.InverseTransformPoint(p);

                if (_skiStick.isEquipped)
                {
                    Vibrate(_vibrationOnCollision);
                }
                
            }
        }

        public void OnCollisionExit(Collision collision)
        {
            if (!enabled)
            {
                return;
            }

            // if exiting collision with terrain
            if (Layers.InLayer(collision.gameObject, Layers.terrain))
            {
                _grounded = false;
            }
        }
    }
}