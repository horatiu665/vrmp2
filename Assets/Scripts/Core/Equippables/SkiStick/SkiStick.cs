namespace Core
{
    using Apex.Services;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;
    using VRCore.VRNetwork;

    [RequireComponent(typeof(Rigidbody))]
    public sealed class SkiStick : NetEquippableSpawnableBase, IHaveThrowMultipliers, IHandleMessage<VRInputTouch>, IHandleMessage<VRInputTouchDown>
    {
        /// <summary>
        /// Allows the use of the touchpad to change length of the ski stick while grabbed
        /// </summary>
        [Header("Shaft Length Adjustment")]
        [SerializeField, FormerlySerializedAs("lengthControlsActive"), Tooltip("Allows the use of the touchpad to change length of the ski stick while grabbed")]
        private bool _lengthControlsActive = false;

        public bool lengthCalibrationClickActive = true;

        [SerializeField, FormerlySerializedAs("touchpadDeltaToLengthAdjust")]
        private AnimationCurve _touchpadDeltaToLengthAdjust = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [MinMax, SerializeField, FormerlySerializedAs("lengthRange")]
        private Vector2 _lengthRange = new Vector2(0.5f, 1.5f);

        [SerializeField, FormerlySerializedAs("lengthAdjustMaxDelta")]
        private float _lengthAdjustMaxDelta = 0.5f;

        [SerializeField, FormerlySerializedAs("stickShaftToRescale")]
        private Transform _stickShaftToRescale = null;

        [Header("Drop when far from hand")]
        [SerializeField]
        private float _HAXDistanceBeforeReequip = 1f;

        [Header("Multipliers")]
        [SerializeField, Range(0.1f, 100f)]
        private float _velocityMultiplier = 1.5f;

        [SerializeField, Range(0.1f, 100f)]
        private float _angularVelocityMultiplier = 1f;

        [Header("Physic materials")]
        // nofriction
        public PhysicMaterial grabbedPhysicMaterial;

        // regular ski stick
        public PhysicMaterial releasedPhysicMaterial;

        private Vector2 _touchpadPressStart;
        private float _stickLengthOnPress;
        private IPlayer _skier;

        private GrabPointComponent _grabPoint;
        public GrabPointComponent grabPoint
        {
            get
            {
                if (_grabPoint == null)
                {
                    _grabPoint = GetComponentInChildren<GrabPointComponent>();
                }
                return _grabPoint;
            }
        }


        public float stickLength
        {
            get
            {
                return _stickShaftToRescale.localScale.z;
            }

            set
            {
                var s = _stickShaftToRescale.localScale;
                s.z = value;
                _stickShaftToRescale.localScale = s;
            }
        }

        public IPlayer skier
        {
            get { return _skier; }
        }

        public float velocityMultiplier
        {
            get { return _velocityMultiplier; }
        }

        public float angularVelocityMultiplier
        {
            get { return _angularVelocityMultiplier; }
        }

        protected override void Awake()
        {
            base.Awake();

            if (_stickShaftToRescale == null)
            {
                _lengthControlsActive = false;
                Debug.LogError(this.ToString() + " is missing a stick shaft to rescale reference");
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();

            GameServices.messageBus.Subscribe<VRInputTouch>(this);
            GameServices.messageBus.Subscribe<VRInputTouchDown>(this);
        }

        private void OnDisable()
        {
            GameServices.messageBus.Unsubscribe<VRInputTouch>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouchDown>(this);
        }

        private void Update()
        {
            if (isEquipped)
            {
                var dist = hand.position - grabPoint.transform.position;
                if (dist.sqrMagnitude > _HAXDistanceBeforeReequip * _HAXDistanceBeforeReequip)
                {
                    hand.Unequip(this, Vector3.zero, Vector3.zero);
                    hand.Equip(this);
                }

                if (lengthCalibrationClickActive)
                {
                    if (InputVR.GetPressDown(hand.isLeft, InputVR.ButtonMask.Touchpad) || InputVR.Oculus.GetPressDown(hand.isLeft, InputVR.OculusButtonMask.OculusA))
                    {
                        // calibrate length from here to playspace ground.... how can it be made more intuitive...? only do it when rotation is towards ground?
                        var calLength = hand.localPosition.y;
                        stickLength = calLength;
                    }
                }
            }
        }

        public override void OnEquipped(IVRHand hand)
        {
            base.OnEquipped(hand);
            _skier = (IPlayer)hand.player;
            foreach (var c in colliders)
            {
                c.material = grabbedPhysicMaterial;
            }
        }

        public override void OnUnequipped(IVRHand hand)
        {
            base.OnUnequipped(hand);
            _skier = null;
            foreach (var c in colliders)
            {
                c.material = releasedPhysicMaterial;
            }
        }

        public void Handle(VRInputTouchDown message)
        {
            var netPlayer = (INetPlayer)player;
            if (!_lengthControlsActive || !_isEquipped || message.input != VRInput.Touchpad || !ReferenceEquals(_hand, message.hand) || netPlayer == null || (netPlayer != null && !netPlayer.isLocal)/* _skier == null || !_skier.isLocal*/)
            {
                return;
            }

            _touchpadPressStart = message.hand.controller.GetAxis();
            _stickLengthOnPress = this.stickLength;
        }

        public void Handle(VRInputTouch message)
        {
            var netPlayer = (INetPlayer)player;
            if (!_lengthControlsActive || !_isEquipped || message.input != VRInput.Touchpad || !ReferenceEquals(_hand, message.hand) || netPlayer == null || (netPlayer != null && !netPlayer.isLocal)/* _skier == null || !_skier.isLocal*/)
            {
                return;
            }

            var delta = message.hand.controller.GetAxis() - _touchpadPressStart;
            var deltaMapped = _touchpadDeltaToLengthAdjust.Evaluate(Mathf.Abs(delta.y)) * Mathf.Sign(delta.y) * _lengthAdjustMaxDelta;
            this.stickLength = Mathf.Clamp(_stickLengthOnPress + deltaMapped, _lengthRange.x, _lengthRange.y);
        }
    }
}