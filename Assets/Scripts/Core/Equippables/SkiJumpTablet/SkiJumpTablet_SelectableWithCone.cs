﻿namespace Core
{
    using UnityEngine;
    using UnityEngine.Serialization;
    using UnityEngine.UI;

    [RequireComponent(typeof(Image))]
    public sealed class SkiJumpTablet_SelectableWithCone : MonoBehaviour
    {
        // public enum types
        // public type selectableType
        [SerializeField, FormerlySerializedAs("selectionType")]
        private SkiJumpTablet.Selection _selectionType = SkiJumpTablet.Selection.None;

        [SerializeField, FormerlySerializedAs("selected")]
        private Sprite _selected = null;

        [SerializeField, FormerlySerializedAs("notSelected")]
        private Sprite _notSelected = null;

        private Image _image;

        public SkiJumpTablet.Selection selectionType
        {
            get { return _selectionType; }
        }

        private void Awake()
        {
            _image = this.GetComponent<Image>();
        }

        public void SetSelected(bool v)
        {
            _image.sprite = v ? _selected : _notSelected;
        }
    }
}