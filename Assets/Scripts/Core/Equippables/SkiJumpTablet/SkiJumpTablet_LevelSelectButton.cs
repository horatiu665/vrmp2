namespace Core
{
    using UnityEngine;
    using UnityEngine.Serialization;
    using UnityEngine.UI;

    public sealed class SkiJumpTablet_LevelSelectButton : MonoBehaviour
    {
        [SerializeField, FormerlySerializedAs("printLevelNumber")]
        private bool _printLevelNumber = true;

        [SerializeField, FormerlySerializedAs("playLevelFromBuildSettings")]
        private int _playLevelFromBuildSettings = 0;

        [SerializeField]
        private Sprite _levelGraphics = null;

        private Text _text;
        private Vector3 _initScale;
        private SkiJumpTablet_LevelSelect _ls;

        public int playLevelFromBuildSettings
        {
            get { return _playLevelFromBuildSettings; }
        }

        private void Awake()
        {
            _initScale = this.transform.localScale;
        }

        private void OnEnable()
        {
            _ls = this.GetComponentInParent<SkiJumpTablet_LevelSelect>();
            _ls.buttons.Add(this);

            if (_printLevelNumber)
            {
                _text = this.GetComponentInChildren<Text>();
                if (_text != null)
                {
                    _text.text = "Level " + _ls.buttons.Count;
                }
            }

            if (_levelGraphics != null)
            {
                var images = GetComponentsInChildren<Image>();
                for (int i = 0; i < images.Length; i++)
                {
                    if (!images[i].GetComponent<Mask>())
                    {
                        images[i].sprite = _levelGraphics;
                    }
                }
            }
        }

        private void OnDisable()
        {
            _ls.buttons.Remove(this);
        }

        public void SetVisibility(int buttonIndex, float indexContinuous)
        {
            float relIndex = buttonIndex - indexContinuous;
            // special case: buttonIndex == 0 or maxIndex-1
            // relative button index goes from 0 to maxIndex, and we want to show button 0 looped with button maxIndex.
            // so we evaluate relativeButtonIndex as well as relativeButtonIndex + maxIndex and -maxIndex.
            var scaleFactor = _ls.scaleButtonCurve.Evaluate(relIndex);
            var posFactor = _ls.positionButtonCurve.Evaluate(relIndex);
            if (buttonIndex == 0 && indexContinuous > 1)
            {
                scaleFactor = _ls.scaleButtonCurve.Evaluate(relIndex + _ls.maxIndex);
                posFactor = _ls.positionButtonCurve.Evaluate(relIndex + _ls.maxIndex);
            }
            else if (buttonIndex == _ls.maxIndex - 1 && indexContinuous < _ls.maxIndex - 2)
            {
                scaleFactor = _ls.scaleButtonCurve.Evaluate(relIndex - _ls.maxIndex);
                posFactor = _ls.positionButtonCurve.Evaluate(relIndex - _ls.maxIndex);
            }

            this.transform.localScale = _initScale * (scaleFactor);
            var pos = transform.localPosition;
            pos.x = posFactor * _ls.positionButtonCurveScale;
            this.transform.localPosition = pos;
        }

        public void SetTextColor(Color v)
        {
            if (_text != null)
            {
                _text.color = v;
            }
        }
    }
}