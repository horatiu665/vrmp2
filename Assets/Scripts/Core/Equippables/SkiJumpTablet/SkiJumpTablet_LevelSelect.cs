namespace Core
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.Serialization;
    using UnityEngine.UI;
    using VRCore;

    public class SkiJumpTablet_LevelSelect : MonoBehaviour
    {
        private readonly IList<SkiJumpTablet_LevelSelectButton> _buttons = new List<SkiJumpTablet_LevelSelectButton>();

        [SerializeField, FormerlySerializedAs("positionButtonCurve")]
        private AnimationCurve _positionButtonCurve = AnimationCurve.Linear(0f, 0f, 1f, 0f);

        [SerializeField, FormerlySerializedAs("positionButtonCurveScale")]
        private float _positionButtonCurveScale = 0.05f;

        [SerializeField, FormerlySerializedAs("scaleButtonCurve")]
        private AnimationCurve _scaleButtonCurve = new AnimationCurve(new Keyframe(-1f, 0f), new Keyframe(0f, 1f), new Keyframe(1f, 0f));

        [SerializeField, FormerlySerializedAs("returnToSnapPosSpeed")]
        private float _returnToSnapPosSpeed = 0.25f;

        [Header("Back Button")]
        [SerializeField, FormerlySerializedAs("backButton")]
        private Text _backButton = null;

        [SerializeField, FormerlySerializedAs("backButtonOn")]
        private Color _backButtonOn = Color.white;

        [SerializeField, FormerlySerializedAs("backButtonOff")]
        private Color _backButtonOff = Color.yellow;

        private float _indexContinuous = 0;
        private Vector2 _touchInput;
        private Vector2 _oldTouchInput;
        private bool _returnToSnapPos = true;
        private bool _loadingLevel = false;
        private bool _backButtonSelected = false;

        private int currentIndex
        {
            get { return Mathf.RoundToInt(_indexContinuous); }
        }

        public int maxIndex
        {
            get { return _buttons.Count; }
        }

        public AnimationCurve scaleButtonCurve
        {
            get { return _scaleButtonCurve; }
        }

        public AnimationCurve positionButtonCurve
        {
            get { return _positionButtonCurve; }
        }

        public float positionButtonCurveScale
        {
            get { return _positionButtonCurveScale; }
        }

        public IList<SkiJumpTablet_LevelSelectButton> buttons
        {
            get { return _buttons; }
        }

        // called by SkiJumpTablet when touchpad is clicked with the level select menu active
        public void ClickTouchpad(SkiJumpTablet skiJumpTablet)
        {
            if (!_loadingLevel)
            {
                if (_backButtonSelected)
                {
                    skiJumpTablet.SetMode(SkiJumpTablet.TabletMode.Menu);
                }
                else
                {
                    _loadingLevel = true;
                    // move to the current selected level
                    var level = _buttons[currentIndex].playLevelFromBuildSettings;
                    //SceneManager.LoadScene(level);
                    ToiletTeleporterManager.instance.TeleportPlayerToToilet(PlayerManager.GetLocalPlayer<IVRPlayer>(), level);
                    _loadingLevel = false;
                    //SteamVR_LoadLevel.Begin(SceneManager.GetSceneAt(level).name); // TODO: HANDLE FOR MULTIPLAYER
                }
            }
        }

        // happens in update while menu is active
        public void PageUpdate(SkiJumpTablet skiJumpTablet)
        {
            if (skiJumpTablet.hand.controller != null)
            {
                var c = skiJumpTablet.hand.controller;
                _touchInput = c.GetAxis();
                if (c.GetTouchDown(VRCore.VRInput.Touchpad))
                {
                    // start swipe
                    _returnToSnapPos = false;
                    _oldTouchInput = _touchInput;
                }
                else if (c.GetTouchUp(VRCore.VRInput.Touchpad))
                {
                    _returnToSnapPos = true;
                }
                else if (c.GetTouch(VRCore.VRInput.Touchpad))
                {
                    var swipeDelta = _touchInput - _oldTouchInput;
                    _indexContinuous -= swipeDelta.x;
                    if (_indexContinuous > maxIndex - 0.51f) // TODO: FACTOR OUT MAGIC NUMBERS
                    {
                        _indexContinuous -= maxIndex;
                    }
                    else if (_indexContinuous < -0.49f) // TODO: FACTOR OUT MAGIC NUMBERS
                    {
                        _indexContinuous += maxIndex;
                    }

                    _oldTouchInput = _touchInput;
                }
            }
        }

        private void Update()
        {
            // snap list to int (a button is always in the center)
            if (_returnToSnapPos)
            {
                _indexContinuous = Mathf.Lerp(_indexContinuous, currentIndex, _returnToSnapPosSpeed);
                if (Mathf.Abs(_indexContinuous - currentIndex) < 0.001f) // TODO: FACTOR OUT MAGIC NUMBERS
                {
                    _indexContinuous = currentIndex;
                }
            }

            // set visibility of all buttons based on indexContinuous
            SetButtonVisibility(_indexContinuous);

            // also consider the "back" button if we click on the bottom of the touchpad
            _backButtonSelected = (Vector3.Angle(_touchInput, Vector3.down) < 30 && _touchInput.sqrMagnitude > (0.5f * 0.5f)); // TODO: FACTOR OUT MAGIC NUMBERS
            SetBackButtonSelected(_backButtonSelected);
        }

        private void SetButtonVisibility(float indexContinuous)
        {
            var count = _buttons.Count;
            for (int i = 0; i < count; i++)
            {
                _buttons[i].SetVisibility(i, indexContinuous);
            }
        }

        private void SetBackButtonSelected(bool enabled)
        {
            _backButton.color = enabled ? _backButtonOn : _backButtonOff;
            var count = _buttons.Count;
            for (int i = 0; i < count; i++)
            {
                if (currentIndex == i)
                {
                    _buttons[i].SetTextColor(!enabled ? _backButtonOn : _backButtonOff);
                }
                else
                {
                    _buttons[i].SetTextColor(_backButtonOff);
                }
            }
        }
    }
}