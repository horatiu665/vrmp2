namespace Core
{
    using System;
    using Score;
    using UnityEngine;
    using UnityEngine.UI;

    [RequireComponent(typeof(RectTransform))]
    public sealed class SkiJumpTablet_GameModeScoresPage : MonoBehaviour
    {
        [SerializeField]
        private GameMode _gameMode = GameMode.Points;

        [SerializeField]
        private Text _totalScoreField = null;

        [Space]
        [SerializeField]
        private ScoreTypeAndText[] _textFields = new ScoreTypeAndText[1];

        public GameMode gameMode
        {
            get { return _gameMode; }
        }

        private void Awake()
        {
            if (_totalScoreField == null)
            {
                throw new ArgumentNullException("_totalScoreField");
            }

            if (_textFields.Length == 0)
            {
                throw new ArgumentNullException("_textFields");
            }
        }

        private Text GetText(ScoreType type)
        {
            for (int i = 0; i < _textFields.Length; i++)
            {
                if (_textFields[i].type == type)
                {
                    return _textFields[i].text;
                }
            }

            return null;
        }

        private void UpdateTotalScore(IPlayer skier)
        {
            _totalScoreField.text = ScorePointsManager.instance.GetPlayerTotalScore(skier).ToString();
        }

        public void SetScore(IPlayer player, ScoreType type, bool updateTotalScore)
        {
            var text = GetText(type);
            if (text == null)
            {
                Debug.LogWarning(this.ToString() + " is missing a text field for the score type == " + type.ToString());
                return;
            }

            var score = ScorePointsManager.instance.GetPlayerScore(player, type);
            var count = score != null ? score.Count : 0; // TODO: Do we still want to show counts of scores rather than actual scores?

            if (type == ScoreType.JumpScore)
            {
                var maxAirTime = 0f;
                for (int i = 0; i < count; i++)
                {
                    if (score[i] > maxAirTime)
                    {
                        maxAirTime = score[i];
                    }
                }

                text.text = maxAirTime.ToString();
            }
            else
            {
                text.text = count.ToString();
            }

            if (updateTotalScore)
            {
                UpdateTotalScore(player);
            }
        }

        public void UpdateScores(IPlayer player)
        {
            for (int i = 0; i < _textFields.Length; i++)
            {
                SetScore(player, _textFields[i].type, false);
            }

            UpdateTotalScore(player);
        }

        [Serializable]
        private class ScoreTypeAndText
        {
            public ScoreType type = ScoreType.None;
            public Text text = null;
        }
    }
}