namespace Core
{
    using System;
    using Apex.Services;
    using Score;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.UI;
    using VRCore;
    using VRCore.VRNetwork;

    public sealed class SkiJumpTablet : NetEquippableSpawnableBase, IHandleMessage<VRInputPressDown>, IHandleMessage<ScoreMessage>
    {
        [Header("Tablet UI")]
        [SerializeField]
        private Transform _graphicsParent = null;

        [SerializeField]
        private Transform _pointer = null;

        [Header("Tablet Menu Pages")]
        [SerializeField]
        private Transform _menu = null;

        [SerializeField]
        private SkiJumpTablet_LevelSelect _selectLevelScript = null;

        [SerializeField]
        private SkiJumpTablet_Help _helpPageScript = null;

        [Header("Text Fields")]
        [SerializeField]
        private Text _realTimeDisplay = null;

        [SerializeField]
        private Text _timeElapsedDisplay = null;

        [Header("Debugging")]
        [SerializeField, ReadOnly]
        private TabletMode _currentMode = TabletMode.None;

        [SerializeField, ReadOnly]
        private Selection _selection = Selection.None;

        private SkiJumpTablet_SelectableWithCone[] _selectableItems;
        private SkiJumpTablet_GameModeScoresPage[] _scores;
        private DateTime _startTime;

        public enum Selection
        {
            None,
            ResetLevel,
            ShowScores,
            SelectLevel,
            Help
        }

        public enum TabletMode
        {
            None,
            Menu,
            Scores,
            SelectLevel,
            Help
        }

        private Transform _selectLevel
        {
            get { return _selectLevelScript.transform; }
        }

        private Transform _helpPage
        {
            get
            {
                return _helpPageScript.transform;
            }
        }

        public override void OnEquipped(IVRHand hand)
        {
            base.OnEquipped(hand);
            SetMode(TabletMode.Menu);
            GameServices.messageBus.Subscribe<ScoreMessage>(this);
        }

        public override void OnUnequipped(IVRHand hand)
        {
            base.OnUnequipped(hand);
            HideTablet();
            GameServices.messageBus.Unsubscribe<ScoreMessage>(this);
        }

        protected override void OnEnable()
        {
            base.OnEnable();
        
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            _startTime = DateTime.Now.ToLocalTime();

            _scores = this.GetComponentsInChildren<SkiJumpTablet_GameModeScoresPage>(true);
            if (_scores == null || _scores.Length == 0)
            {
                Debug.LogError(this.ToString() + " could not find any SkiJumpTablet_GameModeScoresPage in children!");
            }

            _selectableItems = this.GetComponentsInChildren<SkiJumpTablet_SelectableWithCone>();
            if (_selectableItems == null || _selectableItems.Length == 0)
            {
                Debug.LogError(this.ToString() + " could not find any SkiJumpTablet_SelectableWithCone in children!");
            }

            _selectLevelScript = _selectLevelScript ?? this.GetComponentInChildren<SkiJumpTablet_LevelSelect>();
            if (_selectLevelScript == null)
            {
                Debug.LogError(this.ToString() + " could not find a SkiJumpTablet_LevelSelect in children!");
            }

            _helpPageScript = _helpPageScript ?? this.GetComponentInChildren<SkiJumpTablet_Help>();
            if (_helpPageScript == null)
            {
                Debug.LogError(this.ToString() + " could not find a SkiJumpTablet_Help in children!");
            }
        }

        private void OnDisable()
        {
            GameServices.messageBus.Unsubscribe<VRInputPressDown>(this);
            GameServices.messageBus.Unsubscribe<ScoreMessage>(this);
        }

        private void Update()
        {
            if (NetServices.isServer)
            {
                return;
            }
            if (!_isEquipped)
            {
                return;
            }

            if (_currentMode != TabletMode.None)
            {
                var now = DateTime.Now.ToLocalTime();
                _realTimeDisplay.text = string.Concat(now.Hour.ToString(), ":", now.Minute.ToString("00"));

                var elapsed = now - _startTime;
                _timeElapsedDisplay.text = string.Format("{0:00}:{1:00}:{2:000}", elapsed.Minutes, elapsed.Seconds, elapsed.Milliseconds);

                if (_currentMode == TabletMode.Menu)
                {
                    RotateConePointer(_hand.controller.GetAxis());
                }
                else if (_currentMode == TabletMode.SelectLevel)
                {
                    _selectLevelScript.PageUpdate(this);
                }
                else if (_currentMode == TabletMode.Help)
                {
                    _helpPageScript.PageUpdate(this);
                }
            }
        }

        private void Toggle()
        {
            if (_currentMode == TabletMode.None)
            {
                // tablet is not shown currently
                SetMode(TabletMode.Menu);
            }
            else if (_currentMode == TabletMode.Menu)
            {
                // tablet is currently in menu mode, so "quit"
                HideTablet();
            }
            else if (_currentMode == TabletMode.Scores)
            {
                // tablet is currently in scores mode, so go back to menu
                SetMode(TabletMode.Menu);
            }
            else if (_currentMode == TabletMode.SelectLevel)
            {
                SetMode(TabletMode.Menu);
            }
            else if (_currentMode == TabletMode.Help)
            {
                SetMode(TabletMode.Menu);
            }
        }

        public void SetMode(TabletMode mode)
        {
            // set graphics parent
            _graphicsParent.gameObject.SetActive(mode != TabletMode.None);

            var currentGameMode = GameModeManager.instance.currentMode;
            var showScores = mode == TabletMode.Scores;
            for (int i = 0; i < _scores.Length; i++)
            {
                _scores[i].gameObject.SetActive(showScores && _scores[i].gameMode == currentGameMode);
            }

            _menu.gameObject.SetActive(mode == TabletMode.Menu);
            _selectLevel.gameObject.SetActive(mode == TabletMode.SelectLevel);
            _helpPage.gameObject.SetActive(mode == TabletMode.Help);

            // set any custom stuff for each tablet mode
            switch (mode)
            {
                case TabletMode.None:
                {
                    // Move tablet to a location below all players to avoid the tablet being equipped through normal equipping (picking up)
                    this.position += Vector3.down * 1000000f; // TODO: magic number
                    break;
                }

                case TabletMode.Menu:
                {
                    break;
                }

                case TabletMode.Scores:
                {
                    SetScoreTexts();
                    break;
                }

                case TabletMode.SelectLevel:
                {
                    break;
                }
            }

            // set main menu selection
            _selection = Selection.None;

            // keep track of current tablet mode
            _currentMode = mode;
        }

        private void HideTablet()
        {
            SetMode(TabletMode.None);
        }

        private void SetScoreTexts()
        {
            var skier = (IPlayer)_hand.player;
            if (skier == null)
            {
                Debug.LogError(this.ToString() + " cannot set score texts with no valid reference to the skier");
                return;
            }

            var currentGameMode = GameModeManager.instance.currentMode;
            for (int i = 0; i < _scores.Length; i++)
            {
                if (_scores[i].gameMode == currentGameMode)
                {
                    _scores[i].UpdateScores(skier);
                    break;
                }
            }
        }

        private void RotateConePointer(Vector2 touchPadAxis)
        {
            var pos = new Vector3(touchPadAxis.x, touchPadAxis.y);

            if (pos.sqrMagnitude > 0f)
            {
                _pointer.localRotation = Quaternion.LookRotation(pos);

                int closestSelectable = -1;
                float minAngle = float.MaxValue;
                // find closest selectableItem from list, and select it
                for (int i = 0; i < _selectableItems.Length; i++)
                {
                    var selPosDir = _selectableItems[i].transform.localPosition;
                    var angle = Vector3.Angle(pos, selPosDir);
                    if (minAngle > angle)
                    {
                        minAngle = angle;
                        closestSelectable = i;
                    }
                }

                // if any selectable found, select it
                if (closestSelectable != -1)
                {
                    _selection = _selectableItems[closestSelectable].selectionType;
                }
                else
                {
                    _selection = Selection.None;
                }

                // set all other selectables to be deselected
                for (int i = 0; i < _selectableItems.Length; i++)
                {
                    // the selectable items handle their own selection graphics
                    _selectableItems[i].SetSelected(i == closestSelectable);
                }
            }
        }

        public void Handle(VRInputPressDown message)
        {
            if (!_isEquipped || message.input != VRInput.Touchpad || message.isLeft != _hand.isLeft)
            {
                return;
            }

            //// Debug.Log("Tablet pressed. Mode: " + _currentMode.ToString() + ", selection: " + _selection.ToString(), gameObject);

            if (_currentMode == TabletMode.Menu)
            {
                switch (_selection)
                {
                    case Selection.None:
                    {
                        Toggle();
                        break;
                    }

                    case Selection.ResetLevel:
                    {
                        HideTablet();
                        if (NetServices.isClient)
                        {
                            // For multiplayer you cannot simply reload scene, instead move player back to start of slope
                            var player = (IPlayer)_hand.player;
                            //player.Teleport(player.startPosition);
                            // TELEPORT PLAYER IN SOME OTHER WAY. OR JUST GET RID OF THE FUCKING TABLET
                        }
                        else
                        {
                            SteamVR_LoadLevel.Begin(SceneManager.GetActiveScene().name, true);
                        }

                        break;
                    }

                    case Selection.ShowScores:
                    {
                        SetMode(TabletMode.Scores);
                        break;
                    }

                    case Selection.SelectLevel:
                    {
                        SetMode(TabletMode.SelectLevel);
                        break;
                    }

                    case Selection.Help:
                    {
                        SetMode(TabletMode.Help);
                        break;
                    }
                }
            }
            else if (_currentMode == TabletMode.Scores)
            {
                Toggle();
            }
            else if (_currentMode == TabletMode.SelectLevel)
            {
                // do level selection shit
                _selectLevelScript.ClickTouchpad(this);
            }
            else if (_currentMode == TabletMode.Help)
            {
                // send clicks to help script
                _helpPageScript.ClickTouchpad(this);
            }
        }

        public void Handle(ScoreMessage message)
        {
            var player = message.player;
            if (_hand == null || player != _hand.player)
            {
                // not for the player equipping this tablet
                return;
            }

            var scoreType = message.scoreType;
            var currentGameMode = GameModeManager.instance.currentMode;
            for (int i = 0; i < _scores.Length; i++)
            {
                if (_scores[i].gameMode == currentGameMode)
                {
                    _scores[i].SetScore(player, scoreType, true);
                    break;
                }
            }
        }
    }
}