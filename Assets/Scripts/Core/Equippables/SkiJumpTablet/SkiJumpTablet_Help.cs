namespace Core
{
    using UnityEngine;

    public class SkiJumpTablet_Help : MonoBehaviour
    {
        [SerializeField]
        private float _maxScrollDistance = 1f;

        [SerializeField]
        private bool loopScroll = false;

        private float _indexContinuous = 0;
        private Vector2 _touchInput;
        private Vector2 _oldTouchInput;

        /// called by SkiJumpTablet when touchpad is clicked with the level select menu active
        public void ClickTouchpad(SkiJumpTablet skiJumpTablet)
        {
            skiJumpTablet.SetMode(SkiJumpTablet.TabletMode.Menu);
        }

        /// happens in update while menu is active
        public void PageUpdate(SkiJumpTablet skiJumpTablet)
        {
            if (skiJumpTablet.hand.controller != null)
            {
                var c = skiJumpTablet.hand.controller;
                _touchInput = c.GetAxis();
                if (c.GetTouchDown(VRCore.VRInput.Touchpad))
                {
                    // start swipe
                    _oldTouchInput = _touchInput;
                }
                else if (c.GetTouchUp(VRCore.VRInput.Touchpad))
                {
                }
                else if (c.GetTouch(VRCore.VRInput.Touchpad))
                {
                    var swipeDelta = _touchInput - _oldTouchInput;
                    _indexContinuous -= swipeDelta.y;
                    if (loopScroll)
                    {
                        // 0.51f is "a bit more than half"
                        if (_indexContinuous > _maxScrollDistance - 0.51f) // TODO: FACTOR OUT MAGIC NUMBERS
                        {
                            _indexContinuous -= _maxScrollDistance;
                        }
                        else if (_indexContinuous < -0.49f) // TODO: FACTOR OUT MAGIC NUMBERS
                        {
                            _indexContinuous += _maxScrollDistance;
                        }
                    }

                    _oldTouchInput = _touchInput;
                }
            }
        }
    }
}