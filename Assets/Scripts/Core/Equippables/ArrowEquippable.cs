namespace Core
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    public sealed class ArrowEquippable : NetEquippableSpawnableBase, IHaveThrowMultipliers
    {
        [SerializeField]
        private float _velocityMultiplier = 2f;

        [SerializeField]
        private float _angularVelocityMultiplier = 1.5f;
        
        public float velocityMultiplier
        {
            get
            {
                return _velocityMultiplier;
            }
        }

        public float angularVelocityMultiplier
        {
            get
            {
                return _angularVelocityMultiplier;
            }
        }

        protected override void Awake()
        {
            base.Awake();
        }
        
    }
}