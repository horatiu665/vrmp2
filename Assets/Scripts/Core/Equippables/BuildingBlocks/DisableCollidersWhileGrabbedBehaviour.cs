namespace Core
{
    using UnityEngine;
    using VRCore;

    /// <summary>
    /// TODO: right now it's not possible to put it on the player, because of the shitty way I made the _cols reference.
    /// </summary>
    public class DisableCollidersWhileGrabbedBehaviour : VRCustomBehaviourBase
    {
        private Collider[] _cols;

        public override VRBehaviourCapabilities capabilities
        {
            get
            {
                return VRBehaviourCapabilities.Equip | VRBehaviourCapabilities.Unequip;
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _cols = GetComponentsInChildren<Collider>();
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            for (int i = 0; i < _cols.Length; i++)
            {
                _cols[i].enabled = false;
            }
        }

        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            for (int i = 0; i < _cols.Length; i++)
            {
                _cols[i].enabled = true;
            }
        }
    }
}