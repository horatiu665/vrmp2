namespace Core
{
    using UnityEngine;
    using VRCore;

    /// <summary>
    /// TODO: right now it's not possible to put it on the player, because of the shitty way I made the _thisEquippable reference.
    /// </summary>
    public class GridUnequipBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private float gridSize = 1f;

        private IVREquippable _thisEquippable;
        private Vector3 _handOffset;

        public override VRBehaviourCapabilities capabilities
        {
            get
            {
                return VRBehaviourCapabilities.Unequip | VRBehaviourCapabilities.Equip;
            }
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var r = input.equippable.rigidbody;
            r.isKinematic = true;

            _handOffset = r.transform.position - input.hand.position;
        }

        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            var pos = transform.position;
            // implement Mathf.Round but instead of ints, use multiples of gridSize.
            pos = pos / gridSize;
            pos = new Vector3(Mathf.Round(pos.x), Mathf.Round(pos.y), Mathf.Round(pos.z));
            pos = pos * gridSize;
            transform.position = pos;
            transform.rotation = Quaternion.identity;

            input.equippable.rigidbody.isKinematic = true;
        }

        private void Awake()
        {
            _thisEquippable = GetComponent<IVREquippable>();
        }

        private void Update()
        {
            if (_thisEquippable.isEquipped)
            {
                // rotate to identity and move to the same offset to the hand as when first equipping
                transform.rotation = Quaternion.identity;
                transform.position = _thisEquippable.hand.position + _handOffset;
            }
        }
    }
}