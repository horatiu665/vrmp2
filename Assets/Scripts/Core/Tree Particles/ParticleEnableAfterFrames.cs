using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ParticleEnableAfterFrames : MonoBehaviour
{
    public GameObject target;
    public int delayFrames = 1;

    void OnEnable()
    {
        StartCoroutine(pTween.WaitFrames(delayFrames, () =>
        {
            target.SetActive(true);
        }));
    }

}