using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class ForestSpawnMeshes : MonoBehaviour
{
    public MeshRenderer treeToClone;

    public int count = 10;

    public int treesPerFrame = 10;

    public float range = 100f;

    [HideInInspector]
    [SerializeField]
    private GeneratedMeshData generatedMeshes;
    
    private void Awake()
    {
        gameObject.GetOrAddComponent<MeshRenderer>();
    }

    [DebugButton]
    void CloneTreesDebug()
    {
        StartCoroutine(CloneTreesCoroutine());
    }

    IEnumerator CloneTreesCoroutine()
    {
        var treeToCloneData = new TreeToCloneData(treeToClone, treeToClone.GetComponent<MeshFilter>().sharedMesh);

        var vCount = treeToCloneData.vertexCount;
        var tCount = treeToCloneData.triangleCount;

        var n = count;
        var treesForYield = 0;

        generatedMeshes = new GeneratedMeshData(treeToClone, transform);

        while (n > 0)
        {
            // how many trees can we fit into this Mesh?
            // Mathf.Floor(65535 / (amount of vertices in one tree))
            var subCount = Mathf.Min(65535 / treeToCloneData.vertexCount, n);
            var meshCreationData = new MeshCreationData(vCount, tCount, subCount);

            for (int i = 0; i < subCount; i++)
            {
                var pos = Random.onUnitSphere * Random.Range(0, range);
                var rot = Quaternion.Euler(transform.eulerAngles + Vector3.up * Random.Range(0, 360f));
                SpawnOneTree(pos, rot, meshCreationData, treeToCloneData);

                treesForYield++;
                if (treesForYield >= treesPerFrame)
                {
                    treesForYield = 0;
                    yield return 0;
                }
            }

            // make new mesh filter for the new mesh
            generatedMeshes.AddMesh(meshCreationData.GetMesh());

            n -= subCount;
        }

        yield break;
    }
    
    /// <summary>
    /// Must call StartSpawning() before, and FinishSpawning() after!!!
    /// </summary>
    private void SpawnOneTree(Vector3 pos, Quaternion rot, MeshCreationData meshCreationData, TreeToCloneData treeToCloneData)
    {
        var vertexIndexOffset = meshCreationData.lastVertexIndexFilled;
        var triangleIndexOffset = meshCreationData.lastTriangleIndexFilled;
        for (int i = 0; i < treeToCloneData.vertices.Length; i++)
        {
            meshCreationData.vertices[vertexIndexOffset + i] = (rot * treeToCloneData.vertices[i] + pos);
        }

        for (int i = 0; i < treeToCloneData.triangles.Length; i++)
        {
            meshCreationData.triangles[triangleIndexOffset + i] = (treeToCloneData.triangles[i] + vertexIndexOffset);
        }

        for (int i = 0; i < treeToCloneData.normals.Length; i++)
        {
            meshCreationData.normals[vertexIndexOffset + i] = (rot * treeToCloneData.normals[i]);
        }

        for (int i = 0; i < treeToCloneData.uv.Length; i++)
        {
            meshCreationData.uv[vertexIndexOffset + i] = (treeToCloneData.uv[i]);
        }

        meshCreationData.lastVertexIndexFilled += treeToCloneData.vertexCount;
        meshCreationData.lastTriangleIndexFilled += treeToCloneData.triangleCount;

    }

    [DebugButton]
    void ClearMeshes()
    {
        if (generatedMeshes != null)
        {
            generatedMeshes.ClearMeshes();
        }
    }

    // used to pass along the multiple meshes.
    public class TreeToCloneData
    {
        public MeshRenderer treeToClone;
        public Mesh treeMesh;
        public Vector3[] vertices;
        public int[] triangles;
        public Vector3[] normals;
        public Vector2[] uv;
        public int vertexCount;
        public int triangleCount;

        public TreeToCloneData(MeshRenderer treeToClone, Mesh treeMesh)
        {
            this.treeToClone = treeToClone;
            this.treeMesh = treeMesh;

            this.vertices = treeMesh.vertices;
            this.triangles = treeMesh.triangles;
            this.normals = treeMesh.normals;
            this.uv = treeMesh.uv;

            vertexCount = vertices.Length;
            triangleCount = triangles.Length;
        }
    }

    public class MeshCreationData
    {
        public Vector3[] normals;
        public int[] triangles;
        public Vector2[] uv;
        public Vector3[] vertices;
        public int lastVertexIndexFilled = 0, lastTriangleIndexFilled = 0;

        public MeshCreationData(int vertexCount, int triangleCount, int treeCount)
        {
            vertices = new Vector3[(vertexCount * treeCount)];
            triangles = new int[(triangleCount * treeCount)];
            normals = new Vector3[(vertexCount * treeCount)];
            uv = new Vector2[(vertexCount * treeCount)];
        }

        public Mesh GetMesh()
        {
            var m = new Mesh();
            m.vertices = vertices;
            m.triangles = triangles;
            m.normals = normals;
            m.uv = uv;

            m.RecalculateBounds();

            return m;
        }
    }

    public class GeneratedMeshData
    {
        public Transform treeMeshParent;

        public MeshRenderer treeToClone;

        public List<MeshFilter> meshFilters;

        public GeneratedMeshData(MeshRenderer treeToClone, Transform treeMeshParent)
        {
            this.treeToClone = treeToClone;
            this.treeMeshParent = treeMeshParent;
            meshFilters = new List<MeshFilter>();
        }

        public void ClearMeshes()
        {
            for (int i = 0; i < meshFilters.Count; i++)
            {
                Extensions.DestroySafe(meshFilters[i].gameObject);
            }
            meshFilters.Clear();
        }

        public void AddMesh(Mesh m)
        {
            var mf = new GameObject("[generated] Tree Group " + meshFilters.Count, typeof(MeshFilter), typeof(MeshRenderer)).GetComponent<MeshFilter>();
            mf.transform.parent = treeMeshParent;
            meshFilters.Add(mf);
            mf.sharedMesh = m;
            var mr = mf.GetComponent<MeshRenderer>();
            mr.sharedMaterial = treeToClone.sharedMaterial;
        }
    }

}