using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ParticleFixedCount : MonoBehaviour
{
    private ParticleSystem _ps;

    public int count = 650;

    public int frameDelay = 1;

    void OnEnable()
    {
        _ps = GetComponent<ParticleSystem>();
        var m = _ps.main;

        StartCoroutine(pTween.WaitFrames(frameDelay, () =>
        {
            _ps.Clear();
            m.startLifetimeMultiplier = float.MaxValue;
            _ps.Emit(count);
        }));
    }



}