#pragma warning disable 0414

namespace Testing
{
    using System;
    using Core;
    using Helpers;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    /// <summary>
    /// Can be used for simulating VR steering, without VR.
    /// </summary>
    /// <seealso cref="UnityEngine.MonoBehaviour" />
    /// <seealso cref="Helpers.IPlayerStartListener" />
    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(IPlayer), typeof(SkiDragController), typeof(SkiHeadSteeringStrafe))]
    public class CheatSteering : MonoBehaviour, IVRPlayerStartListener
    {
        [Header("If false, disables 3rd person camera and controls")]
        [SerializeField, FormerlySerializedAs("enableThirdPersonMode")]
        private bool _enableThirdPersonMode = false;

        [SerializeField]
        private KeyCode thirdPersonToggleCheatKey = KeyCode.V;

        [SerializeField]
        private KeyCode superSpeedToggle = KeyCode.CapsLock;

        [SerializeField]
        private KeyCode gravityCheatToggle = KeyCode.N;

        [SerializeField, FormerlySerializedAs("thirdPersonCameraRoot")]
        private Transform _thirdPersonCameraRoot = null;

        [SerializeField, FormerlySerializedAs("previewCamera")]
        private Camera _thirdPersonCamera = null;

        [SerializeField]
        private Camera _vrCamera = null;

        [Header("Input")]
        [SerializeField, FormerlySerializedAs("keyLeft")]
        private KeyCode _keyLeft = KeyCode.A;

        [SerializeField, FormerlySerializedAs("keyRight")]
        private KeyCode _keyRight = KeyCode.D;

        [SerializeField, FormerlySerializedAs("keyUp")]
        private KeyCode _keyUp = KeyCode.S;

        [SerializeField, FormerlySerializedAs("keyDown")]
        private KeyCode _keyDown = KeyCode.W;

        [SerializeField, FormerlySerializedAs("inputSmoothness")]
        private float _inputSmoothness = 0.1f;

        [Header("Head pos")]
        [SerializeField, ReadOnly]
        private float _currentHeadY = -1;

        [SerializeField]
        private KeyCode doubleHeadSideKey = KeyCode.LeftShift;

        [Header("Sticks")]
        [SerializeField]
        private KeyCode _boostKey = KeyCode.LeftShift;

        [SerializeField]
        private float _boostForce = 10f;

        [SerializeField]
        private float _steerSpeed = 15f;

        [SerializeField, FormerlySerializedAs("brakeMaxDrag")]
        private float _brakeMaxDrag = 10f;

        [Header("Straight up force")]
        [SerializeField, FormerlySerializedAs("keyStraightUp")]
        private KeyCode _keyStraightUp = KeyCode.E;

        [SerializeField, FormerlySerializedAs("straightUpForce")]
        private float _straightUpForce = 10f;

#if UNITY_EDITOR

        [SerializeField, ReadOnly]
        private float _currentSpeed;

#endif

        private SkiDragController _dragController;
        private SkiHeadSteeringStrafe _headSteering;
        private IPlayer _player;
        private Vector2 _input;

        private Vector2 superSpeedDragRangeSave;
        private float superSpeedDragMaxSave;

        // input in update to fixedupdate
        private Vector2 inputRaw;
        private bool doubleHeadSideInput;
        private bool boostInput;
        private bool straightUpInput;

        private void Awake()
        {
            _dragController = this.GetComponent<SkiDragController>();
            _headSteering = this.GetComponent<SkiHeadSteeringStrafe>();
        }

        private void OnEnable()
        {
            SetThirdPersonMode(_enableThirdPersonMode);

            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
        }

        private void SetThirdPersonMode(bool enableThirdPersonMode)
        {
            if (_thirdPersonCameraRoot != null)
            {
                _thirdPersonCameraRoot.gameObject.SetActive(enableThirdPersonMode);
            }

            if (_vrCamera != null)
            {
                _vrCamera.enabled = !enableThirdPersonMode;
            }

            var shitre = GetComponent<shitreset>();
            if (shitre != null)
            {
                if (enableThirdPersonMode)
                {
                    shitre.enabled = false;
                }
                else
                {
                    shitre.enabled = true;
                }
            }

        }

        public void OnPlayerStart(IVRPlayer player)
        {
            if (_player != null)
            {
                // only initialize once
                return;
            }

            _player = (IPlayer)player;
        }

        private void FixedUpdate()
        {
            if (_player == null || _thirdPersonCamera == null || !_enableThirdPersonMode)
            {
                return;
            }

            // INPUT steering
            _input = Vector2.Lerp(_input, inputRaw, _inputSmoothness);

            // cheat force based on camera is in the direction of camera.right
            var cheatForceBasedOnCamera = _thirdPersonCamera.transform.right * _input.x;

            // like project, but one float instead.
            var actualHeadRight = Vector3.Dot(cheatForceBasedOnCamera, _player.skis.right);

            if (doubleHeadSideInput)
            {
                actualHeadRight *= 2;
            }

            // from camera.right to skis.right we just need to apply dot product
            _player.AddForce(this, actualHeadRight * _player.skis.right * _steerSpeed, ForceMode.Acceleration);

            // subtract the real steer force from the head (so we control purely with cheats)
            _player.AddForce(this, -_player.skis.right * _headSteering.finalStrafeVel, ForceMode.Acceleration);

            // INPUT ski sticks boost
            if (boostInput)
            {
                boostInput = false;
                // BEWARE. FIXEDUPDATE SOMETIMES RUNS TWICE PER FRAME, THEREFORE THIS FORCE GETS APPLIED ONCE OR TWICE AT RANDOM.
                //var pvBefore = _player.velocity.magnitude;
                _player.AddForce(this, _thirdPersonCamera.transform.forward * _boostForce);
                //Debug.Log("Boost fwd, p.vDelta = " + (_player.velocity.magnitude - pvBefore).ToString("F2") + "; frame: " + Time.fixedDeltaTime);
            }

            // INPUT head height
            // head height goes down when pressing forward, because we go faster
            _currentHeadY = Mathf.Lerp(_dragController.settings.heightRange.y, _dragController.settings.heightRange.x, _input.y);

            // addthe fake head drag, and SUBTRACT THE REAL HEAD DRAG (so we control purely with cheats)
            var realHeadHeight = _player.head.position.y - _player.position.y;
            _player.AddDragNextFrame(_dragController.GetDragFromHeadHeight(_currentHeadY) - _dragController.GetDragFromHeadHeight(realHeadHeight));

            // INPUT braking
            _player.AddDragNextFrame(Mathf.Clamp01(-_input.y) * _brakeMaxDrag);

            // INPUT straight up cheat
            if (straightUpInput)
            {
                straightUpInput = false;
                // BEWARE. FIXEDUPDATE SOMETIMES RUNS TWICE PER FRAME, THEREFORE THIS FORCE GETS APPLIED ONCE OR TWICE AT RANDOM.
                //var pvBefore = _player.velocity.magnitude;
                _player.AddForce(this, Vector3.up * _straightUpForce);
                //Debug.Log("Boost upw, p.vDelta = " + (_player.velocity.magnitude - pvBefore).ToString("F2") + "; frame: " + Time.fixedDeltaTime);
            }

            // FAKE GRAPHICS
            _player.SetHeadPosition(actualHeadRight * _headSteering.maxHeadSidePosition, _currentHeadY);
            _player.head.LookAt(_player.head.position + _thirdPersonCamera.transform.forward);
        }

        private void Update()
        {
            if (Input.GetKeyDown(thirdPersonToggleCheatKey))
            {
                _enableThirdPersonMode = !_enableThirdPersonMode;
                SetThirdPersonMode(_enableThirdPersonMode);
            }

            if (Input.GetKeyDown(superSpeedToggle))
            {
                if (_enableThirdPersonMode)
                {
                    SetSuperSpeed();
                }
            }

            if (Input.GetKeyDown(gravityCheatToggle))
            {
                if (_enableThirdPersonMode)
                {
                    _player.rb.useGravity = !_player.rb.useGravity;
                }
            }

            inputRaw = new Vector2(
                (Input.GetKey(_keyLeft) ? -1 : 0) + (Input.GetKey(_keyRight) ? 1 : 0),
                (Input.GetKey(_keyDown) ? -1 : 0) + (Input.GetKey(_keyUp) ? 1 : 0));

            doubleHeadSideInput = Input.GetKey(doubleHeadSideKey);

            if (Input.GetKeyDown(_boostKey))
                boostInput = true;

            if (Input.GetKeyDown(_keyStraightUp))
                straightUpInput = true;
        }

        private void SetSuperSpeed()
        {
            if (_dragController.settings.dragRange != Vector2.zero)
            {
                superSpeedDragRangeSave = _dragController.settings.dragRange;
                superSpeedDragMaxSave = _dragController.settings.addDragMax;
                _dragController.settings.dragRange = Vector2.zero;
                _dragController.settings.addDragMax = 0;
            }
            else
            {
                _dragController.settings.dragRange = superSpeedDragRangeSave;
                _dragController.settings.addDragMax = superSpeedDragMaxSave;
            }
            _dragController.showDebugVelocity = true;

        }

#if UNITY_EDITOR

        private void LateUpdate()
        {
            if (_player != null)
            {
                _currentSpeed = _player.velocity.magnitude; // purely for displaying the current speed in the inspector for debugging
            }
        }

        private void OnValidate()
        {
            if (_thirdPersonCameraRoot != null && _enableThirdPersonMode == true ^ _thirdPersonCameraRoot.gameObject.activeSelf)
            {
                SetThirdPersonMode(_enableThirdPersonMode);
            }
        }

#endif
    }
}