using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;
using System;

public class CaveTeleporter : MonoBehaviour
{
    public CaveTeleporter otherTeleporter;

    public bool isEntrance = false;

    public Transform exitPosition;

    private void OnTriggerEnter(Collider other)
    {
        var player = other.GetPlayer<PlayerBase>();
        if (player != null)
        {
            if (isEntrance)
            {
                TeleportTo(player, this, otherTeleporter);

            }
        }
    }

    public static void TeleportTo(PlayerBase player, CaveTeleporter caveTeleporter, CaveTeleporter otherTeleporter)
    {
        // we might need to spawn the other teleporter first. but whatever for now it's just a prototype

        // local position of player must stay the same compared to the teleporter.
        player.transform.position = otherTeleporter.exitPosition.position;

        // origin shift might need to reset
        OriginShiftManager.instance.CheckFollowTargetAndMove();

    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;
        if (otherTeleporter != null)
        {
            Gizmos.DrawLine(transform.position, otherTeleporter.transform.position);
        }
        if (isEntrance)
        {
            Gizmos.DrawSphere(transform.position + Vector3.up * 100, 10f);
        }
    }
}