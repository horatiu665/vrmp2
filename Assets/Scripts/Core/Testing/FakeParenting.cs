using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class FakeParenting : MonoBehaviour
{
	public Transform target;
	public bool position, rotation;
	public bool keepInitWorldOffset = false;
	Vector3 initWorldOffset;

	void Start()
	{
		if (keepInitWorldOffset)
		{
			initWorldOffset = transform.position - target.position;
		}
	}

	void Update()
	{
		if (position)
		{
			transform.position = target.position;
			if (keepInitWorldOffset)
			{
				transform.position += initWorldOffset;
			}
		}

		if (rotation)
		{
			transform.rotation = target.rotation;
		}

	}
}