namespace Core
{
    using UnityEngine;
    using VRCore;

    public class Speedometer : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private Transform _needle;

        [SerializeField]
        private Vector2 _angleRange = new Vector2(0, 180);

        [SerializeField]
        private float _maxSpeed = 100;

        public bool realSpeedOrMaxSpeed;

        private IPlayer _player;

        private void Awake()
        {
            if (_needle == null)
            {
                _needle = transform.GetChild(0);
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
        }

        private void FixedUpdate()
        {
            var mag = (realSpeedOrMaxSpeed ? _player.rb.velocity.magnitude : _player.velocityController.curMaxVelocity) / _maxSpeed;
            _needle.localEulerAngles = Vector3.up * Mathf.LerpUnclamped(_angleRange.x, _angleRange.y, mag);
        }
    }
}