﻿namespace Core.Testing
{
    using UnityEngine;

    public sealed class TransformDirectionGizmoDebug : MonoBehaviour
    {
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawRay(new Ray(this.transform.position, this.transform.forward));

            Gizmos.color = Color.red;
            Gizmos.DrawRay(new Ray(this.transform.position, this.transform.right));

            Gizmos.color = Color.green;
            Gizmos.DrawRay(new Ray(this.transform.position, this.transform.up));
        }
    }
}
