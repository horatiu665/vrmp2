﻿using Core;
using Core.Level;
using Core.Score;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using VRCore;

public class JumpHighscoreFlag : VRPrefabBase
{
    public float globalHighscoreColorMultiplier = 1.3f;

    [Header("Setup")]
    [SerializeField]
    private Text[] _texts;

    private IPlayer player;
    private ScoreType scoreType;
    private float score;

    public Text[] texts
    {
        get
        {
            if (_texts == null)
            {
                _texts = GetComponentsInChildren<Text>();
            }
            return _texts;
        }
    }

    [SerializeField]
    private List<MeshRenderer> _renderers;
    public List<MeshRenderer> renderers
    {
        get
        {
            if (_renderers == null || _renderers.Count(r => r != null) == 0)
            {
                _renderers = GetComponentsInChildren<MeshRenderer>().ToList();
            }
            return _renderers;
        }
    }

    [SerializeField]
    private List<Collider> _colliders;
    public List<Collider> colliders
    {
        get
        {
            if (_colliders == null || _colliders.Count(r => r != null) == 0)
            {
                _colliders = GetComponentsInChildren<Collider>().ToList();
            }
            return _colliders;
        }
    }


    [Header("Juices")]
    public SmartSound soundScore;
    public SmartSound soundHighscore;

    public Color latestScoreColor = new Color(226 / 255f, 132 / 255f, 89 / 255f);

    [Header("Scale score visibility")]
    public Transform scaleOnDistance;
    private float initHeight = -1f;
    public Transform scaleOnY;
    public AnimationCurve distanceScale = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1f, 0, 0), new Keyframe(1, 5f, 0, 0) } };
    public Vector2 distanceRange = new Vector2(100f, 500f);

    [Space]
    public float enableCollisionStartDelay = 1f;

    private void OnValidate()
    {
        if (texts != null)
        {
            // DUST KICKIN
        }
        if (renderers != null)
        {
            // jack johnson
        }
    }

    private void ScorePointsManager_OnScoreAdded(ScorePointsManager.ScoreAddedEventArgs data)
    {
        if (data.scoreType == scoreType)
        {
            // use the old player color, not the player whose score just came in.
            var pColor = player.color;
            var textCol = /*(pColor.r + pColor.b + pColor.g) > 1.5f ? Color.black : */Color.white;

            bool isThisPoleHighest = false;
            if (ScorePointsManager.instance.maxPlayerScores.ContainsKey(scoreType))
            {
                // old score that we saved
                isThisPoleHighest = score == ScorePointsManager.instance.maxPlayerScores[scoreType].maxScore;
            }
            SetColors(pColor * (isThisPoleHighest ? globalHighscoreColorMultiplier : 1f), textCol);

            // now we reverted to the regular colors, remove listener for highscore shit. if this was global score, it will remain light colored. to show that this player once had the highest score ;)
            ScorePointsManager.OnScoreAdded -= ScorePointsManager_OnScoreAdded;

        }
    }

    void SetColors(Color wood, Color text)
    {
        // set color of qood
        foreach (var r in renderers)
        {
            r.material.color = wood;
        }

        // set color of all scores
        for (int i = 0; i < texts.Length; i++)
        {
            texts[i].color = text;
        }
    }

    public void SetHighscoreFirstTime(string scoreText, ScoreType scoreType, float lastScoreAdded, IPlayer player, bool isGlobalHighscore)
    {
        foreach (var t in texts)
        {
            t.text = scoreText;
        }

        this.scoreType = scoreType;
        this.score = lastScoreAdded;
        this.player = player;

        // JUICE
        if (isGlobalHighscore)
        {
            // highscore!!!!
            if (soundHighscore != null)
            {
                soundHighscore.Play();
            }
        }
        else
        {
            if (soundScore != null)
            {
                soundScore.Play();
            }
        }

        // set color here for when the pole is spawned
        SetColors(latestScoreColor, Color.white);
        ScorePointsManager.OnScoreAdded += ScorePointsManager_OnScoreAdded;
    }

    private void OnEnable()
    {
        if (scaleOnDistance != null)
        {
            initHeight = scaleOnDistance.localPosition.y;
        }

        SetColliders(false);
        StartCoroutine(pTween.Wait(enableCollisionStartDelay, () =>
        {
            SetColliders(true);
        }));
    }

    private void SetColliders(bool active)
    {
        foreach (var c in colliders)
        {
            c.enabled = active;
        }
    }

    private void Update()
    {
        if (scaleOnDistance != null)
        {
            var cameraPos = InputVR.VRCamera.transform.position;
            var playerDist = (transform.position - cameraPos).magnitude;
            var distanceParam = Mathf.InverseLerp(distanceRange.x, distanceRange.y, playerDist);
            var scale = distanceScale.Evaluate(distanceParam);
            scaleOnDistance.localScale = scale * Vector3.one;
            scaleOnDistance.localPosition = new Vector3(scaleOnDistance.localPosition.x, initHeight * scale, scaleOnDistance.localPosition.z);
            if (scaleOnY != null)
            {
                scaleOnY.localScale = new Vector3(1, scale, 1);
            }
        }
    }
}
