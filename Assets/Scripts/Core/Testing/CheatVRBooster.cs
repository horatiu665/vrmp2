namespace Core
{
    using UnityEngine;

    public class CheatVRBooster : MonoBehaviour
    {
        private SkiStick _skiStick;

        [SerializeField]
        private float forceForward = 10f;

        private void Awake()
        {
            _skiStick = GetComponent<SkiStick>();
        }

        private void Update()
        {
            var hand = _skiStick.hand;
            if (hand != null)
            {
                var c = hand.controller;
                if (c != null && _skiStick.isEquipped)
                {
                    if (c.GetPressDown(VRCore.VRInput.Touchpad))
                    {
                        // boost!!!
                        ((IPlayer)_skiStick.hand.player).AddForce(this, _skiStick.hand.player.head.forward * forceForward);
                    }
                }
            }
        }
    }
}