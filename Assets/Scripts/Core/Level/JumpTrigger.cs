namespace Core.Level
{
    using Apex.WorldGeometry;
    using Player;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Level")]
    [RequireComponent(typeof(Collider), typeof(AudioSource))]
    public sealed class JumpTrigger : MonoBehaviour
    {
        [SerializeField, Range(0f, 1f)]
        private float _deltaHeadUpdateAlpha = 0.2f;

        [SerializeField]
        private AnimationCurve _deltaHeadUpToBoost = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        [SerializeField]
        private float _deltaHeadMaxDistance = 2f;

        [SerializeField]
        private float _boostAmount = 100f;

        [SerializeField]
        private float _dragAmount = -4f;

        [SerializeField]
        private float _dragDuration = 5f;

        [SerializeField]
        private Transform _boostDirection = null;

        [SerializeField]
        private Transform _jumpStartPosition = null;
        public Transform jumpStartPosition
        {
            get
            {
                return _jumpStartPosition;
            }
        }

        [SerializeField]
        private VRAudio _niceJumpAudio = null;

        [SerializeField]
        private VRAudio _goodJumpAudio = null;

        [SerializeField]
        private bool showDebug = false;

        private bool _movingThrough;
        private Vector3 _oldHeadPos;
        private Vector3 _deltaHead;
        private Collider _collider;
        private IPlayer _skier;

        public event System.Action<IPlayer> OnJumpStart;

        public Vector3 jumpDirection
        {
            get { return _boostDirection != null ? _boostDirection.forward : this.transform.forward; }
        }

        public Vector3 upDirection
        {
            get { return _boostDirection != null ? _boostDirection.up : this.transform.up; }
        }

        private void OnEnable()
        {
            var source = this.GetComponent<AudioSource>();
            _niceJumpAudio.SetAudioSource(source);
            _goodJumpAudio.SetAudioSource(source);
            this.Link((_collider = this.GetComponent<Collider>()));
            if (_jumpStartPosition == null)
            {
                _jumpStartPosition = transform;
            }
        }

        private void OnDisable()
        {
            this.Unlink(_collider);
        }

        private void Update()
        {
            if (!_movingThrough || _skier == null)
            {
                return;
            }

            _deltaHead = Vector3.Lerp(_deltaHead, (_skier.head.localPosition - _oldHeadPos) / Time.deltaTime, _deltaHeadUpdateAlpha);
            _oldHeadPos = _skier.head.localPosition;

            if (showDebug)
            {
                print(_deltaHead);
            }
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.attachedRigidbody == null)
            {
                return;
            }

            if (!Layers.InLayer(other.attachedRigidbody.gameObject, Layers.players))
            {
                return;
            }

            var skier = other.attachedRigidbody.GetPlayer<IPlayer>();
            if (skier == null)
            {
                Debug.LogError(this.ToString() + " OnTriggerEnter() collided with a player without ISkiPlayer == " + other.ToString());
                return;
            }

            _movingThrough = true;
            _oldHeadPos = skier.head.localPosition;
            _skier = skier;
            if (showDebug)
                Debug.Log("we're good to go", other.attachedRigidbody);

        }

        private void OnTriggerExit(Collider other)
        {
            if (other.attachedRigidbody == null)
            {
                return;
            }

            if (!Layers.InLayer(other.attachedRigidbody.gameObject, Layers.players))
            {
                return;
            }

            var skier = other.attachedRigidbody.GetPlayer<IPlayer>();
            if (skier == null)
            {
                Debug.LogError(this.ToString() + " OnTriggerExit() collided with a player without ISkiPlayer == " + other.ToString());
                return;
            }

            _movingThrough = false;

            // find what the head is doing when exiting the collider (which is at the perfect jump spot) and add forces accordingly
            Vector3 failForce = new Vector3(0, 0, 0);
            var headYParam = _deltaHead.y / _deltaHeadMaxDistance;
            var forceToAdd = _deltaHeadUpToBoost.Evaluate(headYParam) * _boostAmount;
            if (forceToAdd < 0f)
            {
                failForce = Vector3.down * forceToAdd;
                forceToAdd = 0f;
            }

            other.attachedRigidbody.AddForce(forceToAdd * _boostDirection.forward + failForce, ForceMode.Impulse);

            // Audio
            if (_deltaHead.y > 0)
            {
                if (_deltaHead.y > 1f)
                {
                    _goodJumpAudio.Play();
                }
                else
                {
                    _niceJumpAudio.Play();
                }
            }

            if (showDebug)
                print("BAM we jumped: Add " + forceToAdd + " fail " + failForce);

            if (headYParam > 0)
            {
                CoroutineHelper.instance.StartCoroutine(pTween.To(_dragDuration * headYParam, t =>
                {
                    _skier.AddDragNextFrame(_dragAmount * headYParam * Mathf.SmoothStep(1f, 0f, t));
                }));
            }

            // TODO: OPTIMIZE!!
            var scorer = skier.transform.GetComponent<AirTimeScorer>();
            if (scorer != null)
            {
                scorer.BeginTrampolineJump(this);
            }

            _deltaHead = Vector3.zero;

            // this is where jump really happens!
            if (OnJumpStart != null)
            {
                OnJumpStart(skier);
            }
        }
    }
}