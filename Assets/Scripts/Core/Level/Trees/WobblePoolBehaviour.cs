namespace Core.Level
{
    using System;
    using Apex.WorldGeometry;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public sealed class WobblePoolBehaviour : MonoBehaviour, IFFPoolBehaviour
    {
        [SerializeField, ReadOnly]
        private int layer;
        [SerializeField, ReadOnly]
        private Vector3 initScale;
        [SerializeField, ReadOnly]
        private WobbleOnCollision _wobble;

        private void OnValidate()
        {
            if (_wobble == null)
            {
                _wobble = GetComponent<WobbleOnCollision>();
            }

            layer = gameObject.layer;
            initScale = transform.localScale;

        }

        public void OnSpawnFromPool()
        {
            gameObject.layer = layer;
            transform.localScale = initScale;

        }

        public void OnReturnToPool()
        {
            gameObject.layer = LayerMask.NameToLayer("NoCollisions");
            _wobble.BreakJoint();
            Destroy(_wobble.rb);
        }
    }
}