namespace Core.Level
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Level")]
    public sealed class WobbleOnCollision : MonoBehaviour, IOriginShifter
    {
        [SerializeField]
        private LayerMask _layers = 0;

        [SerializeField]
        private bool _setIsKinematic = true;

        [SerializeField]
        private bool _wobble = true;

        [SerializeField]
        private bool _setNoCollisionsAfter = false;

        [Header("Wobble")]
        [SerializeField]
        private Vector3 _force = Vector3.one;

        [SerializeField]
        private Vector3 _angularForce = Vector3.one;

        [Header("Wobble joint setup")]

        [SerializeField]
        private float _jointSpringStrength = 700f;

        [SerializeField]
        private float _jointSpringDamper = 1;

        [SerializeField]
        private ConfigurableJointMotion angularX = ConfigurableJointMotion.Limited, angularY = ConfigurableJointMotion.Limited, angularZ = ConfigurableJointMotion.Limited;

        [SerializeField]
        private float angularXLimit = 10f, angularYLimit = 1f, angularZLimit = 1f;

        [Header("Fly")]
        [SerializeField]
        private float _forceFactor = 50f;

        [SerializeField]
        private float _upFactor = 0.7f;

        [SerializeField]
        private float _angularForceFactor = 5f;

        [SerializeField, MinMax]
        private Vector2 _skierVelocityRange = new Vector2(1f, 10f);

        [SerializeField]
        private AnimationCurve _skierVelocityMapping = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        [SerializeField]
        private Rigidbody _rb;
        public Rigidbody rb
        {
            get
            {
                if (_rb == null)
                {
                    _rb = GetComponent<Rigidbody>();
                }
                return _rb;
            }
        }

        private ConfigurableJoint _joint;
        
        private void OnCollisionEnter(Collision collision)
        {
            OnTriggerEnter(collision.collider);
        }

        private void OnTriggerEnter(Collider collider)
        {
            var go = collider.gameObject;
            Rigidbody otherRB = null;
            if (collider.attachedRigidbody != null)
            {
                go = collider.attachedRigidbody.gameObject;
                otherRB = collider.attachedRigidbody;
                collider = go.GetComponentInChildren<Collider>();
            }
            else
            {
                otherRB = collider.GetComponent<Rigidbody>();
            }

            if (!this.enabled || ReferenceEquals(go, this.gameObject) || otherRB == null)
            {
                return;
            }

            if (!Layers.InLayer(go, _layers))
            {
                return;
            }

            if (_rb == null)
            {
                _rb = gameObject.GetOrAddComponent<Rigidbody>();
            }

            if (_setIsKinematic && _rb.isKinematic)
            {
                _rb.isKinematic = false;
            }

            if (_wobble)
            {
                WobbleThisShit(go, otherRB);

            }
            else
            {
                FlyThisShit(otherRB, go);
            }

        }

        private void FlyThisShit(Rigidbody otherRB, GameObject go)
        {
            // apply the force to make it fly
            var forceDir = ((this.transform.position - go.transform.position).normalized + (Vector3.up * _upFactor));
            var forceFactor = _forceFactor;

            var skier = otherRB.GetPlayer<IPlayer>();
            if (skier != null)
            {
                var velocity = skier.rb.velocity.magnitude;
                var clamped = Mathf.Clamp(velocity, _skierVelocityRange.x, _skierVelocityRange.y);
                var mapped = _skierVelocityMapping.Evaluate(clamped / _skierVelocityRange.y);
                forceFactor *= mapped;
            }

            _rb.AddForce(forceDir * forceFactor, ForceMode.Impulse);
            _rb.AddTorque(Random.onUnitSphere * _angularForceFactor, ForceMode.Impulse);

            if (_setNoCollisionsAfter)
            {
                this.gameObject.layer = LayerMask.NameToLayer("NoCollisions");
            }
        }

        private void WobbleThisShit(GameObject go, Rigidbody otherRB)
        {

            // apply the force to make it wobble
            _joint = this.GetComponentInChildren<ConfigurableJoint>();
            if (_joint == null)
            {
                // Create new joint if none exists
                _joint = SetupJoint();
            }

            var side = GetRandomSide(); // random side
            var delta = go.transform.position.x - this.transform.position.x;
            if (Mathf.Abs(delta) > 0.1f) // TODO: Magic number?
            {
                side = delta > 0f ? -1 : 1; // side is no longer random
            }

            var force = (Vector3.right * side * _force.x) + (Vector3.up * _force.y) + (Vector3.forward * _force.z) + otherRB.velocity;
            _rb.AddForce(force, ForceMode.Impulse);

            var torque = (Vector3.right * GetRandomSide() * _angularForce.x * Random.value) + (Vector3.up * GetRandomSide() * _angularForce.y * Random.value) + (Vector3.forward * _angularForce.z);
            _rb.AddTorque(torque, ForceMode.Impulse);

            if (_setNoCollisionsAfter)
            {
                this.gameObject.layer = LayerMask.NameToLayer("NoCollisions");
            }
        }

        private int GetRandomSide()
        {
            return (Random.Range(0, 2) * 2) - 1;
        }

        private ConfigurableJoint SetupJoint()
        {
            var joint = this.AddComponent<ConfigurableJoint>();
            joint.anchor = Vector3.zero;

            // Setup constarints
            joint.xMotion = joint.yMotion = joint.zMotion = ConfigurableJointMotion.Locked;
            joint.angularXMotion = angularX;
            joint.angularYMotion = angularY;
            joint.angularZMotion = angularZ;

            // Setup angular limits
            var angularLimit = joint.lowAngularXLimit;
            angularLimit.limit = -angularXLimit;
            joint.lowAngularXLimit = angularLimit;

            angularLimit = joint.highAngularXLimit;
            angularLimit.limit = angularXLimit;
            joint.highAngularXLimit = angularLimit;

            angularLimit = joint.angularYLimit;
            angularLimit.limit = angularYLimit;
            joint.angularYLimit = angularLimit;

            angularLimit = joint.angularZLimit;
            angularLimit.limit = angularZLimit;
            joint.angularZLimit = angularLimit;

            // Setup angular springs limit
            var springLimit = joint.angularXLimitSpring;
            springLimit.spring = _jointSpringStrength;
            springLimit.damper = _jointSpringDamper;
            joint.angularXLimitSpring = springLimit;

            springLimit = joint.angularYZLimitSpring;
            springLimit.spring = _jointSpringStrength;
            springLimit.damper = _jointSpringDamper;
            joint.angularYZLimitSpring = springLimit;

            return joint;
        }

        public void BreakJoint()
        {
            if (_joint != null)
            {
                Destroy(_joint);
            }
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
        }

        public void OnWorldMove(Vector3 delta)
        {
            if (_joint != null)
            {
                _joint.connectedAnchor -= delta;
                transform.position -= delta;
            }
        }
    }
}