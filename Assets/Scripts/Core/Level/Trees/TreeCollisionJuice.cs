using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class TreeCollisionJuice : MonoBehaviour
{
    [Header("Particles")]
    [SerializeField]
    private ParticleSystem _fallingSnowParticles;

    [SerializeField]
    private float minDurationBetweenSnowFalls = 1f;
    float nextAllowedSnowFall;

    [SerializeField]
    private AnimationCurve velocityToParticles = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(1, 0, 0, 0), new Keyframe(10, 50, 1, 0) } };

    [Header("Sound")]
    [SerializeField]
    private float minVelForSound = 1f;

    [SerializeField]
    private SmartSound bonkSound;

    private void OnCollisionEnter(Collision c)
    {
        var relVel = c.relativeVelocity.magnitude;
        if (relVel > minVelForSound)
        {
            bonkSound.Play();
        }

        if (Time.time > nextAllowedSnowFall)
        {
            nextAllowedSnowFall = Time.time + minDurationBetweenSnowFalls;
            _fallingSnowParticles.Emit((int)velocityToParticles.Evaluate(relVel));
        }

    }
}