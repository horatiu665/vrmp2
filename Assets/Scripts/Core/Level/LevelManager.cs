namespace Core
{
    using System;
    using System.Collections.Generic;
    using UnityEngine;
    using VRCore;

    public static class LevelManager
    {
        private static IDictionary<Collider, MonoBehaviour> _dict = new Dictionary<Collider, MonoBehaviour>();

        public static void Link(this MonoBehaviour mb, Collider collider)
        {
            if (collider == null)
            {
                Debug.LogError(mb.ToString() + " is missing a collider! Cannot link.");
                return;
            }

            _dict.Add(collider, mb);
        }

        public static bool Unlink(this MonoBehaviour mb, Collider collider)
        {
            if (collider == null)
            {
                Debug.LogError(mb.ToString() + " is missing a collider! Cannot link.");
                return false;
            }

            return _dict.Remove(collider);
        }

        public static MonoBehaviour Get(this Collider collider)
        {
            if (collider == null)
            {
                Debug.LogError("LevelManager.Get passed null collider, cannot look up with null key");
                return null;
            }

            return _dict.GetValueOrDefault(collider);
        }

        public static T Get<T>(this Collider collider) where T : MonoBehaviour
        {
            return Get(collider) as T;
        }

        public static IList<T> GetAll<T>() where T : MonoBehaviour
        {
            var list = new List<T>();
            GetAll<T>(list);
            return list;
        }

        public static void GetAll<T>(IList<T> list) where T : MonoBehaviour
        {
            var enumerator = _dict.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current.Value as T;
                    if (current != null)
                    {
                        list.Add(current);
                    }
                }
            }
            finally
            {
                enumerator.Dispose();
            }
        }

        public static IList<MonoBehaviour> GetAll(Type type)
        {
            var list = new List<MonoBehaviour>();
            GetAll(type, list);
            return list;
        }

        public static void GetAll(Type type, IList<MonoBehaviour> list)
        {
            var enumerator = _dict.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var current = enumerator.Current.Value;
                    var currentType = current.GetType();
                    if (currentType == type || currentType.IsAssignableFrom(type))
                    {
                        list.Add(current);
                    }
                }
            }
            finally
            {
                enumerator.Dispose();
            }
        }
    }
}