﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class CloudSpawner : MonoBehaviour
{
    public List<GameObject> cloudPrefabs = new List<GameObject>();

    public float radius = 3000;

    public int count = 40;

    public float yRatio = 0.2f;
    public float yOffset = 0.1f;

    [Header("Scales")]
    public Vector3 minScale = Vector3.one;
    public Vector3 maxScale = Vector3.one;

    [Header("Rotations")]
    public Vector3 minRot = Vector3.zero;
    public Vector3 maxRot = Vector3.zero;

    [DebugButton]
    public void Spawn()
    {
#if UNITY_EDITOR

        UnityEditor.Undo.SetCurrentGroupName("Spawn " + count + " Clouds");

        for (int i = 0; i < count; i++)
        {
            var pos = Random.onUnitSphere;
            pos.y *= yRatio;
            pos.y += yOffset;
            pos.Normalize();
            pos = pos * radius;

            var rot = Quaternion.Euler(new Vector3(
                    Random.Range(minRot.x, maxRot.x),
                    Random.Range(minRot.y, maxRot.y),
                    Random.Range(minRot.z, maxRot.z)
                ));

            var c = this.InstantiateSafe(cloudPrefabs[Random.Range(0, cloudPrefabs.Count)], pos, rot, transform);
            UnityEditor.Undo.RegisterCreatedObjectUndo(c, "Spawn Cloud");

            var scale = c.transform.localScale;
            scale.Scale(new Vector3(
                    Random.Range(minScale.x, maxScale.x),
                    Random.Range(minScale.y, maxScale.y),
                    Random.Range(minScale.z, maxScale.z)
                ));
            c.transform.localScale = scale;

        }

        UnityEditor.Undo.IncrementCurrentGroup();
#endif

    }


    [DebugButton]
    private void DestroyChildren()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            this.DestroySafe(transform.GetChild(i).gameObject);
        }
    }


}
