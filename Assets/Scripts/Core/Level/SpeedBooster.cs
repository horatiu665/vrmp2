namespace Core.Level
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Level")]
    [RequireComponent(typeof(Collider), typeof(AudioSource))]
    public sealed class SpeedBooster : MonoBehaviour
    {
        [SerializeField, Range(1f, 10f)]
        private float _speedForce = 1.5f;

        [SerializeField]
        private VRAudio _audio = null;

        private void OnEnable()
        {
            _audio.SetAudioSource(this.GetComponent<AudioSource>());
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!Layers.InLayer(other.gameObject, Layers.players))
            {
                return;
            }

            var skier = other.GetPlayer<IPlayer>();
            if (skier == null)
            {
                Debug.LogError(this.ToString() + " collided with a Player without ISkiPlayer == " + other.ToString());
                return;
            }

            skier.rb.velocity *= _speedForce;
        }
    }
}