﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class CairnsMaker : MonoBehaviour
{
    public List<VRPrefabType> rockPrefabs = new List<VRPrefabType>();

    [Header("Settings")]
    public Vector2 rockCountHeight = new Vector2(4, 9);

    public Vector3 rockShapeMin = new Vector3(1, 0.4f, 1), rockShapeMax = new Vector3(0.8f, 0.7f, 0.8f);

    public AnimationCurve rockSizeCurve = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 0.4f, 0, 0) } };

    public float rockSizeVariation = 0.1f;

    public float heightMultiplier = 1.3f;

    private List<IVRPrefab> _spawnedItems = new List<IVRPrefab>();
    public List<IVRPrefab> spawnedItems
    {
        get
        {
            return _spawnedItems;
        }
    }

    public bool onEnable = true;

    private RenderersAndCollidersPoolBehaviour _racpoolBehaviour;
    public RenderersAndCollidersPoolBehaviour racpoolBehaviour
    {
        get
        {
            if (_racpoolBehaviour == null)
            {
                _racpoolBehaviour = GetComponentInParent<RenderersAndCollidersPoolBehaviour>();
            }
            return _racpoolBehaviour;
        }
    }

    private List<Collider> rockCols = new List<Collider>(10);
    private List<Renderer> rockRens = new List<Renderer>(10);

    private void OnEnable()
    {
        // wait a frame goodddammmitt cause the prefab manager is not ready yet with the shitty pools
        if (onEnable)
        {
            StartCoroutine(pTween.WaitFrames(1, MakeCairn));
        }
    }

    [DebugButton]
    public void MakeCairn()
    {
        if (FoggyForestPoolManager.instance == null)
        {
            return;
        }
        ClearChildren();
        int rockCount = GetRandom(rockCountHeight);
        float lastHeight = -0.5f;
        float prevRockHeight = 0;
        for (int i = 0; i < rockCount; i++)
        {
            Vector3 rockscale = Vector3.one * (rockSizeCurve.Evaluate(i / (float)(rockCount - 1)) + GetRandom(-rockSizeVariation, rockSizeVariation));
            rockscale.x *= GetRandom(rockShapeMin.x, rockShapeMax.x);
            rockscale.y *= GetRandom(rockShapeMin.y, rockShapeMax.y);
            rockscale.z *= GetRandom(rockShapeMin.z, rockShapeMax.z);
            float angle = GetRandom(0, 360f);

            var pos = transform.TransformPoint(new Vector3(0, (lastHeight + heightMultiplier * prevRockHeight), 0));
            var rot = transform.rotation * Quaternion.Euler(0, angle, 0);
            var newrock = FoggyForestPoolManager.instance.Spawn(rockPrefabs[GetRandomIndex(rockPrefabs)], pos, rot);
            newrock.transform.SetParent(transform);
            lastHeight = newrock.transform.localPosition.y;
            newrock.transform.localScale = rockscale;

            prevRockHeight = rockscale.y;

            _spawnedItems.Add(newrock);

            var r = newrock.GetComponent<Renderer>();
            var c = newrock.GetComponent<Collider>();
            rockRens.Add(r);
            rockCols.Add(c);
            racpoolBehaviour.AddRenderer(r);
            racpoolBehaviour.AddCollider(c);
        }
    }

    [DebugButton]
    void ClearChildren()
    {
        if (!Application.isPlaying)
        {
            transform.ClearChildren();
        }
        else
        {
            for (int i = 0; i < rockRens.Count; i++)
            {
                racpoolBehaviour.RemoveRenderer(rockRens[i]);
            }
            rockRens.Clear();
            for (int i = 0; i < rockCols.Count; i++)
            {
                racpoolBehaviour.RemoveCollider(rockCols[i]);
            }
            rockCols.Clear();
            for (int i = 0; i < _spawnedItems.Count; i++)
            {
                FoggyForestPoolManager.instance.Return(_spawnedItems[i]);
            }
            _spawnedItems.Clear();
        }
    }

    private int GetRandomIndex(IList list)
    {
        return Random.Range(0, list.Count);
    }

    private float GetRandom(float x1, float x2)
    {
        return Random.Range(x1, x2);
    }

    private int GetRandom(Vector2 v)
    {
        return Mathf.RoundToInt(Random.Range(v.x, v.y));
    }
}