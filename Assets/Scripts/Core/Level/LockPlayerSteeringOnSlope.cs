﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;
using System;
using Core.Level;

public class LockPlayerSteeringOnSlope : MonoBehaviour
{
    [Header("Drag n drop refs")]
    [SerializeField]
    private JumpTrigger _jumpTrigger;
    public JumpTrigger jumpTrigger
    {
        get
        {
            if (_jumpTrigger == null)
            {
                _jumpTrigger = transform.parent.GetComponentInChildren<JumpTrigger>();
            }
            return _jumpTrigger;
        }
    }

    PlayerBase localPlayer
    {
        get
        {
            return PlayerManager.GetLocalPlayer<PlayerBase>();
        }
    }

    [Header("Locks player on this transform's YZ plane")]
    public float lockMinVelocity = 7;

    public float maxLockTime = 9f;

    private float lastJumpTime = 0f;
    private float lockStartTime;

    public bool unlockOnJump = true;
    float initX;

    private void OnEnable()
    {
        if (jumpTrigger != null)
        {
            jumpTrigger.OnJumpStart += JumpTrigger_OnJumpStart;
        }
    }

    private void OnDisable()
    {
        if (jumpTrigger != null)
        {
            jumpTrigger.OnJumpStart -= JumpTrigger_OnJumpStart;
        }
    }

    private void JumpTrigger_OnJumpStart(IPlayer skier)
    {
        if (skier as PlayerBase == localPlayer)
        {
            lastJumpTime = Time.time;
        }
    }

    private void OnValidate()
    {
        if (jumpTrigger != null)
        {
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.attachedRigidbody != null)
        {
            var skier = other.attachedRigidbody.GetPlayer<PlayerBase>();
            if (skier != null)
            {
                StartCoroutine(LockPlayerUntilNot(skier));
            }
        }
    }

    private IEnumerator LockPlayerUntilNot(PlayerBase skier)
    {
        lockStartTime = Time.time;
        var initSkierPos = transform.InverseTransformPoint(skier.position);
        initX = initSkierPos.x;

        while (PlayerStayLocked(skier))
        {
            var localPos = transform.InverseTransformPoint(skier.position);
            // lock position on this objects forward/up plane
            localPos.x = initX;
            skier.position = transform.TransformPoint(localPos);

            yield return new WaitForFixedUpdate();
        }

        yield break;
    }

    private bool PlayerStayLocked(PlayerBase skier)
    {
        // Unlock player when velocity lower than minVel
        if (skier.velocity.magnitude <= lockMinVelocity)
        {
            return false;
        }

        // unlock player when hits the jump trigger
        if (unlockOnJump && lastJumpTime > lockStartTime)
        {
            return false;
        }

        // unlock when time passed over maxLockTime
        if (Time.time - lockStartTime > maxLockTime)
        {
            return false;
        }

        return true;
    }
}