﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ResetJointsUponRescale : MonoBehaviour
{
    Vector3 oldScale;

    private void Update()
    {
        if (oldScale != transform.localScale)
        {
            oldScale = transform.localScale;

            // reset joints
            var joints = GetComponentsInChildren<ConfigurableJoint>();
            for (int i = 0; i < joints.Length; i++)
            {
                joints[i].transform.localPosition = Vector3.zero;
                joints[i].transform.localRotation = Quaternion.identity;
                joints[i].connectedAnchor = joints[i].connectedAnchor;
            }
        }
    }
}