namespace Core.Level
{
    using UnityEngine;
    using VRCore;

    public sealed class QuitGameHandler : SingletonMonoBehaviour<QuitGameHandler>
    {
        [SerializeField, Tooltip("The key used to exit the game immediately.")]
        private KeyCode _exitKey = KeyCode.Escape;

        private void Update()
        {
            if (Input.GetKeyUp(_exitKey))
            {
                this.enabled = false;
#if UNITY_EDITOR
                // pause in editor, quit otherwise
                Debug.Break();
#else
                Application.Quit();
#endif
            }
        }
    }
}