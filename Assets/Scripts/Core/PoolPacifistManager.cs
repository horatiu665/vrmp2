using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class PoolPacifistManager : MonoBehaviour
{
    public static PoolPacifistManager inst;

    public Queue<GameObject> pooledTrees;

    public GameObject tree;

    public int initCapacity = 500;
    int pooledTreesCount = 0;
    
    void Awake()
    {
        inst = this;
        pooledTrees = new Queue<GameObject>(initCapacity);
    }

    public GameObject SpawnTree(Vector3 pos, Quaternion rot)
    {
        if (pooledTrees.Count > 0)
        {
            var go = pooledTrees.Dequeue();
            pooledTreesCount--;
            go.transform.position = pos;
            go.transform.rotation = rot;
            return go;
        }
        else
        {
            var go = this.InstantiateSafe(tree, pos, rot);
            return go;
        }
    }

    public void DestroyTree(GameObject tree)
    {
        var r = tree.GetComponent<Rigidbody>();
        if (r != null)
        {
            r.isKinematic = true;
        }
        tree.transform.SetParent(transform);
        pooledTrees.Enqueue(tree);
        pooledTreesCount++;
    }
}