namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using UnityEngine.Networking;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Client;

    [RequireComponent(typeof(Rigidbody))]
    public sealed class HitPlayerOnCollisionNetwork : MonoBehaviour
    {
        [SerializeField]
        private float _forceMultiplier = 1f;

        private void OnCollisionEnter(Collision collision)
        {
            if (!NetServices.isClient)
            {
                return;
            }

            if (collision.rigidbody == null)
            {
                return;
            }

            if (!Layers.InLayer(collision.rigidbody.gameObject, Layers.players))
            {
                return;
            }

            var playerVictim = collision.rigidbody.GetPlayer<IPlayer>();
            if (playerVictim != null && !playerVictim.isLocal)
            {
                var toPlayer = playerVictim.position + Vector3.up - collision.contacts[0].point;
                var forceToApply = _forceMultiplier * toPlayer;

                // sending playerVictim netId which we will use to inform the other player to get hit
                var message = MessagePool.Get<ApplyForceToPlayerMessage>(playerVictim.netId);
                message.force = forceToApply;

                ClientNetSender.instance.Send(message, QosType.UnreliableSequenced);
                MessagePool.Return(message);
            }
        }
    }
}