namespace Core
{
    using Apex.WorldGeometry;
    using Helpers;
    using Score;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    [RequireComponent(typeof(Collider), typeof(IPlayer))]
    public sealed class PlayerJoustOnCollision : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private LayerMask _layer = 0;

        [SerializeField, MinMax(min = 0f, max = 1000f)]
        private Vector2 _velocityRange = new Vector2(0.01f, 20f);

        [SerializeField]
        private AnimationCurve _velocityMapping = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        [SerializeField]
        private float _finalForceMultiplier = 50f;

        [SerializeField, Range(1, 10000)]
        private int _joustScore = 500;

        [SerializeField, FormerlySerializedAs("hitterForceOnly")]
        private bool _hitterForceOnly = true;

        [SerializeField, FormerlySerializedAs("addOrSetVelocity")]
        private bool _addOrSetVelocity = true;

        private IPlayer _player;

        private void Awake()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
        }

        private void OnTriggerEnter(Collider other)
        {
            // only the correct layer
            if (!Layers.InLayer(other.gameObject, _layer))
            {
                return;
            }

            // only ski stick vs localplayer
            var otherSkiStick = other.attachedRigidbody.GetComponent<SkiStick>();
            if (otherSkiStick == null)
            {
                return;
            }

            // only other player and no unequipped ski sticks
            var otherSkier = otherSkiStick.skier;
            if (!otherSkiStick.isEquipped || otherSkier == _player)
            {
                return;
            }

            var hitterVelocity = otherSkier.velocity;
            var beingHitVelocity = _player.velocity;

            float shootForce = 0f;
            if (_hitterForceOnly)
            {
                shootForce = hitterVelocity.magnitude;
            }
            else
            {
                shootForce = (beingHitVelocity - hitterVelocity).magnitude;
            }

            shootForce = Mathf.InverseLerp(_velocityRange.x, _velocityRange.y, shootForce);
            shootForce = _velocityMapping.Evaluate(shootForce);

            var towardsMyself = _player.position - otherSkier.position;
            var finalForce = shootForce * towardsMyself.normalized;

            if (_addOrSetVelocity)
            {
                _player.velocity += finalForce * _finalForceMultiplier;
            }
            else
            {
                _player.velocity = finalForce * _finalForceMultiplier;
            }

            //ScorePointsManager.instance.AddScore(_player, ScoreType.Joust, _joustScore);
            //_player.AddScore(ScoreType.Joust, _joustScore);

            //ScoreManager.instance.Get(ScoreType.Joust, _joustScore, _player, otherSkier.position);
            //Debug.Log("HIT!!! v=" + (finalForce * _finalForceMultiplier).ToString("F2"));
        }
    }
}