namespace Core
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using VRCore;

    public sealed class FlyPoopManager : SingletonMonoBehaviour<FlyPoopManager>
    {
        [SerializeField]
        private GameObject _flyPoopPrefab;

        [SerializeField]
        private Transform[] _spawnLocations = new Transform[1];

        [SerializeField]
        private int _maxConcurrentPoops = 500;

        [SerializeField]
        private float _scaleTime = 3f;

        private IList<PoopData> _poops;
        private int _lastIndex;

        protected override void Awake()
        {
            base.Awake();
            _poops = new List<PoopData>(_maxConcurrentPoops);
        }

        private void OnEnable()
        {
            StartCoroutine(Init());
        }

        private IEnumerator Init()
        { 
            yield return new WaitForSeconds(1f);

            for (int i = 0; i < _spawnLocations.Length; i++)
            {
                AddPoop(_spawnLocations[i]);
                yield return 0;
            }
        }

        private void Update()
        {
            var count = _poops.Count;
            for (int i = 0; i < count; i++)
            {
                var poop = _poops[i];
                if (!poop.pickedUp && poop.equippable.position != poop.spawnLocation.position)
                {
                    _poops[i] = new PoopData()
                    {
                        equippable = poop.equippable,
                        spawnLocation = poop.spawnLocation,
                        pickedUp = true
                    };

                    AddPoop(poop.spawnLocation);
                }
            }
        }

        private void AddPoop(Transform spawnLocation)
        {
            if (_poops.Count < _maxConcurrentPoops)
            {
                // instantiate new poop
                var poop = (FlyPoopEquippable)VRPrefabManager.instance.Spawn<IVRPrefab>(VRPrefabType.FlyPoop, spawnLocation.transform.position, spawnLocation.transform.rotation);
                _poops.Add(new PoopData()
                {
                    equippable = poop,
                    spawnLocation = spawnLocation,
                    pickedUp = false
                });

                StartCoroutine(ScaleIn(poop.transform));
            }
            else
            {
                // find first non-equipped poop
                PoopData freePoop = _poops[_lastIndex];
                while (freePoop.equippable.isEquipped)
                {
                    freePoop = _poops[++_lastIndex];

                    if (_lastIndex >= _poops.Count)
                    {
                        _lastIndex = 0;
                    }
                }

                _poops[_lastIndex] = new PoopData()
                {
                    equippable = freePoop.equippable,
                    spawnLocation = freePoop.spawnLocation,
                    pickedUp = false
                };

                StartCoroutine(ScaleIn(freePoop.equippable.transform));
            }
        }

        private IEnumerator ScaleIn(Transform transform)
        {
            var targetScale = transform.localScale;
            transform.localScale = Vector3.zero;
            var elapsedTime = 0f;

            while (transform.localScale != targetScale && elapsedTime <= _scaleTime)
            {
                transform.localScale = Vector3.Lerp(Vector3.zero, targetScale, elapsedTime / _scaleTime);
                elapsedTime += Time.deltaTime;

                yield return 0;
            }
        }

        [Serializable]
        private struct PoopData
        {
            public FlyPoopEquippable equippable;
            public Transform spawnLocation;
            public bool pickedUp;
        }
    }
}