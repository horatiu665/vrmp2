﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class FoggyForestPoolPreallocation : MonoBehaviour
{
    public List<PreallocData> preallocData = new List<PreallocData>();

    // testing
    [DebugButton]
    private void PreallocateAll()
    {
        ClearAll();
        for (int i = 0; i < preallocData.Count; i++)
        {
            var p = preallocData[i];
            for (int c = 0; c < p.preSpawned; c++)
            {
                // spawn p.type and store it in the pool
                var a = FoggyForestPoolManager.instance.Spawn(p.prefabType, Vector3.zero, Quaternion.identity);
                a.transform.SetParent(p.parent);
            }
        }

    }

    [DebugButton]
    private void ClearAll()
    {
        for (int i = 0; i < preallocData.Count; i++)
        {
            var p = preallocData[i];
            var list = p.parent.GetChildren();
            for (int j = 0; j < list.Count; j++)
            {
                this.DestroySafe(list[j].gameObject);
            }
        }
    }

    private void InitializePool()
    {
        foreach (var p in preallocData)
        {
            var list = p.parent.GetChildren();
            foreach (var s in list)
            {
                FoggyForestPoolManager.instance.Return(s.GetComponent<IVRPrefab>());
            }
        }
    }

    private void OnEnable()
    {
        InitializePool();
    }

    [Serializable]
    public class PreallocData
    {
        public VRPrefabType prefabType;
        public int preSpawned;

        [SerializeField]
        private Transform _parent;
        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    var newgo = new GameObject("[Pool Parent] " + prefabType);
                    _parent = newgo.transform;
                    _parent.SetParent(FoggyForestPoolManager.instance.transform);
                }
                return _parent;
            }
        }

    }


}