using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FoggyForestPoolManager))]
public class FoggyForestPoolManagerEditor : Editor
{
    bool foldout = false;

    public override void OnInspectorGUI()
    {
        var ff = (FoggyForestPoolManager)target;
        base.OnInspectorGUI();

        GUI.enabled = false;

        EditorGUILayout.IntField("Pools", ff.pool.Count);
        if (foldout = EditorGUILayout.Foldout(foldout, "Pool contents"))
        {
            EditorGUI.indentLevel++;

            var enu = ff.pool.GetEnumerator();
            while (enu.MoveNext())
            {
                EditorGUILayout.IntField(enu.Current.Key.ToString(), enu.Current.Value.Count);
            }

            EditorGUILayout.Space();

            EditorGUI.indentLevel--;
        }

        GUI.enabled = true;

        serializedObject.Update();
    }
}
