public interface IFFPoolBehaviour
{
    void OnSpawnFromPool();
    void OnReturnToPool();

}