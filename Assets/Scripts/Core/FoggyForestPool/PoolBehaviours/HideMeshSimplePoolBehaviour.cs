using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class HideMeshSimplePoolBehaviour : MonoBehaviour, IFFPoolBehaviour
{
	[SerializeField, VRCore.ReadOnly]
    private Renderer _renderer;

    public void OnReturnToPool()
    {
        _renderer.enabled = false;
    }

    public void OnSpawnFromPool()
    {
        _renderer.enabled = true;
    }

	private void Reset()
	{
		OnValidate();
	}

	private void OnValidate()
    {
        _renderer = GetComponentInChildren<Renderer>();
    }
}