using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class FoggyForestPoolManager : SingletonMonoBehaviour<FoggyForestPoolManager>
{
    public Dictionary<VRPrefabType, Queue<IVRPrefab>> pool = new Dictionary<VRPrefabType, Queue<IVRPrefab>>();

    public int initQueueCapacity = 128;
    
    public IVRPrefab Spawn(VRPrefabType type, Vector3 pos, Quaternion rot)
    {
        IVRPrefab go = null;
        // if type already has a pool, use it. else spawn a new one
        if (pool.ContainsKey(type) && pool[type].Count > 0)
        {
            go = pool[type].Dequeue();
            go.position = pos;
            go.rotation = rot;

            var poolBehs = go.gameObject.GetComponents<IFFPoolBehaviour>();
            for (int i = 0; i < poolBehs.Length; i++)
            {
                poolBehs[i].OnSpawnFromPool();
            }
        }
        else
        {
            go = VRPrefabManager.instance.Spawn(type, pos, rot);
        }

        return go;
    }

    public void Return(IVRPrefab obj)
    {
        var poolBehs = obj.gameObject.GetComponents<IFFPoolBehaviour>();
        for (int i = 0; i < poolBehs.Length; i++)
        {
            poolBehs[i].OnReturnToPool();
        }
        
        obj.transform.SetParent(transform);
        if (!pool.ContainsKey(obj.prefabType))
        {
            pool[obj.prefabType] = new Queue<IVRPrefab>(initQueueCapacity);
        }
        pool[obj.prefabType].Enqueue(obj);
    }
}