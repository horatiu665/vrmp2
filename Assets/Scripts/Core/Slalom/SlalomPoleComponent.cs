using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Core;
using UnityEngine;
using Random = UnityEngine.Random;
using Core.Score;

public class SlalomPoleComponent : MonoBehaviour
{
    [Header("Slalom level")]
    public int levelId;
    public int orderInLevel;

    [Header("Setup")]
    public bool isRight;

    // overrides the isRight from above.
    public bool isStart, isEnd;
    
    public int points = 100;

    // TO BE IMPLEMENTED. should auto rescale the trigger and place the flags at the edges of the trigger.
    [HideInInspector]
    public float gateWidth = 1f;

    [Header("Uses triggers on left/right to detect players crossing")]
    public GameObject left;
    public GameObject right;
    public GameObject start;
    public GameObject end;

    private ConfigurableJoint[] _joints;
    public ConfigurableJoint[] joints
    {
        get
        {
            if (_joints == null)
            {
                _joints = GetComponentsInChildren<ConfigurableJoint>(true);
            }
            return _joints;
        }
    }

    [Header("Sounds")]
    public SmartSound soundStartLevel;
    public SmartSound soundEndLevel, soundRegularGate;

    public void RefreshSide()
    {
        left.SetActive(!isStart && !isEnd && !isRight);
        right.SetActive(!isStart && !isEnd && isRight);
        start.SetActive(isStart && !isEnd);
        end.SetActive(!isStart && isEnd);
    }

    private GameObject GetActivePoleGameobject()
    {
        return isStart ? start : isEnd ? end : isRight ? right : left;
    }

    private void OnEnable()
    {
        Initialize();
    }

    public void Initialize()
    {
        // scale trigger based on gate width
        // reposition flags based on gate width

        RefreshSide();

        GameObject activePole = GetActivePoleGameobject();

        // place on ground in case we spawn on a hillside. don't include inactive since we just activated the correct side stick.
        // NOT USING THESE FOR NOW
        //var pog = activePole.GetComponentsInChildren<PlaceOnGroundOnEnable>(false);
        //for (int i = 0; i < pog.Length; i++)
        //{
        //    pog[i].PlaceOnGround();
        //}

    }

    public void PlayerCrossed(PlayerBase p)
    {
        SlalomManager.SlalomLevelScoringData levelfinishData = new SlalomManager.SlalomLevelScoringData();
        // tell manager we crossed the gate. if level progress is valid (we went from gate x to x+1)
        var crossedGateSuccess = SlalomManager.inst.PlayerCrossedGate(this, p, ref levelfinishData);
        if (crossedGateSuccess)
        {
            // for regular gates, score is added with ScoreAwarderComponent on the trigger.
            // for start and end we add it manually here cause there is more feedback
            if (isStart)
            {
                // level start flying score thing. score is level number
                // opt: create new system for showing which level we started with an icon instead of stupid numbers
                ScoreManager.instance.Get(ScoreType.SlalomLevelStart, "L " + levelId, p, p.position + p.skis.forward);

                // sound feedback?
                if (soundStartLevel != null)
                {
                    soundStartLevel.Play();
                }

            }
            else if (isEnd)
            {
                // level END flying score thing. points handled in slalommanager, here we just do graphics
                var completeTime = (Time.time - levelfinishData.startTime);
                var scoreString = SlalomManager.GenerateTimeString(completeTime);
                var sc = ScoreManager.instance.Get(ScoreType.SlalomCompletionTime, scoreString, p, p.position + p.skis.forward);
                ScoreManager.instance.Get(ScoreType.SlalomGatesCompleted, levelfinishData.gatesCompleted.ToString(), p, p.position + p.skis.forward, sc);

                // sound fb
                if (soundEndLevel != null)
                {
                    soundEndLevel.Play();
                }

            }
            else
            {
                // regular score from slalom pole
                ScoreManager.instance.Get(ScoreType.SlalomGate, points.ToString(), p, p.position + p.skis.forward);

                if (soundRegularGate != null)
                {
                    soundRegularGate.Play();
                }
            }

        }

    }

    [DebugButton]
    void FindComponents()
    {
        left = transform.Find("Left").gameObject;
        right = transform.Find("Right").gameObject;
        start = transform.Find("Start").gameObject;
        end = transform.Find("End").gameObject;

    }

}