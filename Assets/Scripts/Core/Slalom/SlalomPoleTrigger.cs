using Core;
using UnityEngine;
using VRCore;

public class SlalomPoleTrigger : MonoBehaviour
{
    private SlalomPoleComponent _pole;
    public SlalomPoleComponent pole
    {
        get
        {
            if (_pole == null)
            {
                _pole = GetComponentInParent<SlalomPoleComponent>();
            }
            return _pole;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        // triggers happen if disabled everybody knws that
        if (isActiveAndEnabled)
        {
            var p = other.GetPlayer<PlayerBase>();
            if (p != null)
            {
                // player crossed the slalom shit
                pole.PlayerCrossed(p);
            }
        }
    }
}