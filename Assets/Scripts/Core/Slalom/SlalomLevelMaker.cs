using System;
using Core;
using UnityEngine;
using VRCore;
using System.Linq;

[ExecuteInEditMode]
public class SlalomLevelMaker : MonoBehaviour
{
    public bool updateEveryFrame = false;

    public int levelId = 0;

    public bool canPoopGates = false;

    public float poopPlacementDelay = 0.5f;

    public VRPrefabBase gatePrefab;

    private PlayerBase _player;
    public PlayerBase player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<PlayerBase>();
            }
            return _player;
        }
    }


    private void Update()
    {
        if (updateEveryFrame)
        {
            SetOrderAndLevel();
        }

        if (canPoopGates)
        {
            HandleVRSlalomPooper();
        }
    }

    // called only when canPoopGates is true
    private void HandleVRSlalomPooper()
    {
        if (GetPoopInputDown())
        {
            if (player != null)
            {
                // spawn slalom gate at player pos on the mountain (on moment of trigger but with a delay)
                var pos = player.position;
                pos.y = MountainHeightProviderSingleton.SampleHeight(player.position);

                // wait a little so the player is not shot into space
                StartCoroutine(pTween.Wait(poopPlacementDelay, () =>
                {
                    var sg = Instantiate(gatePrefab, pos, Quaternion.identity, transform.parent);
                    sg.transform.SetParent(transform);

                }));
            }
        }
    }

    private bool GetPoopInputDown()
    {
        return (Input.GetKeyDown(KeyCode.F) || ControllerWrapper.allControllers.Any(c => c.GetPressDown(ControllerWrapper.ButtonMask.Grip)));
    }

    [DebugButton]
    private void SetOrderAndLevel()
    {
        var firstRight = false;
        for (int i = 0; i < transform.childCount; i++)
        {
            var t = transform.GetChild(i);
            var sp = t.GetComponent<SlalomPoleComponent>();

            if (i == 0)
            {
                firstRight = sp.isRight;
            }

            sp.levelId = levelId;
            sp.orderInLevel = i;
            sp.isRight = i % 2 == 0 ? firstRight : !firstRight; // alternate but keep first.

            if (i == 0)
            {
                sp.isStart = true;
                sp.isEnd = false;
            }
            else
            {
                sp.isStart = false;

                if (i == transform.childCount - 1)
                {
                    sp.isEnd = true;
                }
                else
                {
                    sp.isEnd = false;
                }
            }

            sp.RefreshSide();

        }
    }

    [DebugButton]
    void ReparentChildrenTool()
    {
        if (!gameObject.GetComponent<ReparentChildrenTool>())
            gameObject.AddComponent<ReparentChildrenTool>();
    }

    [DebugButton]
    void ReorderTransformsByZDirection()
    {
        var list = transform.GetChildren();
        list = list.OrderBy(t => t.position.z).ToList();
        for (int i = 0; i < list.Count; i++)
        {
            list[i].SetSiblingIndex(list.Count);
        }
    }



}