using System;
using Core;
using UnityEngine;
using System.Collections.Generic;
using VRCore;

public class SlalomManager : MonoBehaviour
{
    public static SlalomManager inst;

    private PlayerBase _player;
    public PlayerBase player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<PlayerBase>();
            }
            return _player;
        }
    }

    private static SlalomLevelScoringData scoringData = new SlalomLevelScoringData();

    void Awake()
    {
        inst = this;
        scoringData = new SlalomLevelScoringData()
        {
            curLevel = -1,
            startTime = 0,
            gatesCompleted = 0,
            lastGateId = 0,
        };
    }

    public static string GenerateTimeString(float completionTimeSeconds)
    {
        TimeSpan ct = new TimeSpan(0, 0, 0, Mathf.FloorToInt(completionTimeSeconds), (int)((completionTimeSeconds % 1f) * 1000));
        var s = ct.Minutes.ToString("D2") + ":" + ct.Seconds.ToString("D2") + "." + ct.Milliseconds.ToString("D3");
        return s;
    }

    /// <summary>
    /// When a player crosses a slalom pole.
    /// </summary>
    /// <param name="pole">just crossed it</param>
    /// <param name="p">the player who did it</param>
    /// <returns>true when pole is valid/part of the level, and score should be set.</returns>
    public bool PlayerCrossedGate(SlalomPoleComponent pole, PlayerBase p, ref SlalomLevelScoringData levelFinishData)
    {
        if (p == player)
        {
            // on start, resets the level data.
            if (pole.isStart)
            {
                scoringData = new SlalomLevelScoringData()
                {
                    curLevel = pole.levelId,
                    startTime = Time.time,
                    gatesCompleted = 0,
                    lastGateId = 0,
                };
            }
            // keep track of slalom pole order. can't go back ;)
            else if (pole.levelId == scoringData.curLevel)
            {
                if (pole.isEnd)
                {
                    var levelTime = Time.time - scoringData.startTime;
                    var levelGates = scoringData.gatesCompleted;
                    ScorePointsManager.instance.AddScore(p, Core.Score.ScoreType.SlalomCompletionTime, levelTime);
                    ScorePointsManager.instance.AddScore(p, Core.Score.ScoreType.SlalomGatesCompleted, levelGates);

                    levelFinishData = scoringData;
                }
                else
                {
                    if (pole.orderInLevel > scoringData.lastGateId)
                    {
                        scoringData.lastGateId = pole.orderInLevel;
                        scoringData.gatesCompleted++;
                    }
                }
            }
            else
            {
                // pole is not start, and it is not the same level as we currently are.
                // WRONG SLALOM POLE DUDERINO
                // feedback: 
                //      wrong sound
                //      some awkward bounce on the slalom pole
                //      some sign in the sky?
                return false;
            }
        }
        else
        {
            return false;
        }

        return true;
    }

    public struct SlalomLevelScoringData
    {
        public int curLevel; // level to score. gets set by start pole
        public float startTime; // set when crossing start pole
        public int gatesCompleted; // how many gates were completed valid (in the right order)
        public int lastGateId; // which gate was crossed last (starts at zero at start pole, increments with every gate, invalidates gates that are behind)
    }

}