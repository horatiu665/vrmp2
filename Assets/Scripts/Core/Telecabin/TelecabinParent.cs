﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class TelecabinParent : MonoBehaviour
{
    int lastChildCount = 0;

    [DebugButton]
    public void RefreshChildren()
    {
        TelecabinWire prevWire = null;
        var chi = transform.GetChildren();
        for (int i = 0; i < chi.Count; i++)
        {
            var wire = chi[i].GetComponentInChildren<TelecabinWire>();
            if (prevWire != null)
            {
                prevWire.target = wire.transform;
                prevWire.LookAndScale();
            }
            prevWire = wire;

            // last one look at self
            if (i == chi.Count - 1)
            {
                wire.target = wire.transform;
                wire.LookAndScale();
            }
        }
    }

    private void Update()
    {
        if (lastChildCount != transform.childCount)
        {
            lastChildCount = transform.childCount;
            RefreshChildren();
        }
    }

}