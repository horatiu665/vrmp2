﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class TelecabinWire : MonoBehaviour
{
    public Transform target;

    public bool inUpdate = false;

    [DebugButton]
    public void LookAndScale()
    {
        var dir = target.position - transform.position;
        transform.forward = dir;
        var ls = transform.localScale;
        ls.z = dir.magnitude;
        transform.localScale = ls;
    }

#if UNITY_EDITOR
    void Update()
    {
        if (inUpdate)
        {
            if (target != null)
            {
                LookAndScale();
            }

        }
    }
#endif
}