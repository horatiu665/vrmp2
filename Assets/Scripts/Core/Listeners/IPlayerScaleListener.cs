namespace Core
{
    using UnityEngine;

    public interface IPlayerScaleListener
    {
        void OnScaleChange(Vector3 newScale);
    }
}