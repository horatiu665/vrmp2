namespace Core
{
    using Apex.Services;
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    [Apex.ApexComponent("Preview Behaviours")]
    public class FlyPreviewBehaviour : VRPreviewBehaviourBase, IHandleMessage<VRInputPressDown>, IHandleMessage<VRInputPressUp>
    {
        [SerializeField]
        private bool _canToggleWithButton = false;

        [SerializeField]
        private VRInput _toggleButton = VRInput.Touchpad;

        [SerializeField, ReadOnly]
        private bool _isActive;

        private bool _activeLeft, _activeRight;

        private FlyVRController _flyVRController;

        private bool _cannotPressUpAfterPressDown = false;

        private SkiDragController _skiDragController;
        private bool _prevDragControllerState;

        [Header("Fly Mode params")]
        [SerializeField]
        private float _flyMass = 0.1f;

        [SerializeField]
        private float _flyDrag = 5;

        [SerializeField]
        private float _flyLocalScale = 0.1f;

        [SerializeField]
        private Vector2 _flyNearFarPlane = new Vector2(0.01f, 3000f);

        [SerializeField]
        private GameObject _wingLeft, _wingRight;

        private CapsuleCollider _capCol;

        private bool _isFlyModeEnabled;

        public bool isFlyModeEnabled
        {
            get
            {
                return _isFlyModeEnabled;
            }
        }

        // ===================== PREVIOUS VALUES =====================
        //private bool _prevDisallowEquipping;

        //private bool _prevDisallowSpawning;

        private float _prevMass, _prevDrag;
        private bool _prevIsKinematic, _prevGravity;

        private float _prevColliderRadius;

        private float _prevLocalScale;

        private Vector2 _prevNearFarPlane;
        // ===================== END PREVIOUS VALUES =====================

        /// <summary>
        /// Turns player into a fly
        /// </summary>
        /// <param name="input"></param>
        public override void OnPreviewEnter(VRPreviewChangeInput input)
        {
            if (input.hand.isLeft)
            {
                _activeLeft = true;
            }
            else
            {
                _activeRight = true;
            }

            if (_isActive)
            {
                return;
            }

            _isActive = true;

            // enable fly locomotion
            if (_flyVRController != null)
            {
                _flyVRController.enabled = true;
            }

            if (_skiDragController != null)
            {
                _prevDragControllerState = _skiDragController.enabled;
                _skiDragController.enabled = false;
            }

            EnableFlyMode();
        }

        /// <summary>
        /// Reverts the operations done in the Enter method
        /// </summary>
        /// <param name="input"></param>
        public override void OnPreviewExit(VRPreviewChangeInput input)
        {
            if (input.hand.isLeft)
            {
                _activeLeft = false;
            }
            else
            {
                _activeRight = false;
            }

            if (!_isActive)
            {
                return;
            }

            _isActive = false;

            if (_flyVRController != null)
            {
                _flyVRController.enabled = false;
            }

            if (_skiDragController != null)
            {
                _skiDragController.enabled = _prevDragControllerState;
            }

            DisableFlyMode();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _flyVRController = _player.transform.GetComponent<FlyVRController>();
            _skiDragController = _player.transform.GetComponent<SkiDragController>();
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPressUp>(this);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPressUp>(this);
        }

        public void Handle(VRInputPressUp message)
        {
            // if fly mode is enabled - then we can disable it
            if (isFlyModeEnabled)
            {
                // if preview behaviour is active
                if (_isActive)
                {
                    // if correct button
                    if (_canToggleWithButton && message.input == _toggleButton)
                    {
                        // if correct hand
                        if ((message.isLeft && _activeLeft) || (message.isRight && _activeRight))
                        {
                            // when this is true, return instead of disabling. we can disable afterwards.
                            if (_cannotPressUpAfterPressDown)
                            {
                                _cannotPressUpAfterPressDown = false;
                                return;
                            }
                            // disable fly mode
                            DisableFlyMode();
                        }
                    }
                }
            }
        }

        public void Handle(VRInputPressDown message)
        {
            // if fly mode is not enabled
            if (!isFlyModeEnabled)
            {
                // if preview behaviour is active
                if (_isActive)
                {
                    // if correct button
                    if (_canToggleWithButton && message.input == _toggleButton)
                    {
                        // if correct hand
                        if ((message.isLeft && _activeLeft) || (message.isRight && _activeRight))
                        {
                            // enable fly mode, and disallow disabling for the next pressUp
                            EnableFlyMode();
                            _cannotPressUpAfterPressDown = true;
                        }
                    }
                }
            }
        }

        public void EnableFlyMode()
        {
            _isFlyModeEnabled = true;

            var p = _player;

            //_prevDisallowEquipping = p.disallowEquipping;
            p.disallowEquipping = true;
            //_prevDisallowSpawning = p.disallowSpawning;
            p.disallowSpawning = true;

            // set RB mass to 0.1
            _prevMass = p.rb.mass;
            p.rb.mass = _flyMass;
            // set RB drag to 5
            _prevDrag = p.rb.drag;
            p.rb.drag = _flyDrag;
            // set RB kinematic to false
            _prevIsKinematic = p.rb.isKinematic;
            p.rb.isKinematic = false;
            // set RB gravity to false
            _prevGravity = p.rb.useGravity;
            p.rb.useGravity = false;

            // set capsule collider radius to 1 so we're like a ball
            if (_capCol == null)
            {
                _capCol = p.rb.GetComponent<CapsuleCollider>();
            }
            _prevColliderRadius = _capCol.radius;
            _capCol.radius = 1f;

            // store head position before scaling!
            var preScaleHeadPos = p.head.position;

            // set transform scale to V3.one * 0.1
            _prevLocalScale = p.transform.localScale.x;
            p.SetLocalScale(_flyLocalScale);

            // make head position stay the same after scaling
            var curHeadPos = p.head.position;
            p.position += preScaleHeadPos - curHeadPos;

            var camera = p.head.GetComponent<Camera>();
            if (camera != null)
            {
                _prevNearFarPlane = new Vector2(camera.nearClipPlane, camera.farClipPlane);
                camera.nearClipPlane = _flyNearFarPlane.x;
                camera.farClipPlane = _flyNearFarPlane.y;
            }

            // enable wings graphics
            _wingLeft.SetActive(true);
            _wingRight.SetActive(true);

            if (((INetPlayer)p).isLocal)
            {
                // There is no InputControllerDefault on server, so we can only do this on clients or singleplayer
                InputControllerDefault.instance.UnequipAll();
            }
        }

        public void DisableFlyMode()
        {
            _isFlyModeEnabled = false;

            var p = _player;

            p.disallowEquipping = false;
            p.disallowSpawning = false; // TODO: SHOULD USE PREVIOUS VALUES!
            //p.disallowEquipping = _prevDisallowEquipping;
            //p.disallowSpawning = _prevDisallowSpawning;

            p.rb.mass = _prevMass;
            p.rb.drag = _prevDrag;
            p.rb.isKinematic = _prevIsKinematic;
            p.rb.useGravity = _prevGravity;

            if (_capCol == null)
            {
                _capCol = p.rb.GetComponent<CapsuleCollider>();
            }
            _capCol.radius = _prevColliderRadius;

            var preScaleHeadPos = p.head.position;
            p.SetLocalScale(_prevLocalScale);

            var curHeadPos = p.head.position;
            p.position += preScaleHeadPos - curHeadPos;

            // make sure since we are scaling up that we don't go through the ground or whatever
            RaycastHit hit;
            if (Physics.Raycast(preScaleHeadPos, preScaleHeadPos - curHeadPos, out hit, (preScaleHeadPos - curHeadPos).magnitude, Layers.terrain))
            {
                p.position = hit.point;
            }

            var camera = p.head.GetComponent<Camera>();
            if (camera != null)
            {
                camera.nearClipPlane = _prevNearFarPlane.x;
                camera.farClipPlane = _prevNearFarPlane.y;
            }

            _wingLeft.SetActive(false);
            _wingRight.SetActive(false);
        }

    }
}