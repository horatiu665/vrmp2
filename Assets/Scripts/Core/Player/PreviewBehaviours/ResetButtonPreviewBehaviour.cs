namespace Core
{
    using Apex.Services;
    using Apex.WorldGeometry;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using VRCore;
    using VRCore.VRNetwork;

    [Apex.ApexComponent("Preview Behaviours")]
    public class ResetButtonPreviewBehaviour : VRPreviewBehaviourBase, IHandleMessage<VRInputPressDown>, IHandleMessage<VRInputPressUp>
    {
        [SerializeField]
        private VRInput _toggleButton = VRInput.Touchpad;

        private bool _isActive
        {
            get
            {
                return _activeLeft || _activeRight;
            }
        }

        private bool _activeLeft, _activeRight;

        private bool _resetting = false;

        /// <summary>
        /// Turns player into a fly
        /// </summary>
        /// <param name="input"></param>
        public override void OnPreviewEnter(VRPreviewChangeInput input)
        {
            if (input.hand.isLeft)
            {
                _activeLeft = true;
            }
            else
            {
                _activeRight = true;
            }

        }

        /// <summary>
        /// Reverts the operations done in the Enter method
        /// </summary>
        /// <param name="input"></param>
        public override void OnPreviewExit(VRPreviewChangeInput input)
        {
            if (input.hand.isLeft)
            {
                _activeLeft = false;
            }
            else
            {
                _activeRight = false;
            }

        }

        protected override void OnEnable()
        {
            base.OnEnable();
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPressUp>(this);
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPressUp>(this);
        }

        public void Handle(VRInputPressUp message)
        {
            // if preview behaviour is active
            if (_isActive)
            {
                // if correct button
                if (message.input == _toggleButton)
                {
                    // if correct hand
                    if ((message.isLeft && _activeLeft) || (message.isRight && _activeRight))
                    {
                        ResetLevel();
                    }
                }
            }
        }

        private void ResetLevel()
        {
            if (_resetting) return;

            _resetting = true;

            SceneManager.LoadSceneAsync(SceneManager.GetSceneAt(0).buildIndex, LoadSceneMode.Single);
            
        }

        public void Handle(VRInputPressDown message)
        {
            // if preview behaviour is active
            if (_isActive)
            {
                // if correct button
                if (message.input == _toggleButton)
                {
                    // if correct hand
                    if ((message.isLeft && _activeLeft) || (message.isRight && _activeRight))
                    {

                    }
                }
            }
        }

    }
}