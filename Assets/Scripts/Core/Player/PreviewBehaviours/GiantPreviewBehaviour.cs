namespace Core
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    [Apex.ApexComponent("Preview Behaviours")]
    public class GiantPreviewBehaviour : VRPreviewBehaviourBase
    {
        private HeightScaler _hs;
        //private bool _prevDisallowSpawning;

        public override void OnPreviewEnter(VRPreviewChangeInput input)
        {
            
            var p = input.player;

            //_prevDisallowSpawning = p.disallowSpawning;
            p.disallowSpawning = true;

            // enable height scaler
            if (_hs == null)
            {
                _hs = p.transform.GetComponent<HeightScaler>();
            }

            if (_hs != null)
            {
                _hs.enabled = true;
            }

            if (((INetPlayer)p).isLocal)
            {
                // There is no InputControllerDefault on server, so we can only do this on clients or singleplayer
                InputControllerDefault.instance.UnequipAll();
            }
        }
        
        public override void OnPreviewExit(VRPreviewChangeInput input)
        {
            
            var p = input.player;

            p.disallowSpawning = false; // TODO: SHOULD USE PREVIOUS VALUES!
            //p.disallowSpawning = _prevDisallowSpawning;

            if (_hs == null)
            {
                _hs = p.transform.GetComponent<HeightScaler>();
            }

            if (_hs != null)
            {
                // disable height scaler, lerp scale to normal scale.
                _hs.enabled = false;
            }

            p.SetLocalScale(1f);
        }
    }
}