namespace Core
{
    using System;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Preview Behaviours")]
    public class SkiPreviewBehaviour : VRPreviewBehaviourBase
    {
        [SerializeField]
        private GameObject _skisGameObject = null;

        [SerializeField, ReadOnly]
        private bool _isActive;

        private readonly Type[] _skiLocomotionTypes = new Type[]
        {
            // drag controller is needed for regular player anyway, to prevent sliding and to allow falling
            //typeof(SkiDragController),
            typeof(SkiHeadSteeringStrafe),
            typeof(SkiPlayerGlide),
            typeof(SkiPlayerJump),
            typeof(SkiPlayerSounds),
            typeof(SkiPlayerTurningSkiGraphics)
        };

        public override void OnPreviewEnter(VRPreviewChangeInput input)
        {
            if (!enabled)
            {
                return;
            }
            if (_isActive)
            {
                return;
            }

            _isActive = true;

            var p = input.player;
            for (int i = 0; i < _skiLocomotionTypes.Length; i++)
            {
                var comp = p.transform.GetComponent(_skiLocomotionTypes[i]) as MonoBehaviour;
                if (comp != null)
                {
                    comp.enabled = true;
                }
            }

            if (_skisGameObject != null)
            {
                _skisGameObject.SetActive(true);
            }
        }

        public override void OnPreviewExit(VRPreviewChangeInput input)
        {
            if (!_isActive)
            {
                return;
            }

            _isActive = false;

            var p = input.player;
            for (int i = 0; i < _skiLocomotionTypes.Length; i++)
            {
                var comp = p.transform.GetComponent(_skiLocomotionTypes[i]) as MonoBehaviour;
                if (comp != null)
                {
                    comp.enabled = false;
                }
            }

            if (_skisGameObject != null)
            {
                _skisGameObject.SetActive(false);
            }
        }
    }
}