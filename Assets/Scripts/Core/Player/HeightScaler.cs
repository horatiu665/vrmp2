namespace Core
{
    using System.Collections;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    public sealed class HeightScaler : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private AnimationCurve _heightToScale = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        [SerializeField]
        private bool _scaleAtTheHead = true;

        [SerializeField]
        private bool _active = false;

        /// <summary>
        /// Used as min. scale, avoids divide by zero
        /// </summary>
        [SerializeField]
        private float _epsilon = 0.001f;

        [SerializeField]
        private float _startFixMultiplier = 1;

        [Header("Slowdown Curve")]
        [SerializeField]
        private AnimationCurve _slowdownMaxHeightMultiplier = AnimationCurve.Linear(0, 1, 1, 1);

        // initialized as max height.
        private float _height = 2f;

        // initialized as max height.
        private float _oldHeight = 2f;

        private bool _updatedInStart;
        private IVRPlayer _player;

        private Transform head
        {
            get { return _player.head; }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;
        }

        private void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IVRPlayer>());
            }
        }

        private IEnumerator Start()
        {
            if (!_active)
            {
                _updatedInStart = true;
                yield break;
            }

            // move playspace so it will start above the initial position
            while (_player == null)
                yield return 0;

            var oldHeadPos = head.position;

            // initialize height as max height.
            _oldHeight = 2;

            // do a scale update here, so we can move parent for the first (potentially drastic?) pos update
            UpdateScaling();

            var newPos = head.position;
            var delta = newPos - oldHeadPos;

            // move parent so head is at the right pos
            _player.transform.position += delta * _startFixMultiplier;

            yield return 0;
            _updatedInStart = true;
        }

        private void FixedUpdate()
        {
            if (!_updatedInStart || !_active)
            {
                return;
            }

            UpdateScaling();
        }

        private void UpdateScaling()
        {
            // relative height to the base of the StreamVR rig
            _height = head.localPosition.y;

            // old scale (last frame)
            var oldHeightScale = _heightToScale.Evaluate(_oldHeight);
            // new scale
            var scale = _heightToScale.Evaluate(_height);

            if (_player is IPlayer)
            {
                var esp = _slowdownMaxHeightMultiplier.Evaluate((_player as IPlayer).slowdownParam);
                scale *= esp;
                oldHeightScale *= esp;
            }

            // do not div by zero
            if (scale < _epsilon)
                scale = _epsilon;
            if (oldHeightScale < _epsilon)
                oldHeightScale = _epsilon;

            if (_scaleAtTheHead)
            {
                // new object at head's position
                Vector3 newGOpos = head.position;
                // put it on the floor
                newGOpos.y = _player.transform.position.y;
                // new obj, this will scale our room
                GameObject tempObj = VRHelper.dummy;
                // put obj under head, so we scale around head
                tempObj.transform.position = newGOpos;
                // scale new obj as old scale
                tempObj.transform.localScale = Vector3.one * oldHeightScale;
                // scale room temporarily as old scale
                _player.transform.localScale = Vector3.one * oldHeightScale;
                // save old parent of room
                Transform oldParent = _player.transform.parent;
                // set parent of room to the new object (thereby making local scale of room be V3.one)
                _player.transform.SetParent(tempObj.transform);
                // set local scale of room to new scale
                tempObj.transform.localScale = Vector3.one * scale;
                // set old parent back, and it's like we scaled the object around a new location
                _player.transform.SetParent(oldParent);
                // destroy our favorite gameobject
                tempObj.SetActive(false);
            }
            else
            {
                _player.transform.localScale = Vector3.one * scale;
            }

            _player.UpdateConnectedAnchors();

            // save old height for next frame
            _oldHeight = _height;
        }

        /// <summary>
        /// Returns the scale that would be set right now by the script
        /// </summary>
        /// <returns>_heightToScale.Evaluate(_height);</returns>
        [System.Obsolete("Apparently not used anywhere, candidate for removal?")]
        public float GetTargetScale()
        {
            // relative height to the base of the StreamVR rig
            _height = head.localPosition.y;
            return _heightToScale.Evaluate(_height);
        }
    }
}