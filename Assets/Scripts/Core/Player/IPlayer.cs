namespace Core
{
    using System.Collections.Generic;
    using Score;
    using UnityEngine;
    using VRCore.VRNetwork;

    public interface IPlayer : INetPlayer
    {
        Transform skis { get; }
        Transform leftGlove { get; }
        Transform rightGlove { get; }

        string name { get; set; }
        Color color { get; set; }

        bool grounded { get; }
        RaycastHit groundedRay { get; }

        Vector3 startPosition { get; }
        Vector3 lastCheckpoint { get; }

        float headSteeringParam { get; set; }
        float headForwardParam { get; set; }
        float slowdownParam { get; }
        
        RigidbodySyncComponent headSync { get; }
        RigidbodySyncComponent bodySync { get; }

        void AddForce(object sender, Vector3 velocity, ForceMode mode = ForceMode.VelocityChange);
        void AddDragNextFrame(float drag);

        void UpdateScale(Vector3 localScale);

        void SetupRigidbodies(int headSyncId, int bodySyncId);

    }
}