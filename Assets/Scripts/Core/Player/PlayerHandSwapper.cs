namespace Core
{
    using Apex.Services;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Player")]
    public sealed class PlayerHandSwapper : MonoBehaviour, IHandleMessage<VRInputPressDown>
    {
        [SerializeField]
        private VRInput _swapHandsInput = VRInput.ApplicationMenu;

        private void OnEnable()
        {
            GameServices.messageBus.Subscribe(this);
        }

        private void OnDisable()
        {
            GameServices.messageBus.Unsubscribe(this);
        }

        public void Handle(VRInputPressDown message)
        {
            if (message.input == _swapHandsInput)
            {
                InputControllerDefault.instance.UnequipAll();
                ((SpawnManagerPreviewable)SpawnManagerPreviewable.instance).SwapPreviews();
            }
        }
    }
}