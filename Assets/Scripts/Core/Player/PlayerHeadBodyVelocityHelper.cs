using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerHeadBodyVelocityHelper : MonoBehaviour, IOriginShifter
{
    public PlayerBase player;
    private Rigidbody r;
    private Vector3 oldPos;

    private void OnEnable()
    {
        r = GetComponent<Rigidbody>();
        OriginShiftManager.OriginShiftersAdd(this);
    }

    private void OnDisable()
    {
        OriginShiftManager.OriginShiftersRemove(this);
    }

    private void FixedUpdate()
    {
        if (player != null)
        {
            //    if (!player.isLocal)
            //    {
            var delta = player.position - oldPos;
            var vel = delta / Time.fixedDeltaTime;
            // move this rb in the opposite direction of its parent's movement, as if it wasn't parented...
            r.AddForce(-vel);
            oldPos = player.position;
            //    }
        }
    }

    public void OnWorldMove(Vector3 originShiftDelta)
    {
        oldPos -= originShiftDelta;
    }
}
