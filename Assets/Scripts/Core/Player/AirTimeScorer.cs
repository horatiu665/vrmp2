namespace Core.Player
{
    using Core;
    using Core.Level;
    using Helpers;
    using Score;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Player")]
    [RequireComponent(typeof(IPlayer))]
    public sealed class AirTimeScorer : MonoBehaviour, IVRPlayerStartListener, IOriginShifter
    {
        [SerializeField, ReadOnly]
        private float _airTime = 0f;

        [SerializeField]
        private float _timeScoreMultiplier = 1f;

        [SerializeField, ReadOnly]
        private float _distanceTraveled = 0f;

        [SerializeField]
        private float _distanceScoreMultiplier = 1f;
        private Vector3 oldPos, startGlobalPos;

        [SerializeField]
        private bool _showScoreWhileFlying = true;

        [SerializeField]
        private bool _showScoreOnLanding = true;

        private IPlayer _skier;
        private IScore _scoreUI;

        private TrampolineJump _trampolineJump = TrampolineJump.NotOnTrampoline;

        //add a little delay to avoid resetting the scorer if we are grounded shortly after take off, by e.g. bumping into something
        private float _nextGroundedTime = 0f;

        private enum TrampolineJump
        {
            BeginGroundedJump,
            Jumping,
            NotOnTrampoline
        }

        private void OnEnable()
        {
            if (_skier != null)
            {
                return;
            }

            OnPlayerStart(this.GetComponent<IPlayer>());
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            if (_skier != null)
            {
                return;
            }

            _skier = (IPlayer)player;
        }

        private void Update()
        {
            // if score is supported in the score system
            if (!GameModeManager.instance.IsScoreAllowed(ScoreType.JumpScore))
            {
                return;
            }

            var grounded = _skier.grounded;
            var flying = !grounded;

            if (flying && (_trampolineJump == TrampolineJump.BeginGroundedJump || _trampolineJump == TrampolineJump.Jumping))
            {
                // Only count score after we jumped from a trampoline...
                _trampolineJump = TrampolineJump.Jumping;

                var deltaTraveled = OriginShiftManager.LocalToGlobalPos(_skier.position) - startGlobalPos;
                deltaTraveled.y = 0;

                // player is not grounded, so increment scoring
                _airTime += Time.deltaTime * _timeScoreMultiplier;
                _distanceTraveled = deltaTraveled.magnitude * _distanceScoreMultiplier;

                if (_skier.isPlayer)
                {
                    if (_showScoreWhileFlying)
                    {
                        if (_scoreUI == null)
                        {
                            _scoreUI = ScoreManager.instance.Get(ScoreType.JumpScore, "0", _skier, _skier.transform.position + _skier.transform.forward);
                            _scoreUI.paused = true;
                        }

                        _scoreUI.scoreText = CalcuScore(_distanceTraveled) + "m";
                    }
                }
            }
            else if (_airTime > 0f)
            {
                // We are in jump mode and have just become grounded. So we stop jumping.
                if (grounded && _trampolineJump == TrampolineJump.Jumping && Time.time > _nextGroundedTime)
                {
                    _trampolineJump = TrampolineJump.NotOnTrampoline;
                }

                if (_showScoreWhileFlying)
                {
                    if (_scoreUI != null)
                    {
                        _scoreUI.paused = false;
                        _scoreUI = null;
                    }
                }
                else if (_showScoreOnLanding)
                {
                    if (_scoreUI == null)
                    {
                        _scoreUI = ScoreManager.instance.Get(ScoreType.JumpScore, "0", _skier, _skier.transform.position + _skier.transform.forward);
                        _scoreUI.scoreText = CalcuScore(_distanceTraveled) + "m";
                        _scoreUI.paused = false;
                        _scoreUI = null;

                    }
                }

                //_skier.AddScore(ScoreType.AirTime, Mathf.RoundToInt(_score));
                ScorePointsManager.instance.AddScore(_skier, ScoreType.JumpScore, CalcuScore(_distanceTraveled));
                _airTime = 0f;
                _distanceTraveled = 0f;


            }
        }

        public float CalcuScore(float a)
        {
            return Mathf.RoundToInt(a);
        }

        public void BeginTrampolineJump(JumpTrigger jump)
        {
            if (_skier == null)
            {
                return;
            }

            if (_skier.grounded)
            {
                _trampolineJump = TrampolineJump.BeginGroundedJump;
                _nextGroundedTime = Time.time + 1f;
            }
            else
            {
                _trampolineJump = TrampolineJump.Jumping;
            }

            _distanceTraveled = 0;
            oldPos = _skier.position;
            startGlobalPos = OriginShiftManager.LocalToGlobalPos(jump.jumpStartPosition.position);

        }

        public void OnWorldMove(Vector3 originShiftDelta)
        {
            oldPos -= originShiftDelta;
        }

        // CANCEL JUMP WHEN DOING THE DO
        private void OnTriggerEnter(Collider collider)
        {
            if (collider.gameObject.GetComponentInParent<TeleportToToiletRainboe>())
            {
                _trampolineJump = TrampolineJump.NotOnTrampoline;

                // if this is the score landing but we hit a teleport trigger instead, show the :D face
                if (_scoreUI != null)
                {
                    // font is so shitty
                    _scoreUI.scoreText = " :D";
                    _scoreUI.paused = false;
                    _scoreUI = null;
                }

                _airTime = 0f;
                _distanceTraveled = 0f;

            }
        }

        // CANCEL JUMP WHEN DOING THE DO
        private void OnCollisionEnter(Collision collision)
        {
            OnTriggerEnter(collision.collider);

        }
    }
}