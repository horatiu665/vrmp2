namespace Core
{
    using System;
    using System.Collections.Generic;
    using Apex.Services;
    using Apex.WorldGeometry;
    using Score;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;
    using VRCore.VRNetwork;

    [Apex.ApexComponent("Player")]
    [RequireComponent(typeof(Rigidbody), typeof(Collider))]
    public class PlayerBase : NetPlayerBase, IPlayer
    {
        [Header("SkiJump Player")]
        [SerializeField]
        private Transform _skis = null;

        [SerializeField]
        private Transform _leftGlove = null;

        [SerializeField]
        private Transform _rightGlove = null;

        /// <summary>
        /// Renderers which should be colored when changing player color.
        /// </summary>
        [Tooltip("Renderers which should be colored when changing player color.")]
        [SerializeField, FormerlySerializedAs("_coloredSprites")]
        private List<Renderer> _coloredRenderers = new List<Renderer>();

        [SerializeField, Range(0.01f, 10f)]
        private float _groundedRaycastDistance = 1.5f;

        [Header("Network: unicorn head and body")]
        public bool spawnHeadAndBody = true;

        // init to green unicorn
        [SerializeField]
        private Color _colour = new Color(27f / 255, 142f / 255, 99f / 255, 1);

        [Header("Debugging")]

        [SerializeField, ReadOnly]
        private bool _isAI;

        [Space]
        [SerializeField, ReadOnly]
        private bool _grounded = true;
        public bool grounded
        {
            get { return _grounded; }
        }

        private RaycastHit _groundedRay;
        public RaycastHit groundedRay
        {
            get { return _groundedRay; }
        }

        [Space]
        [SerializeField, ReadOnly]
        private Vector3 _startPosition;

        [SerializeField, ReadOnly]
        private Vector3 _lastCheckpoint;

        private SkiDragController _dragController;
        private PlayerNetSyncDefault _sync;
        private IPlayerScaleListener[] _scaleListeners;

        public Transform skis
        {
            get { return _skis; }
        }

        public Transform leftGlove
        {
            get { return _leftGlove; }
        }

        public Transform rightGlove
        {
            get { return _rightGlove; }
        }

        public Color color
        {
            get
            {
                return _colour;
            }

            set
            {
                _colour = value;
                RefreshColor();
            }
        }

        public override bool isPlayer
        {
            get { return !_isAI; }
        }

        public Vector3 startPosition
        {
            get { return _startPosition; }
        }

        public Vector3 lastCheckpoint
        {
            get { return _lastCheckpoint; }
        }

        public float headSteeringParam
        {
            get;
            set;
        }

        public float headForwardParam
        {
            get;
            set;
        }

        public float slowdownParam
        {
            get
            {
                return 0f;
            }
        }

        public RigidbodySyncComponent headSync
        {
            get;
            private set;
        }

        public RigidbodySyncComponent bodySync
        {
            get;
            private set;
        }

        protected override void Awake()
        {
            base.Awake();
            _dragController = this.GetComponent<SkiDragController>();
            _sync = this.GetComponent<PlayerNetSyncDefault>();

            _grounded = true;

            if (_skis == null)
            {
                Debug.LogError(this.ToString() + " is missing a reference to the skis' transform");
            }
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            _scaleListeners = this.GetComponentsInChildren<IPlayerScaleListener>();

            this.LinkPlayer(this.GetComponent<Rigidbody>());
        }

        protected override void Start()
        {
            base.Start();
            _startPosition = _lastCheckpoint = this.position;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            this.UnlinkPlayer(this.GetComponent<Rigidbody>());
        }

        /// <summary>
        /// The check for grounded is in the update method as this ensures the sampling happens from the gameObject's position -> fixed update seems to be somewhat offset (delayed)
        /// </summary>
        private void Update()
        {
            if (!_isLocal)
            {
                return;
            }

            _grounded = Physics.Raycast(this.position + Vector3.up, Vector3.down, out _groundedRay, _groundedRaycastDistance + 1f, Layers.terrain); // + 1 to distance is to account for up vector in origin
        }

        private void RefreshColor()
        {
            var count = _coloredRenderers.Count;
            for (int i = 0; i < count; i++)
            {
                var spriteRenderer = _coloredRenderers[i] as SpriteRenderer;
                if (spriteRenderer != null)
                {
                    spriteRenderer.color = _colour;
                }
                else if (_coloredRenderers[i] != null)
                {
                    _coloredRenderers[i].material.color = _colour;
                    _coloredRenderers[i].material.SetColor("_EmissionColor", _colour);
                }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
                else
                {
                    Debug.LogWarning(this.ToString() + " has no colored renderers at all, even though the list has elements (null elements), nothing to color!");
                }
#endif
            }

        }

        public void AddForce(object sender, Vector3 force, ForceMode mode = ForceMode.VelocityChange)
        {
            velocityController.AddForce(sender, force, mode);
        }

        public override void HandleVRBodyUpdate(VRBodyUpdateData data)
        {
            //Debug.Log(this.ToString() + " HandleVRBodyUpdate");
            if (_sync == null)
            {
                Debug.LogWarning(this.ToString() + " HandleVRBodyUpdate is missing a SkiNetSync component, cannot handle update");
                return;
            }

            _sync.HandleUpdate(data);
        }

        public override void HandleOriginShift(Vector3 originShiftDelta)
        {
            _sync.HandleOriginShift(originShiftDelta);
        }

        public void AddDragNextFrame(float drag)
        {
            if (_dragController != null)
            {
                _dragController.AddDragNextFrame(drag);
            }
        }

        public override void SetHeadPosition(float x, float y)
        {
            head.position = skis.position + (Vector3.up * y) + (skis.right * x);
        }

        public void UpdateScale(Vector3 localScale)
        {
            for (int i = 0; i < _scaleListeners.Length; i++)
            {
                _scaleListeners[i].OnScaleChange(localScale);
            }

            UpdateConnectedAnchors();
        }

        public void SetupRigidbodies(int headSyncId, int bodySyncId)
        {
            var headAnchorRb = _head.GetComponentInChildren<Rigidbody>();
            if (headAnchorRb == null)
            {
                throw new ArgumentNullException("_head", this.ToString() + " player is missing a rigidbody somewhere under or on the head (" + _head.ToString() + ").");
            }

            // first try to find headSync rigidbody, maybe it was already spawned. if we are a client.
            if (headSync == null)
            {
                if (NetServices.isClient)
                {
                    headSync = ClientRigidbodySyncManager.instance.Get(headSyncId);
                }
            }

            // if still null, spawn headSync rigidbody
            if (headSync == null)
            {
                var head = VRPrefabManager.instance.Spawn(VRPrefabType.PlayerHeadRigidbody, _head.transform.position, _head.transform.rotation);
                headSync = head.GetOrAddComponent<RigidbodySyncComponent>();
            }

            headSync.transform.SetParent(this.transform);
            //headSync.transform.SetParent(null);

            headSync.Initialize(headSyncId);

            PlayerHeadBodyVelocityHelper velHelpHead;
            if (NetServices.isServer)
            {
                velHelpHead = headSync.GetOrAddComponent<PlayerHeadBodyVelocityHelper>();
            }
            else
            {
                velHelpHead = headSync.GetComponent<PlayerHeadBodyVelocityHelper>();
            }
            if (velHelpHead != null)
            {
                velHelpHead.player = this;
            }

            var headJoint = headSync.GetOrAddComponent<ConfigurableJoint>();
            headJoint.connectedBody = headAnchorRb;

            // first try to find bodySync, cause maybe it was spawned
            if (bodySync == null)
            {
                if (NetServices.isClient)
                {
                    bodySync = ClientRigidbodySyncManager.instance.Get(bodySyncId);
                }
            }

            // if still null, we didn't find it, therefore spawn bodySync
            if (bodySync == null)
            {
                var body = VRPrefabManager.instance.Spawn(VRPrefabType.PlayerBodyRigidbody, _head.transform.position, _head.transform.rotation);
                bodySync = body.GetOrAddComponent<RigidbodySyncComponent>();
            }
            bodySync.transform.SetParent(_head);
            bodySync.transform.localPosition = new Vector3(0, -0.861f, -0.16f);
            bodySync.transform.localEulerAngles = new Vector3(-99f, 0, 0);

            bodySync.transform.SetParent(this.transform);
            //bodySync.transform.SetParent(null);

            bodySync.Initialize(bodySyncId);

            PlayerHeadBodyVelocityHelper velHelpBody;
            if (NetServices.isServer)
            {
                velHelpBody = bodySync.GetOrAddComponent<PlayerHeadBodyVelocityHelper>();
            }
            else
            {
                velHelpBody = bodySync.GetComponent<PlayerHeadBodyVelocityHelper>();
            }
            if (velHelpBody != null)
            {
                velHelpBody.player = this;
            }

            var headRb = headSync.GetOrAddComponent<Rigidbody>();
            var bodyJoint = bodySync.GetOrAddComponent<ConfigurableJoint>();
            bodyJoint.connectedBody = headRb;

            // set layers depending on local, remote or server
            if (isLocal)
            {
                headRb.gameObject.layer = LayerMask.NameToLayer("NoCollisions");
                var bodyRb = bodySync.GetOrAddComponent<Rigidbody>();
                bodyRb.gameObject.layer = LayerMask.NameToLayer("NoCollisions");
            }

            for (int i = 0; i < headSync.transform.childCount; i++)
            {
                // local: not visible in VR and shadows only.
                var ct = headSync.transform.GetChild(i);
                if (isLocal && ct.name.Contains("NotVisibleInVR"))
                {
                    ct.gameObject.layer = LayerMask.NameToLayer("NotVisibleInVR");
                }
                else
                {
                    // remote and server: everybody is default layer
                    ct.gameObject.layer = LayerMask.NameToLayer("Default");
                }
            }
            for (int i = 0; i < bodySync.transform.childCount; i++)
            {
                var ct = bodySync.transform.GetChild(i);
                if (isLocal && ct.name.Contains("NotVisibleInVR"))
                {
                    ct.gameObject.layer = LayerMask.NameToLayer("NotVisibleInVR");
                }
                else
                {
                    // remote and server: everybody is default layer
                    ct.gameObject.layer = LayerMask.NameToLayer("Default");
                }
            }

            // add joints to the _joints list so they can be updated every frame
            _joints.Add(headJoint);
            _joints.Add(bodyJoint);


            // Add the renderers on body and head and refresh color afterwards to apply
            var headRenderers = headSync.GetOrAddComponent<PlayerColorRenderersComponent>().renderers;
            _coloredRenderers.AddRange(headRenderers);

            var bodyRenderers = bodySync.GetOrAddComponent<PlayerColorRenderersComponent>().renderers;
            _coloredRenderers.AddRange(bodyRenderers);

            RefreshColor();

            if (!spawnHeadAndBody)
            {
                headSync.gameObject.SetActive(false);
                bodySync.gameObject.SetActive(false);
            }
        }
    }
}