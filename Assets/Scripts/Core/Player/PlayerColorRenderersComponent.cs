﻿namespace Core
{
    using UnityEngine;

    public sealed class PlayerColorRenderersComponent : MonoBehaviour
    {
        public Renderer[] renderers = new Renderer[0];
    }
}
