using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkiPlayerForceOnSlope : MonoBehaviour
{
    public LayerMask groundLayer;
    
    public float maxDistanceDown = 3000;

    bool weAreAbove = false;

    Vector3 hitPT;
    
    private void FixedUpdate()
    {
        MoveToGround();
    }

    private void MoveToGround()
    {
        RaycastHit hit;
        if (!Physics.Raycast(transform.position + Vector3.up, Vector3.down, out hit, maxDistanceDown, groundLayer))
        {
            if (Physics.Raycast(transform.position + Vector3.up * maxDistanceDown, Vector3.down, out hit, maxDistanceDown * 2, groundLayer))
            {
                transform.position += Vector3.up * (hit.point.y - transform.position.y);
                hitPT = hit.point;
                weAreAbove = true;
            }
            else
            {
                // fuck
                weAreAbove = false;
            }
        }
        else
        {
            weAreAbove = true;
            hitPT = hit.point;
        }
    }

    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = weAreAbove ? Color.blue : Color.red;
    //    Gizmos.DrawLine(transform.position, hitPT);
    //}
    
}
