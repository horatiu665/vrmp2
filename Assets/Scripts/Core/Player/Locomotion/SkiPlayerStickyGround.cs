﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;


public class SkiPlayerStickyGround : MonoBehaviour
{
    private IPlayer _player;
    public IPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = GetComponent<IPlayer>();
            }
            return _player;
        }
    }

    [SerializeField, ReadOnly]
    private bool _forceGrounded;

    private Queue<NormalData> normals = new Queue<NormalData>();

    private struct NormalData
    {
        public Vector3 normal;
        public Vector3 right; // player.skis.right at the normal's moment

        public NormalData(Vector3 normal, Vector3 right)
        {
            this.normal = normal;
            this.right = right;
        }
    }

    public bool forceGrounded
    {
        get
        {
            return _forceGrounded;
        }
        private set
        {
            _forceGrounded = value;
        }
    }

    [Header("Params")]
    public float minSpeedForGrounding = 5f;

    /// <summary>
    /// Max angle in degrees between old ground normal and new ground normal, for the player to be stuck to the ground instead of jumping
    /// </summary>
    [Tooltip("Max angle in degrees between old ground normal and new ground normal, for the player to be stuck to the ground instead of jumping")]
    public float maxAngleDegForGrounding = 25f;

    public AnimationCurve angleToVelocityClamping = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(0.8f, 0, 0, 0), new Keyframe(1f, 1f, 0, 0) } };

    public bool setPosition, setVelocity;

    [Header("Effect timing")]
    public bool onUpdate;

    public bool onFixedUpdate;

    [Range(1, 10)]
    public int checkFramesBehind = 1;

    public int debugFrameCount = 250;
    int frameCount;
    private Queue<DebugClassShit> debugShit = new Queue<DebugClassShit>();

    private class DebugClassShit
    {
        public float angle;
        public Vector3 pos;
        public Vector3 normal;
        public int frame;
        public bool update;
        internal bool forceGrounded;
    }

    private void OnEnable()
    {
        // DEBUG
        if (debugFrameCount > 0)
        {
            for (int i = 0; i < debugFrameCount; i++)
            {
                debugShit.Enqueue(new DebugClassShit());
            }
        }
    }

    private void Update()
    {
        checkFramesBehind = Mathf.Clamp(checkFramesBehind, 1, 10);
        if (normals.Count != checkFramesBehind)
        {
            normals = new Queue<NormalData>(checkFramesBehind);
            for (int i = 0; i < checkFramesBehind; i++)
            {
                normals.Enqueue(new NormalData(Vector3.up, Vector3.right));
            }
        }

        // DEBUG
        if (debugFrameCount > 0)
        {
            debugFrameCount = Mathf.Min(debugFrameCount, 10000);
            while (debugShit.Count < this.debugFrameCount)
            {
                debugShit.Enqueue(new DebugClassShit());
            }

        }

        if (onUpdate)
        {
            SetOnGround(true);
        }

        frameCount++;
    }

    private void FixedUpdate()
    {
        if (onFixedUpdate)
        {
            SetOnGround(false);
        }
    }

    private void SetOnGround(bool update)
    {
        float clamping = 0f;
        _forceGrounded = false;
        // set force grounded based on pos delta and ground delta. 
        // if we are moving over a bumpy terrain, we should be allowed to fly to smoothen it out. 
        // if we are falling off a cliff, we should be allowed to fly down.
        // if we jump off a trampoline, we should be allowed to fly until we land
        // if we are moving over a smooth terrain, we should be stuck to it. potentially with smoothing
        if (player.grounded && player.velocityController.speed > minSpeedForGrounding)
        {
            var newNormal = player.groundedRay.normal;

            normals.Enqueue(new NormalData(newNormal, player.skis.right)); // min length of normals.Count is 1, so this makes it minimum 2 for the next foreach. meaning we always have 1 comparison of normals.
            float angleDown = float.MinValue;

            NormalData normalOldest = new NormalData();
            NormalData normalSecond = new NormalData();
            bool first = true;
            foreach (var nn in normals)
            {
                // skip one normal, make it the second one because the oldest one will take its value next comparison.
                if (first)
                {
                    normalSecond = nn;
                    first = false;
                    continue;
                }
                normalOldest = normalSecond;
                normalSecond = nn;

                angleDown = Mathf.Max(angleDown, Vector3.SignedAngle(normalOldest.normal, normalSecond.normal, normalOldest.right));
            }

            // when negative angleDown, means we are going uphill.
            if (angleDown < maxAngleDegForGrounding)
            {
                _forceGrounded = true;
                // 1 when close to zero (meaning we CLAMP THAT SHIT), and 0 when close to max angle (meaning we stop clamping when angle is over the max angle limit, and let the player flyyyyy)
                clamping = angleToVelocityClamping.Evaluate(Mathf.Clamp01(angleDown / maxAngleDegForGrounding));
            }

            // DEBUG
            if (debugFrameCount > 0)
            {
                var ds = debugShit.Dequeue();
                ds.angle = angleDown;
                ds.pos = player.position;
                ds.normal = player.groundedRay.normal;
                ds.frame = frameCount;
                ds.update = update;
                ds.forceGrounded = _forceGrounded;
                debugShit.Enqueue(ds);

            }

            // remove oldest normal from beginning of queue.
            normals.Dequeue();
        }

        if (_forceGrounded)
        {
            if (player.grounded)
            {
                var velOnPlane = Vector3.ProjectOnPlane(player.velocity, player.groundedRay.normal);
                var dotOnNormal = Vector3.Dot(player.velocity, player.groundedRay.normal);

                if (setVelocity)
                {
                    // only allow downwards velocity but not upwards (never leave the PLANE)
                    player.velocity = Vector3.Lerp(player.velocity, velOnPlane + player.groundedRay.normal * Mathf.Clamp(dotOnNormal, float.MinValue, 0f), clamping);
                }

                if (setPosition)
                {
                    var pos = player.position;
                    pos.y = player.groundedRay.point.y;
                    player.position = pos;
                }

            }
            else
            {
                // we must ground player manually cause he's too far from the ground! :( weird shit
            }
        }
        else
        {
            // DEBUG
            if (debugFrameCount > 0)
            {
                var ds = debugShit.Dequeue();
                ds.angle = 180;
                ds.pos = player.position;
                ds.normal = player.transform.up * 0.3f;
                ds.frame = frameCount;
                ds.update = update;
                ds.forceGrounded = false;
                debugShit.Enqueue(ds);
            }
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {
        // DEBUG
        if (debugFrameCount > 0)
        {
            int frame = 0;
            float offset = 0f;
            var handleStyle = new GUIStyle(GUI.skin.label);
            var fs = handleStyle.fontSize;
            foreach (var ds in debugShit)
            {
                if (ds.frame == frame)
                {
                    offset += 0.05f;
                }
                else
                {
                    offset = 0f;
                    frame = ds.frame;
                }

                Gizmos.color = ds.forceGrounded ? Colors.Red : Colors.Yellow * 0.7f;
                Gizmos.DrawRay(ds.pos, ds.normal * 2);

                handleStyle.normal.textColor = Gizmos.color;
                if (ds.angle != 0)
                {
                    UnityEditor.Handles.Label(ds.pos + ds.normal * (2.2f + offset), ds.angle.ToString("F1"), handleStyle);
                }
                
            }
        }
    }
#endif

}
