namespace Core
{
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(Rigidbody), typeof(IPlayer))]
    public class SkiPlayerGlide : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private Vector3 _globalGlideVector = new Vector3(0f, 1f, 0f);

        [SerializeField]
        private Vector3 _localGlideVector = new Vector3(0f, 0f, 0f);

        [SerializeField]
        private float _glideForceMultiplier = 6f;

        [SerializeField]
        private float _glideAreaMinThreshold = 0f;

        [SerializeField]
        private ForceMode _glideForceMode = ForceMode.Force;

        [SerializeField]
        private GlideMethod _glideMethod = GlideMethod.HeightDivide;

        [Header("Glide HeightPlusWidth params")]
        [SerializeField]
        private float _glideParamMinThreshold = 0f;

        [SerializeField]
        private float _handToHeadDistanceMax = 1f;

        [SerializeField, MinMax]
        private Vector2 _heightRange = new Vector2(0.6f, 1.7f);

        private IPlayer _player;

        private enum GlideMethod
        {
            Area,
            HeightPlusWidth,
            HeightDivide
        }

        private void Awake()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            if (_player == null)
            {
                _player = (IPlayer)player;
            }
        }

        private void FixedUpdate()
        {
            switch (_glideMethod)
            {
                case GlideMethod.Area:
                {
                    GlideArea();
                    break;
                }

                case GlideMethod.HeightPlusWidth:
                {
                    GlideHeightPlusWidth();
                    break;
                }

                case GlideMethod.HeightDivide:
                {
                    GlideHeightDivide();
                    break;
                }
            }
        }

        private void GlideArea()
        {
            var pH = _player.head.localPosition;
            var pL = _player.leftHand.localPosition;
            var pR = _player.rightHand.localPosition;

            // we get the total length of the vector
            var a = (pL - pH).magnitude;
            var b = (pR - pH).magnitude;
            var c = (pL - pR).magnitude;

            // using Heron's fomula, get the area between arms and head, this is the lift effect
            var s = (a + b + c) * 0.5f;
            var area = Mathf.Sqrt(s * (s - a) * (s - b) * (s - c));
            if (area < _glideAreaMinThreshold)
            {
                return;
            }

            var force = _globalGlideVector;
            if (_localGlideVector != Vector3.zero)
            {
                force += _player.transform.TransformDirection(_localGlideVector);
            }

            force *= _glideForceMultiplier * Time.deltaTime * area;
            _player.AddForce(this, force, _glideForceMode);
        }

        private void GlideHeightPlusWidth()
        {
            var pH = _player.head.localPosition;
            var pL = _player.leftHand.localPosition;
            var pR = _player.rightHand.localPosition;

            var handsToHeadDistance = (pH - pL).magnitude + (pH - pR).magnitude;

            // gliding increases when:
            //      the hands are away from the head
            //      the hands and head are away from the ground (high up in the y axis)
            // we use the height of head and hands to determine height parameter [0,1], and handsToHeadDistance / max to determine the wingspan parameter, then we avg them for the final param
            var glideParam = (Mathf.InverseLerp(_heightRange.x, _heightRange.y, (pH.y + pL.y + pR.y) * 0.3333f) +
                              Mathf.InverseLerp(0f, _handToHeadDistanceMax, handsToHeadDistance)) * 0.5f;
            if (glideParam < _glideParamMinThreshold)
            {
                return;
            }

            var force = _globalGlideVector;
            if (_localGlideVector != Vector3.zero)
            {
                force += _player.transform.TransformDirection(_localGlideVector);
            }

            force *= _glideForceMultiplier * glideParam;
            _player.AddForce(this, force, _glideForceMode);
        }

        private void GlideHeightDivide()
        {
            var pH = _player.head.localPosition;
            var pL = _player.leftHand.localPosition;
            var pR = _player.rightHand.localPosition;

            var y = pH.y;
            var x = (pL - pR).magnitude;

            var glideParam = Mathf.Min(y / x, x / y);
            if (glideParam < _glideParamMinThreshold)
            {
                return;
            }

            var force = _globalGlideVector;
            if (_localGlideVector != Vector3.zero)
            {
                force += _player.transform.TransformDirection(_localGlideVector);
            }

            force *= _glideForceMultiplier * glideParam;
            _player.AddForce(this, force, _glideForceMode);
        }
    }
}