namespace Core
{
    using System.Collections.Generic;
    using Apex;
    using UnityEngine;
    using VRCore;

    [System.Obsolete]
    [ApexComponent("Locomotion")]
    [RequireComponent(typeof(Rigidbody), typeof(IPlayer))]
    public sealed class JumpAndGlideMechanics : MonoBehaviour, IVRPlayerStartListener
    {
        [Header("Jump params")]
        public int sampleFrames = 15;

        public float deltaYThresholdForJump = 3f;
        public Vector3 globalJumpVector = new Vector3(0f, 1f, 0f);
        public Vector3 localJumpVector = new Vector3(0f, 0f, 0f);
        public float jumpForceMultiplier = 100f;
        public ForceMode jumpForceMode = ForceMode.Force;

        public TextMesh jumpTextMesh;

        [Header("Glide params")]
        public Vector3 globalGlideVector = new Vector3(0f, 1f, 0f);

        public Vector3 localGlideVector = new Vector3(0f, 0f, 0f);
        public float glideForceMultiplier = 8f;
        public float glideAreaMinThreshold = 0f;
        public ForceMode glideForceMode = ForceMode.Force;

        public GlideMethod glideMethod = GlideMethod.HeightDivide;

        [Header("Glide HeightPlusWidth params")]
        public float glideParamMinThreshold = 0f;

        public float handToHeadDistanceMax = 1f; // dynamic based on current height? maybe? since wingspan in humans is equal to their height. nah that won't work because you can be tall and pretend to be small
        public Vector2 heightRange = new Vector2(0.6f, 1.7f);

        public bool writeDebug;

        //public Text glideText;
        public TextMesh glideTextMesh;

        private readonly Queue<Vector3> _sampledPositions = new Queue<Vector3>();
        private IPlayer _player;
        private Vector3[] _positionsBuffer;
        private Rigidbody _rb;
        private float _lastdYmax;
        private bool _isGrounded;

        public enum GlideMethod
        {
            Area,
            HeightPlusWidth,
            HeightDivide
        }

        private void Awake()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }

            _rb = this.GetComponent<Rigidbody>();
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            if (_player != null)
            {
                // already initialized
                return;
            }

            _player = (IPlayer)player;
        }

        private void FixedUpdate()
        {
            _isGrounded = IsGrounded();
            SamplePositions();
            CheckForJumpVector();
            switch (glideMethod)
            {
                case GlideMethod.Area:
                CheckForGlideVector_Area();
                break;

                case GlideMethod.HeightPlusWidth:
                CheckForGlideVector_HeightPlusWidth();
                break;

                case GlideMethod.HeightDivide:
                CheckHeightDivide();
                break;
            }
        }

        private void SamplePositions()
        {
            if (_sampledPositions.Count > sampleFrames)
            {
                _sampledPositions.Dequeue();
            }

            if (_player.head.localPosition != Vector3.zero)
            {
                _sampledPositions.Enqueue(_player.head.localPosition);
            }
        }

        private bool IsGrounded()
        {
            return _player.grounded;
            //Ray ray = new Ray(this.transform.position + Vector3.up, Vector3.down);

            //if (Physics.Raycast(ray, 1.5f, terrainMask))
            //{
            //    return true;
            //}

            //return false;
        }

        /// <summary>
        /// Applies a force to the unit if the forward delta Y over a certain range of sampled frames is over the delta Y threshold
        /// </summary>
        private void CheckForJumpVector()
        {
            var dYmax = GetMaxDeltaY(_sampledPositions);

            var text = string.Empty;

            if (_isGrounded)
            {
                if (dYmax > deltaYThresholdForJump)
                {
                    _sampledPositions.Clear();
                    var force = (globalJumpVector + _player.transform.TransformDirection(localJumpVector)) * jumpForceMultiplier * dYmax;
                    _rb.AddForce(force, jumpForceMode);
                    _lastdYmax = dYmax;

                    text = "dYmax > deltaYThresholdForJump -> JUMPING";
                }
                else
                {
                    text = "JUMP dYmax < deltaYThresholdForJump dYmax == " + dYmax.ToString("F2") + " last dYMax " + _lastdYmax.ToString("F2") + " grounded == " + _isGrounded;
                }
            }
            else
            {
                text = "JUMPING last dYMax " + _lastdYmax.ToString("F2") + " grounded == " + _isGrounded;
            }

            WriteToMesh(this.jumpTextMesh, text);
        }

        /// <summary>
        /// Applies a force to the unit based on the area of the triangle made up of the hands and head.
        /// </summary>
        private void CheckForGlideVector_Area()
        {
            var area = 0f;
            var forceY = 0f;

            var text = string.Empty;

            if (_isGrounded)
            {
                text = "NOT GLIDING grounded == " + _isGrounded;
            }
            else
            {
                var pH = _player.head.localPosition;
                var pL = _player.leftHand.localPosition;
                var pR = _player.rightHand.localPosition;

                //we get the total length of the vector;
                var a = (pL - pH).magnitude;
                var b = (pR - pH).magnitude;
                var c = (pL - pR).magnitude;

                //using Heron's fomula, get the area between arms and head, this is the lift effect
                var s = (a + b + c) * 0.5f;
                area = Mathf.Sqrt(s * (s - a) * (s - b) * (s - c));

                if (area < this.glideAreaMinThreshold)
                {
                    return;
                }

                var force = (globalGlideVector + _player.transform.TransformDirection(localGlideVector)) * glideForceMultiplier * Time.deltaTime;
                force *= area;
                forceY = force.y;

                _rb.AddForce(force * _player.slowdownParam, glideForceMode);

                text = "GLIDING " + area.ToString("F2") + "   " + forceY.ToString("F2") + " grounded == " + _isGrounded;
            }

            WriteToMesh(this.glideTextMesh, text);
        }

        private void CheckForGlideVector_HeightPlusWidth()
        {
            var forceY = 0f;

            var text = string.Empty;

            if (_isGrounded)
            {
                text = "NOT GLIDING grounded == " + _isGrounded;
            }
            else
            {
                var pH = _player.head.localPosition;
                var pL = _player.leftHand.localPosition;
                var pR = _player.rightHand.localPosition;

                var handsToHeadDistance = (pH - pL).magnitude + (pH - pR).magnitude;

                // gliding increases when:
                //      the hands are away from the head
                //      the hands and head are away from the ground (high up in the y axis)
                // we use the height of head and hands to determine height parameter [0,1], and handsToHeadDistance / max to determine the wingspan parameter, then we avg them for the final param
                var glideParam = (Mathf.InverseLerp(heightRange.x, heightRange.y, (pH.y + pL.y + pR.y) * 0.3333f) + Mathf.InverseLerp(0, handToHeadDistanceMax, handsToHeadDistance)) * 0.5f;

                if (glideParam < this.glideParamMinThreshold)
                {
                    return;
                }

                var force = (globalGlideVector + _player.transform.TransformDirection(localGlideVector)) * glideForceMultiplier;
                force *= glideParam;
                forceY = force.y;

                _rb.AddForce(force * _player.slowdownParam, glideForceMode);

                text = "GLIDING " + glideParam.ToString("F2") + "   " + forceY.ToString("F2") + " grounded == " + _isGrounded;
            }

            WriteToMesh(this.glideTextMesh, text);
        }

        private void CheckHeightDivide()
        {
            var forceY = 0f;

            var text = string.Empty;

            if (_isGrounded)
            {
                text = "NOT GLIDING grounded == " + _isGrounded;
            }
            else
            {
                var pH = _player.head.localPosition;
                var pL = _player.leftHand.localPosition;
                var pR = _player.rightHand.localPosition;

                var y = pH.y;
                var x = (pL - pR).magnitude;

                var glideParam = Mathf.Min(y / x, x / y);

                if (glideParam < this.glideParamMinThreshold)
                {
                    return;
                }

                var force = (globalGlideVector + _player.transform.TransformDirection(localGlideVector)) * glideForceMultiplier;
                force *= glideParam;
                forceY = force.y;

                _rb.AddForce(force * _player.slowdownParam, glideForceMode);

                text = "GLIDING " + glideParam.ToString("F2") + "   " + forceY.ToString("F2") + " grounded == " + _isGrounded;
            }

            WriteToMesh(this.glideTextMesh, text);
        }

        //calculates the forward max delta Y
        private float GetMaxDeltaY(Queue<Vector3> positions)
        {
            _positionsBuffer = positions.ToArray();

            var dYmax = 0f;
            var count = _positionsBuffer.Length;
            for (int i = count - 1; i > 0; i--)
            {
                //simply measure the delta of the steepest node in the jump
                var pY = (_positionsBuffer[i].y - _positionsBuffer[i - 1].y) * 100f;
                dYmax = Mathf.Max(0, pY);
            }

            return dYmax;
        }

        private void WriteToMesh(TextMesh mesh, string text)
        {
            if (!this.writeDebug)
            {
                return;
            }

            mesh.text = text;
        }
    }
}