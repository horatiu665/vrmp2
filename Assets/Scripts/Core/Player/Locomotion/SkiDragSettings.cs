﻿namespace Core
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;
    using VRCore;

    public class SkiDragSettings : MonoBehaviour
    {

        [Header("Head controlling drag")]
        public AnimationCurve heightToDrag = AnimationCurve.Linear(0, 0, 1, 1);

        [MinMax]
        public Vector2 heightRange = new Vector2(0.6f, 1.7f);

        [MinMax]
        public Vector2 dragRange = new Vector2(0.06f, 0.07f);

        [Header("Adding extra drag based on player speed")]
        public AnimationCurve addDragWithPlayerSpeed = new AnimationCurve(
            new Keyframe[]
            {
                new Keyframe(0.2446769f, 0.9926066f, -3.146942E-08f, -3.146942E-08f),
                new Keyframe(0.5367905f, 0.2505742f, -0.6530238f, -0.6530238f),
                new Keyframe(0.939226f, 0.1058447f, -0.1774507f, -0.1774507f),
                new Keyframe(1.56887f, 0.0316477f, -0.07028771f, -0.07028771f),
                new Keyframe(3f, -7.265946E-06f, -0.05401638f, -0.05401638f),
            }
        );

        public float addDragMax = 8f;
        public float playerSpeedScaleCurve = 10f;

        [Header("Add extra drag on slope angle")]
        public AnimationCurve multiDragWithSlopeAngle = new AnimationCurve(
            new Keyframe[]
            {
                new Keyframe(0f, 1f, -2.392078f, -2.392078f),
                new Keyframe(1f, 0f, 0f, 0f),
            }
        );
        public float slopeAngleDragMax = 0.5f;
        public float maxSlopeAngle = 25f;

        [Header("MaxVelocity increase head down")]
        public AnimationCurve maxVelocityIncrease = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, -2), new Keyframe(1, 0, 0, 0) } };
        public float maxMaxVelIncrease = 40;
        public Vector2 playerSpeedForMaxVelocity = new Vector2(0, 20);

    }
}