namespace Core
{
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(IPlayer), typeof(SkiPlayerTurningSkiGraphics))]
    public class SkiHeadSteeringStrafe : MonoBehaviour, IVRPlayerStartListener
    {
        [Header("Steering parameters")]
        /// <summary>
        /// Lerps max head from maxHeadSidePositionRange.x to y, based on player.rb.velocity.magnitude / maxPlayerSpeedForMaxHead
        /// </summary>
        [SerializeField, Tooltip("Lerps max head from maxHeadSidePositionRange.x to y, based on player.rb.velocity.magnitude / maxPlayerSpeedForMaxHead")]
        private AnimationCurve _playerSpeedToMaxHead = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        /// <summary>
        /// maximum distance (from center of playspace) needed to steer left/right with maximum velocity. Uses playerSpeedToMaxHead curve to lerp between min and max.
        /// </summary>
        [SerializeField, MinMax, Tooltip("maximum distance (from center of playspace) needed to steer left/right with maximum velocity. Uses playerSpeedToMaxHead curve to lerp between min and max.")]
        private Vector2 _maxHeadSidePositionRange = new Vector2(0.3f, 0.5f);

        /// <summary>
        /// scales playerSpeedToMaxHead curve on the X axis by dividing playerSpeed.magnitude by this value.
        /// </summary>
        [SerializeField, Tooltip("scales playerSpeedToMaxHead curve on the X axis by dividing playerSpeed.magnitude by this value")]
        private float _maxPlayerSpeedForMaxHead = 15;

        /// <summary>
        /// specifies the mapping between head position and strafe velocity. X axis = head position magnitude, from 0 to 1 (max head position, see above). Y axis = strafe velocity multiplier.
        /// </summary>
        [SerializeField, Tooltip("specifies the mapping between head position and strafe velocity. X axis = head position magnitude, from 0 to 1 (max head position, see above). Y axis = strafe velocity multiplier.")]
        private AnimationCurve _headPosToStrafeVelocityMax = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        /// <summary>
        /// Maps the player velocity to strafe velocity multiplier. X axis = player velocity, scaled by maxPlayerVelocityForStrafe. Y axis = strafe velocity multiplier.
        /// </summary>
        [SerializeField, Tooltip("Maps the player velocity to strafe velocity multiplier. X axis = player velocity, scaled by maxPlayerVelocityForStrafe. Y axis = strafe velocity multiplier.")]
        private AnimationCurve _playerVelocityToStrafeVelocityMax = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        /// <summary>
        /// scales player velocity on curve above
        /// </summary>
        [SerializeField, Tooltip("scales player velocity on curve above")]
        private float _maxPlayerVelocityForStrafe = 25;

        /// <summary>
        /// multiplies the strafe velocity for easy tweaking
        /// </summary>
        [SerializeField, Tooltip("multiplies the strafe velocity for easy tweaking")]
        private float _strafeVelocityMultiplier = 15;

        [Header("Slowdown Curve")]
        [SerializeField]
        private AnimationCurve _slowdownMultiplier = AnimationCurve.Linear(0, 1, 1, 1);

        [Header("down speed based on player head")]
        public AnimationCurve playerHeadToDownSpeed = new AnimationCurve(
            new Keyframe[]
            {
                new Keyframe(0f, 1f, 0f, 0f),
                new Keyframe(0.2f, 0.3000098f, -1.400301f, -1.400301f),
                new Keyframe(0.8f, 1.192093E-07f, 0f, 0f),
            }
        );

        public float maxDownSpeed = 24;

        [Header("Drag based on player head")]

        [SerializeField]
        private AnimationCurve _dragOnPlayerHeadSide = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
        [SerializeField]
        private float _maxDrag = 0f;


        private IPlayer _player;
        private SkiDragController dragController;

        [Header("MaxVelocity decrease based on head side")]
        [SerializeField]
        private AnimationCurve _maxVelocityAdjust = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0.5f, 0, 0, 0), new Keyframe(2, 1, 0, 0) } };
        [SerializeField]
        private float _maxMaxVelAdjust = -20;

        public Vector2 playerSpeedForMaxVelocity = new Vector2(0, 20);

        [ReadOnly]
        private float _finalStrafeVel;

        // set in FixedUpdate, but revealed for other scripts such as cheat graphics
        private float _maxHeadSidePosition;
        private float _dragFromPlayerSide;

        public float dragFromPlayerSide
        {
            get
            {
                return _dragFromPlayerSide;
            }
        }

        public float maxHeadSidePosition
        {
            get
            {
                return _maxHeadSidePosition;
            }
        }

        // used in the cheat steering script
        public float finalStrafeVel
        {
            get
            {
                return _finalStrafeVel;
            }
        }

        private void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
            dragController = _player.transform.GetComponent<SkiDragController>();
        }

        private void FixedUpdate()
        {
            if (_player == null)
            {
                return;
            }

            var skiForward = _player.skis;

            // ===================== ADDING sideways VELOCITY =======================
            var pSpeed = _player.rb.velocity.magnitude;

            _maxHeadSidePosition = Mathf.Lerp(_maxHeadSidePositionRange.x, _maxHeadSidePositionRange.y, _playerSpeedToMaxHead.Evaluate(pSpeed / _maxPlayerSpeedForMaxHead));

            var headSideParamHelper = Vector3.Dot(_player.head.position - skiForward.position, skiForward.right);
            var headSideParam = headSideParamHelper / maxHeadSidePosition;
            var absHeadSideParam = Mathf.Abs(headSideParam);

            // set head position left/right so other scripts can use it for calculations (f.x. ski stick steering)
            _player.headSteeringParam = headSideParam;
            // set head forward, resized to the same extent as the head side param.
            _player.headForwardParam = Vector3.Dot(_player.head.position - skiForward.position, skiForward.forward) / maxHeadSidePosition;

            var headPosToStrafeVel = _headPosToStrafeVelocityMax.Evaluate(absHeadSideParam) * Mathf.Sign(headSideParam);

            var playerVelToStrafeVel = _playerVelocityToStrafeVelocityMax.Evaluate(pSpeed / _maxPlayerVelocityForStrafe);

            _finalStrafeVel = headPosToStrafeVel * playerVelToStrafeVel * _strafeVelocityMultiplier * _slowdownMultiplier.Evaluate(_player.slowdownParam);

            _player.AddForce(this, skiForward.right * _finalStrafeVel, ForceMode.Force);

            if (dragController != null)
            {
                // in case localposition is not working (two layers of parenting like SteamVR likes it)
                var localHead = _player.head.position.y - _player.position.y;
                var heightParam = Mathf.Clamp01(Mathf.InverseLerp(dragController.settings.heightRange.x, dragController.settings.heightRange.y, localHead));
                var downSpeed = playerHeadToDownSpeed.Evaluate(heightParam) * maxDownSpeed;
                _player.AddForce(this, Vector3.down * downSpeed, ForceMode.Force);

                _dragFromPlayerSide = _dragOnPlayerHeadSide.Evaluate(absHeadSideParam) * _maxDrag;
                dragController.AddDragNextFrame(_dragFromPlayerSide);

            }

            // slow down when steering a lot. curve will take care of that. maxmax needs to be negative for a slowdown.
            var pSpeedMV = playerSpeedForMaxVelocity.InverseLerp(pSpeed);
            _player.velocityController.IncreaseMaxVelocity(_maxVelocityAdjust.Evaluate(absHeadSideParam) * pSpeedMV * _maxMaxVelAdjust * Time.fixedDeltaTime);

        }
    }
}