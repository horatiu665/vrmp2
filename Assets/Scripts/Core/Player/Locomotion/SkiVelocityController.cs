﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class SkiVelocityController : MonoBehaviour
{
    [Header("Max velocity")]
    public float curMaxVelocity = 20f;

    public Vector2 maxVelocityNormalClampRange = new Vector2(20f, 130f);

    public float onlyIncreaseToThisPercentOfCurVelocity = 1.1f;

    public bool clampedUpper = true;

    [Header("Kine when under x, and un-kine when over y.")]
    public Vector2 minVelocity = new Vector2(0.1f, 0.2f);

    [Header("Drag over this value = kine")]
    public float forcedKinematicOnDrag = 10f;

    public ForcedKinematicMode forcedIsKinematic = ForcedKinematicMode.NotForced;

    public enum ForcedKinematicMode
    {
        NotForced,
        ForcedOn,
        ForcedOff,
    }

    private IPlayer _player;

    public IPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = GetComponent<IPlayer>();
            }
            return _player;
        }
    }

    public float speed { get; private set; }

    public bool debug = false;

    public Dictionary<int, ForceRequestData> debugForceRequests = new Dictionary<int, ForceRequestData>();
    public List<string> debugOutput = new List<string>();

    /// <summary>
    /// When calling this every frame, remember to multiply by Time.deltaTime or fixedDeltaTime.
    /// </summary>
    /// <param name="acceleration"></param>
    public void IncreaseMaxVelocity(float acceleration)
    {
        var maxVelLessThanCurPlayerSpeed = curMaxVelocity < speed * onlyIncreaseToThisPercentOfCurVelocity;
        if ((maxVelLessThanCurPlayerSpeed && acceleration > 0) || acceleration < 0)
        {
            curMaxVelocity += acceleration;
        }
    }

    private void Update()
    {
        if (debug)
        {
            debugOutput = debugForceRequests.Select(kv => kv.Value.ToString()).ToList();
        }

        if (clampedUpper)
        {
            curMaxVelocity = Mathf.Min(curMaxVelocity, maxVelocityNormalClampRange.y);
        }

        // always clamp lower.
        curMaxVelocity = Mathf.Max(curMaxVelocity, maxVelocityNormalClampRange.x);
    }

    private void FixedUpdate()
    {
        // forced kinematic ON
        if (forcedIsKinematic == ForcedKinematicMode.ForcedOn)
        {
            player.rb.isKinematic = true;
            speed = 0;
            return;
        }

        // clamp upper speed
        speed = player.rb.velocity.magnitude;
        if (clampedUpper)
        {
            player.rb.velocity = Vector3.ClampMagnitude(player.rb.velocity, curMaxVelocity);
        }

        // forced kinematic OFF
        if (forcedIsKinematic == ForcedKinematicMode.ForcedOff)
        {
            player.rb.isKinematic = false;
            return;
        }

        // kinematic based on speed and drag
        if (player.grounded && speed < minVelocity.x)
        {
            player.rb.isKinematic = true;
        }
        if (player.rb.drag >= forcedKinematicOnDrag)
        {
            player.rb.isKinematic = true;
        }
    }

    public void AddForce(object sender, Vector3 force, ForceMode mode = ForceMode.VelocityChange)
    {
        if (debug)
        {
            var newReq = new ForceRequestData()
            {
                sender = sender,
                force = force,
                forceMode = mode,
                lastRequestTime = Time.time,
                requests = 1,
            };

            var hash = newReq.hash;
            if (debugForceRequests.ContainsKey(hash))
            {
                newReq.requests = debugForceRequests[hash].requests + 1;
            }

            debugForceRequests[hash] = newReq;

        }

        if (player.rb.isKinematic)
        {
            if (!player.grounded)
            {
                player.rb.isKinematic = false;
            }
            else
            {
                if (mode == ForceMode.Acceleration || mode == ForceMode.Force)
                {
                    if (force.magnitude * Time.fixedDeltaTime + speed > minVelocity.y)
                    {
                        player.rb.isKinematic = false;
                    }
                }
                else
                {
                    if (force.magnitude + speed > minVelocity.y)
                    {
                        player.rb.isKinematic = false;
                    }
                }
            }
        }

        // calling rigidbody.AddForce adds the force after this.FixedUpdate() so it's useless in some cases.
        // Setting velocity manually in the switch() way has the same effect as AddForce, but immediately
        //player.rb.AddForce(force, mode);
        switch (mode)
        {
        case ForceMode.Force:
            player.rb.velocity += force * Time.fixedDeltaTime / player.rb.mass;
            break;
        case ForceMode.Acceleration:
            player.rb.velocity += force * Time.fixedDeltaTime;
            break;
        case ForceMode.Impulse:
            player.rb.velocity += force / player.rb.mass;
            break;
        case ForceMode.VelocityChange:
            player.rb.velocity += force;
            break;
        }
    }


    public class ForceRequestData
    {
        public object sender;
        public float lastRequestTime;
        public Vector3 force;
        public ForceMode forceMode;
        public int requests;
        public int hash
        {
            get
            {
                return GetHashCode();
            }
        }

        public override int GetHashCode()
        {
            return sender.GetHashCode() + forceMode.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return "T=" + lastRequestTime + " r=" + requests + "\ts=" + sender.GetType().Name + " f=" + force + " m=" + forceMode.ToString();
        }
    }
}