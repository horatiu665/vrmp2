namespace Core
{
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(IVRPlayer))]
    public sealed class PonycornLocomotion : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private bool _active = true;

        [Header("Speed settings")]
        [SerializeField]
        private float _range = 0.05f;

        [SerializeField]
        private float _speed = 0.05f;

        [SerializeField]
        private float _maxInOneDirection = 0.15f;

        [SerializeField]
        private AnimationCurve _deltaToMovement = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 1.5f, 0) } };

        [SerializeField]
        private float _maxDeltaMove = 1f;

        [Header("Constraints")]
        [SerializeField]
        private bool _stayInsideColliderBounds = false;

        [SerializeField]
        private LayerMask _colliderLayerMask;

        [SerializeField]
        private float _angleAround = 45f;

        [SerializeField]
        private int _samples = 18;

        [SerializeField]
        private float _distanceToCheck = 1f;

        [Header("Slowdown Curve")]
        [SerializeField]
        private AnimationCurve _slowdownMultiplier = AnimationCurve.Linear(0, 1, 1, 0.3f);

        private float _headbangRange;
        private float _curDirection;
        private bool _passed = false;
        private IPlayer _player;

        public bool active
        {
            get { return _active; }
            set { _active = value; }
        }

        private void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IVRPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
        }

        private void Update()
        {
            var playerHead = _player.head;

            var deltaMove = 0f;
            if (playerHead.position.y > _headbangRange + _range * 0.5f)
            {
                if (_curDirection < 0)
                {
                    _curDirection = 0;
                }

                var newPos = playerHead.position.y;
                var delta = newPos - (_headbangRange + _range * 0.5f);
                _headbangRange += delta;
                _passed = true;
                deltaMove = Mathf.Abs(delta);

                _curDirection += delta;
            }
            else if (playerHead.position.y < _headbangRange - _range * 0.5f)
            {
                if (_curDirection > 0)
                {
                    _curDirection = 0;
                }

                var newPos = playerHead.position.y;
                var delta = newPos - (_headbangRange - _range * 0.5f);
                _headbangRange += delta;
                _passed = true;
                deltaMove = Mathf.Abs(delta);

                _curDirection += delta;
            }

            if (Mathf.Abs(_curDirection) < _maxInOneDirection)
            {
                if (_passed)
                {
                    _passed = false;
                    //print(deltaMove / Time.deltaTime);
                    var moveDir = playerHead.forward * _deltaToMovement.Evaluate(deltaMove / Time.deltaTime / _maxDeltaMove) * _speed;
                    moveDir.y = 0;

                    if (_active)
                    {
                        // multiply with player's speed multiplier
                        moveDir *= _slowdownMultiplier.Evaluate(_player.slowdownParam);

                        // if we don't care if player is in the bounds, just move player.
                        if (!_stayInsideColliderBounds)
                        {
                            _player.position += moveDir;
                        }
                        else
                        {
                            // if we care, only move him if he's inside bounds, or if he is moving towards the bounds.
                            if (IsPointInsideCollider(playerHead.position, _colliderLayerMask))
                            {
                                // move object, then check if we exited bounds.
                                _player.position += moveDir;
                            }
                            else
                            {
                                // we are outside the bounds from the start. we should move towards the closest point on the collider.
                                var closestPoint = FindClosestPointOnBounds(playerHead.position, Vector3.forward, _angleAround, _samples, _distanceToCheck);
                                Debug.DrawRay(playerHead.position, Vector3.up * 3, Color.red, 1f);
                                Debug.DrawLine(playerHead.position, closestPoint, Color.cyan, 1f);

                                var towardsCP = closestPoint - playerHead.position;

                                // move in the opposite direction of movedir, projected on closestPoint
                                if (Vector3.Dot(moveDir, towardsCP) > 0)
                                {
                                    playerHead.position += Vector3.Project(moveDir, towardsCP);
                                }
                                else
                                {
                                    playerHead.position += Vector3.Project(-moveDir, towardsCP);
                                }
                            }
                        }
                    }
                }
            }
        }

        /// <summary>
        /// returns the closest point on the collider, by sampling positions using raycasts, based on the direction of movement
        /// </summary>
        /// <param name="pos">start position, closest point compared to this position</param>
        /// <param name="dir">movement direction, use to reduce necessary amount of samples</param>
        /// <param name="angleAround">angle around direction, to try and sample rays</param>
        /// <param name="samples">how many rays</param>
        /// <param name="distanceToCheck">max distance for the sampled rays</param>
        /// <returns>a point on the collider which is pretty much close to the position (accuracy depends on the parameters)</returns>
        private Vector3 FindClosestPointOnBounds(Vector3 pos, Vector3 dir, float angleAround, int samples, float distanceToCheck)
        {
            //RaycastHit info;
            //if (Physics.Raycast(playerHead.position, -dir, out info, dir.magnitude, colliderLayerMask)) {
            //    return info.point;
            //} else {
            //    return pos - dir;
            //}

            RaycastHit info;
            var closestPoint = pos - dir.normalized * distanceToCheck;
            float minDistance = -1f;

            for (int i = 0; i < samples; i++)
            {
                var offset = Quaternion.Euler(0, angleAround * ((i / (float)(samples - 1)) - 0.5f), 0);
                var newDir = offset * dir;
                if (Physics.Raycast(pos, newDir, out info, distanceToCheck, _colliderLayerMask))
                {
                    if ((info.point - pos).sqrMagnitude < (closestPoint - pos).sqrMagnitude)
                    {
                        closestPoint = info.point;
                        minDistance = (info.point - pos).sqrMagnitude;
                    }
                }
            }

            if (minDistance == -1)
            {
                return pos;
            }

            return closestPoint;
        }

        /// <summary>
        /// stolen and adapted from http://answers.unity3d.com/questions/163864/test-if-point-is-in-collider.html
        /// </summary>
        /// <param name="myPoint"></param>
        /// <param name="myCollider"></param>
        /// <returns></returns>
        private bool IsPointInsideCollider(Vector3 myPoint, LayerMask bounceOffCollidersLayerMask)
        {
            Vector3 point;
            Vector3 start = new Vector3(0, 1000, 0); // This is defined to be some arbitrary point far away from the collider.
            Vector3 direction = (myPoint - start).normalized; // This is the direction from start to goal.
            int iterations = 0; // If we know how many times the raycast has hit faces on its way to the target and back, we can tell through logic whether or not it is inside.
            point = start;

            while (point != myPoint) // Try to reach the point starting from the far off point.  This will pass through faces to reach its objective.
            {
                RaycastHit hit;
                if (Physics.Linecast(point, myPoint, out hit, bounceOffCollidersLayerMask)) // Progressively move the point forward, stopping everytime we see a new plane in the way.
                {
                    iterations++;
                    point = hit.point + (direction / 100.0f); // Move the Point to hit.point and push it forward just a touch to move it through the skin of the mesh (if you don't push it, it will read that same point indefinately).
                }
                else
                {
                    point = myPoint; // If there is no obstruction to our goal, then we can reach it in one step.
                }
            }
            while (point != start) // Try to return to where we came from, this will make sure we see all the back faces too.
            {
                RaycastHit hit;
                if (Physics.Linecast(point, start, out hit, bounceOffCollidersLayerMask))
                {
                    iterations++;
                    point = hit.point + (-direction / 100.0f);
                }
                else
                {
                    point = start;
                }
            }

            if (iterations % 2 == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
    }
}