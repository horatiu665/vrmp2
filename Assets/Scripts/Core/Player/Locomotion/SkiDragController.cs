namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(IVRPlayer))]
    public sealed class SkiDragController : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private SkiDragSettings _settings;
        public SkiDragSettings settings
        {
            get
            {
                if (_settings == null || !_settings.isActiveAndEnabled)
                {
                    _settings = GetComponentInChildren<SkiDragSettings>();
                }
                return _settings;
            }
        }

        private float _nextFrameDrag = 0f;
        private IPlayer _player;

        [Header("Debug")]
        public bool showDebugVelocity;

        private void OnEnable()
        {
            var player = this.GetComponent<IVRPlayer>();
            if (player != null)
            {
                OnPlayerStart(player);
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            if (_player != null)
            {
                // already initialized
                return;
            }

            _player = player as IPlayer;
        }

        private void FixedUpdate()
        {
            if (_player == null)
            {
                return;
            }

            // FROM HEAD
            var localHead = _player.head.localPosition.y;

            // height as a percentage 0..1, 0 corresponds to heightRange.x, 1 to heightRange.y
            var heightParam = Mathf.Clamp01(Mathf.InverseLerp(this.settings.heightRange.x, this.settings.heightRange.y, localHead));

            var dragFromHeight = GetDragFromHeadHeight(heightParam);

            // FROM PLAYER SPEED
            var pSpeed = _player.rb.velocity.magnitude;
            var dragPerPlayerSpeed = !_player.grounded ? 0f : this.settings.addDragWithPlayerSpeed.Evaluate(pSpeed / this.settings.playerSpeedScaleCurve) * this.settings.addDragMax;

            var finalDrag = dragFromHeight + _nextFrameDrag + dragPerPlayerSpeed;
            _nextFrameDrag = 0f;

            // var angle
            if (_player.grounded)
            {
                var angle = Vector3.Angle(_player.groundedRay.normal, Vector3.up);
                var slopeDrag = this.settings.multiDragWithSlopeAngle.Evaluate(angle / this.settings.maxSlopeAngle) * this.settings.slopeAngleDragMax;
                finalDrag *= slopeDrag;
            }

            if (finalDrag != _player.rb.drag)
            {
                _player.rb.drag = Mathf.Max(0, finalDrag);
            }

            var pSpeedMV = this.settings.playerSpeedForMaxVelocity.InverseLerp(pSpeed);
            var maxVelInc = this.settings.maxVelocityIncrease.Evaluate(heightParam) * this.settings.maxMaxVelIncrease * pSpeedMV;
            _player.velocityController.IncreaseMaxVelocity(maxVelInc * Time.fixedDeltaTime);

        }

        public void AddDragNextFrame(float drag)
        {
            _nextFrameDrag += drag;
        }

        public float GetDragFromHeadHeight(float heightParam)
        {
            // real drag is the heightRange mapped to the dragRange using the heightToDrag curve (x is heightParam 0 to 1, y is drag param 0 to 1)
            var dragFromHeight = Mathf.Lerp(this.settings.dragRange.x, this.settings.dragRange.y, this.settings.heightToDrag.Evaluate(heightParam));
            return dragFromHeight;
        }

        public void OnGUI()
        {
            if (showDebugVelocity)
            {
                var style = GUI.skin.label;
                GUI.skin.label.fontSize = 40;
                GUI.Label(new Rect(10, 10, 1000, 80), "V:" + _player.rb.velocity.magnitude.ToString("F2"));
                GUI.skin.label = style;
            }
        }
    }
}