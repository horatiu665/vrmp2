namespace Core
{
    using System.Collections;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(Rigidbody))]
    public class FlyThirdPersonController : MonoBehaviour
    {
        // disable warning "assigned but never used"
#pragma warning disable 0414

        [Header("If false, disables 3rd person camera and controls")]
        [SerializeField]
        private bool _enableThirdPersonMode = false;

        [SerializeField]
        private KeyCode thirdPersonToggleCheatKey = KeyCode.V;

        [SerializeField]
        private Transform _thirdPersonCameraRoot = null;

        [SerializeField]
        private Camera _thirdPersonCamera = null;

        [SerializeField]
        private Camera _vrCamera = null;

        [SerializeField]
        private GameObject _thirdPersonGraphics = null, _vrGraphics = null;

        private FlyVRController _flyVRController = null;

#pragma warning restore 0414

        [Header("Input")]
        [SerializeField]
        private KeyCode _keyLeft = KeyCode.A;

        [SerializeField]
        private KeyCode _keyRight = KeyCode.D;

        [SerializeField]
        private KeyCode _keyForward = KeyCode.W;

        [SerializeField]
        private KeyCode _keyBack = KeyCode.S;

        [SerializeField]
        private KeyCode _keyUp = KeyCode.E;

        [SerializeField]
        private KeyCode _keyDown = KeyCode.Q;

        //[SerializeField]
        //private KeyCode _shootBomb = KeyCode.Space;

        [SerializeField]
        private float _inputSmoothness = 0.1f;

        [Header("Control params")]
        [SerializeField]
        private float _flyForce = 1;

        [SerializeField]
        private float _groundedDistanceCheck = 0.04f;

        [SerializeField]
        private float _groundedEpsilon = 0.01f;

        [Header("Audio")]
        [SerializeField]
        private AudioSource _buzzingAudio;

        //[Header("Collisions")]
        //[SerializeField]
        //private AnimationCurve _collisionForceToRagdollDuration = AnimationCurve.EaseInOut(0, 0, 1, 1);

        //[SerializeField]
        //private Vector2 _collisionForceMinMax = new Vector2(0, 5f);

        //[SerializeField]
        //private Vector2 _ragdollDurationMinMax = new Vector2(0.3f, 5f);

        private Vector3 _input;

        private bool _grounded;
        private IPlayer _player;
        //private Animator _anim;
        private Rigidbody _rb;
        //private Ragdollify _ragdollify;

        private void Awake()
        {
            _rb = GetComponent<Rigidbody>();
            //_ragdollify = GetComponent<Ragdollify>();
            if (_buzzingAudio == null)
            {
                _buzzingAudio = GetComponentInChildren<AudioSource>();
            }
            _flyVRController = GetComponent<FlyVRController>();
        }

        private void OnEnable()
        {
            SetThirdPersonMode(_enableThirdPersonMode);

            // anim depends on third person mode
            //_anim = _thirdPersonGraphics.transform.GetComponentInChildren<Animator>();

        }

        private void Update()
        {
            if (Input.GetKeyDown(thirdPersonToggleCheatKey))
            {
                _enableThirdPersonMode = !_enableThirdPersonMode;
                SetThirdPersonMode(_enableThirdPersonMode);
            }

            if (!_enableThirdPersonMode)
            {
                return;
            }

            // INPUT always in update
            var inputRaw = new Vector3(
                (Input.GetKey(_keyLeft) ? -1 : 0) + (Input.GetKey(_keyRight) ? 1 : 0),
                (Input.GetKey(_keyDown) ? -1 : 0) + (Input.GetKey(_keyUp) ? 1 : 0),
                (Input.GetKey(_keyBack) ? -1 : 0) + (Input.GetKey(_keyForward) ? 1 : 0));
            _input = Vector3.Lerp(_input, inputRaw, _inputSmoothness);

            //if (Input.GetKey(_shootBomb))
            //{
            //    SpawnManagerPreviewable.instance.Spawn<IVRSpawnable>(_player.leftHand, VRPrefabType.FlyBomb);
            //}
        }

        private void FixedUpdate()
        {
            if (!_enableThirdPersonMode)
            {
                return;
            }

            // transform input into local camera directions
            var cam = _thirdPersonCamera.transform;
            var camInput = cam.forward * _input.z + Vector3.up * _input.y + cam.right * _input.x;

            _rb.AddForce(camInput * _flyForce);

            var speed = _rb.velocity.magnitude;

            RaycastHit hit;
            if (Physics.Raycast(transform.position + Vector3.up * _groundedDistanceCheck, Vector3.down, out hit, _groundedDistanceCheck * 2))
            {
                if (transform.position.y - hit.point.y < _groundedEpsilon)
                {
                    _grounded = true;
                }
                else
                {
                    _grounded = false;
                }
            }
            else
            {
                _grounded = false;
            }

            // anim
            var groundedAnim = !_grounded || _input.magnitude > 0.1f;
            //if (_anim.isActiveAndEnabled)
            //    _anim.SetBool("Fly", groundedAnim);

            if (!groundedAnim && _buzzingAudio.volume > 0)
            {
                // stop playing audio
                StopCoroutine("FadeAudioCor");
                _buzzingAudio.volume = 0;
            }
            else if (groundedAnim && _buzzingAudio.volume == 0)
            {
                // start playing audio with a little fade
                FadeAudioIn();
            }

            var fw = cam.forward;
            fw.y = 0;
            fw += camInput * 0.7f;
            var rot = Quaternion.LookRotation(fw);
            _rb.MoveRotation(rot);
        }

        //private void OnCollisionEnter(Collision collision)
        //{
        //    _ragdollify.RagdollEnable(collision.relativeVelocity);
        //    var waitTimeParam = _collisionForceToRagdollDuration.Evaluate(Mathf.InverseLerp(_collisionForceMinMax.x, _collisionForceMinMax.y, collision.relativeVelocity.magnitude));
        //    var waitTime = Mathf.Lerp(_ragdollDurationMinMax.x, _ragdollDurationMinMax.y, waitTimeParam);
        //    StartCoroutine(pTween.Wait(waitTime, () =>
        //    {
        //        _ragdollify.RagdollDisable();
        //    }));
        //}

        private void SetThirdPersonMode(bool enableThirdPersonMode)
        {
            if (_thirdPersonCameraRoot != null)
            {
                _thirdPersonCameraRoot.gameObject.SetActive(enableThirdPersonMode);
            }

            if (_vrCamera != null)
            {
                _vrCamera.gameObject.SetActive(!enableThirdPersonMode);
            }

            if (_thirdPersonGraphics != null)
            {
                _thirdPersonGraphics.SetActive(enableThirdPersonMode);
            }

            if (_vrGraphics != null)
            {
                _vrGraphics.SetActive(!enableThirdPersonMode);
            }

            // if graphics obj is the same
            if (_thirdPersonGraphics == _vrGraphics)
            {
                _vrGraphics.SetActive(true);
            }

            if (_flyVRController != null)
            {
                _flyVRController.enabled = !enableThirdPersonMode;
            }
        }

        private void FadeAudioIn()
        {
            StopCoroutine("FadeAudioCor");
            StartCoroutine("FadeAudioCor");
        }

        private IEnumerator FadeAudioCor()
        {
            //var duration = 0.1f;
            //float start = Time.time;
            //float end = start + duration;
            //float durationInv = 1f / duration;
            //float startMulDurationInv = start / duration;
            //for (float t = Time.time; t < end; t = Time.time)
            //{
            //    var f = (Mathf.Lerp(0, 1, t * durationInv - startMulDurationInv));
            //    _buzzingAudio.volume = f;
            //    yield return 0;
            //}
            yield return StartCoroutine(pTween.To(0.1f, 0, 1f, t => {
                _buzzingAudio.volume = t;
            }));
        }
    }
}