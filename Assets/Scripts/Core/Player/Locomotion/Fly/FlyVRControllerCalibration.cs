namespace Core
{
    using Apex;
    using Apex.Services;
    using Core;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;

    [ApexComponent("Locomotion")]
    [RequireComponent(typeof(FlyVRController))]
    public class FlyVRControllerCalibration : MonoBehaviour, IHandleMessage<VRInputPressDown>, IHandleMessage<VRInputPress>
    {

        [SerializeField]
        private bool _calibrateOnLongPress = true;

        [SerializeField]
        private float _calibrationPressDuration = 1.5f;
        float _calibrationStartPressTime;

        [SerializeField]
        private VRInput _calibrationButton = VRInput.Touchpad;

        private FlyVRController _flyVrController;

        void Awake()
        {
            _flyVrController = GetComponent<FlyVRController>();
        }

        private void OnEnable()
        {
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPress>(this);
        }

        private void OnDisable()
        {
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPress>(this);
        }

        public void Handle(VRInputPressDown message)
        {
            if (!_flyVrController.enabled)
            {
                return;
            }

            if (!_calibrateOnLongPress)
            {
                return;
            }

            if (message.input == _calibrationButton)
            {
                // start counting calibration press seconds
                _calibrationStartPressTime = Time.time;
            }
        }

        public void Handle(VRInputPress message)
        {
            if (!_flyVrController.enabled)
            {
                return;
            }

            if (!_calibrateOnLongPress)
            {
                return;
            }

            if (message.input == _calibrationButton)
            {
                // start counting calibration press seconds
                if (Time.time - _calibrationStartPressTime >= _calibrationPressDuration)
                {
                    _flyVrController.CalibrateCenter();
                }
            }

        }
    }
}