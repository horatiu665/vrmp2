namespace Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using VRCore;

    public class FlyVRControllerTouchpadRadar : MonoBehaviour, IVRPlayerStartListener
    {
        private IVRPlayer _player;
        public Transform headDot;

        void Update()
        {
            if (_player != null)
            {
                headDot.localPosition = _player.head.localPosition;
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;
        }
    }
}
