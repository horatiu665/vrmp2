namespace Core
{
    using System.Collections;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(Rigidbody), typeof(IPlayer), typeof(FlyVRControllerCalibration))]
    public class FlyVRController : MonoBehaviour, IVRPlayerStartListener
    {
        [Header("Control params")]
        [SerializeField]
        private float _groundedDistanceCheck = 0.04f;

        [SerializeField]
        private float _groundedEpsilon = 0.01f;

        [Header("Audio")]
        [SerializeField]
        private AudioSource _soundBuzzing;

        [SerializeField]
        private AudioSource _soundCalibrate;

        private bool _grounded;

        private IPlayer _player;

        private Vector3 _oldLeftHand, _oldRightHand;
        private Quaternion _oldLeftHandRot, _oldRightHandRot;

        [Header("Flap controls")]
        [SerializeField]
        private float _handRotMulti = 1f;

        [SerializeField]
        private float _handPosMulti = 0f;

        [SerializeField]
        private AnimationCurve _humanJoystickRemap = new AnimationCurve(new Keyframe[] { new Keyframe(0f, 0f, 0f, 0f),
                                                                                         new Keyframe(0.4657314f, 0.5149549f, 3.206861f, 3.206861f),
                                                                                         new Keyframe(0.7938879f, 1f, 0f, 0f)  });

        [SerializeField]
        private AnimationCurve _hobbleRemap = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [SerializeField]
        private float _hobbleMax = 25;

        [SerializeField]
        private float _flyForce = 3.5f;

        [SerializeField]
        private Vector3 _calibratedCenter = new Vector3(0, 1.5f, 0f);

        [SerializeField]
        private float _headMaxMove = 0.3f;

        [Header("Slowdown Curve")]
        [SerializeField]
        private AnimationCurve _slowdownMultiplier = AnimationCurve.Linear(0, 1, 1, 1);

        private void Awake()
        {
            if (_soundBuzzing == null)
            {
                _soundBuzzing = GetComponentInChildren<AudioSource>();
            }
        }

        private void FixedUpdate()
        {
            // detect hand wobble
            Vector3 leftHandDelta = _player.leftHand.transform.position - _oldLeftHand;
            _oldLeftHand = _player.leftHand.transform.position;
            Vector3 rightHandDelta = _player.leftHand.transform.position - _oldRightHand;
            _oldRightHand = _player.rightHand.transform.position;

            var leftAngle = Quaternion.Angle(_oldLeftHandRot, _player.leftHand.transform.rotation);
            var rightAngle = Quaternion.Angle(_oldRightHandRot, _player.rightHand.transform.rotation);

            //Quaternion leftHandDeltaRot = _player.leftHand.transform.rotation * Quaternion.Inverse(_oldLeftHandRot);
            _oldLeftHandRot = _player.leftHand.transform.rotation;
            //Quaternion rightHandDeltaRot = _player.rightHand.transform.rotation * Quaternion.Inverse(_oldRightHandRot);
            _oldRightHandRot = _player.rightHand.transform.rotation;

            var hobble = (leftAngle + rightAngle) * _handRotMulti + (leftHandDelta.magnitude + rightHandDelta.magnitude) * _handPosMulti;
            var hobbleMapped = _hobbleRemap.Evaluate(hobble / _hobbleMax);

            // hobble can be used to apply a force towards human-analog-joystick
            var humanJoystickDirection = _player.head.localPosition - _calibratedCenter;
            var remappedDir = humanJoystickDirection.normalized * _humanJoystickRemap.Evaluate(humanJoystickDirection.magnitude / _headMaxMove);

            _player.AddForce(this, remappedDir * _flyForce * hobbleMapped * _slowdownMultiplier.Evaluate(_player.slowdownParam), ForceMode.Force);

            // grounded check
            RaycastHit hit;
            if (Physics.Raycast(transform.position + Vector3.up * _groundedDistanceCheck, Vector3.down, out hit, _groundedDistanceCheck * 2))
            {
                if (transform.position.y - hit.point.y < _groundedEpsilon)
                {
                    _grounded = true;
                }
                else
                {
                    _grounded = false;
                }
            }
            else
            {
                _grounded = false;
            }

            // anim
            var groundedAnim = !_grounded || (remappedDir.magnitude * hobbleMapped) > 0.1f;

            // audio
            if (_soundBuzzing != null)
            {
                if (!groundedAnim && _soundBuzzing.volume > 0)
                {
                    // stop playing audio
                    StopCoroutine("FadeAudioCor");
                    _soundBuzzing.volume = 0;
                }
                else if (groundedAnim && _soundBuzzing.volume == 0)
                {
                    // start playing audio with a little fade
                    FadeAudioIn();
                }
            }
        }

        private void FadeAudioIn()
        {
            StopCoroutine("FadeAudioCor");
            StartCoroutine("FadeAudioCor");
        }

        private IEnumerator FadeAudioCor()
        {
            yield return StartCoroutine(pTween.To(0.1f, 0, 1f, t =>
            {
                _soundBuzzing.volume = t;
            }));
        }

        public void CalibrateCenter()
        {
            _calibratedCenter = _player.head.localPosition;
            // GRAPHICS AND FEEDBACK FOR CALIBRATE CENTER
            Debug.Log("Calibrated! at " + _calibratedCenter);

            if (_soundCalibrate != null)
            {
                _soundCalibrate.Play();
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
        }
    }
}