namespace Core
{
    using System.Collections.Generic;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Locomotion")]
    [RequireComponent(typeof(Rigidbody), typeof(IPlayer))]
    public sealed class SkiPlayerJump : MonoBehaviour, IVRPlayerStartListener
    {
        private readonly IList<float> _samples = new List<float>();

        [SerializeField]
        private int _sampleFrames = 15;

        [SerializeField]
        private float _deltaYThresholdForJump = 3f;

        [SerializeField]
        private Vector3 _globalJumpVector = Vector3.up;

        [SerializeField]
        private Vector3 _localJumpVector = Vector3.zero;

        [SerializeField]
        private float _jumpForceMultiplier = 100f;

        [SerializeField]
        private float _maxJumpForce = 50f;

        [SerializeField]
        private float _sampleFactor = 100f;

        [SerializeField]
        private ForceMode _jumpForceMode = ForceMode.Force;

        private IPlayer _player;

        private void Awake()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            if (_player == null)
            {
                _player = (IPlayer)player;
            }
        }

        private void FixedUpdate()
        {
            if (_player == null || !_player.grounded)
            {
                return;
            }

            SampleHeadY();
            if (_samples.Count <= 1)
            {
                // cannot function with a single sample or less
                return;
            }

            var maxDeltaY = GetMaxDeltaY();
            if (maxDeltaY > _deltaYThresholdForJump)
            {
                _samples.Clear();

                var force = _globalJumpVector;
                if (_localJumpVector != Vector3.zero)
                {
                    force += _player.transform.TransformDirection(_localJumpVector);
                }

                force *= Mathf.Min(_maxJumpForce, _jumpForceMultiplier * maxDeltaY);
                _player.AddForce(this, force, _jumpForceMode);
            }
        }

        private void SampleHeadY()
        {
            if (_samples.Count > _sampleFrames)
            {
                _samples.RemoveAt(0);
            }

            if (_player.head.localPosition != Vector3.zero)
            {
                _samples.Add(_player.head.localPosition.y);
            }
        }

        // Calculates the forward max delta Y
        private float GetMaxDeltaY()
        {
            var maxDeltaY = 0f;
            var count = _samples.Count;
            for (int i = count - 1; i > 0; i--)
            {
                // simply measure the delta of the steepest sample in the jump
                var pY = (_samples[i] - _samples[i - 1]) * _sampleFactor;
                if (maxDeltaY < pY)
                {
                    maxDeltaY = pY;
                }
            }

            return maxDeltaY;
        }
    }
}