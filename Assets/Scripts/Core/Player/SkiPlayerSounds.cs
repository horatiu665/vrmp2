namespace Core
{
	using UnityEngine;
	using VRCore;

	[Apex.ApexComponent("Player")]
	[RequireComponent(typeof(IPlayer))]
	public sealed class SkiPlayerSounds : MonoBehaviour, IVRPlayerStartListener
	{
		private SkiHeadSteeringStrafe _headSteer;
		public SkiHeadSteeringStrafe headSteer
		{
			get
			{
				if (_headSteer == null)
				{
					_headSteer = GetComponent<SkiHeadSteeringStrafe>();
				}
				return _headSteer;
			}
		}

		[SerializeField]
		private VRAudio _takeOff = null;

		[SerializeField]
		private VRAudio _skiLanding = null;

		[SerializeField]
		private VRAudio _slideLoop = null;

		//[SerializeField]
		//private VRAudio _slideLoopBrakes = null;

		[SerializeField]
		private VRAudio _windAirborne = null;

		[Space]
		[SerializeField]
		private float _slideLoopSpeedThreshold = 0.5f;

		[SerializeField]
		private AnimationCurve _slideVolumeOnDragMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 1, 0, 0) } };

		[SerializeField]
		private Vector2 _dragRange = new Vector2(0, 1f);

		[SerializeField]
		private AnimationCurve _slideVolumeOnSpeedMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0.8f, 0, 0), new Keyframe(1, 1, 0, 0) } };

		[SerializeField]
		private AnimationCurve _slidePitchOnHeadSteer = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0.8f, 0, 0), new Keyframe(1, 1, 0, 0) } };

		private bool _grounded;
		private IPlayer _player;

		[Space]
		[SerializeField]
		private float oldHeadSide;
		[SerializeField]
		private float lerpOldHeadSide = 0.03f;
		[SerializeField]
		private float maxDeltaHeadSide = 0.1f;

		[Space]
		[SerializeField]
		private Vector2 distortionRange = new Vector2(0.25f, 0.66f);

		[SerializeField]
		private Vector2 cutoff = new Vector2(1200, 22000);

		[SerializeField]
		private AnimationCurve cutoffCurve = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

		private void OnEnable()
		{
			if (_player == null)
			{
				OnPlayerStart(this.GetComponent<IPlayer>());
			}
		}

		public void OnPlayerStart(IVRPlayer player)
		{
			if (_player == null)
			{
				_player = (IPlayer)player;
				_grounded = _player.grounded;
			}
		}

		private void Update()
		{
			if (_player.grounded && _player.velocity.sqrMagnitude > (_slideLoopSpeedThreshold * _slideLoopSpeedThreshold))
			{
				// while player is grounded, play slide loop sounds if the player is actually moving
				if (!_slideLoop.IsPlaying())
				{
					_slideLoop.Play();
				}
				//if (!_slideLoopBrakes.IsPlaying())
				//{
				//	_slideLoopBrakes.Play();
				//}
				SlideLoopVolumeControl();
			}
			else
			{
				if (_slideLoop.IsPlaying())
				{
					_slideLoop.Stop();
				}
				//if (_slideLoopBrakes.IsPlaying())
				//{
				//	_slideLoopBrakes.Stop();
				//}
			}

			if (_player.grounded && !_grounded)
			{
				// Grounded start
				_skiLanding.Play();
				_takeOff.Stop();
				_windAirborne.Stop();
				_grounded = true;
			}
			else if (!_player.grounded && _grounded)
			{
				// Grounded end
				_skiLanding.Stop();
				_takeOff.Play();
				_windAirborne.Play();
				_grounded = false;
			}
		}

		void SlideLoopVolumeControl()
		{
			var s = _slideLoop.GetAudioSource();

			if (s != null && _player != null)
			{
				var mag = _player.velocity.magnitude;
				var drag = headSteer.dragFromPlayerSide;
				var dragLerp = Mathf.InverseLerp(_dragRange.x, _dragRange.y, drag);
				s.volume = Mathf.Clamp01(_slideVolumeOnSpeedMultiplier.Evaluate(mag) * _slideVolumeOnDragMultiplier.Evaluate(dragLerp));
				var headSide = Mathf.Abs(_player.headSteeringParam);
				//s.pitch = _slidePitchOnHeadSteer.Evaluate(headSide);

				var deltaHeadSide = headSide - oldHeadSide;
				oldHeadSide = Mathf.Lerp(oldHeadSide, headSide, lerpOldHeadSide);
				var deltaHeadSideParam = deltaHeadSide / maxDeltaHeadSide;

				var m = s.outputAudioMixerGroup.audioMixer;
				var distortion = distortionRange.Lerp(headSide * deltaHeadSideParam);
				var lowPass = cutoff.Lerp(cutoffCurve.Evaluate(headSide * deltaHeadSideParam));
				m.SetFloat("SkiingDistortion", distortion);
				m.SetFloat("SkiingLowpassCutoff", lowPass);
			}
		}
	}
}