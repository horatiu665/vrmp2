namespace Core
{
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Player")]
    public sealed class SkiPlayerColorWheel : MonoBehaviour, IVRPlayerStartListener
    {
        ////[SerializeField]
        ////private Transform _wheel = null;

        [SerializeField]
        private Color minColor = Color.green, maxColor = Color.red;

        [SerializeField]
        private Material mat = null;

        ////[SerializeField]
        ////[Range(0f, 1f)]
        ////private float _smoothTurn = 0.9f;

        private IPlayer _player;

        private void LateUpdate()
        {
            // move head position under head, on the slope below.
            if (_player != null)
            {
                // amount of steering:
                var steerAmount = _player.headSteeringParam;
                mat.SetColor("_Color", Color.Lerp(minColor, maxColor, Mathf.Abs(steerAmount)));
            }
        }

        private void Awake()
        {
            if (_player == null)
            {
                _player = GetComponentInParent<IPlayer>();
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
        }
    }
}