namespace Core
{
    using System;
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    [Apex.ApexComponent("Player")]
    public sealed class PlayerSnowParticleController : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField, FormerlySerializedAs("rotate")]
        private bool _rotate = false;

        [SerializeField, FormerlySerializedAs("maxRbSpeed")]
        private float _maxRbSpeed = 100;

        [SerializeField, FormerlySerializedAs("maxParticleSpeed")]
        private float _maxParticleSpeed = 1;

        [SerializeField, FormerlySerializedAs("playerSpeedToEmission")]
        private AnimationCurve _playerSpeedToEmission = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField, FormerlySerializedAs("eRbMax")]
        private float _eRbMax = 60;

        [SerializeField, FormerlySerializedAs("eRange")]
        private Vector2 _eRange = new Vector2(500, 5000);

        [SerializeField, FormerlySerializedAs("playerSpeedToPos")]
        private AnimationCurve _playerSpeedToPos = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField, FormerlySerializedAs("speedMax")]
        private float _speedMax = 60;

        [SerializeField, FormerlySerializedAs("posMax")]
        private float _posMax = 60;

        [SerializeField, FormerlySerializedAs("limitVelocityScale")]
        private float _limitVelocityScale = 1f;

        private IPlayer _player;
        private ParticleSystem.ForceOverLifetimeModule _v;
        private ParticleSystem.EmissionModule _e;
        private ParticleSystem.LimitVelocityOverLifetimeModule _l;
        private ParticleSystem _ps;

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
            _ps = this.GetComponent<ParticleSystem>();
            _v = _ps.forceOverLifetime;
            _e = _ps.emission;
            _l = _ps.limitVelocityOverLifetime;
        }

        private void Update()
        {
            if (_player == null)
            {
                return;
            }

            if (_rotate)
            {
                _ps.transform.LookAt(transform.position + _player.rb.velocity + Vector3.down);
            }

            var vel = (_player.rb.velocity / _maxRbSpeed) + Vector3.down;
            vel *= _maxParticleSpeed;
            _v.x = vel.x;
            _v.y = vel.y;
            _v.z = vel.z;

            _e.rateOverTime = Mathf.Lerp(_eRange.x, _eRange.y, _playerSpeedToEmission.Evaluate(_player.rb.velocity.magnitude / _eRbMax));

            _ps.transform.position = _player.position + (_player.rb.velocity.normalized * _playerSpeedToPos.Evaluate(_player.rb.velocity.magnitude / _speedMax) * _posMax);

            _l.limit = _limitVelocityScale;
            
        }
        
    }
}