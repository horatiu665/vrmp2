namespace Core
{
    using UnityEngine;
    using VRCore;

    public sealed class SkiPlayerCaptainsWheel : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private Transform _wheel = null;

        [SerializeField]
        private AnimationCurve _headOffsetToRotateWheel = AnimationCurve.Linear(0, 0, 1, 1);

        [SerializeField]
        private float _maxRotateWheel = 25f;

        [SerializeField]
        [Range(0, 1f)]
        private float _smoothTurn = 0.9f;

        private IPlayer _player;

        private void LateUpdate()
        {
            // move head position under head, on the slope below.
            if (_player != null)
            {
                // amount of steering:
                var steerAmount = _player.headSteeringParam;
                var curRot = _wheel.localEulerAngles.y;
                if (curRot > 180)
                {
                    curRot -= 360;
                }

                var newRot = Mathf.Lerp(curRot, _headOffsetToRotateWheel.Evaluate(Mathf.Abs(steerAmount)) * Mathf.Sign(steerAmount) * _maxRotateWheel, _smoothTurn);
                _wheel.localEulerAngles = Vector3.up * newRot;
            }
        }

        private void Awake()
        {
            if (_player == null)
            {
                _player = GetComponentInParent<IPlayer>();
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
        }
    }
}