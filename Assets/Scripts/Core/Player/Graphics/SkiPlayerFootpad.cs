﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class SkiPlayerFootpad : MonoBehaviour, IVRPlayerStartListener
{
    private SkiPlayerOnSkisFeedback _onSkisFeedback;
    public SkiPlayerOnSkisFeedback onSkisFeedback
    {
        get
        {
            if (_onSkisFeedback == null)
            {
                _onSkisFeedback = GetComponentInParent<SkiPlayerOnSkisFeedback>();
            }
            return _onSkisFeedback;
        }
    }

    private IPlayer _player;
    public IPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = GetComponentInParent<IPlayer>();
            }
            return _player;
        }
    }

    private Renderer _r;
    public Renderer r
    {
        get
        {
            if (_r == null)
            {
                _r = GetComponent<Renderer>();
            }
            return _r;
        }
    }

    [Header("Setup")]
    [SerializeField]
    private Vector3 offSkiPos = new Vector3(0, 0.03f, 0f);

    [SerializeField]
    private Vector3 onSkiPos = new Vector3(0, 0.02f, 0f);

    public Material skiPad;

    [Header("On off colors")]

    public Color colorOn = new Color(0.4f, 1f, 0.3f), colorOff = new Color(0.5f, 0.1f, 0.1f);
    
    private void OnEnable()
    {
        skiPad = r.material;
        onSkisFeedback.SetOnSki += OnSkisFeedback_SetOnSki;
    }

    private void OnDisable()
    {
        onSkisFeedback.SetOnSki -= OnSkisFeedback_SetOnSki;
    }

    private void OnSkisFeedback_SetOnSki(bool onSki)
    {
        SetOnSki(onSki);
    }

    public void SetOnSki(bool onSki)
    {
        if (!player.isLocal)
        {
            onSki = true;
        }
        transform.localPosition = onSki ? onSkiPos : offSkiPos;
        skiPad.color = onSki ? colorOn : colorOff;
    }

    //public void SetSteering(float steeringParam)
    //{
    //}

    //private void Update()
    //{
    //    SetSteering(player.headSteeringParam);
    //}

    public void OnPlayerStart(IVRPlayer conPlayer)
    {
        _player = conPlayer as IPlayer;
        if (player != null)
        {
            colorOn = player.color;
        }
        if (!player.isLocal)
        {
            SetOnSki(true);
        }
    }
}