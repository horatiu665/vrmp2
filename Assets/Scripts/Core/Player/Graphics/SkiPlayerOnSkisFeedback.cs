﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class SkiPlayerOnSkisFeedback : MonoBehaviour
{

    private IPlayer _player;
    public IPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = GetComponent<IPlayer>();
            }
            return _player;
        }
    }

    [Header("On off skis")]
    [SerializeField]
    private Vector2 onOffSkiThresholdsSlow = new Vector2(0.3f, 0.7f);

    // when standing still, onoff threshold should be smaller than when moving...?
    [SerializeField]
    private Vector2 onOffSkiThresholdsFast = new Vector2(0.75f, 1.3f);

    [SerializeField]
    private Vector2 speedRangeForThresholds = new Vector2(3f, 50f);

    public bool onSkis { get; private set; }

    public event System.Action<bool> SetOnSki;

    private void Start()
    {
        var centerToHead = player.head.position - player.skis.position;
        centerToHead.y = 0;
        var dist = centerToHead.magnitude;
        var onOffSkiThresholds = Vector2.Lerp(onOffSkiThresholdsSlow, onOffSkiThresholdsFast, Mathf.InverseLerp(speedRangeForThresholds.x, speedRangeForThresholds.y, player.velocityController.speed));
        if (dist < onOffSkiThresholds.x)
        {
            onSkis = true;
        }
        else
        {
            onSkis = false;
        }
        SetOnSki(onSkis);

    }

    private void Update()
    {
        var onOffSkiThresholds = Vector2.Lerp(onOffSkiThresholdsSlow, onOffSkiThresholdsFast, Mathf.InverseLerp(speedRangeForThresholds.x, speedRangeForThresholds.y, player.velocityController.speed));

        var centerToHead = player.head.position - player.skis.position;
        centerToHead.y = 0;
        var dist = centerToHead.magnitude;
        if (onSkis)
        {
            if (dist > onOffSkiThresholds.y)
            {
                onSkis = false;
                if (SetOnSki != null)
                {
                    SetOnSki(false);
                }
            }
        }
        else
        {
            if (dist < onOffSkiThresholds.x)
            {
                onSkis = true;
                if (SetOnSki != null)
                {
                    SetOnSki(true);
                }
            }
        }
    }

}
