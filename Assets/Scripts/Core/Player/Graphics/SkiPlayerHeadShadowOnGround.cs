namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Player")]
    public sealed class SkiPlayerHeadShadowOnGround : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private Transform _tip = null;

        [SerializeField]
        private Transform _back = null;

        [SerializeField]
        private Transform _center = null;

        [SerializeField]
        private bool _putShadowOnSlope = false;

        private IPlayer _player = null;
        private SkiHeadSteeringStrafe _steeringModule = null;

        [SerializeField]
        private Vector2 _velocityClampRange = new Vector2(1, 7);

        [SerializeField]
        private AnimationCurve _velocityMapping = AnimationCurve.Linear(0, 0, 1, 1f);

        [SerializeField]
        private AnimationCurve _headFwMapping = AnimationCurve.Linear(-1, -1, 1, 1f);

        [SerializeField]
        private float _headFwMax = 1f;

        [SerializeField]
        private float _headFwMultiplier = 1f;

        // initial distance from _back to _center.
        private float _backInitDist;

        private float _initCenterToTip;

        private void LateUpdate()
        {
            // move head position under head, on the slope below.
            if (_player != null && _steeringModule != null)
            {
                // pos is the local position of the head on the ground. Optionally, it can be on the slope itself.
                var pos = _player.head.localPosition;
                pos.y = 0;
                if (_putShadowOnSlope)
                {
                    // if there is a slope below, put the head pos on the ground.
                    RaycastHit hit;
                    if (Physics.Raycast(_player.head.position, Vector3.down, out hit, 3f, Layers.terrain))
                    {
                        pos.y -= _player.transform.position.y - hit.point.y;
                    }
                }

                //var headRight = _player.headSteeringParam;
                var headFw = _player.headForwardParam;

                // cur player speed, cache
                var curSpeed = _player.rb.velocity.magnitude;
                // clamp speed
                var curSpeedClamped = Mathf.Clamp(curSpeed, _velocityClampRange.x, _velocityClampRange.y);

                Vector3 curVelClamped;

                // minimum velocity tiny?  we just set it to forward * min.
                if (curSpeedClamped == _velocityClampRange.x)
                {
                    curVelClamped = _player.skis.forward * _velocityClampRange.x;
                }
                else
                {
                    // remap the curspeedclamped as if the curve was mapped instead of 0 to 1, min to max, on both axes.
                    curSpeedClamped = Mathf.Lerp(_velocityClampRange.x, _velocityClampRange.y, _velocityMapping.Evaluate(Mathf.InverseLerp(_velocityClampRange.x, _velocityClampRange.y, curSpeedClamped)));

                    // clamp velocity using clamped speed
                    curVelClamped = _player.rb.velocity / curSpeed * curSpeedClamped;
                }

                // TIP

                // time it will take to get from the center to the tip, with the current clamped velocity
                var tAtTip = _initCenterToTip / curSpeedClamped;
                // current acceleration based only on strafing
                var a = _steeringModule.finalStrafeVel * _player.skis.right;
                // velocity we will have after the time calculated above, considering the current steering velocity
                var vAtTip = curVelClamped + (a * tAtTip);

                var fw = _headFwMapping.Evaluate(headFw / _headFwMax) * _headFwMultiplier;

                // new position using the formula for position, velocity, acceleration based on time.
                _tip.position = _center.position + (curVelClamped * tAtTip) + (a * tAtTip * tAtTip * 0.5f) + (fw * _player.skis.forward);

                // direction is the velocity at tip.
                _tip.LookAt(_tip.position + vAtTip);

                // BACK
                var tAtBack = -_backInitDist / curSpeedClamped;
                var vAtBack = curVelClamped + (a * tAtBack);
                _back.position = _center.position + (curVelClamped * tAtBack) + (a * tAtBack * tAtBack * 0.5f);
                _back.LookAt(_back.position - vAtBack);
            }
        }

        private void Awake()
        {
            if (_player == null)
            {
                _player = GetComponentInParent<IPlayer>();
            }

            _initCenterToTip = (_center.position - transform.position).magnitude;
            _backInitDist = (_back.position - _center.position).magnitude;
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
            _steeringModule = _player.transform.GetComponent<SkiHeadSteeringStrafe>();
        }
    }
}