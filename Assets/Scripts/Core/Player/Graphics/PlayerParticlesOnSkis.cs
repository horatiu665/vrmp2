using Core;
using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlayerParticlesOnSkis : MonoBehaviour
{
	private IPlayer _player;
	public IPlayer player
	{
		get
		{
			if (_player == null)
			{
				_player = GetComponentInParent<IPlayer>();
			}
			return _player;
		}
	}

	private ParticleSystem _ps;
	public ParticleSystem ps
	{
		get
		{
			if (_ps == null)
			{
				_ps = GetComponent<ParticleSystem>();
			}
			return _ps;
		}
	}

	[Header("Emission")]
	public AnimationCurve emitRateToPlayerSpeed = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
	public Vector2 emitRateRange = new Vector2(20, 50f);
	public Vector2 playerSpeedRange = new Vector2(5, 50f);

	public AnimationCurve emitRateToPlayerDragMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
	public Vector2 dragRange = new Vector2(0.2f, 1f);

	[Header("Local Velocity")]
	public Vector2 particleSpeedRange = new Vector2(1, 5f);
	public Vector2 localVelYRange = new Vector2(-1.5f, -3.5f);
	public Vector2 localVelZRange = new Vector2(-1.5f, -3.5f);

	void Update()
	{
		if (player.grounded)
		{
			var speed = playerSpeedRange.InverseLerp(player.velocity.magnitude);
			//var ers = Mathf.Lerp(emitRateRange.x, emitRateRange.y, emitRateToPlayerSpeed.Evaluate(Mathf.InverseLerp(playerSpeedRange.x, playerSpeedRange.y, pSpeed)));
			var ers = emitRateRange.Lerp(emitRateToPlayerSpeed.Evaluate(speed));
			var drag = dragRange.InverseLerp(player.rb.drag);
			var erd = emitRateToPlayerDragMultiplier.Evaluate(drag);

			var e = ps.emission;
			e.rateOverTime = new ParticleSystem.MinMaxCurve(ers * erd);

			var v = ps.velocityOverLifetime;
			v.zMultiplier = localVelZRange.Lerp(speed);
			v.yMultiplier = localVelYRange.Lerp(speed) * Mathf.Sign(v.yMultiplier);

			var m = ps.main;
			m.startSpeedMultiplier = particleSpeedRange.Lerp(speed);
		}
		else
		{
			var e = ps.emission;
			e.rateOverTime = 0f;
		}

	}
}