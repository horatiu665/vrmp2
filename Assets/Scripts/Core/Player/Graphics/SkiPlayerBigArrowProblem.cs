﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public class SkiPlayerBigArrowProblem : MonoBehaviour
{
    private Animator _anim;
    public Animator anim
    {
        get
        {
            if (_anim == null)
            {
                _anim = GetComponentInChildren<Animator>();
            }
            return _anim;
        }
    }

    private Renderer _r;
    public Renderer r
    {
        get
        {
            if (_r == null)
            {
                _r = GetComponentInChildren<Renderer>();
            }
            return _r;
        }
    }

    private SkiPlayerOnSkisFeedback _onSkisFeedback;
    public SkiPlayerOnSkisFeedback onSkisFeedback
    {
        get
        {
            if (_onSkisFeedback == null)
            {
                _onSkisFeedback = GetComponentInParent<SkiPlayerOnSkisFeedback>();
            }
            return _onSkisFeedback;
        }
    }

    public AnimationCurve playerDistToScale = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    Vector3 initScale;
    private float curScale = 1f;

    public Vector2 maxRatioDelta = new Vector2(0.03f, 0.05f);

    private void OnEnable()
    {
        onSkisFeedback.SetOnSki += OnSkisFeedback_SetOnSki;
        initScale = transform.localScale;
    }

    private void OnDisable()
    {
        onSkisFeedback.SetOnSki -= OnSkisFeedback_SetOnSki;
    }

    private void OnSkisFeedback_SetOnSki(bool onSkis)
    {
        r.enabled = !onSkis;
        anim.enabled = !onSkis;
    }

    private void Update()
    {
        if (!onSkisFeedback.onSkis)
        {
            var dist = onSkisFeedback.player.head.position - onSkisFeedback.player.skis.position;
            var mag = new Vector2(dist.x, dist.z).magnitude;
            SetScale(playerDistToScale.Evaluate(mag));
        }

    }

    private void SetScale(float scale)
    {
        // if >1, new scale is larger. if <1, new scale is smaller.
        var ratio = scale / curScale;
        if (ratio > 1 + maxRatioDelta.y)
        {
            scale = curScale * (1 + maxRatioDelta.y);
        }
        else if (ratio < 1 - maxRatioDelta.x)
        {
            scale = Mathf.Max(curScale * (1 - maxRatioDelta.x), 0.001f);
        }
        transform.localScale = initScale * scale;
        curScale = scale;
    }

}
