namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Player")]
    [RequireComponent(typeof(IPlayer))]
    public sealed class SkiPlayerTurningSkiGraphics : MonoBehaviour, IVRPlayerStartListener, IOriginShifter
    {
        [Header("Turning skis")]
        [SerializeField]
        private float _turnSkisDistance = 0.1f;

        [SerializeField]
        private bool _turnSkisOnlyOnY = false;

        [SerializeField]
        private bool _turnSkisUseNormalForUp = true;

        private Vector3 _turnSkisLastPos;
        private IPlayer _player;

        private void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
        }

        public void OnWorldMove(Vector3 delta)
        {
            _turnSkisLastPos -= delta;
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
            _turnSkisLastPos = _player.skis.position - _player.skis.forward;
        }

        private void FixedUpdate()
        {
            if (_player == null)
            {
                return;
            }

            var skiForward = _player.skis;

            // ========================== ROTATING SKIFORWARD ==============================
            var delta = skiForward.position - _turnSkisLastPos;
            if (_turnSkisOnlyOnY)
            {
                delta.y = 0f;
            }

            var deltaSqrMagnitude = delta.sqrMagnitude;
            if (deltaSqrMagnitude == 0f)
            {
                delta = Vector3.forward;
                _turnSkisLastPos = skiForward.position - (delta.normalized * _turnSkisDistance);
            }
            else if (deltaSqrMagnitude >= (_turnSkisDistance * _turnSkisDistance))
            {
                _turnSkisLastPos = skiForward.position - (delta.normalized * _turnSkisDistance);
            }

            if (_turnSkisUseNormalForUp)
            {
                if (_player.grounded && _player.groundedRay.normal != Vector3.zero)
                {
                    skiForward.rotation = Quaternion.LookRotation(_player.groundedRay.normal, delta);
                    var up = skiForward.up;
                    var fw = skiForward.forward;
                    skiForward.up = fw;
                    skiForward.forward = up;
                }
                else
                {
                    skiForward.forward = delta;
                }
            }
            else
            {
                skiForward.forward = delta;
            }
        }
    }
}