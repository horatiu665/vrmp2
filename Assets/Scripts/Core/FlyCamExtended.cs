using UnityEngine;
using System.Collections;

public class FlyCamExtended : MonoBehaviour
{

    /*
	EXTENDED FLYCAM
		Desi Quintans (CowfaceGames.com), 17 August 2012.
		Based on FlyThrough.js by Slin (http://wiki.unity3d.com/index.php/FlyThrough), 17 May 2011.
 
	LICENSE
		Free as in speech, and free as in beer.
 
	FEATURES
		WASD/Arrows:    Movement
				  Q:    Climb
				  E:    Drop
					  Shift:    Move faster
					Control:    Move slower
						End:    Toggle cursor locking to screen (you can also press Ctrl+P to toggle play mode on and off).
	*/

    public float cameraSensitivity = 90;
    public bool invertMouseY = true;
    public float climbSpeed = -4;
    public float normalMoveSpeed = 10;
    public Vector2 slowMoveFactor = new Vector2(1, 0.25f);
    public Vector2 fastMoveFactor = new Vector2(1f, 100f);

    public float speedIncreaseDuration = 5f;

    float speedIncreaseTime;

    public bool lockCursorOnStart = false;
    bool lockCursor = true;

    private float rotationX = 0.0f;
    private float rotationY = 0.0f;

    bool lockControls = false;

    void Start()
    {
        if (!Application.isEditor && lockCursorOnStart)
        {
            Cursor.lockState = lockCursor ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !lockCursor;
        }

        rotationY = transform.eulerAngles.x - (transform.eulerAngles.x > 90 ? 360 : 0);
        rotationX = transform.eulerAngles.y;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.End))
        {
            lockControls = !lockControls;

            // if controls are locked, cursor visible. if not locked, cursor hidden.
            lockCursor = !lockControls;
            Cursor.lockState = lockCursor ? CursorLockMode.Locked : CursorLockMode.None;
            Cursor.visible = !lockCursor;
        }

        if (lockControls)
            return;

        rotationX += Input.GetAxis("Mouse X") * cameraSensitivity * Time.deltaTime;
        rotationY += (invertMouseY ? -1 : 1) * Input.GetAxis("Mouse Y") * cameraSensitivity * Time.deltaTime;
        rotationY = Mathf.Clamp(rotationY, -90, 90);

        transform.eulerAngles = new Vector3(rotationY, rotationX, transform.eulerAngles.z);

        var iVector = new Vector3(Input.GetAxis("Horizontal"), Input.GetKey(KeyCode.Q) ? -1f : Input.GetKey(KeyCode.E) ? 1f : 0f, Input.GetAxis("Vertical"));
        if (iVector.sqrMagnitude == 0)
        {
            speedIncreaseTime = 0;
        }

        float moveFactor = 1f;

        if (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
        {
            speedIncreaseTime += Time.deltaTime;
            var fastMoveFactor = Mathf.Lerp(this.fastMoveFactor.x, this.fastMoveFactor.y, Mathf.Clamp01(speedIncreaseTime / speedIncreaseDuration));
            moveFactor = fastMoveFactor;
        }
        else if (Input.GetKey(KeyCode.LeftControl) || Input.GetKey(KeyCode.RightControl))
        {
            speedIncreaseTime += Time.deltaTime;
            var slowMoveFactor = Mathf.Lerp(this.slowMoveFactor.x, this.slowMoveFactor.y, Mathf.Clamp01(speedIncreaseTime / speedIncreaseDuration));
            moveFactor = slowMoveFactor;
        }
        else
        {
            speedIncreaseTime = 0;
        }

        transform.position += transform.forward * normalMoveSpeed * moveFactor * iVector.z * Time.deltaTime;
        transform.position += transform.right * normalMoveSpeed * moveFactor * iVector.x * Time.deltaTime;
        transform.position += transform.up * climbSpeed * moveFactor * iVector.y * Time.deltaTime;
    }

}
