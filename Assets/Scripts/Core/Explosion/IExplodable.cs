namespace Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    interface IExplodable
    {
        void OnExplosion(Explosion e);
    }
}
