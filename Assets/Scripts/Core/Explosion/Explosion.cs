namespace Core
{
    using System.Collections;
    using System.Collections.Generic;
    using Apex.Utilities;
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;
    using Random = UnityEngine.Random;
    using VRCore.VRNetwork.Server;
    using Server;

    public class Explosion : MonoBehaviour
    {
        private static readonly Collider[] _collisionBuffer = new Collider[200];

        [Header("Controls")]
        [SerializeField]
        private bool _explodeOnEnable = false;

        [Header("Explosion params")]
        [SerializeField]
        private float _destroySelfDelay = 5f;

        [SerializeField]
        private float _delayAfterParticleStart = 0.2f;

        [SerializeField]
        private float _addForceRadius = 10f;

        [SerializeField]
        private bool _unimpaleArrows = true;

        [SerializeField]
        private float _unimpaleArrowsRadius = 7f;

        [SerializeField]
        private float _addForceAmount = 10f;

        [SerializeField]
        private float _forceUpwardsMultiplier = 1f;

        [SerializeField]
        private AudioSource _soundExplosion;

        [Header("Destroy params")]
        [SerializeField]
        private float _destroyOthersChance = 0.5f;

        // destroy more stuff when there is more stuff to destroy

        [Header("Spawn params")]
        [SerializeField]
        private Vector2 _shrapnelSpawnAmount = new Vector2(2, 5);

        [SerializeField]
        private float _shrapnelSpawnRadius = 0.2f;

        [SerializeField]
        private List<VRPrefabType> _shrapnelTypes = new List<VRPrefabType>();

        private ParticleSystem _ps;

        private void Awake()
        {
            _ps = this.GetComponentInChildren<ParticleSystem>();
        }

        // NOT START because pooling
        private void OnEnable()
        {
            if (_explodeOnEnable)
            {
                Explode();
            }
        }

        public void Explode()
        {
            if (_ps != null)
            {
                _ps.Play();
            }

            if (_soundExplosion != null)
            {
                _soundExplosion.Play();
            }

            // ONLY do on server or single player
            if (NetServices.isServer || !NetServices.isNetworked)
            {
                StartCoroutine(ServerExplodeCoroutine());
            }
            else
            {
                // destroy on client
                StartCoroutine(pTween.Wait(_destroySelfDelay, () =>
                {
                    VRPrefabManager.instance.Return(GetComponent<IVRPrefab>());
                }));
            }

        }

        private IEnumerator ServerExplodeCoroutine()
        {
            yield return new WaitForSeconds(_delayAfterParticleStart);

            // find unique rigidbodies
            Physics.OverlapSphereNonAlloc(transform.position, _addForceRadius, _collisionBuffer);
            var rbs = ListBufferPool.GetBuffer<Rigidbody>(_collisionBuffer.Length);
            for (int i = 0; i < _collisionBuffer.Length; i++)
            {
                if (_collisionBuffer[i] == null)
                {
                    continue;
                }

                var r = _collisionBuffer[i].attachedRigidbody;
                if (r != null && !rbs.Contains(r))
                {
                    rbs.Add(r);
                }
            }

            var count = rbs.Count;

            // destroy rbs
            // decide whether or not to destroy some of those rigidbodies using destroyChance
            if (_destroyOthersChance > 0f || _unimpaleArrows)
            {
                for (int i = count - 1; i >= 0; i--)
                {
                    if (Random.value <= _destroyOthersChance)
                    {
                        if (rbs[i].gameObject.GetComponent<ExplosionDeletable>())
                        {
                            DestroyCorrect(rbs[i].gameObject);

                            // remove from list
                            rbs.RemoveAt(i);
                        }
                    }
                    else if (_unimpaleArrows)
                    {
                        // if not destroy then we can try to remove its joint
                        if ((rbs[i].position - transform.position).sqrMagnitude < _unimpaleArrowsRadius * _unimpaleArrowsRadius)
                        {
                            var arr = rbs[i].GetComponent<Arrow>();
                            if (arr != null)
                            {
                                ImpalementHandler.Unimpale(arr);
                            }
                        }
                    }
                }
            }

            // spawn coals and rubbish
            var pos = transform.position;
            var coalsToSpawn = _shrapnelSpawnAmount.RandomInt();
            for (int i = 0; i < coalsToSpawn; i++)
            {
                // spawn a coal and add it to the rb list for adding forces
                if (NetServices.isNetworked)
                {
                    var p = ServerNetReceiver.instance.SpawnNetEntity(_shrapnelTypes.Random(), pos + Random.onUnitSphere * _shrapnelSpawnRadius);
                    rbs.Add(p.GetComponent<Rigidbody>());
                }
                else // singleplayer
                {
                    var p = VRPrefabManager.instance.Spawn<INetEntity>(_shrapnelTypes.Random(), pos + Random.onUnitSphere * _shrapnelSpawnRadius, Quaternion.identity);
                    rbs.Add(p.GetComponent<Rigidbody>());
                }

            }

            count = rbs.Count;
            // add explosion force to everybody
            for (int i = 0; i < count; i++)
            {
                rbs[i].AddExplosionForce(_addForceAmount, transform.position, _addForceRadius, _forceUpwardsMultiplier, ForceMode.Impulse);
            }

            // call event on everybody wherever it applies
            for (int i = 0; i < rbs.Count; i++)
            {
                var explodables = rbs[i].GetComponents<IExplodable>();
                for (int j = 0; j < explodables.Length; j++)
                {
                    explodables[j].OnExplosion(this);
                }
            }

            yield return new WaitForSeconds(_destroySelfDelay);

            VRPrefabManager.instance.Return(GetComponent<IVRPrefab>());
            ListBufferPool.ReturnBuffer(rbs);
        }

        private void DestroyCorrect(GameObject go)
        {
            var manager = NetEntityDestructionManager.instance;
            if (manager != null)
            {

                // if go is equippable and is equipped, don't destroy
                var eq = go.GetComponent<IVREquippable>();
                if (eq != null && eq.isEquipped)
                {
                    return;
                }

                var netEntity = go.GetComponent<INetEntity>();
                if (netEntity != null)
                {
                    manager.DestroyNetEntity(netEntity);
                }

            }
            else
            {

                // if go is equippable and is equipped, don't destroy
                var eq = go.GetComponent<IVREquippable>();
                if (eq != null && eq.isEquipped)
                {
                    return;
                }

                VRPrefabManager.instance.Return(eq);
            }
        }
    }
}