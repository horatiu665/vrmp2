using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class AutoMoveQuickndirty : MonoBehaviour 
{
    public float speed = 150f;
    public float rotate = 90f;
    
	void Update() 
	{
        transform.position += transform.forward * speed * Time.deltaTime;
        transform.Rotate(0, rotate * Time.deltaTime, 0);
	}
}