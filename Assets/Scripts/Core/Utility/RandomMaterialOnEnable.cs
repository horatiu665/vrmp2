﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

//[ExecuteInEditMode]
public class RandomMaterialOnEnable : MonoBehaviour
{
	public new Renderer renderer;

	public Material[] materials;

	private void Reset()
	{
		if (renderer == null)
		{
			renderer = GetComponentInChildren<Renderer>();
		}
	}

	private void OnEnable()
	{
		renderer.sharedMaterial = materials[Random.Range(0, materials.Length)];
	}
	
	[DebugButton]
	void RandomMat()
	{
		OnEnable();
	}
}