﻿namespace Helpers
{
    using UnityEngine;

    public sealed class DontDestroyOnLoadComponent : MonoBehaviour
    {
        private static bool _loaded = false;

        private void Awake()
        {
            if (_loaded)
            {
                Destroy(this.gameObject);
                return;
            }

            this.transform.SetParent(null); // cannot be parented if it is to be DontDestroyOnLoad
            DontDestroyOnLoad(this.gameObject);
            _loaded = true;
        }
    }
}