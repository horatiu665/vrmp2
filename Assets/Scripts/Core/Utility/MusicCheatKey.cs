﻿namespace Core.Level
{
    using UnityEngine;

    [RequireComponent(typeof(AudioSource))]
    public sealed class MusicCheatKey : MonoBehaviour
    {
        [SerializeField]
        private KeyCode _key = KeyCode.M;

        private AudioSource _audio;

        private void Awake()
        {
            _audio = this.GetComponent<AudioSource>();
        }

        private void Update()
        {
            if (Input.GetKeyDown(_key))
            {
                if (_audio.isPlaying)
                {
                    _audio.Stop();
                }
                else
                {
                    _audio.Play();
                }
            }
        }
    }
}