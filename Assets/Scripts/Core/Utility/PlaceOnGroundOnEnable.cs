using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlaceOnGroundOnEnable : MonoBehaviour
{
	public bool onEnable = false;

	private void OnEnable()
	{
		if (onEnable)
			PlaceOnGround();
	}

	[DebugButton]
	public void PlaceOnGround()
	{
		var pos = transform.position;
		pos.y = MountainHeightProviderSingleton.SampleHeight(pos);
		transform.position = pos;
	}
}