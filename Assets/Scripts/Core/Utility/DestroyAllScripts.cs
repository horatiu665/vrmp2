namespace Core
{
    using UnityEngine;
    using VRCore;

    public class DestroyAllScripts : MonoBehaviour
    {
        [DebugButton]
        void DestroyAllMonobehavioursOnThisObject()
        {
            DeleteRecursive(transform);
        }

        void DeleteRecursive(Transform parent)
        {
            foreach (var gg in GetComponentsInChildren<MonoBehaviour>())
            {
                if (gg != this)
                {
                    DestroyImmediate(gg);
                }
            }
        }
    }
}
