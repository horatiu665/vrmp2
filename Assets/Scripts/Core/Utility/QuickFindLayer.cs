#if UNITY_EDITOR

namespace Core
{
    using System.Linq;
    using System.Text;
    using UnityEditor;
    using UnityEngine;
    using VRCore;

    public sealed class QuickFindLayer : MonoBehaviour
    {
        [DebugButton]
        private void CountObjectsInLayer(string layer)
        {
            var layerInt = LayerMask.NameToLayer(layer);
            var prefabCount = 0;
            StringBuilder sb = new StringBuilder();
            var allPrefabPaths = GetAllPrefabs();
            for (int i = 0; i < allPrefabPaths.Length; i++)
            {
                var prefab = AssetDatabase.LoadMainAssetAtPath(allPrefabPaths[i]) as GameObject;
                if (prefab != null)
                {
                    if (prefab.layer == layerInt)
                    {
                        prefabCount++;
                        sb.AppendLine(prefab.name);
                    }
                }
            }

            Debug.Log("Found " + prefabCount + " prefabs in the layer " + layer + ".\n" + sb.ToString());
        }

        public static string[] GetAllPrefabs()
        {
            var temp = AssetDatabase.GetAllAssetPaths();
            return temp.Where(s => s.Contains(".prefab")).ToArray();
        }
    }
}

#endif