﻿using UnityEngine;

public class ParentToOnStart : MonoBehaviour
{
    [Header("Leave null to parent to null")]
    public Transform parent;

    // Use this for initialization
    private void Start()
    {
        this.transform.SetParent(this.parent);
    }
}