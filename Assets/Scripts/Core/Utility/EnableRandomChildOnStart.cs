using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnableRandomChildOnStart : MonoBehaviour
{
    public bool useUnityRandomOnStart = false;

    private void Start()
    {
        if (useUnityRandomOnStart)
        {
            EnableChild(Random.Range(0, transform.childCount));
        }
    }

    [DebugButton]
    public void EnableChild(int index)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).gameObject.SetActive(false);
        }
        transform.GetChild(index % transform.childCount).gameObject.SetActive(true);
    }

    [DebugButton]
    public void TogglePrevious()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
            {
                transform.GetChild(i).gameObject.SetActive(false);
                transform.GetChild((i - 1 + transform.childCount) % transform.childCount).gameObject.SetActive(true);
                return;
            }
        }
    }
    [DebugButton]
    public void ToggleNext()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
            {
                transform.GetChild(i).gameObject.SetActive(false);
                transform.GetChild((i + 1) % transform.childCount).gameObject.SetActive(true);
                return;
            }
        }
    }

}