﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class ToiletTeleporter : MonoBehaviour
{
	// will be used to identify the toilet for teleporting to it even when so far away that it was not spawned yet
	public int toiletId = 0;

	[Header("Drag and drop")]
	public Transform teleportPosition;



	public void TeleportPlayerHere(IVRPlayer player)
	{
		player.transform.position = teleportPosition.position;

		// set global rotation
		player.transform.rotation = teleportPosition.rotation;

		// set global x and z rotation to zero in case the toilet is on a hillside
		player.transform.eulerAngles = new Vector3(0, player.transform.eulerAngles.y, 0);

		// reset velocities
		player.rb.velocity = Vector3.zero;

	}

	[DebugButton]
	void ToiletAddTool()
	{
		gameObject.AddComponent<ToiletAddTool>();
	}

#if UNITY_EDITOR
	private void OnDrawGizmosSelected()
	{
		Handles.Label(transform.position + Vector3.up * 4, "T " + toiletId);

	}
#endif
}