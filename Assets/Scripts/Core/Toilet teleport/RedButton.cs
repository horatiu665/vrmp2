﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using VRCore;
using Random = UnityEngine.Random;

public class RedButton : MonoBehaviour
{
    public float pressDelay = 0.3f;

    float lastPress;

    public UnityEvent OnPress;

    public IVRPlayer lastEventPlayerData;

    private void OnTriggerEnter(Collider other)
    {
        if (other.name.Contains("test"))
        {
            print("Trigger enter! on the button!");
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        // disallow pressing right after a press
        if (Time.time < lastPress + pressDelay)
        {
            return;
        }

        // check if a player is pressing the button
        var rb = collision.rigidbody;
        if (rb != null)
        {
            var eq = rb.GetComponent<IVREquippable>();
            if (eq != null)
            {
                if (eq.isEquipped)
                {
                    var player = eq.player;

                    // we know the player who is touching this
                    if (player == PlayerManager.GetLocalPlayer<IVRPlayer>())
                    {
                        // it is the local player!
                        PressHappened(player);
                        lastPress = Time.time;

                    }
                }
            }
        }
    }

    // player pressed the button.
    private void PressHappened(IVRPlayer player)
    {
        lastEventPlayerData = player;

        OnPress.Invoke();

        lastEventPlayerData = null;
    }

}