﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class ToiletAddTool : MonoBehaviour
{
    public ToiletTeleporterManager managerInScene;

    [DebugButton]
    void AddSelf()
    {
        if (ToiletTeleporterManager.instance == null || ToiletTeleporterManager.instance.chunks == null)
        {
            Debug.LogError("[ToiletAddTool] Could not add toilets because ToiletTeleporterManager.instance.chunks is null", gameObject);
            return;
        }

        managerInScene = ToiletTeleporterManager.instance;

        var tt = GetComponent<ToiletTeleporter>();
        if (tt == null)
        {
            Debug.LogError("[ToiletAddTool] Cannot add a non-toilet");
            return;
        }

        ToiletTeleporterManager.instance.chunks.AddToilet(tt);
        // child is a toilet. move it now.
        transform.SetParent(ToiletTeleporterManager.instance.chunks.transform);

        Debug.Log("Added toilet and deleted from the scene [click here for the toilet manager]", ToiletTeleporterManager.instance.chunks.gameObject);

        this.DestroySafe(gameObject);
    }

    [DebugButton]
    void FindManager()
    {
        if (ToiletTeleporterManager.instance == null)
        {
            Debug.LogError("[ToiletAddTool] Could not find ToiletTeleporterManager", gameObject);
            return;
        }

        managerInScene = ToiletTeleporterManager.instance;

    }

    [DebugButton]
    void FindManagerAndAddChildrenToilets()
    {
        if (ToiletTeleporterManager.instance == null || ToiletTeleporterManager.instance.chunks == null)
        {
            Debug.LogError("[ToiletAddTool] Could not add toilets because ToiletTeleporterManager.instance.chunks is null", gameObject);
            return;
        }

        managerInScene = ToiletTeleporterManager.instance;

        var children = transform.GetChildren();
        int count = 0;
        for (int i = 0; i < children.Count; i++)
        {
            var c = children[i];
            var child = c.GetComponent<ToiletTeleporter>();
            if (child != null)
            {
                ToiletTeleporterManager.instance.chunks.AddToilet(child);
                // child is a toilet. move it now.
                c.SetParent(ToiletTeleporterManager.instance.chunks.transform);
                count++;
            }
        }

        Debug.Log("Added " + count + " toilets to the manager. They are reparented under this object [click here]", ToiletTeleporterManager.instance.chunks.gameObject);
    }

}