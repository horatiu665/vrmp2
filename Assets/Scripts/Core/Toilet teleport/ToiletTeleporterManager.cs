﻿using Mountain;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class ToiletTeleporterManager : MonoBehaviour
{
    private static ToiletTeleporterManager _instance;
    public static ToiletTeleporterManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<ToiletTeleporterManager>();
            }
            if (_instance == null)
            {
                Debug.LogError("ToiletTeleporterManager could not be found! Cannot teleport into a toilet no matter how hard you try!");
            }
            return _instance;
        }
    }

    private ToiletTeleporterChunks _chunks;
    public ToiletTeleporterChunks chunks
    {
        get
        {
            if (_chunks == null)
            {
                _chunks = GetComponent<ToiletTeleporterChunks>();
            }
            return _chunks;
        }
    }

    public int unlimFrames = 2;

    public float distanceForClearFirst = 2400;

    public SmartSound teleportSound;

    private void Awake()
    {
        if (instance != null)
        {
            //  fuck fuckin
        }
    }

    public ChunkObjectToilet GetToiletFromId(int toiletId)
    {
        return chunks.GetToiletDataFromId(toiletId);
    }

    public void TeleportPlayerToToilet(IVRPlayer player, int toiletId)
    {
        if (teleportSound != null)
        {
            teleportSound.Play();
        }

        // first make a delay and make the avalanche graphics on the player
        // DELAY() + START_AVALANCHE_GRAPHICS()
        var delay = 0f;
        if (AvalancheMakler.instance != null)
        {
            AvalancheMakler.instance.InitAvalanche();
            delay = AvalancheMakler.instance.startDuration;
        }
        StartCoroutine(pTween.Wait(delay, () =>
        {
            // this is required right now because the execution is interrupted when regenerating.... so that should be fixed before this can be optimized.
            DeferredExecutionManager.instance.SetUnlimitedFrame(unlimFrames);

            var toilet = GetToiletFromId(toiletId);

            // put player at the toilet position in the current coord system
            var oldPP = player.position;
            player.position = OriginShiftManager.GlobalToLocalPos(toilet.spawnPos);

            player.rb.velocity = Vector3.zero;

            // if moving more than X, clear first.
            if ((player.position - oldPP).sqrMagnitude > distanceForClearFirst * distanceForClearFirst)
            {
                MountainManagerController.instance.ClearBeforeGeneratingNextAndMoveOrigin();
            }
            else
            {
                // do nothing, it will just generate at the regular pace.
            }

            // how long do we wait for it to generate???? fuckknoews. we just do 2s for fun!
            player.velocityController.forcedIsKinematic = SkiVelocityController.ForcedKinematicMode.ForcedOn;
            StartCoroutine(pTween.Wait(2f, () =>
            {
                player.velocityController.forcedIsKinematic = SkiVelocityController.ForcedKinematicMode.NotForced;
                if (AvalancheMakler.instance != null)
                {
                    AvalancheMakler.instance.EndAvalanche();
                }

            }));

        }));

        // when generated and/or after delay, remove avalanche graphics and allow player to move again (in toilet or under rainbow gate w/ old slalom velocity maybe)
        // STOP_AVALANCHE_GRAPHICS() + APPLY_PRE_TELEPORT_PLAYER_VELOCITY()

    }

    [DebugButton]
    public void PlayerWaitedEnough()
    {
        var player = PlayerManager.GetLocalPlayer<IVRPlayer>();
        player.rb.isKinematic = false;
    }

    public int toiletToGoTo = 0;

    [DebugButton]
    public void TestTeleport()
    {
        var player = PlayerManager.GetLocalPlayer<IVRPlayer>();
        if (player == null)
        {
            Debug.LogError("[ToiletManager] Could not find player");
        }
        TeleportPlayerToToilet(player, toiletToGoTo);
    }
}