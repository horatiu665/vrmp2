﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class ToiletTester : MonoBehaviour
{

    public int toiletToGoTo = 0;

    [DebugButton]
    public void TestTeleport()
    {
        ToiletTeleporterManager.instance.TeleportPlayerToToilet(PlayerManager.GetLocalPlayer<IVRPlayer>(), toiletToGoTo);
    }
}