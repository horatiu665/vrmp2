namespace Core
{
    using System;
    using Score;

    [Serializable]
    public class GameModeSetup
    {
        public GameMode gameMode;
        public ScoreType[] allowedScoreTypes = new ScoreType[1];
        public bool allowAll;
    }
}