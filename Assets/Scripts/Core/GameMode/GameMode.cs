namespace Core
{
    public enum GameMode : byte
    {
        // Basic modes
        Points = 0,
        Race,
        Slalom,
        Jump,

        // Advanced modes
        Joust,
    }
}