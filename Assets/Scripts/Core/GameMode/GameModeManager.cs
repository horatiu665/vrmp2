namespace Core
{
    using Score;
    using UnityEngine;
    using VRCore;

    public sealed class GameModeManager : SingletonMonoBehaviour<GameModeManager>
    {
        [SerializeField]
        private GameMode _currentMode = GameMode.Points;

        [SerializeField]
        private GameModeSetup[] _setup = new GameModeSetup[0];

        public GameMode currentMode
        {
            get { return _currentMode; }
        }

        public bool IsScoreAllowed(ScoreType type)
        {
            GameModeSetup setup = null;
            for (int i = 0; i < _setup.Length; i++)
            {
                if (_setup[i].gameMode == _currentMode)
                {
                    setup = _setup[i];
                    break;
                }
            }

            if (setup == null)
            {
                Debug.LogWarning(this.ToString() + " current game mode (" + _currentMode.ToString() + " has not been setup correctly or is missing setup.");
                return false;
            }

            if (setup.allowAll)
            {
                return true;
            }

            var scores = setup.allowedScoreTypes;
            for (int i = 0; i < scores.Length; i++)
            {
                if (scores[i] == type)
                {
                    return true;
                }
            }

            return false;
        }
    }
}