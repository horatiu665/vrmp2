namespace Core.Server
{
    using System;
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Networking;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Server;
    using VRUnicorns.SelfieStick;

    [Apex.ApexComponent("Network")]
    public sealed class ServerNetReceiver : ServerNetReceiverBase<ServerNetReceiver>
    {
        private NetStartPosition[] _startPositions;

        protected override void Awake()
        {
            _startPositions = FindObjectsOfType<NetStartPosition>();
            if (_startPositions.Length == 0)
            {
                Debug.LogError(this.ToString() + " cannot operate without at least one start position!");
                return;
            }

            base.Awake();
        }

        private IEnumerator Start()
        {
            yield return new WaitForSeconds(1f);

            var onScreen = OnScreenTextUIHandler.instance;
            if (onScreen != null)
            {
                var str = string.Empty;
                var ethernetIp = VRUtils.GetLocalIPv4(System.Net.NetworkInformation.NetworkInterfaceType.Ethernet);
                if (!string.IsNullOrEmpty(ethernetIp))
                {
                    str = "Ethernet IP: " + ethernetIp;
                }

                var wirelessIp = VRUtils.GetLocalIPv4(System.Net.NetworkInformation.NetworkInterfaceType.Wireless80211);
                if (!string.IsNullOrEmpty(wirelessIp))
                {
                    if (!string.IsNullOrEmpty(str))
                    {
                        str += "\n";
                    }

                    str += "Wireless IP: " + wirelessIp;
                }

                onScreen.fadeOut = false;
                onScreen.ShowText(str);
            }
        }

        public override void OnConnect(int connectionId, NetworkError error)
        {
            Debug.Log(this.ToString() + " OnConnect. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString());
        }

        public override void OnData(int connectionId, byte[] buffer, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                Debug.LogWarning(this.ToString() + " OnData from connection ID == " + connectionId.ToString() + ", buffer size == " + buffer.Length.ToString() + ", encountered NetworkError == " + error.ToString());
                return;
            }

            var messageType = buffer.PeekType();
            ////Debug.Log(this.ToString() + " OnData. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString() + ", message type == " + messageType.ToString() + ", buffer size == " + buffer.Length.ToString() + ", contents:\n" + buffer.DebugLogContents());

            switch (messageType)
            {
            case NetMessageType.LocalConnect:
                { HandlePlayerLocalConnect(connectionId, buffer); break; }
            case NetMessageType.VRBodyInput:
                { HandlePlayerVRBodyInput(connectionId, buffer); break; }
            case NetMessageType.SpawnRequest:
                { HandleSpawnRequest(connectionId, buffer); break; }
            case NetMessageType.SpawnEquipRequest:
                { HandleSpawnEquipRequest(connectionId, buffer); break; }
            case NetMessageType.Equip:
                { HandleEquip(connectionId, buffer); break; }
            case NetMessageType.Unequip:
                { HandleUnequip(connectionId, buffer); break; }
            case NetMessageType.PlayerScale:
                { HandlePlayerScale(connectionId, buffer); break; }
            case NetMessageType.ChangePreview:
                { HandleChangePreview(connectionId, buffer); break; }
            case NetMessageType.ApplyForceToPlayer:
                { HandleApplyForceToPlayer(connectionId, buffer); break; }
            case NetMessageType.SelfieStickStartRecording:
                { HandleSelfieStickStartRecording(connectionId, buffer); break; }
            case NetMessageType.SelfieStickDeleteRecording:
                { HandleSelfieStickDeleteRecording(connectionId, buffer); break; }
            case NetMessageType.PlayerStorage:
                { HandlePlayerStorage(connectionId, buffer); break; }
            case NetMessageType.PlayerOriginShift:
                { HandlePlayerOriginShift(connectionId, buffer); break; }
            case NetMessageType.GhettoTimeSyncClient:
                { HandleGhettoTimeSync(buffer); break; }
            case NetMessageType.HighscorePole:
                { HandleHighscorePole(connectionId, buffer); break; }
            default:
                {
                    Debug.LogError(this.ToString() + " OnData unhandled MessageType == " + messageType.ToString());
                    break;
                }
            }
        }

        private void HandleHighscorePole(int connectionId, byte[] buffer)
        {
            IPlayer player = GetPlayer(connectionId) as IPlayer; // cause it's local-to-server

            if (player == null)
            {
                return;
            }

            var msg = MessagePool.Get<HighscorePoleMessage>(buffer);

            // create score data and fire score event!
            var scoreEvent = ScorePointsManager.instance.AddScore(player, msg.scoreType, msg.score);

            // create highscore pole
            HighscoreMarker.instance.CreateHighscorePole(ServerPos(player.netId, msg.position), msg.score, msg.scoreType, player, scoreEvent.isGlobalHighscore);

            ServerNetSender.instance.SendToAll(msg, QosType.Reliable, player.netId);

            MessagePool.Return(msg);
        }

        private void HandleGhettoTimeSync(byte[] buffer)
        {
            // GhettoTimeSyncClientMessage as in: message FROM CLIENT
            var msg = MessagePool.Get<GhettoTimeSyncClientMessage>(buffer);

            GhettoTimeSync.instance.HandleServerTimeSync(msg.netId, msg.clientTime);

            MessagePool.Return(msg);
        }

        public override void OnDisconnect(int connectionId, NetworkError error)
        {
            var player = GetPlayer<IPlayer>(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " OnDisconnect received disconnect from unknown player with connection ID == " + connectionId.ToString());
                return;
            }

            Debug.Log(this.ToString() + " OnDisconnect. Removing player (" + player.name + ") by net id == " + player.netId.ToString() + ". Connection  Id == " + connectionId.ToString() + ", error == " + error.ToString());

            var onScreenText = OnScreenTextUIHandler.instance;
            if (onScreenText != null)
            {
                onScreenText.ShowText("Player (" + player.name + ") disconnected.", player.color);
            }

            var netId = player.netId;

            NetOriginShiftManager.instance.RemovePlayer(netId);

            // Remove disconnected players - before sending out message, to avoid sending to the leaver
            RemovePlayer(player);
            PlayerTypeManager.instance.Return(player);

            // inform all other clients of the leaver, except the one actually leaving (since he is already disconnected)
            var msg = MessagePool.Get<PlayerDisconnectMessageDefault>(netId);
            _network.SendToAll(msg, QosType.Reliable);
            MessagePool.Return(msg);
        }

        // origin shifted incoming position pos
        private Vector3 ServerPos(short localPlayerSenderId, Vector3 position)
        {
            return NetOriginShiftManager.ServerPos(localPlayerSenderId, position);
        }

        private void HandlePlayerLocalConnect(int connectionId, byte[] buffer)
        {
            var localConnectMsg = MessagePool.Get<PlayerLocalConnectMessage>(buffer);
            var name = localConnectMsg.name;
            var playerType = localConnectMsg.playerType;
            MessagePool.Return(localConnectMsg);

            var player = GetPlayer<IPlayer>(connectionId);
            if (player != null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerName another player is already registered with connection id == " + connectionId.ToString() + ", existing player name == " + player.name + ", new player's desired name == " + name);
                return;
            }

            if (string.IsNullOrEmpty(name))
            {
                name = "Random Name"; // TODO: what to do about players with no name? (can it even happen?)
            }

            var rbManager = ServerRigidbodySyncManager.instance as ServerRigidbodySyncManager;

            var netId = GetNextPlayerId();
            var headSyncId = rbManager != null ? rbManager.GetNextSyncId() : -1;
            var bodySyncId = rbManager != null ? rbManager.GetNextSyncId() : -1;
            var color = PlayerColorChooserManager.GetPlayerColor();

            // assume player's origin shift is the startingPos
            NetOriginShiftManager.instance.SetPlayerOriginShift(netId, OriginShiftManager.instance.startingPosition);

            // convert from server pos to player starting pos in his local coords.
            var posInServerCoords = ServerPos(netId, _startPositions[netId % _startPositions.Length].transform.position);

            // send a special 'start message' to the new player so that the local player prefab can be set up. also tells it about the server origin shift
            var startMsg = MessagePool.Get<PlayerLocalStartMessage>(netId);
            startMsg.position = posInServerCoords;
            startMsg.color = color;
            startMsg.playerType = playerType;
            startMsg.headRbSyncId = headSyncId;
            startMsg.bodyRbSyncId = bodySyncId;
            startMsg.serverOriginShift = OriginShiftManager.originShift;

            _network.Send(connectionId, startMsg, QosType.ReliableSequenced);
            MessagePool.Return(startMsg);

            // a new client has connected - inform all other clients of the new player. and don't send this to the new player.
            var connectMsg = MessagePool.Get<PlayerRemoteConnectMessage>(netId);
            connectMsg.position = posInServerCoords;
            connectMsg.originShift = NetOriginShiftManager.instance.GetPlayerOriginShift(netId);
            connectMsg.color = color;
            connectMsg.name = name;
            connectMsg.playerType = playerType;
            connectMsg.headRbSyncId = headSyncId;
            connectMsg.bodyRbSyncId = bodySyncId;

            _network.SendToAll(connectMsg, QosType.Reliable, netId);

            // Inform the new player of all the existing client players (if there are any).
            if (_players.Count > 0)
            {
                var enumerator = _players.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext()) // AddPlayer() adds the player at the end of the function, so don't worry.
                    {
                        // send to the new connecting player, but send a connect message containing details of all the other existing clients
                        var p = (IPlayer)enumerator.Current.Value;
                        connectMsg.netId = p.netId;
                        connectMsg.position = p.position; // position on the server. the client receiver will handle the trasformation using RemotePos(), though it needs the server OS which is why the start message is reliable sequenced.
                        connectMsg.originShift = NetOriginShiftManager.instance.GetPlayerOriginShift(p.netId);
                        connectMsg.color = p.color;
                        connectMsg.name = p.name;
                        connectMsg.playerType = p.playerType;
                        if (p.headSync != null)
                        {
                            connectMsg.headRbSyncId = p.headSync.syncId;
                            connectMsg.bodyRbSyncId = p.bodySync.syncId;
                        }

                        _network.Send(connectionId, connectMsg, QosType.Reliable);
                    }
                }
                finally
                {
                    enumerator.Dispose();
                }
            }

            MessagePool.Return(connectMsg);

            // inform joining player of all existing AIs
            // DELETED MOTHAFUNKSER

            // Inform joining player of synchronized rigidbody positions and rotations
            if (rbManager != null)
            {
                rbManager.HandleNewPlayer(connectionId);
            }

            // Inform joining player of synchronized equippables
            var eqManager = EquippableSyncManager.instance;
            if (eqManager != null)
            {
                eqManager.HandleNewPlayer(connectionId);
            }

            // actually create and add the new player
            var newPlayer = PlayerTypeManager.instance.InstantiatePlayer<IPlayer>(playerType, GameType.Server, posInServerCoords);
            newPlayer.color = color;
            newPlayer.name = name;

            AddPlayer(newPlayer, connectionId, netId);

            if (rbManager != null)
            {
                newPlayer.SetupRigidbodies(headSyncId, bodySyncId);
            }

            Debug.Log(this.ToString() + " HandlePlayerName() - added new player by net id == " + netId.ToString() + " and name == " + name + " for connection id == " + connectionId.ToString());
        }

        private void HandlePlayerVRBodyInput(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerVRBodyInput() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            // get the incoming message and update the head and hands of the correct player
            var msg = MessagePool.Get<VRBodyInputMessage>(buffer);
            var data = new VRBodyUpdateData()
            {
                position = ServerPos(player.netId, msg.position),

                headPosition = msg.headPosition,
                headRotation = msg.headRotation,

                leftHandPosition = msg.leftHandPosition,
                leftHandRotation = msg.leftHandRotation,

                rightHandPosition = msg.rightHandPosition,
                rightHandRotation = msg.rightHandRotation
            };

            player.HandleVRBodyUpdate(data);
            MessagePool.Return(msg);
        }

        private void HandleSpawnRequest(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawn() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var requestMsg = MessagePool.Get<SpawnMessageRequest>(buffer);
            var hand = requestMsg.isLeft ? player.leftHand : player.rightHand;
            SpawnManagerPreviewable.instance.Spawn<INetSpawnable>(hand, requestMsg.prefabType);

            var responseMsg = MessagePool.Get<SpawnMessageResponse>();
            responseMsg.playerNetId = player.netId;
            responseMsg.isLeft = requestMsg.isLeft;
            responseMsg.prefabType = requestMsg.prefabType;
            _network.SendToAll(responseMsg, QosType.ReliableSequenced);

            //Debug.Log(this.ToString() + " HandleSpawn() spawned new prefab of type == " + prefab.prefabType.ToString());
            MessagePool.Return(requestMsg);
            MessagePool.Return(responseMsg);
        }

        private void HandleSpawnEquipRequest(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var requestMsg = MessagePool.Get<SpawnEquipMessageRequest>(buffer);
            var hand = requestMsg.isLeft ? player.leftHand : player.rightHand;
            var prefab = SpawnManagerPreviewable.instance.Spawn<INetSpawnableEquippable>(hand, requestMsg.prefabType);
            if (prefab == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip() spawned prefab for player == " + player.ToString() + ", is null. Prefab type == " + requestMsg.prefabType.ToString());
                MessagePool.Return(requestMsg);
                return;
            }

            if (hand.Equip(prefab) == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip() failed to equip == " + prefab.ToString() + " on player == " + player.ToString());
                VRPrefabManager.instance.Return(prefab);
                MessagePool.Return(requestMsg);
                return;
            }

            if (prefab.netId <= 0)
            {
                var serverNetId = GetNextEntityId();
                AddNetEntity(prefab, serverNetId);
            }
            else
            {
                // in case the prefab was removed upon reentering the pool or some shit.
                AddNetEntity(prefab, prefab.netId);
            }

            var responseMsg = MessagePool.Get<SpawnEquipMessageResponse>();
            responseMsg.playerNetId = player.netId;
            responseMsg.isLeft = hand.isLeft;
            responseMsg.prefabType = prefab.prefabType;
            responseMsg.equippableNetId = prefab.netId;
            _network.SendToAll(responseMsg, QosType.ReliableSequenced);

            //Debug.Log(this.ToString() + " HandleSpawnEquip() spawned prefab == " + prefab.ToString() + " and equipped to player == " + player.ToString());
            MessagePool.Return(requestMsg);
            MessagePool.Return(responseMsg);
        }

        private void HandleEquip(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<EquipMessage>(buffer);
            var equippable = GetEntity<INetEquippable>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() player == " + player.ToString() + ", cannot equip unknown equippable by id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            if (hand.Equip(equippable) == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() failed to equip == " + equippable.ToString() + " on player == " + player.ToString());
                MessagePool.Return(msg);
                return;
            }

            msg.netId = player.netId;
            _network.SendToAll(msg, QosType.ReliableSequenced, player.netId);

            //Debug.Log(this.ToString() + " HandleEquip() player == " + player.ToString() + ", equipped == " + equippable.ToString());
            MessagePool.Return(msg);
        }

        private void HandleUnequip(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleUnequip() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<UnequipMessage>(buffer);
            var equipped = GetEntity<INetEquippable>(msg.netEquippableId);
            if (equipped == null)
            {
                Debug.LogWarning(this.ToString() + " HandleUnequip() player == " + player.ToString() + ", cannot unequip unknown equippable by id == " + msg.netEquippableId.ToString());
                MessagePool.Return(msg);
                return;
            }

            equipped.position = ServerPos(player.netId, msg.position);
            equipped.rotation = msg.rotation;

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            hand.Unequip(equipped, msg.velocity, msg.angularVelocity);

            msg.netId = player.netId;
            _network.SendToAll(msg, QosType.ReliableSequenced, player.netId);

            //Debug.Log(this.ToString() + " HandleUnequip() player == " + player.ToString());
            MessagePool.Return(msg);
        }

        private void HandlePlayerScale(int connectionId, byte[] buffer)
        {
            var player = GetPlayer<IPlayer>(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerScale() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<PlayerScaleMessage>(buffer);
            msg.netId = player.netId;

            // set player scale
            player.UpdateScale(msg.localScale);
            _network.SendToAll(msg, QosType.UnreliableSequenced, player.netId);
            MessagePool.Return(msg);
        }

        private void HandlePlayerOriginShift(int connectionId, byte[] buffer)
        {
            var player = GetPlayer<IPlayer>(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerOriginShift() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<PlayerOriginShiftMessage>(buffer);
            msg.netId = player.netId; // message from player.netId about his local OS delta

            // we now know this player's origin shift data. save it on server and...
            NetOriginShiftManager.instance.SetPlayerOriginShift(player.netId, msg.originShift);

            // ... pass message on to the other clients
            _network.SendToAll(msg, QosType.ReliableSequenced, player.netId);

            MessagePool.Return(msg);
        }

        private void HandleChangePreview(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleToolPreviewEnter() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<ChangePreviewMessage>(buffer);
            msg.netId = player.netId;

            // execute function on server
            player.OnChangePreview(msg.isLeft, msg.oldPrefabType, msg.prefabType);

            // delegate to the other clients
            _network.SendToAll(msg, QosType.ReliableSequenced, player.netId);
            MessagePool.Return(msg);
        }

        private void HandleApplyForceToPlayer(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleApplyForceToPlayer() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<ApplyForceToPlayerMessage>(buffer);
            // this is the id of the player that should get hit
            var playerVictim = GetPlayer(GetConnectionId(msg.netId));
            if (playerVictim != null)
            {
                // apply force to the victim (not on the server, it would do nothing. it has to be done on the playerVictim)
                // tell victim to get hit
                _network.Send(playerVictim.netId, msg, QosType.ReliableSequenced);
            }

            MessagePool.Return(msg);
        }

        private void HandleSelfieStickStartRecording(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSelfieStickStartRecording could not find player by connection ID == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<SelfieStickStartRecordMessage>(buffer);
            var equippable = GetEntity<SelfieStick>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSelfieStickStartRecording could not find equippable selfie stick from id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            _network.SendToAll(msg, QosType.Reliable, player.netId);
            MessagePool.Return(msg);
        }

        private void HandleSelfieStickDeleteRecording(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSelfieStickDeleteRecording could not find player by connection ID == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<SelfieStickStartRecordMessage>(buffer);
            var equippable = GetEntity<SelfieStick>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSelfieStickDeleteRecording could not find equippable selfie stick from id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            _network.SendToAll(msg, QosType.Reliable, player.netId);
            MessagePool.Return(msg);
        }

        private void HandlePlayerStorage(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerStorage could not find player with connection ID == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<PlayerStoreEquippableMessage>(buffer);
            var equippable = GetEntity<INetEquippable>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerStorage could not find equippable from id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            // TODO: possibly need to send different message type so that special visuals can be played
            NetEntityDestructionManager.instance.DestroyNetEntity(equippable);
            MessagePool.Return(msg);
        }

        public INetEntity SpawnNetEntity(VRPrefabType type, Vector3 pos)
        {
            var netEntity = VRPrefabManager.instance.Spawn<INetEntity>(type, pos, transform.rotation);
            AddNetEntity(netEntity, GetNextEntityId());

            // tell everybody else to also spawn explosion
            var msg = MessagePool.Get<SpawnEquippableMessage>();
            msg.position = pos;
            msg.prefabType = type;
            msg.equippableNetId = netEntity.netId;

            _network.SendToAll(msg, QosType.Reliable);
            MessagePool.Return(msg);

            return netEntity;
        }

    }
}