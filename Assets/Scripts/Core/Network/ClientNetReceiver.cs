namespace Core.Client
{
    using System;
    using Steamworks;
    using UnityEngine;
    using UnityEngine.Networking;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Client;
    using VRUnicorns.SelfieStick;

    [Apex.ApexComponent("Network")]
    public sealed class ClientNetReceiver : ClientNetReceiverBase<ClientNetReceiver>
    {
        private string GetPlayerName()
        {
            return SteamManager.Initialized ? SteamFriends.GetPersonaName() : System.Environment.MachineName;
        }

        public override void OnConnect(int connectionId, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                // an error occured !
                Debug.LogError(this.ToString() + " OnConnect could not establish a connection to server, error == " + error.ToString());
                return;
            }

            // if reconnect, send a reconnect message with the previous netId so the server reestablishes the same player as this client
            // {}
            // else {

            // Connection to server established, send a name message
            var nameMessage = MessagePool.Get<PlayerLocalConnectMessage>();
            nameMessage.name = GetPlayerName();
            var pt = PlayerTypeManager.instance != null ? PlayerTypeManager.instance.currentGamePlayerType : PlayerType.Normal;
            nameMessage.playerType = pt;// PlayerType.Normal; // TODO: get player type choice?
            _network.Send(nameMessage, QosType.Reliable);
            MessagePool.Return(nameMessage);

            // }

            Debug.Log(this.ToString() + " OnConnect. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString());
        }

        public override void OnData(int connectionId, byte[] buffer, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                Debug.LogWarning(this.ToString() + " OnData from connection ID == " + connectionId.ToString() + ", buffer size == " + buffer.Length.ToString() + ", encountered NetworkError == " + error.ToString());
                return;
            }

            var messageType = buffer.PeekType();
            ////Debug.Log(this.ToString() + " OnData. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString() + ", message type == " + messageType.ToString() + ", buffer size == " + buffer.Length.ToString() + ", contents=\n" + buffer.DebugLogContents());
            switch (messageType)
            {
            case NetMessageType.LocalStart: { HandlePlayerLocalStart(buffer); break; }
            case NetMessageType.RemoteConnect: { HandlePlayerRemoteConnect(buffer); break; }
            case NetMessageType.Disconnect: { HandlePlayerDisconnect(buffer); break; }
            case NetMessageType.VRBodyUpdate: { HandleVRBodyUpdate(buffer); break; }
            case NetMessageType.SpawnResponse: { HandleSpawn(buffer); break; }
            case NetMessageType.SpawnEquipResponse: { HandleSpawnEquip(buffer); break; }
            case NetMessageType.Equip: { HandleEquip(buffer); break; }
            case NetMessageType.Unequip: { HandleUnequip(buffer); break; }
            case NetMessageType.PlayerScale: { HandlePlayerScale(buffer); break; }
            case NetMessageType.PlayerOriginShift: { HandlePlayerOriginShift(buffer); break; }
            case NetMessageType.ChangePreview: { HandleChangePreview(buffer); break; }
            case NetMessageType.ApplyForceToPlayer: { HandleApplyForceToPlayer(buffer); break; }
            case NetMessageType.RigidbodySyncSpawn: { HandleRigidbodySyncSpawn(buffer); break; }
            case NetMessageType.RigidbodySyncUpdate: { HandleRigidbodySyncUpdate(buffer); break; }
            case NetMessageType.EquippableSyncUpdate: { HandleEquippableSyncUpdate(buffer); break; }
            case NetMessageType.Impalement: { HandleImpalement(buffer); break; }
            case NetMessageType.Unimpalement: { HandleUnimpalement(buffer); break; }
            case NetMessageType.SelfieStickStartRecording: { HandleSelfieStickStartRecording(buffer); break; }
            case NetMessageType.SelfieStickDeleteRecording: { HandleSelfieStickDeleteRecording(buffer); break; }
            case NetMessageType.DestroyNetEntity: { HandleDestroyNetEntity(buffer); break; }
            case NetMessageType.SpawnExplosion: { HandleSpawnExplosion(buffer); break; }
            case NetMessageType.SpawnEquippable: { HandleSpawnEquippable(buffer); break; }
            case NetMessageType.GhettoTimeSyncServer: { HandleGhettoTimeSync(buffer); break; }
            case NetMessageType.HighscorePole: { HandleHighscorePole(buffer); break; }
            default:
                { Debug.LogError(this.ToString() + " OnData unhandled MessageType == " + messageType.ToString()); break; }
            }
        }

        public override void OnDisconnect(int connectionId, NetworkError error)
        {
            Debug.Log(this.ToString() + " OnDisconnect. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString());

            var onScreenText = OnScreenTextUIHandler.instance;
            if (onScreenText != null)
            {
                onScreenText.ShowText("Disconnected from Network. Error == " + error.ToString(), Color.red);
            }

            // FEEDBACK FOR DISCOMNECT
            // try to reconnect here...? in case unity does not try
        }

        private void HandleHighscorePole(byte[] buffer)
        {
            var msg = MessagePool.Get<HighscorePoleMessage>(buffer);
            var player = GetPlayer(msg.netId) as IPlayer;
            if (player != null)
            {
                // create score data and fire score event!
                var scoreEventData = ScorePointsManager.instance.AddScore(player, msg.scoreType, msg.score);

                HighscoreMarker.instance.CreateHighscorePole(RemotePos(msg.position), msg.score, msg.scoreType, player, scoreEventData.isGlobalHighscore);
            }
            else
            {
                Debug.LogWarning(this.ToString() + " HandleHighscorePole() could not find player by netId == " + msg.netId.ToString());
            }
            
            MessagePool.Return(msg);
        }

        private void HandleGhettoTimeSync(byte[] buffer)
        {
            // GhettoTimeSyncServerMessage as in: message FROM SERVER
            var msg = MessagePool.Get<GhettoTimeSyncServerMessage>(buffer);

            GhettoTimeSync.instance.HandleClientTimeSync(msg.clientTime, msg.serverTime);

            MessagePool.Return(msg);
        }

        // position is ServerPos. According to the transfer formulas, it should be interpreted: serverPos + serverOS - remoteOS (local player is considered remote, compared to server... yeah naming is weird)
        private Vector3 RemotePos(Vector3 position)
        {
            return NetOriginShiftManager.RemotePos(position);
        }

        private void HandleSpawnEquippable(byte[] buffer)
        {
            var msg = MessagePool.Get<SpawnEquippableMessage>(buffer);

            // spawn explosion of type msg.type at position
            var pos = RemotePos(msg.position);
            var prefab = VRPrefabManager.instance.Spawn(msg.prefabType, pos, Quaternion.identity);

            // if prefab is equippable, give it id. but if it is not, it's just spawned so this can work for nonequippables
            if (prefab is INetEquippable)
            {
                AddNetEntity((INetEquippable)prefab, msg.equippableNetId);
            }

            MessagePool.Return(msg);
        }

        private void HandleSpawnExplosion(byte[] buffer)
        {
            var msg = MessagePool.Get<SpawnExplosionMessage>(buffer);
            // spawn explosion of type msg.type at position
            var pos = RemotePos(msg.position);
            VRPrefabManager.instance.Spawn(msg.prefabType, pos, Quaternion.identity);
            MessagePool.Return(msg);
        }

        private void HandleDestroyNetEntity(byte[] buffer)
        {
            var msg = MessagePool.Get<DestroyNetEntityMessage>(buffer);
            var netEntity = GetEntity(msg.netEntityId);
            if (netEntity == null)
            {
                Debug.LogWarning(this.ToString() + " HandleDestroyEquippable could not find net entity from id == " + msg.netEntityId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var equippable = netEntities as INetEquippable;
            if (equippable != null && equippable.isEquipped)
            {
                // make sure that it is not equipped when returned to the pool
                equippable.hand.Unequip(equippable, Vector3.zero, Vector3.zero);
            }

            RemoveNetEntity(netEntity);
            //netEntity.SetNetId(0);
            VRPrefabManager.instance.Return(netEntity);
            MessagePool.Return(msg);
        }

        private void HandleSelfieStickStartRecording(byte[] buffer)
        {
            var msg = MessagePool.Get<SelfieStickStartRecordMessage>(buffer);
            var equippable = GetEntity<SelfieStick>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSelfieStickStartRecording could not find equippable selfie stick from id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            equippable.StartRecording();
            MessagePool.Return(msg);
        }

        private void HandleSelfieStickDeleteRecording(byte[] buffer)
        {
            var msg = MessagePool.Get<SelfieStickStartRecordMessage>(buffer);
            var equippable = GetEntity<SelfieStick>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSelfieStickDeleteRecording could not find equippable selfie stick from id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            equippable.DeleteRecording();
            MessagePool.Return(msg);
        }

        private void HandlePlayerScale(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerScaleMessage>(buffer);

            var player = GetPlayer<IPlayer>(msg.netId);
            if (player != null)
            {
                // set player scale
                player.UpdateScale(msg.localScale);
            }
            else
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerScale() could not find player by netId == " + msg.netId.ToString());
            }

            MessagePool.Return(msg);
        }

        private void HandlePlayerOriginShift(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerOriginShiftMessage>(buffer);

            if (msg.netId == -2)
            {
                // message from server about server OS delta.
                NetOriginShiftManager.instance.SetServerOriginShift(msg.originShift);
            }
            else
            {
                // message from server about other player's OS delta.
                var player = GetPlayer<IPlayer>(msg.netId);
                if (player != null)
                {
                    NetOriginShiftManager.instance.SetPlayerOriginShift(player.netId, msg.originShift);
                    player.HandleOriginShift(msg.originShift);
                }
                else
                {
                    Debug.LogWarning(this.ToString() + " HandlePlayerOriginShift() could not find player by netId == " + msg.netId.ToString());
                }

            }

            MessagePool.Return(msg);
        }

        private void HandlePlayerLocalStart(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerLocalStartMessage>(buffer);
            var id = msg.netId;
            if (GetPlayer(id) != null)
            {
                Debug.LogError(this.ToString() + " HandlePlayerStart another player is already registered with net id == " + id.ToString());
                MessagePool.Return(msg);
                return;
            }

            var serverOS = msg.serverOriginShift;
            NetOriginShiftManager.instance.SetServerOriginShift(serverOS);

            // msg.position is serverpos. local player must know about serverOS before it can perform RemotePos
            var pos = RemotePos(msg.position);
            var color = msg.color;
            var playerType = msg.playerType;

            // do not use a pool for the local player, since there will only ever be one of those
            var localPlayer = PlayerTypeManager.instance.InstantiatePlayer<IPlayer>(playerType, GameType.Local, pos);
            localPlayer.SetIsLocal();
            localPlayer.color = color;
            localPlayer.name = GetPlayerName();

            AddPlayer(localPlayer, id);

            if (msg.headRbSyncId >= 0 && msg.bodyRbSyncId >= 0)
            {
                localPlayer.SetupRigidbodies(msg.headRbSyncId, msg.bodyRbSyncId);
            }

            Debug.Log(this.ToString() + " HandlePlayerStart created local player by id == " + localPlayer.netId.ToString() + " and name == " + localPlayer.name);
            MessagePool.Return(msg);

            if (ConnectUIHandler.instance != null)
            {
                ConnectUIHandler.instance.Disable();
            }
        }

        private void HandlePlayerRemoteConnect(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerRemoteConnectMessage>(buffer);
            var id = msg.netId;
            if (GetPlayer(id) != null)
            {
                Debug.LogError(this.ToString() + " HandlePlayerConnect another player is already registered with net id == " + id.ToString());
                MessagePool.Return(msg);
                return;
            }

            ////Debug.Log(this.ToString() + " HandlePlayerConnect create remote player by id == " + id.ToString() + ", name == " + msg.name.ToString() + ", at position == " + msg.position.ToString());

            var os = msg.originShift;

            // remote connect = a new player connected. his origin shift is startingPos on his start.
            NetOriginShiftManager.instance.SetPlayerOriginShift(id, os);

            var pos = RemotePos(msg.position);
            var color = msg.color;
            var name = msg.name;

            // use the pool for remotes, they are all the same
            var player = PlayerTypeManager.instance.InstantiatePlayer<IPlayer>(msg.playerType, GameType.Remote, pos);
            player.color = color;
            player.name = name;

            AddPlayer(player, id);

            if (msg.headRbSyncId >= 0 && msg.bodyRbSyncId >= 0)
            {
                player.SetupRigidbodies(msg.headRbSyncId, msg.bodyRbSyncId);
            }

            Debug.Log(this.ToString() + " HandlePlayerConnect created remote player by id == " + player.netId.ToString() + " and name == " + player.name);
            MessagePool.Return(msg);
        }

        private void HandlePlayerDisconnect(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerDisconnectMessageDefault>(buffer);
            var player = GetPlayer<IPlayer>(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandlePlayerDisconnect received disconnect message for unknown player with net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            Debug.Log(this.ToString() + " HandlePlayerDisconnect for player by netId == " + msg.netId.ToString() + ", player name == " + player.name.ToString());

            NetOriginShiftManager.instance.RemovePlayer(msg.netId);

            if (!RemovePlayer(msg.netId))
            {
                Debug.LogWarning(this.ToString() + " found player by id == " + msg.netId.ToString() + ", removal was unsuccessful");
            }

            PlayerTypeManager.instance.Return(player);
            MessagePool.Return(msg);
        }

        private void HandleVRBodyUpdate(byte[] buffer)
        {
            var msg = MessagePool.Get<VRBodyUpdateMessage>(buffer);
            var player = GetPlayer<IPlayer>(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleVRBodyUpdate no player found through net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            if (player.isLocal)
            {
                Debug.LogWarning(this.ToString() + " HandleVRBodyUpdate player by net id == " + msg.netId.ToString() + " is a local player, and thus cannot be updated");
                MessagePool.Return(msg);
                return;
            }

            var data = new VRBodyUpdateData()
            {
                position = RemotePos(msg.position),

                headPosition = msg.headPosition,
                headRotation = msg.headRotation,

                leftHandPosition = msg.leftHandPosition,
                leftHandRotation = msg.leftHandRotation,

                rightHandPosition = msg.rightHandPosition,
                rightHandRotation = msg.rightHandRotation
            };

            player.HandleVRBodyUpdate(data);
            //Debug.Log(this.ToString() + " HandleVRBodyUpdate for player by net id == " + player.netId.ToString());
            MessagePool.Return(msg);
        }

        private void HandleSpawn(byte[] buffer)
        {
            var msg = MessagePool.Get<SpawnMessageResponse>(buffer);
            var player = GetPlayer(msg.playerNetId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleSpawn() no player found by net id == " + msg.playerNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            SpawnManagerPreviewable.instance.Spawn<INetSpawnable>(hand, msg.prefabType);
            MessagePool.Return(msg);
        }

        private void HandleSpawnEquip(byte[] buffer)
        {
            var msg = MessagePool.Get<SpawnEquipMessageResponse>(buffer);
            var player = GetPlayer(msg.playerNetId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleSpawnEquip() no player found by net id == " + msg.playerNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            var prefab = SpawnManagerPreviewable.instance.Spawn<INetSpawnableEquippable>(hand, msg.prefabType);
            if (prefab == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip received spawn equip message for non-INetSpawnableEquippable prefab of type == " + msg.prefabType.ToString());
                MessagePool.Return(msg);
                return;
            }

            AddNetEntity(prefab, msg.equippableNetId);
            hand.Equip(prefab);

            //Debug.Log(this.ToString() + " HandleSpawnEquip() spawned prefab == " + prefab.ToString() + ", and equipped on player == " + player.ToString());
            MessagePool.Return(msg);
        }

        private void HandleEquip(byte[] buffer)
        {
            var msg = MessagePool.Get<EquipMessage>(buffer);
            var player = GetPlayer(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleEquip no player found by net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var equippable = GetEntity<INetEquippable>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() player == " + player.ToString() + ", cannot equip unknown equippable by id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            hand.Equip(equippable);
            //Debug.Log(this.ToString() + " HandleEquip for net id == " + msg.netId.ToString() + ", type == " + msg.equippable + ", is left == " + msg.isLeft.ToString());
            MessagePool.Return(msg);
        }

        private void HandleUnequip(byte[] buffer)
        {
            var msg = MessagePool.Get<UnequipMessage>(buffer);
            var player = GetPlayer(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleUnequip no player found through net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var equipped = GetEntity<INetEquippable>(msg.netEquippableId);
            if (equipped == null)
            {
                Debug.LogWarning(this.ToString() + " HandleUnequip() player == " + player.ToString() + ", cannot unequip unknown equippable by id == " + msg.netEquippableId.ToString());
                MessagePool.Return(msg);
                return;
            }

            equipped.position = RemotePos(msg.position);
            equipped.rotation = msg.rotation;
            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            hand.Unequip(equipped, msg.velocity, msg.angularVelocity);

            //Debug.Log(this.ToString() + " HandleUnequip for player by net id == " + msg.netId.ToString() + ", is left == " + msg.isLeft.ToString());
            MessagePool.Return(msg);
        }

        private void HandleChangePreview(byte[] buffer)
        {
            var msg = MessagePool.Get<ChangePreviewMessage>(buffer);
            var player = GetPlayer(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleChangePreview() no player found through net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            player.OnChangePreview(msg.isLeft, msg.oldPrefabType, msg.prefabType);
            MessagePool.Return(msg);
        }

        private void HandleApplyForceToPlayer(byte[] buffer)
        {
            var msg = MessagePool.Get<ApplyForceToPlayerMessage>(buffer);
            var playerVictim = GetPlayer(msg.netId);

            if (playerVictim == null)
            {
                Debug.LogWarning(this.ToString() + " HandleApplyForceToPlayer() could not find player by net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            if (!playerVictim.isLocal)
            {
                MessagePool.Return(msg);
                return;
            }

            // tell victim to get hit
            ((IPlayer)playerVictim).AddForce(this, msg.force);

            MessagePool.Return(msg);
        }

        private void HandleEquippableSyncUpdate(byte[] buffer)
        {
            var msg = MessagePool.Get<EquippableSyncMessage>(buffer);
            var initial = msg.initial;
            var data = msg.data;
            var count = data.Count;
            for (int i = 0; i < count; i++)
            {
                var d = data[i];

                // server to client (origin shift) position tweak.
                d.position = RemotePos(d.position);

                var equippable = GetEntity(d.netId);
                if (equippable == null)
                {
                    equippable = NewEquippableSync(d);
                }

                var sync = equippable.GetComponent<EquippableSyncComponent>();
                if (sync == null)
                {
                    Debug.LogWarning(this.ToString() + " equippable (" + equippable.ToString() + ") is missing an EquippableSyncComponent!");
                    continue;
                }

                sync.HandleUpdate(d, initial);
            }

            MessagePool.Return(msg);
        }

        // position already updated to RemotePos(d)
        private INetEntity NewEquippableSync(EquippableSyncData data)
        {
            var prefab = VRPrefabManager.instance.Spawn<INetEntity>(data.prefabType, data.position, data.rotation);
            if (prefab == null)
            {
                Debug.LogError(this.ToString() + " NewEquippableSync missing prefab setup for prefab of type == " + data.prefabType.ToString());
                return null;
            }

            AddNetEntity(prefab, data.netId);
            return prefab;
        }

        // only used by debug shit
        private void HandleRigidbodySyncSpawn(byte[] buffer)
        {
            var msg = MessagePool.Get<RigidbodySyncSpawnMessage>(buffer);
            var manager = (ClientRigidbodySyncManager)ClientRigidbodySyncManager.instance;
            if (manager.Has(msg.data.syncId))
            {
                Debug.LogWarning(this.ToString() + " already has a synced rigidbody by id == " + msg.data.syncId.ToString() + ", aborting creation of a new one");
                MessagePool.Return(msg);
                return;
            }

            var data = msg.data;
            data.position = RemotePos(data.position);
            NewRigidbodySync(data);
            MessagePool.Return(msg);
        }

        private void HandleRigidbodySyncUpdate(byte[] buffer)
        {
            var msg = MessagePool.Get<RigidbodySyncUpdateMessage>(buffer);
            var manager = (ClientRigidbodySyncManager)ClientRigidbodySyncManager.instance;
            var initial = msg.initial;
            var data = msg.data;
            var count = data.Count;
            for (int i = 0; i < count; i++)
            {
                var d = data[i];
                d.position = RemotePos(d.position);
                var rb = manager.Get(d.syncId);
                if (rb == null)
                {
                    // if the client does not have the synced rigidbody, create if from the data received
                    rb = NewRigidbodySync(d);
                }

                rb.HandleUpdate(d, initial);
            }

            MessagePool.Return(msg);
        }

        // position already updated to RemotePos(d)
        private RigidbodySyncComponent NewRigidbodySync(RigidbodySyncData data)
        {
            var prefab = VRPrefabManager.instance.Spawn(data.prefabType, data.position, data.rotation);
            if (prefab == null)
            {
                Debug.LogError(this.ToString() + " NewRigidbodySync missing prefab setup for prefab of type == " + data.prefabType.ToString());
                return null;
            }

            var rb = prefab.GetComponent<RigidbodySyncComponent>();
            if (rb == null)
            {
                rb = prefab.AddComponent<RigidbodySyncComponent>();
                Debug.LogWarning(this.ToString() + " NewRigidbodySync prefab missing a RigidbodySyncComponent, prefab == " + prefab.ToString() + ", adding the component automatically.");
            }

            rb.Initialize(data.syncId);
            return rb;
        }

        private void HandleImpalement(byte[] buffer)
        {
            var msg = MessagePool.Get<ImpalementMessage>(buffer);

            Transform tr = null;
            if (msg.impalementType == ImpalementType.Player)
            {
                var player = GetPlayer((short)msg.netId);
                tr = player != null ? player.transform : null;
            }
            else if (msg.impalementType == ImpalementType.Equippable)
            {
                var equippable = GetEntity<INetEquippable>(msg.netId);
                tr = equippable != null ? equippable.transform : null;
            }
            else if (msg.impalementType == ImpalementType.SyncedRigidbody)
            {
                var syncedRb = ((ClientRigidbodySyncManager)ClientRigidbodySyncManager.instance).Get(msg.netId);
                tr = syncedRb != null ? syncedRb.transform : null;
            }

            if (tr == null)
            {
                Debug.LogError(this.ToString() + " HandleImpalement could not find impaler transform to use from type == " + msg.impalementType.ToString() + ", and id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            Transform otherTr = null;
            if (msg.otherType == ImpalementType.Player)
            {
                var otherPlayer = GetPlayer((short)msg.otherId);
                otherTr = otherPlayer != null ? otherPlayer.transform : null;
            }
            else if (msg.otherType == ImpalementType.Equippable)
            {
                var otherEquippable = GetEntity<INetEquippable>(msg.otherId);
                otherTr = otherEquippable != null ? otherEquippable.transform : null;
            }
            else if (msg.otherType == ImpalementType.SyncedRigidbody)
            {
                var otherSyncedRb = ((ClientRigidbodySyncManager)ClientRigidbodySyncManager.instance).Get(msg.otherId);
                otherTr = otherSyncedRb != null ? otherSyncedRb.transform : null;
            }

            if (otherTr == null && msg.otherType != ImpalementType.Ground)
            {
                // only the ground results in a null transform as a valid case
                Debug.LogError(this.ToString() + " HandleImpalement could not find the other from type == " + msg.otherType.ToString() + ", and id == " + msg.otherId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var impaler = tr.GetComponentInChildren<ICanImpale>();
            if (impaler == null)
            {
                Debug.LogError(this.ToString() + " HandleImpalement could not find an ICanImpale component on the identified transform == " + tr.ToString());
                MessagePool.Return(msg);
                return;
            }

            ImpalementHandler.FixClientImpalement(impaler, otherTr != null ? otherTr.GetComponent<Rigidbody>() : null, RemotePos(msg.position), msg.rotation, msg.anchor, msg.hardSound);
            MessagePool.Return(msg);
        }

        private void HandleUnimpalement(byte[] buffer)
        {
            var msg = MessagePool.Get<UnimpalementMessage>(buffer);

            Transform tr = null;
            if (msg.impalementType == ImpalementType.Player)
            {
                var player = GetPlayer((short)msg.netId);
                tr = player != null ? player.transform : null;
            }
            else if (msg.impalementType == ImpalementType.Equippable)
            {
                var equippable = GetEntity<INetEquippable>(msg.netId);
                tr = equippable != null ? equippable.transform : null;
            }
            else if (msg.impalementType == ImpalementType.SyncedRigidbody)
            {
                var syncedRb = ((ClientRigidbodySyncManager)ClientRigidbodySyncManager.instance).Get(msg.netId);
                tr = syncedRb != null ? syncedRb.transform : null;
            }

            if (tr == null)
            {
                Debug.LogError(this.ToString() + " HandleUnimpalement could not find impaler transform to use from type == " + msg.impalementType.ToString() + ", and id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var impaler = tr.GetComponentInChildren<ICanImpale>();
            if (impaler == null)
            {
                Debug.LogError(this.ToString() + " HandleUnimpalement could not find an ICanImpale component on the identified transform == " + tr.ToString());
                MessagePool.Return(msg);
                return;
            }

            ImpalementHandler.Unimpale(impaler);
            MessagePool.Return(msg);
        }

        /// <summary>
        /// Is not implemented for clients. DO NOT USE ON CLIENTS! TODO: Not so nice to have a method that is not allowed to call!
        /// </summary>
        /// <param name="netId"></param>
        /// <returns></returns>
        public override int GetConnectionId(short netId)
        {
            throw new System.NotImplementedException();
        }
    }
}