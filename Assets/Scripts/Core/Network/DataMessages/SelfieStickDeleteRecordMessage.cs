namespace Core
{
    using VRCore.VRNetwork;

    public sealed class SelfieStickDeleteRecordMessage : DataMessage
    {
        public SelfieStickDeleteRecordMessage() : base(NetMessageType.SelfieStickDeleteRecording)
        {
        }

        public SelfieStickDeleteRecordMessage(byte[] buffer) : base(buffer)
        {
        }

        public int equippableNetId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return 4;
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write(this.equippableNetId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.equippableNetId = s.ReadInt();
        }
    }
}
