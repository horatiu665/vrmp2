namespace Core
{
    using UnityEngine;
    using VRCore.VRNetwork;

    public sealed class PlayerRemoteConnectMessage : PlayerRemoteConnectMessageDefault
    {
        public Color color
        {
            get;
            set;
        }

        public string name
        {
            get;
            set;
        }

        public int headRbSyncId
        {
            get;
            set;
        }

        public int bodyRbSyncId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 17 + this.name.GetByteSize();  // color (8) + head rb sync id (4) + body rb sync id (4) + string count (1) + string
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.color);
            s.Write(this.name);
            s.Write(this.headRbSyncId);
            s.Write(this.bodyRbSyncId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.color = s.ReadColor();
            this.name = s.ReadString();
            this.headRbSyncId = s.ReadInt();
            this.bodyRbSyncId = s.ReadInt();
        }
    }
}