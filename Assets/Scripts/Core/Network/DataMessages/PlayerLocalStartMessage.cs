namespace Core
{
    using UnityEngine;
    using VRCore.VRNetwork;

    public sealed class PlayerLocalStartMessage : PlayerLocalStartMessageDefault
    {
        public Color color
        {
            get;
            set;
        }

        public int headRbSyncId
        {
            get;
            set;
        }

        public int bodyRbSyncId
        {
            get;
            set;
        }

		public Vector3 serverOriginShift
		{
			get;
			set;
		}

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 22; // color (8) + head rb sync id (4) + body rb sync id (4) + serverOS (6)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.color);
            s.Write(this.headRbSyncId);
            s.Write(this.bodyRbSyncId);
			s.Write(this.serverOriginShift);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.color = s.ReadColor();
            this.headRbSyncId = s.ReadInt();
            this.bodyRbSyncId = s.ReadInt();
			this.serverOriginShift = s.ReadVector3();
        }
    }
}