namespace Core
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    /// <summary>
    /// Server to clients message telling them to spawn a prefab type at a position and rotation.
    /// </summary>
    public sealed class SpawnExplosionMessage : PrefabTypeMessageBase
    {
        public SpawnExplosionMessage() : base(NetMessageType.SpawnExplosion)
        {
        }

        public SpawnExplosionMessage(byte[] buffer) : base(buffer)
        {
        }
        
        /// <summary>
        /// inexact world position for spawned object
        /// </summary>
        public Vector3 position
        {
            get;
            set;
        }
        
        protected override int GetByteSize()
        {
            return base.GetByteSize() + 6; // prefab type (1/2/4) + inexact world position (6)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);

            s.Write(this.position);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);

            this.position = s.ReadVector3();
        }
    }
}