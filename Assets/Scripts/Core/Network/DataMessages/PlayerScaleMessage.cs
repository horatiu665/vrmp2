namespace Core
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Client;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class PlayerScaleMessage : NetIdMessageBase
    {
        public PlayerScaleMessage() : base(NetMessageType.PlayerScale)
        {
        }

        public PlayerScaleMessage(byte[] buffer) : base(buffer)
        {
        }

        public Vector3 localScale { get; set; }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 3 * 2;
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.localScale);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.localScale = s.ReadVector3();
        }
    }
}