namespace Core
{
    using VRCore.VRNetwork;

    public sealed class SelfieStickStartRecordMessage : DataMessage
    {
        public SelfieStickStartRecordMessage(byte[] buffer) : base(buffer)
        {
        }

        public SelfieStickStartRecordMessage() : base(NetMessageType.SelfieStickStartRecording)
        {
        }

        public int equippableNetId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return 4;
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write(this.equippableNetId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.equippableNetId = s.ReadInt();
        }
    }
}
