namespace Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using VRCore.VRNetwork;

    public class GhettoTimeSyncServerMessage : DataMessage
    {
        public GhettoTimeSyncServerMessage() : base(NetMessageType.GhettoTimeSyncServer)
        {
        }

        public GhettoTimeSyncServerMessage(byte[] buffer) : base(buffer)
        {
        }

        public float clientTime { get; set; }

        public float serverTime { get; set; }

        protected override int GetByteSize()
        {
            // two floats
            return 8;
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write(clientTime);
            s.Write(serverTime);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            clientTime = s.ReadFloat();
            serverTime = s.ReadFloat();
        }

    }
}