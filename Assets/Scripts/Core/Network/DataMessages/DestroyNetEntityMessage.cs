namespace Core
{
    using VRCore.VRNetwork;

    public sealed class DestroyNetEntityMessage : DataMessage
    {
        public DestroyNetEntityMessage(byte[] buffer) : base(buffer)
        {
        }

        public DestroyNetEntityMessage() : base(NetMessageType.DestroyNetEntity)
        {
        }

        public int netEntityId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return 4;
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write(this.netEntityId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.netEntityId = s.ReadInt();
        }
    }
}