namespace Core
{
    using VoiceChat;
    using VRCore.VRNetwork;

    public sealed class VoiceChatMessage : DataMessage
    {
        public VoiceChatMessage(byte[] buffer)
            : base(buffer)
        {
        }

        public VoiceChatMessage()
            : base(NetMessageType.Voip)
        {
        }

        public VoiceChatPacket data
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            var d = data;
            return 1 + 4 + 8 + 4 + d.Length;
        }

        protected override void Serialize(NetSerializer s)
        {
            var d = data;
            s.Write((byte)d.Compression);
            s.Write(d.NetworkId);
            s.Write(d.PacketId);
            s.Write(d.Length);
            s.Write(d.Data, d.Length);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            var d = new VoiceChatPacket
            {
                Compression = (VoiceChatCompression)s.ReadByte(),
                NetworkId = s.ReadInt(),
                PacketId = s.ReadULong(),
                Length = s.ReadInt()
            };

            d.Data = s.ReadBytes(d.Length);

            this.data = d;
        }
    }
}