namespace Core
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using VRCore.VRNetwork;

    public class GhettoTimeSyncClientMessage : NetIdMessageBase
    {
        public GhettoTimeSyncClientMessage() : base(NetMessageType.GhettoTimeSyncClient)
        {
        }

        public GhettoTimeSyncClientMessage(byte[] buffer) : base(buffer)
        {
        }

        public float clientTime { get; set; }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 4;
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(clientTime);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            clientTime = s.ReadFloat();

        }

    }
}