namespace Core
{
    using VRCore;
    using VRCore.VRNetwork;

    public sealed class ChangePreviewMessage : PrefabTypeMessageBase
    {
        public ChangePreviewMessage() : base(NetMessageType.ChangePreview)
        {
        }

        public ChangePreviewMessage(byte[] buffer) : base(buffer)
        {
        }

        public VRPrefabType oldPrefabType
        {
            get;
            set;
        }

        public bool isLeft
        {
            get;
            set;
        }

        public short netId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + this.oldPrefabType.GetByteSize() + 3; // prefab type (1/2/4) + is left bool (1) + net id (2)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);

            s.Write(this.oldPrefabType);
            s.Write(this.isLeft);
            s.Write(this.netId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);

            this.oldPrefabType = s.ReadPrefabType();
            this.isLeft = s.ReadBool();
            this.netId = s.ReadShort();
        }
    }
}