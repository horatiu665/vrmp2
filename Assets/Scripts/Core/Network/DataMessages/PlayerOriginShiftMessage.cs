namespace Core
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Client;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;

    public class PlayerOriginShiftMessage : NetIdMessageBase
    {
        public PlayerOriginShiftMessage() : base(NetMessageType.PlayerOriginShift)
        {
        }

        public PlayerOriginShiftMessage(byte[] buffer) : base(buffer)
        {
        }

        public Vector3 originShift { get; set; }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 3 * 4; // high precision float vector3
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.WriteExact(this.originShift);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.originShift = s.ReadExactVector3();
        }
    }
}