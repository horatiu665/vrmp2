namespace Core
{
    using UnityEngine;
    using VRCore.VRNetwork;

    [Apex.ApexComponent("Network")]
    public sealed class ServerPlayerController : ServerPlayerControllerDefault, IPlayerScaleListener
    {
        public void OnScaleChange(Vector3 newScale)
        {
            // No need for lerping on server - just apply scale
            _player.transform.localScale = newScale;
        }
    }
}