namespace Core
{
    using UnityEngine;
    using VRCore;

    [Apex.ApexComponent("Network")]
    [RequireComponent(typeof(IPlayer))]
    public sealed class RemotePlayerController : MonoBehaviour, IVRPlayerStartListener, IPlayerScaleListener
    {
        [SerializeField]
        private float _scaleDuration = 0.1f;

        private float _lastScaleTime = 0f;
        private Vector3 _lastScale;
        private Vector3 _targetScale;
        private IPlayer _player;

        private void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (IPlayer)player;
            _targetScale = _player.transform.localScale;
        }

        public void OnScaleChange(Vector3 newScale)
        {
            _lastScale = _player.transform.localScale;
            _targetScale = newScale;
            _lastScaleTime = Time.timeSinceLevelLoad;
        }

        private void Update()
        {
            if (_targetScale == _player.transform.localScale)
            {
                // no change needed
                return;
            }

            var dt = Time.timeSinceLevelLoad - _lastScaleTime;
            _player.transform.localScale = Vector3.Lerp(_lastScale, _targetScale, Mathf.Clamp01(dt / _scaleDuration));
            _player.UpdateConnectedAnchors();
        }
    }
}