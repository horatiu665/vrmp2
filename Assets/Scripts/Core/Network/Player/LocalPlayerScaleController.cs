namespace Core
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;
    using VRCore.VRNetwork.Client;

    [Apex.ApexComponent("Network")]
    [RequireComponent(typeof(IVRPlayer))]
    public class LocalPlayerScaleController : NetBehaviourBase, IVRPlayerStartListener
    {
        private readonly PlayerScaleMessage _message = new PlayerScaleMessage();

        private IVRPlayer _player;
        private Vector3 _sentScale;

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<IVRPlayer>());
            }
        }

        protected override void OnSend()
        {
            if (_message.localScale == _player.transform.localScale)
            {
                return;
            }

            _message.localScale = _player.transform.localScale;
            ClientNetSender.instance.Send(_message, UnityEngine.Networking.QosType.UnreliableSequenced);
        }
    }
}