
namespace Core.Network
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    public sealed class PrefabRecyclingManager : SingletonMonoBehaviour<PrefabRecyclingManager>
    {
        [SerializeField]
        private PrefabTypeLimit[] _prefabTypeLimitSetup = new PrefabTypeLimit[0];

        private readonly Dictionary<VRPrefabType, List<IVRPrefab>> _dict = new Dictionary<VRPrefabType, List<IVRPrefab>>();

        private void OnEnable()
        {
            VRPrefabManager.OnSpawn += VRPrefabManager_OnSpawn;
        }

        private void OnDisable()
        {
            VRPrefabManager.OnSpawn -= VRPrefabManager_OnSpawn;
        }

        private void VRPrefabManager_OnSpawn(IVRPrefab obj)
        {
            if (!NetServices.isServer && NetServices.isNetworked)
            {
                return;
            }

            PrefabTypeLimit limit;
            if (!GetPrefabTypeLimit(obj.prefabType, out limit))
            {
                return;
            }

            if (!_dict.ContainsKey(obj.prefabType))
            {
                _dict.Add(obj.prefabType, new List<IVRPrefab>(limit.limitCount));
            }

            var list = _dict.GetValueOrDefault(obj.prefabType);
            for (int i = 0; i < list.Count; i++)
            {
                var prefab = list[i];
                if (prefab == null)
                {
                    list.RemoveAt(i);
                    continue;
                }

                if (((UnityEngine.Object)prefab).IsDestroyed())
                {
                    list.RemoveAt(i);
                    continue;
                }

                if (!prefab.gameObject.activeSelf)
                {
                    list.RemoveAt(i);
                }
            }

            if (list.Count >= limit.limitCount)
            {
                for (int i = 0; i < list.Count; i++)
                {
                    var prefab = list[i];

                    var e = prefab as IVREquippable;
                    if (e != null && e.isEquipped)
                    {
                        continue;
                    }

                    var impaler = prefab as ICanImpale;
                    if (impaler != null)
                    {
                        ImpalementHandler.Unimpale(impaler);
                    }

                    var ne = prefab as INetEntity;
                    if (NetServices.isServer && ne != null)
                    {
                        NetEntityDestructionManager.instance.DestroyNetEntity(ne);
                    }
                    else
                    {
                        VRPrefabManager.instance.Return(prefab);
                    }

                    list.RemoveAt(i);
                    break;
                }
            }

            list.Add(obj);
        }
        
        private bool GetPrefabTypeLimit(VRPrefabType type, out PrefabTypeLimit limit)
        {
            for (int i = 0; i < _prefabTypeLimitSetup.Length; i++)
            {
                if (_prefabTypeLimitSetup[i].prefabType == type)
                {
                    limit = _prefabTypeLimitSetup[i];
                    return true;
                }
            }

            limit = default(PrefabTypeLimit);
            return false;
        }

        [Serializable]
        private struct PrefabTypeLimit
        {
            public VRPrefabType prefabType;
            public int limitCount;
        }
    }
}
