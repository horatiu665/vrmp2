using Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRCore;
using VRCore.VRNetwork;
using VRCore.VRNetwork.Server;
using VRCore.VRNetwork.Client;
using System;

/// <summary>
/// Attempts to sync time values from the server with the clients by averaging over time. It gets better over time but might fluctuate with lagginess.
/// </summary>
public class GhettoTimeSync : SingletonMonoBehaviour<GhettoTimeSync>
{
    [SerializeField]
    private float _updateFreq = 3f;

    private GhettoTimeSyncClientMessage clientMsg = new GhettoTimeSyncClientMessage();
    private GhettoTimeSyncServerMessage serverMsg = new GhettoTimeSyncServerMessage();

    INetPlayer _player;
    INetPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<INetPlayer>();
            }
            return _player;
        }
    }

    float _serverTimeDelta;
    public float serverTime
    {
        get
        {
            return Time.time + _serverTimeDelta;
        }
        set
        {
            // serverTime == clientTime + _serverTimeDelta, and value is the approx serverTime
            _serverTimeDelta = value - Time.time;
        }

    }

    [SerializeField]
    [Range(0, 1f)]
    private float _serverLagSmoothUpdate = 1f;

    [SerializeField, ReadOnly]
    float _serverLag;
    public float serverLag
    {
        get
        {
            return _serverLag;
        }
        private set
        {
            _serverLag = value;
        }
    }

    [Header("Make this the same value on server and client, to test sync just kind of with your eye")]
    [SerializeField]
    private float _debugInterval = 0;
    float nextDebugTime = 0;

    private void OnEnable()
    {
        serverTime = Time.time;
        StartCoroutine(UpdateRare());
    }

    private IEnumerator UpdateRare()
    {
        while (true)
        {
            if (player != null)
            {
                if (NetServices.isNetworked && NetServices.isClient)
                {
                    clientMsg.clientTime = serverTime;
                    clientMsg.netId = player.netId;
                    ClientNetSender.instance.Send(clientMsg, UnityEngine.Networking.QosType.Unreliable);
                }
            }
            yield return new WaitForSeconds(1f / Mathf.Max(0.01f, _updateFreq));
        }
        //yield break;
    }

    public void HandleClientTimeSync(float clientTime, float serverTime)
    {
        // client received response.
        // round trip time
        var RTT = this.serverTime - clientTime;
        _serverLag = Mathf.Lerp(_serverLag, RTT / 2, _serverLagSmoothUpdate);
        this.serverTime = serverTime - this._serverLag;
    }

    public void HandleServerTimeSync(short netId, float clientTime)
    {
        // server received our shitty message.
        serverMsg.clientTime = clientTime;
        serverMsg.serverTime = Time.time;
        ServerNetSender.instance.Send(netId, serverMsg, UnityEngine.Networking.QosType.Unreliable);
    }

    private void Update()
    {
        if (_debugInterval > 0)
        {
            // print every exactly debugInterval seconds
            if (serverTime > nextDebugTime)
            {
                nextDebugTime += _debugInterval;
                Debug.Log("YO! it's " + serverTime + " time here.");
            }
        }
    }


}
