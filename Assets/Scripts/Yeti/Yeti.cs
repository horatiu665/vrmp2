namespace Core
{
    using Apex.WorldGeometry;
    using UnityEngine;

    /// <summary>
    /// the base class containing all his functionality and shit
    /// </summary>
    public sealed class Yeti : MonoBehaviour
    {
        private YetiAnim _yetiAnim;
        public YetiAnim yetiAnim
        {
            get
            {
                return _yetiAnim;
            }
        }

        public Transform head
        {
            get
            {
                return _yetiAnim.head;
            }
        }

        // rotation input stuff
        private Vector3 _desiredLookDirection;

        public Vector3 desiredLookDirection
        {
            get
            {
                return _desiredLookDirection;
            }

            set
            {
                _desiredLookDirection = value;
            }
        }

        private Rigidbody _rb;

        public Rigidbody rb
        {
            get
            {
                if (_rb == null)
                {
                    _rb = GetComponent<Rigidbody>();
                }
                return _rb;
            }
        }
        
        [Header("Gravity stuff")]
        [SerializeField]
        private bool _useFakeGravity;

        [SerializeField]
        private bool _grounded;

        [SerializeField]
        private float _groundedSphereCastRadius = 0f;

        private Vector3 _fallingVelocity = Vector3.zero;

        [SerializeField]
        private float _gravityMultiplier = 1f;

        [Header("Movement stuff")]

        [SerializeField]
        // dist. traveled in animation (400 according to oleg) / animation length (0.625) * animation timescale in the animator (1f) * maya import settings (0.01)
        private float _fwdWalkSpeed = 4f / 0.625f;

        public float fwdWalkSpeed
        {
            get
            {
                return _fwdWalkSpeed;
            }
        }

        [SerializeField]
        private float _minHitVelocityForAnimation = 1f;

        [SerializeField]
        private Vector2 _hitVelocityAnimationRange = new Vector2(1, 5f);

        [SerializeField]
        private float _bodyTurningSmoothness = 0.1f;

        private Vector2 _movementInputXZ;

        private bool _isAttacking;

        private RaycastHit _lastHitInfo;

        [SerializeField]
        private bool _useRootMotion = true;

        public Vector3 position
        {
            get { return transform.position; }
        }

        public bool grounded
        {
            get { return _grounded; }
        }

        private Vector3 gravity
        {
            get { return Physics.gravity; }
        }

        public Vector2 movementInputXZ
        {
            get { return _movementInputXZ; }
            set { _movementInputXZ = value; }
        }

        private Vector3 movementDirection
        {
            get
            {
                return (movementInputXZ.y * transform.forward) + (movementInputXZ.x * transform.right);
            }
        }

        private float movementSpeed
        {
            get
            {
                return _fwdWalkSpeed * movementInputXZ.y;
            }
        }

        private void Awake()
        {
            _yetiAnim = GetComponent<YetiAnim>();
        }

        private void FixedUpdate()
        {
            HandleGravity();
            HandleRotation();
            MoveTowards();
        }

        private void OnCollisionEnter(Collision c)
        {
            var rel = c.relativeVelocity.magnitude;
            if (rel > _minHitVelocityForAnimation)
            {
                var hitSize = Mathf.Clamp01(Mathf.InverseLerp(_hitVelocityAnimationRange.x, _hitVelocityAnimationRange.y, rel));
                // direction towards collider. contacts[0].normal could also work, if the physics system wasn't shit
                var hitDirection = c.collider.transform.position - transform.position;
                hitDirection.y = 0;
                var localHitDirection = transform.InverseTransformDirection(hitDirection);
                _yetiAnim.HitAnimation(hitSize, localHitDirection);
            }
        }

        private bool Raycast(Ray ray, float maxDistance)
        {
            if (_groundedSphereCastRadius == 0)
            {
                return (Physics.Raycast(ray, maxDistance, Layers.terrain));
            }
            else
            {
                ray.origin += Vector3.up * _groundedSphereCastRadius;
                return Physics.SphereCast(ray, _groundedSphereCastRadius, 1f, Layers.terrain);
            }
        }

        private bool Raycast(Ray ray, float maxDistance, out RaycastHit hitInfo)
        {
            if (_groundedSphereCastRadius == 0)
            {
                return (Physics.Raycast(ray, out hitInfo, maxDistance, Layers.terrain));
            }
            else
            {
                return Physics.SphereCast(ray, _groundedSphereCastRadius, out hitInfo, maxDistance, Layers.terrain);
            }
        }

        private void MoveTowards()
        {
            yetiAnim.MoveForward(movementInputXZ.y);
            if (_movementInputXZ.sqrMagnitude > 0f)
            {
                if (!_useRootMotion)
                {
                    //if (!_isAttacking)
                    //{
                    if (_lastHitInfo.normal == Vector3.up)
                    {
                        transform.position += _fwdWalkSpeed * transform.forward * Time.fixedDeltaTime * _movementInputXZ.y;
                    }
                    else
                    {
                        float downAmount = 1f - Vector3.Dot(_lastHitInfo.normal, Vector3.up);
                        transform.position += _fwdWalkSpeed * (transform.forward + Vector3.down * downAmount) * Time.fixedDeltaTime * _movementInputXZ.y;
                    }
                    //}
                }
            }
        }

        private void HandleGravity()
        {
            if (_useFakeGravity)
            {
                // grounded, gravity stuff
                var ray = new Ray(transform.position + Vector3.up, Vector3.down);
                _grounded = Raycast(ray, 1f, out _lastHitInfo);

                if (!_grounded)
                {
                    RaycastHit hitInfo;
                    // check if we can fall downwards with fallingVelocity
                    if (!Raycast(new Ray(transform.position + Vector3.up, Vector3.down), _fallingVelocity.magnitude * Time.fixedDeltaTime, out hitInfo))
                    {
                        // apply velocity to position
                        transform.position += _fallingVelocity * Time.fixedDeltaTime;
                        // apply acceleration to velocity
                        _fallingVelocity += Physics.gravity * _gravityMultiplier * Time.fixedDeltaTime;
                    }
                    else
                    {
                        // cannot fall because something in the way, just place us on the surface of the hit surface
                        transform.position = hitInfo.point;
                        _fallingVelocity = Vector3.zero;
                        // set grounding to true? maybe not because it's set before the  if(!_grounded)  above.
                    }
                }
                else
                {
                    _fallingVelocity = Vector3.zero;
                }
            }
            else
            {
                // just set info about groundedness
                var ray = new Ray(transform.position + Vector3.up, Vector3.down);
                _grounded = Raycast(ray, 1f, out _lastHitInfo);

            }
        }

        private void HandleRotation()
        {
            // set rotation to where we are moving
            var e = movementDirection;
            e = Vector3.ProjectOnPlane(e, Vector3.up);
            transform.forward = Vector3.Lerp(transform.forward, e, _bodyTurningSmoothness);
        }

        public void Attack()
        {
            yetiAnim.Attack();
            _isAttacking = true;

            // hardcoded attack anim duration
            StartCoroutine(pTween.Wait(0.958f, () =>
            {
                _isAttacking = false;
            }));
        }

    }
}