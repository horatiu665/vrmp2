using Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class YetiAnim : MonoBehaviour
{
    Yeti yeti;

    [Header("Animation params")]
    ////[SerializeField]
    ////private float _headTurningSmoothness = 0.1f;

    [SerializeField]
    private float _headTurningMaxDeltaRad = 0.1f;

    [SerializeField]
    private Transform _headRotationCompare = null;

    [SerializeField]
    private float _headMaxTurningAngle = 135f, _headLerpArbitraryMaxAngle = 45f;

    /// <summary>
    /// Corresponds to Body, Head, Eyes and Clamp weights in the Animator.SetLookAtWeights()
    /// </summary>
    [Header("Corresponds to Body, Head, Eyes and Clamp weights in the Animator.SetLookAtWeights()")]
    [SerializeField]
    private Vector4 _lookAtWeights = new Vector4(0.55f, 0.8f, 1, 1);

    [SerializeField]
    private float _lookAtGlobalWeight = 1f;

    private Animator _anim;

    [SerializeField]
    private Transform _yetiHead = null;

    public Transform head
    {
        get
        {
            return _yetiHead;
        }
    }

    private void Reset()
    {
        if (_yetiHead == null)
        {
            // find head
            _yetiHead = FindHeadRecursive(transform);
        }
    }

    private Transform FindHeadRecursive(Transform parent)
    {
        if (parent.name.ToLower().Contains("head"))
        {
            return parent;
        }

        for (int i = 0; i < parent.childCount; i++)
        {
            var headTest = FindHeadRecursive(parent.GetChild(i));
            if (headTest != null)
            {
                return headTest;
            }
        }

        return null;
    }

    private void Awake()
    {
        _anim = GetComponent<Animator>();
        yeti = GetComponent<Yeti>();
    }

    private void OnAnimatorIK()
    {
        _anim.SetLookAtPosition(_yetiHead.position + yeti.desiredLookDirection);
        _anim.SetLookAtWeight(_lookAtGlobalWeight, _lookAtWeights.x, _lookAtWeights.y, _lookAtWeights.z, _lookAtWeights.w);

        // limit head rotation compared to regular forward, so we never snap the head backwards.
        var desiredTurnAngle = Mathf.Clamp(Vector3.Angle(_headRotationCompare.forward, yeti.desiredLookDirection), 0, _headMaxTurningAngle);
        var desiredLookDirectionClamped = Vector3.Lerp(_headRotationCompare.forward, yeti.desiredLookDirection, Mathf.InverseLerp(0, _headLerpArbitraryMaxAngle, desiredTurnAngle));
        var newDir = Vector3.RotateTowards(_yetiHead.forward, desiredLookDirectionClamped, _headTurningMaxDeltaRad, 1f);
        if (newDir != Vector3.zero)
        {
            _yetiHead.forward = newDir; // Vector3.Lerp(_yetiHead.forward, _desiredLookDirection, _headTurningSmoothness);
        }
    }

    public void HitAnimation(float hitSize, Vector3 localHitDirection)
    {
        _anim.SetFloat("GetHitSize", hitSize);
        _anim.SetFloat("GetHitDirectionX", localHitDirection.x);
        _anim.SetFloat("GetHitDirectionZ", localHitDirection.z);
        _anim.SetTrigger("GetHit");
    }

    public void Attack()
    {
        _anim.SetTrigger("Attack");
        _anim.SetBool("AttackLeftHand", Random.value < 0.5f);
    }

    public void MoveForward(float y)
    {
        // move fwd
        _anim.SetBool("Running", y != 0);
        _anim.SetFloat("Speed", y);
    }
}