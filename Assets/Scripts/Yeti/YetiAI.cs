namespace Core
{
    using UnityEngine;
    using VRCore.VRNetwork;

    [RequireComponent(typeof(Yeti))]
    public sealed class YetiAI : MonoBehaviour
    {
        private Yeti _yeti;

        private IPlayer _player;

        // TODO: This is a total hack-solution and will NOT work in multiplayer reliably
        public IPlayer player
        {
            get
            {
                if (_player == null)
                {
                    if (SinglePlayerControllerDefault.instance != null)
                    {
                        _player = (IPlayer)SinglePlayerControllerDefault.instance.player;
                    }
                    else
                    {
                        _player = (IPlayer)LocalPlayerControllerDefault.instance.player;
                    }
                }

                return _player;
            }
        }

        public float minDistance = 2f;
        public float distToBoost = 30f;

        public AnimationCurve distToSideMovement = AnimationCurve.EaseInOut(0, 0, 1, 1);
        public float maxPlayerSpeed = 10f;
        public float sideMovementAmount = 1f;

        private void Awake()
        {
            _yeti = GetComponent<Yeti>();
        }

        private void Update()
        {
            if (player == null)
            {
                return;
            }

            _yeti.desiredLookDirection = player.position - transform.position;

            // move to player but also zigzag a bit
            var toPlayer = player.position - transform.position;
            if (toPlayer.sqrMagnitude > minDistance * minDistance)
            {
                var dist = toPlayer.magnitude;
                var extraSideMove = distToSideMovement.Evaluate(dist / maxPlayerSpeed) * sideMovementAmount * player.rb.velocity.normalized;

                var moveInput = transform.InverseTransformDirection(toPlayer.normalized + extraSideMove).normalized;
                moveInput.y = 0f;

                if (toPlayer.sqrMagnitude > distToBoost * distToBoost)
                {
                    moveInput.z *= 3f;
                }

                _yeti.movementInputXZ = new Vector2(moveInput.x, moveInput.z);
            }
            else
            {
                _yeti.movementInputXZ = Vector2.zero;
                _yeti.Attack();
            }

            // retardo reset
            if (transform.position.y < -1000)
            {
                transform.position = Vector3.zero;
            }
        }
    }
}