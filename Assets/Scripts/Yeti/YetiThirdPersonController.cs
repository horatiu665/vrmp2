namespace Core
{
    using UnityEngine;

    [RequireComponent(typeof(Yeti))]
    public sealed class YetiThirdPersonController : MonoBehaviour
    {
        [Header("Refs")]
        [SerializeField]
        private Camera _thirdPersonCamera = null;

        [Header("Movement Input")]
        [SerializeField]
        private KeyCode _keyLeft = KeyCode.A;

        [SerializeField]
        private KeyCode _keyRight = KeyCode.D;

        [SerializeField]
        private KeyCode _keyUp = KeyCode.W;

        [SerializeField]
        private KeyCode _keyDown = KeyCode.S;

        [SerializeField]
        private KeyCode _speedBoostShift = KeyCode.LeftShift;

        [SerializeField]
        private float _inputSmoothness = 0.1f;

        [SerializeField]
        private float speedBoostMultiplier = 3f;

        ////[SerializeField]
        ////private float _minInputForRunning = 0.3f;

        [Header("Attack Input")]
        private KeyCode _attackKey = KeyCode.Space;

        private Vector2 _input;
        private Yeti _yeti;

        [Header("lookat")]
        public Transform lookatTarget;

        private void Awake()
        {
            _yeti = GetComponent<Yeti>();
        }

        private void FixedUpdate()
        {
            if (_thirdPersonCamera == null)
            {
                return;
            }

            // INPUT steering
            var inputRaw = new Vector2(
                (Input.GetKey(_keyLeft) ? -1 : 0) + (Input.GetKey(_keyRight) ? 1 : 0),
                (Input.GetKey(_keyDown) ? -1 : 0) + (Input.GetKey(_keyUp) ? 1 : 0));

            if (Input.GetKey(_speedBoostShift))
            {
                inputRaw.y *= speedBoostMultiplier;
            }

            _input = Vector2.Lerp(_input, inputRaw, _inputSmoothness);
            if (_input.magnitude < 0.01f)
            {
                _input = Vector3.zero;
            }

            _yeti.movementInputXZ = _input;

            // set rotation to where we are looking (use third person camera just like player cheat steering)
            ////transform.LookAt(transform.position + _thirdPersonCamera.transform.forward);

            if (lookatTarget != null)
            {
                _yeti.desiredLookDirection = lookatTarget.position - _yeti.head.position;
            }
            else
            {
                var e = _thirdPersonCamera.transform.forward;
                _yeti.desiredLookDirection = e;

            }

            if (Input.GetKeyDown(_attackKey))
            {
                _yeti.Attack();
            }
        }


    }
}