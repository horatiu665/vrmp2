namespace Core
{
    using UnityEngine;
    using VRCore;

    public class GodzillaPaws : MonoBehaviour
    {
        public float _forceMultiplier = 1f;

        private IVRPlayer _player;
        private Vector3 _deltaPos;
        private Vector3 _prevPos;

        private void OnTriggerEnter(Collider c)
        {
            var vrPlayer = c.GetPlayer<IPlayer>();
            //var vrPlayer = c.GetComponent<IVRPlayer>();
            if (vrPlayer != null)
            {
                // check yourself before you wreck yourself
                if (vrPlayer != _player)
                {
                    // add force in the direction of the hand movement
                    vrPlayer.AddForce(this, _deltaPos / Time.fixedDeltaTime * _forceMultiplier, ForceMode.Impulse);
                    
                }
            }
        }

        private void Awake()
        {
            _player = GetComponentInParent<IVRPlayer>();
        }

        private void FixedUpdate()
        {
            _deltaPos = transform.position - _prevPos;
            _prevPos = transform.position;
        }
    }
}