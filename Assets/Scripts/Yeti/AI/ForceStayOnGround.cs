using Apex.WorldGeometry;
using Mountain.Filters;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ForceStayOnGround : MonoBehaviour
{
    public bool useRaycast = false;

    public bool update = true;
    public bool fixedUpdate = false;

    public float maxDistance = 1000f;

    private Vector3 oldPos;
    private float posDelta;

    private void FixedUpdate()
    {
        if (fixedUpdate)
            SetHeight();
    }

    private void Update()
    {
        if (update)
            SetHeight();

        posDelta = (transform.position - oldPos).magnitude;
        oldPos = transform.position;

    }

    void SetHeight()
    {
        if (useRaycast || MountainHeightProviderSingleton.instance == null)
        {
            RaycastHit info;
            if (Physics.Raycast(transform.position + Vector3.up * (1 + posDelta), Vector3.down, out info, maxDistance, Layers.terrain))
            {
                var h = info.point.y;
                transform.position = new Vector3(transform.position.x, h, transform.position.z);
                
            }
            Debug.DrawRay(transform.position + Vector3.up * (1 + posDelta), Vector3.down * maxDistance, Color.red);
        }
        else
        {
            var h = MountainHeightProviderSingleton.SampleHeight(transform.position);
            transform.position = new Vector3(transform.position.x, h, transform.position.z);
            
        }
    }
}