using PlantmanAI4;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using Core;
using VRCore;

public class StateFlankAttack : StateYetiBase, IState
{
    StateFlankPlayer flankState;

    YetiPlayerTarget playerTarget
    {
        get
        {
            return yetiCuriosity.yetiPlayerTarget;
        }
    }

    public float stayInFrontOfPlayerBeforeAttack = 5f;

    bool attackStarted = false;
    float attackStartTime = 0f;

    public float blendAttackDuration = 1f;

    public float distToPlayerForAttackAnim = 1f;

    public Vector2 joustForce = new Vector2(20, 5f);

    public override string GetName()
    {
        return "Flank Attack";
    }

    public override float GetPriority()
    {
        return priority;
    }

    public override void OnEnter()
    {
        attackStartTime = Time.time;
    }

    public override void OnExecute(float deltaTime)
    {
        base.OnExecute(deltaTime);

        var t = Mathf.Clamp01((Time.time - attackStartTime) / blendAttackDuration);

        var flankTarget = this.playerTarget.transform.position;
        var velocityRatio = 1f - t;
        var playerPos = player.transform.position + player.rb.velocity * velocityRatio;

        yeti.SetLookTarget(player.head);

        yeti.SetMoveTarget(Vector3.Lerp(flankTarget, playerPos, t));

        var towardsPlayer = playerPos - yeti.transform.position;

        // if close to player, joust them!
        if (towardsPlayer.magnitude < distToPlayerForAttackAnim)
        {
            yeti.Attack();
            attackStarted = false;
            // joust force
            player.AddForce(this, towardsPlayer.normalized * joustForce.x + Vector3.up * joustForce.y);
        }

    }

    public override void OnExit()
    {
        attackStarted = false;
    }

    public override bool ConditionsMet()
    {
        if (player == null)
            return false;

        if (attackStarted)
        {
            return true;
        }

        if (yetiCuriosity.curiosity.playerLineOfSight && yetiCuriosity.curiosity.playerLookingAtYeti)
        {
            if (yetiCuriosity.curiosity.stayingTimeInFrontOfPlayer > stayInFrontOfPlayerBeforeAttack)
            {
                attackStarted = true;
            }
        }

        return attackStarted;
    }

}