using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using PlantmanAI4;
using Core;
using VRCore;
using VRCore.VRNetwork;

/// <summary>
/// References to player and all needed for character operations
/// </summary>
public class StateYetiBase : MonoBehaviour, IState
{
    public float priority = 1f;

    private IPlayer _player;

    public IPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<IPlayer>();
            }

            return _player;
        }
    }

    private CharacterBaseController _yeti;
    public CharacterBaseController yeti
    {
        get
        {
            if (_yeti == null)
            {
                _yeti = GetComponentInParent<CharacterBaseController>();
            }
            return _yeti;
        }
    }

    YetiPlayerCuriosity _yetiCuriosity;
    public YetiPlayerCuriosity yetiCuriosity
    {
        get
        {
            if (_yetiCuriosity == null)
            {
                _yetiCuriosity = GetComponentInParent<YetiPlayerCuriosity>();
            }
            return _yetiCuriosity;
        }
    }

    public virtual string GetName()
    {
        return "YetiBase";
    }

    public virtual float GetPriority()
    {
        return priority;
    }

    public virtual bool GetUninterruptible()
    {
        return false;
    }

    public virtual void OnEnter()
    {

    }

    public virtual void OnExecute(float deltaTime)
    {

    }

    public virtual void OnExit()
    {

    }

    public virtual bool ConditionsMet()
    {
        return true;
    }

}