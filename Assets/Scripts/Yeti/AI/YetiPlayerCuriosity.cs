using Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using VRCore.VRNetwork;
using Random = UnityEngine.Random;

public class YetiPlayerCuriosity : MonoBehaviour
{
    private IPlayer _player;
    public IPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<IPlayer>();
            }

            return _player;
        }
    }

    private YetiPlayerTarget _yetiPlayerTarget;
    public YetiPlayerTarget yetiPlayerTarget
    {
        get
        {
            if (_yetiPlayerTarget == null)
            {
                _yetiPlayerTarget = GetComponentInChildren<YetiPlayerTarget>();
            }
            return _yetiPlayerTarget;
        }
    }

    [Header("Yeti perception")]
    public Curiosity curiosity;

    [Serializable]
    public class Curiosity
    {
        public bool playerLineOfSight = false;

        public bool playerLookingAtYeti = false;

        public float seenByPlayerTime = 0f;

        public float inFrontOfPlayerScore = 0f;

        public float stayingTimeInFrontOfPlayer = 0f;
    }

    [Header("Params")]
    public LayerMask lineOfSightLayers = -1;
    public float minDotForPlayerLook = 0.5f;
    public float minDotForFrontOfPlayer = 0.5f;

    private void Update()
    {
        if (player == null)
        {
            return;
        }

        // is yeti visible by player?
        curiosity.playerLineOfSight = !Physics.Linecast(transform.position + Vector3.up, player.position + Vector3.up, lineOfSightLayers);

        if (Vector3.Dot(player.head.forward, (transform.position - player.position).normalized) > minDotForPlayerLook)
        {
            curiosity.playerLookingAtYeti = true;
        }
        else
        {
            curiosity.playerLookingAtYeti = false;
        }

        // how long does the player look at the yeti?
        if (curiosity.playerLineOfSight && curiosity.playerLookingAtYeti)
        {
            curiosity.seenByPlayerTime += Time.deltaTime;
        }
        else
        {
            curiosity.seenByPlayerTime -= Time.deltaTime;
        }
        curiosity.seenByPlayerTime = Mathf.Max(curiosity.seenByPlayerTime, 0f);

        // is yeti in front of player based on movement?
        if (player.rb.velocity.sqrMagnitude > 1f)
        {
            curiosity.inFrontOfPlayerScore = Vector3.Dot((transform.position - player.position).normalized, player.rb.velocity.normalized);
        }
        else
        {
            curiosity.inFrontOfPlayerScore = 0f;
        }

        // how long has yeti been in front of the player?
        if (curiosity.inFrontOfPlayerScore > minDotForFrontOfPlayer)
        {
            curiosity.stayingTimeInFrontOfPlayer += Time.deltaTime;
        }
        else
        {
            curiosity.stayingTimeInFrontOfPlayer -= 0f;
        }
    }
}