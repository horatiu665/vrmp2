using Core;
using PlantmanAI4;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore.VRNetwork;
using Random = UnityEngine.Random;

public class YetiPlayerTarget : MonoBehaviour
{
    private IPlayer _player;

    public IPlayer player
    {
        get
        {
            return yetiCuriosity.player;
        }
    }

    private CharacterBaseController _yeti;
    public CharacterBaseController yeti
    {
        get
        {
            if (_yeti == null)
            {
                _yeti = GetComponentInParent<CharacterBaseController >();
            }
            return _yeti;
        }
    }

    YetiPlayerCuriosity _yetiCuriosity;
    public YetiPlayerCuriosity yetiCuriosity
    {
        get
        {
            if (_yetiCuriosity == null)
            {
                _yetiCuriosity = GetComponentInParent<YetiPlayerCuriosity>();
            }
            return _yetiCuriosity;
        }
    }

    [SerializeField]
    private float playerAvoidRange = 20f;
    public AnimationCurve playerAvoidRangeVelocityMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [Header("Blend params")]
    public AnimationCurve frontOfPlayerSideBlend = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(-1, 0, 0, 0), new Keyframe(0, 1, 0, 0), new Keyframe(1, 0.5f, 0, 0) } };

    public AnimationCurve frontOfPlayerFrontBlend = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(-1, 1, 0, 0), new Keyframe(0, 0.5f, 0, 0), new Keyframe(1, 1f, 0, 0) } };

    public AnimationCurve avoidRangeSideBlend = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

    [Header("Speed")]
    public AnimationCurve frontOfPlayerSpeedMultiply = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(-1, 1.2f, 0, 0), new Keyframe(0, 1.2f, 0, 0), new Keyframe(1, 0.9f, 0, 0) } };

    public AnimationCurve avoidRangeSpeedMultiply = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1f, 0, 0), new Keyframe(1, 0.9f, 0, 0) } };

    public float speedMultiplier = 0;

    private void Update()
    {
        if (player == null)
        {
            return;
        }

        var vel = player.rb.velocity;
        vel.y = 0;

        var dir = yeti.transform.position - player.position;
        dir.y = 0;

        var dist = dir.magnitude;

        // avoid range based on velocity
        var avoidRange = playerAvoidRange * playerAvoidRangeVelocityMultiplier.Evaluate(vel.magnitude);

        // 1 when close to player, 0 when far.
        var avoidParam = 1f - Mathf.Clamp01(dist / avoidRange);

        var velocityRightCross = Vector3.Cross(vel, Vector3.up).normalized;
        Vector3 playerRightFront = vel + velocityRightCross * avoidRange;
        Vector3 playerLeftFront = vel + -velocityRightCross * avoidRange;

        // normalized vector towards left or right of velocity (whichever closest)
        Vector3 closestSideVector = (yeti.transform.position - (player.position + playerRightFront)).sqrMagnitude > (yeti.transform.position - (player.position + playerLeftFront)).sqrMagnitude
            ? (-velocityRightCross) : (velocityRightCross);

        var fop = yetiCuriosity.curiosity.inFrontOfPlayerScore;
        Vector3 targetPos = player.position
            + frontOfPlayerFrontBlend.Evaluate(fop) * vel
            + frontOfPlayerSideBlend.Evaluate(fop) * closestSideVector * avoidRange
            + avoidRangeSideBlend.Evaluate(avoidParam) * closestSideVector * avoidRange;

        speedMultiplier = frontOfPlayerSpeedMultiply.Evaluate(fop) * avoidRangeSpeedMultiply.Evaluate(avoidParam);

        transform.position = targetPos;
    }
}