using PlantmanAI4;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using Core;
using VRCore;

public class StateStaticPlayerAttack : StateYetiBase, IState
{

    YetiPlayerTarget playerTarget
    {
        get
        {
            return yetiCuriosity.yetiPlayerTarget;
        }
    }

    bool attackStarted = false;
    float attackStartTime = 0f;

    public float distToPlayerForAttackAnim = 1f;

    public Vector2 joustForce = new Vector2(20, 5f);

    public Vector2 attackAbsVelocityRange = new Vector2(12, 100f);

    public float playerVelocityMax = 5f;
    float playerSlowTimer = 0f;

    public float playerSlowDurationForAttack = 5f;

    public override string GetName()
    {
        return "Static Attack";
    }

    public override float GetPriority()
    {
        return priority;
    }

    public override void OnEnter()
    {
        attackStartTime = Time.time;
    }

    public override void OnExecute(float deltaTime)
    {
        base.OnExecute(deltaTime);




        float moveSpeed = 1f;

        var towardsPlayer = player.transform.position - yeti.position;
        var moveDir = towardsPlayer;
        moveSpeed = player.rb.velocity.magnitude * playerTarget.speedMultiplier;
        moveSpeed = Mathf.Clamp(moveSpeed, attackAbsVelocityRange.x, attackAbsVelocityRange.y);

        yeti.SetLookTarget(player.head);

        yeti.SetMoveTarget(yeti.transform.position + moveDir);

        // if close to player, joust them!
        if (towardsPlayer.magnitude < distToPlayerForAttackAnim)
        {
            yeti.Attack();
            attackStarted = false;
            // joust force
            player.AddForce(this, towardsPlayer.normalized * joustForce.x + Vector3.up * joustForce.y);
        }

    }


    public override bool ConditionsMet()
    {
        if (player == null)
            return false;

        if (attackStarted)
        {
            return true;
        }

        if (player.rb.velocity.sqrMagnitude < playerVelocityMax * playerVelocityMax)
        {
            if (playerSlowTimer == 0)
            {
                playerSlowTimer = Time.time;
            }
            else
            {
                if (Time.time - playerSlowTimer > playerSlowDurationForAttack)
                {
                    attackStarted = true;
                }
            }
        }
        else
        {
            playerSlowTimer = 0;
        }

        return attackStarted;
    }

}