using PlantmanAI4;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using Core;
using VRCore;

public class StateFlankPlayer : StateYetiBase, IState
{
    public float distanceToStartFollow = 500;

    public AnimationCurve maxSpeedDistanceMultiplier = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
    public float maxSpeed = 100;

    public float yetiMinSpeed = 10f;

    YetiPlayerTarget playerTarget
    {
        get
        {
            return yetiCuriosity.yetiPlayerTarget;
        }
    }


    public override string GetName()
    {
        return "Flank Player";
    }

    public override float GetPriority()
    {
        return priority;
    }

    public override void OnEnter()
    {

    }

    public override void OnExecute(float deltaTime)
    {
        base.OnExecute(deltaTime);

        yeti.SetLookTarget(player.head, player.velocity / 3);

        yeti.SetMoveTarget(playerTarget.transform.position);

    }

    public override void OnExit()
    {

    }

    public override bool ConditionsMet()
    {
        if (player == null)
            return false;

        var sqrDist = (player.transform.position - transform.position).sqrMagnitude;
        var inRange = sqrDist < distanceToStartFollow * distanceToStartFollow;

        return inRange;
    }

}