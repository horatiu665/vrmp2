namespace Core
{
    using UnityEngine;

    public class YetiLookAtTest : MonoBehaviour
    {
        public Transform lookAtTarget = null;

        public Transform left = null, right = null;

        public Transform head = null;

        public bool rotateBodyToHeadFw = true;

        [SerializeField]
        [Range(0f, 1f)]
        private float _bodyTurningSmoothness = 0.1f;

        [Range(0f, 1f)]
        public float global = 0f, body = 0f, headWeight = 0f, eyes = 0f, clamp = 0f;

        private Animator _anim;

        private void Start()
        {
            _anim = GetComponent<Animator>();
        }

        private void Update()
        {
            if (rotateBodyToHeadFw)
            {
                // set rotation to where we are moving
                var e = head.forward;
                e = Vector3.ProjectOnPlane(e, Vector3.up);
                transform.forward = Vector3.Lerp(transform.forward, e, _bodyTurningSmoothness);
            }
        }

        private void OnAnimatorIK()
        {
            // body ik
            _anim.SetLookAtWeight(global, body, headWeight, eyes, clamp);
            _anim.SetLookAtPosition(lookAtTarget.position);

            // hands ik
            SetHandIK(AvatarIKGoal.RightHand, AvatarIKHint.RightElbow, right);
            SetHandIK(AvatarIKGoal.LeftHand, AvatarIKHint.LeftElbow, left);
        }

        private void SetHandIK(AvatarIKGoal ikGoal, AvatarIKHint ikHint, Transform target)
        {
            // elbow
            var hintPos = (target.position + head.position) * 0.5f;
            hintPos += Vector3.down * transform.lossyScale.x;
            // extra offset just downwards on target local direction
            var extra = -target.up * transform.lossyScale.x;
            hintPos += extra;
            _anim.SetIKHintPosition(ikHint, hintPos);
            _anim.SetIKHintPositionWeight(ikHint, 1f);

            // hand
            _anim.SetIKPosition(ikGoal, target.position);
            _anim.SetIKRotation(ikGoal, target.rotation);
            _anim.SetIKPositionWeight(ikGoal, 1f);
            _anim.SetIKRotationWeight(ikGoal, 1f);
        }
    }
}