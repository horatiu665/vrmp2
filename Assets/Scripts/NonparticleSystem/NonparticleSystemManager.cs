﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

/// <summary>
/// Specialized pool system for Nonparticles. (objects that behave like particles but are 3d and not a particle system) 
/// </summary>
public class NonparticleSystemManager : MonoBehaviour
{
    public Dictionary<VRPrefabType, Queue<Nonparticle>> pool = new Dictionary<VRPrefabType, Queue<Nonparticle>>();

    public int initQueueCapacity = 256;

    public Nonparticle Get(VRPrefabType particleType, Vector3 pos, Quaternion rot)
    {
        Nonparticle part = null;
        // if type already has a pool, use it. else spawn a new one
        if (pool.ContainsKey(particleType) && pool[particleType].Count > 0)
        {
            part = pool[particleType].Dequeue();
            part.transform.position = pos;
            part.transform.rotation = rot;
        }
        else
        {
            part = VRPrefabManager.instance.Spawn(particleType, pos, rot).GetComponent<Nonparticle>();
            part.transform.SetParent(transform);
        }

        part.OnSpawnFromPool();

        return part;
    }

    public void Return(Nonparticle part)
    {
        if (!pool.ContainsKey(part.prefabType))
        {
            pool[part.prefabType] = new Queue<Nonparticle>(initQueueCapacity);
        }

        part.OnReturnToPool();

        pool[part.prefabType].Enqueue(part);
    }

}