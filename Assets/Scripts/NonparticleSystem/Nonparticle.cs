﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

/// <summary>
/// Fake particle. Separate objects, 3d, does all the regular gameobject stuff, but is treated like a particle using the NonparticleSystem and the NonparticleSystemManager
/// Important naming convention: Nonparticle with lowercase "p" so it does not look like "NonParticle". Has nothing to do with Particle.
/// </summary>
public class Nonparticle : MonoBehaviour
{
    private IVRPrefab _vrPrefab;
    public IVRPrefab vrPrefab
    {
        get
        {
            if (_vrPrefab == null)
            {
                _vrPrefab = GetComponent<IVRPrefab>();
            }
            return _vrPrefab;
        }
    }

    public VRPrefabType prefabType
    {
        get
        {
            return vrPrefab.prefabType;
        }
    }

    public void OnSpawnFromPool()
    {
        // enable renderer

    }

    public void OnReturnToPool()
    {
        // disable renderer

    }
}