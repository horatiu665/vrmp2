namespace Helpers
{
    using UnityEngine;
    using VRCore;

    /// <summary>
    /// SmartRange is meant for use as a random value provider,
    /// which can be switched in the Unity inspector between different modes, such as:
    /// constant, random range, distribution curve, and potentially extensible to other modes.
    /// Its property drawer can be useful for drawing a curve and multiple parameter fields on one line. Please steal.
    /// </summary>
    [System.Serializable]
    public class SmartRange
    {
        [SerializeField]
        private Mode _mode = Mode.RandomRange;

        // needed as separate value for serialization and for inspector.
        [SerializeField]
        private float _constValue = 0f;

        // used for both RandomRange and for DistributionCurve modes.
        [SerializeField]
        private Vector2 _range = new Vector2(0f, 1f);

        /// <summary>
        /// Curve used to remap a random value from 0 to 1 (on the X axis)
        /// to a value from 0 to 1 (on the y axis), and using that result to interpolate between range.x and range.y.
        /// </summary>
        [SerializeField]
        private AnimationCurve _distributionCurve = AnimationCurve.Linear(0f, 0f, 1f, 1f);

        /// <summary>
        /// Initializes the SmartRange with <c>mode = Mode.Constant</c>, and an initial <c>constValue</c> specified.
        /// </summary>
        public SmartRange(float constant)
        {
            _constValue = constant;
            _mode = Mode.Constant;
        }

        /// <summary>
        /// Initializes the SmartRange with <c>mode = Mode.RandomRange</c>, and a specified value for <c>range.x</c> and <c>range.y</c>.
        /// </summary>
        public SmartRange(float rangeX, float rangeY)
            : this(new Vector2(rangeX, rangeY))
        {
        }

        /// <summary>
        /// Initializes the SmartRange with <c>mode = Mode.RandomRange</c>, and a specified value for <c>range</c>.
        /// </summary>
        public SmartRange(Vector2 range)
        {
            _range = range;
            _mode = Mode.RandomRange;
        }

        /// <summary>
        /// For the lazy coder: TRUE IS EASE, FALSE IS LINEAR.
        /// Initializes the SmartRange with <c>mode = Mode.DistributionCurve</c>, and a specified value for <c>range</c>.
        /// Assigns a default value for <c>distributionCurve</c>, which is the EaseInOut curve when tooLazyToInit is true, and the Linear curve when false.
        /// </summary>
        /// <param name="tooLazyToInit">If true, initializes <c>distributionCurve</c> to <c>AnimationCurve.EaseInOut(0, 0, 1, 1)</c>, else to <c>AnimationCurve.Linear(0, 0, 1, 1)</c></param>
        public SmartRange(float rangeX, float rangeY, bool tooLazyToInit)
            : this(new Vector2(rangeX, rangeY), tooLazyToInit)
        {
        }

        /// <summary>
        /// For the lazy coder: TRUE IS EASE, FALSE IS LINEAR.
        /// Initializes the SmartRange with <c>mode = Mode.DistributionCurve</c>, and a specified value for <c>range</c>.
        /// Assigns a default value for <c>distributionCurve</c>, which is the EaseInOut curve when tooLazyToInit is true, and the Linear curve when false.
        /// </summary>
        /// <param name="tooLazyToInit">If true, initializes <c>distributionCurve</c> to <c>AnimationCurve.EaseInOut(0, 0, 1, 1)</c>, else to <c>AnimationCurve.Linear(0, 0, 1, 1)</c></param>
        public SmartRange(Vector2 range, bool tooLazyToInit)
            : this(range, tooLazyToInit ? AnimationCurve.EaseInOut(0f, 0f, 1f, 1f) : AnimationCurve.Linear(0f, 0f, 1f, 1f))
        {
        }

        /// <summary>
        /// Initializes the SmartRange with <c>mode = Mode.DistributionCurve</c>, and a specified value for <c>range</c> and <c>distributionCurve</c>.
        /// </summary>
        public SmartRange(float rangeX, float rangeY, AnimationCurve distributionCurve)
            : this(new Vector2(rangeX, rangeY), distributionCurve)
        {
        }

        /// <summary>
        /// Initializes the SmartRange with <c>mode = Mode.DistributionCurve</c>, and a specified value for <c>range</c> and <c>distributionCurve</c>.
        /// </summary>
        public SmartRange(Vector2 range, AnimationCurve distributionCurve)
        {
            _range = range;
            _distributionCurve = distributionCurve;
            _mode = Mode.DistributionCurve;
        }

        public enum Mode
        {
            Constant,
            RandomRange,
            DistributionCurve
        }

        /// <summary>
        /// Use to get random float value based on the settings specified in the inspector.
        /// </summary>
        public float value
        {
            get
            {
                switch (_mode)
                {
                    case Mode.Constant:
                    {
                        return _constValue;
                    }

                    case Mode.RandomRange:
                    {
                        return _range.Random();
                    }

                    case Mode.DistributionCurve:
                    {
                        // assume uniform distribution if not initialized/curve length zero. This can happen when resizing an array in the unity inspector.
                        if (_distributionCurve.length == 0)
                        {
                            return _range.Random();
                        }

                        return Mathf.Lerp(_range.x, _range.y, _distributionCurve.Evaluate(Random.Range(0f, 1f)));
                    }

                    default:
                    {
                        Debug.LogWarning(this.ToString() + " unhandled mode == " + _mode.ToString());
                        return 0f;
                    }
                }
            }
        }

        /// <summary>
        /// Use to get random int value based on the settings specified in the inspector.
        /// </summary>
        public int intValue
        {
            get
            {
                return Mathf.RoundToInt(this.value);
            }
        }
    }
}