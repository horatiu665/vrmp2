﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

/// <summary>
/// Moves an object in the same way it would be moved by the bendy shader. 
/// Can be used for billboards and UI and other objects that cannot support the shader.
/// </summary>
public class UIBendyHack : MonoBehaviour
{
    private IVRPlayer _player;
    public IVRPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<IVRPlayer>();
            }
            return _player;
        }
    }

    RomanticShaderManager.ShaderFloatValue flatRadius, falloff, offset;

    private void Start()
    {
        if (RomanticShaderManager.instance != null)
        {
            flatRadius = RomanticShaderManager.instance.floatValues.First(fv => fv.name == "_FlatRadius");
            falloff = RomanticShaderManager.instance.floatValues.First(fv => fv.name == "_FalloffRadius");
            offset = RomanticShaderManager.instance.floatValues.First(fv => fv.name == "_Offset");
        }

    }

    void Update()
    {
        if (player != null && RomanticShaderManager.instance != null)
        {
            // equivalent formula to the one in the bendy shader.. this should bend it the same way, using the same values.
            var cameraToObject = (player.position - transform.parent.position).magnitude;
            cameraToObject = Mathf.Clamp(cameraToObject - flatRadius.value, 0, 99999999);
            float dist = cameraToObject / falloff.value;
            var pos = transform.parent.position;
            pos.y -= dist * dist * offset.value;
            transform.position = pos;

            /*
                float4 worldPos = mul(unity_ObjectToWorld, v.vertex);
                float cameraToObject = distance(_WorldSpaceCameraPos, worldPos);
                cameraToObject = clamp((cameraToObject - _FlatRadius), 0.0, 99999999);
                float dist = cameraToObject / _FalloffRadius;
                worldPos -= float4(0, dist * dist * _Offset, 0, 0);
                float4 localOffsetPos = mul(unity_WorldToObject, worldPos);
                o.pos = UnityObjectToClipPos(localOffsetPos);
            */
        }
    }
}
