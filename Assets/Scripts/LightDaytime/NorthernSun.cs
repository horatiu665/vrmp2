﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class NorthernSun : MonoBehaviour
{
    [Header("Params")]
    public float autoScroll = 1 / 24f;
    public float timeOfYear = 0f;
    public float timeOfDay = 0f;

    public float sunOrbitRadius = 2500f;

    public bool setLightToSun = false;

    [Header("Set these transforms up like the earth and a person on its surface")]
    public Transform earthUpAxis;
    public Transform earthMan;

    [Header("Debug")]
    public bool swap = false;
    public bool viewPosInYear = false;

    public int gizmoResolution = 120;

    private void OnValidate()
    {
        // loop
        timeOfDay = (timeOfDay + 1) % 1f;
        timeOfYear = (timeOfYear + 1) % 1f;
    }

    private void OnEnable()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update += Update;
#endif
    }

    private void OnDisable()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.update -= Update;
#endif
    }

    private void Update()
    {
        if (autoScroll != 0)
        {
            timeOfDay += autoScroll * Time.deltaTime;
            timeOfYear += autoScroll * Time.deltaTime / 365f;

            OnValidate();

            if (setLightToSun)
            {
                var lights = Light.GetLights(LightType.Directional, -1);
                lights[0].transform.forward = -GetSunDirection(timeOfYear, timeOfDay);

            }

#if UNITY_EDITOR
            UnityEditor.SceneView.RepaintAll();
#endif
        }

    }


    /// <summary>
    /// They seem flipped but it's true this way: the earth appears to rotate with the year time (compared to the static sun) and the sun rotates with the day time (compared to earth/..?) fuck it's confusing as hell
    /// </summary>
    /// <returns></returns>
    public Vector3 GetSunDirection(float yearTime01, float dayTime01)
    {
        if (swap)
        {
            float temp = yearTime01;
            yearTime01 = dayTime01;
            dayTime01 = temp;
        }
        Transform toRotate = viewPosInYear ? earthUpAxis : earthMan;

        // 1 km seems far enough to not get scorched by the sun. hehehe NOT because the sun's diameter is a mere 1.39 million km
        var yearAngle = Mathf.PI * 2 * dayTime01;
        var sunDir = new Vector3(Mathf.Sin(yearAngle), 0f, Mathf.Cos(yearAngle));

        var dayAngle = 360f * yearTime01;
        var old = toRotate.localEulerAngles;
        toRotate.localEulerAngles = new Vector3(old.x, dayAngle, old.z);

        sunDir = earthMan.rotation * earthUpAxis.rotation * sunDir;

        toRotate.localEulerAngles = old;

        return sunDir;
    }

    private void OnDrawGizmos()
    {
        if (earthUpAxis != null)
        {
            if (earthMan != null)
            {
                DrawSunShape();
            }
        }
    }

    void DrawSunShape()
    {
        Gizmos.color = Color.yellow;
        Vector3 oldPos = GetSunDirection(0f, timeOfYear) * sunOrbitRadius;
        for (int i = 0; i < gizmoResolution; i++)
        {
            var pos = GetSunDirection(i / (gizmoResolution - 1f), timeOfYear) * sunOrbitRadius;
            Gizmos.DrawLine(pos, oldPos);
            oldPos = pos;
        }

        Gizmos.DrawSphere(GetSunDirection(timeOfDay, timeOfYear) * sunOrbitRadius, sunOrbitRadius * 0.1f);
    }

}