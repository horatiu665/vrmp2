namespace Core
{
    using System.Collections;
    using System.Collections.Generic;
    using UnityEngine;
    using VRCore;

    public class SkiStickRespawner : MonoBehaviour
    {
        [SerializeField]
        private List<RespawnSetup> _list = new List<RespawnSetup>();

        [SerializeField]
        private float _updateInterval = 1f;

        private void Awake()
        {
            BakePositions();
        }

        private void Start()
        {
            StartCoroutine(UpdateRare());
        }

        private IEnumerator UpdateRare()
        {
            // check every X=_updateInterval seconds
            while (Application.isPlaying)
            {
                yield return new WaitForSeconds(_updateInterval);

                for (int i = 0; i < _list.Count; i++)
                {
                    if (_list[i].instance != null)
                    {
                        if (_list[i].instance.position != _list[i].position)
                        {
                            _list[i].SpawnListItem(this);
                        }
                    }
                    else
                    {
                        // list instance is null. if we have position, spawn something here.
                        _list[i].SpawnListItem(this);
                    }
                }
            }
        }

        private IEnumerator AnimateSpawned(IVRPrefab spawned)
        {
            var initScale = spawned.transform.localScale;
            yield return StartCoroutine(pTween.To(0.3f, t =>
            {
                spawned.transform.localScale = initScale * Mathf.SmoothStep(0, 1, t);
            }));
        }

        [DebugButton]
        private void MakeListFromChildren()
        {
            // add all transform children to the list, in order.
            for (int i = 0; i < transform.childCount; i++)
            {
                // if item does not fit in the list, add new list element.
                if (_list.Count <= i)
                {
                    var r = new RespawnSetup();
                    r.instance = transform.GetChild(i);
                    _list.Add(r);
                }
                else
                {
                    _list[i].instance = transform.GetChild(i);
                }

                // try to assign the prefab type
                var vrPrefab = _list[i].instance.GetComponent<VRPrefabBase>();
                if (vrPrefab != null)
                {
                    _list[i].prefabType = vrPrefab.prefabType;
                }
            }
        }

        [DebugButton]
        private void BakePositions()
        {
            // init position of all list items, to check when they move
            for (int i = 0; i < _list.Count; i++)
            {
                if (_list[i].instance != null)
                {
                    _list[i].position = _list[i].instance.position;
                    _list[i].rotation = _list[i].instance.rotation;
                }
            }
        }

        [System.Serializable]
        private class RespawnSetup
        {
            public Transform instance;
            public VRPrefabType prefabType;

            [ReadOnly]
            [SerializeField]
            public Vector3 position;

            [ReadOnly]
            [SerializeField]
            public Quaternion rotation;

            public void SpawnListItem(SkiStickRespawner parent)
            {
                // instance moved. spawn another in tits place
                var spawned = VRPrefabManager.instance.Spawn<IVRPrefab>(prefabType);
                spawned.position = position;
                spawned.rotation = rotation;

                parent.StartCoroutine(parent.AnimateSpawned(spawned));
                // destroy the old instance? maybe not..... hmm.

                // keep track of newly spawned instance.
                instance = spawned.transform;
            }
        }
    }
}