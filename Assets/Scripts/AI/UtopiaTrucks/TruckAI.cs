using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class TruckAI : MonoBehaviour
{
    [Header("Pos")]
    public float movementSpeed = 1f;

    public float maxSpeed = 10f;

    public Transform centerOfMass;

    [Header("Jump")]
    public Vector2 jumpyTime = new Vector2(1, 3);
    private float jumpyTimer;

    public float jumpForce = 10f;
    public float jumpSideRandomForce = 2f;

    [Header("Rot")]
    public Vector2 changeDirectionTime = new Vector2(3, 12);
    public float rotationMaxAngle = 1f;
    public float targetRotationAngle = 0f;
    private float changeDirectionTimer = 0;

    [Range(0,1f)]
    public float changeDirectionMaxAmount = 1f;

    private Rigidbody r;

    public List<Rigidbody> wheels;


    private void Awake()
    {
        r = GetComponent<Rigidbody>();
    }

    private void Start()
    {
        if (centerOfMass != null)
        {
            r.centerOfMass = centerOfMass.localPosition;
        }
    }

    void Update()
    {
        if (wheels.Count > 0)
        {

            ChangeDirectionUpdate();
            JumpyUpdate();

            MoveForward();
            RotateTowards(targetRotationAngle);

        }
    }

    private void JumpyUpdate()
    {
        if (jumpyTimer < 0)
        {
            jumpyTimer = jumpyTime.Random();
            // do a jump
            r.AddForce(Vector3.up * jumpForce + Random.onUnitSphere * jumpSideRandomForce, ForceMode.VelocityChange);
        }
        jumpyTimer -= Time.deltaTime;
    }

    private void ChangeDirectionUpdate()
    {
        if (changeDirectionTimer < 0)
        {
            changeDirectionTimer = changeDirectionTime.Random();
            targetRotationAngle += Random.Range(0, 360f * changeDirectionMaxAmount);
            targetRotationAngle = targetRotationAngle % 360f;
            //targetRotationAngle = Random.Range(0, 360f);
        }
        changeDirectionTimer -= Time.deltaTime;
    }

    private void MoveForward()
    {
        // moves car
        var dir = transform.forward;
        dir.y = 0f;
        var standingUpParam = Mathf.Max(0, Vector3.Dot(transform.up, Vector3.up));
        standingUpParam *= standingUpParam;
        r.velocity = Vector3.ClampMagnitude(r.velocity, maxSpeed);

        //r.AddForce(dir.normalized * standingUpParam * movementSpeed, ForceMode.Acceleration);
        for (int i = 0; i < wheels.Count; i++)
        {
            var w = wheels[i];
            w.AddRelativeTorque(Vector3.right * movementSpeed, ForceMode.Acceleration);

        }
    }

    private void RotateTowards(float y)
    {
        var targetRot = Quaternion.Euler(transform.eulerAngles.x, y, transform.eulerAngles.z);
        var ang = Quaternion.Angle(transform.rotation, targetRot);
        transform.rotation = Quaternion.Lerp(transform.rotation, targetRot, rotationMaxAngle / ang);
    }
}