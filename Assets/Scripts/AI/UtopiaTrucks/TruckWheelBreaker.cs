using Core;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class TruckWheelBreaker : MonoBehaviour, IExplodable
{
    public float health = 100f;
    bool broken, dead = false;

    public float equippableBreakForce = 1f;

    public float impaleBreakForce = 1f;

    public float bombBreakForce = 50f;

    TruckAI truckAi;

    private void OnCollisionEnter(Collision c)
    {
        if (c.gameObject.GetComponent<Arrow>() != null)
        {
            health -= impaleBreakForce * c.relativeVelocity.magnitude;
        }
        else if (c.gameObject.GetComponent<IVREquippable>() != null)
        {
            health -= equippableBreakForce * c.relativeVelocity.magnitude;
        }
    }

    public void OnExplosion(Explosion e)
    {
        health -= bombBreakForce;
    }


    private void Awake()
    {
        truckAi = transform.parent.GetComponentInChildren<TruckAI>();
    }

    void Start()
    {

    }

    void Update()
    {
        if (health < 50f && !broken)
        {
            broken = true;
            Break();
        }

        if (health < 0 && !dead)
        {
            dead = true;
            Die();
        }
    }

    private void Die()
    {
        var j = GetComponent<ConfigurableJoint>();
        Destroy(j);
        truckAi.wheels.Remove(GetComponent<Rigidbody>());
    }

    private void Break()
    {
        GetComponent<MeshRenderer>().enabled = false;
        GetComponent<MeshCollider>().enabled = false;
        transform.GetChild(0).gameObject.SetActive(true);
    }

    //[DebugButton]
    void MakeBroken()
    {
        var g = new GameObject("Broken " + transform.name, typeof(MeshFilter), typeof(MeshRenderer));

        var mf = g.GetComponent<MeshFilter>();
        mf.sharedMesh = GetComponent<MeshFilter>().sharedMesh;

        var mr = g.GetComponent<MeshRenderer>();
        mr.sharedMaterials = g.GetComponent<MeshRenderer>().sharedMaterials;

        var mc = g.AddComponent<MeshCollider>();
        mc.convex = true;

        g.transform.SetParent(transform);
        g.transform.localPosition = Vector3.zero;
        g.transform.localRotation = Quaternion.identity;
        g.transform.localScale = Vector3.one;


    }
}