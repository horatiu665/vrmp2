using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class RoadBuilderAI : MonoBehaviour
{
    public Transform cabinFront, cabinBack;

    public float truckLength = 1f;

    private Vector3 prevFrontTargetPos, prevBackTargetPos;
    private Quaternion prevFrontTargetRot, prevBackTargetRot;

    private Vector3 cabinFrontTargetPos, cabinBackTargetPos;
    private Quaternion cabinFrontTargetRot, cabinBackTargetRot;

    private float t;

    public float movementSpeed = 1f;

    [Range(0, 1f)]
    public float chanceToTurn = 0.33f;


    [DebugButton]
    public void MoveForward()
    {
        prevFrontTargetPos = cabinFrontTargetPos;
        prevBackTargetPos = cabinBackTargetPos;
        prevFrontTargetRot = cabinFrontTargetRot;
        prevBackTargetRot = cabinBackTargetRot;

        cabinBackTargetPos = cabinFrontTargetPos;
        cabinBackTargetRot = prevFrontTargetRot;

        var dir = cabinFront.forward;
        cabinFrontTargetPos += truckLength * dir;

    }

    [DebugButton]
    public void Turn(bool right)
    {
        prevFrontTargetPos = cabinFrontTargetPos;
        prevBackTargetPos = cabinBackTargetPos;
        prevFrontTargetRot = cabinFrontTargetRot;
        prevBackTargetRot = cabinBackTargetRot;

        cabinBackTargetPos = cabinFrontTargetPos;

        var dir = cabinFront.forward;
        var side = cabinFront.right * (right ? 1 : -1);
        cabinFrontTargetPos += truckLength * dir + truckLength * side;
        cabinFrontTargetRot = Quaternion.Euler(0, right ? 90 : -90, 0) * cabinFrontTargetRot;
    }

    private void OnEnable()
    {
        cabinFrontTargetPos = prevFrontTargetPos = cabinFront.position;
        cabinFrontTargetRot = prevFrontTargetRot = cabinFront.rotation;
        cabinBackTargetPos = prevBackTargetPos = cabinBack.position;
        cabinBackTargetRot = prevBackTargetRot = cabinBack.rotation;

    }

    private void Update()
    {
        t += Time.deltaTime * movementSpeed;
        // move cabin front

        cabinFront.transform.position = Vector3.Lerp(prevFrontTargetPos, cabinFrontTargetPos, t);
        cabinBack.transform.position = Vector3.Lerp(prevBackTargetPos, cabinBackTargetPos, t);
        cabinFront.transform.rotation = Quaternion.Slerp(prevFrontTargetRot, cabinFrontTargetRot, t);
        cabinBack.transform.rotation = Quaternion.Slerp(prevBackTargetRot, cabinBackTargetRot, t);

        if (t >= 1)
        {
            t = 0;
            // randomly decide to move fwd or turn
            if (Random.Range(0, 1f) < chanceToTurn)
            {
                Turn(Random.Range(0, 2) == 0);
            }
            else
            {
                MoveForward();
            }
        }
    }



}