using PlantmanAI4;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;

public class StateIdle : MonoBehaviour, IState
{
    public string GetName()
    {
        return "Idle";
    }

    public float GetPriority()
    {
        return 0;
    }

    public bool GetUninterruptible()
    {
        return false;
    }

    public void OnEnter()
    {
    }

    public void OnExecute(float deltaTime)
    {
    }

    public void OnExit()
    {
    }

    public bool ConditionsMet()
    {
        return true;
    }

}