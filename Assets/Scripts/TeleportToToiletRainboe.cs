﻿using Core;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class TeleportToToiletRainboe : MonoBehaviour
{
    public int toilet = -1;

    private void OnTriggerEnter(Collider other)
    {
        if (!this.isActiveAndEnabled)
        {
            return;
        }
        var player = other.GetPlayer<IPlayer>();
        if (player != null)
        {
            if (player.isLocal)
            {
                if (toilet != -1)
                {
                    ToiletTeleporterManager.instance.TeleportPlayerToToilet(player, toilet);

                }
                else
                {
                    Debug.LogWarning("[TeleportToToiletRainboe] no toilet id");
                }
            }

        }
    }

}