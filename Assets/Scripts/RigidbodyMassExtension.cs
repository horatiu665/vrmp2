﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public static class RigidbodyMassExtension
{
    public static void SetMassFromDensity(this Rigidbody rigidbody, float density)
    {
        rigidbody.SetDensity(density);

        // classic physx physics shit to apply properties to themselves
        rigidbody.mass = rigidbody.mass;
    }

    /// <summary>
    /// Calculates density based on rigidbody.mass. Uses heuristic method - trial and error over many iterations.
    /// </summary>
    /// <param name="rigidbody"></param>
    /// <param name="initDensityAssumption"></param>
    /// <param name="epsilon"></param>
    /// <param name="maxIterations"></param>
    /// <returns></returns>
    public static float GetDensity(this Rigidbody rigidbody, float initDensityAssumption = 1f, float epsilon = 0.01f, int maxIterations = 100)
    {
        // the final density calculated startin from this value. the closer it is to the final value, the fewer iterations needed.
        float density = initDensityAssumption;

        // save mass we are trying to reach
        var targetMass = rigidbody.mass;

        // setting this density means setting the mass so it matches
        rigidbody.SetDensity(density);

        // while dif between targe mass and current mass is larger than a tiny percentage of the target mass, keep trying to set density. (only try for a limited number of iterations)
        var iterations = maxIterations;
        while (Mathf.Abs(rigidbody.mass - targetMass) > targetMass * epsilon || iterations-- > 0)
        {
            if (rigidbody.mass > targetMass)
            {
                density *= 0.86234234f;
            }
            else
            {
                density *= 1.2364364f;
            }
            rigidbody.SetDensity(density);
        }

        // reset mass to the init mass, because it should not have changed even a slight bit
        rigidbody.mass = targetMass;

        return density;
    }


}