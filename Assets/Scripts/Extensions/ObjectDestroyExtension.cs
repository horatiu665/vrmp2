using UnityEngine;

public static class ObjectDestroyExtension
{
    public static bool IsDestroyed(this UnityEngine.Object obj)
    {
        try
        {
            if (obj is Component)
            {
                var activeself = (obj as Component).gameObject.activeSelf;
            }
            return obj == null;
        }
        catch (MissingReferenceException e)
        {
            Debug.LogWarning("[IsDestroyed()] Considering object " + obj + " to be destroyed. " + e);
            return true;
        }
    }
}
