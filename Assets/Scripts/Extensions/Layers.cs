﻿namespace Apex.WorldGeometry
{
    using UnityEngine;

    public static partial class Layers
    {
        /// <summary>
        /// Gets or sets the players' layer, representing all players, including AI/bots.
        /// </summary>
        /// <value>
        /// The players.
        /// </value>
        public static LayerMask players { get; set; }

        /// <summary>
        /// Gets or sets the scorables' layer, representing all <see cref="IScoreAwarder"/>s.
        /// </summary>
        /// <value>
        /// The scorables.
        /// </value>
        public static LayerMask scorables { get; set; }

        /// <summary>
        /// Gets or sets the default layer.
        /// </summary>
        /// <value>
        /// The default layer.
        /// </value>
        public static LayerMask defaultLayer { get; set; }

        /// <summary>
        /// Gets or sets the not visible in vr layer.
        /// </summary>
        /// <value>
        /// The not visible in vr layer.
        /// </value>
        public static LayerMask notVisibleInVRLayer { get; set; }
    }
}