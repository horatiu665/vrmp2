namespace Helpers
{
    using Apex.WorldGeometry;
    using UnityEngine;

    public sealed class LayersHelper : LayerMappingComponent
    {
        public LayerMask playerLayer;
        public LayerMask scorablesLayer; // for AI
        public LayerMask defaultLayer;
        public LayerMask notVisibleInVRLayer;

        internal override void Map()
        {
            base.Map();
            Layers.players = this.playerLayer;
            Layers.scorables = this.scorablesLayer;
            Layers.defaultLayer = this.defaultLayer;
            Layers.notVisibleInVRLayer = this.notVisibleInVRLayer;
        }
    }
}