﻿using System;
using System.Collections.Generic;

[Serializable]
public class DeferredExecutionData
{
	public DeferredExecutionTags tag = DeferredExecutionTags.Default;
	public int maxOperations = 20;

	public float operationsThisFrame = 0;
    public float operationsLastFrame = 0;

	public List<IDeferExecutions> registeredScripts = new List<IDeferExecutions>();

	public void Add(IDeferExecutions deferrer)
	{
		registeredScripts.Add(deferrer);
	}

	public void Remove(IDeferExecutions deferrer)
	{
		registeredScripts.Remove(deferrer);
	}
}