﻿public enum DeferredExecutionTags
{
	Default,
	SpawnJob,
	DeleteJob,
}
