﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class DeferredExecutionManager : MonoBehaviour
{
    private static DeferredExecutionManager _instance;
    public static DeferredExecutionManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DeferredExecutionManager>();
            }
            return _instance;
        }
    }

    // initialized on validate and awake to contain data for every type in the enum (with defaults if not setup)
    [SerializeField]
    private List<DeferredExecutionData> _tags = new List<DeferredExecutionData>();
    public List<DeferredExecutionData> tags
    {
        get
        {
            return _tags;
        }
    }

    [SerializeField]
    private int _unlimitedFramesOnStart = 2;

    [Header("Defaults")]
    public int defaultMaxOperations = 20;

    public void Register(IDeferExecutions deferrer, DeferredExecutionTags tag)
    {
        _tags[(int)tag].Add(deferrer);
    }

    public void Unregister(IDeferExecutions deferrer, DeferredExecutionTags tag)
    {
        _tags[(int)tag].Remove(deferrer);
    }

    public void CountOperation(IDeferExecutions deferrer, DeferredExecutionTags tag, float operationCount = 1)
    {
        // we must assume deferrer is already registered for performance reasons
        _tags[(int)tag].operationsThisFrame += operationCount;
    }

    public bool CanContinue(IDeferExecutions deferrer, DeferredExecutionTags tag)
    {
        return _tags[(int)tag].operationsThisFrame < _tags[(int)tag].maxOperations;
    }

    // at the end of the frame
    private void LateUpdate()
    {
        // clear all operationsThisFrame
        for (int i = 0; i < _tags.Count; i++)
        {
            _tags[i].operationsLastFrame = _tags[i].operationsThisFrame;

            _tags[i].operationsThisFrame = 0;
        }
    }

    private void OnValidate()
    {
        SetupTagsFromEnum();
    }

    // pretty expensive and unnecessary when data is properly serialized. should only happen once on validate and every time the enum is changed.
    private void SetupTagsFromEnum()
    {
        // create list of default values for all possible execution tags in the enum
        var fixedTags = new List<DeferredExecutionData>();
        var enumValues = Enum.GetNames(typeof(DeferredExecutionTags));
        for (int i = 0; i < enumValues.Length; i++)
        {
            fixedTags.Add(new DeferredExecutionData()
            {
                tag = (DeferredExecutionTags)i,
                maxOperations = defaultMaxOperations,
                operationsThisFrame = 0,
                registeredScripts = new List<IDeferExecutions>(),
            });
        }
        // overwrite all tags that are already set up in the editor 
        for (int i = 0; i < _tags.Count; i++)
        {
            fixedTags[(int)_tags[i].tag] = _tags[i];
        }
        _tags = fixedTags;
    }

    private void OnEnable()
    {
        if (_unlimitedFramesOnStart > 0)
        {
            SetUnlimitedFrame(_unlimitedFramesOnStart);
        }
    }

    public void SetUnlimitedFrame(int frames = 1)
    {
        if (frames <= 0)
        {
            return;
        }
        var realMaxOperations = new List<int>(tags.Count);
        for (int i = 0; i < tags.Count; i++)
        {
            realMaxOperations.Add(tags[i].maxOperations);
            tags[i].maxOperations = int.MaxValue;
        }
        StartCoroutine(pTween.WaitFrames(frames, () =>
        {
            for (int i = 0; i < tags.Count; i++)
            {
                tags[i].maxOperations = realMaxOperations[i];
            }
        }));
    }
}