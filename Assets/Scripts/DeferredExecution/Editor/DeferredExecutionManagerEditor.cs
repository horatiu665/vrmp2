﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.Generic;

[CustomEditor(typeof(DeferredExecutionManager))]
public class DeferredExecutionManagerEditor : Editor
{
    private MonoScript _script;
    private DeferredExecutionManager manager;
    bool[] deferrersFoldouts;

    private void OnEnable()
    {
        manager = target as DeferredExecutionManager;
        _script = MonoScript.FromMonoBehaviour(manager);
        deferrersFoldouts = new bool[manager.tags.Count];
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Tags and registered scripts", EditorStyles.boldLabel);

        EditorGUI.indentLevel++;
        for (int d = 0; d < manager.tags.Count; d++)
        {
            var data = manager.tags[d];

            EditorGUILayout.LabelField(data.tag.ToString());

            EditorGUI.indentLevel++;

            deferrersFoldouts[d] = EditorGUILayout.Foldout(deferrersFoldouts[d], "Registered scripts (" + data.registeredScripts.Count + ")");
            GUI.enabled = false;
            if (deferrersFoldouts[d])
            {
                if (data.registeredScripts.Count == 0)
                {
                    EditorGUILayout.LabelField("none");
                }
                for (int i = 0; i < data.registeredScripts.Count; i++)
                {
                    var obj = data.registeredScripts[i] as MonoBehaviour;
                    EditorGUILayout.ObjectField(obj, typeof(MonoBehaviour), true);
                }
            }
            GUI.enabled = true;

            EditorGUI.indentLevel--;

        }

        EditorGUI.indentLevel--;

    }

}