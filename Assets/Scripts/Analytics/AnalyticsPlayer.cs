namespace Apex.Analytics
{
    using UnityEngine;

    public sealed class AnalyticsPlayer : MonoBehaviour
    {
        [SerializeField, Range(0.1f, 10f)]
        private float _sendRate = 2f;

        [SerializeField]
        private string _analyticsEventName = "PlayerPosition";

        private float _lastSend;

        private void LateUpdate()
        {
            var time = Time.timeSinceLevelLoad;
            if (time - _lastSend < 1f / _sendRate)
            {
                return;
            }

            _lastSend = time;
            AnalyticsHelper.instance.HeatmapEvent(_analyticsEventName, this.transform);
        }
    }
}