﻿namespace Apex.Analytics
{
    using UnityEngine;
    using VRCore;

    public class VelocityVisualizer : MonoBehaviour
    {
        private IVRPlayer _player;

        public Vector3 rightHandVelocity
        {
            get { return _player.rightHand.controller.velocity; }
        }

        public Vector3 rightHandRotation
        {
            get { return _player.rightHand.controller.rotation.eulerAngles; }
        }

        public Vector3 rightHandAngularRotation
        {
            get { return _player.rightHand.controller.angularVelocity; }
        }

        public Vector3 leftHandVelocity
        {
            get { return _player.leftHand.controller.velocity; }
        }

        public Vector3 leftHandRotation
        {
            get { return _player.leftHand.controller.rotation.eulerAngles; }
        }

        public Vector3 leftHandAngularRotation
        {
            get { return _player.leftHand.controller.angularVelocity; }
        }

        private void Awake()
        {
            _player = GetComponent<IVRPlayer>();
        }
    }
}