﻿namespace Apex.Analytics
{
    using System.Collections.Generic;
    using System.Text;
    using UnityEngine;
    using UnityEngine.Analytics;
    using VRCore;
    using UA = UnityAnalyticsHeatmap;

    public sealed class AnalyticsHelper : SingletonMonoBehaviour<AnalyticsHelper>
    {
        private readonly IDictionary<string, object> _dict = new Dictionary<string, object>();

        [SerializeField, Tooltip("Set to true to enable debug logging along with event reporting.")]
        private bool _debugLog = false;

        [SerializeField, Tooltip("If set to true, will send the time since level load on enabled.")]
        private bool _sendOnEnable = false;

        private void OnEnable()
        {
            if (!_sendOnEnable)
            {
                return;
            }

            CustomEvent("onEnable", new KeyValuePair<string, object>("timeSinceLevelLoad", Time.timeSinceLevelLoad));
        }

        /// <summary>
        /// Sets the given user information and sends it to Unity analytics.
        /// </summary>
        /// <param name="gender">The gender.</param>
        /// <param name="birthYear">The birth year.</param>
        public void SetUserInfo(Gender gender, int birthYear)
        {
            Analytics.SetUserGender(gender);
            Analytics.SetUserBirthYear(birthYear);
        }

        /// <summary>
        ///  Sets the given user information and sends it to Unity analytics.
        /// </summary>
        /// <param name="gender">The gender.</param>
        /// <param name="birthYear">The birth year.</param>
        /// <param name="userId">The user identifier.</param>
        public void SetUserInfo(Gender gender, int birthYear, string userId)
        {
            Analytics.SetUserId(userId);
            Analytics.SetUserGender(gender);
            Analytics.SetUserBirthYear(birthYear);
        }

        /// <summary>
        /// Sends a customized event to Unity analytics along with any relevant data (only primitive data types).
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="eventData">The event data.</param>
        public void CustomEvent(string customEventName, params KeyValuePair<string, object>[] eventData)
        {
            CustomEvent(customEventName, (IList<KeyValuePair<string, object>>)eventData);
        }

        /// <summary>
        /// Sends a customized event to Unity analytics along with any relevant data (only primitive data types).
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="eventData">The event data.</param>
        public void CustomEvent(string customEventName, IList<KeyValuePair<string, object>> eventData)
        {
            _dict.Clear();

            var count = eventData.Count;
            for (int i = 0; i < count; i++)
            {
                _dict.Add(eventData[i].Key, eventData[i].Value);
            }

            CustomEvent(customEventName, _dict);
        }

        /// <summary>
        /// Sends a customized event to Unity analytics along with any relevant data (only primitive data types).
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="eventData">The event data.</param>
        public void CustomEvent(string customEventName, IDictionary<string, object> eventData)
        {
            if (eventData != null && eventData.Count > 0)
            {
                Analytics.CustomEvent(customEventName, eventData);

                if (_debugLog)
                {
                    var sb = new StringBuilder();
                    sb.Append("Analytics CustomEvent: ");
                    sb.AppendLine(string.Concat("\"", customEventName, "\""));
                    sb.Append("Args: ");

                    foreach (var pair in eventData)
                    {
                        sb.AppendLine(string.Concat("\"", pair.Key, "\" = ", pair.Value.ToString()));
                    }

                    Debug.Log(sb.ToString());
                }
            }
            else
            {
                CustomEvent(customEventName);
            }
        }

        /// <summary>
        /// Sends a customized event to Unity analytics along with a given position.
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="eventPosition">The event position.</param>
        public void CustomEvent(string customEventName, Vector3 eventPosition)
        {
            Analytics.CustomEvent(customEventName, eventPosition);

            if (_debugLog)
            {
                Debug.Log(string.Concat("Analytics CustomEvent: ", customEventName, ", position arg : ", eventPosition.ToString()));
            }
        }

        /// <summary>
        /// Sends a customized event to Unity analytics.
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        public void CustomEvent(string customEventName)
        {
            Analytics.CustomEvent(customEventName);

            if (_debugLog)
            {
                Debug.Log(string.Concat("Analytics CustomEvent: ", customEventName, " (no args)"));
            }
        }

        /// <summary>
        /// Sends a Heatmap event for the specified <see cref="Transform"/> to Unity analytics.
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="transform">The transform.</param>
        public void HeatmapEvent(string customEventName, Transform transform)
        {
            UA.HeatmapEvent.Send(customEventName, transform, Time.timeSinceLevelLoad);

            if (_debugLog)
            {
                Debug.Log(string.Concat("Analytics HeatmapEvent: ", customEventName));
            }
        }

        /// <summary>
        /// Sends a Heatmap event for the specified <see cref="Transform"/> to Unity analytics with the given extra event data.
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="transform">The transform.</param>
        /// <param name="eventData">The event data.</param>
        public void HeatmapEvent(string customEventName, Transform transform, params KeyValuePair<string, object>[] eventData)
        {
            HeatmapEvent(customEventName, transform, (IList<KeyValuePair<string, object>>)eventData);
        }

        /// <summary>
        /// Sends a Heatmap event for the specified <see cref="Transform"/> to Unity analytics with the given extra event data.
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="transform">The transform.</param>
        /// <param name="eventData">The event data.</param>
        public void HeatmapEvent(string customEventName, Transform transform, IList<KeyValuePair<string, object>> eventData)
        {
            _dict.Clear();

            var count = eventData.Count;
            for (int i = 0; i < count; i++)
            {
                _dict.Add(eventData[i].Key, eventData[i].Value);
            }

            HeatmapEvent(customEventName, transform, _dict);
        }

        /// <summary>
        /// Sends a Heatmap event for the specified <see cref="Transform"/> to Unity analytics with the given extra event data.
        /// </summary>
        /// <param name="customEventName">Name of the custom event.</param>
        /// <param name="transform">The transform.</param>
        /// <param name="eventData">The event data.</param>
        public void HeatmapEvent(string customEventName, Transform transform, IDictionary<string, object> eventData)
        {
            if (eventData != null && eventData.Count > 0)
            {
                UA.HeatmapEvent.Send(customEventName, transform, Time.timeSinceLevelLoad, (Dictionary<string, object>)eventData);

                if (_debugLog)
                {
                    var sb = new StringBuilder();
                    sb.Append("Analytics HeatmapEvent: ");
                    sb.AppendLine(string.Concat("\"", customEventName, "\""));
                    sb.Append("Args: ");

                    foreach (var pair in eventData)
                    {
                        sb.AppendLine(string.Concat("\"", pair.Key, "\" = ", pair.Value.ToString()));
                    }

                    Debug.Log(sb.ToString());
                }
            }
            else
            {
                HeatmapEvent(customEventName, transform);
            }
        }
    }
}