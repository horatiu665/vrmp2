namespace FastNoiseEditor
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using UnityEditor;
    using System.Collections;

//    public class FastNoisePreviewWindow : EditorWindow
//    {
//        public static FastNoisePreviewWindow lastVisibleWindow;
//        private static int stepsOverTime = 1;

//        public const int PREVIEW_SIZE = 512;
//        public const float FREQUENCY_SCALE = 100f;
//        const double MAX_TIME_PER_FRAME = 0.05;

//        [SerializeField]
//        private FastNoise noise;

//        private Texture2D tex;

//        private float[] noiseValues;

//        private Color[] colors;

//        private float calculationTime;

//        private Texture2D whiteSquare;
//        private Texture2D blackSquare;
//        private Texture2D progressBarSquare;

//        private bool normalize = false;
//        private bool coroutine = true;

//        private float progressBar = 0f;

//        private Vector2 mapDragOffset;

//        EditorCoroutine currentGenerationRoutine;

//        public static void ShowWindow(FastNoise noise)
//        {
//            var w = GetWindow(typeof(FastNoisePreviewWindow)) as FastNoisePreviewWindow;
//            //w.maxSize = w.minSize = new Vector2(previewSize + 10 + 10, previewSize + 10 + 10);
//            w.noise = noise;
//            if (w.tex == null)
//            {
//                w.RegenTexture(PREVIEW_SIZE);
//            }
//            w.Generate();

//            lastVisibleWindow = w;

//            w.blackSquare = new Texture2D(1, 1, TextureFormat.ARGB32, false);
//            w.blackSquare.SetPixel(0, 0, new Color(0, 0, 0, 0.4f));
//            w.blackSquare.Apply();

//            w.progressBarSquare = new Texture2D(1, 1, TextureFormat.ARGB32, false);
//            w.progressBarSquare.SetPixel(0, 0, new Color(0.5f, 0.5f, 0.5f, 0.5f));
//            w.progressBarSquare.Apply();

//            w.whiteSquare = new Texture2D(1, 1, TextureFormat.ARGB32, false);
//            w.whiteSquare.SetPixel(0, 0, new Color(1, 1, 1));
//            w.whiteSquare.Apply();
//        }

//        void RegenTexture(int previewSize)
//        {
//            tex = new Texture2D(previewSize, previewSize);
//            noiseValues = new float[tex.width * tex.height];
//            colors = new Color[tex.width * tex.height];
//        }

//        private void OnGUI()
//        {
//            // show texture
//            var textureRect = new Rect(0, 0, this.position.width, this.position.height);
//            GUI.DrawTexture(textureRect, tex, ScaleMode.ScaleToFit);

//            GUIContent timeLabel = new GUIContent("Calc time: " + calculationTime.ToString("F4"));
//            var calcTimeRect = new Rect(new Vector2(10, 10), GUI.skin.label.CalcSize(timeLabel));
//            GUI.DrawTexture(calcTimeRect, blackSquare);
//            var progressBarRect = calcTimeRect;
//            progressBarRect.width *= progressBar;
//            GUI.DrawTexture(progressBarRect, progressBarSquare);
//            GUI.Label(calcTimeRect, timeLabel);

//            var normalizeRect = calcTimeRect;
//            normalizeRect.y += calcTimeRect.height + 4;
//            var newNormalize = GUI.Toggle(normalizeRect, normalize, "Normalize");
//            if (newNormalize != normalize)
//            {
//                normalize = newNormalize;
//                Generate();
//            }

//            var coroutineRect = normalizeRect;
//            coroutineRect.y += normalizeRect.height + 4;
//            var newCoroutine = GUI.Toggle(coroutineRect, coroutine, "Progressive");
//            if (newCoroutine != coroutine)
//            {
//                coroutine = newCoroutine;
//                Generate();
//            }

//            // draw scale unit
//            var scaleRect = coroutineRect;
//            scaleRect.y += coroutineRect.height + 4;
//            GUI.Label(scaleRect, FREQUENCY_SCALE + " Unity meters");
//            scaleRect.y += scaleRect.height + 4;
//            scaleRect.height = 1f;
//            scaleRect.width = /*Mathf.Min(this.position.width, this.position.height) / PREVIEW_SIZE **/ FREQUENCY_SCALE;
//            GUI.DrawTexture(scaleRect, whiteSquare);
//            GUI.DrawTexture(new Rect(scaleRect.x, scaleRect.y - 4, 1f, 8), whiteSquare);
//            GUI.DrawTexture(new Rect(scaleRect.x + scaleRect.width - 1f, scaleRect.y - 4, 1f, 8), whiteSquare);

//            var e = Event.current;
//            if (e.isMouse && e.clickCount == 2 && textureRect.Contains(e.mousePosition) && e.type == EventType.MouseDown)
//            {
//                mapDragOffset = new Vector2(0, 0);
//                var p = this.position;
//                if (p.width == PREVIEW_SIZE)
//                {
//                    RegenTexture(PREVIEW_SIZE * 2);
//                    Generate();
//                    p.width = tex.width;
//                    p.height = tex.height;
//                }
//                else
//                {
//                    RegenTexture(PREVIEW_SIZE);
//                    Generate();
//                    p.width = tex.width;
//                    p.height = tex.height;
//                }
//                p.y -= 5;
//                this.position = p;
//            }

//            // drag and drop shit
//            if (e.isMouse && e.type == EventType.mouseDrag && e.button == 0)
//            {
//                mapDragOffset += e.delta;
//                Generate();
//            }

//            if (e.isMouse && e.type == EventType.MouseDown && e.button == 1)
//            {
//                mapDragOffset = Vector2.zero;
//                Generate();
//            }

//        }

//        public void Generate()
//        {
//            if (coroutine)
//            {
//                if (currentGenerationRoutine != null)
//                {
//                    currentGenerationRoutine.Stop();
//                }
//                currentGenerationRoutine = EditorCoroutine.Start(GenerateOverTime());
//                return;
//            }

//            var initTime = System.DateTime.Now;
//            var w = tex.width;
//            var h = tex.height;
//            var w2 = -w * 0.5f - mapDragOffset.x;
//            var h2 = -h * 0.5f + mapDragOffset.y;

//            for (int x = 0; x < w; x++)
//            {
//                for (int y = 0; y < h; y++)
//                {
//                    var i = x + y * w;
//                    var noiseValue = noise.GetNoise(x + w2, y + h2);
//                    if (normalize)
//                    {
//                        var g = noiseValue;
//                        noiseValues[i] = g;
//                    }
//                    else
//                    {
//                        var g = (noiseValue + 1f) * 0.5f;
//                        colors[i] = new Color(g, g, g);
//                    }
//                }
//            }

//            if (normalize)
//            {
//                var max = noiseValues.Max();
//                var min = noiseValues.Min();
//                for (int x = 0; x < tex.width; x++)
//                {
//                    for (int y = 0; y < tex.height; y++)
//                    {
//                        var i = x + y * tex.width;
//                        var normalized = Mathf.InverseLerp(min, max, noiseValues[i]);
//                        colors[i] = new Color(normalized, normalized, normalized);
//                    }
//                }
//            }

//            var deltaTime = System.DateTime.Now.Subtract(initTime);
//            calculationTime = (float)deltaTime.TotalSeconds;

//            tex.SetPixels(colors);
//            tex.Apply();

//        }

//        private IEnumerator GenerateOverTime()
//        {
//            yield return 0;

//            // every step should generate a small portion of pixels based on the time it takes to compute
//            var initTime = System.DateTime.Now;
//            TimeSpan deltaTime;
//            var w = tex.width;
//            var h = tex.height;
//            var wxh = w * h;
//            var w2 = -w * 0.5f - mapDragOffset.x;
//            var h2 = -h * 0.5f + mapDragOffset.y;

//            // calculate steps by evaluating how long it takes to calculate 100 pixels, then assuming linear increment for w * h pixels, and forcing a max time per frame.
//            for (int i = 0; i < 1000; i++)
//            {
//                // random operations just for the sake of wasting time so we can calculate totalTime. yeah pretty fucked up wasting cycles but csf
//#pragma warning disable 0219 // Variable is assigned but never used
//                var test = noise.GetNoise(0 - i, i - 2) + 1 * 0.5f;
//#pragma warning restore 0219 // Variable is assigned but never used

//            }
//            deltaTime = DateTime.Now.Subtract(initTime);
//            var totalTimeMillis = deltaTime.TotalMilliseconds * w * h;
//            if (totalTimeMillis / 1000000.0 > MAX_TIME_PER_FRAME)
//            {
//                stepsOverTime = Mathf.Max(1, (int)(totalTimeMillis / MAX_TIME_PER_FRAME / 1000000.0));
//            }
//            int pixelsPerStep = w * h / stepsOverTime;
//            int pixelsThisStep = 0;

//            progressBar = 0f;

//            for (int step = 0; step < stepsOverTime; step++)
//            {
//                for (int i = step; i < wxh; i += stepsOverTime)
//                {
//                    var x = i % w;
//                    var y = i / w;

//                    var noiseValue = noise.GetNoise(x + w2, y + h2);
//                    var g = (noiseValue + 1f) * 0.5f;
//                    colors[i] = new Color(g, g, g);

//                    if (pixelsThisStep++ > pixelsPerStep)
//                    {
//                        pixelsThisStep = 0;

//                        deltaTime = System.DateTime.Now.Subtract(initTime);
//                        calculationTime = (float)deltaTime.TotalSeconds;

//                        progressBar = calculationTime / (float)totalTimeMillis / 1000f;

//                        tex.SetPixels(colors);
//                        tex.Apply();
//                        this.Repaint();

//                        yield return 0;
//                    }

//                }

//            }

//            deltaTime = System.DateTime.Now.Subtract(initTime);
//            calculationTime = (float)deltaTime.TotalSeconds;
//            progressBar = 1f;

//            tex.SetPixels(colors);
//            tex.Apply();
//            this.Repaint();


//            yield break;
//        }



//    }

}