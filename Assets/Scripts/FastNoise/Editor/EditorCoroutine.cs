namespace FastNoiseEditor
{
    using UnityEditor;
    using System.Collections;

    // From: https://gist.github.com/benblo/10732554
    public class EditorCoroutine
    {
        public static EditorCoroutine Start(IEnumerator routine)
        {
            EditorCoroutine coroutine = new EditorCoroutine(routine);
            coroutine.Start();
            return coroutine;
        }

        private readonly IEnumerator _routine;

        EditorCoroutine(IEnumerator routine)
        {
            this._routine = routine;
        }

        void Start()
        {
            EditorApplication.update += Update;
        }

        public void Stop()
        {
            EditorApplication.update -= Update;
        }

        void Update()
        {
            if (!_routine.MoveNext())
            {
                Stop();
            }
        }
    }
}