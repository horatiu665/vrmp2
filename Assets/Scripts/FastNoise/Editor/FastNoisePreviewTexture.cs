namespace FastNoiseEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEditor;
    using UnityEngine;

    public class FastNoisePreviewTexture
    {
        private static int stepsOverTime = 1;
        const double MAX_TIME_PER_FRAME = 0.05;
		
        private FastNoise noise = null;
        private Texture2D tex;

        private float[] noiseValues;

        private Color[] colors;

        private float calculationTime;

        private Texture2D whiteSquare;
        private Texture2D blackSquare;
        private Texture2D progressBarSquare;

        private bool normalize = false;
        private bool coroutine = true;

        private float progressBar = 0f;

        private Vector2 mapDragOffset;

        EditorCoroutine currentGenerationRoutine;

        private System.Action yieldCallback;

        private static float frequencyScale = 1f;

        public FastNoisePreviewTexture(FastNoise noise, int previewSize, System.Action yieldCallback)
        {
            this.noise = noise;
            this.yieldCallback = yieldCallback;

            if (tex == null)
            {
                RegenTexture(previewSize);
            }
            Generate();

            blackSquare = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            blackSquare.SetPixel(0, 0, new Color(0, 0, 0, 0.4f));
            blackSquare.Apply();

            progressBarSquare = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            progressBarSquare.SetPixel(0, 0, new Color(0.5f, 0.5f, 0.5f, 0.5f));
            progressBarSquare.Apply();

            whiteSquare = new Texture2D(1, 1, TextureFormat.ARGB32, false);
            whiteSquare.SetPixel(0, 0, new Color(1, 1, 1));
            whiteSquare.Apply();

        }

        public void OnGUI(Rect position)
        {
            // show texture
            var textureRect = position;
            GUI.DrawTexture(textureRect, tex, ScaleMode.ScaleToFit);

            GUIContent timeLabel = new GUIContent("Calc time: " + calculationTime.ToString("F4"));
            var calcTimeRect = new Rect(position.position + new Vector2(10, 10), GUI.skin.label.CalcSize(timeLabel));
            GUI.DrawTexture(calcTimeRect, blackSquare);
            var progressBarRect = calcTimeRect;
            progressBarRect.width *= progressBar;
            GUI.DrawTexture(progressBarRect, progressBarSquare);
            GUI.Label(calcTimeRect, timeLabel);

            var normalizeRect = calcTimeRect;
            normalizeRect.y += calcTimeRect.height + 4;
            var newNormalize = GUI.Toggle(normalizeRect, normalize, "Normalize");
            if (newNormalize != normalize)
            {
                normalize = newNormalize;
                Generate();
            }

            var coroutineRect = normalizeRect;
            coroutineRect.y += normalizeRect.height + 4;
            var newCoroutine = GUI.Toggle(coroutineRect, coroutine, "Progressive");
            if (newCoroutine != coroutine)
            {
                coroutine = newCoroutine;
                Generate();
            }

            // draw scale unit
            var scaleRect = coroutineRect;
            scaleRect.y += coroutineRect.height + 4;
            var freqDivide = 1f;
            if (frequencyScale > 0 && !float.IsNaN(frequencyScale) && !float.IsInfinity(frequencyScale))
            {
                while (freqDivide / frequencyScale <= 10f)
                {
                    freqDivide *= 10f;
                }
                while (freqDivide / frequencyScale >= 100f)
                {
                    freqDivide /= 10f;
                }
            }

            GUI.Label(scaleRect, freqDivide + " units");
            scaleRect.y += scaleRect.height + 4;
            scaleRect.height = 1f;
            scaleRect.width = /*Mathf.Min(this.position.width, this.position.height) / PREVIEW_SIZE **/ freqDivide / frequencyScale;
            GUI.DrawTexture(scaleRect, whiteSquare);
            GUI.DrawTexture(new Rect(scaleRect.x, scaleRect.y - 4, 1f, 8), whiteSquare);
            GUI.DrawTexture(new Rect(scaleRect.x + scaleRect.width - 1f, scaleRect.y - 4, 1f, 8), whiteSquare);

            var e = Event.current;

            // drag and drop shit
            if (e.isMouse && e.type == EventType.MouseDrag && e.button == 0)
            {
                mapDragOffset += e.delta * frequencyScale;
                Generate();
            }

            if (e.isMouse && e.type == EventType.MouseDown && e.button == 1)
            {
                mapDragOffset = Vector2.zero;
                frequencyScale = 1f;
                Generate();
            }

            if (e.type == EventType.ScrollWheel && textureRect.Contains(e.mousePosition))
            {
                var changeAmount = 0.25f;
                var inverseChangeAmount = changeAmount / (1f + changeAmount);
                var sign = Mathf.Sign(e.delta.y);
                if (sign > 0)
                {
                    frequencyScale += frequencyScale * changeAmount;
                }
                else if (sign < 0)
                {
                    frequencyScale -= frequencyScale * inverseChangeAmount;
                }

                Generate();

                Event.current.Use();

            }
        }

        void RegenTexture(int previewSize)
        {
            tex = new Texture2D(previewSize, previewSize);
            noiseValues = new float[tex.width * tex.height];
            colors = new Color[tex.width * tex.height];
        }

        public void Generate()
        {
            if (coroutine)
            {
                if (currentGenerationRoutine != null)
                {
                    currentGenerationRoutine.Stop();
                }
                currentGenerationRoutine = EditorCoroutine.Start(GenerateOverTime());
                return;
            }

            var initTime = System.DateTime.Now;
            var w = tex.width;
            var h = tex.height;
            var w2 = -w * 0.5f * frequencyScale - mapDragOffset.x;
            var h2 = -h * 0.5f * frequencyScale + mapDragOffset.y;

            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    var i = x + y * w;
                    var noiseValue = noise.GetNoise(x * frequencyScale + w2, y * frequencyScale + h2);
                    if (normalize)
                    {
                        var g = noiseValue;
                        noiseValues[i] = g;
                    }
                    else
                    {
                        var g = (noiseValue + 1f) * 0.5f;
                        colors[i] = new Color(g, g, g);
                    }
                }
            }

            if (normalize)
            {
                var max = noiseValues.Max();
                var min = noiseValues.Min();
                for (int x = 0; x < tex.width; x++)
                {
                    for (int y = 0; y < tex.height; y++)
                    {
                        var i = x + y * tex.width;
                        var normalized = Mathf.InverseLerp(min, max, noiseValues[i]);
                        colors[i] = new Color(normalized, normalized, normalized);
                    }
                }
            }

            var deltaTime = System.DateTime.Now.Subtract(initTime);
            calculationTime = (float)deltaTime.TotalSeconds;

            tex.SetPixels(colors);
            tex.Apply();

            if (yieldCallback != null)
            {
                yieldCallback();
            }
        }

        private IEnumerator GenerateOverTime()
        {
            yield return 0;

            // every step should generate a small portion of pixels based on the time it takes to compute
            var initTime = System.DateTime.Now;
            TimeSpan deltaTime;
            var w = tex.width;
            var h = tex.height;
            var wxh = w * h;
            var w2 = -w * 0.5f * frequencyScale - mapDragOffset.x;
            var h2 = -h * 0.5f * frequencyScale + mapDragOffset.y;

            // calculate steps by evaluating how long it takes to calculate 100 pixels, then assuming linear increment for w * h pixels, and forcing a max time per frame.
            for (int i = 0; i < 1000; i++)
            {
                // random operations just for the sake of wasting time so we can calculate totalTime. yeah pretty fucked up wasting cycles but csf
#pragma warning disable 0219 // Variable is assigned but never used
                var test = noise.GetNoise(0 - i, i - 2) + 1 * 0.5f;
#pragma warning restore 0219 // Variable is assigned but never used

            }
            deltaTime = DateTime.Now.Subtract(initTime);
            var totalTimeMillis = deltaTime.TotalMilliseconds * w * h;
            if (totalTimeMillis / 1000000.0 > MAX_TIME_PER_FRAME)
            {
                stepsOverTime = Mathf.Max(1, (int)(totalTimeMillis / MAX_TIME_PER_FRAME / 1000000.0));
            }
            int pixelsPerStep = w * h / stepsOverTime;
            int pixelsThisStep = 0;

            progressBar = 0f;

            for (int step = 0; step < stepsOverTime; step++)
            {
                for (int i = step; i < wxh; i += stepsOverTime)
                {
                    var x = i % w;
                    var y = i / w;

                    var noiseValue = noise.GetNoise(x * frequencyScale + w2, y * frequencyScale + h2);
                    var g = (noiseValue + 1f) * 0.5f;
                    colors[i] = new Color(g, g, g);

                    if (pixelsThisStep++ > pixelsPerStep)
                    {
                        pixelsThisStep = 0;

                        deltaTime = System.DateTime.Now.Subtract(initTime);
                        calculationTime = (float)deltaTime.TotalSeconds;

                        progressBar = calculationTime / (float)totalTimeMillis / 1000f;

                        tex.SetPixels(colors);
                        tex.Apply();
                        if (yieldCallback != null)
                        {
                            yieldCallback();
                        }

                        yield return 0;
                    }

                }

            }

            deltaTime = System.DateTime.Now.Subtract(initTime);
            calculationTime = (float)deltaTime.TotalSeconds;
            progressBar = 1f;

            tex.SetPixels(colors);
            tex.Apply();
            if (yieldCallback != null)
            {
                yieldCallback();
            }

            yield break;
        }


    }

}
