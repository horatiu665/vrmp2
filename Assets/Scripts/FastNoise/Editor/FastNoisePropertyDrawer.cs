namespace FastNoiseEditor
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEditor;
    using UnityEngine;
    using Random = UnityEngine.Random;

    [CustomPropertyDrawer(typeof(FastNoise))]
    public class FastNoisePropertyDrawer : PropertyDrawer
    {
        private const int PREVIEW_ROW = -2;
        private const int PREVIEW_SIZE = 256;

        private static bool _updateOnChange = true;

        private FastNoise noise;

        FastNoisePreviewTexture previewTexture = null;

        private int rows
        {
            get
            {
                if (noise != null)
                {
                    if (!noise.unityEditorFoldout)
                    {
                        return 1;
                    }
                    var typ = noise.GetNoiseType();
                    return 6 + (ShowFractal(typ) ? 4 : 0) + (ShowCellular(typ) ? 2 : 0) + (ShowInterp(typ) ? 1 : 0);
                }
                return 14;
            }
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            if (noise == null)
            {
                noise = fieldInfo.GetValue(property.serializedObject.targetObject) as FastNoise;
            }
            return base.GetPropertyHeight(property, label) * rows + GetNoisePreviewHeight();
        }

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (noise == null)
            {
                noise = fieldInfo.GetValue(property.serializedObject.targetObject) as FastNoise;
            }
            var curRow = 0;

            noise.unityEditorFoldout = EditorGUI.Foldout(GetRow(position, curRow++), noise.unityEditorFoldout, property.displayName);

            if (noise.unityEditorFoldout)
            {
                EditorGUI.indentLevel++;

                // UPDATE ON CHANGE?
                _updateOnChange = EditorGUI.Toggle(GetRow(position, curRow++), "Update Preview On Change", _updateOnChange);

                bool shouldUpdateWindow = false;

                // SEED
                var noiseSeed = noise.GetSeed();
                var leftRect = GetRow(position, curRow);
                leftRect.width = leftRect.width * 0.75f - 1;
                var rightRect = GetRow(position, curRow);
                rightRect.x += rightRect.width * 0.75f;
                rightRect.width *= 0.25f;
                curRow++;
                int seed = EditorGUI.IntField(leftRect, new GUIContent("Seed"), noiseSeed);
                if (seed != noiseSeed)
                {
                    noise.SetSeed(seed);
                    shouldUpdateWindow = true;
                }
                if (GUI.Button(rightRect, "Random"))
                {
                    noise.SetSeed(Random.Range(0, int.MaxValue));
                    shouldUpdateWindow = true;
                }

                // NOISE TYPE
                var noiseType = noise.GetNoiseType();
                var type = (FastNoise.NoiseType)EditorGUI.EnumPopup(GetRow(position, curRow++), new GUIContent("Noise Type"), noiseType);
                if (type != noiseType)
                {
                    noise.SetNoiseType(type);
                    shouldUpdateWindow = true;
                }

                // NOISE FREQ
                var noiseFreq = noise.GetFrequency();
                var freq = EditorGUI.FloatField(GetRow(position, curRow++), "Frequency", noiseFreq);
                if (freq != noiseFreq)
                {
                    noise.SetFrequency(freq);
                    shouldUpdateWindow = true;
                }

                if (ShowInterp(noise.GetNoiseType()))
                {
                    // NOISE INTERP TYPE
                    var noiseInterp = noise.GetInterp();
                    var interp = (FastNoise.Interp)EditorGUI.EnumPopup(GetRow(position, curRow++), new GUIContent("Interpolation"), noiseInterp);
                    if (interp != noiseInterp)
                    {
                        noise.SetInterp(interp);
                        shouldUpdateWindow = true;
                    }
                }

                // fractal shit
                if (ShowFractal(noise.GetNoiseType()))
                {
                    // NOISE fractal TYPE
                    var noiseFractalType = noise.GetFractalType();
                    var fractalType = (FastNoise.FractalType)EditorGUI.EnumPopup(GetRow(position, curRow++), new GUIContent("Fractal Type"), noiseFractalType);
                    if (fractalType != noiseFractalType)
                    {
                        noise.SetFractalType(fractalType);
                        shouldUpdateWindow = true;
                    }

                    // NOISE FRACTAL OCTAVES
                    var noiseFractalOctaves = noise.GetFractalOctaves();
                    var fractalOctaves = EditorGUI.IntField(GetRow(position, curRow++), new GUIContent("Octaves"), noiseFractalOctaves);
                    if (fractalOctaves != noiseFractalOctaves)
                    {
                        noise.SetFractalOctaves(fractalOctaves);
                        shouldUpdateWindow = true;
                    }

                    // NOISE FRACTAL lacunarity
                    var noiseFractalLacunarity = noise.GetFractalLacunarity();
                    var fractalLacunarity = EditorGUI.FloatField(GetRow(position, curRow++), new GUIContent("Lacunarity"), noiseFractalLacunarity);
                    if (fractalLacunarity != noiseFractalLacunarity)
                    {
                        noise.SetFractalLacunarity(fractalLacunarity);
                        shouldUpdateWindow = true;
                    }

                    // NOISE FRACTAL gain
                    var noiseFractalGain = noise.GetFractalGain();
                    var fractalGain = EditorGUI.FloatField(GetRow(position, curRow++), new GUIContent("Gain"), noiseFractalGain);
                    if (fractalGain != noiseFractalGain)
                    {
                        noise.SetFractalGain(fractalGain);
                        shouldUpdateWindow = true;
                    }
                }

                if (ShowCellular(noise.GetNoiseType()))
                {

                    // NOISE CELLULAR DISTANCE
                    var noiseCellularDist = noise.GetCellularDistanceFunction();
                    var cellularDist = (FastNoise.CellularDistanceFunction)EditorGUI.EnumPopup(GetRow(position, curRow++), new GUIContent("Cellular Distance"), noiseCellularDist);
                    if (cellularDist != noiseCellularDist)
                    {
                        noise.SetCellularDistanceFunction(cellularDist);
                        shouldUpdateWindow = true;
                    }

                    // NOISE RETURN TYPE
                    var noiseCellularReturnType = noise.GetCellularReturnType();
                    var cellularReturnType = (FastNoise.CellularReturnType)EditorGUI.EnumPopup(GetRow(position, curRow++), new GUIContent("Cellular Return Type"), noiseCellularReturnType);
                    if (cellularReturnType != noiseCellularReturnType)
                    {
                        noise.SetCellularReturnType(cellularReturnType);
                        shouldUpdateWindow = true;
                    }
                }

                // only show once we figure out wtf is gradient perturb and how to use it
                if (ShowGradientPerturb(noise.GetNoiseType()))
                {
                    // NOISE gradient perturb ampl
                    var noiseGradientAmpl = noise.GetGradientPerturbAmp();
                    var gradientAmpl = EditorGUI.FloatField(GetRow(position, curRow++), new GUIContent("Gradient Perturb Amplitude"), noiseGradientAmpl);
                    if (gradientAmpl != noiseGradientAmpl)
                    {
                        noise.SetGradientPerturbAmp(gradientAmpl);
                        shouldUpdateWindow = true;
                    }

                }


                // PREVIEW BUTTON
                //if (GUI.Button(GetRow(position, curRow++), "Preview Noise Window"))
                //{
                //    // create window
                //    FastNoisePreviewWindow.ShowWindow(noise);
                //}

                //if (_updateOnChange && shouldUpdateWindow)
                //{
                //    if (FastNoisePreviewWindow.lastVisibleWindow != null)
                //    {
                //        FastNoisePreviewWindow.lastVisibleWindow.Generate();
                //        FastNoisePreviewWindow.lastVisibleWindow.Repaint();
                //    }
                //    if (previewTexture != null)
                //    {
                //        previewTexture.Generate();
                //    }
                //}

                // NOISE TEXTURE PREVIEW
                if (previewTexture == null)
                {
                    previewTexture = new FastNoisePreviewTexture(noise, PREVIEW_SIZE, () =>
                    {
                        if (property != null)
                        {
                            if (property.serializedObject != null)
                            {
                                EditorUtility.SetDirty(property.serializedObject.targetObject);
                            }
                        }
                    });
                }
                var previewTexPosition = GetRow(position, PREVIEW_ROW);
                previewTexture.OnGUI(previewTexPosition);

                EditorGUI.indentLevel--;

            }
            else //  !foldout
            {
                previewTexture = null;
            }

            property.serializedObject.ApplyModifiedProperties();

        }

        private Rect GetRow(Rect position, int row)
        {
            var y = position.y;
            var previewSize = GetNoisePreviewHeight();
            var rowH = (position.height - previewSize) / rows;
            if (row == PREVIEW_ROW)
            {
                return new Rect(position.x, y + rowH * rows + 2, position.width, PREVIEW_SIZE);
            }
            else
            {
                return new Rect(position.x, y + rowH * row, position.width, rowH);
            }
        }

        private float GetNoisePreviewHeight()
        {
            return noise.unityEditorFoldout ? (PREVIEW_SIZE + 4) : 0;
        }

        private bool ShowFractal(FastNoise.NoiseType noiseType)
        {
            switch (noiseType)
            {
            case FastNoise.NoiseType.Value:
            case FastNoise.NoiseType.Perlin:
            case FastNoise.NoiseType.Simplex:
            case FastNoise.NoiseType.Cellular:
            case FastNoise.NoiseType.WhiteNoise:
            case FastNoise.NoiseType.Cubic:
            default:
                return false;
            case FastNoise.NoiseType.ValueFractal:
            case FastNoise.NoiseType.PerlinFractal:
            case FastNoise.NoiseType.SimplexFractal:
            case FastNoise.NoiseType.CubicFractal:
                return true;
            }
        }

        private bool ShowCellular(FastNoise.NoiseType noiseType)
        {
            switch (noiseType)
            {
            case FastNoise.NoiseType.Value:
            case FastNoise.NoiseType.Perlin:
            case FastNoise.NoiseType.Simplex:
            case FastNoise.NoiseType.WhiteNoise:
            case FastNoise.NoiseType.Cubic:
            case FastNoise.NoiseType.ValueFractal:
            case FastNoise.NoiseType.PerlinFractal:
            case FastNoise.NoiseType.SimplexFractal:
            case FastNoise.NoiseType.CubicFractal:
            default:
                return false;
            case FastNoise.NoiseType.Cellular:
                return true;
            }
        }

        private bool ShowInterp(FastNoise.NoiseType noiseType)
        {
            switch (noiseType)
            {
            case FastNoise.NoiseType.Simplex:
            case FastNoise.NoiseType.SimplexFractal:
            case FastNoise.NoiseType.Cellular:
            case FastNoise.NoiseType.WhiteNoise:
            case FastNoise.NoiseType.Cubic:
            case FastNoise.NoiseType.CubicFractal:
                return false;
            case FastNoise.NoiseType.Value:
            case FastNoise.NoiseType.Perlin:
            case FastNoise.NoiseType.ValueFractal:
            case FastNoise.NoiseType.PerlinFractal:
            default:
                return true;
            }
        }

        private bool ShowGradientPerturb(FastNoise.NoiseType noiseType)
        {
            return false;
        }

    }

}