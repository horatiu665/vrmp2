namespace Core
{
    using System.Linq;
    using UnityEngine;
    using VRCore;

    public class Bow : MonoBehaviour
    {
        [Header("References setup")]
        /// <summary>
        /// This trigger is used to position objects that we might wanna shoot with the bow
        /// </summary>
        public Transform arrowRoot;

        public Transform arrowTip;

        [Space]
        [Header("Shooting params")]
        public AnimationCurve bowTensionToShootForce = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };

        public float bowMaxStretchDistance = 1f;

        public float shootForceMultiplier = 1f;

        public int colliderOffFrames = 5;

        private Arrow currentArrow = null;

        private float bowRestingDistance;
        private Vector3 arrowRootDefaultPos;

        // used for sounds
        [Header("Sounds")]
        public AudioSource soundStretch;

        public SmartSound soundShoot;
        public AnimationCurve deltaAimToBowStretchSoundPitch = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
        public AnimationCurve deltaAimToBowStretchSoundVol = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
        public float maxDeltaAim = 1f;
        private Vector3 oldAimPos;
        private float deltaAim = 0;

        private Collider[] bowColliders;
        private BowString bowString;

        public IVRHand hand;

        public void Grab(IVRHand hand)
        {
            this.hand = hand;
        }

        public void Ungrab()
        {
            hand = null;
            if (currentArrow != null)
            {
                CancelAim();
            }
        }

        public bool IsGrabbed()
        {
            return hand != null;
        }

        /// <summary>
        /// Called by BowAimTrigger.OnTriggerEnter therefore it can happen at random times even when an arrow is already being aimed.
        /// thus we need to have a few checks here.
        /// </summary>
        /// <param name="arr"></param>
        /// <param name="arrowBgbt"></param>
        public void StartAim(Arrow arr)
        {
            if (currentArrow == null)
            {
                currentArrow = arr;
                arr.StartAim(this);
                Aim();
            }
        }

        public void CancelAim()
        {
            currentArrow.CancelAim();
            arrowRoot.localPosition = arrowRootDefaultPos;
            oldAimPos = arrowRoot.position;
            currentArrow = null;
        }

        public void Shoot(Arrow arrow)
        {
            if (soundShoot != null)
            {
                soundShoot.Play();
            }

            arrowRoot.localPosition = arrowRootDefaultPos;
            oldAimPos = arrowRoot.position;
            currentArrow = null;

            SetColliders(false);
            StartCoroutine(pTween.WaitFixedFrames(colliderOffFrames, () =>
            {
                SetColliders(true);
            }));
        }

        private void Awake()
        {
            bowColliders = GetComponentsInChildren<Collider>().Where(c => !c.isTrigger).ToArray();
            bowString = GetComponentInChildren<BowString>();
            bowRestingDistance = (arrowTip.position - arrowRoot.position).magnitude;
            arrowRootDefaultPos = arrowRoot.localPosition;
            oldAimPos = arrowRootDefaultPos;
        }

        private void Update()
        {
            Aim();
            BowStretchSound();
        }

        private void BowStretchSound()
        {
            if (currentArrow != null)
            {
                var da = deltaAim / maxDeltaAim;
                soundStretch.pitch = deltaAimToBowStretchSoundPitch.Evaluate(da);
                soundStretch.volume = deltaAimToBowStretchSoundVol.Evaluate(da);
            }
            else
            {
                // fadeout
                soundStretch.volume *= 0.95f;
            }
        }

        public float GetForce(Arrow arrow)
        {
            // calculate percent of arrow pull (out of maximum allowed which is arrow length)
            var bowPullRaw = arrowTip.position - arrowRoot.position;

            // bow tension is zero when bow at resting position, bowRestingDistance. and 1 when bow at max arrow length.
            var bowTension = (Mathf.InverseLerp(bowRestingDistance, bowMaxStretchDistance, bowPullRaw.magnitude));

            // here we would add player velocity if player was moving, like in skijump or something
            var force = bowTensionToShootForce.Evaluate(bowTension) * shootForceMultiplier;

            return force;
        }

        private void SetColliders(bool active)
        {
            for (int i = 0; i < bowColliders.Length; i++)
            {
                // when active, collider is not trigger. and viceversa. setting "enabled" instead crashes unity
                bowColliders[i].isTrigger = !active;
            }
        }

        /// <summary>
        /// assumes all references are in place
        /// </summary>
        private void Aim()
        {
            if (currentArrow != null)
            {
                if (currentArrow.IsGrabbed())
                {
                    arrowRoot.position = currentArrow.hand.transform.position;
                    var arrowPreferredPos = currentArrow.PositionArrow(arrowRoot, arrowTip);
                    // perhaps un-grab arrow here on some condition.
                    arrowRoot.position = arrowPreferredPos;
                    bowString.SetArrow(arrowRoot);

                    deltaAim = (oldAimPos - arrowRoot.position).magnitude / Time.deltaTime;
                    oldAimPos = arrowRoot.position;

                    return;
                }
            }

            // if no arrow around, set string to resting position
            bowString.SetArrow(null);
        }
    }
}