namespace Core
{
    using UnityEngine;

    public class BowAimTrigger : MonoBehaviour
    {
        private Bow bow;

        private void Awake()
        {
            bow = GetComponentInParent<Bow>();
        }

        private void OnTriggerEnter(Collider other)
        {
            // if bow is in the hand
            if (bow.IsGrabbed())
            {
                // if we got a RB
                var r = other.attachedRigidbody;
                if (r != null)
                {
                    // if we got an Arrow
                    Arrow arr = r.GetComponent<Arrow>();
                    if (arr != null)
                    {
                        // start shooting other rb
                        if (arr.IsGrabbed() && arr.hand != bow.hand)
                        {
                            // we can position it by the grab point on the string
                            bow.StartAim(arr);
                        }
                    }
                }
            }
        }
    }
}