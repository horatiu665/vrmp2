namespace Core
{
    using UnityEngine;

    public class BowString : MonoBehaviour
    {
        /// <summary>
        /// line should be set up as a 3 point line. point[1] will always be repositioned based on arrow position
        /// </summary>
        private LineRenderer line;

        private void Awake()
        {
            line = GetComponent<LineRenderer>();
        }

        public void SetArrow(Transform arrowRoot)
        {
            if (arrowRoot != null)
            {
                line.SetPosition(1, line.transform.InverseTransformPoint(arrowRoot.position));
            }
            else
            {
                line.SetPosition(1, line.GetPosition(0));
            }
        }
    }
}