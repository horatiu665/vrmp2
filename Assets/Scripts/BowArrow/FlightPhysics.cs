namespace Core
{
    using UnityEngine;

    public class FlightPhysics : MonoBehaviour
    {
        [Header("References setup")]
        public Transform arrowRoot;

        public Transform arrowTip;

        [Header("Params")]

        /// <summary>
        /// Increases the forces applied to the arrow to keep its tip towards flight direction
        /// </summary>
        [Tooltip("Increases the forces applied to the arrow to keep its tip towards flight direction")]
        public float aerodynamics = 1f;

        /// <summary>
        /// Decay time. When set to zero, decay is not present.
        /// </summary>
        [Tooltip("Decay time. When set to zero, decay is not present.")]
        public float flightPhysicsDecayDuration = 5f;

        public float angularDragMax = 15f;
        public AnimationCurve angDragDecay = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 0, -2, 0) } };
        private float initAngularDrag = -1;

        private float currentFlightPhysics = 0;

        private Rigidbody r;

        private void Awake()
        {
            r = GetComponent<Rigidbody>();
        }

        private void FixedUpdate()
        {
            if (currentFlightPhysics > 0 || flightPhysicsDecayDuration == 0)
            {
                // t decreases over time, except when flightPhysicsDecayDuration is zero, then t is always 1
                float t;
                if (flightPhysicsDecayDuration > 0)
                {
                    t = currentFlightPhysics / flightPhysicsDecayDuration;
                }
                else
                {
                    t = 1;
                }

                var flightDir = r.velocity;
                var arrowDir = arrowTip.position - arrowRoot.position;
                var force = (flightDir - arrowDir * 0.5f) * t * aerodynamics;

                if (force.x != float.NaN && force.y != float.NaN && force.z != float.NaN)
                {
                    r.AddForceAtPosition(force, arrowTip.position);
                }
                if (force.x != float.NaN && force.y != float.NaN && force.z != float.NaN)
                {
                    r.AddForceAtPosition(-force, arrowRoot.position);
                }
                if (angularDragMax > 0)
                {
                    r.angularDrag = Mathf.Lerp(initAngularDrag, angularDragMax, angDragDecay.Evaluate(1f - t));
                }

                // not less than zero
                currentFlightPhysics = Mathf.Max(currentFlightPhysics - Time.fixedDeltaTime, 0);
            }
        }

        /// <summary>
        /// Called upon shooting or throwing something, to enable flight physics that decay over time.
        /// </summary>
        public void StartFlightPhysics(float decayDuration = -1)
        {
            // if drag uninitialized, init it
            if (initAngularDrag == -1)
            {
                initAngularDrag = r.angularDrag;
            }

            if (decayDuration == -1)
            {
                decayDuration = flightPhysicsDecayDuration;
            }

            currentFlightPhysics = decayDuration;
        }
    }
}