namespace Core
{
    using System;
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;

    /// <summary>
    /// Defines an object as an arrow. requires a rigidbody and minimal setup so it can be grabbed and shot with.
    /// Setup: rigidbody, collider, arrow root w/ forward towards tip, arrow tip (for measuring distance)
    /// </summary>
    [RequireComponent(typeof(INetEquippable))]
    public class Arrow : MonoBehaviour, ICanImpale, IOriginShifter
    {
        [Header("References setup")]
        public Transform arrowRoot;

        public Transform arrowTip;

        [Header("Shooting params")]
        public float colliderEnableDelay = 0;

        public bool IsGrabbed()
        {
            return hand != null;
        }

        [Header("Flight physics")]
        /// <summary>
        /// Increases the forces applied to the arrow to keep its tip towards flight direction
        /// </summary>
        [Tooltip("Increases the forces applied to the arrow to keep its tip towards flight direction")]
        public float aerodynamics = 1f;

        /// <summary>
        /// Multiplies angular velocity on shoot - f.x. less than 1 so the hand throw doesn't have so much impact on the arrow trajectory
        /// </summary>
        [Tooltip("Multiplies angular velocity on shoot - f.x. less than 1 so the hand throw doesn't have so much impact on the arrow trajectory")]
        public float angularVelocityShootMultiplier = 1f;

        /// <summary>
        /// Clamps av under this value to prevent weird shots (or allow them)
        /// </summary>
        [Tooltip("Clamps av under this value to prevent weird shots (or allow them)")]
        public float angularVelocityShootMax = 100f;

        /// <summary>
        /// Decay time. When set to zero, decay is not present.
        /// </summary>
        [Tooltip("Decay time. When set to zero, decay is not present.")]
        public float flightPhysicsDecayDuration = 5f;

        public float angularDragMax = 15f;
        public AnimationCurve angDragDecay = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 1, 0, 0), new Keyframe(1, 0, -2, 0) } };
        private float initAngularDrag = -1;

        private float currentFlightPhysics = 0;

        private Rigidbody rb;
        private Collider[] activeColliders;

        public bool flightPhysicsAfterShoot = true;

        public bool flightPhysicsAfterThrow = true;

        #region impale params

        [Header("Impale")]
        public bool canImpale = false;

        public ImpaleParams ip;

        private Vector3 curPos, prevPos;
        private Quaternion curRot, prevRot;

        private bool springWeak = false;

        // this is used only when the hand.getconnectedjoint does not return because the object already was dropped upon onunequip. the problem is we cannot revert the hand weakness if we don't cache it cause we don't know which joint should be set
        private ConfigurableJoint handJointCache;

        #endregion impale params

        //Vector3 oldCOM;
        private Bow aimingBow;

        private INetEquippable _equippable;

        public IVRHand hand;

        private Vector3 _handLocalPosOnEquip;

        // ICanImpale props
        Rigidbody ICanImpale.rigidbody
        {
            get { return rb; }
        }

        Collider[] ICanImpale.activeColliders
        {
            get { return activeColliders; }
        }

        Vector3 ICanImpale.prevPos
        {
            get { return prevPos; }
        }

        Quaternion ICanImpale.prevRot
        {
            get { return prevRot; }
        }

        ImpaleParams ICanImpale.impaleParameters
        {
            get { return ip; }
        }

        public float handPush { get; private set; }

        public void StartAim(Bow bow)
        {
            rb.isKinematic = true;
            SetColliders(false);
            aimingBow = bow;
        }

        public void OnEquipEvent(IVRHand grabHand)
        {
            if (canImpale)
            {
                Unimpale();
            }
            rb.isKinematic = false;
            this.hand = grabHand;
            _handLocalPosOnEquip = transform.InverseTransformPoint(hand.position);
        }

        /// <summary>
        /// on release, if aiming, shoots arrow
        /// </summary>
        public void OnUnequipEvent(IVRHand grabHand)
        {
            if (aimingBow != null)
            {
                var force = aimingBow.GetForce(this);
                Shoot(force);
            }
            else
            {
                // trigger a simple throw
                if (flightPhysicsAfterThrow)
                {
                    currentFlightPhysics = flightPhysicsDecayDuration;
                }
            }

            // revert weakness (at least in hand) because we will grab new shit
            SetHandWeakness(false);

            hand = null;
        }

        public void CancelAim()
        {
            rb.isKinematic = false;
            SetColliders(true);
            aimingBow = null;
        }

        public void Shoot(float force)
        {
            // add force based on the parameters of the bow
            rb.isKinematic = false;
            rb.AddForce(force * arrowRoot.forward, ForceMode.VelocityChange);

            if (flightPhysicsAfterShoot)
            {
                currentFlightPhysics = flightPhysicsDecayDuration;
            }
            rb.angularVelocity *= angularVelocityShootMultiplier;
            rb.angularVelocity = Vector3.ClampMagnitude(rb.angularVelocity, angularVelocityShootMax);

            // if drag uninitialized, init it
            if (initAngularDrag == -1)
            {
                initAngularDrag = rb.angularDrag;
            }

            StartCoroutine(pTween.Wait(colliderEnableDelay, () =>
            {
                SetColliders(true);
            }));

            aimingBow.Shoot(this);
            aimingBow = null;
        }

        /// <summary>
        /// positions arrow so that the root is at the rootTarget, and the tip is looking towards the tipTarget.
        /// Returns the clamped position of the arrow root (based on distance to tip)
        /// </summary>
        /// <param name="arrowRootTarget">where should the arrow root be</param>
        /// <param name="arrowTipTarget">where should arrow tip be</param>
        /// <returns>the clamped position of the arrow root (based on distance to tip)</returns>
        public Vector3 PositionArrow(Transform arrowRootTarget, Transform arrowTipTarget)
        {
            var arrowLength = (arrowTip.transform.position - arrowRoot.transform.position).magnitude;

            var dummyObjectForSmartRotations = VRHelper.dummy.transform;
            // save old parent
            var oldParent = transform.parent;
            // move dummy
            dummyObjectForSmartRotations.position = arrowRoot.position;
            dummyObjectForSmartRotations.rotation = arrowRoot.rotation;

            // transform is now child of dummy
            transform.SetParent(dummyObjectForSmartRotations);

            // move dummy (take arrow transform along)
            dummyObjectForSmartRotations.position = arrowRootTarget.position;
            dummyObjectForSmartRotations.LookAt(arrowTipTarget, arrowRootTarget.up);

            // clamp position to max arrow length
            dummyObjectForSmartRotations.position = arrowTipTarget.position - Vector3.ClampMagnitude(arrowTipTarget.position - arrowRootTarget.position, arrowLength);

            // set old parent for arrow
            transform.SetParent(oldParent);

            dummyObjectForSmartRotations.gameObject.SetActive(false);

            return arrowRoot.position;
        }

        private void Reset()
        {
            if (arrowRoot == null)
            {
                arrowRoot = new GameObject("ArrowRoot").transform;
                arrowRoot.SetParent(transform);
                arrowRoot.localPosition = Vector3.zero;
                arrowRoot.localRotation = Quaternion.identity;
                arrowRoot.localScale = Vector3.one;
            }

            if (arrowTip == null)
            {
                arrowTip = new GameObject("ArrowTip").transform;
                arrowTip.SetParent(transform);
                arrowTip.localPosition = Vector3.forward;
                arrowTip.localRotation = Quaternion.identity;
                arrowTip.localScale = Vector3.one;
            }
        }

        private void Awake()
        {
            rb = GetComponent<Rigidbody>();

            activeColliders = GetComponentsInChildren<Collider>(false);
            if (arrowRoot == null)
            {
                arrowRoot = transform;
            }

            _equippable = this.GetComponent<INetEquippable>();
        }

        private void OnEnable()
        {
            NetInputControllerDefault.OnUnequipMessageHook += NetInputControllerDefault_OnUnequipMessageHook;
            OriginShiftManager.OriginShiftersAdd(this);

            _equippable.OnEquipEvent += OnEquipEvent;
            _equippable.OnUnequipEvent += OnUnequipEvent;

            //gg = DebugInGame.DrawVisibleGizmo(Color.green);
        }

        private void OnDisable()
        {
            ImpalementHandler.Unimpale(this, true);
            NetInputControllerDefault.OnUnequipMessageHook -= NetInputControllerDefault_OnUnequipMessageHook;
            OriginShiftManager.OriginShiftersRemove(this);

            _equippable.OnEquipEvent -= OnEquipEvent;
            _equippable.OnUnequipEvent -= OnUnequipEvent;
        }

        public void OnWorldMove(Vector3 delta)
        {
            // we must destroy the joint, move the object, then recreate it.
            // first save impalement data....

            transform.position -= delta;

            ImpalementHandler.Reimpale(this);
            // set the same value on the weakness of the joint
            SetImpaleJointWeakness(springWeak);

        }

        private void FixedUpdate()
        {
            FlightPhysics();

            UpdateSpringGrabSettings();

            HandPushUnimpaleCheck();

            prevPos = curPos;
            curPos = rb.position;
            prevRot = curRot;
            curRot = rb.rotation;
        }
        //GameObject gg;
        private void HandPushUnimpaleCheck()
        {
            // calculate handpush
            if (IsGrabbed())
            {
                // hand pos vs grab point
                // need global space to avoid scaling differences among arrows
                var handOnEquip = transform.TransformPoint(_handLocalPosOnEquip);
                var gripToHandDistance = Vector3.Dot(hand.position - handOnEquip, ip.impaleForward.forward);
                handPush = gripToHandDistance / ip.handPushMaxDistance;

                //DebugInGame.DrawVisibleGizmo(gg, hand.position, handOnEquip - hand.position);

                // if handpush smaller than ip.handPushForDepenetration, we are trying to pull the arrow from the impale point. therefore we should unimpale.
                if (handPush < ip.handPushForUnimpale)
                {
                    // unimpale!!!
                    Unimpale();
                }
            }
        }

        private void UpdateSpringGrabSettings()
        {
            if (!canImpale)
            {
                return;
            }

            if (IsGrabbed())
            {
                // check it every frame if grabbed, cause of unpredictable player
                SetImpaleJointWeakness(true);
            }
            else
            {
                // only do this once if not grabbed.
                if (springWeak)
                {
                    SetImpaleJointWeakness(false);
                }
            }
        }

        public void SetImpaleJointWeakness(bool weak)
        {
            springWeak = weak;

            // if arrow is impaled rn
            if (ip.impaleJoint != null)
            {
                var sd = ip.impaleJoint.slerpDrive;
                var targetValue = weak ? 0 : ip.jointSpringStrength;
                // minor optimize..?
                if (sd.positionSpring != targetValue)
                {
                    sd.positionSpring = targetValue;
                    ip.impaleJoint.slerpDrive = sd;
                    SetHandWeakness(weak); // when arrow is impaled, and we try to make joint weak, make hand weak too. always weak together.
                }
            }
            else
            {
                // if arrow is not impaled, then make the hand strong.
                SetHandWeakness(false);
            }
        }

        public void SetHandWeakness(bool weak)
        {
            if (hand != null)
            {
                // tweak values on cur grabbing joint
                var handJoint = hand.GetConnectedJoint(rb);
                if (handJoint == null)
                {
                    // fuck! the joint is null cause we already dropped. use the cache that we set previously.
                    handJoint = handJointCache;
                }
                if (handJoint != null)
                {
                    // hand joint is not null! we can and should cache it, for when we drop the object and we have no joint to tweak.
                    handJointCache = handJoint;
                    var sd = handJoint.slerpDrive;
                    var targetValue = weak ? 0 : hand.player.handJointSpringStrength;
                    // minor optimize..?
                    if (sd.positionSpring != targetValue)
                    {
                        sd.positionSpring = targetValue;
                        handJoint.slerpDrive = sd;
                    }
                }
            }
        }

        private void NetInputControllerDefault_OnUnequipMessageHook(INetEquippable equippable, UnequipMessage msg)
        {
            if (equippable != _equippable)
            {
                return;
            }

            msg.position = rb.position;
            msg.rotation = rb.rotation;
            var vel = rb.velocity;
            var serverOffsetDistance = GhettoTimeSync.instance.serverLag * 2 * vel.magnitude;
            RaycastHit hitInfo;
            if (!Physics.Raycast(rb.position, vel, out hitInfo, serverOffsetDistance))
            {
                msg.position += vel * GhettoTimeSync.instance.serverLag * 2;
            }
            else
            {
                // if found rb is actually just the throwed object, do the same thing.
                if (hitInfo.collider.attachedRigidbody != null)
                {
                    if (hitInfo.collider.attachedRigidbody == rb)
                    {
                        msg.position += vel * GhettoTimeSync.instance.serverLag * 2;
                    }
                }
            }
            msg.velocity = vel;
            msg.angularVelocity = rb.angularVelocity;
        }

        private void FlightPhysics()
        {
            if (rb.isKinematic)
                return;

            if (currentFlightPhysics > 0 || flightPhysicsDecayDuration == 0)
            {
                // t decreases over time, except when flightPhysicsDecayDuration is zero, then t is always 1
                float t;
                if (flightPhysicsDecayDuration > 0)
                {
                    t = currentFlightPhysics / flightPhysicsDecayDuration;
                }
                else
                {
                    t = 1;
                }

                var flightDir = rb.velocity;
                var arrowDir = arrowTip.position - arrowRoot.position;
                //var arrowUp = Vector3.Cross(arrowDir, flightDir);
                //var arrowRight = Vector3.Cross(arrowDir, arrowUp);

                // force on tail: opposite of flightDir
                // force on head: same dir as flightDir
                // when aligned, forces cancel out. when perpendicular, they turn the arrow towards flightDir

                rb.AddForceAtPosition((flightDir - arrowDir * 0.5f) * t * aerodynamics, arrowTip.position);
                rb.AddForceAtPosition((-flightDir + arrowDir * 0.5f) * t * aerodynamics, arrowRoot.position);
                rb.angularDrag = Mathf.Lerp(initAngularDrag, angularDragMax, angDragDecay.Evaluate(1f - t));

                // not less than zero
                currentFlightPhysics = Mathf.Max(currentFlightPhysics - Time.fixedDeltaTime, 0);
            }
        }

        private void SetColliders(bool active)
        {
            for (int i = 0; i < activeColliders.Length; i++)
            {
                activeColliders[i].enabled = active;
            }
        }

        #region impale methods

        /// <summary>
        /// Impales arrow and makes appropriate sound.
        /// </summary>
        private void Impale(Collision collision)
        {
            // we reset spring weak cause the joint is fresh. this will make the update function run and weaken the joint on collision ;)
            springWeak = false;
            ImpalementHandler.Impale(this, collision.collider, collision.relativeVelocity, collision.contacts[0].point);
        }

        public void Unimpale()
        {
            SetHandWeakness(false);
            ImpalementHandler.Unimpale(this);
        }

        private void OnCollisionEnter(Collision c)
        {
            if (canImpale)
            {
                // if not impaled yet heheheh
                if (ip.impaleJoint == null)
                {
                    // check if arrow has flight mode physics, and if it can be impaled in this situation
                    if ((currentFlightPhysics > 0 && ip.impaleOnlyWhileFlying) || !ip.impaleOnlyWhileFlying)
                    {
                        // only a specific collider?
                        if (ip.impaleCollider != null)
                        {
                            if (c.contacts[0].thisCollider != ip.impaleCollider)
                            {
                                return;
                            }
                        }

                        // only a specific direction?
                        if (ip.impaleForward != null)
                        {
                            // if abs value of the projection of the relative velocity on the forward vector is smaller than impact force
                            // then the force component in the correct direction was not strong enough for an impale.
                            if ((Vector3.Dot(-c.relativeVelocity, ip.impaleForward.forward)) < ip.minImpaleForce)
                            {
                                return;
                            }
                        }

                        // impale at previous frame position because of possible unity physics one-frame delay
                        Impale(c);
                    }
                }
            }
        }

        private void OnJointBreak(float breakForce)
        {
            if (canImpale)
            {
                Unimpale();
            }
        }
        #endregion impale methods
    }
}