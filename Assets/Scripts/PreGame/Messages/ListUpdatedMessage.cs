﻿namespace Apex.Messages
{
    using System.Collections.Generic;

    public class ListUpdatedMessage<T>
    {
        public ListUpdatedMessage(IEnumerable<T> list)
        {
            this.list = list;
        }

        public IEnumerable<T> list { get; private set; }
    }
}
