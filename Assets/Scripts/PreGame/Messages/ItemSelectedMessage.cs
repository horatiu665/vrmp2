﻿namespace Apex.Messages
{
    public class ItemSelectedMessage<T>
    {
        public T item { get; set; }
    }
}
