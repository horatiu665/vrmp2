﻿namespace Apex.Messages
{
    using System.Collections.Generic;

    public class ItemUpdatedMessage<T>
    {
        public ItemUpdatedMessage(T item)
            : this(item, null)
        {
        }

        public ItemUpdatedMessage(T item, IEnumerable<T> list)
        {
            this.item = item;
            this.list = list;
            index = -1;
        }

        public T item { get; set; }

        public IEnumerable<T> list { get; set; }

        public int index { get; set; }
    }
}
