﻿namespace Apex.Messages
{
    using Steam;

    public class GameStartingMessage
    {
        public GameStartingMessage(HostAddress host, IGameSettings settings)
        {
            this.host = host;
            this.settings = settings;
        }

        public HostAddress host { get; set; }

        public IGameSettings settings { get; set; }
    }
}
