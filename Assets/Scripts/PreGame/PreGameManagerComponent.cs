﻿namespace Apex.Steam
{
    using Apex.Services;
    using Messages;
    using Steamworks;
    using UI.Steam;
    using UnityEngine;
    using VRCore;

    public class PreGameManagerComponent : SingletonMonoBehaviour<PreGameManagerComponent>, IHandleMessage<ItemSelectedMessage<Lobby>>
    {
        private PreGameState _state;
        private GameObject _currentUI;

        private Lobby _selectedLobby;

        public GameObject multiplayerUI;
        public GameObject lobbyListUI;
        public GameObject lobbyUI;
        public GameObject inviteFriendsUI;

        public bool canInviteFriends
        {
            get
            {
                return _selectedLobby != null && (_selectedLobby.isOwner || _selectedLobby.type == ELobbyType.k_ELobbyTypePublic);
            }
        }

        protected override void Awake()
        {
            base.Awake();

            multiplayerUI.SetActive(false);
            lobbyListUI.SetActive(false);
            lobbyUI.SetActive(false);
            inviteFriendsUI.SetActive(false);

            SetState(PreGameState.InMultiplaySelect);
        }

        private void OnEnable()
        {
            GameServices.messageBus.Subscribe<ItemSelectedMessage<Lobby>>(this);
        }

        private void OnDisable()
        {
            GameServices.messageBus.Unsubscribe<ItemSelectedMessage<Lobby>>(this);

            LeaveCurrentLobby();
        }

        private void OnDestroy()
        {
            Lobby.DisposeStatic();
        }

        public void CreateGame(bool isPrivate)
        {
            if (_selectedLobby != null)
            {
                if (!_selectedLobby.Leave())
                {
                    return;
                }
            }

            var name = string.Concat(SteamFriends.GetPersonaName(), "'s lobby");
            var lobbyType = isPrivate ? ELobbyType.k_ELobbyTypePrivate : ELobbyType.k_ELobbyTypePublic;
            Lobby.Create(name, lobbyType, 4, null, (lobby, result) =>
            {
                if (result != EResult.k_EResultOK)
                {
                    //TODO: do some error handling
                    Debug.Log("Create: " + result);
                    return;
                }

                _selectedLobby = lobby;
                SetState(PreGameState.InLobby);

                //TODO: create game
            });
        }

        public void FindGame()
        {
            SetState(PreGameState.InLobbySelect);
        }

        public void JoinGame()
        {
            if (_selectedLobby == null)
            {
                return;
            }

            _selectedLobby.Join((result) =>
            {
                if (result != EChatRoomEnterResponse.k_EChatRoomEnterResponseSuccess)
                {
                    //TODO: do handling of failure result
                    Debug.Log("Join: " + result);
                    return;
                }

                SetState(PreGameState.InLobby);

                //TODO: join game
            });
        }

        public void InviteFriends()
        {
            if (!this.canInviteFriends)
            {
                return;
            }

            SetState(PreGameState.InvitingFriends);
        }

        public void InviteFriends(FriendsListView source)
        {
            var invitees = source.GetSelected();
            foreach (var friend in invitees)
            {
                friend.InviteToLobby(_selectedLobby.id);
            }

            SetState(PreGameState.InLobby);
        }

        public void ReadyToStart()
        {
            if (_selectedLobby != null)
            {
                _selectedLobby.ToggleReadyState();
            }
        }

        public void Back()
        {
            switch (_state)
            {
                case PreGameState.InLobby:
                {
                    bool wasOwner = _selectedLobby.isOwner;

                    if (wasOwner)
                    {
                        SetState(PreGameState.InMultiplaySelect);
                    }
                    else
                    {
                        SetState(PreGameState.InLobbySelect);
                    }

                    break;
                }

                case PreGameState.InvitingFriends:
                {
                    SetState(PreGameState.InLobby);
                    break;
                }

                case PreGameState.InLobbySelect:
                {
                    SetState(PreGameState.InMultiplaySelect);
                    break;
                }

                case PreGameState.InMultiplaySelect:
                {
                    break;
                }
            }
        }

        void IHandleMessage<ItemSelectedMessage<Lobby>>.Handle(ItemSelectedMessage<Lobby> message)
        {
            if (_state == PreGameState.InLobbySelect)
            {
                _selectedLobby = message.item;
            }
        }

        private void SetState(PreGameState state)
        {
            _state = state;
            if (_currentUI != null)
            {
                _currentUI.SetActive(false);
            }

            switch (_state)
            {
                case PreGameState.InMultiplaySelect:
                {
                    LeaveCurrentLobby();
                    _currentUI = this.multiplayerUI;
                    break;
                }

                case PreGameState.InLobbySelect:
                {
                    LeaveCurrentLobby();
                    _currentUI = this.lobbyListUI;
                    break;
                }

                case PreGameState.InLobby:
                {
                    _currentUI = this.lobbyUI;
                    break;
                }

                case PreGameState.InvitingFriends:
                {
                    _currentUI = this.inviteFriendsUI;
                    break;
                }
            }

            _currentUI.SetActive(true);
        }

        private void LeaveCurrentLobby()
        {
            if (_selectedLobby != null)
            {
                _selectedLobby.Leave();
                _selectedLobby = null;
            }
        }
    }
}