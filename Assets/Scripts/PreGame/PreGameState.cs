﻿namespace Apex
{
    public enum PreGameState
    {
        InMultiplaySelect,
        InLobbySelect,
        InLobby,
        InvitingFriends,
        GameStarting
    }
}
