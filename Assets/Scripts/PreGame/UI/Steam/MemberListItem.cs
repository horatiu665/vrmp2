﻿namespace Apex.UI.Steam
{
    using Apex.Steam;
    using UnityEngine;
    using UnityEngine.UI;

    public class MemberListItem : ListItemComponent<Member>
    {
        public Image readyIndicator;
        public Text text;

        public override void Render()
        {
            if (text != null)
            {
                text.text = this.data.name;
            }

            if (readyIndicator != null)
            {
                readyIndicator.enabled = this.data.isReady;
            }
        }
    }
}
