﻿namespace Apex.UI.Steam
{
    using Apex.Steam;

    public class LobbyListView : ListViewComponent<Lobby>
    {
        private LobbyListing _lobbylisting;

        public int maxLobbyCount = 20;

        protected override void OnDisable()
        {
            base.OnDisable();

            if (_lobbylisting != null)
            {
                _lobbylisting.Dispose();
                _lobbylisting = null;
            }
        }

        protected override void GetItems(ListCallback callback)
        {
            if (_lobbylisting != null)
            {
                _lobbylisting.Dispose();
            }

            LobbyListing.Create(maxLobbyCount, (listing, error) =>
            {
                _lobbylisting = listing;

                if (error)
                {
                    callback(null, "There was an error retrieving the Lobby list, please try again in a moment.");
                }
                else if (listing != null)
                {
                    callback(listing.lobbies, null);
                }
            });
        }
    }
}
