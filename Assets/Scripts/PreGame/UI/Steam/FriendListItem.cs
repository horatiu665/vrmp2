﻿namespace Apex.UI.Steam
{
    using Apex.Steam;
    using UnityEngine.UI;

    public class FriendListItem : ListItemComponent<Friend>
    {
        public Text text;

        public override void Render()
        {
            if (text != null)
            {
                text.text = this.data.name;
            }
        }
    }
}
