﻿namespace Apex.UI.Steam
{
    using Apex.Steam;
    using UnityEngine;
    using UnityEngine.UI;

    public class LobbyListItem : ListItemComponent<Lobby>
    {
        private Text _text;

        protected override void Awake()
        {
            base.Awake();

            _text = GetComponentInChildren<Text>();
            if (_text == null)
            {
                Debug.LogWarning("Missing Text component: " + this.gameObject.name);
            }
        }

        public override void Render()
        {
            if (_text != null)
            {
                _text.text = this.data.name;
            }
        }
    }
}
