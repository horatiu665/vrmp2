﻿namespace Apex.UI.Steam
{
    using System;
    using Apex.Steam;
    using Messages;
    using Services;

    public class MemberListView : ListViewComponent<Member>, IHandleMessage<ListUpdatedMessage<Member>>
    {
        protected override void OnEnable()
        {
            base.OnEnable();

            //It should be noted that this only works when a single list of members is ever active. Should that not be the case it must be implemented differently.
            GameServices.messageBus.Subscribe<ListUpdatedMessage<Member>>(this);
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            GameServices.messageBus.Unsubscribe<ListUpdatedMessage<Member>>(this);
        }

        public void Handle(ListUpdatedMessage<Member> message)
        {
            Show(message.list);
        }
    }
}
