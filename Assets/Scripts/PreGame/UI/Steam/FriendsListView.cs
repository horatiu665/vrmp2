﻿namespace Apex.UI.Steam
{
    using Apex.Steam;

    public class FriendsListView : ListViewComponent<Friend>
    {
        protected override void GetItems(ListCallback callback)
        {
            callback(FriendsList.GetFriends(), null);
        }
    }
}
