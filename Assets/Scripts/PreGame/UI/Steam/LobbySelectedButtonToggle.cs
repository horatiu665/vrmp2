﻿namespace Apex.Steam
{
    using Messages;
    using Services;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public class LobbySelectedButtonToggle : UIBehaviour, IHandleMessage<ItemSelectedMessage<Lobby>>
    {
        private Button _btn;

        protected override void Awake()
        {
            _btn = GetComponent<Button>();
            if (_btn == null)
            {
                Debug.LogError("LobbySelectedButtonToggle must have a Button to work on!");
            }
        }

        protected override void OnEnable()
        {
            _btn.interactable = false;
            GameServices.messageBus.Subscribe(this);
        }

        protected override void OnDisable()
        {
            GameServices.messageBus.Unsubscribe(this);
        }

        void IHandleMessage<ItemSelectedMessage<Lobby>>.Handle(ItemSelectedMessage<Lobby> message)
        {
            _btn.interactable = (message.item != null);
        }
    }
}
