﻿namespace Apex.UI
{
    using System.Collections.Generic;
    using Messages;
    using Services;
    using UnityEngine;
    using UnityEngine.UI;

    public abstract class ListViewComponent<T> : ExtendedUIBehaviour, IHandleMessage<ItemUpdatedMessage<T>>
    {
        private IEnumerable<T> _dataList;
        private bool _valid;
        private Transform _contentTransform;
        private Text _messageText;

        public GameObject itemMold;
        public GameObject messageOverlay;

        public delegate void ListCallback(IEnumerable<T> list, string errorMessage);

        protected override void Awake()
        {
            var scrollRect = GetComponent<ScrollRect>();
            if (scrollRect == null)
            {
                Debug.LogWarning("Missing ScrollRect on ListView: " + this.gameObject.name);
                return;
            }

            _contentTransform = scrollRect.content;
            if (scrollRect == null)
            {
                Debug.LogWarning("Missing Content on ScrollRect: " + this.gameObject.name);
                return;
            }

            if (itemMold == null)
            {
                Debug.LogWarning("Missing itemMold: " + this.gameObject.name);
                return;
            }

            var lstItem = itemMold.GetComponent<IListItem<T>>();
            if (lstItem == null)
            {
                Debug.LogWarning("Missing IListItem implementation on itemMold: " + this.gameObject.name);
                return;
            }

            if (messageOverlay != null)
            {
                _messageText = messageOverlay.GetComponentInChildren<Text>(true);
            }

            _valid = true;
        }

        protected override void OnStartAndEnable()
        {
            GameServices.messageBus.Subscribe(this);

            if (_valid)
            {
                Refresh();
            }
        }

        protected override void OnDisable()
        {
            GameServices.messageBus.Unsubscribe(this);

            if (_valid)
            {
                Clear();
            }
        }

        public void Show(IEnumerable<T> list)
        {
            Clear();
            PopulateList(list, null);
        }

        public void Refresh()
        {
            Clear();
            ShowMessage("Loading...");
            GetItems(PopulateList);
        }

        public IList<T> GetSelected()
        {
            var list = new List<T>();
            var count = _contentTransform.childCount;
            for (int i = 0; i < count; i++)
            {
                var item = _contentTransform.GetChild(i).GetComponent<IListItem<T>>();
                if (item.selected)
                {
                    list.Add(item.data);
                }
            }

            return list;
        }

        void IHandleMessage<ItemUpdatedMessage<T>>.Handle(ItemUpdatedMessage<T> message)
        {
            if (message.index < 0 || message.list != _dataList)
            {
                return;
            }

            if (message.item == null)
            {
                RemoveItem(message.index);
                return;
            }

            var btn = _contentTransform.GetChild(message.index);
            if (btn == null)
            {
                return;
            }

            var lstItem = btn.GetComponent<IListItem<T>>();
            lstItem.data = message.item;

            lstItem.Render();
        }

        protected virtual void GetItems(ListCallback callback)
        {
            callback(_dataList, null);
        }

        private void Clear()
        {
            for (int i = _contentTransform.childCount - 1; i >= 0; i--)
            {
                Destroy(_contentTransform.GetChild(i).gameObject);
            }
        }

        private bool RemoveItem(int idx)
        {
            if (idx < 0 || idx >= _contentTransform.childCount)
            {
                return false;
            }

            Destroy(_contentTransform.GetChild(idx).gameObject);
            return true;
        }

        private void PopulateList(IEnumerable<T> items, string errorMessage)
        {
            if (!string.IsNullOrEmpty(errorMessage))
            {
                ShowMessage(errorMessage);
                return;
            }

            HideMessage();

            _dataList = items;
            if (_dataList == null)
            {
                return;
            }

            foreach (var item in items)
            {
                var btn = Instantiate(itemMold);
                btn.SetActive(true);

                var lstItem = btn.GetComponent<IListItem<T>>();
                lstItem.data = item;

                btn.transform.SetParent(_contentTransform, false);
                lstItem.Render();
            }
        }

        private void ShowMessage(string msg)
        {
            if (_messageText != null)
            {
                _messageText.text = msg;
                this.messageOverlay.SetActive(true);
            }
        }

        private void HideMessage()
        {
            if (_messageText != null)
            {
                _messageText.text = string.Empty;
                this.messageOverlay.SetActive(false);
            }
        }
    }
}
