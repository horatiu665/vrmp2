﻿namespace Apex.UI
{
    public interface IListItem<T>
    {
        T data { get; set; }

        bool selected { get; set; }

        void Render();
    }
}
