﻿namespace Apex.UI
{
    using Messages;
    using Services;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.UI;

    public abstract class ListItemComponent<T> : UIBehaviour, IListItem<T>
    {
        public GameObject selectedVisual;

        private Button _btn;
        private bool _selected;

        public T data
        {
            get;
            set;
        }

        public bool selected
        {
            get
            {
                return _selected;
            }

            set
            {
                if (_selected == value)
                {
                    return;
                }

                _selected = value;
                if (selectedVisual != null)
                {
                    selectedVisual.SetActive(_selected);
                }
            }
        }

        public abstract void Render();

        protected override void Awake()
        {
            _btn = GetComponent<Button>();

            if (_btn != null)
            {
                _btn.onClick.AddListener(OnClick);
            }
        }

        protected override void OnDestroy()
        {
            if (_btn != null)
            {
                _btn.onClick.RemoveAllListeners();
            }
        }

        protected virtual void OnClick()
        {
            this.selected = !this.selected;

            var msg = new ItemSelectedMessage<T>
            {
                item = this.data
            };

            GameServices.messageBus.Post(msg);
        }
    }
}
