﻿/* Copyright © 2014 Apex Software. All rights reserved. */
namespace Apex.UI
{
    using UnityEngine;
    using UnityEngine.EventSystems;

    /// <summary>
    /// MonoBehaviour extension that couples Start and OnEnable to ensure that certain initialization logic is performed in both instances but only once.
    /// </summary>
    public abstract class ExtendedUIBehaviour : UIBehaviour
    {
        private bool _hasStarted;

        /// <summary>
        /// Called on Start
        /// </summary>
        protected override void Start()
        {
            _hasStarted = true;
            OnStartAndEnable();
        }

        /// <summary>
        /// Called when enabled.
        /// </summary>
        protected override void OnEnable()
        {
            if (_hasStarted)
            {
                OnStartAndEnable();
            }
        }

        /// <summary>
        /// Called on Start and OnEnable, but only one of the two, i.e. at startup it is only called once.
        /// </summary>
        protected virtual void OnStartAndEnable()
        {
        }
    }
}
