﻿namespace Apex.Steam
{
    using System;

    public interface IGameHost
    {
        void StartHost(Action<HostAddress> callback);

        void ConnectToHost(HostAddress addr);
    }
}
