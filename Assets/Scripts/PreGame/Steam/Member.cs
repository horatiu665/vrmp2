﻿namespace Apex.Steam
{
    using Steamworks;

    public class Member
    {
        public Member(CSteamID id)
        {
            this.id = id;
        }

        public CSteamID id { get; private set; }

        public string name { get; set; }

        public bool isReady { get; set; }
    }
}
