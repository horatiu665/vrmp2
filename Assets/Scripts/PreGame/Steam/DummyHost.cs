﻿namespace Apex.Steam
{
    using System;
    using UnityEngine;

    public class DummyHost : IGameHost
    {
        public void ConnectToHost(HostAddress addr)
        {
            Debug.Log(addr);
        }

        public void StartHost(Action<HostAddress> callback)
        {
            callback(new HostAddress
            {
                IP = 1234,
                port = 123
            });
        }
    }
}
