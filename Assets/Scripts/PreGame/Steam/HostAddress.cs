﻿namespace Apex.Steam
{
    public struct HostAddress
    {
        public uint IP;
        public ushort port;

        public override string ToString()
        {
            return string.Concat("[HostAddress = (", this.IP, ":", this.port, ")]");
        }
    }
}