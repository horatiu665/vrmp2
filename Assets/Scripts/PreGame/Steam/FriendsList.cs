﻿namespace Apex.Steam
{
    using System.Collections.Generic;
    using Steamworks;

    public static class FriendsList
    {
        public static IList<Friend> GetFriends()
        {
            var friendType = EFriendFlags.k_EFriendFlagImmediate;
            var list = new List<Friend>();

            int friendsCount = SteamFriends.GetFriendCount(friendType);
            for (int i = 0; i < friendsCount; i++)
            {
                var friendId = SteamFriends.GetFriendByIndex(i, friendType);
                AddFriendToGroup(friendId, list);
            }

            return list;
        }

        public static IList<FriendsGroup> GetGroupedFriends()
        {
            var groupList = new List<FriendsGroup>();

            CSteamID[] idBuffer = new CSteamID[20];
            HashSet<CSteamID> taggedFriends = new HashSet<CSteamID>();

            //Groups are actually not groups, but tags
            int grpCount = SteamFriends.GetFriendsGroupCount();
            for (int i = 0; i < grpCount; i++)
            {
                var grpId = SteamFriends.GetFriendsGroupIDByIndex(i);
                if (grpId == FriendsGroupID_t.Invalid)
                {
                    continue;
                }

                var memberCount = SteamFriends.GetFriendsGroupMembersCount(grpId);
                if (memberCount == 0)
                {
                    continue;
                }

                var grpName = SteamFriends.GetFriendsGroupName(grpId);
                var grp = new FriendsGroup(grpName);

                if (idBuffer.Length < memberCount)
                {
                    idBuffer = new CSteamID[memberCount];
                }

                SteamFriends.GetFriendsGroupMembersList(grpId, idBuffer, memberCount);
                for (int j = 0; j < memberCount; j++)
                {
                    var memberId = idBuffer[j];
                    taggedFriends.Add(memberId);
                    AddFriendToGroup(memberId, grp.members);
                }

                groupList.Add(grp);
            }

            var defGrp = new FriendsGroup("Friends");
            AddFriendsByFlag(EFriendFlags.k_EFriendFlagImmediate, defGrp, taggedFriends);
            if (defGrp.members.Count > 0)
            {
                groupList.Add(defGrp);
            }

            return groupList;
        }

        public static IList<Friend> GetFriendsInLobby()
        {
            var friendType = EFriendFlags.k_EFriendFlagImmediate;
            var list = new List<Friend>();

            int friendsCount = SteamFriends.GetFriendCount(friendType);
            for (int i = 0; i < friendsCount; i++)
            {
                FriendGameInfo_t friendGameInfo;
                var friendId = SteamFriends.GetFriendByIndex(i, friendType);
                if (SteamFriends.GetFriendGamePlayed(friendId, out friendGameInfo) && friendGameInfo.m_steamIDLobby.IsValid())
                {
                    //friendGameInfo.m_gameID ?
                    // friendGameInfo.m_steamIDLobby is a valid lobby, you can join it or use RequestLobbyData() get it's metadata
                    AddFriendToGroup(friendId, list);
                }
            }

            return list;
        }

        private static void AddFriendsByFlag(EFriendFlags flag, FriendsGroup grp, HashSet<CSteamID> skip)
        {
            int friendsCount = SteamFriends.GetFriendCount(flag);
            if (friendsCount == 0)
            {
                return;
            }

            for (int i = 0; i < friendsCount; i++)
            {
                var friendId = SteamFriends.GetFriendByIndex(i, flag);

                // This mimicks the Steam client's feature where it only shows
                // untagged friends in the canonical Friends section by default
                if (!skip.Contains(friendId))
                {
                    AddFriendToGroup(friendId, grp.members);
                }
            }
        }

        private static void AddFriendToGroup(CSteamID friendId, IList<Friend> grp)
        {
            if (!friendId.IsValid())
            {
                return;
            }

            var name = SteamFriends.GetFriendPersonaName(friendId);

            var nickName = SteamFriends.GetPlayerNickname(friendId);
            if (!string.IsNullOrEmpty(nickName))
            {
                name = string.Concat(name, "(", nickName, ")");
            }

            grp.Add(new Friend(friendId, name));
        }

        public class FriendsGroup
        {
            public FriendsGroup(string name)
            {
                this.name = name;
                this.members = new List<Friend>();
            }

            public string name { get; private set; }

            public IList<Friend> members { get; private set; }
        }
    }
}
