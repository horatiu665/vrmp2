﻿namespace Apex.Steam
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Messages;
    using Services;
    using Steamworks;

    public class Lobby
    {
        private const string StateMessagePrefix = "#";
        private const string MemberStateUpdatedMessage = "#MU§";
        private const string MemberReadyKey = "ready";
        private const string SetValue = "1";
        private const string NotSetValue = "";

        private static CallResult<LobbyCreated_t> _createResult;

        private byte[] _chatOutBuffer = new byte[10];
        private byte[] _chatInBuffer = new byte[10];

        private CallResult<LobbyEnter_t> _enterResult;
        private Callback<LobbyChatUpdate_t> _lobbyMembersUpdated;
        private Callback<LobbyDataUpdate_t> _lobbyDataUpdated;
        private Callback<PersonaStateChange_t> _lobbyMemberDataUpdated;
        private Callback<LobbyChatMsg_t> _chatUpdated;
        private Callback<LobbyGameCreated_t> _gameReady;

        private string _name;
        private IGameSettings _settings;
        private IList<Member> _members;

        private Lobby(CSteamID id)
        {
            this.id = id;
        }

        public CSteamID id { get; private set; }

        public bool isJoined { get; private set; }

        public bool isOwner { get; private set; }

        public ELobbyType type { get; private set; }

        public int maxMembers { get; private set; }

        public string name
        {
            get
            {
                return _name;
            }

            set
            {
                if (this.isOwner)
                {
                    _name = value;
                    SteamMatchmaking.SetLobbyData(this.id, "name", _name);
                }
            }
        }

        public IGameSettings settings
        {
            get
            {
                if (_settings == null)
                {
                    //TODO: instantiate and init
                }

                return _settings;
            }

            private set
            {
                _settings = value;
            }
        }

        public IEnumerable<Member> members
        {
            get
            {
                return membersInternal;
            }
        }

        private IList<Member> membersInternal
        {
            get
            {
                if (_members != null && _members.Count > 0)
                {
                    return _members;
                }

                if (_members == null)
                {
                    _members = new List<Member>(this.maxMembers);
                }

                int memberCount = SteamMatchmaking.GetNumLobbyMembers(this.id);
                for (int i = 0; i < memberCount; i++)
                {
                    var memberId = SteamMatchmaking.GetLobbyMemberByIndex(this.id, i);

                    var member = new Member(memberId)
                    {
                        name = SteamFriends.GetFriendPersonaName(memberId),
                        isReady = (SteamMatchmaking.GetLobbyMemberData(this.id, memberId, MemberReadyKey) == SetValue)
                    };

                    _members.Add(member);
                }

                return _members;
            }
        }

        public static void DisposeStatic()
        {
            _createResult = null;
        }

        public static void Create(string name, ELobbyType type, int maxMembers, IGameSettings settings, Action<Lobby, EResult> callback)
        {
            if (_createResult == null)
            {
                _createResult = CallResult<LobbyCreated_t>.Create();
            }
            else if (_createResult.IsActive())
            {
                callback(null, EResult.k_EResultDuplicateRequest);
                return;
            }

            var req = SteamMatchmaking.CreateLobby(type, maxMembers);

            _createResult.Set(
                req,
                (lobbyData, error) =>
                {
                    if (error)
                    {
                        callback(null, EResult.k_EResultUnexpectedError);
                    }
                    else if (lobbyData.m_eResult != EResult.k_EResultOK)
                    {
                        callback(null, lobbyData.m_eResult);
                    }

                    var lobby = new Lobby(lobbyData.m_ulSteamIDLobby)
                    {
                        isOwner = true,
                        isJoined = true,
                        settings = settings,
                        type = type,
                        maxMembers = maxMembers
                    };

                    //Sequence here is important since we must set it to owner before setting the name.
                    lobby.name = name;

                    lobby.HookCallbacks();
                    callback(lobby, EResult.k_EResultOK);
                });

            SteamFriends.SetRichPresence("status", "Creating a lobby");
        }

        public static Lobby FromId(CSteamID id)
        {
            var lobby = new Lobby(id);
            lobby.Refresh();

            return lobby;
        }

        public void Refresh()
        {
            _name = SteamMatchmaking.GetLobbyData(this.id, "name");
            _settings = null;
        }

        public void Join(Action<EChatRoomEnterResponse> callback)
        {
            if (this.isJoined)
            {
                callback(EChatRoomEnterResponse.k_EChatRoomEnterResponseSuccess);
                return;
            }

            var req = SteamMatchmaking.JoinLobby(this.id);

            if (_enterResult == null)
            {
                _enterResult = CallResult<LobbyEnter_t>.Create();
            }
            else if (_enterResult.IsActive())
            {
                callback(EChatRoomEnterResponse.k_EChatRoomEnterResponseNotAllowed);
                return;
            }

            _enterResult.Set(
                req,
                (result, error) =>
                {
                    if (error)
                    {
                        callback(EChatRoomEnterResponse.k_EChatRoomEnterResponseError);
                        return;
                    }

                    var resultCode = (EChatRoomEnterResponse)result.m_EChatRoomEnterResponse;
                    isJoined = (resultCode == EChatRoomEnterResponse.k_EChatRoomEnterResponseSuccess);
                    if (isJoined)
                    {
                        HookCallbacks();
                    }

                    callback(resultCode);
                });
        }

        public bool Leave()
        {
            if (!isJoined)
            {
                return false;
            }
            
            if (SteamManager.Initialized)
            {
                SetReadyState(false);
                SteamMatchmaking.LeaveLobby(this.id);
            }

            isJoined = isOwner = false;

            if (_lobbyMembersUpdated != null)
            {
                _lobbyMembersUpdated.Unregister();
                _lobbyDataUpdated.Unregister();
                _lobbyMemberDataUpdated.Unregister();
                _chatUpdated.Unregister();
                _gameReady.Unregister();

                _lobbyMembersUpdated = null;
                _lobbyDataUpdated = null;
                _lobbyMemberDataUpdated = null;
                _chatUpdated = null;
                _gameReady = null;
            }

            return true;
        }

        public void ToggleReadyState()
        {
            if (!isJoined)
            {
                return;
            }

            var rdy = SteamMatchmaking.GetLobbyMemberData(this.id, SteamUser.GetSteamID(), MemberReadyKey) == SetValue;

            SetReadyState(!rdy);
            SendStatusMessage(MemberStateUpdatedMessage);
        }

        private void SetReadyState(bool ready)
        {
            SteamMatchmaking.SetLobbyMemberData(this.id, MemberReadyKey, ready ? SetValue : NotSetValue);
        }

        private void HookCallbacks()
        {
            _lobbyMembersUpdated = Callback<LobbyChatUpdate_t>.Create(OnLobbyMembersChanged);
            _lobbyDataUpdated = Callback<LobbyDataUpdate_t>.Create(OnLobbyDataChanged);
            _lobbyMemberDataUpdated = Callback<PersonaStateChange_t>.Create(OnLobbyMemberDataChanged);
            _chatUpdated = Callback<LobbyChatMsg_t>.Create(OnChatMessage);
            _gameReady = Callback<LobbyGameCreated_t>.Create(OnGameReady);
        }

        private void ResetMembers()
        {
            if (_members != null)
            {
                _members.Clear();
            }
        }

        private void OnLobbyDataChanged(LobbyDataUpdate_t data)
        {
            if (data.m_ulSteamIDLobby != this.id)
            {
                return;
            }

            Refresh();

            GameServices.messageBus.Post(new ItemUpdatedMessage<Lobby>(this));
        }

        private void OnLobbyMembersChanged(LobbyChatUpdate_t data)
        {
            if (data.m_ulSteamIDLobby != this.id)
            {
                return;
            }

            this.isOwner = (SteamMatchmaking.GetLobbyOwner(this.id) == SteamUser.GetSteamID());
            if (this.isOwner)
            {
                IGameHost host = new DummyHost(); //TODO: get a reference to this somehow. Again injection would come in handy.
                host.StartHost((hostAddress) =>
                {
                    SteamMatchmaking.SetLobbyGameServer(this.id, hostAddress.IP, hostAddress.port, 0L);
                });
            }
        }

        private void OnLobbyMemberDataChanged(PersonaStateChange_t data)
        {
            if (!SteamFriends.IsUserInSource(data.m_ulSteamID, this.id))
            {
                return;
            }

            //TODO: handle data.m_nChangeFlags specifically since some changes are irrelevant
            ResetMembers();
            GameServices.messageBus.Post(new ListUpdatedMessage<Member>(this.membersInternal));
        }

        private void SendStatusMessage(string message)
        {
            var msgLength = Encoding.Unicode.GetBytes(message, 0, message.Length, _chatOutBuffer, 0);
            SteamMatchmaking.SendLobbyChatMsg(this.id, _chatOutBuffer, msgLength);
        }

        private void OnChatMessage(LobbyChatMsg_t chatData)
        {
            if (chatData.m_ulSteamIDLobby != this.id)
            {
                return;
            }

            CSteamID senderId;
            EChatEntryType entryType;
            var msgLength = SteamMatchmaking.GetLobbyChatEntry(this.id, (int)chatData.m_iChatID, out senderId, _chatInBuffer, _chatInBuffer.Length, out entryType);

            var msg = Encoding.Unicode.GetString(_chatInBuffer, 0, msgLength);
            if (!msg.StartsWith(StateMessagePrefix))
            {
                return;
            }

            if (msg == MemberStateUpdatedMessage)
            {
                ResetMembers();
                GameServices.messageBus.Post(new ListUpdatedMessage<Member>(this.membersInternal));

                if (!this.isOwner)
                {
                    return;
                }

                bool ready = this.membersInternal.All(m => m.isReady);
                if (ready)
                {
                    SteamMatchmaking.SetLobbyJoinable(this.id, false);

                    IGameHost host = new DummyHost(); //TODO: get a reference to this somehow. Again injection would come in handy.
                    host.StartHost((hostAddress) =>
                    {
                        SteamMatchmaking.SetLobbyGameServer(this.id, hostAddress.IP, hostAddress.port, 0L);
                    });
                }
            }
        }

        private void OnGameReady(LobbyGameCreated_t gameData)
        {
            if (gameData.m_ulSteamIDLobby != this.id)
            {
                return;
            }

            Leave();

            GameServices.messageBus.Post(new GameStartingMessage(
                new HostAddress
                {
                    IP = gameData.m_unIP,
                    port = gameData.m_usPort
                },
                this.settings));
        }
    }
}