﻿namespace Apex.Steam
{
    using Steamworks;

    public class Friend
    {
        public Friend(CSteamID id, string name)
        {
            this.id = id;
            this.name = name;
        }

        public CSteamID id { get; private set; }

        public string name { get; set; }

        public void InviteToLobby(CSteamID lobbyId)
        {
            SteamMatchmaking.InviteUserToLobby(lobbyId, this.id);
        }
    }
}
