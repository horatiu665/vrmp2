﻿namespace Apex.Steam
{
    using System;
    using System.Collections.Generic;
    using Messages;
    using Services;
    using Steamworks;

    public class LobbyListing : IDisposable
    {
        private SteamCallback<LobbyListing> _callback;
        private List<Lobby> _lobbies;

        private CallResult<LobbyMatchList_t> _lobbyListRequestResult;
#pragma warning disable 0414
        private Callback<LobbyDataUpdate_t> _lobbyListUpdateResult;
#pragma warning restore 0414

        private LobbyListing(SteamCallback<LobbyListing> callback)
        {
            _callback = callback;
            _lobbyListRequestResult = CallResult<LobbyMatchList_t>.Create();
            _lobbyListUpdateResult = Callback<LobbyDataUpdate_t>.Create(OnLobbyDataUpdated);
        }

        public IList<Lobby> lobbies
        {
            get { return _lobbies; }
        }

        public static void Create(int maxCount, SteamCallback<LobbyListing> callback)
        {
            var ll = new LobbyListing(callback);
            ll.GetLobbies(maxCount);
        }

        private void GetLobbies(int maxCount)
        {
            SteamMatchmaking.AddRequestLobbyListResultCountFilter(maxCount);
            var callbackHandle = SteamMatchmaking.RequestLobbyList();

            _lobbyListRequestResult.Set(callbackHandle, OnLobbyListReady);
        }

        private void OnLobbyListReady(LobbyMatchList_t lobbyList, bool error)
        {
            if (error)
            {
                _callback(null, true);
                return;
            }

            int lobbyCount = (int)lobbyList.m_nLobbiesMatching;

            if (_lobbies == null)
            {
                _lobbies = new List<Lobby>(lobbyCount);
            }

            for (int i = 0; i < lobbyCount; i++)
            {
                var lobbyId = SteamMatchmaking.GetLobbyByIndex(i);
                if (!lobbyId.IsValid())
                {
                    continue;
                }

                _lobbies.Add(Lobby.FromId(lobbyId));
            }

            _callback(this, false);
        }

        private void OnLobbyDataUpdated(LobbyDataUpdate_t data)
        {
            if (_lobbies == null || data.m_bSuccess == 0)
            {
                return;
            }

            int count = _lobbies.Count;
            for (int i = 0; i < count; i++)
            {
                var lobby = _lobbies[i];
                if (lobby.id == data.m_ulSteamIDLobby)
                {
                    lobby.Refresh();

                    GameServices.messageBus.Post(new ItemUpdatedMessage<Lobby>(lobby, this.lobbies)
                    {
                        index = i
                    });

                    break;
                }
            }
        }

        public void Dispose()
        {
            //TODO: replace with dispose calls if/when they are implemented
            _lobbyListRequestResult.Cancel();
            _lobbyListUpdateResult.Unregister();

            _lobbyListRequestResult = null;
            _lobbyListUpdateResult = null;
        }
    }
}
