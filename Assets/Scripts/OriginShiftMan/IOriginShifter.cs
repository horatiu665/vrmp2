using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface IOriginShifter
{
    void OnWorldMove(Vector3 originShiftDelta);

    // REGISTER with the OriginShiftManager
    // private void OnEnable();
    // private void OnDisable();
}
