﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(NetOriginShiftManager))]
public class NetOriginShiftManagerEditor : OriginShiftManagerEditor
{
	private NetOriginShiftManager _etarget;
	private bool playersFoldout = true;

	protected override void OnEnable()
	{
		base.OnEnable();
		_etarget = target as NetOriginShiftManager;

	}

	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		playersFoldout = EditorGUILayout.Foldout(playersFoldout, "Players Origin Shifts");
		EditorGUI.indentLevel++;
		if (playersFoldout)
		{
			GUI.enabled = false;
			if (NetOriginShiftManager.editorPlayerOriginShifts.Count == 0)
			{
				EditorGUILayout.LabelField("none");
			}
			foreach (var pos in NetOriginShiftManager.editorPlayerOriginShifts)
			{
				EditorGUILayout.Vector3Field("Player " + pos.Key, pos.Value);
			}
			GUI.enabled = true;

		}
		EditorGUI.indentLevel--;

	}

	protected override void OnSceneGUI()
	{
		base.OnSceneGUI();
	}
}