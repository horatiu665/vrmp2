using System.Linq;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(OriginShiftManager), true)]
public class OriginShiftManagerEditor : Editor
{
    //private MonoScript _script;
    private OriginShiftManager _target;

    private bool regenerateMountainUponOriginShiftEditor = true;

    private Mountain.MountainManager _mountainMan;
    public Mountain.MountainManager mountainMan
    {
        get
        {
            if (_mountainMan == null)
            {
                _mountainMan = FindObjectsOfType<Mountain.MountainManager>().FirstOrDefault(mm => mm.mountainManager == null);
            }
            return _mountainMan;
        }
    }

    protected virtual void OnEnable()
    {
        _target = this.target as OriginShiftManager;
        //_script = MonoScript.FromMonoBehaviour(_target);

    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.LabelField("Editor interface");
        EditorGUI.indentLevel++;

        // draw some info shit
        GUI.enabled = false;
        EditorGUILayout.Vector3Field("Static Origin Shift", OriginShiftManager.originShiftEditor);
        GUI.enabled = true;

        regenerateMountainUponOriginShiftEditor = EditorGUILayout.Toggle("Regen on originShift change", regenerateMountainUponOriginShiftEditor);

        if (GUILayout.Button("Reset OriginShift to zero"))
        {
            OriginShiftManager.FocusOn(Vector3.zero);
        }
        if (GUILayout.Button("Reset OriginShift to start pos"))
        {
            OriginShiftManager.FocusOn(OriginShiftManager.instance.startingPosition);
        }
        if (GUILayout.Button("Set StartingPosition to OriginShift"))
        {
            OriginShiftManager.instance.startingPosition = OriginShiftManager.originShiftEditor;
        }
        if (GUILayout.Button("Regen Mountain"))
        {
            RetardoRegenerateMountain();
        }
        if (GUILayout.Button("Clear Mountain"))
        {
            RetardoClearMountain();
        }

        EditorGUI.indentLevel--;

    }

    protected virtual void OnSceneGUI()
    {
        var dirs = new Vector3[]
        {
            Vector3.right * _target.portalSize.x,
            -Vector3.right * _target.portalSize.x,
            Vector3.forward * _target.portalSize.z,
            -Vector3.forward * _target.portalSize.z,
            Vector3.up * _target.portalSize.y,
            -Vector3.up * _target.portalSize.y,
        };
        foreach (var dir in dirs)
        {
            if (Handles.Button(dir, Quaternion.LookRotation(dir), _target.portalSize.x * 0.1f, _target.portalSize.x * 0.05f, Handles.ConeHandleCap))
            {
                OriginShiftManager.originShiftEditor += dir;
                Repaint();

                if (regenerateMountainUponOriginShiftEditor)
                {
                    RetardoRegenerateMountain();
                }
            }
        }
    }

    private void RetardoRegenerateMountain()
    {
        mountainMan.ClearAllInstant();
        mountainMan.GenerateAt(Vector3.zero);

    }

    private void RetardoClearMountain()
    {
        mountainMan.DestroyAllGenChildren();
    }
}