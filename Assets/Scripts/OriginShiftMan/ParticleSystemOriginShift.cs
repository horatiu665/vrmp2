using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class ParticleSystemOriginShift : MonoBehaviour, IOriginShifter
{
    private ParticleSystem _ps;

    private void OnEnable()
    {
        _ps = this.GetComponent<ParticleSystem>();
        OriginShiftManager.OriginShiftersAdd(this);
    }

    private void OnDisable()
    {
        OriginShiftManager.OriginShiftersRemove(this);
    }

    public void OnWorldMove(Vector3 delta)
    {
        ParticleSystem.Particle[] parts = new ParticleSystem.Particle[_ps.particleCount];
        var count = _ps.GetParticles(parts);
        for (int i = 0; i < count; i++)
        {
            parts[i].position -= delta;
        }
        _ps.SetParticles(parts, count);
    }

}