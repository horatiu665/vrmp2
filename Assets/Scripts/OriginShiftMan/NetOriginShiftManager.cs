using Core;
using MovementEffects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VRCore;
using VRCore.VRNetwork;
using VRCore.VRNetwork.Client;
using VRCore.VRNetwork.Server;

/*
 * Notes about Client => Server => Remote origin shifting
 * 
 * ######## CALCULATION ########
 * 
 * Client calculates origin shift based on player.
 * Same as single player.
 * 
 * Server gets origin shift of each player.
 * It shifts based on the smallest origin shift of all players.
 * Alternative option is to shift to an average shift.
 * 
 * Remotes get the raw origin shift of each player, and can also compute the server OS.
 * 
 * 
 * ######## POSITIONING ########
 * 
 * Positioning objects is done in the following way:
 * Local player positions with local origin shift, and tells server this local position.
 * Server interprets it as: localPos + playerOS - serverOS
 * Server sends this data to the remotes.
 * Remotes get server position and interpret it as: serverPos + serverOS - remoteOS
 * 
 * Thus, if players are far from each other, their relative positions will be coherent
 * 
 * */



public class NetOriginShiftManager : OriginShiftManager
{
    private static NetOriginShiftManager _instance;
    public static new NetOriginShiftManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<NetOriginShiftManager>();
            }
            return _instance;
        }
    }

    PlayerOriginShiftMessage _message = new PlayerOriginShiftMessage();

    private static Dictionary<short, Vector3> playerOriginShifts = new Dictionary<short, Vector3>();
    public static Dictionary<short, Vector3> editorPlayerOriginShifts
    {
        get
        {
            return playerOriginShifts;
        }
    }

    [SerializeField, ReadOnly]
    private Vector3 serverOriginShift = Vector3.zero;

    INetPlayer _player;
    INetPlayer player
    {
        get
        {
            if (_player == null)
            {
                _player = PlayerManager.GetLocalPlayer<INetPlayer>();
            }
            return _player;
        }
    }

    // CONSIDER NOT RUNNING MOVEOBJECTS() AND SUCH ON START. OR TEST ALL PLAYER CONNECT DELAY PERMUTATIONS because here some desyncing might occur...
    protected override void Start()
    {
        // server OS is initially the startPosition, until it changes based on other players.
        serverOriginShift = startingPosition;

        // on start, server will know about the player's OS from the connect message, 
        // so Start should not send any messages because player might still be null.
        base.Start();

        if (!NetServices.isServer)
        {
            Timing.RunCoroutine(PlayerTarget());
        }
    }

    private IEnumerator<float> PlayerTarget()
    {
        while (player == null)
        {
            yield return 0;
        }
        followTarget = player.transform;
    }

    protected override void MoveObjects(Vector3 originShiftDelta)
    {
        base.MoveObjects(originShiftDelta);

        if (player != null)
        {
            // we are the client, and we just moved objects.
            if (NetServices.isNetworked && NetServices.isClient)
            {
                // send message to server cause we just moved origin.
                _message.netId = player.netId;
                _message.originShift = originShift;
                ClientNetSender.instance.Send(_message, UnityEngine.Networking.QosType.ReliableSequenced);

            }
        }

        if (NetServices.isServer)
        {
            serverOriginShift = originShift;

            if (Application.isPlaying)
            {
                _message.netId = -2;
                _message.originShift = originShift;
                ServerNetSender.instance.SendToAll(_message, UnityEngine.Networking.QosType.ReliableSequenced);
            }
        }
    }

    public void SetPlayerOriginShift(short netId, Vector3 originShift)
    {
        playerOriginShifts[netId] = originShift;

        ServerCheckOriginShiftAndMove();

    }

    private void ServerCheckOriginShiftAndMove()
    {
        if (NetServices.isServer)
        {
            var originShiftDelta = ComputeServerOriginShift() - originShift;
            //Debug.Log("Calculated server OS DELTA: " + originShiftDelta);
            if (originShiftDelta != Vector3.zero)
            {
                MoveObjects(originShiftDelta);
            }
        }
    }

    /// <summary>
    /// ONLY SERVER
    /// Calculates server OS from all players in dict
    /// </summary>
    /// <returns></returns>
    private Vector3 ComputeServerOriginShift()
    {
        Vector3 minOriginShift = serverOriginShift;
        short minPlayerId = -1;
        if (NetServices.isServer)
        {
            minOriginShift = Vector3.one * float.MaxValue;
        }
        else            // is client or singleplayer
        {
            if (player != null)
            {
                // set min origin shift to this player's origin shift. need to take that into account for server OS calculation.
                minOriginShift = originShift;
                minPlayerId = player.netId;
            }
        }
        var enu = playerOriginShifts.GetEnumerator();
        while (enu.MoveNext())
        {
            var po = enu.Current;
            if (minOriginShift.sqrMagnitude > enu.Current.Value.sqrMagnitude)
            {
                minOriginShift = enu.Current.Value;
                minPlayerId = enu.Current.Key;
            }
        }
        if (minPlayerId != -1)
        {
            return minOriginShift;
        }

        return Vector3.zero;
    }

    // assumes playerId exists in origin shift dict
    public static Vector3 ServerPos(short playerId, Vector3 localPos)
    {
        return localPos + (playerOriginShifts[playerId] - originShift);
    }

    public static Vector3 RemotePos(Vector3 serverPos)
    {
        //serverPos + serverOS - remoteOS
        return serverPos + instance.serverOriginShift - originShift;
    }

    // on disconnect and such
    public void RemovePlayer(short netId)
    {
        playerOriginShifts.Remove(netId);
    }

    public void SetServerOriginShiftDelta(Vector3 originShiftDelta)
    {
        serverOriginShift += originShiftDelta;
    }

    public void SetServerOriginShift(Vector3 originShift)
    {
        serverOriginShift = originShift;
    }

    // server
    public Vector3 GetPlayerOriginShift(short netId)
    {
        if (playerOriginShifts.ContainsKey(netId))
        {
            return playerOriginShifts[netId];
        }
        return startingPosition;
    }
}
