using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class MoveChildrenOnOriginShift : MonoBehaviour, IOriginShifter
{
    private void OnEnable()
    {
        OriginShiftManager.OriginShiftersAdd(this);
    }

    private void OnDisable()
    {
        OriginShiftManager.OriginShiftersRemove(this);
    }

    public void OnWorldMove(Vector3 delta)
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            transform.GetChild(i).position -= delta;
        }
    }

}
