using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnOriginShift : MonoBehaviour, IOriginShifter
{
    private void OnEnable()
    {
        OriginShiftManager.OriginShiftersAdd(this);
    }

    private void OnDisable()
    {
        OriginShiftManager.OriginShiftersRemove(this);
    }

    public void OnWorldMove(Vector3 delta)
    {
        transform.position -= delta;
    }

}
