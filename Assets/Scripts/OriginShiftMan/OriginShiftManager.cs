using MovementEffects;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using Mountain;
using VRCore;
using Core.Level;
using VRCore.VRNetwork;

public class OriginShiftManager : MonoBehaviour
{
    private static OriginShiftManager _instance;
    public static OriginShiftManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<OriginShiftManager>();
            }
            return _instance;
        }
    }


    public bool shiftingEnabled = true;

    [SerializeField]
    private bool moveSceneViewCamera = true;

    [SerializeField]
    private bool showDebug = false;


    [Header("Initial origin shift?")]
    public Vector3 startingPosition = Vector3.zero;

    public Transform followTarget;

    public List<Transform> otherObjectsToMove = new List<Transform>();

    public float updateRate = 1f;

    // default value is based on the hex grid of the mesh: it should be multiple of the hex grid so the grid points do not change height - think nyquist freq and aliasing...
    public Vector3 portalSize = new Vector3(1200, 1200, 1200);

    /// <summary>
    /// World should be simulated as if its origin was at this position.
    /// Local position to world achieved by adding originShift to scene position.
    /// World to local achieved by subtracting originShift.
    /// That's why we subtract the originShiftDelta when world moves, cause we are turning world positions into local positions.
    /// </summary>
    public static Vector3 originShift
    {
        get
        {
            return instance._originShift;
        }
        private set
        {
            instance._originShift = value;
        }
    }

    // only used by editor to revert valueof originShift upon deserialization (coming back from visual studio or reopening scene)
    [SerializeField, ReadOnly]
    private Vector3 _originShift;

    // only for use by editor script!!!!
    public static Vector3 originShiftEditor
    {
        get
        {
            return originShift;
        }
        set
        {
            originShift = value;
        }
    }

    /// <summary>
    /// Tells scripts that the origin shift has moved by some amount
    /// </summary>
    public static event System.Action<Vector3> OnWorldMove;

    // every script that can origin shift must register here...
    private HashSet<IOriginShifter> originShifters = new HashSet<IOriginShifter>();

    protected virtual void Start()
    {
        if (shiftingEnabled)
        {
            if (startingPosition - originShift != Vector3.zero)
            {
                MoveObjects(startingPosition - originShift);
            }
        }
    }

    private void OnEnable()
    {
        if (NetServices.isClient || (!NetServices.isNetworked))
        {
            Timing.RunCoroutine(CheckShiftingRare());
        }
    }

    /// <summary>
    /// Sets the origin shift to a value that makes this globalPosition close to zero.
    /// ADD ORIGINSHIFT TO A POS TO MAKE IT GLOBALPOS. 
    /// for example: local player pos: 100, 0, 0 but origin shift is -1000, 0, 0. that means origin (real 0,0,0) is at -1000 and player is really at 100 + (-1000) = -900.
    /// </summary>
    /// <param name="globalPosition">position to focus on</param>
    public static void FocusOn(Vector3 globalPosition)
    {
        var newShift = instance.OriginShiftFromRawPosition(globalPosition);
        var delta = newShift - originShift;
        // should we also move objects here?
        // ehh maybe
        if (delta != Vector3.zero)
        {
            instance.MoveObjects(delta);
        }
        if (instance.showDebug)
        {
            Debug.Log("[OriginShiftManager] FocusOn happened. moved by delta = " + delta);
        }
    }

    private IEnumerator<float> CheckShiftingRare()
    {
        while (true)
        {
            yield return Timing.WaitForSeconds(updateRate);

            if (shiftingEnabled)
            {
                CheckFollowTargetAndMove();
            }
        }
    }

    /// <summary>
    /// moves by originShiftDelta. does not ask questions.
    /// </summary>
    /// <param name="originShiftDelta"></param>
    /// <returns></returns>
    protected virtual void MoveObjects(Vector3 originShiftDelta)
    {
        originShift += originShiftDelta;

        // move target back by originShift amount
        if (followTarget != null)
        {
            followTarget.position -= originShiftDelta;
        }

        // all players?? except local player? even on server? lol
        foreach (var p in PlayerManager.allPlayers)
        {
            var pp = PlayerManager.GetPlayer(p);
            if (pp.transform != followTarget)
            {
                pp.position -= originShiftDelta;
            }
        }
        
        // move all objects back by originShift amount
        for (int i = 0; i < otherObjectsToMove.Count; i++)
        {
            if (otherObjectsToMove[i] != null)
            {
                otherObjectsToMove[i].transform.position -= originShiftDelta;
            }
        }

        if (OnWorldMove != null)
        {
            OnWorldMove(originShiftDelta);
        }

        var enu = originShifters.GetEnumerator();
        //for (int i = 0; i < originShifters.Count; i++)
        while (enu.MoveNext())
        {
            //originShifters[i].OnWorldMove(originShiftDelta);
            enu.Current.OnWorldMove(originShiftDelta);
        }

#if UNITY_EDITOR
        if (moveSceneViewCamera)
        {
            // fuck unity's lack of documentation and its weird non-standards
            var sceneviews = UnityEditor.SceneView.sceneViews.ToArray().Select(o => o as UnityEditor.SceneView).ToList();
            for (int i = 0; i < sceneviews.Count; i++)
            {
                if (sceneviews[i] != null)
                {
                    sceneviews[i].pivot -= originShiftDelta;
                }
            }
        }

        if (instance.showDebug)
        {
            Debug.Log("[OriginShiftManager] MoveObjects happened, delta = " + originShiftDelta);
        }
#endif
    }

    public void CheckFollowTargetAndMove()
    {
        Vector3 originShiftDelta = Vector3.zero;

        if (followTarget != null)
        {
            // see if the non-global followTarget.position is in any grid different than zero (or the current one), and if so, move it in that direction.
            originShiftDelta = OriginShiftFromRawPosition(followTarget.position);
        }

        if (originShiftDelta != Vector3.zero)
        {
            MoveObjects(originShiftDelta);
        }
    }

    public Vector3 OriginShiftFromRawPosition(Vector3 position)
    {
        var round = new Vector3(
                Mathf.Round(position.x / portalSize.x),
                Mathf.Round(position.y / portalSize.y),
                Mathf.Round(position.z / portalSize.z)
                );
        round.Scale(portalSize);
        return round;
    }

    public static void OriginShiftersAdd(IOriginShifter originShifter)
    {
        instance.originShifters.Add(originShifter);
    }

    public static void OriginShiftersRemove(IOriginShifter shifter)
    {
        if (instance != null)
        {
            instance.originShifters.Remove(shifter);
        }
    }


    /// <summary>
    /// Local to global is done by ADDING originShift.
    /// Local position (where you see an object in the scene), a.k.a. origin-shifted position. 
    /// </summary>
    public static Vector3 LocalToGlobalPos(Vector3 localPos)
    {
        return localPos + originShift;
    }

    /// <summary>
    /// Global to local is done by SUBTRACTING originShift.
    /// Global position (such as object data in a premade object spawner) is non-shifted position and can be a very big number.
    /// </summary>
    public static Vector3 GlobalToLocalPos(Vector3 globalPos)
    {
        return globalPos - originShift;
    }
}