﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class ShadowTool : MonoBehaviour
{
    [Header("Intuitive")]
    public Vector3 cascadesInMeters;

    public float maxShadowDistance = 1000;

    [Header("RAW")]
    public Vector3 cascades;

    private Vector3 oldSettings, oldLocalValue;

    private void Init()
    {
        maxShadowDistance = QualitySettings.shadowDistance;

        oldSettings = cascades = QualitySettings.shadowCascade4Split;

        cascadesInMeters = cascades * maxShadowDistance;

    }


    private void Awake()
    {
        Init();
    }

    private void Reset()
    {
        Init();
    }

    private void Update()
    {
        // if settings changed, update local value.
        // if local value changed, update settings.
        if (QualitySettings.shadowCascade4Split != oldSettings)
        {
            oldSettings = cascades = QualitySettings.shadowCascade4Split;
            cascadesInMeters = oldLocalValue = oldSettings * QualitySettings.shadowDistance;
            maxShadowDistance = QualitySettings.shadowDistance;
        }

        if (cascades != oldSettings)
        {
            oldSettings = QualitySettings.shadowCascade4Split = cascades;
            cascadesInMeters = oldLocalValue = oldSettings * QualitySettings.shadowDistance;

        }

        if (cascadesInMeters != oldLocalValue)
        {
            oldLocalValue = cascadesInMeters;
            cascades = cascadesInMeters / maxShadowDistance;
            oldSettings = cascades;
            QualitySettings.shadowCascade4Split = cascades;
        }
    }


}