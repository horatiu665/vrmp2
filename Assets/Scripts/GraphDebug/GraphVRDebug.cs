﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GraphVRDebug : MonoBehaviour
{
    private static GraphVRDebug _instance;
    public static GraphVRDebug instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<GraphVRDebug>();
            }
            return _instance;
        }
    }

    Queue<GameObject> graphPoints = new Queue<GameObject>();

    private static Dictionary<string, GraphVRDebug> currentGraphs = new Dictionary<string, GraphVRDebug>();

    public GameObject cursorPrefab;

    public string debugName = "Default";

    public int resolution = 100;

    private void Awake()
    {
        if (!currentGraphs.ContainsKey(debugName))
        {
            // init dict
            currentGraphs.Add(debugName, this);
        }
    }

    public void SetValue(float value, Color color = default(Color), float time = 1f)
    {
        GameObject cursor;
        if (graphPoints.Count >= resolution)
        {
            cursor = graphPoints.Dequeue();
        }
        else
        {
            cursor = Instantiate(cursorPrefab);
            cursor.name = "[Gen]";
            cursor.transform.SetParent(transform);
        }

        if (cursor == null)
        {
            return;
        }

        graphPoints.Enqueue(cursor);

        cursor.GetComponent<MeshRenderer>().material.color = color;

        cursor.transform.localPosition = new Vector3(time, value, 0);

        foreach (var g in graphPoints)
        {
            g.transform.localPosition -= Vector3.right / resolution;
        }
    }

    public static GraphVRDebug GetGraph(string name)
    {
        if (instance == null)
        {
            Debug.LogError("[GraphVRDebug] Cannot use GraphVRDebug if none exists in the scene. Please add one so it can be reused");
            return null;
        }

        if (!currentGraphs.ContainsKey(name))
        {
            var n = Instantiate(instance);
            n.debugName = name;
            currentGraphs[name] = n;
            n.transform.SetParent(instance.transform.parent);
            n.transform.localPosition = instance.transform.localPosition + Vector3.right * 1.1f;
            n.transform.localRotation = instance.transform.localRotation;
            n.transform.localScale = instance.transform.localScale;
        }

        return currentGraphs[name];
    }
}
