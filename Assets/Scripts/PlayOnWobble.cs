﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRTweakTool;
using Random = UnityEngine.Random;

public class PlayOnWobble : MonoBehaviour
{
    private ParticleSystem _ps;
    public ParticleSystem ps
    {
        get
        {
            if (_ps == null)
            {
                _ps = GetComponent<ParticleSystem>();
            }
            return _ps;
        }
    }

    private Wobbler _wobbler;
    public Wobbler wobbler
    {
        get
        {
            if (_wobbler == null)
            {
                _wobbler = GetComponentInParent<Wobbler>();
            }
            return _wobbler;
        }
    }
    public float wobbleThreshold = 0.15f;
    public float wobbleMax = 10f;
    public int count = 30;

    void Update()
    {
        if (wobbler.deltaWobble.magnitude / Time.deltaTime > wobbleThreshold)
        {
            var m = ps.emission;
            var b = m.GetBurst(0);
            b.count = count * wobbler.deltaWobble.magnitude / wobbleMax;
            m.SetBurst(0, b);
            ps.Play();
        }
    }
}