using Core;
using Helpers;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using VRCore.VRNetwork;
using Random = UnityEngine.Random;

public class RbSncTester : MonoBehaviour, IOriginShifter
{
    public bool isClient;
    public RbSncTester other;

    [Header("Server")]
    public SmartRange randomDelay = new SmartRange(0.05f, 0.13f);

    public float messagesPerSecond = 20;
    float nextMessageTime = 0f;

    public Vector3 initServerPosOffset;

    public float dropMessageChance = 0.03f;

    [Header("Client")]
    [SerializeField]
    private bool _interpolate = true;

    [SerializeField]
    private Vector2 _syncDeltaTimeRange = new Vector2(0.001f, 0.15f);

    [SerializeField, ReadOnly]
    private bool _hasBeenUpdated;

    [SerializeField]
    private float _minDeltaPositionThreshold = 0.005f;

    private Vector3 _lastSyncPos;
    private Quaternion _lastSyncRot;
    private Vector3 _targetPosition;
    private Quaternion _targetRotation;
    private Vector3 _lastSyncVel;
    private Vector3 _lastSyncAV;
    private Vector3 _targetVel;
    private Vector3 _targetAV;
    private int _framesLerped;
    private float _timeLerped = 0f;

    // 2 network frames ago
    private float _prevSyncTime;

    // 1 network frame ago
    private float _lastSyncTime;

    [SerializeField]
    private float _maxLerpSmoothness = 1f;

    [SerializeField]
    private float _maxTForUpdates = 1f;

    [SerializeField]
    private float _syncLerpAmount = 0.125f;

    [Range(0, 2)]
    public int extrapolationFrames = 0;

    [Range(0, 1f)]
    public float velocityExtrapolationMultiplier = 1f;

    public float _debugTime = 3f;

    private Rigidbody _rb;
    public Rigidbody rb
    {
        get
        {
            if (_rb == null)
            {
                _rb = GetComponent<Rigidbody>();
            }
            return _rb;
        }
    }

    /// <summary>
    /// Gets a value indicating whether this <see cref="RigidbodySyncComponent"/> has changed position or rotation since last update. Only used by the server.
    /// </summary>
    public bool changed
    {
        get { return (_lastSyncPos != this.transform.position) || (_lastSyncRot != this.transform.rotation); }
    }

    private void Reset()
    {
        other = FindObjectsOfType<RbSncTester>().Where(r => r != this).FirstOrDefault();
    }

    private void OnEnable()
    {
        if (other != null)
        {
            if (!isClient)
                initServerPosOffset = other.transform.position - transform.position;
        }

        OriginShiftManager.OriginShiftersAdd(this);

    }

    private void OnDisable()
    {
        OriginShiftManager.OriginShiftersRemove(this);
        ResetSync();
    }

    public void OnWorldMove(Vector3 originShiftDelta)
    {
        _lastSyncPos -= originShiftDelta;
        _targetPosition -= originShiftDelta;

    }


    public void ResetSync()
    {
        _lastSyncRot = _targetRotation = new Quaternion(0f, 0f, 0f, 0f);
        _lastSyncPos = _targetPosition = Vector3.zero;
        _lastSyncVel = _targetVel = Vector3.zero;
        _lastSyncAV = _targetAV = Vector3.zero;
        _hasBeenUpdated = false;
        _prevSyncTime = 0f;
        _lastSyncTime = 0f;
        _framesLerped = 0;
        _timeLerped = 0;
    }

    private void Update()
    {
        if (isClient)
        {
            if (!_hasBeenUpdated)
            {
                return;
            }

            if ((transform.position - _targetPosition).sqrMagnitude < (_minDeltaPositionThreshold * _minDeltaPositionThreshold))
            {
                // if the position change is super small then stop lerping to it
                return;
            }

            var updated = UpdatePos();

            Debug.DrawRay(transform.position, Vector3.right * 4f, updated ? Color.green : Color.yellow, _debugTime);

        }
        else
        {
            // we are server
            // pretend to send data with delay
            if (Time.time > nextMessageTime)
            {
                nextMessageTime = Time.time + 1f / messagesPerSecond;
                var data = GetSyncData();
                if (Random.Range(0, 1f) >= dropMessageChance)
                {
                    StartCoroutine(pTween.Wait(randomDelay.value, () =>
                    {
                        other.HandleUpdate(data);
                    }));
                }
            }

            Debug.DrawRay(transform.position, Vector3.right * 4f, Color.green, _debugTime);

            Debug.DrawLine(transform.position + Vector3.right * 4f, other.transform.position, Color.white * 0.3f, _debugTime);
        }

    }

    private bool UpdatePos()
    {
        var lastTimeDelta = Mathf.Clamp(_lastSyncTime - _prevSyncTime, _syncDeltaTimeRange.x, _syncDeltaTimeRange.y);
        var t = _timeLerped / lastTimeDelta;

        bool updateIt = t <= _maxTForUpdates;
        // only update while we're within the last sync frames. if we exceed, we let the simulation work 100% locally
        if (updateIt)
        {
            t = Mathf.Min(_maxLerpSmoothness, t);

            // what is the most likely next frame pos for the server, according to the change in velocity of the previous two frames?
            // lastSyncPos goes with lastSyncVel
            // targetPos goes with targetVel
            // nextPos can be (targetPos - lastSyncPos) * (targetVel / lastSyncVel)
            //      because: 
            var nextPos = _targetPosition + (_targetPosition - _lastSyncPos) * (_targetVel.magnitude / _lastSyncVel.magnitude);
            var nextVel = _targetVel + (_targetVel - _lastSyncVel) * velocityExtrapolationMultiplier;
            var nextNextPos = nextPos + (nextPos - _targetPosition) * (nextVel.magnitude / _targetVel.magnitude);

            rb.velocity = Vector3.Lerp(rb.velocity, Vector3.LerpUnclamped(_lastSyncVel, _targetVel, t), _syncLerpAmount);
            rb.angularVelocity = Vector3.Lerp(rb.angularVelocity, Vector3.LerpUnclamped(_lastSyncAV, _targetAV, t), _syncLerpAmount);

            if (extrapolationFrames == 0)
            {
                // no extrapolation
                transform.position = Vector3.Lerp(transform.position, Vector3.LerpUnclamped(_lastSyncPos, _targetPosition, t), _syncLerpAmount);
            }
            else if (extrapolationFrames == 1)
            {
                // extrapolate to nextPos
                transform.position = Vector3.Lerp(transform.position, Vector3.LerpUnclamped(_targetPosition, nextPos, t), _syncLerpAmount);
            }
            else if (extrapolationFrames == 2)
            {
                // extrapolate twice to nextNextPos
                transform.position = Vector3.Lerp(transform.position, Vector3.LerpUnclamped(nextPos, nextNextPos, t), _syncLerpAmount);
            }

            transform.rotation = Quaternion.SlerpUnclamped(transform.rotation, Quaternion.SlerpUnclamped(_lastSyncRot, _targetRotation, t), _syncLerpAmount);

        }

        _framesLerped++;
        _timeLerped += Time.deltaTime;

        return updateIt;
    }

    /// <summary>
    /// Handles an <see cref="EquippableSyncData"/> update. Only called on clients.
    /// </summary>
    /// <param name="syncData">The synchronized data.</param>
    /// <param name="initialSetup">If true, applies the position and rotation immediately. Otherwise, sets targets used in interpolation.</param>
    public void HandleUpdate(EquippableSyncData syncData, bool initialSetup = false)
    {
        if (!_interpolate || initialSetup)
        {
            rb.velocity = syncData.velocity;
            rb.angularVelocity = syncData.angularVelocity;
            this.transform.position = syncData.position;
            this.transform.rotation = syncData.rotation;
        }
        else
        {
            _hasBeenUpdated = true;

            if (_targetPosition == Vector3.zero)
            {
                _lastSyncPos = syncData.position;
            }
            else
            {
                _lastSyncPos = _targetPosition;
            }

            if (_targetRotation.IsZero())
            {
                _lastSyncRot = syncData.rotation;
            }
            else
            {
                _lastSyncRot = _targetRotation;
            }

            if (_targetVel == Vector3.zero)
            {
                _lastSyncVel = syncData.velocity;
            }
            else
            {
                _lastSyncVel = _targetVel;
            }

            if (_targetAV == Vector3.zero)
            {
                _lastSyncAV = syncData.angularVelocity;
            }
            else
            {
                _lastSyncAV = _targetAV;
            }

            _targetPosition = syncData.position;
            _targetRotation = syncData.rotation;
            _targetVel = syncData.velocity;
            _targetAV = syncData.angularVelocity;

            _prevSyncTime = _lastSyncTime;
            _lastSyncTime = Time.timeSinceLevelLoad;
            _framesLerped = 0;
            _timeLerped = 0f;

            Debug.DrawRay(_targetPosition, Vector3.left * 3f, Color.red, _debugTime);

        }
    }

    /// <summary>
    /// Gets the synchronize data. Also records the position and rotation for use in changed check. Only called on server.
    /// </summary>
    /// <returns></returns>
    public EquippableSyncData GetSyncData()
    {
        var pos = this.transform.position;
        var rot = this.transform.rotation;
        var vel = rb.velocity;
        var av = rb.angularVelocity;

        _lastSyncPos = pos;
        _lastSyncRot = rot;
        _lastSyncVel = vel;
        _lastSyncAV = av;

        Debug.DrawRay(pos, Vector3.left * 3f, Color.red, _debugTime);

        return new EquippableSyncData()
        {
            position = pos + initServerPosOffset,
            rotation = rot,
            velocity = vel,
            angularVelocity = av
        };
    }

    [DebugButton]
    private void CreateAClone()
    {
        var clone = Instantiate(this.gameObject);
        var cloneRbSnc = clone.GetComponent<RbSncTester>();
        cloneRbSnc.isClient = true;
        this.other = cloneRbSnc;
        cloneRbSnc.other = this;
        clone.transform.SetParent(transform.parent);
        var ar = clone.GetComponent<Arrow>();
        if (ar != null)
        {
            ar.canImpale = false;
        }
        this.initServerPosOffset = new Vector3(2.25f, 0, 0);

    }

    private void OnGUI()
    {
        if (!isClient)
        {
            var style = GUI.skin.label;
            GUI.skin.label.fontSize = 40;
            GUI.Label(new Rect(10, 10, 1000, 80), "V:" + rb.velocity.magnitude.ToString("F2"));
            GUI.skin.label = style;
        }
    }
}