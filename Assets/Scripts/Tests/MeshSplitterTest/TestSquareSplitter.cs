﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using Random = UnityEngine.Random;
using VRCore;

public class TestSquareSplitter : MonoBehaviour
{
    [SerializeField]
    private List<Transform> players;

    [SerializeField]
    private List<SquareClass> squares;


    [Header("Square settings")]
    public float squareEdgeSize = 1f;

    [Header("Algorithm params")]
    public float minPlayerDistanceForNewSquare = 0.5f;


    [Header("Random shit")]

    // random cached shit
    [SerializeField]
    private List<LineShit> lines = new List<LineShit>(100);

    int randomSeed;

    private void OnValidate()
    {
        if (!Application.isPlaying)
        {
            if (transform.childCount != players.Count)
            {
                InitPlayers();
            }
        }
    }

    private void Start()
    {
        InitPlayers();
        randomSeed = (int)System.DateTime.Now.ToBinary();
    }

    [DebugButton]
    private void InitPlayers()
    {
        players = transform.GetChildren();
    }


    private void Update()
    {
        CalculateSquares();
    }

    [DebugButton]
    private void CalculateSquares()
    {
        // if players are far away, create non-overlapping squares on top of each.
        squares.Clear();
        foreach (var p in players)
        {
            squares.Add(new SquareClass()
            {
                center = p.position,
                radius = squareEdgeSize,
            });
        }

        lines.Clear();
        var mPDFNSSquared = minPlayerDistanceForNewSquare * minPlayerDistanceForNewSquare;
        for (int i = 0; i < players.Count; i++)
        {
            for (int j = 0; j < i; j++)
            {
                var p = players[i];
                var q = players[j];
                if (SquaresOverlap(p.position, q.position))
                {
                    var l = new LineShit()
                    {
                        start = p.position,
                        end = q.position,
                    };
                    lines.Add(l);
                    var dir = l.end - l.start;
                    squares[i].center -= dir.normalized * (dir.magnitude);
                }

            }
        }

    }

    bool SquaresOverlap(Vector3 s, Vector3 t)
    {
        var h = squareEdgeSize / 2;
        var smin = new Vector2(s.x - h, s.z - h);
        var smax = new Vector2(s.x + h, s.z + h);
        var tmin = new Vector2(t.x - h, t.z - h);
        var tmax = new Vector2(t.x + h, t.z + h);
        return (smin.x < tmax.x && smin.y < tmax.y && tmin.x < smax.x && tmin.y < smax.y);

    }

    private void OnDrawGizmos()
    {
        Random.InitState(randomSeed);
        for (int i = 0; i < squares.Count; i++)
        {
            Gizmos.color = Random.ColorHSV(0f, 0.5f, 0.3f, 0.6f, 0.4f, 0.7f);
            squares[i].OnDrawGizmos();
        }

        Gizmos.color = Color.red;
        for (int i = 0; i < lines.Count; i++)
        {
            lines[i].OnDrawGizmos();
        }
    }

    [Serializable]
    public class SquareClass
    {
        public Vector3 center;
        public float radius;

        public void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(center, new Vector3(radius, 0.01f, radius));
        }
    }

    [Serializable]
    public class LineShit
    {
        public Vector3 start, end;

        public void OnDrawGizmos()
        {
            Gizmos.DrawLine(start, end);
        }
    }
}
