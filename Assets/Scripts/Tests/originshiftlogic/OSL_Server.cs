﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class OSL_Server : MonoBehaviour
{
    private static OSL_Server _instance;
    public static OSL_Server instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<OSL_Server>();
            }
            return _instance;
        }
    }

    public Vector3 originShift = Vector3.zero;

    public List<OSL_ServerClientData> clientData = new List<OSL_ServerClientData>();

    public float messageDelayS = 0.5f;

    public bool showGizmos = true;

    private void Delayed(Action a)
    {
        StartCoroutine(pTween.Wait(messageDelayS * Random.Range(0.5f, 1f), a));
    }

    internal void ClientToServer_Register(int id, Vector3 originShift)
    {
        clientData.Add(new OSL_ServerClientData()
        {
            id = id,
            originShift = originShift,
        });

        // tell the new guy about other clients (except himself)
        var newguy = OSL_Client.allClients.First(c => c.id == id);
        foreach (var c in OSL_Client.allClients)
        {
            if (c.id != id)
            {
                Delayed(() => { newguy.ServerToClient_RegisterRemote(c.id, Vector3.zero); });
            }
        }

        // tell other clients about the new guy
        foreach (var c in OSL_Client.allClients)
        {
            if (c.id != id)
            {
                Delayed(() => { c.ServerToClient_RegisterRemote(id, originShift); });
            }
        }
    }

    internal void ClientToServer_Deregister(int id)
    {
        Delayed(() => { Deregister(id); });
    }

    private void Deregister(int id)
    {
        clientData.RemoveAll(d => d.id == id);
        foreach (var c in OSL_Client.allClients)
        {
            Delayed(() => { c.ServerToClient_Deregister(id); });
        }
    }

    internal void ClientToServer_OriginShiftChange(int id, Vector3 originShift)
    {
        clientData.First(c => c.id == id).originShift = originShift;
        foreach (var c in OSL_Client.allClients)
        {
            if (c.id != id)
            {
                Delayed(() => { c.ServerToClient_OriginShiftChange(id, originShift); });
            }
        }
    }

    private void OnDrawGizmos()
    {
        if (!showGizmos)
        {
            return;
        }
        Gizmos.color = Colors.DarkOliveGreen;
        foreach (var c in clientData)
        {
            Gizmos.DrawSphere(c.originShift, 0.5f);
        }
    }
}


[Serializable]
public class OSL_ServerClientData
{
    public int id;
    public Vector3 originShift;
}