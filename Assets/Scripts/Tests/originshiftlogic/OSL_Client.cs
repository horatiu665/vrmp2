﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class OSL_Client : MonoBehaviour
{
    public static List<OSL_Client> allClients = new List<OSL_Client>();
    public int id;

    public Vector3 originShift;

    public Vector3 serverOriginShift;

    public List<OSL_ClientClientData> clientData = new List<OSL_ClientClientData>();

    public float messageDelayS = 0.5f;



    private void OnEnable()
    {
        allClients.Add(this);
        id = allClients.Count;
        Delayed(() => { ClientToServer_Register(id, originShift); });

    }

    private void ClientToServer_Register(int id, Vector3 originShift)
    {
        OSL_Server.instance.ClientToServer_Register(id, originShift);
    }

    private void OnDisable()
    {
        allClients.Remove(this);
        OSL_Server.instance.ClientToServer_Deregister(id);
    }

    private void Update()
    {
        var gridPos = transform.position;
        gridPos.x = Mathf.Round(gridPos.x);
        gridPos.y = Mathf.Round(gridPos.y);
        gridPos.z = Mathf.Round(gridPos.z);
        if (gridPos != originShift)
        {
            // moved!
            originShift = gridPos;
            ClientToServer_OriginShiftChange(id, originShift);
        }
    }

    private void Delayed(Action a)
    {
        StartCoroutine(pTween.Wait(messageDelayS * Random.Range(0.5f, 1f), a));
    }

    internal void ServerToClient_RegisterRemote(int id, Vector3 originShift)
    {
        if (!clientData.Any(c => c.id == id))
        {
            clientData.Add(new OSL_ClientClientData()
            {
                id = id,
                originShift = originShift,
            });
        }

    }

    internal void ServerToClient_Deregister(int id)
    {
        clientData.RemoveAll(d => d.id == id);
    }

    private void ClientToServer_OriginShiftChange(int id, Vector3 originShift)
    {
        Delayed(() => OSL_Server.instance.ClientToServer_OriginShiftChange(id, originShift));
    }

    internal void ServerToClient_OriginShiftChange(int id, Vector3 originShift)
    {
        clientData.First(c => c.id == id).originShift = originShift;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Colors.HotPink;
        Gizmos.DrawSphere(serverOriginShift, 0.5f);

        Gizmos.color = Colors.LawnGreen;
        foreach (var c in clientData)
        {
            Gizmos.DrawSphere(c.originShift, 0.5f);
        }
    }
}

[Serializable]
public class OSL_ClientClientData
{
    public int id;
    public Vector3 originShift;
}