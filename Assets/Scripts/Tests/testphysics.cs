﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class testphysics : MonoBehaviour
{
    private Rigidbody _r;
    public Rigidbody r
    {
        get
        {
            if (_r == null)
            {
                _r = GetComponent<Rigidbody>();
            }
            return _r;
        }
    }


    public Vector3 force;
    public ForceMode mode;
    public bool velocityChange = false;

    public bool doIt;
    public bool reset;
    private Vector3 initPos;
    private Quaternion initRot;

    private void OnEnable()
    {
        initPos = transform.position;
        initRot = transform.rotation;
    }

    private void FixedUpdate()
    {
        if (doIt)
        {
            doIt = false;
            AddForce();
        }

        if (reset)
        {
            reset = false;
            Reset();
        }
    }

    private void Reset()
    {
        r.velocity = Vector3.zero;
        r.angularVelocity = Vector3.zero;
        r.position = initPos;
        r.rotation = initRot;
    }

    public void AddForce()
    {
        if (!velocityChange)
        {
            r.AddForce(force, mode);
        }
        else
        {
            switch (mode)
            {
            case ForceMode.Force:
                r.velocity += force * Time.fixedDeltaTime / r.mass;
                break;
            case ForceMode.Acceleration:
                r.velocity += force * Time.fixedDeltaTime;
                break;
            case ForceMode.Impulse:
                r.velocity += force / r.mass;
                break;
            case ForceMode.VelocityChange:
                r.velocity += force;
                break;
            default:
                break;
            }
        }
    }
}