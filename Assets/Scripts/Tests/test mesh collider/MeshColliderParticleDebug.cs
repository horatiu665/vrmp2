using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class MeshColliderParticleDebug : MonoBehaviour
{
    ParticleSystem ps;

    MeshSetVerticesTest mt;
    private int oldtris;

    private Dictionary<int, double> triCountToDuration = new Dictionary<int, double>();
    private bool particleRefresh;

    public GameObject gizmo;
    private GameObject[] gizmos = new GameObject[10000];

    public int autoTris = 0;

    void Awake()
    {
        ps = GetComponent<ParticleSystem>();
        mt = FindObjectOfType<MeshSetVerticesTest>();
        if (gizmos.Length != 10000) gizmos = new GameObject[10000];
        for (int i = 0; i < 10000; i++)
        {
            triCountToDuration[i] = 0.0;
            gizmos[i] = Instantiate(gizmo);
            gizmos[i].transform.SetParent(transform);
        }
    }

    private IEnumerator Start()
    {
        while (true)
        { 
            yield return new WaitForFixedUpdate();
            if (autoTris > 0)
            {
                mt.triangles += autoTris;
            }

            yield return new WaitForFixedUpdate();
        }

        //yield break;
    }

    void FixedUpdate()
    {
        if (mt.triangles != oldtris)
        {
            oldtris = mt.triangles;
            triCountToDuration[oldtris] = mt.colliderSetTime;
            particleRefresh = true;
        }
        else
        {
            if (particleRefresh)
            {
                particleRefresh = false;
                RefreshParticles();
            }
        }

    }

    private void RefreshParticles()
    {
        for (int i = 0; i < 10000; i++)
        {
            gizmos[i].transform.localPosition = new Vector3(i / 100f, (float)triCountToDuration[i], 0f);
        }
    }
}