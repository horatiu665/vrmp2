using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class MeshSetVerticesTest : MonoBehaviour
{
    private MeshFilter mf;
    private MeshRenderer mr;
    private MeshCollider mc;

    public Vector2 randomizeVerticesPerFrame = new Vector2(10, 30);
    public float randomizeDist = 0.33f;

    public int vertices = 100;
    public int triangles = 100;

    Vector3[] verts;
    int[] tris;

    Mesh m;

    public double colliderSetTime = 0;

    public bool setMesh = true;

    private void Awake()
    {
        mr = gameObject.GetComponent<MeshRenderer>();
        if (mr == null)
        {
            mr = gameObject.AddComponent<MeshRenderer>();
            mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
        }
        mf = gameObject.GetOrAddComponent<MeshFilter>();
        mc = gameObject.GetOrAddComponent<MeshCollider>();
    }

    private void OnEnable()
    {
        m = new Mesh();

    }

    private void OnDisable()
    {
        Destroy(m);
    }

    private void FixedUpdate()
    {
        if (verts == null || verts.Length != vertices || triangles * 3 != tris.Length)
        {
            verts = new Vector3[vertices];
            for (int i = 0; i < vertices; i++)
            {
                verts[i] = Random.insideUnitSphere;
            }
            tris = new int[triangles * 3];
            int prevV, curV = 0, nextV = Random.Range(0, vertices);
            for (int t = 0; t < tris.Length; t += 3)
            {
                prevV = curV;
                curV = nextV;
                nextV = Random.Range(0, vertices);
                tris[t] = prevV;// Random.Range(0, vertices);
                tris[t + 1] = curV;// Random.Range(0, vertices);
                tris[t + 2] = nextV;// Random.Range(0, vertices);
            }
        }
        else
        {
            var n = Random.Range(randomizeVerticesPerFrame.x, randomizeVerticesPerFrame.y);
            for (int i = 0; i < n; i++)
            {
                var v = Random.Range(0, verts.Length);
                verts[v] += Random.insideUnitSphere * randomizeDist;
                verts[v] = Vector3.ClampMagnitude(verts[v], 1f);
            }
        }

        m.vertices = verts;
        m.triangles = tris;

        m.RecalculateNormals();
        m.RecalculateBounds();

        if (setMesh)
        {
            mf.sharedMesh = m;
            System.DateTime ti = System.DateTime.Now;
            mc.sharedMesh = m;
            colliderSetTime = System.DateTime.Now.Subtract(ti).TotalMilliseconds;

        }
    }

}