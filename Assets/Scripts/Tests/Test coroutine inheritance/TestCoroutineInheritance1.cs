using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using MovementEffects;

public class TestCoroutineInheritance1 : MonoBehaviour
{
    
    protected int frameCounter = 0;

    private void Start()
    {
        Timing.RunCoroutine(CoroutineShit(), "Shift");
    }

    private void Update()
    {
        frameCounter++;
    }

    /// <summary>
    /// FOR FINDINGS <see cref="TestCoroutineInheritance2"/>
    /// </summary>
    public virtual IEnumerator<float> CoroutineShit()
    {
        print("2. RUNNING AT BASE CLASS. FRAMES: " + frameCounter + ";\ttime: " + Time.time + " " + (Time.time == 0 ? "YEAH!!!" : "nah"));
        yield break;
    }

}