using MovementEffects;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class TestCoroutineInheritance2 : TestCoroutineInheritance1
{
    // FINDINGS:
    // Timing.RunCoroutine(base.Coroutine()) adds the base method to the end of some execution list
    // and the base executes right after the child. So the order of messages will be: 1, 3, 4, and 2 (base). However, 4. and 2. happen at frameCounter == 1 and time == 0
    // so in a way this works, but one cannot call the base class before executing all the child stuff.
    public override IEnumerator<float> CoroutineShit()
    {
        Timing.RunCoroutine(base.CoroutineShit());

        print("1. CHILD START. FRAMES: " + frameCounter + ";\ttime: " + Time.time + " " + (Time.time == 0 ? "YEAH!!!" : "nah"));

        print("3. RUNNING AT CHILD CLASS. FRAMES: " + frameCounter + ";\ttime: " + Time.time + " " + (Time.time == 0 ? "YEAH!!!" : "nah"));

        yield return 0;

        print("4. ONE FRAME AFTER. FRAMES: " + frameCounter + ";\ttime: " + Time.time + " " + (frameCounter == 1 ? "YEAH!!!" : "nah"));

    }



}