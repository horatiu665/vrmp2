﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class AvalancheMakler : MonoBehaviour
{
    private static AvalancheMakler _instance;
    public static AvalancheMakler instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<AvalancheMakler>();
            }
            return _instance;
        }
    }

    [Header("Avalanche setup")]
    public VRPrefabType avalanchePrefab;

    private List<AvalanchePiece> spawnedAvalanche = new List<AvalanchePiece>();

    [Space]
    public int avalancheSize = 10;

    private AvalanchePiece theHeadPiece;

    [Header("The Head Piece params")]
    public float theMainPieceScale = 1.5f;
    public float theHeadPieceDelay = 0.5f;

    public float theHeadPieceScaleUpDuration = 0.1f;
    public float theHeadPieceScaleDownDuration = 0.1f;

    [Header("Normal Pieces Params")]
    public float turbulence = 1f;

    public float turbulenceVelocity = 5f;

    public float turbulenceRadius = 1f;

    public float pieceScale = 1f;

    public Vector2 scaleUpRange = new Vector2(0.2f, 0.5f);
    public Vector2 scaleDownRange = new Vector2(0.5f, 2f);

    /// <summary>
    /// returns delay until the avalanche is in FULL BLOW
    /// </summary>
    public float startDuration
    {
        get
        {
            return theHeadPieceDelay + theHeadPiece.scaleUpDuration;
        }
    }

    [DebugButton]
    public void InitAvalanche()
    {
        EndAvalanche();
        spawnedAvalanche.Clear();
        for (int i = 0; i < avalancheSize; i++)
        {
            // POOL INSTEAD NOOB
            GameObject go = FoggyForestPoolManager.instance.Spawn(avalanchePrefab, transform.position, transform.rotation).gameObject;
            go.transform.SetParent(transform);
            var ap = go.GetOrAddComponent<AvalanchePiece>();
            spawnedAvalanche.Add(ap);
            // transfer attributes
            ap.turbulence = turbulence;
            ap.turbulenceRadius = turbulenceRadius;
            ap.turbulenceVelocity = turbulenceVelocity;
            ap.delay = 0f;
            ap.scaleUpDuration = scaleUpRange.Random();
            ap.scaleDownDuration = scaleDownRange.Random();
            // init turbulence pos, vel
            ap.velocity = Random.onUnitSphere * turbulenceVelocity;
            ap.transform.localPosition = Random.onUnitSphere * turbulenceRadius;
            ap.scale = Vector3.one * pieceScale;
            ap.moveChaos = true;
            ap.ScaleUp();
        }

        GameObject gog = FoggyForestPoolManager.instance.Spawn(avalanchePrefab, transform.position, transform.rotation).gameObject;
        gog.transform.SetParent(transform);
        theHeadPiece = gog.GetOrAddComponent<AvalanchePiece>();
        theHeadPiece.turbulence = 0f;
        theHeadPiece.delay = theHeadPieceDelay;
        theHeadPiece.scaleUpDuration = theHeadPieceScaleUpDuration;
        theHeadPiece.scaleDownDuration = theHeadPieceScaleDownDuration;
        theHeadPiece.transform.localScale = Vector3.one * theMainPieceScale;
        theHeadPiece.scale = Vector3.one * theMainPieceScale;
        theHeadPiece.moveChaos = false;
        theHeadPiece.ScaleUp();
    }

    [DebugButton]
    public void EndAvalanche()
    {
#if UNITY_EDITOR
        if (!Application.isPlaying)
        {
            ClearAvalanche();
            return;
        }
#endif

        foreach (var ap in spawnedAvalanche)
        {
            ap.End();
        }
        spawnedAvalanche.Clear();
        if (theHeadPiece != null)
        {
            theHeadPiece.End();
            theHeadPiece = null;
        }
    }

    public void ClearAvalanche()
    {
        DestroyChildren();
    }

    [DebugButton]
    private void DestroyChildren()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            this.DestroySafe(transform.GetChild(i).gameObject);
        }
    }

}
