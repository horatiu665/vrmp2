﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class AvalanchePiece : MonoBehaviour, IFFPoolBehaviour
{
    float _scaleUpTime = 0f;
    float _scaleDownTime = 0f;


    [Header("Params from parent")]
    public float delay = 0;

    public bool moveChaos = true;
    public float turbulenceVelocity;
    public float turbulence;
    public float turbulenceRadius;

    public Vector3 velocity;

    public Vector3 scale;


    // private shit

    enum State
    {
        Culled = 0,
        ScaleUp,
        Normal,
        ScaleDown
    }

    private State _state;

    public float scaleUpDuration;
    public float scaleDownDuration;

    [SerializeField]
    private VRPrefabBase _vrPrefab;

    public VRPrefabBase vrPrefab
    {
        get
        {
            if (_vrPrefab == null)
            {
                _vrPrefab = GetComponent<VRPrefabBase>();
            }
            return _vrPrefab;
        }
    }

    private void OnValidate()
    {
        if (vrPrefab != null)
        {
            // gus kekin
        }
    }

    public void OnReturnToPool()
    {
        // nothing.
        _state = State.Culled;
    }


    public void OnSpawnFromPool()
    {
    }

    public void ScaleUp()
    {
        _state = State.ScaleUp;
        _scaleUpTime = Time.time + delay;
    }

    public void End()
    {
        _state = State.ScaleDown;
        _scaleDownTime = Time.time + delay;
    }

    void Update()
    {
        float t;
        switch (_state)
        {
        case State.Culled:
            // cull all
            return;
        case State.ScaleUp:
            t = (Time.time - _scaleUpTime) / scaleUpDuration;
            transform.localScale = scale * Mathf.SmoothStep(0f, 1f, t);
            if (t >= 1f)
            {
                _state = State.Normal;
            }
            break;
        case State.Normal:
            break;
        case State.ScaleDown:
            t = (Time.time - _scaleDownTime) / scaleDownDuration;
            transform.localScale = scale * Mathf.SmoothStep(1f, 0f, t);
            if (t >= 1f)
            {
                // return to pool
                FoggyForestPoolManager.instance.Return(vrPrefab);
            }
            break;
        default:
            break;
        }

        if (moveChaos)
        {
            MoveChaos();
        }
    }

    void MoveChaos()
    {
        velocity = Vector3.Lerp(velocity, Random.onUnitSphere * turbulenceVelocity, turbulence);
        var pos = transform.localPosition;
        pos = Vector3.ClampMagnitude(pos + velocity, turbulenceRadius);
        transform.localPosition = pos;
    }
}