﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class PlaceAllChildrenOnMeshRaycasts : MonoBehaviour
{
    [DebugButton]
    public void Place()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var c = transform.GetChild(i);
            RaycastHit hit;
            if (Physics.Raycast(c.position, Vector3.down, out hit, 10000f))
            {
                c.position = hit.point;
            }
        }
    }
}