using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class RomanticShaderManager : MonoBehaviour
{
    private static RomanticShaderManager _instance;
    public static RomanticShaderManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<RomanticShaderManager>();
            }
            return _instance;
        }
    }

    [Serializable]
    public class ShaderFloatValue
    {
        public string name;
        public float value;
        public float editorValue;
    }

    public List<ShaderFloatValue> floatValues = new List<ShaderFloatValue>()
    {
        new ShaderFloatValue() {name = "_FlatRadius", value = 2000 , editorValue = 2000},
        new ShaderFloatValue() {name = "_FalloffRadius", value = 350, editorValue = 350 },
        new ShaderFloatValue() {name = "_Offset", value = 20, editorValue = 0 },
    };

    [DebugButton]
    void ApplyValues()
    {
        SetValues(!Application.isPlaying);
    }

    [DebugButton]
    void ApplyOppositeValues()
    {
        SetValues(Application.isPlaying);
    }

    private void SetValues(bool isEditorMode)
    {
        for (int i = 0; i < floatValues.Count; i++)
        {
            var f = floatValues[i];
            Shader.SetGlobalFloat(f.name, isEditorMode ? f.editorValue : f.value);
        }
    }

    private void OnEnable()
    {
        ApplyValues();
    }

    private void OnValidate()
    {
        ApplyValues();
    }
}