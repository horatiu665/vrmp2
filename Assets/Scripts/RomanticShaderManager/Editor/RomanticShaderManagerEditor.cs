﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

[CustomPropertyDrawer(typeof(RomanticShaderManager.ShaderFloatValue))]
public class ShaderFloatValueDrawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return base.GetPropertyHeight(property, label) * 2 + 1;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		position.height = position.height / 2 - 1;
		var nameRect = position;
		var otherRect1 = position;
		otherRect1.width -= 20;
		otherRect1.x += 20;
		otherRect1.y += otherRect1.height + 1;
		var otherRect2 = otherRect1;
		otherRect1.width = otherRect1.width / 2 - 1;
		otherRect2.width = otherRect1.width;
		otherRect2.x += otherRect1.width + 1;

		EditorGUI.PropertyField(nameRect, property.FindPropertyRelative("name"));
		EditorGUI.PropertyField(otherRect1, property.FindPropertyRelative("value"));
		EditorGUI.PropertyField(otherRect2, property.FindPropertyRelative("editorValue"));

		property.serializedObject.ApplyModifiedProperties();
	}
}