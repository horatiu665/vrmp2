namespace Mountain.Editor
{
    using Helpers.Editor;
    using UnityEditor;
    using UnityEngine;

    [CanEditMultipleObjects]
    [CustomEditor(typeof(MountainFilter), true)]
    public class MountainFilterEditor : Editor
    {
        private MountainFilter _filter;

        private void OnEnable()
        {
            _filter = this.target as MountainFilter;
        }

        public override void OnInspectorGUI()
        {
            // buttons for apply and clear, then the rest of the controls

            EditorGUILayout.Separator();

            GUILayout.BeginHorizontal();
            if (GUILayout.Button("Generate"))
            {
                _filter.GenerateAt(Vector3.zero);
            }
            if (GUILayout.Button("Clear All"))
            {
                _filter.ClearAllInstant();
            }
            GUI.color = Color.green;
            if (GUILayout.Button("GenAll", GUILayout.Width(60)))
            {
                _filter.mountainManager.ClearAllInstant();
                _filter.mountainManager.GenerateAt(Vector3.zero);
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            EditorGUILayout.Separator();

            GUI.enabled = false;
            EditorGUILayout.Toggle("Is Applied", _filter.isApplied);
            GUI.enabled = true;

            base.OnInspectorGUI();
        }
    }
}