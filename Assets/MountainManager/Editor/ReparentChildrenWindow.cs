﻿using UnityEngine;
using UnityEditor;

public class ReparentChildrenWindow : EditorWindow
{
    Transform targetParent;

    [MenuItem("Tools/Reparent Children Window")]
    static void DoIt()
    {
        GetWindow<ReparentChildrenWindow>("Orphanage");
    }

    private void OnGUI()
    {
        targetParent = EditorGUILayout.ObjectField("Target Parent", targetParent, typeof(Transform), true) as Transform;
        if (GUILayout.Button("Reparent To Target"))
        {
            var undoName = "Reparent Children to " + targetParent.name;
            foreach (var s in Selection.GetTransforms(SelectionMode.TopLevel))
            {
                var list = s.GetChildren();
                foreach (var l in list)
                {
                    Undo.SetTransformParent(l, targetParent, undoName);
                    //l.SetParent(targetParent);
                }
            }
            Undo.IncrementCurrentGroup();
        }
    }
}