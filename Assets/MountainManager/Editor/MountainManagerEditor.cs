
namespace Mountain.Editor
{
    using System.IO;
    using System.Linq;
    using UnityEditor;
    using UnityEditorInternal;
    using UnityEngine;
    using VRCore.Editor;

    [CustomEditor(typeof(MountainManager))]
    public sealed class MountainManagerEditor : Editor
    {
        private MonoScript _script;
        public MonoScript script
        {
            get
            {
                if (_script == null)
                {
                    if (mountainManager != null)
                    {
                        _script = MonoScript.FromMonoBehaviour(mountainManager);
                    }
                }
                return _script;
            }
        }


        private ReorderableList _filterDisplayList;
        public ReorderableList filterDisplayList
        {
            get
            {
                if (_filterDisplayList == null)
                {
                    InitFilterList();
                }
                return _filterDisplayList;
            }
        }

        public bool initializedFilterList { get; private set; }

        private MountainManager _mountainManager;
        private MountainManager mountainManager
        {
            get
            {
                if (_mountainManager == null)
                {
                    _mountainManager = this.target as MountainManager;
                }
                return _mountainManager;
            }

            set
            {
                _mountainManager = value;
            }
        }

        //private void OnEnable()
        //{
        //    // reorder list on start.
        //    InitFilterList();
        //}

        public void InitFilterList()
        {
            // init reorderable list with ref to serialized object and its field. true true represents that the list is reorderable, and false false that we don't want the +- buttons at the bottom
            _filterDisplayList = new ReorderableList(mountainManager.filters, typeof(IMountainFilter), true, true, false, false);

            _filterDisplayList.headerHeight = 36;
            _filterDisplayList.drawHeaderCallback = DrawFilterListHeaderCallback;
            _filterDisplayList.drawElementCallback = DrawFilterListElementCallback;
            _filterDisplayList.onReorderCallback = ReorderFilterListCallback;

            initializedFilterList = true;

        }

        /// <summary>
        /// Reordering the list manually triggers a recalculation of the .order parameters in the filters.
        /// The algorithm is: if a filter is dragged to either end of the list,
        /// it is assumed to be necessary that its order is respected, so the order value is put as the adjacent value +- 1.
        /// However, if a filter is dragged in between two elements, there are two cases:
        /// if the elements are equal, the moved filter should have the same value as them;
        /// but if they are distinct, the moved filter should have their average value.
        /// If that means it is equal to either one, it has to shift the entire list by 1 so it makes some room for itself.
        ///
        /// TODO: make the reordered values "round", so have a preference for not reassigning the value 0,
        /// and choosing values such as 5, 10, 100, -20, -50, rather than 3, 7, 21 or other primes.
        /// </summary>
        /// <param name="list"></param>
        private void ReorderFilterListCallback(ReorderableList list)
        {
            serializedObject.Update();

            // the filter with the wrong filter.order is the currently selected one: filters[list.index].
            var filters = mountainManager.filters;
            var n = filters.Count;

            if (filters.Count > 1)
            {
                // edge cases:
                if (list.index == 0)
                {
                    filters[0].order = filters[1].order - 1;
                }
                else if (list.index == n - 1)
                {
                    filters[n - 1].order = filters[n - 2].order + 1;
                }
                else
                {
                    var w = list.index;
                    // the unordered filter is in the middle of the list.
                    // give it the value of average between the two neighbors. then deal with consequences
                    filters[w].order = (filters[w - 1].order + filters[w + 1].order) / 2;

                    // if filters' new neighbors are different, move them apart. if they are the same don't do anything, we wanted the filter to fit in here.
                    if (filters[w - 1].order != filters[w + 1].order)
                    {
                        if (filters[w].order == filters[w - 1].order)
                        {
                            for (int i = w - 1; i >= 0; i--)
                            {
                                filters[i].order -= 1;
                            }
                        }
                        else if (filters[w].order == filters[w + 1].order)
                        {
                            for (int i = w + 1; i < filters.Count; i++)
                            {
                                filters[i].order += 1;
                            }
                        }
                    }
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawFilterListHeaderCallback(Rect rect)
        {
            // SIZES CLONED FROM DrawFilterListElementCallback. UPDATE BOTH BEFORE SAVING!!!
            var applyButtonWidth = 70;
            var xButtonWidth = 24;
            var genTimeLabelWidth = 60;
            var buttonsOffset = 36;
            var orderButtonWidth = 54;

            serializedObject.Update();
            EditorGUI.LabelField(rect, "Filters", EditorStyles.boldLabel);
            var bgRect = new Rect(rect.x - 5, rect.y + 16, rect.width + 9, rect.height - 16);
            var darkGrey = 0.375f;
            EditorGUI.DrawRect(bgRect, new Color(darkGrey, darkGrey, darkGrey));

            var toggleRect = new Rect(rect.x + 14, rect.y + 16, 16, 16);
            var toggleGraphicsEnabled = mountainManager.filters.Any(f => !f.enabled);
            // if someone presses the toggle like a button
            if (EditorGUI.Toggle(toggleRect, GUIContent.none, !toggleGraphicsEnabled) == toggleGraphicsEnabled)
            {
                // if any filter is disabled, enable all first.
                if (toggleGraphicsEnabled)
                {
                    // enable all
                    EnableAllFilters(true);
                }
                else
                {
                    EnableAllFilters(false);
                }
            }

            var orderButtonRect = toggleRect;
            orderButtonRect.x += toggleRect.width + 8;
            orderButtonRect.width = orderButtonWidth;
            if (GUI.Button(orderButtonRect, "reorder"))
            {
                for (int i = 0; i < mountainManager.filters.Count; i++)
                {
                    mountainManager.filters[i].order = i;
                }
                mountainManager.SortFilters();
            }

            var genTimeLabelRect = toggleRect;
            genTimeLabelRect.x = rect.width - applyButtonWidth - xButtonWidth - genTimeLabelWidth * 2 + buttonsOffset - 14;
            genTimeLabelRect.width = genTimeLabelWidth * 2;
            var totalExecTime = mountainManager.generationTime;
            // draw label with sum of all generation times
            GUI.skin.label.alignment = TextAnchor.MiddleRight;
            GUI.Label(genTimeLabelRect, "Gen in " + totalExecTime.ToString("F0") + "ms");
            GUI.skin.label.alignment = TextAnchor.MiddleLeft;

            serializedObject.ApplyModifiedProperties();
        }

        private void EnableAllFilters(bool enabled)
        {
            for (int i = 0; i < mountainManager.filters.Count; i++)
            {
                var thisFilter = mountainManager.filters[i];
                var thisFilterMonobehaviour = (MonoBehaviour)thisFilter;
                thisFilterMonobehaviour.enabled = enabled;
                // if filters are enabled, set their gameobject to active.
                if (enabled)
                {
                    if (!thisFilterMonobehaviour.gameObject.activeInHierarchy)
                    {
                        thisFilterMonobehaviour.gameObject.SetActive(true);
                    }
                }
            }
        }

        private void DrawFilterListElementCallback(Rect rect, int index, bool isActive, bool isFocused)
        {
            serializedObject.Update();

            // widths
            var toggleWidth = 26;
            var orderWidth = 29;
            var orderToTextGap = 2;
            var applyButtonWidth = 70;
            var xButtonWidth = 24;
            var genTimeLabelWidth = 60;
            var buttonsOffset = 36;
            var gameobjectWidth = 36;

            // heights
            var rectHeight = rect.height;
            var boxHeight = 16;

            var thisFilter = mountainManager.filters[index];
            var thisFilterMonobehaviour = (MonoBehaviour)thisFilter;
            string filterName = thisFilterMonobehaviour.gameObject.name;// + " / " + thisFilter.ToString();

            if (mountainManager.filters.Count > index)
            {

                // Enabled toggle box
                var curRect = rect;
                curRect.width = toggleWidth;
                // set script enabled to exactly the value of filter.enabled
                thisFilterMonobehaviour.enabled = EditorGUI.Toggle(curRect, GUIContent.none, thisFilterMonobehaviour.enabled);

                // too buggy for now. 
                //// set gameObject.active to true if script is enabled and the gameObject isn't. but leave it false if script was disabled, whatever
                //if (thisFilterMonobehaviour.enabled && !thisFilterMonobehaviour.gameObject.activeInHierarchy)
                //{
                //    thisFilterMonobehaviour.gameObject.SetActive(true);
                //}

                // Order int field
                curRect.x += curRect.width;
                curRect.width = orderWidth;
                curRect.y += 2;
                curRect.height = boxHeight;
                thisFilter.order = EditorGUI.DelayedIntField(curRect, GUIContent.none, thisFilter.order);
                curRect.y -= 2;
                curRect.height = rectHeight;
                // when changing the order, all surrounding nodes should change their values
                // in case they interfere with the new order. the node should not instead jump around the list so you lose it

                // Filter name
                curRect.x += curRect.width + orderToTextGap;
                var labelX = curRect.x;

                // Label to show duration of generation
                curRect.x = rect.width - applyButtonWidth - xButtonWidth - genTimeLabelWidth + buttonsOffset;
                curRect.width = genTimeLabelWidth;

                GUI.skin.label.alignment = TextAnchor.MiddleRight;
                GUI.Label(curRect, thisFilter.generationFrames + "/" + thisFilter.generationTime.ToString("F0") + "ms");
                GUI.skin.label.alignment = TextAnchor.MiddleLeft;

                // Button for applying/reapplying filter (also shows w/ colors if the filter is applied or not)
                curRect.x = rect.width - applyButtonWidth - xButtonWidth + buttonsOffset;
                curRect.width = applyButtonWidth;
                if (GUIButtonColored(thisFilter, curRect, thisFilter.isApplied ? "Reapply" : "Apply"))
                {
                    ApplyFilterFancy(thisFilter);
                }

                // Button for clearing filter (X)
                curRect.x = rect.width - xButtonWidth + buttonsOffset;
                curRect.width = xButtonWidth;
                if (GUI.Button(curRect, "X"))
                {
                    ClearFilterFancy(thisFilter);
                }

                curRect.x = labelX;
                // width is what remains from label.left until buttons left
                curRect.width = rect.width - curRect.x - applyButtonWidth - genTimeLabelWidth - xButtonWidth + buttonsOffset;

                //GUI.Label(curRect, filterName);
                GUI.enabled = false;
                curRect.y += 2;
                curRect.height = boxHeight;
                var prevWidth = curRect.width;
                curRect.width = gameobjectWidth;
                EditorGUI.ObjectField(curRect, GUIContent.none, thisFilterMonobehaviour.gameObject, typeof(GameObject), true);
                GUI.enabled = true;
                curRect.width = prevWidth - gameobjectWidth - 1;
                curRect.x += gameobjectWidth + 1;
                bool isClearing = thisFilter.isSlowClearing;
                GUI.Label(curRect, (isClearing ? "*" : "") + filterName + "\t[" + thisFilterMonobehaviour.GetType().Name + "]");

            }
            else
            {
                // one of the elements is null. fuck!
                mountainManager.SortFilters();
            }

            serializedObject.ApplyModifiedProperties();

            // draw thisFilter's filter editor below. ghetto mode but at least it's usable.
            if (isActive)
            {
                EditorGUILayout.Separator();
                EditorGUILayout.BeginHorizontal();
                EditorGUILayout.LabelField("Ghetto editor for " + filterName, EditorStyles.boldLabel);
                if (GUILayout.Button("X", GUILayout.Width(24)))
                {

                }
                EditorGUILayout.EndHorizontal();

                var theEditor = Editor.CreateEditor(thisFilterMonobehaviour);
                theEditor.OnInspectorGUI();
                if (theEditor is MountainManagerEditor)
                {
                    var mme = (theEditor as MountainManagerEditor);
                    if (mme.initializedFilterList)
                    {
                        mme.InitFilterList();
                    }
                }
                EditorGUILayout.Separator();
            }
        }

        public override void OnInspectorGUI()
        {
            //DrawDefaultInspector();

            this.serializedObject.Update();

            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", script, typeof(MonoScript), false);
            EditorGUILayout.Toggle("Is Applied", mountainManager.isApplied);
            GUI.enabled = true;

            EditorGUILayout.PropertyField(serializedObject.FindProperty("_order"));


            EditorGUILayout.Separator();

            // if root mountain, control spawning. if not, take data from parent.
            MountainManager parentMountain = null;
            if (mountainManager.transform.parent != null)
            {
                parentMountain = mountainManager.transform.parent.GetComponentInParent<MountainManager>();
            }
            if (parentMountain != null)
            {
                EditorGUILayout.HelpBox("This mountain manager is not the root, therefore it does not control its own generation", MessageType.Warning);
                GUI.enabled = false;
            }
            EditorGUILayout.PropertyField(serializedObject.FindProperty("_regenGrid"));

            if (parentMountain != null)
            {
                GUI.enabled = true;
            }

            EditorGUILayout.Separator();

            // ============================== BUTTONS ==============================

            if (GUILayout.Button("Generate All"))
            {
                mountainManager.GenerateAt(Vector3.zero);
            }

            if (GUILayout.Button("Clear All"))
            {
                mountainManager.ClearAllInstant();
            }

            if (filterDisplayList != null)
            {
                filterDisplayList.DoLayoutList();
            }
            else
            {
                Debug.LogError("NullRef: The ReorderableList in the " + mountainManager.name + " SlopeManager inspector was null", mountainManager.gameObject);
            }

            if (GUILayout.Button("Destroy All Generated Children"))
            {
                mountainManager.DestroyAllGenChildren();
            }

            EditorGUILayout.Separator();

            this.serializedObject.ApplyModifiedProperties();

        }

        private void ApplyFilterFancy(IMountainFilter thisFilter)
        {
            // algo: clear all filters after this one, clear self, apply all unapplied previous filters, apply self. do not disturb the filters with equal order.
            var filters = mountainManager.filters;

            // which filters to clear? just the ones after self. doesn't matter if they're applied or not, due to manual meddling.
            var clearFilters = filters.Where(f => f.order > thisFilter.order).ToList();

            // clear all filters with order after self, except equal priority
            for (int j = clearFilters.Count - 1; j >= 0; j--)
            {
                clearFilters[j].ClearAllInstant();
            }
            thisFilter.ClearAllInstant();

            var applyFilters = filters.Where(f => f.order < thisFilter.order && !f.isApplied && f.enabled).ToList();
            // add this filter to the end of the list
            applyFilters.Add(thisFilter);

            // apply this filter only.
            for (int j = 0; j < applyFilters.Count; j++)
            {
                applyFilters[j].GenerateAt(Vector3.zero);
            }
        }

        private void ClearFilterFancy(IMountainFilter thisFilter)
        {
            // which filters to clear?
            // only filters whose order is larger than this one.
            var clearFilters = mountainManager.filters.Where(f => f.order > thisFilter.order).ToList();

            // clear all filters with order after self, except equal priority
            for (int j = clearFilters.Count - 1; j >= 0; j--)
            {
                clearFilters[j].ClearAllInstant();
            }
            thisFilter.ClearAllInstant();

        }

        private bool GUILayoutButtonColored(IMountainFilter filter, string buttonText, float buttonWidth)
        {
            GUI.color = filter.isApplied ? Color.green : Color.red;
            var buttonResult = GUILayout.Button(buttonText, GUILayout.Width(buttonWidth));
            GUI.color = Color.white;
            return buttonResult;
        }

        private bool GUIButtonColored(IMountainFilter filter, Rect rect, string buttonText)
        {
            GUI.color = filter.isApplied ? Color.green : Color.red;
            var buttonResult = GUI.Button(rect, buttonText);
            GUI.color = Color.white;
            return buttonResult;
        }
    }
}