﻿using Mountain;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;
using System;
using MovementEffects;
using VRCore;

public class MountainManagerWindow : EditorWindow
{
    private MountainManager _mm;
    public MountainManager mm
    {
        get
        {
            if (_mm == null)
            {

                var mms = FindObjectsOfType<MountainManager>();
                if (mms.Any())
                {
                    _mm = mms.First(m => m.mountainManager == null);
                }
            }
            return _mm;
        }
    }

    private Texture _mmTexGenerate;
    public Texture mmTexGenerate
    {
        get
        {
            if (_mmTexGenerate == null)
            {
                _mmTexGenerate = (Texture)AssetDatabase.LoadAssetAtPath(@"Assets\MountainManager\Editor\Resources\Icons\generate.png", typeof(Texture));
            }
            return _mmTexGenerate;
        }
    }
    private Texture _mmTexClearAll;
    public Texture mmTexClearAll
    {
        get
        {
            if (_mmTexClearAll == null)
            {
                _mmTexClearAll = (Texture)AssetDatabase.LoadAssetAtPath(@"Assets\MountainManager\Editor\Resources\Icons\clearall.png", typeof(Texture));
            }
            return _mmTexClearAll;
        }
    }
    private Texture _mmTexDestroyGenerated;
    public Texture mmTexDestroyGenerated
    {
        get
        {
            if (_mmTexDestroyGenerated == null)
            {
                _mmTexDestroyGenerated = (Texture)AssetDatabase.LoadAssetAtPath(@"Assets\MountainManager\Editor\Resources\Icons\destroy.png", typeof(Texture));
            }
            return _mmTexDestroyGenerated;
        }
    }
    private Texture _mmTexObjectsChanged;
    public Texture mmTexObjectsChanged
    {
        get
        {
            if (_mmTexObjectsChanged == null)
            {
                _mmTexObjectsChanged = (Texture)AssetDatabase.LoadAssetAtPath(@"Assets\MountainManager\Editor\Resources\Icons\objectschanged.png", typeof(Texture));
            }
            return _mmTexObjectsChanged;
        }
    }
    private Texture _mmTexMagicSave;
    public Texture mmTexMagicSave
    {
        get
        {
            if (_mmTexMagicSave == null)
            {
                _mmTexMagicSave = (Texture)AssetDatabase.LoadAssetAtPath(@"Assets\MountainManager\Editor\Resources\Icons\magicsave.png", typeof(Texture));
            }
            return _mmTexMagicSave;
        }
    }


    //private Vector2 _scroll;

    // chunk quicksave button
    private SpawnableChunkBase quickSaveChunks;
    private int lastDrawnCountNeedUpdate;

    private void OnEnable()
    {
        this.minSize = new Vector2(124, 40);
    }

    // Add menu named "My Window" to the Window menu
    [MenuItem("Tools/Mountain Manager")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        var window = GetWindow<MountainManagerWindow>("MountainMan");
        window.minSize = new Vector2(124, 40);
    }

    private void OnGUI()
    {
        //_scroll = GUILayout.BeginScrollView(_scroll);

        if (mm != null)
        {
            var style = new GUIStyle(GUI.skin.button);
            style.imagePosition = ImagePosition.ImageOnly;
            //style.padding = new RectOffset(0, 0, 0, 0);
            //style.border = new RectOffset(0, 0, 0, 0);
            //style.normal.background = null;
            //style.hover.background = null;
            //style.active.background = null;
            //style.focused.background = null;
            style.fixedHeight = mmTexClearAll.height / 2;

            var bgColor = new Color(73f / 255, 165f / 255, 1, 1);
            var genColor = new Color(134f / 255, 1, 200f / 255, 1);
            var changedColor = new Color(1f, 210f / 255, 90f / 255, 1);

            // invalid if the mountain manager is not applied, yet we have a generated parent in the scene with something as a child. obs: gen parent can exist when the mountain is cleared too.
            var mmInvalid = !mm.isApplied && MountainManagerGeneratedParent.instance != null && MountainManagerGeneratedParent.instance.GetComponentInChildren<Renderer>();

            GUILayout.BeginHorizontal();
            {
                GUIContent create = new GUIContent(mmTexGenerate, "Generates the mountain");
                GUIContent clear = new GUIContent(mmTexClearAll, "Clears the mountain");
                GUIContent destroy = new GUIContent(mmTexDestroyGenerated, "Destroys all generated objects");
                lastDrawnCountNeedUpdate = EditorSpawnInfoManager.countNeedUpdate;
                GUIContent changed = new GUIContent(lastDrawnCountNeedUpdate.ToString(), mmTexObjectsChanged, "Some spawned objects were changed, must be updated. Shift + click to select them, click to update them");

                // green "Generate" if not applied and not invalid.
                GUI.backgroundColor = mm.isApplied ? bgColor : mmInvalid ? bgColor : genColor;
                if (GUILayout.Button(create, style, GUILayout.MinWidth(36)))
                {
                    if (mm != null)
                    {
                        mm.GenerateAt(Vector3.zero);
                    }
                }

                GUI.backgroundColor = bgColor;
                if (GUILayout.Button(clear, style, GUILayout.Width(36)))
                {
                    if (mm != null)
                    {
                        mm.ClearAllInstant();
                    }
                }

                // red Destroy Children if mm is applied or invalid.
                GUI.backgroundColor = (mm.isApplied || mmInvalid) ? Color.red : bgColor;
                if (GUILayout.Button(destroy, style, GUILayout.MinWidth(36)))
                {
                    if (mm != null)
                    {
                        mm.DestroyAllGenChildren();
                    }
                }

                style.imagePosition = ImagePosition.ImageLeft;

                var anyNeedsUpdate = EditorSpawnInfoManager.anyNeedsUpdate;
                // updateable button red when mm not applied or invalid, blue normally and green when something needs update.
                GUI.backgroundColor = mm.isApplied || mmInvalid ? (anyNeedsUpdate ? changedColor : genColor) : Color.red;
                GUI.enabled = anyNeedsUpdate && mm.isApplied && !mmInvalid;
                if (GUILayout.Button(changed, style, GUILayout.Width(46)))
                {
                    // first press selects, the next press updates.
                    var selObj = EditorSpawnInfoManager.GetAllWhoNeedUpdate().Select(si => si.gameObject).ToArray();
                    if (selObj.Count() != Selection.objects.Length)
                    {
                        Selection.objects = selObj;
                    }
                    else
                    {
                        // update all objects via the EditorSpawnInfo scripts
                        EditorSpawnInfoManager.UpdateAll();

                    }

                }
                GUI.enabled = true;
                GUI.backgroundColor = Color.white;

                style.imagePosition = ImagePosition.ImageOnly;

                GUILayout.BeginVertical(GUILayout.Width(16));
                GUILayout.Space(3);
                if (GUILayout.Button(">", GUILayout.Height(14)))
                {
                    // reset reference.
                    _mm = null;
                    if (mm != null)
                    {
                        Selection.activeObject = mm.gameObject;
                        EditorGUIUtility.PingObject(mm.gameObject);
                    }
                }

                var mmgp = MountainManagerGeneratedParent.instance;
                GUI.enabled = mmgp != null;
                if (GUILayout.Button("<", GUILayout.Height(14)))
                {
                    if (mmgp != null)
                    {
                        Collapse(mmgp.gameObject);
                    }
                }
                GUI.enabled = true;
                GUILayout.EndVertical();

            }
            GUILayout.EndHorizontal();

            // magic save
            GUILayout.BeginHorizontal();

            if (GUILayout.Button(new GUIContent("MAGIC SAVE", mmTexMagicSave, "Saves objects magically"), GUILayout.Height(style.fixedHeight)))
            {
                // saves all IVRPrefabs from selected object and children into the quickSaveChunks, as a quicker method than selecting it and clicking the debug button.
                var prefabsToSave = Selection.gameObjects.SelectMany(go => go.GetComponentsInChildren<IVRPrefab>());
                SpawnableChunksUtils.MagicSaveThosePrefabs(prefabsToSave);

            }

            GUILayout.EndHorizontal();


            EditorGUILayout.Separator();
            EditorGUILayout.Separator();
            EditorGUILayout.Separator();

            GUILayout.BeginHorizontal();

            if (GUILayout.Button("Save >", GUILayout.Height(16)))
            {
                int count = 0;
                // saves all IVRPrefabs from selected object and children into the quickSaveChunks, as a quicker method than selecting it and clicking the debug button.
                var prefabsToSave = Selection.gameObjects.SelectMany(go => go.GetComponentsInChildren<IVRPrefab>());
                foreach (var ps in prefabsToSave)
                {
                    ps.transform.SetParent(quickSaveChunks.transform);
                    count++;
                }
                quickSaveChunks.AddFromChildren();
                quickSaveChunks.DestroyChildren();
                Debug.Log("[MountainMan] Saved " + count + " objects into " + quickSaveChunks);
            }

            // because unity editor scripting is retarded we cannot just make buttons and fields. we have to REALIGN THE FUCKING PIXELS
            var rect = EditorGUILayout.GetControlRect();
            rect.y += 2;
            quickSaveChunks = (SpawnableChunkBase)EditorGUI.ObjectField(rect, quickSaveChunks, typeof(SpawnableChunkBase), true);


            GUILayout.EndHorizontal();

        }
        else
        {
            EditorGUILayout.LabelField("MountainManager not found");
        }

        //GUILayout.EndScrollView();
    }

    private void OnInspectorUpdate()
    {
        var count = EditorSpawnInfoManager.countNeedUpdate;
        if (count != lastDrawnCountNeedUpdate)
        {
            // draw again
            this.Repaint();
        }
    }

    private void Collapse(GameObject go)
    {
        Collapse(go, false);
    }

    // stolen from http://answers.unity3d.com/questions/656869/foldunfold-gameobject-from-code.html
    public void Collapse(GameObject go, bool open)
    {
        // bail out immediately if the go doesn't have children
        if (go.transform.childCount == 0)
            return;
        // get a reference to the hierarchy window
        var hierarchy = GetFocusedWindow("Hierarchy");

        var prevSelect = Selection.activeObject;

        // select our go
        SelectObject(go);

        EditorCoroutine.Start(pTween.WaitFrames(1, () =>
        {

            // create a new key event (RightArrow for collapsing, LeftArrow for folding)
            var key = new Event { keyCode = open ? KeyCode.RightArrow : KeyCode.LeftArrow, type = EventType.KeyDown };
            // finally, send the window the event
            hierarchy.SendEvent(key);

            SelectObject(null);

        }));
    }

    public static void SelectObject(UnityEngine.Object obj)
    {
        Selection.activeObject = obj;
    }

    public static EditorWindow GetFocusedWindow(string window)
    {
        FocusOnWindow(window);
        return EditorWindow.focusedWindow;
    }

    public static void FocusOnWindow(string window)
    {
        EditorApplication.ExecuteMenuItem("Window/" + window);
    }

}