﻿namespace Mountain
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Random = UnityEngine.Random;

    public class MountainMeshUnderground : MonoBehaviour
    {
        [SerializeField, ReadOnly]
        MountainManager mm;

        [SerializeField, ReadOnly]
        MeshFilter mm_mf;

        [SerializeField, ReadOnly]
        MeshFilter mf;

        private void OnValidate()
        {
            if (isActiveAndEnabled)
            {
                mm = GetComponentInParent<MountainManager>();
                mm_mf = mm.GetComponent<MeshFilter>();
                mf = GetComponent<MeshFilter>();
                InitSharedMesh();
            }
        }

        private void OnEnable()
        {
            InitSharedMesh();
        }

        private void InitSharedMesh()
        {
            // set my mesh to be the mesh of the mountainmanager
            mf.sharedMesh = mm_mf.sharedMesh;
        }

        private void Reset()
        {
            var mr = this.GetOrAddComponent<MeshRenderer>();
            mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
            mr.receiveShadows = false;
        }

    }
}