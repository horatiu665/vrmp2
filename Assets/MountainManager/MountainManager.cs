namespace Mountain
{

    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;
    using UnityEngine.Serialization;
    using VRCore;
    using VRCore.VRNetwork;
    using MovementEffects;
    using System;

    public class MountainManager : MountainFilter
    {
        [SerializeField, HideInInspector]
        private Transform _generatedParent;

        // used for grouping all filters. NO SPAWNED OBJECTS SHOULD BE HERE!
        [SerializeField, HideInInspector]
        private Transform _filtersParent;

        private List<IMountainFilter> _filters;

        // used for grouping all filters. THIS SHOULD BE PARENTED UNDER SLOPE ITSELF! NO SPAWNED OBJECTS SHOULD BE HERE!
        public Transform filtersParent
        {
            get
            {
                if (_filtersParent == null)
                {
                    _filtersParent = new GameObject("Filters").transform;
                    _filtersParent.SetParent(this.transform);
                    _filtersParent.localPosition = Vector3.zero;
                    _filtersParent.localRotation = Quaternion.identity;
                }
                return _filtersParent;
            }
        }

        // used for grouping all spawned objects. NO FILTERS SHOULD BE HERE! because they will be deleted on ClearAll()
        public Transform generatedParent
        {
            get
            {
                if (mountainManager != null)
                {
                    return mountainManager.generatedParent;
                }
                if (_generatedParent == null)
                {
                    if (MountainManagerGeneratedParent.instance != null)
                    {
                        _generatedParent = MountainManagerGeneratedParent.instance.transform;
                    }
                }
                if (_generatedParent == null)
                {
                    _generatedParent = new GameObject("[Generated] by MountainManager", typeof(MountainManagerGeneratedParent)).transform;
                    // should not be parented under this because it will lag when selecting it for editing.
                    _generatedParent.SetParent(null);
                    _generatedParent.SetSiblingIndex(transform.GetSiblingIndex() + 1);
                    _generatedParent.localPosition = Vector3.zero;
                    _generatedParent.localRotation = Quaternion.identity;
                    _generatedParent.GetComponent<MountainManagerGeneratedParent>().mountainManager = this;
                }
                return _generatedParent;
            }
        }

        public List<IMountainFilter> filters
        {
            get
            {
                if (_filters == null)
                {
                    _filters = new List<IMountainFilter>();
                    SortFilters();
                }
                return _filters;
            }
        }

        [SerializeField]
        private float _regenGrid = 150;
        public float regenGrid
        {
            get
            {
                if (mountainManager != null)
                {
                    return mountainManager.regenGrid;
                }
                return _regenGrid;
            }
        }

        public override void GenerateAt(params Vector3[] extraPos)
        {
            base.GenerateAt(extraPos);

            GenerateAll(extraPos);

        }

        private void GenerateAll(params Vector3[] extraPos)
        {
            for (int i = 0; i < filters.Count; i++)
            {
                if (filters[i].enabled)
                {
                    filters[i].GenerateAt(extraPos);
                }
                else
                {
                    if (filters[i].isApplied)
                    {
                        filters[i].ClearAllInstant();
                    }
                }
            }

            DoneApplying();
        }

        public void DestroyAllGenChildren()
        {
            ClearFilters();
            if (generatedParent != null)
            {
                this.DestroySafe(generatedParent.gameObject);
            }
            ClearAllInstant();
        }

        /// <summary>
        /// starts the clearing algorithm over multiple frames. CheckAllClear() will be true when all filters are clear.
        /// </summary>
        public override void ClearSlow()
        {
            base.ClearSlow();

            for (int i = 0; i < filters.Count; i++)
            {
                if (filters[i].enabled)
                {
                    filters[i].ClearSlow();
                }
            }
        }

        /// <summary>
        /// True when all filters are clear
        /// </summary>
        /// <returns></returns>
        public bool CheckAllClear()
        {
            for (int i = 0; i < filters.Count; i++)
            {
                // if they are not applied OR if they are clearing (which assumes they are not applied anyway), it's NOT clear.
                if (filters[i].isSlowClearing)
                {
                    return false;
                }
            }
            return true;
        }

        public override void ClearAllInstant()
        {
            base.ClearAllInstant();

            ClearFilters();

            SortFilters();

            DoneClearing();
        }

        private void ClearFilters()
        {
            for (int i = 0; i < filters.Count; i++)
            {
                filters[i].ClearAllInstant();
            }
        }

        public void SortFilters()
        {
            // clear instead of reassign list so it keeps the reference in the inspector.
            filters.Clear();

            // add from self
            var unorderedList = GetComponents<IMountainFilter>().Where(mf => mf != (IMountainFilter)this).ToList();

            // add one layer of filters. in case there are more mountain managers in there.
            for (int i = 0; i < filtersParent.childCount; i++)
            {
                unorderedList.AddRange(filtersParent.GetChild(i).GetComponents<IMountainFilter>());
            }

            // THIS SORT REORDERS ELEMENTS WITH THE SAME ORDER. THIS SHOULD NOT HAPPEN, THEY SHOULD KEEP THEIR ORDER.
            // PLEASE MAKE SURE IT DOES THAT BEFORE CHANGING THE LINQ ORDERBY() BELOW.....!!!!
            //_filters.AddRange(unorderedList);
            //_filters.Sort((a, b) => { return a.order.CompareTo(b.order); }); // also it's just ints. why use crazy comparison shit?

            // add the orderBY result directly without ToList()
            filters.AddRange(unorderedList.OrderBy(f => f.order));

        }
        
        private void Update()
        {
            if (isSlowClearing)
            {
                if (CheckAllClear())
                {
                    DoneClearing();
                }
            }
            
        }

    }
}