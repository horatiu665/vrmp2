﻿namespace Mountain
{
    using UnityEngine;
    using System.Collections;
    using MovementEffects;
    using System.Collections.Generic;
    using VRCore;
    using VRCore.VRNetwork;
    using System.Linq;

    public class MountainManagerController : MonoBehaviour, IOriginShifter
    {
        private static MountainManagerController _instance;
        public static MountainManagerController instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<MountainManagerController>();
                }
                return _instance;
            }
        }


        private MountainManager _mountainManager;
        public MountainManager mountainManager
        {
            get
            {
                if (_mountainManager == null)
                {
                    _mountainManager = GetComponent<MountainManager>();
                }
                return _mountainManager;
            }
        }

        [SerializeField]
        public Transform[] followTargets = new Transform[0];

        public bool autoGenerating = true;

        public bool destroyFirstOnStart = true;

        private float regenGrid
        {
            get
            {
                return mountainManager.regenGrid;
            }
        }

        private Vector3[] followPos = new Vector3[0];
        private List<Vector3> followOldPos = new List<Vector3>();

        private IEnumerator<float> PlayerTarget()
        {
            while (PlayerManager.GetLocalPlayer<INetPlayer>() == null)
            {
                yield return 0;
            }
            followTargets = new Transform[1];
            followTargets[0] = PlayerManager.GetLocalPlayer<INetPlayer>().transform;
        }

        float checkTime_PlayersServerTarget = 0f;

        private void PlayersServerTargetUpdate()
        {
            if (Time.time > checkTime_PlayersServerTarget)
            {
                checkTime_PlayersServerTarget = Time.time + 0.5f;

                if (followTargets.Length != PlayerManager.allPlayers.Count)
                {
                    followTargets = PlayerManager.allPlayers.Select(r => r.transform).ToArray();
                }

            }
        }

        private void Start()
        {
            if (destroyFirstOnStart)
            {
                mountainManager.DestroyAllGenChildren();
            }

            if (!NetServices.isServer)
            {
                Timing.RunCoroutine(PlayerTarget());
            }
        }

        private void Update()
        {
            if (NetServices.isServer)
            {
                PlayersServerTargetUpdate();
            }

            if (autoGenerating && followTargets != null && followTargets.Length > 0)
            {
                if (followPos.Length != followTargets.Length)
                {
                    followPos = new Vector3[followTargets.Length];
                    followOldPos = followPos.ToList();
                }
                for (int i = 0; i < followPos.Length; i++)
                {
                    if (followTargets[i] != null)
                    {
                        followPos[i] = GridifyFollowPos(followTargets[i].position);
                    }
                }

                // if followPos has moved in the grid
                var changedPos = false;
                for (int i = 0; i < followPos.Length; i++)
                {
                    if (followPos[i] != followOldPos[i])
                    {
                        followOldPos[i] = followPos[i];
                        changedPos = true;
                        // do all of them so we keep track of the new overall state of positions
                    }
                }

                if (changedPos)
                {
                    // we finished clearing, and we still didn't move so we have to generate now.
                    mountainManager.GenerateAt(followPos);
                }
            }
        }

        private Vector3 GridifyFollowPos(Vector3 followPos)
        {
            // snap to grid
            followPos.x = Mathf.Round(followPos.x / regenGrid) * regenGrid;
            followPos.z = Mathf.Round(followPos.z / regenGrid) * regenGrid;
            followPos.y = 0;
            return followPos;
        }

        public void ClearBeforeGeneratingNextAndMoveOrigin()
        {
            autoGenerating = false;
            mountainManager.ClearSlow();
            StartCoroutine(pTween.WaitCondition(() =>
            {
                return !mountainManager.isSlowClearing;
            }, () =>
            {
                if (followTargets.Length > 0)
                {
                    if (followOldPos.Count < followTargets.Length)
                    {
                        followOldPos.Add(new Vector3());
                    }
                    // move origin BEFORE generating again!
                    // global position means current coord system plus originShift ("as if the origin of the world was here")
                    OriginShiftManager.FocusOn(OriginShiftManager.LocalToGlobalPos(followTargets[0].position));

                    autoGenerating = true;
                    var pos = GridifyFollowPos(followTargets[0].position);
                    mountainManager.GenerateAt(pos);
                    followOldPos[0] = pos;

                }
            }));
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
        }

        public void OnWorldMove(Vector3 originShiftDelta)
        {
            for (int i = 0; i < followOldPos.Count; i++)
            {
                followOldPos[i] -= originShiftDelta;
            }
        }

    }
}