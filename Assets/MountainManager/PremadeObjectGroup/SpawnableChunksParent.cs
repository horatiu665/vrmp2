using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;
using Mountain.Filters;
using System;

public class SpawnableChunksParent : MonoBehaviour
{
    private static SpawnableChunksParent _instance;
    public static SpawnableChunksParent instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<SpawnableChunksParent>();
            }
            return _instance;
        }
    }


}