using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class BigObjects : MonoBehaviour
{
	public List<BigObject> list = new List<BigObject>();

	private void OnValidate()
	{ 
		list = GetComponentsInChildren<BigObject>().ToList();
	}

	[DebugButton]
	void ConvertChildrenToBigObject()
	{
		var n = transform.childCount;
		// for all children. opposite dir, in case we add some children.
		for (int i = n - 1; i >= 0; i--)
		{
			var t = transform.GetChild(i);

			// if they're not already big objects
			var boScript = t.GetComponent<BigObject>();
			if (boScript == null)
			{
				// make a new big object, child of this
				var bo = new GameObject("[BO] " + t.name, typeof(BigObject)).GetComponent<BigObject>();
				bo.transform.SetParent(transform);

				// set bigObject data
				t.SetParent(bo.transform);

                bo.UpdateBigObject(t);

				// perhaps delete the prefab?
				//bo.DeleteChild();

			}
		}
		list = GetComponentsInChildren<BigObject>().ToList();

	}

	[DebugButton]
	void SpawnAllPrefabs()
	{
		list = GetComponentsInChildren<BigObject>().ToList();
		// for all children
		for (int i = 0; i < list.Count; i++)
		{
			var bo = list[i];
			bo.SpawnBigObject();
		}
	}

	[DebugButton]
	void DestroyAllPrefabs()
	{
		list = GetComponentsInChildren<BigObject>().ToList();
		for (int i = 0; i < list.Count; i++)
		{
			list[i].DeleteChild();
		}
	}

}