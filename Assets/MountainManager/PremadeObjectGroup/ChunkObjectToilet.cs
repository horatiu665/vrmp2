﻿using UnityEngine;
using VRCore;

[System.Serializable]
public class ChunkObjectToilet : ChunkObject
{
    public int toiletId = -1;

    public Vector3 spawnPos; // stores the position of the teleportTransform, so the player can spawn there without the toilet actually being spawned yet.

    public override IVRPrefab Spawn(Transform parent)
    {
        var obj = base.Spawn(parent);

        var tt = obj.GetComponent<ToiletTeleporter>();
        tt.toiletId = toiletId;

        return obj;
    }

}
