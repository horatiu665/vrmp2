using Mountain.Filters;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class BigObject : MonoBehaviour
{
    [Header("Object to spawn when any of the coords are in range")]
    public VRPrefabType bigObjPrefab;
    public int gridSize = 150;

    [Header("Pos and rotation in global space")]
    public Vector3 position;
    public Quaternion rotation;
    public Vector3 localScale;

    [Header("Coords bounds. When whichever corner is in range, object is spawned.")]
    public SpawnJobCoord minCoord;
    public SpawnJobCoord maxCoord;

    [Header("Debug")]
    [SerializeField]
    private bool showGizmos;

    // returns success
    public bool UpdateBigObject(Transform bigObj)
    {
        bool succ = SetupBigObjectPosition(bigObj);
        if (succ)
        {
            SetCoordsFromMeshBounds(bigObj);
        }

        return succ;
    }

    [DebugButton]
    private void GetPositionFromChild()
    {
        if (transform.childCount > 0)
        {
            var mr = transform.GetChild(0);
            SetupBigObjectPosition(mr);
        }
    }

    private bool SetupBigObjectPosition(Transform bigObject)
    {
        var ivrpre = bigObject.GetComponent<IVRPrefab>();
        if (ivrpre != null)
        {
            bigObjPrefab = ivrpre.prefabType;
            position = OriginShiftManager.LocalToGlobalPos(bigObject.position);
            rotation = bigObject.rotation;
            localScale = bigObject.localScale; // TODO: instead of one, use this obj. scale divided by the prefab scale? make sure to update spawner accordingly
            return true;
        }
        return false;
    }

    [DebugButton]
    private void SetCoordsFromMeshBounds()
    {
        SetCoordsFromMeshBounds(transform);
    }

    private void SetCoordsFromMeshBounds(Transform bigObject)
    {
        minCoord.x = 0;
        minCoord.y = 0;
        maxCoord.x = 0;
        maxCoord.y = 0;
        var osCoord = SpawnJobCoord.PosToCoord(OriginShiftManager.originShift, gridSize);
        foreach (var mr in bigObject.GetComponentsInChildren<MeshRenderer>())
        {
            var mic = SpawnJobCoord.PosToCoord(mr.bounds.min, gridSize) + osCoord;
            var mac = SpawnJobCoord.PosToCoord(mr.bounds.max, gridSize) + osCoord;
            if (minCoord.x > mic.x || minCoord.x == 0)
            {
                minCoord.x = mic.x;
            }
            if (minCoord.y > mic.y || minCoord.y == 0)
            {
                minCoord.y = mic.y;
            }
            if (maxCoord.x < mac.x || maxCoord.x == 0)
            {
                maxCoord.x = mac.x;
            }
            if (maxCoord.y < mac.y || maxCoord.y == 0)
            {
                maxCoord.y = mac.y;
            }
        }
    }

    public bool ShouldBeSpawned(SpawnJobCoord coords)
    {
        return coords.x >= minCoord.x && coords.x <= maxCoord.x
            && coords.y >= minCoord.y && coords.y <= maxCoord.y;
    }

    [DebugButton]
    public void SpawnBigObject()
    {
        var go = VRPrefabManager.instance.Spawn(bigObjPrefab, OriginShiftManager.GlobalToLocalPos(position), rotation);
        go.transform.SetParent(transform);
        go.transform.localScale = localScale;
    }

    [DebugButton]
    public void DeleteChild()
    {
        if (transform.childCount > 0)
        {
            this.DestroySafe(transform.GetChild(0).gameObject);
        }
    }


    #region grizmos shit
    private void OnDrawGizmos()
    {
        if (!showGizmos)
            return;

        var originShiftCoord = SpawnJobCoord.PosToCoord(OriginShiftManager.originShift, gridSize);
        for (int x = minCoord.x; x <= maxCoord.x; x++)
        {
            for (int y = minCoord.y; y <= maxCoord.y; y++)
            {
                GizmoDrawCoord(new SpawnJobCoord(x, y) - originShiftCoord);
            }
        }
    }

    private void GizmoDrawCoord(SpawnJobCoord c)
    {
        Gizmos.color = Color.yellow * 0.4f;

        Gizmos.DrawCube(SpawnJobCoord.CoordToPos(c, gridSize), Vector3.one * gridSize * 0.8f);
    }
    #endregion

}