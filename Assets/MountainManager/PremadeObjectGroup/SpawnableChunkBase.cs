using Mountain.Filters;
using System.Collections.Generic;
using UnityEngine;
using System;

public abstract class SpawnableChunkBase : MonoBehaviour
{
    // Instead of GetSpawnList() where I have to do crazy hacks because unity can't serialize interfaces, I do GetNotNull and GetElement directly, returning an interface. Then I don't have to communicate the list itself, just access its elements.
	//public abstract IEnumerable<IChunkObject> GetSpawnList(SpawnJobCoord chunk);
    public abstract bool GetIsSpawnListNotNull(SpawnJobCoord chunk, out int listIndex, out int listCount);
    // Beware! Must use GetIsSpawnListNotNull() before using GetSpawnListElement. This is a hack due to unity's inability to serialize interfaces, read more at GetIsSpawnListNotNull() description
    public abstract IChunkObject GetSpawnListElement(int listIndex, int index);

	public abstract int gridSize { get; }

    public abstract bool showGizmo { get; set; }

    /// <summary>
    /// Updates object spawn data after presumably being changed in the scene. Returns true if successful, false if not successful.
    /// </summary>
    /// <param name="originalData">original data class ref saved in spawner object memory</param>
    /// <param name="spawnedObject">the gameobject that was spawned. will be cast to IVRPrefab</param>
    /// <returns>true when successfully updated data</returns>
    public abstract bool UpdateObjectData(IChunkObject originalData, GameObject spawnedObject);

    /// <summary>
    /// Deletes object from spawner data. Returns true if successful, false if not.
    /// </summary>
    /// <param name="originalData">original data class ref saved in spawner object memory</param>
    /// <param name="spawnedObject">the gameobject that was spawned. will be cast to IVRPrefab</param>
    /// <returns>true when successfully found and deleted the object</returns>
    public abstract bool DeleteFromSpawner(IChunkObject originalData, GameObject spawnedObject);

    public abstract void AddFromChildren();

    public abstract void DestroyChildren();
}