using UnityEngine;
using VRCore;

[System.Serializable]
public class ChunkObjectSlalomPole : ChunkObject
{
	public int levelId = -1;

	public int orderInLevel = -1;

	public bool isRight;

	public bool isEnd;

	public bool isStart;
    
	public override IVRPrefab Spawn(Transform parent)
	{
		var obj = base.Spawn(parent);

		var sp = obj.GetComponent<SlalomPoleComponent>();
		sp.levelId = levelId;
		sp.orderInLevel = orderInLevel;
		sp.isRight = isRight;
		sp.isEnd = isEnd;
		sp.isStart = isStart;

		sp.Initialize();

		return obj;
	}

}
