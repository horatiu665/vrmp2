using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class PremadeObjectGroup : MonoBehaviour
{
	[HelpBox("Generate this list using the debug button. All prefabs will be deleted on startup and spawned at runtime.", HelpBoxType.Info)]
	public List<ChunkObject> spawnObjects = new List<ChunkObject>();

	[DebugButton]
	private void GenerateFromChildren()
	{
		if (transform.childCount == 0)
		{
			Debug.LogError("HORRIBLE WARNING: Transform has no children. You might have mistakenly cleared your cool premade object group.");
			return;
		}

		spawnObjects.Clear();
		for (int i = 0; i < transform.childCount; i++)
		{
			var c = transform.GetChild(i);
			spawnObjects.Add(new ChunkObject()
			{
				prefabType = c.GetComponent<IVRPrefab>().prefabType,
				globalPos = c.localPosition,
				rot = c.localRotation,
				localScale = c.localScale
			});
		}
	}

	[DebugButton]
	private void SpawnFromList()
	{
		for (int i = 0; i < spawnObjects.Count; i++)
		{
			var s = spawnObjects[i];
			var pp = s.Spawn(transform);
			pp.position += transform.position;
			pp.rotation *= transform.rotation;

		}
	}

	[DebugButton]
	private void DestroyChildren()
	{
		for (int i = transform.childCount - 1; i >= 0; i--)
		{
			this.DestroySafe(transform.GetChild(i).gameObject);
		}
	}

}