using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;
using Mountain.Filters;
using System;

public class PremadeObjectGroupChunks : SpawnableChunkBase
{
    [HelpBox("Objects can be spawned in chunks using a job filter. Save them at runtime after painting with whatever tools.", HelpBoxType.Info)]
    public int _gridSize = 150;
    public override int gridSize { get { return _gridSize; } }

    #region ARRAY SHIT

    // 2D array of lists. but 1d for serialization
    [HideInInspector]
    [SerializeField]
    ListRaper[] _spawnedObjects2D = new ListRaper[0];

    // index offset for getting array elements. ultimately kind of pretends to be a dictionary. To iterate the array, use -_arrayIndexOffset at the beginning.
    [HideInInspector]
    [SerializeField]
    SpawnJobCoord _arrayIndexOffset;

    [HideInInspector]
    [SerializeField]
    SpawnJobCoord _arrayMaxSize = new SpawnJobCoord(0, 0);

    private SpawnJobCoord minCoordEver
    {
        get
        {
            return new SpawnJobCoord(-_arrayIndexOffset.x, -_arrayIndexOffset.y);
        }
    }

    private SpawnJobCoord maxCoordEver
    {
        get
        {
            return _arrayMaxSize - _arrayIndexOffset - new SpawnJobCoord(1, 1);
        }
    }

    // Used for counting _spawnedObjects rather than opening the list in the inspector which might cause lag for like 5000+ objects..
    [SerializeField, ReadOnly]
    protected int _spawnedCount;

    [Header("Gizmos")]
    [SerializeField]
    private bool showGizmos = true;
    public override bool showGizmo { get { return showGizmos; } set { showGizmos = value; } }

    [SerializeField]
    private Gradient occupancyGizmoColors = new Gradient()
    {
        colorKeys = new GradientColorKey[] { new GradientColorKey(Color.white, 0f), new GradientColorKey(Color.yellow, 0.1f), new GradientColorKey(Color.red, 1f) },
        alphaKeys = new GradientAlphaKey[] { new GradientAlphaKey(0.3f, 0f), new GradientAlphaKey(0.5f, 0.95f), new GradientAlphaKey(0.85f, 1f) },
        mode = GradientMode.Blend
    };

    [SerializeField]
    private int gizmoCountMax = 42;

    private int w
    {
        get
        {
            return _arrayMaxSize.x;
        }
    }

    private int h
    {
        get
        {
            return _arrayMaxSize.y;
        }
    }

    // MUST be called before operations where the coords are likely to be out of bounds
    // this tells the array what position MUST be contained, so the array scales with the appropriate offset.
    // ONLY EVER INCREASES SIZE!!!
    private void InitArrays(SpawnJobCoord realCoord)
    {
        var deltadelta = new SpawnJobCoord(0, 0);
        var oldMax = _arrayMaxSize;
        if (ArrayBoundsInit(realCoord, ref deltadelta))
        {
            //print("YEAH! resizing. Realcoord: " + realCoord + " deltadelta: " + deltadelta + " oldmax : " + oldMax + " maxsize: " + _arrayMaxSize);
            var newDict = _spawnedObjects2D;
            newDict = new ListRaper[_arrayMaxSize.x * _arrayMaxSize.y];
            for (int x = 0; x < oldMax.x; x++)
            {
                for (int y = 0; y < oldMax.y; y++)
                {
                    // move jobs[i,j] to newjobs[i+deltax, j+deltay]
                    newDict[(x + deltadelta.x) * h + y + deltadelta.y] = _spawnedObjects2D[x * oldMax.y + y];
                }
            }

            _spawnedObjects2D = newDict;
        }
    }

    // only used in InitArrays()
    private bool ArrayBoundsInit(SpawnJobCoord realCoord, ref SpawnJobCoord deltadelta)
    {
        bool changed = false;

        if (realCoord.x + _arrayIndexOffset.x < 0)
        {
            // new delta (-x) minus old delta (arrayIndexOffset.x)
            deltadelta.x = -realCoord.x - _arrayIndexOffset.x;
            _arrayIndexOffset.x = -realCoord.x;
            _arrayMaxSize.x += deltadelta.x;
            changed = true;
        }
        else if (realCoord.x + _arrayIndexOffset.x >= w)
        {
            _arrayMaxSize.x = realCoord.x + _arrayIndexOffset.x + 1;
            changed = true;
        }

        if (realCoord.y < -_arrayIndexOffset.y)
        {
            deltadelta.y = -realCoord.y - _arrayIndexOffset.y;
            _arrayIndexOffset.y = -realCoord.y;
            _arrayMaxSize.y += deltadelta.y;
            changed = true;
        }
        else if (realCoord.y + _arrayIndexOffset.y >= h)
        {
            _arrayMaxSize.y = realCoord.y + _arrayIndexOffset.y + 1;
            changed = true;
        }
        return changed;
    }

    // long but simple - checks all edges of the min/max spawned array, and reduces bounds if there are no trees in that range.
    private void ReduceArraySize()
    {
        // minCoordEver is array[0,0], equivalent chunk = -indexOffset
        var minCoord = this.minCoordEver;
        // maxCoordEver is array[size-1, size-1], equivalent chunk = maxSize - 1 - indexOffset.
        var maxCoord = this.maxCoordEver;

        // check if there is anything on the minCoordEver.y row.
        bool foundSomething = false;
        while (!foundSomething && minCoord.y <= maxCoord.y)
        {
            foundSomething = false;
            for (int x = minCoord.x; x <= maxCoord.x; x++)
            {
                var coordMin = new SpawnJobCoord(x, minCoord.y);
                if (FoundSomethingHere(coordMin))
                {
                    foundSomething = true;
                    break;
                }
            }
            // we did not find any trees on this row! don't search it as min. coord anymore.
            // and check again for the next row.
            if (!foundSomething)
            {
                minCoord.y++;
            }
        }

        // check if there is anything on the maxCoordEver.y row.
        foundSomething = false;
        while (!foundSomething && minCoord.y <= maxCoord.y)
        {
            foundSomething = false;
            for (int x = minCoord.x; x <= maxCoord.x; x++)
            {
                var coordMin = new SpawnJobCoord(x, maxCoord.y);
                if (FoundSomethingHere(coordMin))
                {
                    foundSomething = true;
                    break;
                }
            }
            if (!foundSomething)
            {
                maxCoord.y--;
            }
        }


        // check if there is anything on the mincoordever.x row.
        foundSomething = false;
        while (!foundSomething && minCoord.x <= maxCoord.x)
        {
            foundSomething = false;
            for (int y = minCoord.y; y <= maxCoord.y; y++)
            {
                var coordMin = new SpawnJobCoord(minCoord.x, y);
                if (FoundSomethingHere(coordMin))
                {
                    foundSomething = true;
                    break;
                }
            }

            if (!foundSomething)
            {
                minCoord.x++;
            }
        }


        // check if there is anything on the mincoordever.x row.
        foundSomething = false;
        while (!foundSomething && minCoord.x <= maxCoord.x)
        {
            foundSomething = false;
            for (int y = minCoord.y; y <= maxCoord.y; y++)
            {
                var coordMin = new SpawnJobCoord(maxCoord.x, y);
                if (FoundSomethingHere(coordMin))
                {
                    foundSomething = true;
                    break;
                }
            }
            if (!foundSomething)
            {
                maxCoord.x--;
            }
        }

        // minCoord and maxCoord now represent the edges of the new array.
        // we must move the old contents at these positions in a new array which only contains this new range.

        //print("YEAH! resizing. Realcoord: " + realCoord + " deltadelta: " + deltadelta + " oldmax : " + oldMax + " maxsize: " + _arrayMaxSize);
        var newDict = _spawnedObjects2D;
        // max - min + 1 is the size of the new array
        var newMax = maxCoord - minCoord + new SpawnJobCoord(1, 1);
        newDict = new ListRaper[newMax.x * newMax.y];
        for (int xx = minCoord.x; xx <= maxCoord.x; xx++)
        {
            for (int yy = minCoord.y; yy <= maxCoord.y; yy++)
            {
                // take elements from [(x,y) + offset] of the old array, and put them in the new array at [(x,y) - minCoordEver]
                newDict[(xx - minCoord.x) * newMax.y + yy - minCoord.y] = _spawnedObjects2D[(xx + _arrayIndexOffset.x) * h + (yy + _arrayIndexOffset.y)];
            }
        }

        _spawnedObjects2D = newDict;
        _arrayIndexOffset = new SpawnJobCoord(0, 0) - minCoord;
        _arrayMaxSize = newMax;
    }

    // true when job at coordMin is spawning or contains any spawned objects
    private bool FoundSomethingHere(SpawnJobCoord coordMin)
    {
        int listIndex;
        int listCount;
        var listNotNull = GetIsSpawnListNotNull(coordMin, out listIndex, out listCount);
        return listNotNull && listCount > 0;
    }

    private void AddToSpawnList(SpawnJobCoord chunk, IChunkObject element)
    {
        InitArrays(chunk);
        var i = chunk + _arrayIndexOffset;

        var index = i.x * h + i.y;

        if (_spawnedObjects2D[index].list == null)
        {
            _spawnedObjects2D[index].list = new List<ChunkObject>();
        }

        _spawnedObjects2D[index].list.Add(element as ChunkObject);
    }

    public override bool GetIsSpawnListNotNull(SpawnJobCoord chunk, out int listIndex, out int listCount)
    {
        var i = chunk + _arrayIndexOffset;
        listIndex = i.x * h + i.y;
        listCount = -1;
        // null if out of range
        if (i.x < 0 || i.y < 0 || i.x >= w || i.y >= h)
        {
            return false;
        }
        if (_spawnedObjects2D[listIndex].list == null)
        {
            return false;
        }
        listCount = _spawnedObjects2D[listIndex].list.Count;
        return true;
    }

    public override IChunkObject GetSpawnListElement(int listIndex, int index)
    {
        return _spawnedObjects2D[listIndex].list[index];
    }

    #endregion

    //[DebugButton]
    private void PrintAllListCounts()
    {
        var p = "Spawndict list count: " + _spawnedObjects2D.Length;
        p += "\nTotal object count: " + _spawnedObjects2D.Sum(lr => lr.list != null ? lr.list.Count : 0);
        foreach (var d in _spawnedObjects2D)
        {
            if (d.list != null)
            {
                p += "\nList cuont: " + d.list.Count;
            }
        }
        print(p);
    }

    [DebugButton]
    private void EditorNavigateHere()
    {
        var coord = (minCoordEver + maxCoordEver) / 2;
        var pos = SpawnJobCoord.CoordToPos(coord, this.gridSize);
        pos.y = MountainHeightProviderSingleton.SampleHeight(pos);
        OriginShiftManager.FocusOn(pos);
    }

    [DebugButton]
    public override void AddFromChildren()
    {
        if (transform.childCount == 0)
        {
            Debug.LogError("HORRIBLE WARNING: Transform has no children. You might have mistakenly cleared your cool premade object group.");
            return;
        }

        for (int i = 0; i < transform.childCount; i++)
        {
            var c = transform.GetChild(i);
            var prefab = c.GetComponent<IVRPrefab>();
            if (prefab != null)
            {
                var pos = c.position + OriginShiftManager.originShift;
                ChunkObject so = CreateChunkObject(new ChunkObject(), prefab, pos);
                var chunk = SpawnJobCoord.PosToCoord(pos, _gridSize);
                AddToSpawnList(chunk, so);
                _spawnedCount++;
            }
        }

        ReduceArraySize();
    }

    private static ChunkObject CreateChunkObject(ChunkObject chunkObject, IVRPrefab spawnedPrefab, Vector3 globalPosition)
    {
        chunkObject.prefabType = spawnedPrefab.prefabType;
        chunkObject.globalPos = globalPosition;
        chunkObject.rot = spawnedPrefab.transform.localRotation;
        chunkObject.localScale = spawnedPrefab.transform.localScale;
        return chunkObject;
    }

    public override bool UpdateObjectData(IChunkObject originalData, GameObject spawnedObject)
    {
        var ivrp = spawnedObject.GetComponent<IVRPrefab>();
        if (ivrp != null)
        {
            foreach (var sol in _spawnedObjects2D)
            {
                var co = (ChunkObject)originalData;
                if (co != null)
                {
                    if (sol.list != null)
                    {
                        var index = sol.list.IndexOf(co);
                        if (index >= 0)
                        {
                            // success!
                            sol.list[index] = CreateChunkObject(co, ivrp, OriginShiftManager.LocalToGlobalPos(ivrp.position));

                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public override bool DeleteFromSpawner(IChunkObject originalData, GameObject spawnedObject)
    {
        var ivrp = spawnedObject.GetComponent<IVRPrefab>();
        if (ivrp != null)
        {
            foreach (var sol in _spawnedObjects2D)
            {
                var co = (ChunkObject)originalData;
                if (sol.list != null)
                {
                    var index = sol.list.IndexOf(co);
                    if (index >= 0)
                    {
                        // success! found the obj.
                        sol.list.RemoveAt(index);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    [DebugButton]
    private void SpawnAll()
    {
        for (int x = 0; x < _arrayMaxSize.x; x++)
        {
            for (int y = 0; y < _arrayMaxSize.y; y++)
            {
                var coord = new SpawnJobCoord(x - _arrayIndexOffset.x, y - _arrayIndexOffset.y);
                Spawn(coord);

            }
        }
    }

    private void Spawn(SpawnJobCoord coord)
    {
        int listIndex, listCount;
        bool listNotNull = GetIsSpawnListNotNull(coord, out listIndex, out listCount);
        for (int i = 0; i < listCount; i++)
        {
            var s = GetSpawnListElement(listIndex, i);
            s.Spawn(transform);
        }
    }

    [DebugButton]
    public override void DestroyChildren()
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            this.DestroySafe(transform.GetChild(i).gameObject);
        }
    }

    [DebugButton]
    private void ClearData()
    {
#if UNITY_EDITOR
        UnityEditor.Undo.RecordObject(this, "Clear PremadeObjectChunk");
#endif
        if (w * h > _spawnedObjects2D.Length)
        {
            Debug.LogError("Spawned object list is shorter than the max size. Deleting the whole damn thing");
        }
        else
        {
            for (int x = 0; x < w; x++)
            {
                for (int y = 0; y < h; y++)
                {
                    if (_spawnedObjects2D[x * h + y].list != null)
                    {
                        _spawnedObjects2D[x * h + y].list.Clear();
                    }
                }
            }
        }
        _arrayIndexOffset = new SpawnJobCoord(0, 0);
        _arrayMaxSize = new SpawnJobCoord(0, 0);
        _spawnedObjects2D = new ListRaper[1];
        _spawnedCount = 0;
        InitArrays(new SpawnJobCoord(0, 0));

    }

    [System.Serializable]
    public struct ListRaper
    {
        // for unity serializing and saving the actual data to disk (and edit in inspector or with tools)
        public List<ChunkObject> list;

    }

    private void OnDrawGizmosSelected()
    {
        if (showGizmos)
        {
            var os = OriginShiftManager.originShift;
            os.y = 0;
            var minCoord = minCoordEver;
            var maxCoord = maxCoordEver;
            for (int x = minCoord.x; x <= maxCoord.x; x++)
            {
                for (int y = minCoord.y; y <= maxCoord.y; y++)
                {
                    var coord = new SpawnJobCoord(x, y);
                    int listCount, listIndex;
                    var listNotNull = GetIsSpawnListNotNull(coord, out listIndex, out listCount);
                    if (listNotNull)
                    {
                        var col = occupancyGizmoColors.Evaluate(listCount / (float)gizmoCountMax);
                        Gizmos.color = col;
                        var cubePos = OriginShiftManager.GlobalToLocalPos(SpawnJobCoord.CoordToPos(coord, _gridSize));
                        cubePos.y = MountainHeightProviderSingleton.SampleHeight(cubePos);
                        Gizmos.DrawCube(cubePos, Vector3.one * _gridSize * 0.8f);
                    }
                }
            }
        }
    }
}