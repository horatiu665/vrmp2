﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public static class SpawnableChunksUtils
{
    public static SpawnableChunksParent chunksParent { get { return SpawnableChunksParent.instance; } }

    // Editor only, saves prefabs in mountain in the spawner group.
    public static void MagicSaveThosePrefabs(IEnumerable<IVRPrefab> prefabsToSave)
    {
        // group by type. create the chunk groups. name them based on the amount of obj saved. save the obj in the group 
        var groupedPBs = prefabsToSave.GroupBy(pp => GetPrefabGroupType(pp));

        foreach (var group in groupedPBs)
        {
            // for this group, create a new spawner and save it under levels.
            var ngsGO = new GameObject("[gen][" + System.DateTime.Now.ToShortDateString() + " " + System.DateTime.Now.ToShortTimeString() + "] ");
            SpawnableChunkBase newGroupScript;
            switch (group.Key)
            {
            case PrefabChunkGroupTypes.Regular:
                newGroupScript = ngsGO.AddComponent<PremadeObjectGroupChunks>();
                break;
            case PrefabChunkGroupTypes.SlalomPole:
                newGroupScript = ngsGO.AddComponent<SlalomPoleChunks>();
                break;
            case PrefabChunkGroupTypes.Toilet:
                newGroupScript = ngsGO.AddComponent<ToiletTeleporterChunks>();
                break;
            default:
                Debug.LogError("[MAGIC SAVE] [ERROR] No chunks object for type " + group.Key.ToString());
                continue;
                break;
            }

            newGroupScript.showGizmo = false;

            ngsGO.transform.SetParent(GetSpawnChunksParent());

            // add objects and save their data for the name and opt. metadata
            Dictionary<VRPrefabType, int> objCounts = new Dictionary<VRPrefabType, int>();
            foreach (var p in group)
            {
                p.transform.SetParent(ngsGO.transform);
                if (!objCounts.ContainsKey(p.prefabType))
                {
                    objCounts[p.prefabType] = 0;
                }
                objCounts[p.prefabType]++;
            }
            newGroupScript.AddFromChildren();

            // create name for obj.
            int objAdd = 0;
            var name = ngsGO.name;
            foreach (var obname in objCounts)
            {
                name += obname.Key + " (" + obname.Value + "), ";
                objAdd++;
                if (objAdd >= 5)
                {
                    name += " and others...";
                    break;
                }
            }
            ngsGO.name = name;

            // delete all children cause they are added now...??
            newGroupScript.DestroyChildren();

        }
    }

    private static Transform GetSpawnChunksParent()
    {
        return chunksParent.transform;
    }

    /// <summary>
    /// Simply decides what kind of object the prefab is, and what kind of spawner is allowed to spawn it
    /// </summary>
    /// <param name="pp"></param>
    /// <returns></returns>
    private static PrefabChunkGroupTypes GetPrefabGroupType(IVRPrefab pp)
    {
        if (pp.GetComponent<ToiletTeleporter>() != null)
        {
            // must add as toilet
            Debug.LogError("[MAGIC SAVE] [ERROR] Cannot save toilets yet. Please save toilets manually", pp.gameObject);
            return PrefabChunkGroupTypes.Toilet;
        }
        else
        {
            var spc = pp.GetComponent<SlalomPoleComponent>();
            if (spc != null)
            {
                return PrefabChunkGroupTypes.SlalomPole;
            }
            else
            {
                return PrefabChunkGroupTypes.Regular;
            }
        }

        return PrefabChunkGroupTypes.Regular;
    }

    private enum PrefabChunkGroupTypes
    {
        Regular,
        SlalomPole,
        Toilet
    }
}