using UnityEngine;
using VRCore;
using Apex;
using UnityEngine.Serialization;

[System.Serializable]
public class ChunkObject : IChunkObject
{
    public VRPrefabType prefabType;
    [FormerlySerializedAs("localPos")]
    public Vector3 globalPos;
    [FormerlySerializedAs("localRot")]
    public Quaternion rot;
    public Vector3 localScale;

    public virtual IVRPrefab Spawn(Transform parent)
    {
        var obj = FoggyForestPoolManager.instance.Spawn(prefabType, OriginShiftManager.GlobalToLocalPos(globalPos), rot);
        obj.transform.localScale = localScale;
        obj.transform.SetParent(parent);
        return obj;
    }

}

// effort to keep premade obj filter the same for various premade object group types
public interface IChunkObject
{
    IVRPrefab Spawn(Transform parent);
}