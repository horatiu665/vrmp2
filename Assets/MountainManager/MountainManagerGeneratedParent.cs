﻿using UnityEngine;
using System.Collections;
using Mountain;

public class MountainManagerGeneratedParent : MonoBehaviour
{
    private static MountainManagerGeneratedParent _instance;
    public static MountainManagerGeneratedParent instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<MountainManagerGeneratedParent>();
            }
            return _instance;
        }
    }

    public MountainManager mountainManager;

}
