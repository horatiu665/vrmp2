using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class VillageCenterConstraints : MonoBehaviour
{
    // provides constraints for village height.
    public List<VillageLocation> villages = new List<VillageLocation>();

    /// <summary>
    /// Gets distance to village center/radius. 1 is inside some village. 0 is outside any village. 0..1 is in falloff radius
    /// </summary>
    /// <param name="x"></param>
    /// <param name="z"></param>
    /// <returns></returns>
    public float GetVillageLerp(float x, float z)
    {
        var pos = new Vector2(x, z);

        float maxVillageBelong = 0f;

        for (int i = 0; i < villages.Count; i++)
        {
            var vl = villages[i];
            var sqmag = (vl.pos - pos).sqrMagnitude;
            var min = vl.radius + vl.falloff;
            if (sqmag < min * min)
            {
                var dist = 1f - Mathf.Clamp01((Mathf.Sqrt(sqmag) - vl.radius) / vl.falloff);
                if (maxVillageBelong < dist)
                {
                    maxVillageBelong = dist;
                }
            }
        }

        return maxVillageBelong;
    }

    public VillageLocation GetNearestVillage(Vector2 pos)
    {
        float maxVillageBelong = 0f;
        int nearestVillage = 0;

        for (int i = 0; i < villages.Count; i++)
        {
            var vl = villages[i];
            var sqmag = (vl.pos - pos).sqrMagnitude;
            var min = vl.radius + vl.falloff;
            if (sqmag < min * min)
            {
                var dist = 1f - Mathf.Clamp01((Mathf.Sqrt(sqmag) - vl.radius) / vl.falloff);
                if (maxVillageBelong < dist)
                {
                    maxVillageBelong = dist;
                    nearestVillage = i;
                }
            }
        }

        return villages[nearestVillage];
    }

    [DebugButton]
    void UpdateFromChildSpheres()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            var t = transform.GetChild(i);

            // switch to global pos for storing the data (for correct later interpretation)
            var pos = OriginShiftManager.LocalToGlobalPos(t.position);
            // calculate height delta based on delta of the sphere compared to the current height at that position. or zero if there is no height set there which would be a stupid workflow anyway.
            var heightDelta = t.position.y - (MountainHeightProviderSingleton.instance != null ? MountainHeightProviderSingleton.SampleHeight(t.position) : 0);

            VillageLocation vl;
            if (villages.Count > i)
            {
                vl = villages[i];
                vl.pos = new Vector2(pos.x, pos.z);
                vl.radius = t.localScale.x;
                vl.heightDelta = heightDelta;
                villages[i] = vl;
            }
            else
            {
                vl = new VillageLocation();
                vl.pos = new Vector2(pos.x, pos.z);
                vl.radius = t.localScale.x;
                vl.falloff = t.localScale.x;
                vl.heightDelta = heightDelta;
                var vts = villages.LastOrDefault(v => v.villageToSpawn != null);
                if (vts != null)
                {
                    vl.villageToSpawn = vts.villageToSpawn;
                }
                villages.Add(vl);
            }

        }
    }

    [DebugButton]
    void CreateChildSpheres()
    {
        for (int i = 0; i < villages.Count; i++)
        {
            var v = villages[i];
            var go = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            var pos = OriginShiftManager.GlobalToLocalPos(new Vector3(v.pos.x, 0, v.pos.y));
            pos.y = MountainHeightProviderSingleton.instance != null ? MountainHeightProviderSingleton.SampleHeight(pos) : 0;
            pos.y += v.heightDelta;
            go.transform.position = pos;
            go.transform.localScale = Vector3.one * v.radius;
            go.transform.SetParent(transform);
        }
    }

    [DebugButton]
    void ClearChildSpheres()
    {
        while (transform.childCount > 0)
        {
            this.DestroySafe(transform.GetChild(0).gameObject);
        }
    }

    [DebugButton]
    void ShowVillageHeights()
    {
        for (int i = 0; i < villages.Count; i++)
        {
            var v = villages[i];
            var villageHeight = MountainHeightProviderSingleton.instance.heightMeshFilter.SampleHeight(this, new Vector3(v.pos.x, 0, v.pos.y));
            v.villageHeightDebug = villageHeight;
            villages[i] = v;
        }
    }

    [Serializable]
    public class VillageLocation
    {
        public Vector2 pos;
        public float radius;
        public float falloff;
        public float heightDelta;
        public PremadeObjectGroup villageToSpawn;
        public float villageHeightDebug;
    }

}