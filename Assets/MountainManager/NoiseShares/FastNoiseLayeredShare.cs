using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FastNoiseLayeredShare : FastNoiseShare
{
    private static FastNoiseCombineData data = new FastNoiseCombineData(0,0,0);

    [HideInInspector]
    public FastNoiseLayer[] noiseLayers;

    private void Reset()
    {
        InitLayers();
    }

    private void OnEnable()
    {
        InitLayers();
    }

	private void OnValidate()
	{
		InitLayers();
	}

	public void InitLayers()
    {
        noiseLayers = GetComponentsInChildren<FastNoiseLayer>(true);
    }

    public override float GetNoise(float x, float y)
    {
        data.Clear(x, y);

        for (int i = 0; i < noiseLayers.Length; i++)
        {
            if (noiseLayers[i].isActiveAndEnabled)
            {
                data = noiseLayers[i].Combine(data);
				if (data.interrupt)
				{
					break;
				}
            }
        }

        return data.noise;
    }

}

public class FastNoiseCombineData
{
    public float x, y;
    public float noise = 0f;
    public bool interrupt = false;

    // villages
    public float villageLerp = 0f;
    public VillageCenterConstraints.VillageLocation nearestVillage = null;

    public FastNoiseCombineData(float x, float y, float value)
    {
        this.x = x;
        this.y = y;
        this.noise = value;

		interrupt = false;

        // villages
        villageLerp = 0;
    }

    public void Clear(float x, float y)
    {
        this.noise = 0;
        this.interrupt = false;
        this.x = x;
        this.y = y;
        this.villageLerp = 0;
        this.nearestVillage = null;
    }
}