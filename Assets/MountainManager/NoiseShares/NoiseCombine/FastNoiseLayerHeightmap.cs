﻿using UnityEngine;
using System.Collections;

public class FastNoiseLayerHeightmap : FastNoiseLayer
{
	private FastNoiseHeightmap _heightmap;
	public FastNoiseHeightmap heightmap
	{
		get
		{
			if (_heightmap == null)
			{
				_heightmap = GetComponent<FastNoiseHeightmap>();
			}
			return _heightmap;
		}
	}

	public override FastNoiseCombineData Combine(FastNoiseCombineData input)
	{
		float weight;
		var hNoise = heightmap.GetNoise(input.x, input.y, out weight);
		input.noise = Mathf.Lerp(input.noise, hNoise, weight);
		return input;
	}

}
