﻿using UnityEngine;
using System.Collections;
using System;

public class FastNoiseLayerAdd : FastNoiseLayerBasic
{
    public float multiplier = 1f;
    
    public override FastNoiseCombineData Combine(FastNoiseCombineData input)
    {
        input.noise = noise.GetNoise(input.x, input.y) * multiplier + input.noise;
        return input;
    }
}
