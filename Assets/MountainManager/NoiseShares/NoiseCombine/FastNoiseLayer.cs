﻿using UnityEngine;
using System.Collections;

public abstract class FastNoiseLayer : MonoBehaviour
{
    public abstract FastNoiseCombineData Combine(FastNoiseCombineData input);

}