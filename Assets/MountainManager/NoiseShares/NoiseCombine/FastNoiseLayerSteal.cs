﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class FastNoiseLayerSteal : FastNoiseLayer
{
	[Header("Steal this noise")]
	public FastNoiseShare noiseToSteal;

	public override FastNoiseCombineData Combine(FastNoiseCombineData input)
	{
		input.noise = noiseToSteal.GetNoise(input.x, input.y);
		return input;
	}

}