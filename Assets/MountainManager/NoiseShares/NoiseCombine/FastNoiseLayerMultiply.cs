﻿using UnityEngine;
using System.Collections;

public class FastNoiseLayerMultiply : FastNoiseLayerBasic
{
    public override FastNoiseCombineData Combine(FastNoiseCombineData input)
    {
        input.noise = noise.GetNoise(input.x, input.y) * input.noise;
        return input;
    }

}
