﻿using UnityEngine;
using System.Collections;
using System;

public abstract class FastNoiseLayerBasic : FastNoiseLayer
{
    [SerializeField]
    private FastNoise _noise = new FastNoise();
    public FastNoise noise { get { return _noise; } }
    
    public override FastNoiseCombineData Combine(FastNoiseCombineData input)
    {
        return input;
    }
}