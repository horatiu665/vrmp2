﻿using UnityEngine;
using System.Collections;
using VRCore;

public class FastNoiseLayerNotNoise : FastNoiseLayer
{
	[HelpBox("Returns alternativeValue when the other noise is larger than threshold (or smaller if largerThan is false)", HelpBoxType.Info)]
	public FastNoiseShare noiseToAvoid;
	public float threshold = 0f;
	public bool largerThan = true;

	public float alternativeValue = 0;

	public override FastNoiseCombineData Combine(FastNoiseCombineData input)
	{
		var otherNoise = noiseToAvoid.GetNoise(input.x, input.y);
		var success = largerThan ? otherNoise >= threshold : otherNoise <= threshold;
		if (success)
		{
			input.noise = alternativeValue;
		}
		return input;
	}

}
