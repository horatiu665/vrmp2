﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class FastNoiseLayerLake : FastNoiseLayer
{
    // offsets x and y to the same value over a larger surface... so the output looks like the terrain is leveled there.
    public VillageCenterConstraints villages;

    public override FastNoiseCombineData Combine(FastNoiseCombineData input)
    {
        var vlerp = villages.GetVillageLerp(input.x, input.y);
        if (vlerp > 0)
        {
            input.nearestVillage = villages.GetNearestVillage(new Vector2(input.x, input.y));
            input.villageLerp = vlerp;
            input.noise = input.nearestVillage.heightDelta * vlerp + input.noise * (1 - vlerp);
        }

        return input;
    }
}
