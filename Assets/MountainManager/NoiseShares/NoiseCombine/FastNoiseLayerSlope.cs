﻿using UnityEngine;
using System.Collections;

public class FastNoiseLayerSlope : FastNoiseLayerBasic
{
    public enum SlopeType
    {
        Radial,
        Directional
    }

    public SlopeType slopeType = SlopeType.Radial;

    public float slopePerUnit = -0.5f;

    public override FastNoiseCombineData Combine(FastNoiseCombineData input)
    {
        input.noise = input.noise + GetHeight(new Vector2(input.x, input.y));
        if (input.villageLerp > 0)
        {
            input.noise = Mathf.Lerp(input.noise, GetHeight(input.nearestVillage.pos), input.villageLerp);
        }
        return input;
    }

    private float GetHeight(Vector2 pos)
    {
        float dist;
        if (slopeType == SlopeType.Directional)
        {
            dist = pos.y;
        }
        else // radial
        {
            dist = Mathf.Sqrt(pos.x * pos.x + pos.y * pos.y);
        }
        return dist * slopePerUnit;
    }
}