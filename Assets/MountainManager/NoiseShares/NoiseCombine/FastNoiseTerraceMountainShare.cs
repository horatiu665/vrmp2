using System;
using UnityEngine;
using VRCore;

public class FastNoiseTerraceMountainShare : FastNoiseLayerBasic
{
	[Header("Piste")]
	public FastNoisePiste piste;

	[Header("Villages")]
	public VillageCenterConstraints villageCenters;

	[Header("Heightmap params")]
	public float terraceWidth = 1000;

	public TerraceData[] terraces = new TerraceData[] {
										new TerraceData()
										{
											heightScale = 1000,
											pisteHeightScale = 600f,
											slopePerUnit = -0.3f,
											blendWidth = 100
										}
									};

	[Serializable]
	public struct TerraceData
	{
		public float heightScale; // terrain.Y offset based on noise
		public float pisteHeightScale; // terrain.Y offset based on piste distance
		public float slopePerUnit; // terrain.Y offset based on dist to origin
		[ReadOnly]
		public float worldHeightOffset; // manually- or auto-calculated from the previous terraces, so the global slope per unit function's derivative can be continuous
		public float blendWidth; // blend dist between this terrace and the next. always smaller than terrace width.
	}

	public enum SlopeType
	{
		Radial,
		Directional
	}

	public SlopeType slopeType = SlopeType.Radial;

    public override FastNoiseCombineData Combine(FastNoiseCombineData input)
    {
        input.noise = GetNoise(input.x, input.y) + input.noise;
        return input;
    }

    public float GetNoise(float x, float y)
	{
		int i = GetTerraceIndex(x, y);
		float blend = GetTerraceBlend(x, y);

		float globalHeight = GetGlobalHeight(x, y, i, blend);

		var pisteDist = piste.GetPisteDist(x, y);
		float pisteHeight = 0f;
		if (pisteDist < piste.pisteTransitionWidth)
		{
			pisteHeight = GetPisteNoise(x, y) * GetTerracePisteHeightScale(i, blend) + globalHeight;
		}

		var heightScale = GetTerraceHeightScale(i, blend);

		var villageLerp = villageCenters.GetVillageLerp(x, y);
		float villageHeight = 0f;
		if (villageLerp > 0)
		{
			var nearestVillage = villageCenters.GetNearestVillage(new Vector2(x, y));
			villageHeight = GetVillageHeight(nearestVillage) * heightScale + nearestVillage.heightDelta + GetGlobalHeight(nearestVillage, i, blend);
		}

		float mountainHeight = 0f;
		mountainHeight = noise.GetNoise(x, y) * heightScale + globalHeight;

		// if there is a village, return the village height.
		// else if there is a slope, return slope height.
		// else return base height
		float finalHeight = 0f;

		finalHeight = mountainHeight;

		if (pisteDist < piste.pisteTransitionWidth)
		{
			// 0 is on piste, 1 is off.
			var pisteLerp = Mathf.InverseLerp(piste.pisteWidth, piste.pisteTransitionWidth, pisteDist);
			finalHeight = Mathf.Lerp(pisteHeight, finalHeight, pisteLerp);
		}

		if (villageLerp > 0)
		{
			finalHeight = Mathf.Lerp(finalHeight, villageHeight, villageLerp);
		}

		return finalHeight;
	}

	private float GetTerracePisteHeightScale(int i, float terraceBlend)
	{
		if (terraceBlend == 0)
		{
			return terraces[i].pisteHeightScale;
		}
		else
		{
			return Mathf.SmoothStep(terraces[i].pisteHeightScale, terraces[i + 1].pisteHeightScale, terraceBlend);
		}
	}

	private float GetTerraceHeightScale(int i, float terraceBlend)
	{
		if (terraceBlend == 0)
		{
			return terraces[i].heightScale;
		}
		else
		{
			return Mathf.SmoothStep(terraces[i].heightScale, terraces[i + 1].heightScale, terraceBlend);
		}
	}

	float GetGlobalHeight(VillageCenterConstraints.VillageLocation village, int terrace, float terraceBlend)
	{
		return GetGlobalHeight(village.pos.x, village.pos.y, terrace, terraceBlend);
	}

	float GetGlobalHeight(float x, float y, int terrace, float terraceBlend)
	{
		float dist;
		if (slopeType == SlopeType.Directional)
		{
			dist = y;
		}
		else // radial
		{
			dist = Mathf.Sqrt(x * x + y * y);
		}
		if (terraceBlend == 0)
		{
			return dist * terraces[terrace].slopePerUnit + terraces[terrace].worldHeightOffset;
		}
		else
		{
			var a = dist * terraces[terrace].slopePerUnit + terraces[terrace].worldHeightOffset;
			var b = dist * terraces[terrace + 1].slopePerUnit + terraces[terrace + 1].worldHeightOffset;
			var gh = Mathf.SmoothStep(a, b, terraceBlend);
			return gh;
		}
	}

	float GetPisteNoise(float x, float y)
	{
		return noise.GetNoise(0, y);
	}

	float GetVillageHeight(VillageCenterConstraints.VillageLocation village)
	{
		return noise.GetNoise(village.pos.x, village.pos.y);
	}

	int GetTerraceIndex(float x, float y)
	{
		if (slopeType == SlopeType.Directional)
		{
			return Mathf.Clamp(Mathf.FloorToInt(y / terraceWidth), 0, terraces.Length - 1);
		}
		else
		{
			return Mathf.Clamp(Mathf.FloorToInt(Mathf.Sqrt(x * x + y * y) / terraceWidth), 0, terraces.Length - 1);
		}

	}

	// Gets the interpolant 0..1 between the terraces we are currently in between.
	float GetTerraceBlend(float x, float y)
	{
		float dist;
		if (slopeType == SlopeType.Directional)
		{
			dist = y;
		}
		else // radial
		{
			dist = Mathf.Sqrt(x * x + y * y);
		}
		var ti = Mathf.Clamp(dist / terraceWidth, 0, terraces.Length - 1);
		var i = Mathf.FloorToInt(ti);
		if (i == terraces.Length - 1)
		{
			return 0f; // blend 0 for the last terrace. also for the first one? nah.
		}
		// 0..1 -> -1..0 -> (-tw)..0 -> (-tw + blend)..(blend) -> ((-tw + blend) / blend)..1 -> 0..1
		var t01 = Mathf.Clamp01(((ti - i - 1) * terraceWidth + terraces[i].blendWidth) / terraces[i].blendWidth);
		return t01;
	}

	private void OnValidate()
	{
		AutoCalculateAllWorldHeightOffsets();
	}

	void AutoCalculateAllWorldHeightOffsets()
	{
		for (int i = 0; i < terraces.Length; i++)
		{
			var dist = terraceWidth * i;
			// what height would this terrace start at if it were not offset?
			var terraceHeightWithoutOffset = terraces[i].slopePerUnit * dist;

			// what height did the previous terrace finish at?
			var prevTerraceFinish = i == 0 ? 0f : (terraces[i - 1].slopePerUnit * dist + terraces[i - 1].worldHeightOffset);

			float blendShit = i == 0 ? 0 : terraces[i - 1].blendWidth * 0.5f * terraces[i].slopePerUnit;

			var offset = prevTerraceFinish - terraceHeightWithoutOffset + blendShit;

			terraces[i].worldHeightOffset = offset;

		}
	}

	//[DebugButton]
	//void RetardoGenerateMountain()
	//{
	//	FindObjectOfType<Mountain.MountainManager>().GenerateAt(Vector3.zero);
	//}
}

