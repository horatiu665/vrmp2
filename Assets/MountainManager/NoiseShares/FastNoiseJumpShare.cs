using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class FastNoiseJumpShare : FastNoiseBasic
{
	[Header("Piste")]
	public FastNoisePiste piste;

	[Header("spawning params")]
	public float offPisteConstant = -1;

	public override float GetNoise(float x, float y)
	{
		var pisteDist = piste.GetPisteDist(x, y);
		if (pisteDist > piste.pisteTransitionWidth)
		{
			return offPisteConstant;
		}
		else if (pisteDist < piste.pisteWidth)
		{
			return base.GetNoise(x, y);
		}
		else
		{
			// 0 is on piste, 1 is off piste.
			var pisteLerp = Mathf.InverseLerp(piste.pisteWidth, piste.pisteTransitionWidth, pisteDist);
			return Mathf.Lerp(base.GetNoise(x, y), offPisteConstant, pisteLerp);
		}
	}
}