using System;
using UnityEngine;

public class FastNoiseMountainShare : FastNoiseBasic
{
    [Header("Piste")]
    public FastNoisePiste piste;

    [Header("Villages")]
    public VillageCenterConstraints villageCenters;

    [Header("Heightmap params")]
    public float heightScale = 1000;

    public float pisteHeightScale = 600f;

    public float slopePerUnit = -0.3f;

    public enum SlopeType
    {
        Radial,
        Directional
    }

    public SlopeType slopeType = SlopeType.Radial;
    
    public override float GetNoise(float x, float y)
    {
        float globalHeight = GetGlobalHeight(x, y);

		var pisteDist = piste.GetPisteDist(x, y);
        float pisteHeight = 0f;
        if (pisteDist < piste.pisteTransitionWidth)
        {
            pisteHeight = GetPisteNoise(x, y) * pisteHeightScale + globalHeight;
        }

        var villageLerp = villageCenters.GetVillageLerp(x, y);
        float villageHeight = 0f;
        if (villageLerp > 0)
        {
            var nearestVillage = villageCenters.GetNearestVillage(new Vector2(x, y));
            villageHeight = GetVillageHeight(nearestVillage) * heightScale + nearestVillage.heightDelta + GetGlobalHeight(nearestVillage);
        }

        float mountainHeight = 0f;
        mountainHeight = noise.GetNoise(x, y) * heightScale + globalHeight;

        // if there is a village, return the village height.
        // else if there is a slope, return slope height.
        // else return base height
        float finalHeight = 0f;

        finalHeight = mountainHeight;

        if (pisteDist < piste.pisteTransitionWidth)
        {
			// 0 is on piste, 1 is off.
			var pisteLerp = Mathf.InverseLerp(piste.pisteWidth, piste.pisteTransitionWidth, pisteDist);
			finalHeight = Mathf.Lerp(pisteHeight, finalHeight, pisteLerp);
        }

        if (villageLerp > 0)
        {
            finalHeight = Mathf.Lerp(finalHeight, villageHeight, villageLerp);
        }

        return finalHeight;
    }

    float GetGlobalHeight(VillageCenterConstraints.VillageLocation village)
    {
        return GetGlobalHeight(village.pos.x, village.pos.y);
    }

    float GetGlobalHeight(float x, float y)
    {
        return (slopeType == SlopeType.Radial ? Mathf.Sqrt(x * x + y * y) : y) * slopePerUnit;
    }

    float GetPisteNoise(float x, float y)
    {
        return noise.GetNoise(0, y);
    }

    float GetVillageHeight(VillageCenterConstraints.VillageLocation village)
    {
        return noise.GetNoise(village.pos.x, village.pos.y);
    }
}

