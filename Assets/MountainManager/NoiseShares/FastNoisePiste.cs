using UnityEngine;

public class FastNoisePiste : MonoBehaviour
{
	// ######################### THIS IS HOW A SIMPLE STRAIGHT ASS PISTE LOOKS LIKE #########################
	//[Header("Main piste params")]
	//public float pisteWidth = 150f;
	//public float pisteTransitionWidth = 250f;
	//public float GetPisteLerp(float x, float y)
	//{
	//    return (Mathf.Abs(x) - pisteWidth) / pisteTransitionWidth;
	//}

	[Header("Main piste params")]
	public float pisteWidth = 150f;
	public float pisteTransitionWidth = 250f;

	public FastNoise pisteNoise1D = new FastNoise();

	[Header("Piste curviness")]
	public float sideWobble = 1000;

	/// <summary>
	/// Returns the (abs) distance to the main piste. 0 is piste center. Check pisteWidth and pisteTransitionWidth for detecting the region
	/// </summary>
	/// <param name="x"></param>
	/// <param name="y"></param>
	/// <returns></returns>
	public float GetPisteDist(float x, float y)
	{
		if (y < 0)
		{
			return -y + Mathf.Abs(x);
		}

		float sideFluctuate = pisteNoise1D.GetNoise(0, y) * sideWobble;

		return Mathf.Abs(x - sideFluctuate);
	}
	
	public Vector3 GetPisteForward(float x, float y)
	{
		var sideFluct = pisteNoise1D.GetNoise(0, y);

		var plusOne = pisteNoise1D.GetNoise(0, y - 10f);

		float deltaX = (plusOne - sideFluct) * sideWobble;

		return new Vector3(deltaX, 0, -10f);
	}
}

