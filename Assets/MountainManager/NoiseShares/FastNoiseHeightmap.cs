﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using VRCore;
using System.Linq;
using System;
using Polybrush;

[ExecuteInEditMode]
public class FastNoiseHeightmap : FastNoiseShare
{
    // non-serializable dictionary
    // always save global space coords! and use those when comparing too!
    private Dictionary<Vector2, float> heightData = new Dictionary<Vector2, float>();
    private Dictionary<Vector2, float> weightData = new Dictionary<Vector2, float>();

    public float defaultValue;

    [Header("Heights mesh")]
    [SerializeField]
    private MeshFilter _heightsMesh;

    // used to check if the mesh is the same or if it has renewed, so we need to repaint it again with the colors.
    [SerializeField, ReadOnly]
    private Mesh heightsMeshSharedMeshRef;

    [Header("Brush params", order = 0)]

    [HelpBox("Green corresponds to weightData. Make sure colors are saved on the mesh itself, not in additionalVertexStreams", HelpBoxType.Info, order = 1)]
    // make false in inspector to reinitialize brush mode.
    public bool initializedBrushMode = false;

    [Header("When true, this script saves mountain heights, where the mesh has green vertex color.")]
    public bool paintHeightsMode = false;

    // for displaying the affected parts for vertex painting and shit.
    [SerializeField]
    private Material _vertexColorMat;
    public Material vertexColorMat
    {
        get
        {
            if (_vertexColorMat == null)
            {
                _vertexColorMat = Resources.Load("SlopeVertexColorsMat") as Material;
            }
            return _vertexColorMat;
        }
    }

    private Color[] colorsCache = new Color[0];

    [Header("Instead of brushes")]
    public LayerMask weightsLayer;
    public Vector2 weightRange = new Vector2(-1000, 0f);

    [Header("dictionary contents that can serialize")]
    // serializablde dictionary shit
    [SerializeField, ReadOnly]
    private List<Vector2> positions = new List<Vector2>();

    [SerializeField, ReadOnly]
    private List<float> heights = new List<float>();

    [SerializeField, ReadOnly]
    private List<float> weights = new List<float>();

    public override float GetNoise(float x, float y)
    {
        var c = new Vector2(x, y);
        if (heightData.ContainsKey(c))
        {
            return heightData[c];
        }
        else
        {
            return defaultValue;
        }
    }

    public float GetNoise(float x, float y, out float weight)
    {
        // x and y are origin-shifted
        var c = new Vector2(x, y);
        if (heightData.ContainsKey(c))
        {
            weight = weightData[c];
            return heightData[c];
        }
        else
        {
            weight = 0f;
            return defaultValue;
        }
    }

    private void OnEnable()
    {
        // because dict gets lost, only lists can store the info and pass it on to the dict at runtime.
        ListsToDictionary();
    }

    private void OnValidate()
    {
        if (Application.isEditor && !Application.isPlaying)
        {
            // this is a good idea except when painting directly into the dictionary somehow
            // but we never do that
            ListsToDictionary();
        }

        if (vertexColorMat != null)
        {
            // just ckceking
        }
    }

    [DebugButton]
    void DictionaryToLists()
    {
        positions = heightData.Keys.ToList();
        heights = heightData.Values.ToList();
        weights = weightData.Values.ToList();
    }

    [DebugButton]
    void ListsToDictionary()
    {
        heightData.Clear();
        for (int i = 0; i < positions.Count; i++)
        {
            heightData[positions[i]] = heights[i];
        }

        weightData.Clear();
        if (weights.Count != positions.Count)
        {
            weights = new List<float>(positions.Count);
            for (int i = 0; i < positions.Count; i++)
            {
                weights.Add(1f);
            }
        }

        for (int i = 0; i < positions.Count; i++)
        {
            weightData[positions[i]] = weights[i];
        }

    }

    [DebugButton]
    void ClearAllData()
    {
        heightData.Clear();
        weightData.Clear();
        positions.Clear();
        heights.Clear();
        weights.Clear();
    }

#if UNITY_EDITOR
    private void OnGUI()
    {
        HandleEditorBrushes();
    }
#endif

#if UNITY_EDITOR
    /// <summary>
    /// NOTE: we can use any method of painting meshes, as long as we update the conditions in this script.
    /// </summary>
    private void HandleEditorBrushes()
    {
        if (!paintHeightsMode)
        {
            if (initializedBrushMode)
            {
                DisableBrushMode();
            }
            return;
        }

        if (!initializedBrushMode)
        {
            CheckAndEnableBrushMode();
        }
        else
        {
            if (CheckCancelBrushMode())
            {
                DisableBrushMode();
            }
        }

        if (Event.current != null)
        {
            if (initializedBrushMode)
            {
                // update dictionaries based on the colors of the mesh... indirect shit right here
                UpdateDictionariesFromMeshColors();

                // after manip dictionary, we must serialize so we don't lose the data
                DictionaryToLists();
            }
        }

    }

#endif // editor only

    private void DisableBrushMode()
    {
        ClearUnusedHeights();
        initializedBrushMode = false;
        if (_heightsMesh != null)
        {
            RemoveVertexColorMat(_heightsMesh.GetComponent<Renderer>());
            heightsMeshSharedMeshRef = null;
        }
    }

    private void CheckAndEnableBrushMode()
    {
        // check if we can enable brush mode (and also store refs to useful shit like the heightsMesh)
        var hp = MountainHeightProviderSingleton.instance;
        if (hp != null)
        {
            var meshFilters = hp.heightMeshFilter.meshesParent.GetComponentsInChildren<MeshFilter>();
            if (meshFilters.Length == 1)
            {
                // cannot be null now
                _heightsMesh = meshFilters[0];

                heightsMeshSharedMeshRef = _heightsMesh.sharedMesh;

                // assume this mesh filter is the object we are changing. doesn't matter how we modify it, as long as we use the green channel for weight data, and height data is just the Y position.
                var r = _heightsMesh.gameObject.GetComponent<Renderer>();
                AddVertexColorMat(r);

                // set vertex colors so we can see what is already painted
                InitVertexColorsFromDictionary();

                initializedBrushMode = true;
            }
        }
    }

    // we initialize the vertex colors to represent painted data. initialized painted data.
    private void InitVertexColorsFromDictionary()
    {
        if (_heightsMesh == null || _heightsMesh.sharedMesh == null)
        {
            return;
        }

        var sm = _heightsMesh.sharedMesh;
        var vertices = sm.vertices;

        // init to black, same count as vertices.
        Color[] weightDataToColors = vertices.Select(v => new Color(0, 0, 0)).ToArray();

        // assign colors based on dict of heights/weights
        for (int i = 0; i < vertices.Length; i++)
        {
            // is it the original mesh or the new mesh?????? here in the init func it is the regular mesh, cause the add mesh was just created potentially.
            var vert = vertices[i];

            var globalVertex = OriginShiftManager.LocalToGlobalPos(vert);
            var coord = new Vector2(globalVertex.x, globalVertex.z);
            if (heightData.ContainsKey(coord) && weightData.ContainsKey(coord))
            {
                var h = heightData[coord];
                var w = weightData[coord];
                weightDataToColors[i] = new Color(0f, w, 0f); // 1:1 weight-to-green ratio
            }
        }

        _heightsMesh.sharedMesh.colors = weightDataToColors;
    }

    // after initializing, any changes to the vertex colors means a paint job, so we must update weights array based on color (presence of GREEN) and height based on localposition.y
    private void UpdateDictionariesFromMeshColors()
    {
        if (_heightsMesh == null || _heightsMesh.sharedMesh == null)
        {
            return;
        }

        var sm = _heightsMesh.sharedMesh;
        var vertices = sm.vertices;

        // we assume vertices count == colors count. if not, we abort mission.
        var meshColors = _heightsMesh.sharedMesh.colors;
        if (meshColors.Length != vertices.Length)
        {
            if (meshColors.Length != 0)
            {
                Debug.LogError("[FastNoiseHeightmap] Problem with mesh colors: different count even though we didn't do nothing wrong!");
            }
            else
            {
                // colors.length is zero, do nothing.
            }

            return;
        }

        // save ALL colors and heights, and later discard the ones with 0 so we save some memory
        for (int i = 0; i < vertices.Length; i++)
        {
            // constantly update dictionaries, so the color always reflects the state of the stored data (weights). this time color has authority.
            var vert = vertices[i];

            // weight is green amount, height is plain height of mesh at this position, and we lock it in place.
            var c = meshColors[i];

            // coord in mesh space:
            // to global space:
            var verticesGlobal = OriginShiftManager.LocalToGlobalPos(vert);
            var coord = new Vector2(verticesGlobal.x, verticesGlobal.z);

            heightData[coord] = verticesGlobal.y;
            weightData[coord] = c.g;

        }
    }

    /// <summary>
    /// Returns true when we should finalize brush mode (initialized should be false)
    /// </summary>
    /// <returns></returns>
    private bool CheckCancelBrushMode()
    {
        // if height provider is gone
        var hp = MountainHeightProviderSingleton.instance;
        if (hp == null)
        {
            return true;
        }

        // if mesh filter count is not 1
        var meshFilters = hp.heightMeshFilter.meshesParent.GetComponentsInChildren<MeshFilter>();
        if (meshFilters.Length != 1)
        {
            return true;
        }

        if (_heightsMesh == null)
        {
            return true;
        }

        if (heightsMeshSharedMeshRef != _heightsMesh.sharedMesh)
        {
            return true;
        }

        return false;
    }

    [DebugButton]
    private void ClearUnusedHeights()
    {
        ListsToDictionary();

        var toRemove = new HashSet<Vector2>();
        // check if weight data is zero
        foreach (var h in heightData)
        {
            // if there is no weight for that key, or if the weight is zero
            if (!weightData.ContainsKey(h.Key) || weightData[h.Key] == 0)
            {
                // mark for remove key
                toRemove.Add(h.Key);
            }
        }
        foreach (var r in toRemove)
        {
            heightData.Remove(r);
            weightData.Remove(r);
        }

        DictionaryToLists();

    }

    private void AddVertexColorMat(Renderer r)
    {
        if (vertexColorMat == null)
        {
            Debug.LogError("Please assign a vertex color material", gameObject);
            return;
        }
        if (!r.sharedMaterials.Contains(vertexColorMat))
        {
            r.sharedMaterials = r.sharedMaterials.Concat(new Material[] { vertexColorMat });
        }
    }

    private void RemoveVertexColorMat(Renderer r)
    {
        if (r.sharedMaterials.Contains(vertexColorMat))
        {
            var sm = r.sharedMaterials.ToList();
            sm.Remove(vertexColorMat);
            r.sharedMaterials = sm.ToArray();
        }
    }


    // ===============================  in place of brushes  =============================== //

    [DebugButton]
    void GetAllDataFromMesh()
    {
        if (_heightsMesh == null)
        {
            _heightsMesh = GetComponentInChildren<MeshFilter>();
        }
        var mesh = _heightsMesh.sharedMesh;
        var verts = mesh.vertices;
        positions.Clear();
        heights.Clear();
        // local to global pos = adding.
        var os = OriginShiftManager.originShift;
        for (int i = 0; i < verts.Length; i++)
        {
            positions.Add(new Vector2(verts[i].x + os.x, verts[i].z + os.z));
            heights.Add(verts[i].y + os.y);
        }

        ListsToDictionary();
    }

    [DebugButton]
    void UpdateHeightsFromMesh()
    {
        ListsToDictionary();
        if (_heightsMesh == null)
        {
            _heightsMesh = GetComponentInChildren<MeshFilter>();
        }
        var mesh = _heightsMesh.sharedMesh;
        var verts = mesh.vertices;
        // local to global pos = adding.
        var os = OriginShiftManager.originShift;
        for (int i = 0; i < verts.Length; i++)
        {
            var c = new Vector2(verts[i].x + os.x, verts[i].z + os.z);
            if (heightData.ContainsKey(c))
            {
                heightData[c] = verts[i].y + os.y;
            }
        }

        DictionaryToLists();
    }

    [DebugButton]
    void SetWeightsWithRaycasts()
    {
        for (int i = 0; i < positions.Count; i++)
        {
            // global to localpos = subtracting.
            var os = OriginShiftManager.originShift;
            var start = new Vector3(positions[i].x - os.x, 0, positions[i].y - os.z);
            float weight = 0;
            RaycastHit hitInfo;
            var hit = Physics.Raycast(start + Vector3.up * 1000, Vector3.down * 10000, out hitInfo, 10000f, weightsLayer);
            if (hit)
            {
                weight = Mathf.InverseLerp(weightRange.x, weightRange.y, hitInfo.point.y);
            }
            Debug.DrawRay(start, Vector3.up * (weight * 1000 + 100), Color.red, 3f);
            weights[i] = weight;
        }
        ListsToDictionary();
    }

    [DebugButton]
    void AddHeightOffset(float height)
    {
        for (int i = 0; i < heights.Count; i++)
        {
            heights[i] += height;
        }
        ListsToDictionary();
    }

}
