using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class FastNoiseTreeShare : FastNoiseBasic
{
	[Header("Piste")]
	public FastNoisePiste piste;

	// how close to the center should it make trees?
	[Header("Offsets tree limit by ratio of pisteWidth")]
	public float pisteBorderRatio = 1f;

	[Header("Transition ratio")]
	public float pisteTransitionRatio = 1f;

	[Header("Tree density params")]
	public float pisteConstant = -1;

	public override float GetNoise(float x, float y)
	{
		var offPisteNoise = noise.GetNoise(x, y);
		var pisteDist = piste.GetPisteDist(x, y);
		var pisteWidth = piste.pisteWidth * pisteBorderRatio;
		var transitionWidth = piste.pisteTransitionWidth * pisteTransitionRatio;
		if (pisteDist < pisteWidth)
		{
			return pisteConstant;
		}
		else if (pisteDist > transitionWidth)
		{
			return offPisteNoise;
		}
		else
		{
			// 0 is on piste, 1 is off
			var pisteLerp = Mathf.InverseLerp(pisteWidth, transitionWidth, pisteDist);
			return Mathf.Lerp(pisteConstant, offPisteNoise, pisteLerp);
		}
	}
}