﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(FastNoiseLayeredShare))]
public class FastNoiseLayeredShareEditor : Editor
{
    private FastNoiseLayeredShare fastNoiseLayered;

    private void OnEnable()
    {
        fastNoiseLayered = target as FastNoiseLayeredShare;
        fastNoiseLayered.InitLayers();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        EditorGUILayout.LabelField("Noise Layers", EditorStyles.boldLabel);
        
        EditorGUI.indentLevel++;
        for (int i = 0; i < fastNoiseLayered.noiseLayers.Length; i++)
        {
            var layer = fastNoiseLayered.noiseLayers[i] as MonoBehaviour;

            EditorGUILayout.ObjectField(layer.name, layer, layer.GetType(), true);

        }
        EditorGUI.indentLevel--;
    }
}