using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFastNoiseShare
{
    // doesn't have to have a field called "noise". it's just the simplest way to get noisy
    //FastNoise noise { get; }

    float GetNoise(float x, float y);
}
