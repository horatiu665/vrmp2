using UnityEngine;

// inherit other classes from this when using the basic format with 1 noise and 1 noise function
public class FastNoiseBasic : FastNoiseShare, IFastNoiseShare
{
    [SerializeField]
    private FastNoise _noise = new FastNoise();
    public FastNoise noise { get { return _noise; } }
    
    public override float GetNoise(float x, float y)
    {
        return noise.GetNoise(x, y);
    }
    
}

