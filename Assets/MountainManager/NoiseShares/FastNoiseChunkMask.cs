﻿using Mountain.Filters;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class FastNoiseChunkMask : FastNoiseShare
{
	[HelpBox("Objects can be spawned in chunks using a job filter. Save them at runtime after painting with whatever tools.", HelpBoxType.Info)]
	public int gridSize = 150;

	public LayerMask paintLayer = -1;

	#region ARRAY SHIT

	// 2D array of lists. but 1d for serialization
	[HideInInspector]
	[SerializeField]
	bool[] _chunks = new bool[1];

	// index offset for getting array elements. ultimately kind of pretends to be a dictionary. To iterate the array, use -_arrayIndexOffset at the beginning.
	[HideInInspector]
	[SerializeField]
	SpawnJobCoord _arrayIndexOffset;

	[HideInInspector]
	[SerializeField]
	SpawnJobCoord _arrayMaxSize = new SpawnJobCoord(0, 0);

	[SerializeField, ReadOnly]
	private int _markedChunks = 0;

	[Header("Gizmos")]
	[SerializeField]
	private bool showGizmos = true;


	private int w
	{
		get
		{
			return _arrayMaxSize.x;
		}
	}

	private int h
	{
		get
		{
			return _arrayMaxSize.y;
		}
	}

	private SpawnJobCoord minCoordInArray
	{
		get
		{
			return new SpawnJobCoord(-_arrayIndexOffset.x, -_arrayIndexOffset.y);
		}
	}

	private SpawnJobCoord maxCoordInArray
	{
		get
		{
			return _arrayMaxSize - _arrayIndexOffset - new SpawnJobCoord(1, 1);
		}
	}

	// MUST be called before operations where the coords are likely to be out of bounds
	// this tells the array what position MUST be contained, so the array scales with the appropriate offset.
	// ONLY EVER INCREASES SIZE!!!
	private void InitArrays(SpawnJobCoord realCoord)
	{
		var deltadelta = new SpawnJobCoord(0, 0);
		var oldMax = _arrayMaxSize;
		if (ArrayBoundsInit(realCoord, ref deltadelta))
		{
			//print("YEAH! resizing. Realcoord: " + realCoord + " deltadelta: " + deltadelta + " oldmax : " + oldMax + " maxsize: " + _arrayMaxSize);
			var newDict = _chunks;
			newDict = new bool[_arrayMaxSize.x * _arrayMaxSize.y];
			for (int x = 0; x < oldMax.x; x++)
			{
				for (int y = 0; y < oldMax.y; y++)
				{
					// move jobs[i,j] to newjobs[i+deltax, j+deltay]
					newDict[(x + deltadelta.x) * h + y + deltadelta.y] = _chunks[x * oldMax.y + y];
				}
			}

			_chunks = newDict;
		}
	}

	// only used in InitArrays()
	private bool ArrayBoundsInit(SpawnJobCoord realCoord, ref SpawnJobCoord deltadelta)
	{
		bool changed = false;

		if (realCoord.x + _arrayIndexOffset.x < 0)
		{
			// new delta (-x) minus old delta (arrayIndexOffset.x)
			deltadelta.x = -realCoord.x - _arrayIndexOffset.x;
			_arrayIndexOffset.x = -realCoord.x;
			_arrayMaxSize.x += deltadelta.x;
			changed = true;
		}
		else if (realCoord.x + _arrayIndexOffset.x >= w)
		{
			_arrayMaxSize.x = realCoord.x + _arrayIndexOffset.x + 1;
			changed = true;
		}

		if (realCoord.y < -_arrayIndexOffset.y)
		{
			deltadelta.y = -realCoord.y - _arrayIndexOffset.y;
			_arrayIndexOffset.y = -realCoord.y;
			_arrayMaxSize.y += deltadelta.y;
			changed = true;
		}
		else if (realCoord.y + _arrayIndexOffset.y >= h)
		{
			_arrayMaxSize.y = realCoord.y + _arrayIndexOffset.y + 1;
			changed = true;
		}
		return changed;
	}

	// long but simple - checks all edges of the min/max spawned array, and reduces bounds if there is no marked data in that range.
	private void ReduceArraySize()
	{
		// minCoordEver is array[0,0], equivalent chunk = -indexOffset
		var minCoordEver = minCoordInArray;
		// maxCoordEver is array[size-1, size-1], equivalent chunk = maxSize - 1 - indexOffset.
		var maxCoordEver = maxCoordInArray;

		// check if there is anything on the minCoordEver.y row.
		bool foundSomething = false;
		while (!foundSomething && minCoordEver.y <= maxCoordEver.y)
		{
			foundSomething = false;
			for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
			{
				var coordMin = new SpawnJobCoord(x, minCoordEver.y);
				if (FoundSomethingHere(coordMin))
				{
					foundSomething = true;
					break;
				}
			}
			// we did not find any trees on this row! don't search it as min. coord anymore.
			// and check again for the next row.
			if (!foundSomething)
			{
				minCoordEver.y++;
			}
		}

		// check if there is anything on the maxCoordEver.y row.
		foundSomething = false;
		while (!foundSomething && minCoordEver.y <= maxCoordEver.y)
		{
			foundSomething = false;
			for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
			{
				var coordMin = new SpawnJobCoord(x, maxCoordEver.y);
				if (FoundSomethingHere(coordMin))
				{
					foundSomething = true;
					break;
				}
			}
			if (!foundSomething)
			{
				maxCoordEver.y--;
			}
		}


		// check if there is anything on the mincoordever.x row.
		foundSomething = false;
		while (!foundSomething && minCoordEver.x <= maxCoordEver.x)
		{
			foundSomething = false;
			for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
			{
				var coordMin = new SpawnJobCoord(minCoordEver.x, y);
				if (FoundSomethingHere(coordMin))
				{
					foundSomething = true;
					break;
				}
			}

			if (!foundSomething)
			{
				minCoordEver.x++;
			}
		}


		// check if there is anything on the mincoordever.x row.
		foundSomething = false;
		while (!foundSomething && minCoordEver.x <= maxCoordEver.x)
		{
			foundSomething = false;
			for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
			{
				var coordMin = new SpawnJobCoord(maxCoordEver.x, y);
				if (FoundSomethingHere(coordMin))
				{
					foundSomething = true;
					break;
				}
			}
			if (!foundSomething)
			{
				maxCoordEver.x--;
			}
		}

		// minCoord and maxCoord now represent the edges of the new array.
		// we must move the old contents at these positions in a new array which only contains this new range.

		//print("YEAH! resizing. Realcoord: " + realCoord + " deltadelta: " + deltadelta + " oldmax : " + oldMax + " maxsize: " + _arrayMaxSize);
		var newDict = _chunks;
		// max - min + 1 is the size of the new array
		var newMax = maxCoordEver - minCoordEver + new SpawnJobCoord(1, 1);
		newDict = new bool[newMax.x * newMax.y];
		for (int xx = minCoordEver.x; xx <= maxCoordEver.x; xx++)
		{
			for (int yy = minCoordEver.y; yy <= maxCoordEver.y; yy++)
			{
				// take elements from [(x,y) + offset] of the old array, and put them in the new array at [(x,y) - minCoordEver]
				newDict[(xx - minCoordEver.x) * newMax.y + yy - minCoordEver.y] = _chunks[(xx + _arrayIndexOffset.x) * h + (yy + _arrayIndexOffset.y)];
			}
		}

		_chunks = newDict;
		_arrayIndexOffset = new SpawnJobCoord(0, 0) - minCoordEver;
		_arrayMaxSize = newMax;
	}

	private bool FoundSomethingHere(SpawnJobCoord coord)
	{
		return GetChunk(coord);
	}

	// returns true when this chunk is marked true
	public bool GetChunk(SpawnJobCoord worldSpaceChunk)
	{
		var i = worldSpaceChunk + _arrayIndexOffset;
		// null if out of range
		if (i.x < 0 || i.y < 0 || i.x >= w || i.y >= h)
		{
			return false;
		}

		return _chunks[i.x * h + i.y];
	}

	public void SetChunk(SpawnJobCoord worldSpaceChunk, bool value)
	{
		var i = worldSpaceChunk + _arrayIndexOffset;
		// break if out of range
		if (i.x < 0 || i.y < 0 || i.x >= w || i.y >= h)
		{
			Debug.Log("[FastNoiseLayerChunkMask] Want to set a chunk that is not in the array! InitArrays() first!");
			return;
		}
		_chunks[i.x * h + i.y] = value;
	}

	#endregion

	// EDITOR. Paint data using collider children, and applying them like a stamp, to darken the grid.
	[DebugButton]
	void AddDataFromColliders()
	{
		SetDataFromChildColliders(true);
	}

	[DebugButton]
	void RemoveDataFromColliders()
	{
		SetDataFromChildColliders(false);
	}

	void SetDataFromChildColliders(bool add)
	{
		var cols = GetComponentsInChildren<Collider>();
		if (cols.Length > 0)
		{
			// find chunk bounds of all colliders by iterating the list
			var minBound = Vector3.one * float.MaxValue;
			var maxBound = Vector3.one * float.MinValue;
			for (int i = 0; i < cols.Length; i++)
			{
				var c = cols[i];
				if (minBound.x > c.bounds.min.x)
				{
					minBound.x = c.bounds.min.x;
				}
				if (minBound.y > c.bounds.min.y)
				{
					minBound.y = c.bounds.min.y;
				}
				if (minBound.z > c.bounds.min.z)
				{
					minBound.z = c.bounds.min.z;
				}
				if (maxBound.x < c.bounds.max.x)
				{
					maxBound.x = c.bounds.max.x;
				}
				if (maxBound.y < c.bounds.max.y)
				{
					maxBound.y = c.bounds.max.y;
				}
				if (maxBound.z < c.bounds.max.z)
				{
					maxBound.z = c.bounds.max.z;
				}
			}

			var os = SpawnJobCoord.PosToCoord(OriginShiftManager.originShift, gridSize);

			// we have to keep those in non-shifted space for the overlapbox position
			var minChunk = SpawnJobCoord.PosToCoord(minBound, gridSize);
			var maxChunk = SpawnJobCoord.PosToCoord(maxBound, gridSize);

			// we have to init the shifted pos in case we are editing away from the origin
			InitArrays(minChunk + os);
			InitArrays(maxChunk + os);

			// iterates all chunks in square and marks if there are colliders here or not, using overlap box
			for (int x = minChunk.x; x <= maxChunk.x; x++)
			{
				for (int y = minChunk.y; y <= maxChunk.y; y++)
				{
					var c = new SpawnJobCoord(x, y);
					var castPos = SpawnJobCoord.CoordToPos(c, gridSize);
					var castSize = new Vector3(gridSize, 100000, gridSize);
					var hitAnything = Physics.OverlapBox(castPos, castSize / 2, Quaternion.identity, paintLayer).Length > 0;
					if (hitAnything)
					{
						SetChunk(c + os, add);
						_markedChunks += add ? 1 : -1;
					}
				}
			}
		}

	}

	[DebugButton]
	private void ClearData()
	{
#if UNITY_EDITOR
		UnityEditor.Undo.RecordObject(this, "Clear FastNoiseLayerChunkMask");
#endif

		_arrayIndexOffset = new SpawnJobCoord(0, 0);
		_arrayMaxSize = new SpawnJobCoord(0, 0);
		_chunks = new bool[1];
		InitArrays(new SpawnJobCoord(0, 0));
		_markedChunks = 0;
	}

	public override float GetNoise(float x, float y)
	{
		var coord = SpawnJobCoord.PosToCoord(new Vector3(x, 0, y), gridSize);
		return GetChunk(coord) ? 0f : 1f;
	}

	private void OnDrawGizmos()
	{
		if (showGizmos)
		{
			// minCoordEver is array[0,0], equivalent chunk = -indexOffset
			var minCoordEver = new SpawnJobCoord(-_arrayIndexOffset.x, -_arrayIndexOffset.y);
			// maxCoordEver is array[size-1, size-1], equivalent chunk = maxSize - 1 - indexOffset.
			var maxCoordEver = _arrayMaxSize - _arrayIndexOffset - new SpawnJobCoord(1, 1);
			for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
			{
				for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
				{
					var coord = new SpawnJobCoord(x, y);
					var marked = GetChunk(coord);
					var col = marked ? Color.red : Color.green;
					col.a = 0.6f;
					Gizmos.color = col;
                    var cubePos = SpawnJobCoord.CoordToPos(coord, gridSize);
                    cubePos = OriginShiftManager.GlobalToLocalPos(cubePos);
                    cubePos.y = 0;
                    Gizmos.DrawCube(cubePos, Vector3.one * gridSize * 0.8f);
				}
			}
		}
	}
}