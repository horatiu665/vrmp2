using UnityEngine;

// reference this in scripts for easy sharing of editable noise function
public abstract class FastNoiseShare : MonoBehaviour, IFastNoiseShare
{
    // x and y in noise function space (usually global)
    public abstract float GetNoise(float x, float y);
    
}

