using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using TriangleNet;
using TriangleNet.Geometry;
using TriangleNet.Meshing;
using System;

public class DemoTriangle : MonoBehaviour
{
    private MeshFilter mf;
    private MeshRenderer mr;

    public Material meshMat;

    public float minAngle = 5f;

    [Header("This will be roughly the size of the edges of the mesh")]
    public float maxAreaSqrt = 100f;

    [Header("Red square shows the approx size of 1 cell")]
    public bool showAreaDebug = true;

    public float heightScale = 1f;

    public FastNoise noise = new FastNoise();

    public List<DemoVertex> vertices = new List<DemoVertex>();

    private void Start()
    {
        InitVertices();

        DoIt();
    }

    void InitVertices()
    {
        vertices.Clear();
        for (int i = 0; i < transform.childCount; i++)
        {
            var demoV = transform.GetChild(i).GetComponent<DemoVertex>();
            if (demoV != null)
            {
                vertices.Add(demoV);
            }
        }
    }

    [DebugButton]
    void DoIt()
    {
        if (vertices.Count != transform.childCount)
        {
            InitVertices();
        }

        Polygon p = new Polygon();
        // populate with bunch of vertices and shit

        for (int i = 0; i < vertices.Count; i++)
        {
            var v = vertices[i];
            var tv = v.GetVertex();
            p.Add(tv);

        }

        ConstraintOptions options = new ConstraintOptions();
        QualityOptions quality = new QualityOptions();

        quality.MinimumAngle = minAngle;
        quality.MaximumArea = maxAreaSqrt * maxAreaSqrt;

        var mesh = p.Triangulate(options, quality);

        var debug = "";
        debug += mesh.Triangles.Count + " triangles" + "\n";
        debug += mesh.Vertices.Count + " verts" + "\n";
        debug += mesh.Edges.Count() + " edges" + "\n";
        print(debug);

        // render mesh
        UnityEngine.Mesh m = new UnityEngine.Mesh();
        //Vector3[] verts = new Vector3[mesh.Vertices.Count];
        //var meshVertices = mesh.Vertices.ToList();
        //for (int i = 0; i < meshVertices.Count; i++)
        //{
        //    var v = meshVertices[i];
        //    // set Y to the original vertices position.
        //    print(i);
        //    verts[i] = new Vector3((float)v.X, vertices[i].transform.position.y, (float)v.Y);
        //}

        mesh.Renumber();
        m.vertices = mesh.Vertices.Select(v => new Vector3((float)v.X, GetHeight(v) * heightScale, (float)v.Y)).ToArray();
        m.triangles = mesh.Triangles.Select(t =>
            {
                bool add = true;
                for (int i = 0; i < 3; i++)
                {
                    if (t.GetVertexID(i) < 0 || t.GetVertexID(i) >= m.vertices.Length)
                    {
                        Debug.Log("AAAAAAH " + t.ID + " verts: " + t.GetVertexID(0) + " " + t.GetVertexID(2) + " " + t.GetVertexID(1));
                        add = false;
                    }
                }
                if (add)
                {
                    return new int[3] { t.GetVertexID(0), t.GetVertexID(2), t.GetVertexID(1) };
                }
                else
                {
                    return new int[3] { 0, 0, 0 };
                }
            }
            ).SelectMany(intarr => intarr).ToArray();
        m.RecalculateNormals();
        m.RecalculateBounds();

        if (mf == null)
        {
            mf = GetComponent<MeshFilter>();
            if (mf == null) mf = gameObject.AddComponent<MeshFilter>();
            mr = GetComponent<MeshRenderer>();
            if (mr == null) mr = gameObject.AddComponent<MeshRenderer>();
            mr.sharedMaterial = meshMat;
        }
        mf.sharedMesh = m;

    }

    [DebugButton]
    void Clear()
    {
        if (mf != null)
        {
            mf.sharedMesh = null;
        }
    }

    private float GetHeight(Vertex v)
    {
        return noise.GetNoise((float)v.X, (float)v.Y);
    }

    private void OnDrawGizmosSelected()
    {
        if (showAreaDebug)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(transform.position, new Vector3(maxAreaSqrt, 0.01f, maxAreaSqrt));
        }
    }
}