using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TriangleNet.Geometry;
using UnityEngine;

public class DemoVertex : MonoBehaviour
{
    public Vertex GetVertex()
    {
        return new Vertex(transform.localPosition.x, transform.localPosition.z);
    }

}
