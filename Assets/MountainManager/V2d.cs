﻿namespace Mountain
{
	public struct V2d
	{
		public double x, y;
		public V2d(double x, double z)
		{
			this.x = x;
			y = z;
		}

		public static V2d operator -(V2d a, V2d b)
		{
			return new V2d(a.x - b.x, a.y - b.y);
		}
	}
}