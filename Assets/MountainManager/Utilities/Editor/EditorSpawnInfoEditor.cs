﻿namespace Mountain.Editor
{

    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEditor;
    using Random = UnityEngine.Random;
    using System;

    [CustomEditor(typeof(EditorSpawnInfo))]
    [CanEditMultipleObjects]
    public class EditorSpawnInfoEditor : Editor
    {
        private EditorSpawnInfo _spawnInfo;
        public EditorSpawnInfo spawnInfo
        {
            get
            {
                if (_spawnInfo == null)
                {
                    _spawnInfo = target as EditorSpawnInfo;
                }
                return _spawnInfo;
            }
        }

        public List<EditorSpawnInfo> spawnInfos
        {
            get
            {
                return targets.Select(t => t as EditorSpawnInfo).Where(t => t != null).ToList();
            }
        }


        public override void OnInspectorGUI()
        {
            if (targets.Length == 1)
            {
                SingleInspector();
            }
            else
            {
                MultiInspector();
            }

        }

        private void MultiInspector()
        {
            var sourcesGroupedByType = spawnInfos.GroupBy(esi => esi.spawnSourceType);
            var sourcesGroupedByTypeCount = sourcesGroupedByType.Count();
            var plural = (sourcesGroupedByTypeCount > 1 ? "s" : "");
            var latext = "Objects spawned by " + sourcesGroupedByTypeCount + " type" + plural + " of source" + plural + ": ";
            foreach (var sg in sourcesGroupedByType)
            {
                latext += sg.First().spawnSourceType + ", ";
            }
            EditorGUILayout.LabelField(latext);

            var needsUpdate = spawnInfos.Any(si => si.needsUpdate);

            GUI.color = needsUpdate ? Colors.MediumVioletRed : Color.white;
            if (GUILayout.Button(needsUpdate ? "Objects changed. Update Data Here" : "Update Data"))
            {
                bool anyFails = false;
                EditorSpawnInfo firstFail = null;
                for (int i = 0; i < spawnInfos.Count; i++)
                {
                    var s = spawnInfos[i];
                    bool success = ButtonAction_UpdateSpawnData(s);
                    if (!success)
                    {
                        anyFails = true;
                        if (firstFail == null)
                        {
                            firstFail = s;
                        }
                    }
                }
                if (anyFails)
                {
                    DebugLogError("[EditorSpawnInfo] Error updating one or more objects!", firstFail);
                }
                else
                {
                    DebugLog("[EditorSpawnInfo] Updated " + spawnInfos.Count + " objects");
                }
            }
            GUI.color = Color.red;

            if (GUILayout.Button("Delete from spawner"))
            {
                bool anyFails = false;
                EditorSpawnInfo firstFail = null;
                for (int i = 0; i < spawnInfos.Count; i++)
                {
                    var s = spawnInfos[i];
                    var succ = ButtonAction_DeleteObject(s);
                    if (!succ)
                    {
                        anyFails = true;
                        if (firstFail == null)
                        {
                            firstFail = s;
                        }
                    }
                }
                if (anyFails)
                {
                    DebugLogError("[EditorSpawnInfo] Could not delete from spawner " + spawnInfo, spawnInfo);
                }
                else
                {
                    DebugLog("[EditorSpawnInfo] Deleted " + spawnInfos.Count + " objects from their spawners", spawnInfo);
                    // only focus on scene when no errors ;)
                    ButtonAction_PostDelete();
                }

            }

            GUI.color = Color.white;

        }

        private void SingleInspector()
        {
            var type = spawnInfo.spawnSourceType;
            if (type == EditorSpawnInfo.SpawnSourceType.None)
            {
                EditorGUILayout.LabelField("Object spawned by hand or undefined");
            }
            else
            {
                EditorGUILayout.LabelField("Object spawned procedurally");
            }

            bool canUpdate = false;
            switch (type)
            {
            case EditorSpawnInfo.SpawnSourceType.Procedural:
                EditorGUILayout.LabelField("... by procedural system. Cannot tweak.");
                EditorGUILayout.ObjectField(spawnInfo.sourceGeneric, typeof(MonoBehaviour), true);
                break;
            case EditorSpawnInfo.SpawnSourceType.Chunks:
                EditorGUILayout.LabelField("... by chunk system.");
                EditorGUILayout.ObjectField(spawnInfo.sourceChunk, typeof(SpawnableChunkBase), true);
                canUpdate = true;
                break;
            case EditorSpawnInfo.SpawnSourceType.BigObject:
                EditorGUILayout.LabelField("... by BigObject.");
                EditorGUILayout.ObjectField(spawnInfo.sourceBigObject, typeof(BigObject), true);
                canUpdate = true;
                break;
            default:
                break;
            }

            var needsUpdate = spawnInfo.needsUpdate;
            GUI.enabled = canUpdate;
            GUI.color = needsUpdate ? Colors.PaleVioletred : Color.white;
            if (GUILayout.Button(needsUpdate ? "Object changed. Update Data Here" : "Update Data"))
            {
                var success = ButtonAction_UpdateSpawnData(spawnInfo);
                if (success)
                {
                    DebugLog("[EditorSpawnInfo] Updated " + spawnInfo, spawnInfo);
                }
                else
                {
                    DebugLogError("[EditorSpawnInfo] Could not update " + spawnInfo, spawnInfo);
                }
            }

            GUI.color = Color.white;
            GUI.color = Color.red;

            if (GUILayout.Button("Delete from spawner"))
            {
                bool succ = ButtonAction_DeleteObject(spawnInfo);
                if (succ)
                {
                    DebugLog("[EditorSpawnInfo] Deleted from spawner " + spawnInfo, spawnInfo);
                }
                else
                {
                    DebugLogError("[EditorSpawnInfo] Could not delete from spawner " + spawnInfo, spawnInfo);
                }
                ButtonAction_PostDelete();
            }
            GUI.color = Color.white;

            GUI.enabled = true;
        }

        private bool ButtonAction_DeleteObject(EditorSpawnInfo si)
        {
            return si.DeleteFromSpawner();
        }

        private void ButtonAction_PostDelete()
        {
            // make editor focus on scene so we can just press "delete" and delete the shit
            if (SceneView.sceneViews.Count > 0)
            {
                var sv = SceneView.sceneViews[0] as SceneView;
                sv.Focus();
            }
        }

        private bool ButtonAction_UpdateSpawnData(EditorSpawnInfo si)
        {
            return si.UpdateSpawnData();
        }

        // DEBUG FUNCTIONS. MIGHT WANNA CHANGE TO A SEPARATE CONSOLE/?????? MAYBE NOT BUT THIS GIVES THAT OPTION

        private void DebugLogError(string v, UnityEngine.Object context = null)
        {
            Debug.LogError(v, context);
        }

        private void DebugLog(string v, UnityEngine.Object context = null)
        {
            Debug.Log(v, context);
        }

    }
}