﻿namespace Mountain
{
    using System;
    using Helpers;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Mountain.Filters;

    public class EditorSpawnInfoManager : MonoBehaviour
    {
        private static EditorSpawnInfoManager _instance;
        public static EditorSpawnInfoManager instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = FindObjectOfType<EditorSpawnInfoManager>();
                }
                if (_instance == null)
                {
                    _instance = new GameObject("EditorSpawnInfoManager", typeof(EditorSpawnInfoManager)).GetComponent<EditorSpawnInfoManager>();
                }
                return _instance;
            }
        }


        [SerializeField]
        private bool _canAddSpawnInfo = true;

        // returns true when any of the EditorSpawnInfo.needUpdate is true.
        public static bool anyNeedsUpdate
        {
            get
            {
                return EditorSpawnInfo.allSpawnInfo.Any(si => si.needsUpdate);
            }
        }

        public static int countNeedUpdate
        {
            get
            {
                return EditorSpawnInfo.allSpawnInfo.Count(si => si.needsUpdate);
            }
        }

        public bool canAddSpawnInfo
        {
            get
            {
                return _canAddSpawnInfo && Application.isEditor && !Application.isPlaying;
            }
        }

        public void AddSpawnInfoProcedural(IVRPrefab spawnedObject, MonoBehaviour spawnSource)
        {
            if (canAddSpawnInfo)
            {
                var si = spawnedObject.gameObject.GetOrAddComponent<EditorSpawnInfo>();
                si.spawnSourceType = EditorSpawnInfo.SpawnSourceType.Procedural;
                si.sourceGeneric = spawnSource;
            }
        }

        public void AddSpawnInfoChunks(IVRPrefab spawnedObject, SpawnableChunkBase chunkSource, ChunkObject spawnSourceChunk)
        {
            if (canAddSpawnInfo)
            {
                var si = spawnedObject.gameObject.GetOrAddComponent<EditorSpawnInfo>();
                si.sourceChunk = chunkSource;
                si.spawnSourceType = EditorSpawnInfo.SpawnSourceType.Chunks;
                si.sourceChunkObject = spawnSourceChunk;
            }
        }

        public void AddSpawnInfoBigObject(IVRPrefab spawnedObject, BigObject bigObj)
        {
            if (canAddSpawnInfo)
            {
                var si = spawnedObject.gameObject.GetOrAddComponent<EditorSpawnInfo>();
                si.spawnSourceType = EditorSpawnInfo.SpawnSourceType.BigObject;
                si.sourceBigObject = bigObj;
            }
        }

        public static void UpdateAll()
        {
            foreach (var si in EditorSpawnInfo.allSpawnInfo)
            {
                if (si.needsUpdate)
                {
                    si.UpdateSpawnData();
                }
            }
        }

        public static IEnumerable<EditorSpawnInfo> GetAllWhoNeedUpdate()
        {
            return EditorSpawnInfo.allSpawnInfo.Where(si => si.needsUpdate);
        }
    }
}
