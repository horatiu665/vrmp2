using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class ReparentChildrenTool : MonoBehaviour
{
    [Header("Reparent to this transform")]
    public Transform target;

    [Header("Only reparents to null if this is true")]
    public bool toNull = false;

    [DebugButton]
    void Reparent()
    {
#if UNITY_EDITOR
        if (target != null)
        {
            UnityEditor.Undo.SetCurrentGroupName("Reparent " + transform.childCount + " children of " + name + " to " + target.name);
        }
#endif

        if (target != null || toNull)
        {
            while (transform.childCount > 0)
            {
#if UNITY_EDITOR
                if (Application.isEditor)
                {
                    UnityEditor.Undo.SetTransformParent(transform.GetChild(transform.childCount - 1), target, "Reparent");
                }
                else
                {
#endif
                    transform.GetChild(transform.childCount - 1).SetParent(target);
#if UNITY_EDITOR
                }
#endif
            }
        }
    }
}