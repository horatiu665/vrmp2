﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class ReplaceWithPrefabTool : MonoBehaviour
{
    public List<ReplaceConnection> prefabs = new List<ReplaceConnection>();
    public bool keepScale = true;

    [DebugButton]
    void ReplaceAll()
    {
        var children = transform.GetChildren();
        for (int i = 0; i < children.Count; i++)
        {
            var pos = children[i].transform.position;
            var rot = children[i].transform.rotation;
            var ls = children[i].transform.localScale;
            var who = prefabs.FirstOrDefault(rc => children[i].name.Contains(rc.nameContains));
            if (who != null)
            {
                var spawned = SpawnManagerDefault.instance.InstantiateSafe(who.replaceWith, pos, rot, transform);
                if (keepScale)
                {
                    spawned.transform.localScale = ls;
                }
                this.DestroySafe(children[i].gameObject);
            }
        }
    }

    [Serializable]
    public class ReplaceConnection
    {
        public string nameContains;
        public GameObject replaceWith;
    }
}