﻿namespace Mountain
{
    using Helpers;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Mountain.Filters;
    using System;

    /// <summary>
    /// This component is used only for editor functionality when editing spawned objects.
    /// </summary>
    [ExecuteInEditMode]
    public class EditorSpawnInfo : MonoBehaviour
    {
        public static HashSet<EditorSpawnInfo> allSpawnInfo = new HashSet<EditorSpawnInfo>();

        static bool debugConsiderThisAntiSpammer = false;

        private IVRPrefab _vrPrefab;
        public IVRPrefab vrPrefab
        {
            get
            {
                if (_vrPrefab == null)
                {
                    _vrPrefab = GetComponent<IVRPrefab>();
                }
                return _vrPrefab;
            }
        }

        public enum SpawnSourceType
        {
            None = 0, // placed by hand or undefined
            Procedural = 1, // spawned by procedural system using noise or such
            Chunks = 2, // can modify, spawned with chunk system
            BigObject = 3, // can modify, spawned by big object system
        }

        [SerializeField, ReadOnly]
        private SpawnSourceType _spawnSourceType = SpawnSourceType.None;
        public SpawnSourceType spawnSourceType
        {
            get
            {
                return _spawnSourceType;
            }
            set
            {
                _spawnSourceType = value;
            }
        }

        // ======== Data Types ========

        [SerializeField]
        public ChunkObject sourceChunkObject;

        // Chunk Object
        [SerializeField]
        public SpawnableChunkBase sourceChunk;

        // Generic (procedural, cannot modify)
        [SerializeField]
        public MonoBehaviour sourceGeneric;

        // Big Object
        [SerializeField]
        public BigObject sourceBigObject;


        // ======== Needs updating checks ========
        [SerializeField]
        private bool _needsUpdate = false;
        public bool needsUpdate
        {
            get
            {
                return _needsUpdate;
            }
        }

        [SerializeField]
        private bool _needsUpdateDataInitialized = false;

        // update checking common data
        [SerializeField]
        private VRPrefabType _spawnedPrefabType;
        [SerializeField]
        private Vector3 _spawnedPosition;
        [SerializeField]
        private Quaternion _spawnedRotation;
        [SerializeField]
        private Vector3 _spawnedLocalScale;

        //  update checking custom data shit.......
        [SerializeField]
        private int _spawnedToiletId;
        [SerializeField]
        private bool _spawnedSlalomPoleIsRight;

        /// <summary>
        /// Updates data where possible. returns true when it did update successfully.
        /// </summary>
        /// <returns>returns true when successful, false otherwise</returns>
        public bool UpdateSpawnData()
        {
            bool success = false;

            switch (_spawnSourceType)
            {
            case SpawnSourceType.None:
                // easiest one
                break;
            case SpawnSourceType.Procedural:
                // nothing to do lol
                if (!debugConsiderThisAntiSpammer)
                {
                    debugConsiderThisAntiSpammer = true;
                    Debug.LogWarning("[EditorSpawnInfo] Cannot update spawn data of a procedural object. Consider using a mask noise layer and a PremadeChunks solution", gameObject);
                }
                break;
            case SpawnSourceType.Chunks:
                // find this object in the scb's dictionary/array of lists, and update its data.
                if (sourceChunk != null && sourceChunkObject != null)
                {
                    success = sourceChunk.UpdateObjectData(sourceChunkObject, gameObject);
                    // do not return here
                }
                break;
            case SpawnSourceType.BigObject:
                if (sourceBigObject != null)
                {
                    success = sourceBigObject.UpdateBigObject(transform);
                    // do not return here
                }
                break;
            default:
                break;
            }

            InitData();

            return success;
        }

        private void InitData()
        {
            _needsUpdateDataInitialized = true;
            _needsUpdate = false;
            _spawnedPrefabType = vrPrefab.prefabType;
            _spawnedPosition = transform.position;
            _spawnedRotation = transform.rotation;
            _spawnedLocalScale = transform.localScale;

            if (sourceChunkObject != null && sourceChunkObject is ChunkObjectToilet)
            {
                var tco = sourceChunkObject as ChunkObjectToilet;
                _spawnedToiletId = tco.toiletId;
            }

            if (sourceChunkObject != null && sourceChunkObject is ChunkObjectSlalomPole)
            {
                var sp = sourceChunkObject as ChunkObjectSlalomPole;
                _spawnedSlalomPoleIsRight = sp.isRight;
            }

        }

        private void Update()
        {
            if (_needsUpdateDataInitialized)
            {
                // only check positions and data if the object doesn't already need an update
                if (!_needsUpdate)
                {
                    switch (_spawnSourceType)
                    {
                    case SpawnSourceType.Procedural:
                        if (_spawnedPosition != transform.position
                            || _spawnedRotation != transform.rotation
                            || _spawnedLocalScale != transform.localScale
                            || _spawnedPrefabType != vrPrefab.prefabType
                            // || or if custom data has changed. such as slalomPole.isRight or toilet ID. this can maybe be done in a smart way. for now not so much.
                            )
                        {
                            _needsUpdate = true;
                        }
                        break;
                    case SpawnSourceType.Chunks:
                        // find this object in the scb's dictionary/array of lists, and update its data.
                        if (_spawnedPosition != transform.position
                            || _spawnedRotation != transform.rotation
                            || _spawnedLocalScale != transform.localScale
                            || _spawnedPrefabType != vrPrefab.prefabType
                            // || or if custom data has changed. such as slalomPole.isRight or toilet ID. this can maybe be done in a smart way. for now not so much.
                            )
                        {
                            _needsUpdate = true;
                        }

                        // check custom data
                        if (sourceChunkObject != null && sourceChunkObject is ChunkObjectToilet)
                        {
                            var tco = sourceChunkObject as ChunkObjectToilet;
                            if (tco.toiletId != _spawnedToiletId)
                            {
                                _needsUpdate = true;
                            }
                        }

                        if (sourceChunkObject != null && sourceChunkObject is ChunkObjectSlalomPole)
                        {
                            var sp = sourceChunkObject as ChunkObjectSlalomPole;
                            if (sp.isRight != _spawnedSlalomPoleIsRight)
                            {
                                _needsUpdate = true;
                            }
                        }
                        break;
                    case SpawnSourceType.BigObject:
                        if (_spawnedPosition != transform.position
                            || _spawnedRotation != transform.rotation
                            || _spawnedLocalScale != transform.localScale
                            || _spawnedPrefabType != vrPrefab.prefabType
                            )
                        {
                            _needsUpdate = true;
                        }
                        break;
                    }

                }

            }
            else
            {
                InitData();
            }

            debugConsiderThisAntiSpammer = false;

        }

        private void OnEnable()
        {
            if (!allSpawnInfo.Contains(this))
            {
                allSpawnInfo.Add(this);
            }
        }

        private void OnDisable()
        {
            allSpawnInfo.Remove(this);
        }

        public bool DeleteFromSpawner()
        {
            var succ = false;
            switch (_spawnSourceType)
            {
            case SpawnSourceType.Procedural:
                // cannot do it
                if (!debugConsiderThisAntiSpammer)
                {
                    debugConsiderThisAntiSpammer = true;
                    Debug.LogWarning("[EditorSpawnInfo] Cannot delete an object from a procedural source. Consider using a mask. Auto-selected a random mask for you.", FindObjectsOfType(typeof(FastNoiseChunkMask)).Random());
                }
                break;
            case SpawnSourceType.Chunks:
                if (sourceChunk != null && sourceChunkObject != null)
                {
                    succ = sourceChunk.DeleteFromSpawner(sourceChunkObject, gameObject);
                    // delete object here? no because we can just press 'Delete' - but otherwise it would be hard to get the object back heheh. unless we do insane "undo" shit. but beware! serialized chunk object data, etc. lots of shit to save.
                }
                break;
            case SpawnSourceType.BigObject:
                if (sourceBigObject != null)
                {
                    var parentBO = sourceBigObject.GetComponentInParent<BigObjects>();
                    if (parentBO != null)
                    {
                        parentBO.list.Remove(sourceBigObject); // problem with onvalidate before we actually destroy the big object?
                    }
                    this.DestroySafe(sourceBigObject.gameObject);
                    succ = true;
                }
                break;
            }

            return succ;

        }
    }

}