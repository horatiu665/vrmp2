namespace Mountain
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using Random = UnityEngine.Random;
    using VRCore;

    public class MountainFilter : MonoBehaviour, IMountainFilter
    {
        [SerializeField]
        private int _order;

        [NonSerialized, ReadOnly]
        private bool _isApplied;

        private DateTime _applyTime;

        public bool isApplied
        {
            get
            {
                return _isApplied;
            }

            set
            {
                _isApplied = value;
            }
        }

        public bool isSlowClearing { get; private set; }

        public int order
        {
            get
            {
                return _order;
            }

            set
            {
                _order = value;
            }
        }

        MountainManager _mountainManager;
        public MountainManager mountainManager
        {
            get
            {
                if (_mountainManager == null)
                {
                    if (transform.parent != null)
                    {
                        _mountainManager = transform.parent.GetComponentInParent<MountainManager>();
                    }
                }

                return _mountainManager;
            }
        }

        public int generationFrames { get; set; }

        public float generationTime { get; set; }

        public virtual void Reset()
        {
            if (name == "GameObject")
            {
                name = ((this is MountainManager) && mountainManager != null ? "Sub" : "") + GetType().Name.Replace("Filter", "");
            }
        }

        public virtual void GenerateAt(params Vector3[] extraPos)
        {
            if (!isApplied)
            {
                isApplied = true;
            }
            _applyTime = DateTime.Now;
        }

        public virtual void ClearAllInstant()
        {
            _applyTime = DateTime.Now;
        }

        public virtual void ClearSlow()
        {
            isSlowClearing = true;
            _applyTime = DateTime.Now;
        }

        public void DoneApplying()
        {
            var ms = DateTime.Now.Subtract(_applyTime).Milliseconds;
            generationTime = ms;
            generationFrames = 1;
        }

        public void DoneClearing()
        {
            var ms = DateTime.Now.Subtract(_applyTime).Milliseconds;
            generationTime = ms;
            generationFrames = 1;
            isSlowClearing = false;
            isApplied = false;
        }


    }
}