namespace Mountain.Filters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Random = UnityEngine.Random;

    public class ActivateNearPlayer : MountainFilter, IOriginShifter
    {
        private IMountainObjectSpawnFilter _filterWithDictionary;
        public IMountainObjectSpawnFilter filterWithDictionary
        {
            get
            {
                if (_filterWithDictionary == null)
                {
                    _filterWithDictionary = GetComponent<IMountainObjectSpawnFilter>();

                }
                return _filterWithDictionary;
            }
        }

        public override void Reset()
        {
            base.Reset();
            OnValidate();
        }

        private void OnValidate()
        {
            if (order <= filterWithDictionary.order)
            {
                Debug.LogWarning("[ForestSpawnActivateNearPlayer] Beware! This filter's order (" + order + ") is less than the filter it's activating"
                    + ". Automatically changed to order + 1 = " + _filterWithDictionary.order + 1 + "!", gameObject);
                order = _filterWithDictionary.order + 1;
            }
        }

        [SerializeField]
        private int _radius = 1;

        [SerializeField]
        private List<SpawnJobCoord> _activatedCoords = new List<SpawnJobCoord>();

        [SerializeField]
        private bool showGizmos;

        private Vector3 lastGenPos;

        private MountainMeshFilter _heightProvider;
        public MountainMeshFilter heightProvider
        {
            get
            {
                if (_heightProvider == null)
                {
                    _heightProvider = MountainHeightProviderSingleton.instance.heightMeshFilter;
                }
                return _heightProvider;
            }
        }

        public override void GenerateAt(params Vector3[] extraPos)
        {
            base.GenerateAt(extraPos);
            DoneApplying();
            return; // canceled because this does not work reliably. needs to be REMADE. but I don't wanna lose the settings in the MountainManager so I keep it for now ;)
            var worldPos = extraPos[0];
            var curCoord = filterWithDictionary.PosToCoord(worldPos);

            for (int i = _activatedCoords.Count - 1; i >= 0; i--)
            {
                var c = _activatedCoords[i];
                if (OutsideRadius(c, curCoord, _radius))
                {
                    DeactivateCoord(c);
                    _activatedCoords.RemoveAt(i);
                }
            }

            for (int x = -_radius; x <= _radius; x++)
            {
                for (int z = -_radius; z <= _radius; z++)
                {
                    var c = curCoord + new SpawnJobCoord(x, z);
                    if (!_activatedCoords.Contains(c))
                    {
                        ActivateCoord(c);
                        _activatedCoords.Add(c);
                    }
                }
            }

            //// enumerator instead of a list
            //var enu = _activatedCoords.GetEnumerator();
            //while (enu.MoveNext())
            //{
            //	var c = enu.Current;
            //	if (OutsideRadius(c, curCoord, _radius))
            //	{
            //		DeactivateCoord(c);
            //		_deactivatedCoords.Add(c);
            //	}
            //}
            //enu = _deactivatedCoords.GetEnumerator();
            //while (enu.MoveNext())
            //{
            //	_activatedCoords.Remove(enu.Current);
            //}
            //_deactivatedCoords.Clear();

            DoneApplying();

            lastGenPos = worldPos;
        }

        public override void ClearAllInstant()
        {
            base.ClearAllInstant();
            _activatedCoords.Clear();

            DoneClearing();
        }

        public override void ClearSlow()
        {
            base.ClearSlow();
            _activatedCoords.Clear();

            DoneClearing();
        }

        private void ActivateCoord(SpawnJobCoord c)
        {
            var spawnList = _filterWithDictionary.GetSpawnedObjectsList(c);
            if (spawnList != null)
            {
                for (int i = 0; i < spawnList.Count; i++)
                {
                    // activate each object of this list
                    var ac = spawnList[i].GetComponent<FunctionalityLodBase>();
                    if (ac != null)
                    {
                        ac.Activate();
                        if (showGizmos)
                        {
                            Debug.DrawRay(ac.transform.position + Vector3.right * 0.5f, Vector3.up * 1000, Color.green, 5f);
                        }
                    }

                }
            }
        }

        private void DeactivateCoord(SpawnJobCoord c)
        {
            var spawnList = _filterWithDictionary.GetSpawnedObjectsList(c);
            if (spawnList != null)
            {
                for (int j = 0; j < spawnList.Count; j++)
                {
                    // deactivate each object of this list
                    var ac = spawnList[j].GetComponent<FunctionalityLodBase>();
                    if (ac != null)
                    {
                        ac.Deactivate();
                        if (showGizmos)
                        {
                            Debug.DrawRay(ac.transform.position, Vector3.up * 1000, Color.red, 5f);
                        }
                    }
                }
            }
        }

        private bool OutsideRadius(SpawnJobCoord spawnJobCoord, SpawnJobCoord curCoord, int _radius)
        {
            return spawnJobCoord.x < curCoord.x - _radius || spawnJobCoord.x > curCoord.x + _radius ||
                    spawnJobCoord.y < curCoord.y - _radius || spawnJobCoord.y > curCoord.y + _radius;
        }

        private void OnDrawGizmos()
        {
            if (!showGizmos)
                return;

            // draw square around radius
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(Vector3.zero, filterWithDictionary.gridSize * (_radius * 2 + 1) * new Vector3(1, 0, 1));

            var enu = _activatedCoords.GetEnumerator();
            while (enu.MoveNext())
            {
                GizmoDrawCoord(enu.Current);
            }
        }

        void GizmoDrawCoord(SpawnJobCoord c)
        {
            var col = Color.yellow;
            col.a = 0.4f;
            Gizmos.color = col;

            var pos = filterWithDictionary.CoordToPos(c);
            if (heightProvider.isApplied)
            {
                pos.y = heightProvider.SampleHeight(this, pos);
            }
            Gizmos.DrawCube(pos, Vector3.one * filterWithDictionary.gridSize * 0.8f);
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
        }

        public void OnWorldMove(Vector3 originShiftDelta)
        {
            var os = SpawnJobCoord.PosToCoord(originShiftDelta, filterWithDictionary.gridSize);

            // origin shifted. we have to regenerate because otherwise we leave behind enabled assets
            // before OS:
            //		generate c[0,0]
            //		generate c[0,1]
            // after OS:
            //		generate c[os, os+1] ... corresponds to c[0,1]
            //		generate c[os, os+2] ... corresponds to c[0,2]
            //		we must un-generate c[os, os] because it corresponds to c[0,0]
            // therefore we do like the array mover in the forest job filter. force regenerate at lastpos - delta. tested and it works. (though it is confusing)
            if (this.isApplied && filterWithDictionary.isApplied)
            {

                // commented out until this shit is remade because it's broken AF
                //GenerateAt(lastGenPos - originShiftDelta);
                //GenerateAt(lastGenPos + originShiftDelta);
            }
        }
    }
}