using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Mountain.Filters
{
    public interface IMountainObjectSpawnFilter
    {
		// basic filter order. needed because activating spawned objects needs to happen after spawning
        int order { get; }
		int gridSize { get; }
		bool isApplied { get; }

        SpawnJobCoord PosToCoord(Vector3 position);
        Vector3 CoordToPos(SpawnJobCoord coords);

        List<GameObject> GetSpawnedObjectsList(SpawnJobCoord c);

    }
}
