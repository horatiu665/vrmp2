using Mountain.Filters;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;
using System;

public class MountainHeightProviderSingleton : MonoBehaviour
{
    private static MountainHeightProviderSingleton _instance;
    public static MountainHeightProviderSingleton instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<MountainHeightProviderSingleton>();
            }
            return _instance;
        }
    }


    [HelpBox("Put script on a MountainMeshFilter and it will be found by things to set height to mesh", HelpBoxType.None)]
    public bool helpText;

    public static float SampleHeight(Vector3 position)
    {
        // consider optimizing.
        if (instance == null || instance.heightMeshFilter == null || !instance.heightMeshFilter.isApplied)
        {
            return position.y;
        }
        return instance.heightMeshFilter.SampleHeight(instance, position);
    }

    private MountainMeshFilter _heightMeshFilter;
    public MountainMeshFilter heightMeshFilter
    {
        get
        {
            if (_heightMeshFilter == null)
            {
                _heightMeshFilter = GetComponent<MountainMeshFilter>();
            }
            return _heightMeshFilter;
        }
    }


}