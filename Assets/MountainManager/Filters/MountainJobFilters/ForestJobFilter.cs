namespace Mountain.Filters
{
	using Helpers;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using VRCore;

	public class ForestJobFilter : MountainJobFilterBase
	{
		// cont. [Header("Grid params")]
		[SerializeField]
		private int _subGridRowCount = 4;

		[Header("Spawn: Basic")]
		[SerializeField]
		private VRPrefabType[] _spawnPrefabs;

		[SerializeField]
		private float _spawnNoiseThreshold = 0.5f;

		[SerializeField]
		private FastNoiseShare treeNoise;

		[SerializeField]
		private MountainMeshFilter _heightProvider;
		public MountainMeshFilter heightProvider
		{
			get
			{
				if (_heightProvider == null)
				{
					_heightProvider = MountainHeightProviderSingleton.instance.heightMeshFilter;
				}
				return _heightProvider;
			}
		}

		[Header("Spawn: Rotations and scale")]
		[SerializeField]
		private float _rotateToNormal = 0f;

		[SerializeField]
		private float _randomRotation = 1f;

		[SerializeField]
		private FastNoisePiste pisteNoise;

		[SerializeField]
		private float _alignWithSlopeForward = 0;

		/// <summary>
		/// If != 1, sets uniform scale. Cannot multiply due to pooling - objects will exponentially scale after pooling 
		/// </summary>
		[Tooltip("If != 1, sets uniform scale. Cannot multiply due to pooling - objects will exponentially scale after pooling")]
		[SerializeField]
		private SmartRange2 _randomScale = new SmartRange2(1f);

		[Header("Sampling params")]
		[SerializeField]
		private float _sampleRandomOffset = 1f;

		[SerializeField]
		private FastNoise _sampleNoise = new FastNoise();

		public Vector3 originShift
		{
			get
			{
				return OriginShiftManager.originShift;
			}
		}

		[SerializeField, ReadOnly]
		private Transform _parent = null;

		public Transform parent
		{
			get
			{
				if (_parent == null)
				{
					_parent = new GameObject("JOBrest of " + _spawnPrefabs[0].ToString() + "s").transform;
					_parent.SetParent(mountainManager.generatedParent, true);
				}
				return _parent;
			}
		}

        // data for the deferred for loop (while) in the SpawnCoord non-coroutine
        private Vector3 spawnPosition, spawnStartPos;
        private int spawnX, spawnZ;

        protected override void SpawnInstantly(SpawnJobCoord coord)
		{
			// init spawn vars
			var position = CoordToPos(coord);
			var startPos = position - new Vector3(_gridEdgeSize * 0.5f, 0, _gridEdgeSize * 0.5f);
			float halfGridEdgeSize = this._gridEdgeSize / 2;

			for (int x = 0; x < _subGridRowCount; x++)
			{
				for (int z = 0; z < _subGridRowCount; z++)
				{
					EvaluateAndMaybeSpawnTree(x, z, coord, position, startPos, halfGridEdgeSize);
				}
			}
		}

        protected override void SpawnCoordPre(SpawnJobCoord coord)
        {
            base.SpawnCoordPre(coord);

            // init spawn vars
            spawnPosition = CoordToPos(coord);
            spawnStartPos = spawnPosition - new Vector3(_gridEdgeSize * 0.5f, 0, _gridEdgeSize * 0.5f);

            // init deferred for loop (while)
            spawnX = spawnZ = 0;
        }

        protected override bool SpawnCoord(SpawnJobCoord coord)
		{
			// do not start spawning until the trees per frame is zero again. happens when moving target around too fast
			while (!deferMan.CanContinue(this, spawnDeferTag))
			{
                return false;
			}

            if (CheckForCanceledPosition(coord))
            {
                CancelSpawner(coord);
                return true;
            }

            float halfGridEdgeSize = this._gridEdgeSize / 2;

            while (spawnX < _subGridRowCount)
            {
                while (spawnZ < _subGridRowCount)
                {
                    EvaluateAndMaybeSpawnTree(spawnX, spawnZ, coord, spawnPosition, spawnStartPos, halfGridEdgeSize);

                    if (ShouldPauseSpawning())
                    {
                        return false;
                    }

                    spawnZ++;
                }

                spawnX++;
                spawnZ = 0;
            }
            
			FinishSpawner(coord);
            return true;
		}

		#region EvaluateAndMaybeSpawnTree() and other spawning methods
		private void EvaluateAndMaybeSpawnTree(int x, int z, SpawnJobCoord coord, Vector3 position, Vector3 startPos, float halfGridEdgeSize)
		{
			var samplePos = startPos + new Vector3(_gridEdgeSize * x / _subGridRowCount, 0, _gridEdgeSize * z / _subGridRowCount);

            // get noise in global pos
			var sampleNoise = _sampleNoise.GetNoise(samplePos.x + originShift.x, samplePos.z + originShift.z);

			// add noise to sampled pos
			//samplePos += new Vector3(sampleNoise, 0, _sampleNoise.GetNoise(samplePos.x + originShift.x, samplePos.z + originShift.z, 1000)) * _sampleRandomOffset; // two get noise calls
			var noisepi2 = sampleNoise * Mathf.PI * 2;
			samplePos += new Vector3(Mathf.Sin(noisepi2), 0, Mathf.Cos(noisepi2)) * _sampleRandomOffset;

			if (!IsSampleInsideGrid(samplePos, position, halfGridEdgeSize))
			{
				return;
			}

            // get noise in global pos
			var noiseValue = treeNoise.GetNoise(samplePos.x + originShift.x, samplePos.z + originShift.z);
			if (noiseValue < _spawnNoiseThreshold)
			{
				return;
			}

			Quaternion rot = RotationAndHeight(ref samplePos, sampleNoise);

			var i = Mathf.FloorToInt((sampleNoise + 1f) * _spawnPrefabs.Length) % _spawnPrefabs.Length;
			var r = SpawnTree(_spawnPrefabs[i], coord, samplePos, rot);

			var scale = _randomScale.Value(sampleNoise);
			if (scale != 1)
			{
				r.transform.localScale = Vector3.one * scale;
			}

#if UNITY_EDITOR
            EditorSpawnInfoManager.instance.AddSpawnInfoProcedural(r, this);
#endif

            deferMan.CountOperation(this, spawnDeferTag);
		}

		private bool ShouldPauseSpawning()
		{
			return !deferMan.CanContinue(this, spawnDeferTag);
		}

		private Quaternion RotationAndHeight(ref Vector3 samplePos, float sampleNoise)
		{
			Vector3 upVector = Vector3.up;
			Vector3 normalFwd = Vector3.forward;
			Vector3 randomFwd = Vector3.forward;
			Vector3 slopeAlignedFwd = Vector3.forward;
			bool zero = true;

			// decide rotation
			if (_rotateToNormal > 0)
			{
				zero = false;
				samplePos.y = heightProvider.SampleHeight(this, samplePos, ref upVector);
				if (upVector != Vector3.up)
				{
					var normalRight = Vector3.Cross(upVector, Vector3.up).normalized;
					normalFwd = Vector3.Cross(upVector, normalRight);
				}
			}
			else
			{
				// use height provider for height
				samplePos.y = heightProvider.SampleHeight(this, samplePos);
			}

			if (_randomRotation > 0)
			{
				zero = false;
				var randomAngle = sampleNoise * 360f;
				randomFwd = new Vector3(Mathf.Sin(randomAngle), 0, Mathf.Cos(randomAngle));
			}

			if (_alignWithSlopeForward > 0)
			{
				zero = false;
                // get noise in global pos
				slopeAlignedFwd = pisteNoise.GetPisteForward(samplePos.x + originShift.x, samplePos.z + originShift.z);
				var rightVector = Vector3.Cross(slopeAlignedFwd, upVector);
				slopeAlignedFwd = Vector3.Cross(rightVector, upVector);
			}

			if (zero)
			{
				return Quaternion.identity;
			}

			var fwVector = (normalFwd * _rotateToNormal + slopeAlignedFwd * _alignWithSlopeForward + randomFwd * _randomRotation)
				/ (_rotateToNormal + _alignWithSlopeForward + _randomRotation);

			Quaternion rot = Quaternion.LookRotation(fwVector, upVector);
			return rot;
		}

		private IVRPrefab SpawnTree(VRPrefabType prefabToSpawn, SpawnJobCoord coord, Vector3 position, Quaternion rotation)
		{
			var tree = FoggyForestPoolManager.instance.Spawn(prefabToSpawn, position, rotation);
			tree.transform.SetParent(parent);

            GetSpawnedObjectsList(coord).Add(tree.gameObject);
			_spawnedCount++;

			return tree;
		}
		#endregion

		protected override bool DeleteAllFromCoord(SpawnJobCoord coord)
		{
			var n = GetSpawnedObjectsList(coord).Count;
			for (int i = n - 1; i >= 0; i--)
			{
				var s = GetSpawnedObjectsList(coord)[i];
				FoggyForestPoolManager.instance.Return(s.GetComponent<IVRPrefab>());
				_spawnedCount--;
				GetSpawnedObjectsList(coord).RemoveAt(i);

				deferMan.CountOperation(this, deleteDeferTag);

				if (!deferMan.CanContinue(this, deleteDeferTag))
				{
					return false;
				}
			}

			return true;
		}
	}
}
