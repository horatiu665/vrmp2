namespace Mountain.Filters
{
    using Helpers;
    //using MovementEffects;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using System;

    // Handles all job array issues. Can be extended with functionality for what happens inside each job.
    public abstract class MountainJobFilterBase : MountainFilter, IOriginShifter, IMountainObjectSpawnFilter, IDeferExecutions
    {
        [SerializeField]
        private bool showGizmos = true;

        [SerializeField]
        private bool forceSpawnInstantly = false;

        [Header("Grid params")]
        [SerializeField]
        protected int _radius = 4;

        [SerializeField]
        protected int _maskRadius = 0;

        [Header("Grid size must be % with originShift portalSize")]
        [SerializeField]
        protected int _gridEdgeSize = 150;
        int _gridEdgeSizeValidator;

        private SpawnJobCoord minCoordEver, maxCoordEver;
        
        private const string spawnCoordTag = "ApplyGridPiece";

        protected int runningSpawnersCount = 0;

        protected DeferredExecutionManager deferMan

        {
            get
            {
                return DeferredExecutionManager.instance;
            }
        }

        public DeferredExecutionTags spawnDeferTag
        {
            get
            {
                return DeferredExecutionTags.SpawnJob;
            }
        }

        public DeferredExecutionTags deleteDeferTag
        {
            get
            {
                return DeferredExecutionTags.DeleteJob;
            }
        }

        SpawnJobCoord lastRunningSpawner;

        // debug attributesss heh
        [SerializeField, ReadOnly]
        List<SpawnJobCoord> currentCoordsList = new List<SpawnJobCoord>(4);

        #region Jobs[,] array and spawnedObjects[,] handling
        private enum JobType
        {
            None = 0,
            Spawn, // in the process of spawning.
            SpawnCanceled, // canceled in the midst of spawning. should not be touched cause it's still spawning. will be deleted when spawning is finished.
            Finished, // freshly spawned
            FinishedButDeletable, // can be turned into Finished to avoid deleting and it's like it was just freshly spawned
            Deleting, // in the process of deleting.
        }

        // MUST be called before operations where the coords are likely to be out of bounds
        private void InitArrays(SpawnJobCoord realCoord)
        {
            var deltadelta = new SpawnJobCoord(0, 0);
            if (ArrayBoundsInit(realCoord, ref deltadelta))
            {
                var newJobs = new JobType[_arrayMaxSize.x, _arrayMaxSize.y];

                for (int i = 0; i < _jobs.GetLength(0); i++)
                {
                    for (int j = 0; j < _jobs.GetLength(1); j++)
                    {
                        // move jobs[i,j] to newjobs[i+deltax, j+deltay]
                        newJobs[i + deltadelta.x, j + deltadelta.y] = _jobs[i, j];
                    }
                }
                _jobs = newJobs;

                var newDict = _spawnedObjects;
                newDict = new List<GameObject>[_arrayMaxSize.x, _arrayMaxSize.y];
                for (int i = 0; i < _spawnedObjects.GetLength(0); i++)
                {
                    for (int j = 0; j < _spawnedObjects.GetLength(1); j++)
                    {
                        // move jobs[i,j] to newjobs[i+deltax, j+deltay]
                        newDict[i + deltadelta.x, j + deltadelta.y] = _spawnedObjects[i, j];
                    }
                }

                _spawnedObjects = newDict;

            }
        }

        // only used in InitArrays()
        private bool ArrayBoundsInit(SpawnJobCoord realCoord, ref SpawnJobCoord deltadelta)
        {
            bool changed = false;

            if (realCoord.x + _arrayIndexOffset.x < 0)
            {
                // new delta (-x) minus old delta (arrayIndexOffset.x)
                deltadelta.x = -realCoord.x - _arrayIndexOffset.x;
                _arrayIndexOffset.x = -realCoord.x;
                _arrayMaxSize.x += deltadelta.x;
                changed = true;
            }
            else if (realCoord.x + _arrayIndexOffset.x >= _jobs.GetLength(0))
            {
                _arrayMaxSize.x = realCoord.x + _arrayIndexOffset.x + 1;
                changed = true;
            }

            if (realCoord.y < -_arrayIndexOffset.y)
            {
                deltadelta.y = -realCoord.y - _arrayIndexOffset.y;
                _arrayIndexOffset.y = -realCoord.y;
                _arrayMaxSize.y += deltadelta.y;
                changed = true;
            }
            else if (realCoord.y + _arrayIndexOffset.y >= _jobs.GetLength(1))
            {
                _arrayMaxSize.y = realCoord.y + _arrayIndexOffset.y + 1;
                changed = true;
            }
            return changed;
        }

        private JobType JobsGet(SpawnJobCoord c)
        {
            var i = c + _arrayIndexOffset;
            return _jobs[i.x, i.y];
        }

        private void JobsSet(SpawnJobCoord c, JobType value)
        {
            var i = c + _arrayIndexOffset;
            _jobs[i.x, i.y] = value;
        }

        private void JobsRemove(SpawnJobCoord c)
        {
            JobsSet(c, JobType.None);
        }

        private void SpawnedTreesRemove(SpawnJobCoord c)
        {
            var i = c + _arrayIndexOffset;
            _spawnedObjects[i.x, i.y].Clear();
        }

        public List<GameObject> GetSpawnedObjectsList(SpawnJobCoord c)
        {
            var i = c + _arrayIndexOffset;
            if (_spawnedObjects[i.x, i.y] == null)
            {
                _spawnedObjects[i.x, i.y] = new List<GameObject>();
            }
            return _spawnedObjects[i.x, i.y];
        }

        SpawnJobCoord _arrayIndexOffset;
        SpawnJobCoord _arrayMaxSize = new SpawnJobCoord(0, 0);
        JobType[,] _jobs = new JobType[0, 0];

        // Used for counting _spawnedObjects rather than opening the list in the inspector which might cause lag for like 5000+ objects..
        [SerializeField, ReadOnly]
        protected int _spawnedCount;

        protected List<GameObject>[,] _spawnedObjects = new List<GameObject>[0, 0];

        private float nextCheckIfWeShouldDeleteAnything;

        #endregion

        public override void GenerateAt(params Vector3[] extraPos)
        {
            base.GenerateAt(extraPos);

            currentCoordsList.Clear();

            // find the two extremes...? for all the damn extra positions
            var curCoords = PosToCoord(extraPos[0]);
            SpawnJobCoord minC = curCoords, maxC = curCoords;
            for (int i = 0; i < extraPos.Length; i++)
            {
                var e = PosToCoord(extraPos[i]);
                currentCoordsList.Add(e);
                if (minC.x > e.x)
                {
                    minC.x = e.x;
                }
                if (minC.y > e.y)
                {
                    minC.y = e.y;
                }
                if (maxC.x < e.x)
                {
                    maxC.x = e.x;
                }
                if (maxC.y < e.y)
                {
                    maxC.y = e.y;
                }
            }

            minC += new SpawnJobCoord(-_radius, -_radius);
            maxC += new SpawnJobCoord(_radius, _radius);

            // init arrays for the two extremes
            InitArrays(minC);
            InitArrays(maxC);
            
            for (int i = 0; i < extraPos.Length; i++)
            {
                var e = PosToCoord(extraPos[i]);
                GenerateAt_SpawnForCoords(e, minC, maxC);
            }

            SetMinMaxCoords();

            DoneApplying();

        }

        private void GenerateAt_SpawnForCoords(SpawnJobCoord coords, SpawnJobCoord minC, SpawnJobCoord maxC)
        {
            for (int x = minC.x; x <= maxC.x; x++)
            {
                for (int z = minC.y; z <= maxC.y; z++)
                {
                    var j = new SpawnJobCoord(x, z);
                    if (IsWithinRadiusAndNotMaskRadius(coords, j))
                    {
                        GenerateAt_MakeSpawningOrFinished(j);
                    }
                }
            }
        }

        private void GenerateAt_MakeSpawningOrFinished(SpawnJobCoord coordToTestForSpawn)
        {
            var jobType = JobsGet(coordToTestForSpawn);
            if (jobType == JobType.None)
            {
                MakeSpawningJob(coordToTestForSpawn);
            }
            else if (jobType == JobType.FinishedButDeletable)
            {
                JobsSet(coordToTestForSpawn, JobType.Finished);
            }

            // min max coords
            #region min max coords
            if (coordToTestForSpawn.x < minCoordEver.x)
            {
                minCoordEver.x = coordToTestForSpawn.x;
            }
            if (coordToTestForSpawn.x > maxCoordEver.x)
            {
                maxCoordEver.x = coordToTestForSpawn.x;
            }
            if (coordToTestForSpawn.y < minCoordEver.y)
            {
                minCoordEver.y = coordToTestForSpawn.y;
            }
            if (coordToTestForSpawn.y > maxCoordEver.y)
            {
                maxCoordEver.y = coordToTestForSpawn.y;
            }
            #endregion
        }

        public override void ClearAllInstant()
        {
            base.ClearAllInstant();

            // stop all spawners
            for (int i = 0; i < _jobs.GetLength(0); i++)
            {
                for (int j = 0; j < _jobs.GetLength(1); j++)
                {
                    _jobs[i, j] = JobType.None;
                }
            }
            // stop all spawning non-coroutines (done by changing type of job to none)
            //Timing.KillCoroutines(spawnCoordTag);

            for (int i = 0; i < _spawnedObjects.GetLength(0); i++)
            {
                for (int j = 0; j < _spawnedObjects.GetLength(1); j++)
                {
                    if (_spawnedObjects[i, j] != null)
                    {
                        for (int k = _spawnedObjects[i, j].Count - 1; k >= 0; k--)
                        {
                            this.DestroySafe(_spawnedObjects[i, j][k]);
                        }
                        _spawnedObjects[i, j].Clear();
                    }
                }
            }

            minCoordEver = maxCoordEver = new SpawnJobCoord(0, 0);

            _spawnedCount = 0;

            DoneClearing();
        }

        public override void ClearSlow()
        {
            base.ClearSlow();

            // stop all spawners
            for (int i = 0; i < _jobs.GetLength(0); i++)
            {
                for (int j = 0; j < _jobs.GetLength(1); j++)
                {
                    _jobs[i, j] = JobType.Deleting;
                }
            }
            //Timing.KillCoroutines(spawnCoordTag);

            // all the jobs are gonna be taken care of now by the update.
        }

        #region Job handling

        #region Spawning jobs
        // Makes spawning job and calls StartSpawning, for the kids to actually implement
        private void MakeSpawningJob(SpawnJobCoord coord)
        {
            if (!Application.isPlaying || forceSpawnInstantly)
            {
                // SpawnInstantly() avoids coroutines but is 100% equivalent to SpawnCoord() at end of execution
                SpawnInstantly(coord);
                JobsSet(coord, JobType.Finished);
            }
            else
            {
                // make a spawning job
                JobsSet(coord, JobType.Spawn);

                //SpawnCoordPre(coord);
                //Timing.RunCoroutine(SpawnCoord(coord), spawnCoordTag);
            }
        }

        private void ExecuteSpawningJobsNonCoroutine()
        {
            for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
            {
                for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                {
                    var j = new SpawnJobCoord(x, y);

                    var job = JobsGet(j);
                    if (job == JobType.Spawn)
                    {
                        if (j != lastRunningSpawner)
                        {
                            // run the initializer for this coord because it's a new one.
                            SpawnCoordPre(j);
                            lastRunningSpawner = j;
                        }
                        if (!SpawnCoord(j))
                        {
                            return;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Override with an editor version of the spawn method. DO NOT USE JOBS, COROUTINES. THIS MUST WORK IN THE EDITOR
        /// </summary>
        /// <param name="coord"></param>
        protected abstract void SpawnInstantly(SpawnJobCoord coord);

        // Happens before SpawnCoord(coord), as a method to override coroutines.
        protected virtual void SpawnCoordPre(SpawnJobCoord coord)
        {

        }

        /// <summary> 
        /// Override this with sampling and spawn methods.
        /// Remember to use FinishSpawner(coord) when finished, and
        ///     use CheckForCanceledPosition(coord) and CancelSpawner(coord) when you want to cancel the job.
        /// 
        /// For base class overriding, there is  void SpawnCoordPre(coord).
        /// this was made with coroutines first. but still makes sense, cause we need to keep track of the start of the spawn coord call, what with origin shifting etc
        /// </summary>
        /// <param name="coord">where to execute spawning</param>
        /// <returns>Returns false when the spawner should wait a frame, and true when the spawner is complete and we can continue without waiting.</returns>
        protected abstract bool SpawnCoord(SpawnJobCoord coord);

        // returns true when a running spawner has been marked for stopping
        protected bool CheckForCanceledPosition(SpawnJobCoord coord)
        {
            return JobsGet(coord) == JobType.SpawnCanceled;
        }

        // finish a spawner correctly. it is here to stay until someone wants to delete it.
        protected void FinishSpawner(SpawnJobCoord coord)
        {
            JobsSet(coord, JobType.Finished);
            runningSpawnersCount--;
        }

        // Cancel spawner - even if it spawned anything, it will be deleted
        protected void CancelSpawner(SpawnJobCoord coord)
        {
            JobsSet(coord, JobType.Deleting);
            runningSpawnersCount--;
        }

        #endregion

        #region Canceling and deleting jobs

        private void CheckIfWeShouldDeleteAnythingRarely()
        {
            //while (true)
            //{
            if (Time.time >= nextCheckIfWeShouldDeleteAnything)
            {
                nextCheckIfWeShouldDeleteAnything = Time.time + 0.3f;
                //yield return Timing.WaitForSeconds(0.3f);
                CheckIfWeShouldDeleteAnything();

            }
            //}
        }

        // Checks jobs for being within range, and marks for deletion
        private void CheckIfWeShouldDeleteAnything()
        {
            // for all the other jobs left behind, mark for deletion.
            for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
            {
                for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                {
                    var j = new SpawnJobCoord(x, y);

                    bool withinSomeonesRadius = false;
                    
                    for (int i = 0; i < currentCoordsList.Count; i++)
                    {
                        if (IsWithinRadiusAndNotMaskRadius(currentCoordsList[i], j))
                        {
                            withinSomeonesRadius = true;
                            break;
                        }
                    }
                    
                    if (!withinSomeonesRadius)
                    {
                        CheckIfWeShouldDeleteAnything_ReadyToDelete(j);
                    }
                }
            }
        }

        private void CheckIfWeShouldDeleteAnything_ReadyToDelete(SpawnJobCoord j)
        {
            var job = JobsGet(j);
            if (job == JobType.Spawn)
            {
                JobsSet(j, JobType.SpawnCanceled);
            }
            else if (job == JobType.Finished)
            {
                JobsSet(j, JobType.FinishedButDeletable);
            }
        }

        // called every frame, iterates through jobs and deletes objects inside deletable spawners.
        private void StopSpawnersNonCoroutine()
        {
            //int minDeleteX = maxCoordEver.x, minDeleteY = maxCoordEver.y, maxDeleteX = minCoordEver.x, maxDeleteY = minCoordEver.y;
            for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
            {
                for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                {
                    var j = new SpawnJobCoord(x, y);

                    var job = JobsGet(j);
                    if (job == JobType.Deleting)
                    {
                        if (!DeleteAllFromCoord(j))
                        {
                            return;
                        }

                        // we deleted all spawned things in this coord space without the need to return; so we can mark this as completely deleted.
                        JobsSet(j, JobType.None);
                    }
                    else if (job == JobType.FinishedButDeletable)
                    {
                        JobsSet(j, JobType.Deleting);
                        // go back one index and erase this little shit
                        if (y > minCoordEver.y)
                        {
                            y--;
                        }
                        else if (x > minCoordEver.x)
                        {
                            x--;
                            y = maxCoordEver.y;
                        }
                    }
                }
            }

            SetMinMaxCoords();
        }

        /// <summary>
        /// Called every frame on deletable jobs. Deletes objects. Instead of coroutine, we just return; when we want to stop deleting for a frame.
        /// Return true when it deletes all the objects in this coord, and false when it doesn't finish. 
        /// </summary>
        protected abstract bool DeleteAllFromCoord(SpawnJobCoord coord);

        #endregion

        #endregion

        #region Coords manipulation and checks
        public SpawnJobCoord PosToCoord(Vector3 position)
        {
            return SpawnJobCoord.PosToCoord(position, _gridEdgeSize);
        }

        public Vector3 CoordToPos(SpawnJobCoord coords)
        {
            return SpawnJobCoord.CoordToPos(coords, _gridEdgeSize);
        }

        int IMountainObjectSpawnFilter.gridSize
        {
            get
            {
                return this._gridEdgeSize;
            }
        }

        /// <summary>
        /// is (x,z) within radius of curCoords?
        /// </summary>
        /// <param name="curCoords">coords of the player/spawning area</param>
        /// <param name="x">x of the coord being checked for spawning</param>
        /// <param name="z">z of the coord being checked for spawning</param>
        /// <returns>true when (x,z) is inside square with center at curCoords and _radius radius.</returns>
        private bool WithinRadius(SpawnJobCoord curCoords, int x, int z)
        {
            return (x >= curCoords.x - _radius && x <= curCoords.x + _radius &&
                z >= curCoords.y - _radius && z <= curCoords.y + _radius);
        }

        /// <summary>
        /// is (x,z) within mask radius of curCoords?
        /// </summary>
        /// <param name="curCoords">coords of the player/spawning area</param>
        /// <param name="x">x of the coord being checked for spawning</param>
        /// <param name="z">z of the coord being checked for spawning</param>
        /// <returns>true when (x,z) is inside square with center at curCoords and _maskRadius radius.</returns>
        private bool WithinMaskRadius(SpawnJobCoord curCoords, int x, int z)
        {
            return (x > curCoords.x - _maskRadius && x < curCoords.x + _maskRadius &&
                z > curCoords.y - _maskRadius && z < curCoords.y + _maskRadius);
        }

        /// <summary>
        /// is (j) within radius but not mask radius from the curCoords centered square?
        /// </summary>
        /// <param name="j"></param>
        /// <returns></returns>
        private bool IsWithinRadiusAndNotMaskRadius(SpawnJobCoord curCoords, SpawnJobCoord j)
        {
            return !WithinMaskRadius(curCoords, j.x, j.y) && WithinRadius(curCoords, j.x, j.y);
        }

        /// <summary>
        /// long but simple - checks all edges of the min/max spawned dictionary, and reduces bounds if there are no trees in that range.
        /// </summary>
        private void SetMinMaxCoords()
        {
            // check if there is anything on the minCoordEver.y row.
            bool foundSomething = false;
            while (!foundSomething && minCoordEver.y <= maxCoordEver.y)
            {
                foundSomething = false;
                for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
                {
                    var coordMin = new SpawnJobCoord(x, minCoordEver.y);
                    if (FoundSomethingHere(coordMin))
                    {
                        foundSomething = true;
                        break;
                    }
                }
                // we did not find any trees on this row! don't search it as min. coord anymore.
                // and check again for the next row.
                if (!foundSomething)
                {
                    for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
                    {
                        var coordMin = new SpawnJobCoord(x, minCoordEver.y);
                        JobsRemove(coordMin);
                        SpawnedTreesRemove(coordMin);
                    }
                    minCoordEver.y++;
                }
            }

            // check if there is anything on the maxCoordEver.y row.
            foundSomething = false;
            while (!foundSomething && minCoordEver.y <= maxCoordEver.y)
            {
                foundSomething = false;
                for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
                {
                    var coordMin = new SpawnJobCoord(x, maxCoordEver.y);
                    if (FoundSomethingHere(coordMin))
                    {
                        foundSomething = true;
                        break;
                    }
                }
                if (!foundSomething)
                {
                    for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
                    {
                        var coordMin = new SpawnJobCoord(x, maxCoordEver.y);
                        JobsRemove(coordMin);
                        SpawnedTreesRemove(coordMin);
                    }
                    maxCoordEver.y--;
                }
            }


            // check if there is anything on the mincoordever.x row.
            foundSomething = false;
            while (!foundSomething && minCoordEver.x <= maxCoordEver.x)
            {
                foundSomething = false;
                for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                {
                    var coordMin = new SpawnJobCoord(minCoordEver.x, y);
                    if (FoundSomethingHere(coordMin))
                    {
                        foundSomething = true;
                        break;
                    }
                }

                if (!foundSomething)
                {
                    for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                    {
                        var coordMin = new SpawnJobCoord(minCoordEver.x, y);
                        JobsRemove(coordMin);
                        SpawnedTreesRemove(coordMin);
                    }
                    minCoordEver.x++;
                }
            }


            // check if there is anything on the mincoordever.x row.
            foundSomething = false;
            while (!foundSomething && minCoordEver.x <= maxCoordEver.x)
            {
                foundSomething = false;
                for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                {
                    var coordMin = new SpawnJobCoord(maxCoordEver.x, y);
                    if (FoundSomethingHere(coordMin))
                    {
                        foundSomething = true;
                        break;
                    }
                }
                if (!foundSomething)
                {
                    for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                    {
                        var coordMin = new SpawnJobCoord(maxCoordEver.x, y);
                        JobsRemove(coordMin);
                        SpawnedTreesRemove(coordMin);
                    }
                    maxCoordEver.x--;
                }
            }

        }

        /// <summary>
        /// true when job at coordMin is spawning or contains any spawned objects 
        /// </summary>
        /// <param name="coordMin"></param>
        /// <returns></returns>
        private bool FoundSomethingHere(SpawnJobCoord coordMin)
        {
            var job = JobsGet(coordMin);
            return (GetSpawnedObjectsList(coordMin).Count > 0)
                || (job == JobType.Spawn || job == JobType.SpawnCanceled);
        }

        protected bool IsSampleInsideGrid(Vector3 sample, Vector3 gridCenter, float halfGridEdgeSize)
        {
            return sample.x > gridCenter.x - halfGridEdgeSize
                && sample.x < gridCenter.x + halfGridEdgeSize
                && sample.z > gridCenter.z - halfGridEdgeSize
                && sample.z < gridCenter.z + halfGridEdgeSize;
        }

        #endregion

        protected virtual void Start()
        {
            InitArrays(minCoordEver);
            InitArrays(maxCoordEver);

        }

        protected virtual void OnEnable()
        {
            _spawnedCount = _spawnedObjects.Length;

            OriginShiftManager.OriginShiftersAdd(this);
            //Timing.RunCoroutine(CheckIfWeShouldDeleteAnythingRarely());

            deferMan.Register(this, spawnDeferTag);
            deferMan.Register(this, deleteDeferTag);
        }

        protected virtual void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);

            if (deferMan != null)
            {
                deferMan.Unregister(this, spawnDeferTag);
                deferMan.Unregister(this, deleteDeferTag);
            }
        }

        protected virtual void Update()
        {
            ExecuteSpawningJobsNonCoroutine();

            CheckIfWeShouldDeleteAnythingRarely();

            StopSpawnersNonCoroutine();

            if (isSlowClearing)
            {
                if (_spawnedCount == 0)
                {
                    // clear last deleting jobs
                    ClearAllInstant();
                    DoneClearing();
                }
            }
        }

        protected virtual void OnValidate()
        {
            if (OriginShiftManager.instance != null)
            {
                var ps = OriginShiftManager.instance.portalSize;
                if (_gridEdgeSize != _gridEdgeSizeValidator)
                {
                    var dir = _gridEdgeSize - _gridEdgeSizeValidator;
                    dir = dir / Mathf.Abs(dir);
                    while (ps.x % _gridEdgeSize != 0 && _gridEdgeSize > 10 && _gridEdgeSize < ps.x)
                    {
                        _gridEdgeSize += dir;
                    }
                    _gridEdgeSize = Mathf.Clamp(_gridEdgeSize, 10, (int)ps.x);
                    _gridEdgeSizeValidator = _gridEdgeSize;
                }
            }

        }

        #region Origin Shift stuff
        public virtual void OnWorldMove(Vector3 originShiftDelta)
        {
            // Cancel all running spawners. they will be restarted after we moved.
            if (_jobs.Length > 0)
            {
                for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
                {
                    for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                    {
                        var c = new SpawnJobCoord(x, y);
                        var j = JobsGet(c);
                        if (j == JobType.Spawn)
                        {
                            CancelSpawner(c);
                        }

                    }
                }
            }

            //Timing.KillCoroutines(spawnCoordTag);

            var coordDelta = PosToCoord(-originShiftDelta);
            //curCoords += coordDelta;
            minCoordEver += coordDelta;
            maxCoordEver += coordDelta;

            // move chunks by coordDelta.
            // and make space in the array if out of bounds
            InitArrays(minCoordEver);
            InitArrays(maxCoordEver);

            // now move all existing coord lists into their new slots
            MoveArrays(coordDelta);

            // now move all the damn spawned objects
            for (int x = 0; x < _spawnedObjects.GetLength(0); x++)
            {
                for (int y = 0; y < _spawnedObjects.GetLength(1); y++)
                {
                    var list = _spawnedObjects[x, y];
                    if (list != null)
                    {
                        for (int k = 0; k < list.Count; k++)
                        {
                            list[k].transform.position -= originShiftDelta;
                        }
                    }
                }
            }
        }

        private void MoveArrays(SpawnJobCoord coordDelta)
        {
            JobType[,] newjobs = new JobType[_jobs.GetLength(0), _jobs.GetLength(1)];
            var newDict = new List<GameObject>[_jobs.GetLength(0), _jobs.GetLength(1)];

            int sox = 0, soy = 0, eox = 0, eoy = 0;

            if (coordDelta.x > 0)
            {
                eox = coordDelta.x;
            }
            else
            {
                sox = -coordDelta.x;
            }

            if (coordDelta.y > 0)
            {
                eoy = coordDelta.y;
            }
            else
            {
                soy = -coordDelta.y;
            }

            for (int x = sox; x < _jobs.GetLength(0) - eox; x++)
            {
                for (int y = soy; y < _jobs.GetLength(1) - eoy; y++)
                {
                    newjobs[x + coordDelta.x, y + coordDelta.y] = _jobs[x, y];
                    newDict[x + coordDelta.x, y + coordDelta.y] = _spawnedObjects[x, y];
                }
            }

            _jobs = newjobs;
            _spawnedObjects = newDict;

        }
        #endregion

        #region grizmos shit
        private void OnDrawGizmos()
        {
            if (!showGizmos)
                return;

            // draw square around radius
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(Vector3.zero, _gridEdgeSize * (_radius * 2 + 1) * new Vector3(1, 0, 1));

            InitArrays(minCoordEver);
            InitArrays(maxCoordEver);
            for (int x = minCoordEver.x; x <= maxCoordEver.x; x++)
            {
                for (int y = minCoordEver.y; y <= maxCoordEver.y; y++)
                {
                    GizmoDrawCoord(new SpawnJobCoord(x, y));
                }
            }
        }

        void GizmoDrawCoord(SpawnJobCoord c)
        {
            switch (JobsGet(c))
            {
            case JobType.None:
                Gizmos.color = Color.white;
                break;
            case JobType.Spawn:
                Gizmos.color = Color.yellow;
                break;
            case JobType.SpawnCanceled:
                Gizmos.color = Color.red;
                break;
            case JobType.Finished:
                Gizmos.color = Color.green;
                break;
            case JobType.FinishedButDeletable:
                Gizmos.color = Color.cyan;
                break;
            case JobType.Deleting:
                Gizmos.color = Color.magenta;
                break;
            default:
                break;
            }
            var co = Gizmos.color;
            if (c.x < minCoordEver.x || c.x > maxCoordEver.x || c.y < minCoordEver.y || c.y > maxCoordEver.y)
            {
                co.a = 0.15f;
            }
            else
            {
                co.a = 0.5f;
            }
            Gizmos.color = co;
            Gizmos.DrawCube(CoordToPos(c), Vector3.one * _gridEdgeSize * 0.8f);
        }
        #endregion
    }
}
