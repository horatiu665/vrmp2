namespace Mountain.Filters
{
	using Helpers;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using VRCore;
	using System;

	public class PremadeObjectsJobFilter : MountainJobFilterBase
	{
		[Header("Spawn: Source")]
		/// <summary>
		/// If not null, gets all SpawnableChunkBase from children and uses as spawnSources
		/// </summary>
		[Tooltip("If not null, gets all SpawnableChunkBase from children and uses as spawnSources")]
		[SerializeField]
		private Transform spawnSourcesParent;

		/// <summary>
		/// Overwritten in case spawnSourcesParent is not null!!!
		/// </summary>
		[Tooltip("Overwritten in case spawnSourcesParent is not null!!!")]
		[SerializeField]
		private SpawnableChunkBase[] spawnSources;

		public Vector3 originShift
		{
			get
			{
				return OriginShiftManager.originShift;
			}
		}

		[SerializeField, ReadOnly]
		private Transform _parent = null;

		public Transform parent
		{
			get
			{
				if (_parent == null)
				{
					var name = "[Premade Spawned] " + spawnSourcesParent != null ? spawnSourcesParent.name : spawnSources[0].name;
					_parent = new GameObject(name).transform;
					_parent.SetParent(mountainManager.generatedParent, true);
				}
				return _parent;
			}
		}

        // data for the deferred for loop (while) in the SpawnCoord non-coroutine
        private int spawnSourcesIndex;
        private int spawnSourcesElementIndex;

        private void Awake()
        {
            // sanity check init spawn sources in case we forget to do at edit time. causes minor spike at startup but worth it
			InitSpawnSources();
        }

        protected override void OnValidate()
		{
			base.OnValidate();
			InitSpawnSources();

			// ensure grid size is the same
			for (int i = 0; i < spawnSources.Length; i++)
			{
				var gs = spawnSources[i].gridSize;
				if (gs != _gridEdgeSize)
				{
					Debug.LogError("[PremadeObjectsJobFilter] Error! Grid size of spawnable chunks (" + gs + ") cannot be different than on the spawner (" + _gridEdgeSize + ")!", spawnSources[i].gameObject);
				}
			}

		}

		public override void GenerateAt(params Vector3[] extraPos)
		{
			if (Application.isEditor && !Application.isPlaying)
			{
				// when not playing, do this every time in case something changed. Otherwise it is done in awake
				InitSpawnSources();
			}

			base.GenerateAt(extraPos);
		}

		private void InitSpawnSources()
		{
			if (spawnSourcesParent != null)
			{
				spawnSources = spawnSourcesParent.GetComponentsInChildren<SpawnableChunkBase>();
			}
		}

		protected override void SpawnInstantly(SpawnJobCoord coord)
		{
            spawnSourcesIndex = 0;
            spawnSourcesElementIndex = 0;
            while (spawnSourcesIndex < spawnSources.Length)
            {
				var spawnSource = spawnSources[spawnSourcesIndex];
				// spawn all objects from this list:
				var shiftedCoord = coord + SpawnJobCoord.PosToCoord(originShift, _gridEdgeSize);
                int spawnListIndex, spawnListCount;
                if (spawnSource.GetIsSpawnListNotNull(shiftedCoord, out spawnListIndex, out spawnListCount))
                {
                    while (spawnSourcesElementIndex < spawnListCount)
                    {
                        var spawnObj = spawnSource.GetSpawnListElement(spawnListIndex, spawnSourcesElementIndex);

                        var go = Spawn(spawnSource, spawnObj, coord);

                        spawnSourcesElementIndex++;
                    }
                }

                spawnSourcesIndex++;
                spawnSourcesElementIndex = 0;
            }
        }

        protected override void SpawnCoordPre(SpawnJobCoord coord)
        {
            base.SpawnCoordPre(coord);

            spawnSourcesIndex = 0;
            spawnSourcesElementIndex = 0;
        }

        protected override bool SpawnCoord(SpawnJobCoord coord)
		{
			// do not start spawning until the trees per frame is zero again. happens when moving target around too fast
			while (!deferMan.CanContinue(this, spawnDeferTag))
			{
                return false;
			}

            if (CheckForCanceledPosition(coord))
            {
                CancelSpawner(coord);
                return true;
            }

            // cache
            var originShift = this.originShift;

            while (spawnSourcesIndex < spawnSources.Length)
            {
                var spawnSource = spawnSources[spawnSourcesIndex];
                // spawn all objects from this list:
                var shiftedCoord = coord + SpawnJobCoord.PosToCoord(originShift, _gridEdgeSize);
                int spawnListIndex, spawnListCount;
                if (spawnSource.GetIsSpawnListNotNull(shiftedCoord, out spawnListIndex, out spawnListCount))
                {
                    while (spawnSourcesElementIndex < spawnListCount)
                    {
                        var spawnObj = spawnSource.GetSpawnListElement(spawnListIndex, spawnSourcesElementIndex);
                        Spawn(spawnSource, spawnObj, coord);
                        deferMan.CountOperation(this, spawnDeferTag);

                        spawnSourcesElementIndex++;
                    }

                    if (ShouldPauseSpawning())
                    {
                        return false;
                    }

                }

                spawnSourcesIndex++;
                spawnSourcesElementIndex = 0;
            }
            
			FinishSpawner(coord);
            return true;
		}

		private bool ShouldPauseSpawning()
		{
			return !deferMan.CanContinue(this, spawnDeferTag);
		}

		private GameObject Spawn(SpawnableChunkBase spawnSource, IChunkObject spawnObject, SpawnJobCoord coord)
		{
			var tree = spawnObject.Spawn(parent);

#if UNITY_EDITOR
            EditorSpawnInfoManager.instance.AddSpawnInfoChunks(tree, spawnSource, (ChunkObject)spawnObject);
#endif

            GetSpawnedObjectsList(coord).Add(tree.gameObject);
			_spawnedCount++;

			return tree.gameObject;
		}

		protected override bool DeleteAllFromCoord(SpawnJobCoord coord)
		{
			var n = GetSpawnedObjectsList(coord).Count;
			for (int i = n - 1; i >= 0; i--)
			{
				var s = GetSpawnedObjectsList(coord)[i];
				FoggyForestPoolManager.instance.Return(s.GetComponent<IVRPrefab>());
				_spawnedCount--;
				GetSpawnedObjectsList(coord).RemoveAt(i);
				deferMan.CountOperation(this, deleteDeferTag);

				if (!deferMan.CanContinue(this, deleteDeferTag))
				{
					return false;
				}
			}

			return true;
		}

	}
}
