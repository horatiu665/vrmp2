using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

public class FlodDisablePhysics : FunctionalityLodBase
{
	[SerializeField, ReadOnly]
	private Collider[] _cols = new Collider[0];
	
	public override void Deactivate()
	{
		base.Deactivate();
		for (int i = 0; i < _cols.Length; i++)
		{
			_cols[i].enabled = false;
		}
	}

	public override void Activate()
	{
		base.Activate();
		for (int i = 0; i < _cols.Length; i++)
		{
			_cols[i].enabled = true;
		}
	}

	private void Reset()
	{
		ResetColliders();
	}

	private void OnValidate()
	{
		ResetColliders();
	}

	private void ResetColliders()
	{
		_cols = GetComponentsInChildren<Collider>(true);
	}
}