using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;
using VRCore;
using System;
using Helpers.Editor;

namespace Mountain.Filters
{

    public class MountainMeshFilter : MountainFilter, IOriginShifter
    {
        // YEAH FUCK FLOATS. ints are probably even more optimized for some operations, as well as help for the dictionary precision
        // commented numbers are for an equilateral hex grid.
        // origin shift multiplier 2078.46
        public const int xConst = 1;// 0.8660254037844386f;
        public const int xConst2 = 2;// 1.732050807568877f;

        #region  // ===================== MESH PROPS ===================== //

        [SerializeField]
        private MeshLiveData _meshLiveData;
        private MeshLiveData meshLiveData
        {
            get
            {
                if (_meshLiveData == null || _meshLiveData.owner == null)
                {
                    _meshLiveData = new MeshLiveData(this);
                }
                return _meshLiveData;
            }
        }
        private List<MeshLiveData> extraMeshLiveData = new List<MeshLiveData>(16);

        #endregion

        #region   // ===================== RANDOM PROPS ===================== //

        [Header("Gen params")]
        public int runtimeRadiusCount = 6;
        public int editorRadiusCount = 18;
        public int radiusCount
        {
            get
            {
                if (Application.isPlaying)
                {
                    return runtimeRadiusCount;
                }
                else
                {
                    return editorRadiusCount;
                }
            }
        }

        public int gridSize = 150;

        public FastNoiseShare heightNoise;

        public bool showGizmos = true;

        [Header("Others")]

        /// <summary>
        /// Generates the heights in the dictionary, to be used with the HeightProvider. ALWAYS TRUE, DOESNOT DO ANYTHING, JUST SHOWS FEEDBACK THAT THIS HAPPENS!! ;)
        /// </summary>
        [Tooltip("Generates the heights in the dictionary, to be used with the HeightProvider")]
        [SerializeField, ReadOnly]
        private bool _generateHeights = true;

        /// <summary>
        /// Generates the mesh graphics. can be heavy and it should not be done on the server
        /// </summary>
        [Tooltip("Generates the mesh graphics. can be heavy and it should not be done on the server")]
        [SerializeField]
        private bool _generateGraphics = true;

        /// <summary>
        /// Generates the mesh collider, heaviest part to set the mesh collider in one frame.
        /// </summary>
        [Tooltip("Generates the mesh collider, heaviest part to set the mesh collider in one frame.")]
        [SerializeField]
        private bool _generateCollider = true;

        [Header("Dragndrop params")]

        public Material slopeMat;
        public PhysicMaterial physicMat;

        [Header("DEBUG")]

        [Tooltip("Red square shows the approx size of 1 cell - if it was a square hahah")]
        public bool showAreaDebug = false;

        public Transform debugTarget;

        public float debugDuration = 0.1f;

        /// <summary>
        /// Creates debug objects showing a lot of info and debug rays about each position sampled. BEWARE! DON'T USE ON 5000 trees unless you have a lot of time on your hands
        /// </summary>
        [Tooltip("Creates debug objects showing a lot of info and debug rays about each position sampled. BEWARE! DON'T USE ON 5000 trees unless you have a lot of time on your hands")]
        public bool debugSpheresForEachHeightSampled = false;

        //public Material meshMat;

        private List<Vector3> extraCurrentGeneratedPos = new List<Vector3>();
        private Dictionary<Vector2Int, float> heightData = new Dictionary<Vector2Int, float>(); // non serializable by trade

        [SerializeField]
        private Transform _meshesParent;
        public Transform meshesParent
        {
            get
            {
                if (_meshesParent == null)
                {
                    var name = "[MeshParent] " + (_generateGraphics ? "graphics" : "") + (_generateGraphics && _generateCollider ? " + " : "") + (_generateCollider ? "physics" : "");
                    _meshesParent = new GameObject(name).transform;
                    _meshesParent.SetParent(mountainManager.generatedParent, true);
                }
                return _meshesParent;
            }
        }


        public Vector3 originShift
        {
            get
            {
                return OriginShiftManager.originShift;
            }
        }

        #endregion

        #region // ===================== FUNKS ===================== //

        public override void GenerateAt(params Vector3[] extraPos)
        {
            heightData.Clear();
            //Debug.Log("[MountainMeshFilter][GenerateAt] Cleared heightData", gameObject);

            base.GenerateAt(extraPos);
            
            for (int i = 0; i < extraPos.Length; i++)
            {
                MeshLiveData e;
                if (extraMeshLiveData.Count <= i)
                {
                    e = new MeshLiveData(this);
                    //Debug.Log("GenerateAt created " + e.owner, e.owner);
                    extraMeshLiveData.Add(e);
                }
                e = extraMeshLiveData[i];

                Generate(extraMeshLiveData[i], extraPos[i]);
            }

            extraCurrentGeneratedPos = extraPos.ToList();

            DoneApplying();
            //Debug.Log("[MountainMeshFilter][GenerateAt] Done Generating", gameObject);

        }

        public override void ClearAllInstant()
        {
            base.ClearAllInstant();
            ClearMeshAndVertices();

            DoneClearing();
        }

        public override void ClearSlow()
        {
            base.ClearSlow();
            ClearMeshAndVertices(); // can't make it slower...?

            DoneClearing();
        }

        public override void Reset()
        {
            base.Reset();
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            foreach (var e in extraMeshLiveData)
            {
                e.UnityDestroyMesh();
            }
            OriginShiftManager.OriginShiftersRemove(this);
        }

        private void OnDestroy()
        {
            foreach (var e in extraMeshLiveData)
            {
                e.UnityDestroyMesh();
            }
        }

        // makes sure there is no funky serialized data that might break something
        private void Start()
        {
            ClearAllInstant();
        }

        private void Update()
        {
            if (debugTarget != null)
            {
                SampleHeight(this, debugTarget.position);
            }

        }

        public void OnWorldMove(Vector3 delta)
        {
            heightData.Clear();
            
            for (int i = 0; i < extraCurrentGeneratedPos.Count; i++)
            {
                extraCurrentGeneratedPos[i] -= delta;
                Generate(extraMeshLiveData[i], extraCurrentGeneratedPos[i]);
            }

            // THIS DOES NOT WORK because the heightData is not updated with the originshifted heights. we MUST regenerate the shit........... ;(((
            //// consider moving mesh GOs on world move, and moving them back to zero when generating. then we don't regenerate on world move and don't have to keep ref to all the positions spawned
            //for (int i = 0; i < extraMeshLiveData.Count; i++)
            //{
            //    extraMeshLiveData[i].meshGO.transform.position -= delta;
            //}

        }

        void Generate(MeshLiveData mld, Vector3 worldPos)
        {
            // initialize with grid, surrounding position
            mld.CreateVertices(worldPos);

            mld.GenerateMesh();

        }

        #region public float SampleHeight(object whoWannaKnow)
        public float SampleHeight(object whoWannaKnow, Vector3 pos)
        {
            Vector3 normal = Vector3.zero;
            return SampleHeight(whoWannaKnow, pos, ref normal);
        }

        /// <summary>
        /// samples height of any position(x,z) using the existing generated vertex data.
        /// Pass false to wantNormal if you don't want to calculate it. Or just use the other overload.
        /// </summary>
        public float SampleHeight(object whoWannaKnow, Vector3 pos, ref Vector3 normal, bool wantNormal = true)
        {
            var gridSizeSqrt3 = gridSize * xConst2;
            var gridSizeSqrt3div2 = gridSize * xConst;

            bool even = (pos.x % gridSizeSqrt3 == pos.x % gridSizeSqrt3div2) ^ (pos.x >= 0);

            var C = new Vector3(
                pos.x - pos.x % gridSizeSqrt3div2 - (pos.x < 0f ? gridSizeSqrt3div2 : 0) + (even ? gridSizeSqrt3div2 : 0),
                0,
                Mathf.RoundToInt(pos.z / gridSize) * gridSize
                );

            // then find two more points that belong to the triangle surrounding the target position.
            Vector3 A = Vector3.zero;
            Vector3 B = Vector3.zero;
            float minSum = float.MaxValue;
            var posy0 = new Vector3(pos.x, 0, pos.z);
            var magc = (posy0 - C).magnitude;
            // calculate magnitudes first
            Vector3[] corners = new Vector3[6];
            float[] mags = new float[6];
            for (int i = 0; i < 6; i++)
            {
                var a = GetCorner(gridSizeSqrt3div2, C, i);
                corners[i] = a;
                mags[i] = (posy0 - a).magnitude;
            }
            int minIndex = -1;
            // find minimum dist to adjacent triangle corners
            for (int i = 0; i < 6; i++)
            {
                var sum = (mags[i] + mags[(i + 1) % 6] + magc);
                if (sum < minSum)
                {
                    minIndex = i;
                    minSum = sum;
                }
            }

            A = corners[minIndex];
            B = corners[(minIndex + 1) % 6];

            // calculate heights
            var cxz = new V2d(C.x, C.z);
            var axz = new V2d(A.x, A.z);
            var bxz = new V2d(B.x, B.z);
            var coordC = new Vector2Int((int)C.x, (int)C.z);
            var coordA = new Vector2Int((int)A.x, (int)A.z);
            var coordB = new Vector2Int((int)B.x, (int)B.z);
            try
            {
                if (heightData.ContainsKey(coordC))
                {
                    C.y = heightData[coordC];
                    A.y = heightData[coordA];
                    B.y = heightData[coordB];
                }
                else
                {
                    Debug.LogError("Dictionary does not contain key for location " + coordC + " or another", whoWannaKnow as UnityEngine.Object);
                }
            }
            catch (Exception e)
            {
                Debug.LogError("Dictionary does not contain key for location " + coordA + " or " + coordB, whoWannaKnow as UnityEngine.Object);
                Debug.LogError(e);
                Debug.DrawRay(A, Vector3.up * 1000, Color.blue, 10f);
            }

            // calc barycentric coords. stolen from https://en.wikipedia.org/wiki/Barycentric_coordinate_system
            //var P = new V2d(pos.x, pos.z) - cxz;
            double detb = ((bxz.y - cxz.y) * (axz.x - cxz.x) + (cxz.x - bxz.x) * (axz.y - cxz.y));
            if (detb == 0)
            {
                print("ZEROZEROZEROZERO");
                Debug.DrawLine(C, A, Color.red, 3F);
                Debug.DrawLine(B, A, Color.magenta, 3F);
                Debug.DrawLine(C, B, Color.blue, 3F);

                Debug.DrawRay(C, Vector3.up * 30, Color.green, 3f);
                Debug.DrawRay(A, Vector3.up * 10, Color.yellow, 3f);
                Debug.DrawRay(B, Vector3.up * 20, Color.cyan, 3f);

                Debug.DrawRay(pos - Vector3.up * 500, Vector3.up * 1000, Color.red, 3f);

            }
            double b1 = ((bxz.y - cxz.y) * (pos.x - cxz.x) + (cxz.x - bxz.x) * (pos.z - cxz.y)) /
                     detb;
            double b2 = ((cxz.y - axz.y) * (pos.x - cxz.x) + (axz.x - cxz.x) * (pos.z - cxz.y)) /
                     detb;
            double b3 = 1.0 - b1 - b2;

            if (wantNormal)
            {
                normal = Vector3.Cross(A - C, B - C);
                if (Vector3.Dot(normal, Vector3.up) < 0)
                    normal *= -1;
            }

            var final = (float)(b1 * A.y + b2 * B.y + b3 * C.y);

            // draw some debugs for the triangle we just found?
            if (debugDuration > 0)
            {
                var targetPos = (float)b1 * A + (float)b2 * B + (float)b3 * C;
                Debug.DrawLine(C, A, Color.red, debugDuration);
                Debug.DrawLine(A, B, Color.red * 0.85f, debugDuration);
                Debug.DrawLine(B, C, Color.red * 0.7f, debugDuration);
                DrawDebugCross(C, 100f, even ? Color.yellow : Color.yellow * 0.7f, debugDuration);

                Debug.DrawRay(C, Vector3.up * 100, Color.white * 0.7f, debugDuration);
                Debug.DrawRay(A, Vector3.up * 100, Color.white * 0.7f, debugDuration);
                Debug.DrawRay(B, Vector3.up * 100, Color.white * 0.7f, debugDuration);

                Debug.DrawLine(pos, targetPos, Color.green, debugDuration);
            }

            if (debugSpheresForEachHeightSampled)
            {
                var debugParent = GameObject.Find("[DebugSphereParent]");
                if (debugParent == null)
                {
                    debugParent = new GameObject("[DebugSphereParent]");
                }
                var dg = new GameObject("[DebugSph] C: " + C
                    + " A" + minIndex + ": " + A
                    + " B" + ((minIndex + 1) % 6) + ": " + B
                    , typeof(TransformGizmoVisualizer), typeof(DebugHeightSampler)).transform;
                dg.transform.position = new Vector3(pos.x, final, pos.z);
                dg.transform.SetParent(debugParent.transform);
                var hs = dg.GetComponent<DebugHeightSampler>();
                hs.A = A;
                hs.B = B;
                hs.C = C;
            }

            return final;
        }

        private Vector3 GetCorner(int gridSizeSqrt3div2, Vector3 C, int i)
        {
            float x, z;
            switch (i)
            {
            case 0:
            case 3:
                x = C.x;
                break;
            case 1:
            case 2:
                x = C.x + gridSizeSqrt3div2;
                break;
            case 4:
            case 5:
                x = C.x - gridSizeSqrt3div2;
                break;
            default:
                x = 0;
                break;
            }

            switch (i)
            {
            case 0:
                z = C.z - gridSize;
                break;
            case 1:
            case 5:
                z = C.z - gridSize / 2;
                break;
            case 2:
            case 4:
                z = C.z + gridSize / 2;
                break;
            case 3:
                z = C.z + gridSize;
                break;
            default:
                z = 0;
                break;
            }

            return new Vector3(x, 0, z);
        }

        private bool Colinear(int minIndex, int secondMinIndex)
        {
            return minIndex >= 0 && secondMinIndex >= 0 && Mathf.Abs(minIndex - secondMinIndex) == 3;
            //return
            //	(minIndex == 0 && secondMinIndex == 3) ||
            //	(minIndex == 1 && secondMinIndex == 4) ||
            //	(minIndex == 2 && secondMinIndex == 5) ||
            //	(minIndex == 3 && secondMinIndex == 0) ||
            //	(minIndex == 4 && secondMinIndex == 1) ||
            //	(minIndex == 5 && secondMinIndex == 2);
        }

        private static void DrawDebugCross(Vector3 crossPos, float crossSize, Color crossCol, float debugDuration)
        {
            Debug.DrawRay(crossPos - new Vector3(crossSize, 0, crossSize) / 2, new Vector3(crossSize, 0, crossSize), crossCol, debugDuration);
            Debug.DrawRay(crossPos - new Vector3(-crossSize, 0, crossSize) / 2, new Vector3(-crossSize, 0, crossSize), crossCol, debugDuration);
        }
        #endregion

        private float GetHeight(float x, float z)
        {
            return heightNoise.GetNoise(x, z);
        }



        #endregion

        #region  // ===================== MESH FUNKS ===================== //

        [DebugButton]
        void ClearMeshAndVertices()
        {
            if (meshLiveData != null)
            {
                meshLiveData.ClearMeshAndVertices();
            }
            foreach (var e in extraMeshLiveData)
            {
                if (e != null)
                {
                    e.ClearMeshAndVertices();
                }
            }
            heightData.Clear();
        }

        [Serializable]
        private class MeshLiveData
        {
            public MeshLiveData(MountainMeshFilter owner)
            {
                this.owner = owner;
                this.slopeMat = owner.slopeMat;
                this.physicMaterial = owner.physicMat;
                this.generateCollider = owner._generateCollider;
                this.generateGraphics = owner._generateGraphics;
                this.layer = owner.gameObject.layer;
            }

            // references
            public MountainMeshFilter owner; // ref to this

            [SerializeField]
            private MountainManager mountainManager
            {
                get
                {
                    return owner.mountainManager;
                }
            }

            [SerializeField]
            private PhysicMaterial physicMaterial; // ref

            [SerializeField]
            private Material slopeMat; // ref

            [SerializeField]
            private LayerMask layer;

            [SerializeField]
            private bool generateGraphics, generateCollider; // ref


            // generated
            [SerializeField]
            private GameObject _meshGO;
            public GameObject meshGO
            {
                get
                {
                    if (_meshGO == null)
                    {
                        _meshGO = new GameObject("[Generated] " + (generateGraphics ? "graphics" : "") + (generateGraphics && generateCollider ? " + " : "") + (generateCollider ? "physics" : ""));
                        _meshGO.transform.SetParent(owner.meshesParent);
                        _meshGO.layer = layer;
#if UNITY_EDITOR
                        if (layer == LayerMask.NameToLayer("Default"))
                        {
                            Debug.LogError("[MountainMeshFilter] The layer of the mountain is Default! You might wanna change it to ground BECAUSE THAT'S WHY YOU'RE GOING THROUGH IT!!!");
                        }
#endif
                    }
                    return _meshGO;
                }
            }

            // mg.mesh filter
            [SerializeField]
            private MeshFilter mf;           // gen

            // mg.mesh renderer
            [SerializeField]
            private MeshRenderer mr;         // gen

            // mg.mesh collider
            [SerializeField]
            private MeshCollider mc;         // gen

            // mf.sharedmesh
            private Mesh mesh;               // gen

            private Vector3[] vertices;      // gen
            private int[] triangles;         // gen


            public void CreateVertices(Vector3 worldPos)
            {
                var originShift = owner.originShift;
                var radiusCount = owner.radiusCount;
                var gridSize = owner.gridSize;
                var xCount = radiusCount % 2 == 0 ? radiusCount + 1 : radiusCount;
                var zCount = radiusCount % 2 == 0 ? radiusCount : radiusCount + 1;

                var X = xCount * 2;
                var Z = zCount * 2;
                int vertexCount = (X + 1) * (Z + 1);
                int triCount = ((X + 1) * (Z + 1) - (X + 1) - (Z + 1) + 1) * 6;

                if (vertices == null || triangles == null || vertices.Length != vertexCount || triangles.Length != triCount)
                {
                    InitVertices(vertexCount, triCount);
                }

                // snap to grid 
                Vector3Int worldPosInt = new Vector3Int(
                    Mathf.RoundToInt(worldPos.x - (worldPos.x % (gridSize * xConst2))),
                    0,
                    Mathf.RoundToInt(worldPos.z - (worldPos.z % gridSize))
                    );

                var xStep = gridSize * xConst;
                var zStep = gridSize;

                var startPosXZ = new Vector2Int(worldPosInt.x - xCount * xStep, worldPosInt.z - zCount * zStep);

                int vertexIndex = 0;
                int triIndex = 0;
                bool even = true;
                for (int x = 0; x <= X; x++)
                {
                    for (int z = 0; z <= Z; z++)
                    {
                        Vector2Int posXZ = startPosXZ + new Vector2Int(xStep * x, zStep * z - (even ? gridSize / 2 : 0));
                        var posy = owner.GetHeight(posXZ.x + originShift.x, posXZ.y + originShift.z) - originShift.y;
                        vertices[vertexIndex] = new Vector3(posXZ.x, posy, posXZ.y);
                        owner.heightData[posXZ] = posy;
                        // edges of the mesh
                        if (x < X && z < Z)
                        {
                            if (even)
                            {
                                triangles[triIndex++] = vertexIndex;
                                triangles[triIndex++] = vertexIndex + 1;
                                triangles[triIndex++] = vertexIndex + Z + 1;

                                triangles[triIndex++] = vertexIndex + 1;
                                triangles[triIndex++] = vertexIndex + Z + 1 + 1;
                                triangles[triIndex++] = vertexIndex + Z + 1;
                            }
                            else
                            {
                                triangles[triIndex++] = vertexIndex;
                                triangles[triIndex++] = vertexIndex + Z + 1 + 1;
                                triangles[triIndex++] = vertexIndex + Z + 1;

                                triangles[triIndex++] = vertexIndex + 1;
                                triangles[triIndex++] = vertexIndex + Z + 1 + 1;
                                triangles[triIndex++] = vertexIndex;
                            }
                        }

                        vertexIndex++;
                    }
                    even = !even;
                }
            }

            void NewMesh()
            {
                // render mesh
                if (mesh == null)
                {
                    mesh = new UnityEngine.Mesh();
                }
                else
                {
                    mesh.Clear();
                }
            }

            public void ClearMeshAndVertices()
            {
                InitVertices(0, 0);

                if (mesh != null)
                {
                    mesh.Clear();
                }

                if (mf != null)
                {
                    if (mf.sharedMesh != null)
                    {
                        DestroyMesh(mf.sharedMesh);
                    }
                    mf.sharedMesh = null;
                }

                if (mc != null)
                {
                    if (mc.sharedMesh != null)
                    {
                        DestroyMesh(mc.sharedMesh);
                    }
                    mc.sharedMesh = null;
                }
            }

            public void InitVertices(int vertexCount, int triCount)
            {
                vertices = new Vector3[vertexCount];
                triangles = new int[triCount];
            }

            public void UnityDestroyMesh()
            {
                DestroyMesh(mesh);
            }

            private void DestroyMesh(Mesh mesh)
            {
#if UNITY_EDITOR
                if (Application.isPlaying)
#endif
                {
                    Destroy(mesh);
                }
#if UNITY_EDITOR
                else
                {
                    DestroyImmediate(mesh);
                }
#endif
            }

            public void InitMeshComponents()
            {
                if (mf == null && generateGraphics)
                {
                    mf = meshGO.GetComponent<MeshFilter>();
                    if (mf == null)
                        mf = meshGO.AddComponent<MeshFilter>();
                    mr = meshGO.GetComponent<MeshRenderer>();
                    if (mr == null)
                        mr = meshGO.AddComponent<MeshRenderer>();
                    //mr.sharedMaterial = meshMat;
                    mr.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
                    mr.receiveShadows = true;
                    if (mr.sharedMaterial == null)
                    {
                        mr.sharedMaterial = slopeMat;
                    }
                }

                if (mc == null && generateCollider)
                {
                    mc = meshGO.GetComponent<MeshCollider>();
                    if (mc == null)
                        mc = meshGO.AddComponent<MeshCollider>();
                    mc.sharedMaterial = physicMaterial;
                }
            }

            public void GenerateMesh()
            {
                InitMeshComponents();

                // if no graphics nor collider, it still generates heights for use with height provider system.
                if (generateGraphics || generateCollider)
                {
                    NewMesh();

                    // set vertices and shit here
                    mesh.vertices = vertices;
                    mesh.triangles = triangles;

                    mesh.RecalculateNormals();
                    mesh.RecalculateBounds();

                    if (generateGraphics)
                    {
                        mf.sharedMesh = mesh;
                    }

                    if (generateCollider)
                    {
                        mc.sharedMesh = mesh;
                    }
                }
            }

        }


#endregion

#region // ===================== RANDOM FUNKS ===================== //

        private void OnDrawGizmos()
        {
            if (showAreaDebug)
            {
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(transform.position, new Vector3(gridSize, 0.01f, gridSize));
            }
        }

#endregion

    }
}
