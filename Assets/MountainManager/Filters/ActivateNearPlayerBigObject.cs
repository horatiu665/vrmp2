namespace Mountain.Filters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Random = UnityEngine.Random;

    public class ActivateNearPlayerBigObject : MountainFilter, IOriginShifter
    {
        private MountainBigObjectFilter _filterWithDictionary;
        public MountainBigObjectFilter filterWithDictionary
        {
            get
            {
                if (_filterWithDictionary == null)
                {
                    _filterWithDictionary = GetComponent<MountainBigObjectFilter>();

                }
                return _filterWithDictionary;
            }
        }

        public override void Reset()
        {
            base.Reset();
            OnValidate();
        }

        private void OnValidate()
        {
            if (order <= filterWithDictionary.order)
            {
                Debug.LogWarning("[ForestSpawnActivateNearPlayer] Beware! This filter's order (" + order + ") is less than the filter it's activating"
                    + ". Automatically changed to order + 1 = " + _filterWithDictionary.order + 1 + "!", gameObject);
                order = _filterWithDictionary.order + 1;
            }
        }

        [SerializeField]
        private int _radius = 1;

        [SerializeField]
        private List<SpawnJobCoord> _activatedCoords = new List<SpawnJobCoord>();

        [SerializeField]
        private bool showGizmos;

        private Vector3 lastGenPos;

        private MountainMeshFilter _heightProvider;
        public MountainMeshFilter heightProvider
        {
            get
            {
                if (_heightProvider == null)
                {
                    _heightProvider = MountainHeightProviderSingleton.instance.heightMeshFilter;
                }
                return _heightProvider;
            }
        }

        [SerializeField]
        HashSet<FunctionalityLodBase> toDeactivate = new HashSet<FunctionalityLodBase>();
        [SerializeField]
        HashSet<FunctionalityLodBase> toActivate = new HashSet<FunctionalityLodBase>();


        public override void GenerateAt(params Vector3[] extraPos)
        {
            base.GenerateAt(extraPos);
            DoneApplying();
            return; // temp until we fix/remake it
            var worldPos = extraPos[0];
            var curCoord = filterWithDictionary.PosToCoord(worldPos);

            toDeactivate.Clear();
            toActivate.Clear();

            // check if activated objects must be deactivated again
            for (int i = _activatedCoords.Count - 1; i >= 0; i--)
            {
                var c = _activatedCoords[i];
                if (OutsideRadius(c, curCoord, _radius))
                {
                    var spawnList = _filterWithDictionary.GetSpawnedObjectsList(c);
                    if (spawnList != null)
                    {
                        for (int j = 0; j < spawnList.Count; j++)
                        {
                            // activate each object of this list
                            var ac = spawnList[j].GetComponent<FunctionalityLodBase>();
                            if (ac != null)
                            {
                                toDeactivate.Add(ac);
                            }
                        }
                    }

                    _activatedCoords.RemoveAt(i);
                }
            }

            for (int x = -_radius; x <= _radius; x++)
            {
                for (int z = -_radius; z <= _radius; z++)
                {
                    var c = curCoord + new SpawnJobCoord(x, z);

                    var spawnList = _filterWithDictionary.GetSpawnedObjectsList(c);
                    if (spawnList != null)
                    {
                        for (int i = 0; i < spawnList.Count; i++)
                        {
                            // activate each object of this list
                            var ac = spawnList[i].GetComponent<FunctionalityLodBase>();
                            if (ac != null)
                            {
                                toDeactivate.Remove(ac);
                                toActivate.Add(ac);
                            }

                        }
                    }

                    if (!_activatedCoords.Contains(c))
                        _activatedCoords.Add(c);
                }
            }

            foreach (var d in toDeactivate)
            {
                d.Deactivate();
            }

            foreach (var a in toActivate)
            {
                a.Activate();
            }

            DoneApplying();

            lastGenPos = worldPos;
        }

        public override void ClearAllInstant()
        {
            base.ClearAllInstant();
            _activatedCoords.Clear();

            DoneClearing();
        }

        public override void ClearSlow()
        {
            base.ClearSlow();
            _activatedCoords.Clear();

            DoneClearing();
        }

        private bool OutsideRadius(SpawnJobCoord spawnJobCoord, SpawnJobCoord curCoord, int _radius)
        {
            return spawnJobCoord.x < curCoord.x - _radius || spawnJobCoord.x > curCoord.x + _radius ||
                    spawnJobCoord.y < curCoord.y - _radius || spawnJobCoord.y > curCoord.y + _radius;
        }

        private void OnDrawGizmos()
        {
            if (!showGizmos)
                return;

            // draw square around radius
            Gizmos.color = Color.red;
            Gizmos.DrawWireCube(Vector3.zero, filterWithDictionary.gridSize * (_radius * 2 + 1) * new Vector3(1, 0, 1));

            var enu = _activatedCoords.GetEnumerator();
            while (enu.MoveNext())
            {
                GizmoDrawCoord(enu.Current);
            }
        }

        void GizmoDrawCoord(SpawnJobCoord c)
        {
            var col = Color.yellow;
            col.a = 0.4f;
            Gizmos.color = col;

            var pos = filterWithDictionary.CoordToPos(c);
            if (heightProvider.isApplied)
            {
                pos.y = heightProvider.SampleHeight(this, pos);
            }
            Gizmos.DrawCube(pos, Vector3.one * filterWithDictionary.gridSize * 0.8f);
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
        }

        public void OnWorldMove(Vector3 originShiftDelta)
        {
            var os = SpawnJobCoord.PosToCoord(originShiftDelta, filterWithDictionary.gridSize);

            // see comments at ActivateNearPlayer, same func
            if (this.isApplied && filterWithDictionary.isApplied)
            {
                //GenerateAt(lastGenPos - originShiftDelta);
                //GenerateAt(lastGenPos + originShiftDelta);
            }
        }

    }
}