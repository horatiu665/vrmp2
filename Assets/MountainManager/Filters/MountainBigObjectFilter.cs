namespace Mountain.Filters
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Random = UnityEngine.Random;

    public class MountainBigObjectFilter : MountainFilter, IOriginShifter, IDeferExecutions
    {
        [SerializeField]
        private int _gridSize = 150;

        [SerializeField]
        private int _radius = 14;

        [SerializeField]
        [Tooltip("If not null, gets all SpawnableChunkBase from children and uses as spawnSources")]
        private Transform spawnSourcesParent;

        [SerializeField]
        private BigObjects[] bigObjsList;

        public bool showGizmos = false;

        private Dictionary<BigObject, GameObject> spawnedBigObjects = new Dictionary<BigObject, GameObject>();

        private List<GameObject> spawnedObjectsListCache = new List<GameObject>(10);

        [SerializeField, ReadOnly]
        private Transform _parent = null;

        public Transform parent
        {
            get
            {
                if (_parent == null)
                {
                    _parent = new GameObject("[BigObjects] " + bigObjsList[0].name).transform;
                    _parent.SetParent(mountainManager.generatedParent, true);
                }
                return _parent;
            }
        }

        private void OnValidate()
        {
            InitSpawnSources();

            // ensure grid size is the same
            for (int i = 0; i < bigObjsList.Length; i++)
            {
                for (int j = 0; j < bigObjsList[i].list.Count; j++)
                {
                    var bo = bigObjsList[i].list[j];
                    if (bo.gridSize != _gridSize)
                    {
                        Debug.LogError("[MountainBigObjectFilter] Error! Grid size of spawnable chunks (" + bo.gridSize + ") cannot be different than on the spawner (" + _gridSize + ")!", bo.gameObject);
                    }

                }
            }

        }

        private void InitSpawnSources()
        {
            if (spawnSourcesParent != null)
            {
                bigObjsList = spawnSourcesParent.GetComponentsInChildren<BigObjects>();
            }
        }

        public override void GenerateAt(params Vector3[] extraPos)
        {
            base.GenerateAt(extraPos);
            
            for (int b = 0; b < bigObjsList.Length; b++)
            {
                var bigObjs = bigObjsList[b];

                if (bigObjs.isActiveAndEnabled)
                {

                    for (int i = 0; i < bigObjs.list.Count; i++)
                    {
                        var bigObj = bigObjs.list[i];

                        bool shouldBigBeSpawned = false;

                        for (int k = 0; k < extraPos.Length; k++)
                        {
                            if (ShouldBigObjectBeSpawned(extraPos[k], bigObj))
                            {
                                shouldBigBeSpawned = true;
                                break;
                            }
                        }
                        
                        if (shouldBigBeSpawned)
                        {
                            if (!spawnedBigObjects.ContainsKey(bigObj))
                            {
                                // spawn and save ref.
                                Spawn(bigObj);
                            }
                        }
                        else
                        {
                            // delete it
                            if (spawnedBigObjects.ContainsKey(bigObj))
                            {
                                Despawn(bigObj);
                            }
                        }
                    }

                }
            }

            DoneApplying();
        }

        private bool ShouldBigObjectBeSpawned(Vector3 worldPos, BigObject bigObj)
        {
            var curShiftedCoord = SpawnJobCoord.PosToCoord(OriginShiftManager.LocalToGlobalPos(worldPos), _gridSize);
            var minCoord = curShiftedCoord - new SpawnJobCoord(_radius, _radius);
            var maxCoord = curShiftedCoord + new SpawnJobCoord(_radius, _radius);

            // big obj should be spawned if the squares intersect and/or overlap
            if (SquaresOverlap(bigObj.minCoord, bigObj.maxCoord, minCoord, maxCoord))
            {
                return true;
            }
            return false;
        }

        private bool SquaresOverlap(SpawnJobCoord bigObjMinCoord, SpawnJobCoord bigObjMaxCoord,
            SpawnJobCoord curMinCoord, SpawnJobCoord curMaxCoord)
        {
            return bigObjMinCoord.x <= curMaxCoord.x && bigObjMinCoord.y <= curMaxCoord.y &&
                   bigObjMaxCoord.x >= curMinCoord.x && bigObjMaxCoord.y >= curMinCoord.y;
        }

        public override void ClearAllInstant()
        {
            base.ClearAllInstant();

            var enu = spawnedBigObjects.GetEnumerator();
            while (enu.MoveNext())
            {
                this.DestroySafe(enu.Current.Value);
            }
            spawnedBigObjects.Clear();

            DoneClearing();
        }

        public override void ClearSlow()
        {
            base.ClearSlow();

            // wait for the update to clear all the shit
        }

        private void Despawn(BigObject bigObj)
        {
            DeferredExecutionManager.instance.CountOperation(this, DeferredExecutionTags.DeleteJob);
#if UNITY_EDITOR
            if (!Application.isPlaying)
            {
                this.DestroySafe(spawnedBigObjects[bigObj]);
            }
            else
            {
#endif
                FoggyForestPoolManager.instance.Return(spawnedBigObjects[bigObj].GetComponent<IVRPrefab>());
#if UNITY_EDITOR
            }
#endif
            spawnedBigObjects.Remove(bigObj);
        }

        private void Spawn(BigObject bigObj)
        {
            DeferredExecutionManager.instance.CountOperation(this, DeferredExecutionTags.SpawnJob);
            var obj = FoggyForestPoolManager.instance.Spawn(bigObj.bigObjPrefab, OriginShiftManager.GlobalToLocalPos(bigObj.position), bigObj.rotation);
            obj.transform.SetParent(parent);
            obj.transform.localScale = bigObj.localScale;

            spawnedBigObjects.Add(bigObj, obj.gameObject);

#if UNITY_EDITOR
            EditorSpawnInfoManager.instance.AddSpawnInfoBigObject(obj, bigObj);
#endif
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);
        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
        }

        private void Update()
        {
            if (isSlowClearing)
            {
                bool anyCleared = false;
                for (int b = 0; b < bigObjsList.Length; b++)
                {
                    var bigObjs = bigObjsList[b];

                    for (int i = 0; i < bigObjs.list.Count; i++)
                    {
                        var bigObj = bigObjs.list[i];

                        if (DeferredExecutionManager.instance.CanContinue(this, DeferredExecutionTags.DeleteJob))
                        {
                            // delete it
                            if (spawnedBigObjects.ContainsKey(bigObj))
                            {
                                Despawn(bigObj);
                                anyCleared = true;
                            }
                        }
                    }
                }

                if (!anyCleared)
                {
                    // nobody was cleared! we're done
                    DoneClearing();
                }
            }
        }

        public void OnWorldMove(Vector3 originShiftDelta)
        {
            var enu = spawnedBigObjects.GetEnumerator();
            while (enu.MoveNext())
            {
                var obj = enu.Current.Value;
                obj.transform.position -= originShiftDelta;
            }
        }

        private void OnDrawGizmos()
        {
            if (showGizmos)
            {
                // draw square around radius
                Gizmos.color = Color.red;
                Gizmos.DrawWireCube(Vector3.zero, _gridSize * (_radius * 2 + 1) * new Vector3(1, 0, 1));

            }
        }

        public int gridSize
        {
            get
            {
                return this._gridSize;
            }
        }

        //IMountainObjectSpawnFilter
        public SpawnJobCoord PosToCoord(Vector3 position)
        {
            return SpawnJobCoord.PosToCoord(position, _gridSize);
        }

        public Vector3 CoordToPos(SpawnJobCoord coords)
        {
            return SpawnJobCoord.CoordToPos(coords, _gridSize);
        }

        public List<GameObject> GetSpawnedObjectsList(SpawnJobCoord c)
        {
            spawnedObjectsListCache.Clear();

            var os = SpawnJobCoord.PosToCoord(OriginShiftManager.originShift, gridSize);

            // create and return a list of big objects at this coord
            var enu = spawnedBigObjects.GetEnumerator();
            while (enu.MoveNext())
            {
                var obj = enu.Current;
                if (SquaresOverlap(obj.Key.minCoord - os, obj.Key.maxCoord - os, c, c))
                {
                    spawnedObjectsListCache.Add(obj.Value);
                }
            }

            return spawnedObjectsListCache;
        }
    }
}