namespace Mountain.Filters
{

    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using VRCore;
    using Helpers;
    using MovementEffects;
    using System;

    [Serializable]
    public struct SpawnJobCoord
    {
        public int x, y;

        public SpawnJobCoord(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public SpawnJobCoord(Vector2 coord)
        {
            this.x = (int)coord.x;
            this.y = (int)coord.y;
        }

        public static SpawnJobCoord operator +(SpawnJobCoord a, SpawnJobCoord b)
        {
            return new SpawnJobCoord(a.x + b.x, a.y + b.y);
        }

        public static SpawnJobCoord operator -(SpawnJobCoord a, SpawnJobCoord b)
        {
            return new SpawnJobCoord(a.x - b.x, a.y - b.y);
        }

        public static SpawnJobCoord operator /(SpawnJobCoord a, float b)
        {
            return new SpawnJobCoord(Mathf.RoundToInt(a.x / b), Mathf.RoundToInt(a.y / b));
        }

        public override int GetHashCode()
        {
            // because int is 32 bit and we will not likely have such big coords, this is unique.
            return (y << 16) ^ x;
            //return base.GetHashCode();
        }

        public static bool operator !=(SpawnJobCoord a, SpawnJobCoord b)
        {
            return !a.Equals(b);
        }

        public static bool operator ==(SpawnJobCoord a, SpawnJobCoord b)
        {
            return a.Equals(b);
        }

        public override bool Equals(object obj)
        {
            if (obj is SpawnJobCoord)
            {
                var oc = (SpawnJobCoord)obj;
                return oc.x == x && oc.y == y;
            }
            return base.Equals(obj);
        }

        public override string ToString()
        {
            return "(" + x + ", " + y + ")";
        }

        public static SpawnJobCoord PosToCoord(Vector3 position, int gridEdgeSize)
        {
            return new SpawnJobCoord(Mathf.RoundToInt(position.x / gridEdgeSize), Mathf.RoundToInt(position.z / gridEdgeSize));
        }

        public static Vector3 CoordToPos(SpawnJobCoord coords, int gridEdgeSize)
        {
            return new Vector3(coords.x * gridEdgeSize, 0, coords.y * gridEdgeSize);
        }
    }

}