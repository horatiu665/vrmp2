﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using Mountain.Filters;
using Mountain.Editor;

[CustomEditor(typeof(ActivateNearPlayer))]
public class ActivateNearPlayerEditor : MountainFilterEditor
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		ActivateNearPlayer anp = target as ActivateNearPlayer;
		EditorGUILayout.ObjectField("Filter To Activate", anp.filterWithDictionary as MonoBehaviour, typeof(MonoBehaviour), true);
	}
}
