using UnityEngine;

public class DebugHeightSampler : MonoBehaviour
{
	public float radius = 0.5f;
	public Vector3 A, B, C;

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Colors.Aquamarine;
		Gizmos.DrawSphere(A, radius);
		Gizmos.DrawLine(A, transform.position);
		Gizmos.color = Colors.Blue;
		Gizmos.DrawSphere(B, radius);
		Gizmos.DrawLine(B, transform.position);
		Gizmos.color = Colors.Chocolate;
		Gizmos.DrawSphere(C, radius);
		Gizmos.DrawLine(C, transform.position);
	}
}