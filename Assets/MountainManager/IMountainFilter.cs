using UnityEngine;

namespace Mountain
{
    public interface IMountainFilter
    {
        string ToString();

        bool enabled { get; }

        int order { get; set; }

        bool isApplied { get; set; }

        bool isSlowClearing { get; }

        // how long for the filter to be applied?
        float generationTime { get; set; }

        // how many frames for the filter to be applied?
        int generationFrames { get; set; }
        
        // Generates at position, and cleans up elsewhere (after itself)
        void GenerateAt(params Vector3[] extraPos);

        void ClearAllInstant();

        void ClearSlow();
    }
}