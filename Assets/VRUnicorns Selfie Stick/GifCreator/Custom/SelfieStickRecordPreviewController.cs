using UnityEngine;
using System.Collections;
//using Atomtwist.SoundClip;
using System.Collections.Generic;
using System;
using VRCore;
using VRCore.VRNetwork;
using Core;
using VRCore.VRNetwork.Client;

namespace VRUnicorns.SelfieStick
{

    [RequireComponent(typeof(SelfieStickButtonSetter))]
    public class SelfieStickRecordPreviewController : MonoBehaviour
    {
        SelfieStick equippable;

        SelfieStickButtonSetter buttonSetter;

        public GifRecorder recorder;
        public GifPreviewer previewer;
        public SelfieStickTweeter tweeter;

        bool showingPreview = false;
        bool recording = false;
        public bool Recording
        {
            get
            {
                return recording;
            }
        }

        private RenderTexture theCameraTexture;

        public void SetCameraRenderTexture(RenderTexture rt)
        {
            theCameraTexture = rt;
            recorder.recorderParams.camera.targetTexture = rt;
        }

        public Material cameraPreviewAndMirrorMaterial
        {
            get
            {
                return previewer.renderer.material;
            }
        }

        public int delayBeforeRecordingInSeconds = 3;

        [Header("Graphics overlay")]
        public Renderer overlayOnCountdown;
        public AnimationCurve overlayOnCountdownAlphaOverTime = new AnimationCurve() { keys = new Keyframe[] { new Keyframe(0, 0, 0, 0), new Keyframe(1, 1, 0, 0) } };
        public Transform overlayWhileRecording;
        public Transform overlayBeforeAndAfter;
        public float overlayBeforeAndAfterDuration = 0.1f;
        public Transform overlayWhilePlayback;
        public float resetCameraPoufSize = 0.2f;

        [Header("Sounds")]
        public SmartSound resetCameraSound;
        private SelfieStickSounds sounds;

        void Awake()
        {
            equippable = GetComponent<SelfieStick>();
            sounds = GetComponent<SelfieStickSounds>();
        }

        void Start()
        {
            buttonSetter = GetComponent<SelfieStickButtonSetter>();
            buttonSetter.SetRecordButton(SelfieStickButtonSetter.RecordButtonMode.CanRecord);

            recorder.gifPreviewer = previewer;

            recorder.OnRecordingDone += Recorder_OnRecordingDone;
            recorder.PreviewDone += Recorder_PreviewDone;

            InitRecording();
        }

        void InitRecording()
        {
            overlayOnCountdown.gameObject.SetActive(true);
            overlayWhileRecording.gameObject.SetActive(false);
            overlayBeforeAndAfter.gameObject.SetActive(false);
            overlayWhilePlayback.gameObject.SetActive(false);

            SetMirrorOnPreview(true);

            buttonSetter.SetRecordButton(SelfieStickButtonSetter.RecordButtonMode.CanRecord);

        }


        public void OnTouchpadTouchUp()
        {
            if (IsEquipped())
            {
                buttonSetter.OnTouchUp();
            }
        }

        public void OnTouchpadTouch(Vector2 axis)
        {
            if (IsEquipped())
            {
                var y = axis.y;
                buttonSetter.OnTouch(y);
            }
        }

        public void OnTouchpadPressUp()
        {
            if (IsEquipped())
            {
                buttonSetter.OnPressUp();
            }
        }

        public void OnTouchpadPressDown(Vector2 axis)
        {
            if (IsEquipped())
            {
                var y = axis.y;
                buttonSetter.OnPress(y);
                // record stuff
                if (y < 0.5f)
                {
                    OnRecordButtonClicked();
                }
                else
                {
                    tweeter.OnTweetButtonClicked();
                }
            }
        }

        // is button interface active? in other words, is the object grabbed?
        private bool IsEquipped()
        {
            return equippable.isEquipped;
            // return GetComponent<BeingGrabbedByTool>() != null;

        }

        void SetMirrorOnPreview(bool mirrorOn)
        {
            cameraPreviewAndMirrorMaterial.SetTextureScale("_MainTex", new Vector2(mirrorOn ? -1 : 1, 1));

        }

        void OnRecordButtonClicked()
        {
            if (!recording)
            {
                if (showingPreview)
                {
                    // IF PREVIEW SHOW CAMERA AGAIN
                    ScrapRecordingAndShowCamera();

                    if (NetServices.isClient && NetServices.isNetworked)
                    {
                        var msg = MessagePool.Get<SelfieStickDeleteRecordMessage>();
                        msg.equippableNetId = equippable.netId;
                        ClientNetSender.instance.Send(msg, UnityEngine.Networking.QosType.Reliable);
                    }
                }
                else if (!showingPreview)
                {
                    // IF NOT PREVIEW, START RECORDING
                    RecordAndPreviewOnSameScreen();

                    if (NetServices.isClient && NetServices.isNetworked)
                    {
                        var msg = MessagePool.Get<SelfieStickStartRecordMessage>();
                        msg.equippableNetId = equippable.netId;
                        ClientNetSender.instance.Send(msg, UnityEngine.Networking.QosType.Reliable);
                    }
                }
            }
        }

        void Recorder_OnRecordingDone()
        {
            overlayWhileRecording.gameObject.SetActive(false);
            OverlayBeforeAndAfterFire();
            overlayWhilePlayback.gameObject.SetActive(true);

            // make preview realistic, as camera would show it (flipped, so no mirroring)
            SetMirrorOnPreview(false);

            // preview, so set button
            buttonSetter.SetRecordButton(SelfieStickButtonSetter.RecordButtonMode.Previewing);

        }

        void Recorder_PreviewDone()
        {
            // preview is done, means we can activate the tweet stuff
            showingPreview = true;
            recording = false;

        }

        void OverlayBeforeAndAfterFire()
        {
            // start rec overlay
            overlayBeforeAndAfter.gameObject.SetActive(true);
            StartCoroutine(pTween.Wait(overlayBeforeAndAfterDuration, () =>
            {
                overlayBeforeAndAfter.gameObject.SetActive(false);

            }));
        }

        /// <summary>
        /// Initializes recorder, resets overlays and button.
        /// Records gif and sets the preview.
        /// Finishes when recorder status is PreviewDone
        /// </summary>
        [DebugButton]
        public void RecordAndPreviewOnSameScreen()
        {
            // init recording, but tweet takes care of itself.
            InitRecording();

            showingPreview = false;
            recording = true;
            var c = overlayOnCountdown.material.GetColor("_TintColor");

            // we are recording, so set button
            buttonSetter.SetRecordButton(SelfieStickButtonSetter.RecordButtonMode.Recording);

            // countdown olay
            StartCoroutine(pTween.To(delayBeforeRecordingInSeconds, 0, delayBeforeRecordingInSeconds, t =>
            {
                c.a = overlayOnCountdownAlphaOverTime.Evaluate(t);
                overlayOnCountdown.material.SetColor("_TintColor", c);
            }));

            StartCoroutine(pTween.Wait(delayBeforeRecordingInSeconds, () =>
            {
                recorder.RecordAndPreview();
                // while rec overlay
                overlayWhileRecording.gameObject.SetActive(true);
                OverlayBeforeAndAfterFire();
            }));
            sounds.SnapshotWithCountdown((int)delayBeforeRecordingInSeconds, recorder.recorderParams.duration);

        }

        [DebugButton]
        public void ScrapRecordingAndShowCamera()
        {
            // wait until recorder has finished preprocessing = so the separate thread that saves the gifs can start working in parallel.
            showingPreview = false;

            // flip preview to show mirrored image
            SetMirrorOnPreview(true);

            buttonSetter.SetRecordButton(SelfieStickButtonSetter.RecordButtonMode.CanRecord);

            previewer.Init();

            // do not discard frames yet, only when new recording starts.

            cameraPreviewAndMirrorMaterial.SetTexture("_MainTex", theCameraTexture);
            if (resetCameraSound != null)
            {
                resetCameraSound.Play();
            }
            overlayWhilePlayback.gameObject.SetActive(false);

        }

    }
}