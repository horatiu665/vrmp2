using UnityEngine;
using System.Collections;
using System;
using VRCore;
using PygmyMonkey.GifCreator;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace VRUnicorns.SelfieStick
{

    public class SelfieStickRecordingSaver : MonoBehaviour
    {
        public GifRecorder gifRecorder;
        Recorder recorder;

        public string gifFileName = "SelfieTennisGif";
        public bool saveUnderMyDocuments = true;
        public string gifDirectory = @"VRUnicorns\SelfieTennis";
        public string myDocumentsSlashGifs
        {
            get
            {
                return Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + @"\" + gifDirectory;
            }
        }
        //string lastSavedGifPath;
        string nextSavedGifPath;
        string nextSavedFilenameWithoutExtension;

        DateTime savingFileStartTime;

        [Range(0, 1)]
        public float preProcessProgress;

        [Range(0, 1)]
        public float fileSaveProgress;

        public bool savingFile = false;

        public event Action<string> FileSaved;

        public string GetNextGifSavePath()
        {
            return nextSavedGifPath;
        }

        public void StartSavingToFile()
        {
            if (recorder.State != RecorderState.PreProcessing
                && recorder.State != RecorderState.Recording
                && recorder.State != RecorderState.Saving)
            {
                if (!savingFile)
                {
                    savingFile = true;
                    SaveToFile();
                }
            }
        }

        [DebugButton]
        private void SaveToFile()
        {
            fileSaveProgress = 0;
            preProcessProgress = 0;

            // GOES TO SAVE->PREPROCESS->FileSaved.
            recorder.Save(nextSavedFilenameWithoutExtension, 0, recorder.FrameList.Count - 1);
            savingFileStartTime = DateTime.Now;
        }

        private void OnFileSaved(string filePath)
        {
            print("File saved after " + (DateTime.Now.Subtract(savingFileStartTime).ToString()));

            //lastSavedGifPath = filePath;

            // now we can tweet
            if (FileSaved != null)
                FileSaved(filePath);

            // set flag after doing the actions on the event ;) - clever boy
            savingFile = false;
        }

        public void Start()
        {
            recorder = gifRecorder.recorder;
            recorder.OnFileSaved += delegate (int threadId, string filePath) {
                this.fileSaveProgress = 1;
                OnFileSaved(filePath);
            };
            recorder.OnFileSaveProgress += delegate (int threadId, float progress) {
                this.fileSaveProgress = progress;
            };
            recorder.UpdatedPreProcessProgress += delegate (float progress) {
                this.preProcessProgress = progress;
            };
            gifRecorder.OnRecordingDone += OnRecordingDone;

            preProcessProgress = 0;
            fileSaveProgress = 0;
        }

        /// <summary>
        /// save filename here, for tweet system to be able to trigger its saving.
        /// </summary>
        void OnRecordingDone()
        {
            gifRecorder.recorderParams.destinationFolder = myDocumentsSlashGifs;
            var date = DateTime.Now.ToString("yyyy-MM-dd hh-mm-ss");
            nextSavedFilenameWithoutExtension = gifFileName + " " + date;
            nextSavedGifPath = myDocumentsSlashGifs + @"\" + nextSavedFilenameWithoutExtension + ".gif";

            fileSaveProgress = 0;
        }
        
    }
}