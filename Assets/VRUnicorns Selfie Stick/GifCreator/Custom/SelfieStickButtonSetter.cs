using UnityEngine;
using System.Collections;
using System;
namespace VRUnicorns.SelfieStick
{

    public class SelfieStickButtonSetter : MonoBehaviour
    {

        public enum TweetButtonMode
        {
            TweetActive,
            TweetInactive,
            UploadingCancel
        }

        public enum RecordButtonMode
        {
            CanRecord,
            Recording,
            Previewing
        }

        [Header("Buttons")]
        public Renderer recordButtonRenderer;
        public Renderer tweetButtonRenderer;

        public Material recordMat
        {
            get
            {
                return recordButtonRenderer.material;
            }
        }

        public Material tweetMat
        {
            get
            {
                return tweetButtonRenderer.material;
            }
        }

        public Texture2D butRecord;
        public Texture2D butRecording, butPlaybackTrash, butTweet, butTweetInactive, butTweetCancel;
        public SetAlphaCutoffOnMaterial tweetProgressBar;
        public Transform progressBarRotateRoot;

        #region highlight on touch over
        [Header("Highlight darken stuff")]
        public float highlightAmount = 1.4f;
        public float darkenAmount = 0.6f;

        void Highlight(Material mat, bool highlight)
        {
            var value = highlight ? highlightAmount : 1f;
            mat.SetFloat("_Highlight", value);

        }

        void Darken(Material mat, bool dark)
        {
            var value = dark ? darkenAmount : 1f;
            mat.SetColor("_TintColor", Color.white * value);

        }

        public void OnTouch(float touchY)
        {
            if (touchY >= 0.5f)
            {
                Highlight(recordMat, false);
                Highlight(tweetMat, true);

            }
            else
            {
                Highlight(recordMat, true);
                Highlight(tweetMat, false);

            }
        }

        public void OnTouchUp()
        {
            Highlight(recordMat, false);
            Highlight(tweetMat, false);

        }

        public void OnPress(float pressY)
        {
            if (pressY >= 0.5f)
            {
                Darken(tweetMat, true);

            }
            else
            {
                Darken(recordMat, true);

            }
        }

        internal void OnPressUp()
        {
            Darken(tweetMat, false);
            Darken(recordMat, false);

        }
        #endregion

        TweetButtonMode tweetButtonMode;
        public void SetTweetButton(TweetButtonMode mode)
        {
            tweetButtonMode = mode;
            tweetMat.SetTexture("_MainTex", mode == TweetButtonMode.TweetActive ? butTweet : mode == TweetButtonMode.TweetInactive ? butTweetInactive : butTweetCancel);

        }

        public void SetRecordButton(RecordButtonMode mode)
        {
            recordMat.SetTexture("_MainTex", mode == RecordButtonMode.CanRecord ? butRecord : mode == RecordButtonMode.Recording ? butRecording : butPlaybackTrash);

        }

        public void SetTweetProgress(float progress)
        {
            tweetProgressBar.SetAlphaCutoff(progress);

        }

        public void SetRotation(bool on)
        {
            progressBarRotateRoot.GetComponent<SelfieStickProgressRotate>().rotate = on;
            if (!on)
            {
                var r = progressBarRotateRoot.localEulerAngles;
                r.z = 0;
                progressBarRotateRoot.localEulerAngles = r;

            }
        }

        public TweetButtonMode GetTweetButtonMode()
        {
            return tweetButtonMode;
        }
    }
}