using UnityEngine;
using System.Collections;
using PygmyMonkey.GifCreator;
using System.Collections.Generic;
using System.Linq;
using System;
using VRCore;
using VRCore.VRNetwork;

namespace VRUnicorns.SelfieStick
{

    [ExecuteInEditMode]
    public class GifRecorder : MonoBehaviour
    {
        #region recording

        #region variables

        [SerializeField]
        public List<ITexture> FrameList;

        /// <summary>
        /// cannot be serialized. uses parameters below to access parameters from that class
        /// </summary>
        public RecorderParameters recorderParams;

        [Serializable]
        public class RecorderParametersInterface
        {
            public Camera camera;
            public RecorderParameters.RecordMode recordMode = RecorderParameters.RecordMode.SINGLE_CAMERA;
            public RecorderParameters.Section section = RecorderParameters.Section.CustomRectAbsolute;
            public Rect relative = new Rect() { x = 0, y = 0, width = 0.5f, height = 0.5f };
            public Rect absolute = new Rect() { x = 0, y = 0, width = 512, height = 512 };
            public Color customRectPreviewColor = new Color() { r = 1, g = 0, b = 0, a = 0.33f };
            public bool showCustomRectPreview = true, recordUpsideDown = true, pauseAfterRecord = false;
            public RecorderParameters.RecordType recordType = RecorderParameters.RecordType.Duration;
            public int framePerSecond = 15;
            public int repeat = 0;
            public int quality = 95;
            public float duration = 3f;
            public int recordWidth = 450;
            public System.Threading.ThreadPriority workerPriority = System.Threading.ThreadPriority.BelowNormal;
            public string destinationFolder = "";
            public bool openFolderAfterSave = false;
            public int outputWidth = 450;

            public void SetParams(RecorderParameters target)
            {
                target.camera = camera;
                target.recordMode = recordMode;
                target.section = section;
                target.relativePosX = relative.x;
                target.relativePosY = relative.y;
                target.relativeWidth = relative.width;
                target.relativeHeight = relative.height;
                target.absolutePosX = (int)absolute.x;
                target.absolutePosY = (int)absolute.y;
                target.absoluteWidth = (int)absolute.width;
                target.absoluteHeight = (int)absolute.height;
                target.customRectPreviewColor = customRectPreviewColor;
                target.showCustomRectPreview = showCustomRectPreview;
                target.recordUpsideDown = recordUpsideDown;
                target.pauseAfterRecord = pauseAfterRecord;
                target.recordType = recordType;
                target.framePerSecond = framePerSecond;
                target.repeat = repeat;
                target.quality = quality;
                target.duration = duration;
                target.recordWidth = recordWidth;
                target.workerPriority = workerPriority;
                target.destinationFolder = destinationFolder;
                target.openFolderAfterSave = openFolderAfterSave;
                target.outputWidth = outputWidth;
            }
        }
        public RecorderParametersInterface recorderParamsInspector = new RecorderParametersInterface();

        bool initialized = false;

        public GifPreviewer gifPreviewer;
        public Recorder recorder;

        public enum RecordingStatus
        {
            NotInitialized, // cannot do anything when here
            Initialized, // can start recording, cannot tweet.
            Recording, // recording, cannot tweet.
            RecordingDone, // done recording, can start previewing, still cannot tweet.
            CalculatingPreview, // started previewing but not visible yet, still cannot tweet.
            PreviewDone, // preview is looping. can start tweeting.
        }

        RecordingStatus status = RecordingStatus.NotInitialized;
        public RecordingStatus GetStatus()
        {
            return status;
        }

        SelfieStickRecordingSaver saver;
        #endregion

        #region Record and Preview

        // initializes recorder. cannot work without, auto-done at Start
        [DebugButton]
        public void Init()
        {
            //recorderParams = gifSettings.recorderParams;
            //recorderParams.camera = gifCamera;
            if (recorder == null) {
                recorder = gameObject.GetOrAddComponent<Recorder>();
                //recorder = gameObject.GetComponent<Recorder>();
                //if (recorder == null) {
                //    recorder = gameObject.AddComponent<Recorder>();
                //}

            }
            if (recorderParams == null) {
                recorderParams = new RecorderParameters();
            }
            recorderParamsInspector.SetParams(recorderParams);

            recorder.Init(recorderParams);

            status = RecordingStatus.Initialized;
        }

        public event Action RecordStart;

        [DebugButton]
        // calls coroutine below
        public void RecordAndPreview()
        {
            StartCoroutine(RecordAndPreviewCor());
        }

        IEnumerator RecordAndPreviewCor()
        {
            Init();

            if (RecordStart != null) {
                RecordStart();
            }

            // GOES TO RECORD=>RecordingDone()
            Record();

            // when recording is done
            while (status == RecordingStatus.Recording) {
                yield return 0;
            }

            if (status == RecordingStatus.RecordingDone) {
                // recording done, we can preview
                // GOES TO PREVIEW->PreviewDone()
                Preview();

                // if nothing is happening and performance is good, save GIF here.

            }

        }

        #region Record

        // records a gif, saves the result and resets recorder
        [DebugButton]
        public void Record()
        {
            if (!initialized) {
                Init();
                initialized = true;
            }
            recorder.Record();

            status = RecordingStatus.Recording;
        }

        /// <summary>
        /// returns gif path for the twitter system to follow.
        /// </summary>
        public event Action OnRecordingDone;

        private void RecordingDone()
        {
            FrameList = recorder.FrameList;
            //FrameList = recorder.FrameList.Select(i => i.GetTexture2D()).ToList();

            if (OnRecordingDone != null) {
                OnRecordingDone();
            }

            status = RecordingStatus.RecordingDone;

        }

        #endregion

        #endregion Record and Preview

        #region Preview

        [Header("How many gif frames to process each frame? Avoids lag in VR")]
        public int processGifFramesPerGameFrame = 1;

        public bool separateGetAndSetPixelsForPerformance = true;

        public event Action PreviewDone;

        [DebugButton]
        // starts cor below
        public void Preview()
        {
            StartCoroutine(PreviewCor());
        }

        /// <summary>
        /// calculates preview on the preview gameobject (screen)
        /// When it is done, calls PreviewDone event and changes state to PreviewDone
        /// </summary>
        private IEnumerator PreviewCor()
        {
            if (status != RecordingStatus.CalculatingPreview) {
                status = RecordingStatus.CalculatingPreview;
                if (gifPreviewer.textures != null) {
                    gifPreviewer.textures.Clear();
                }
                gifPreviewer.textures = new List<Texture2D>();
                int countFrames = 0;
                foreach (var rt in FrameList) {
                    countFrames++;
                    if (countFrames >= processGifFramesPerGameFrame) {
                        countFrames = 0;
                        yield return 0;
                    }
                    var rtt = rt.GetTexture2D();
                    var newT = new Texture2D(rtt.width, rtt.height, rtt.format, false);
                    if (separateGetAndSetPixelsForPerformance) {
                        var getP = rtt.GetPixels32();
                        yield return 0;
                        newT.SetPixels32(getP);

                    } else {
                        newT.SetPixels32(rtt.GetPixels32());

                    }
                    // apply overlay here
                    //var overlayRect = new Rect(overlayTexturePositionPercent.x, overlayTexturePositionPercent.y, overlayTexture.width, overlayTexture.height);
                    //ApplyOverlay(newT, overlayTexture, overlayRect);

                    newT.Apply();
                    gifPreviewer.textures.Add(newT);
                }
                gifPreviewer.StartPreview(recorderParams.framePerSecond);

                // why are we resetting the save progress here? we are just starting a new preview, and we might be in the middle of tweeting something.

                status = RecordingStatus.PreviewDone;

                if (PreviewDone != null) {
                    PreviewDone();
                }

            }
        }

        #region add overlay from code
        public Texture2D overlayTexture;
        public Vector2 overlayTexturePositionPercent;

        private void ApplyOverlay(Texture2D newT, Texture2D overlayTexture, Rect r)
        {
            for (int x = (int)r.x; x < r.x + r.width; x++) {
                for (int y = (int)r.y; y < r.y + r.height; y++) {
                    var px = x - (int)r.x;
                    var py = y - (int)r.y;
                    // alpha blend pixels
                    var overlaypixel = overlayTexture.GetPixel(px, py);
                    var pixel = overlaypixel * overlaypixel.a + (1 - overlaypixel.a) * newT.GetPixel(px, py);
                    newT.SetPixel(x, y, pixel);

                }
            }
        }

        #endregion

        #endregion

        #endregion


        void Awake()
        {
            Init();
            recorder.OnRecordingDone += delegate () {
                RecordingDone();
            };

            recorder.OnEncoderFinished += Recorder_OnEncoderFinished;

            recorder.OnPreProcessingDone += delegate () {
                // preProcessing is done, that means the separate thread has started to work on encoding and saving the gif to file.
                // we can safely Init recorder now
                Init();

            };

        }

        public event Action<byte[]> OnRecorderEncoderFinished;

        private void Recorder_OnEncoderFinished(byte[] hackedBytes)
        {
            //this.fileSaveProgress = 1;

            if (OnRecorderEncoderFinished != null)
                OnRecorderEncoderFinished(hackedBytes);

        }

        public bool printStateEveryFrame = true;

        internal void Stop()
        {
            StopAllCoroutines();
        }

    }
}