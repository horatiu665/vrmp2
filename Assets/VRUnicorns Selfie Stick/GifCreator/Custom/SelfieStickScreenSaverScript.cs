namespace VRUnicorns.SelfieStick
{
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;
    using PygmyMonkey.GifCreator;
    using System.Linq;
    using System;


    public class SelfieStickScreenSaverScript : MonoBehaviour
    {

        public List<Texture2D> textures = new List<Texture2D>();
        public Texture defaultTexture;
        int i = 0;
        Renderer r;
        public bool useSharedMaterial = true;

        public float framesPerSecond = 15;

        public bool previewOnStart = false;

        void Awake()
        {
            r = GetComponent<Renderer>();

        }

        void Start()
        {

            if (previewOnStart)
            {
                StartPreview(framesPerSecond);
            }
            else
            {
                Init();

            }
        }

        public void StartPreview(float framesPerSecond)
        {
            this.framesPerSecond = framesPerSecond;
            StopAllCoroutines();
            StartCoroutine(UpdateWithFramerate(framesPerSecond));
        }

        IEnumerator UpdateWithFramerate(float fps)
        {
            if (fps <= 0) fps = 15;
            float frameTime = 1 / fps;
            while (true)
            {
                if (textures != null)
                    if (textures.Any())
                    {
                        i = i % textures.Count;
                        if (useSharedMaterial)
                        {
                            r.sharedMaterial.SetTexture("_MainTex", textures[i]);
                        }
                        else
                        {
                            r.material.SetTexture("_MainTex", textures[i]);
                        }
                        i++;
                    }
                yield return new WaitForSeconds(frameTime);
            }
        }

        void OnEnable()
        {
            StartPreview(framesPerSecond);

        }

        public void Init()
        {
            if (useSharedMaterial)
            {
                r.sharedMaterial.SetTexture("_MainTex", defaultTexture);
            }
            else
            {
                r.material.SetTexture("_MainTex", defaultTexture);
            }
        }

    }
}