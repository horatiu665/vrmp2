using UnityEngine;
using System.Collections;
//using Atomtwist.SoundClip;
using System.Collections.Generic;
using System.Linq;
using VRCore;

namespace VRUnicorns.SelfieStick
{

    /// <summary>
    /// Tweeter. Handles tweet interaction through the selfie stick interface.
    /// </summary>
    public class SelfieStickTweeter : MonoBehaviour
    {
        public static SelfieStickTweeter inst;

        [Header("TWEETS ALWAYS CONTAIN THIS HARDCODED GAME NAME")]
        public static string gameName = "#Utopia";

        SelfieStickRecordPreviewController ssController;
        GifRecorder gifRecorder;
        SelfieStickRecordingSaver saver;

        public TwitterController twitterController;
        SelfieStickButtonSetter buttonSetter;

        const string FILENAME_NULL = "---";


        /// <summary>
        /// path to the most recently saved gif file. set by recorder event OnFileSaved()
        /// </summary>
        string lastSavedGifPath = FILENAME_NULL;
        string lastSuccessfulTweetFilename;

        public bool showDebugLogs = false;

        public enum TweeterStates
        {
            NotTweeting,
            Tweeting

        }

        public TweeterStates state = TweeterStates.NotTweeting;

        // made true in OnRecordingDone
        bool recordingDoneAndPreviewed = false;

        // initializes values and references
        void Awake()
        {
            inst = this;

            ssController = GetComponent<SelfieStickRecordPreviewController>();
            gifRecorder = ssController.recorder;
            buttonSetter = GetComponent<SelfieStickButtonSetter>();
            buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetInactive);
            buttonSetter.SetTweetProgress(0);

            saver = GetComponent<SelfieStickRecordingSaver>();

            if (twitterController == null)
            {
                twitterController = TwitterController.inst;
            }
            if (twitterController == null)
            {
                DebugLog("TwitterDemo script is not set up. Did you add it somewhere in the scene?");
            }
            twitterController.TweetPosted += OnTweetPosted;
            twitterController.TweetMediaUploadPosted += OnTweetMediaUploadPosted;
            saver.FileSaved += Saver_FileSaved;
            gifRecorder.OnRecordingDone += GifRecorder_OnRecordingDone;
            gifRecorder.PreviewDone += GifRecorder_PreviewDone;
            gifRecorder.RecordStart += GifRecorder_RecordStart;

            recordingDoneAndPreviewed = false;
        }

        /// <summary>
        /// returns true when the most recent file is saved
        ///    and ready to be tweeted, and hasn't been already tweeted.
        /// </summary>
        public bool LastRecordingAlreadyUploaded()
        {
            return (lastSavedGifPath == lastSuccessfulTweetFilename);
            //return gifRecorder.fileSaveProgress == 1;
        }

        public bool IsTweeting()
        {
            return state == TweeterStates.Tweeting;
        }

        public void CancelAndReset()
        {
            // maybe stop all coroutines on the twitterController?
            // then also stop the Tweet() coroutine?
            // but also make sure to stop or cancel saving the file being saved in the recorder?
        }

        /// <summary>
        /// Tweets the last file saved by the recorder.
        /// </summary>
        IEnumerator StartTweeting()
        {
            if (!recordingDoneAndPreviewed)
            {
                PlayCannotTweetSound();
                yield break;
            }

            if (state == TweeterStates.NotTweeting)
            {
                state = TweeterStates.Tweeting;

                // start saving the file = show uploading button.
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.UploadingCancel);

            }
            else
            {
                PlayCannotTweetSound();
                yield break;
            }

            // state is definitely NotTweeting.

            // if gifRecorder did not save file yet, or saved a new file since the last one we tweeted
            if (saver.fileSaveProgress != 1
                || (saver.fileSaveProgress == 1 && saver.GetNextGifSavePath() != lastSavedGifPath))
            {
                // if gifRecorder did not start saving file
                if (!saver.savingFile)
                {
                    DebugLog("Starting to save file because " +
                        (saver.fileSaveProgress != 1
                            ? "gifRecorder didn't start saving file"
                            : "gifRecorder saved a new file since last tweet"));
                    saver.StartSavingToFile();
                }
                else
                {
                    // gifRecorder started saving to file already.
                    DebugLog("waiting for gif to finish saving the current file");
                }

                while (saver.savingFile)
                {
                    yield return 0;
                }
            }

            // gifRecorder definitely finished saving the file.

            if (!LastRecordingAlreadyUploaded())
            {
                // GOES TO OnTweetPosted() and OnTweetMediaUploadPosted()
                twitterController.TweetGif(lastSavedGifPath, GenerateTweet());

            }
            else
            {
                // file is not ready, or trying to double tweet.
                PlayCannotTweetSound();
                state = TweeterStates.NotTweeting;
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetInactive);
            }
        }

        #region Events

        /// <summary>
        /// SelfieStick interface button clicked.
        /// </summary>
        [DebugButton]
        public void OnTweetButtonClicked()
        {
            StartCoroutine(StartTweeting());
        }

        void OnTweetPosted(bool success)
        {
            state = TweeterStates.NotTweeting;

            if (success)
            {
                // good feedback
                if (tweetSuccessParticles != null)
                    tweetSuccessParticles.Play();
                if (tweetSuccessSound != null)
                {
                    tweetSuccessSound.Play();
                }
                // to avoid tweeting the same GIF again, save its filename.
                lastSuccessfulTweetFilename = lastSavedGifPath;

            }
            else
            {
                // not successful. play fail sound
                PlayCannotTweetSound();

                #region //retweet
                // maybe try tweeting the stuff again, 
                //      until cancel or until X amount of tries.
                // using Tweet();
                // or the coroutine or something ...
                #endregion

            }

            // BUTTON
            // if last savedgif is different than the next gif to save, we can set tweet button to active
            if (lastSavedGifPath != saver.GetNextGifSavePath())
            {
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetActive);
            }
            else
            {
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetInactive);
            }
        }

        private void OnTweetMediaUploadPosted(bool success)
        {
            if (!success)
            {
                // if not success, not tweeting anymore
                state = TweeterStates.NotTweeting;

                // BUTTON
                if (lastSavedGifPath != saver.GetNextGifSavePath())
                {
                    buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetActive);
                }
                else
                {
                    buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetInactive);
                }
            }
            else
            {
                // if successful, automatically tweets the uploaded media.
                // this happens in the twitterController.
            }
        }

        private void GifRecorder_RecordStart()
        {
            // set recordingDoneAndPreviewed to false, because we have started a new recording and we should not tweet.
            recordingDoneAndPreviewed = false;

            if (!IsTweeting())
            {
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetInactive);
            }

        }

        private void GifRecorder_OnRecordingDone()
        {
            // for performance reasons, start saving only on PreviewDone
        }

        private void GifRecorder_PreviewDone()
        {
            recordingDoneAndPreviewed = true;

            // if not tweeting
            if (state == TweeterStates.NotTweeting)
            {
                // activate button
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetActive);
            }
            else
            {
                // if already tweeting, wait for tweet to fail to decide button.

            }
        }

        private void Saver_FileSaved(string path)
        {
            lastSavedGifPath = path;

            // if not tweeting
            if (state == TweeterStates.NotTweeting)
            {
                // set active button
                buttonSetter.SetTweetButton(SelfieStickButtonSetter.TweetButtonMode.TweetActive);
            }
            else
            {
                // if tweeting, let the tweet finish before changing interface.
            }
        }

        #endregion

        #region Generating tweet text string

        [Header("Tweet text variables")]

        public List<string> defaultTextsForTweet = new List<string>();

        public UnityEngine.UI.InputField overrideDefaultTweetText
        {
            get
            {
                if (CustomTweetHandling.inst != null)
                {
                    return CustomTweetHandling.inst.CustomTweetInputField;
                }
                return null;
            }
        }
        private string GenerateTweet()
        {
            var tweet = defaultTextsForTweet[Random.Range(0, defaultTextsForTweet.Count)];
            if (overrideDefaultTweetText != null)
            {
                if (overrideDefaultTweetText.text != "")
                {
                    tweet = overrideDefaultTweetText.text;
                    if (!tweet.ToLower().Contains(gameName.ToLower()))
                    {
                        tweet = tweet + " " + gameName;
                    }
                }
            }
            return tweet;
        }

        #endregion

        #region Sounds and juice

        [Header("Sounds and juice")]

        public ParticleSystem tweetSuccessParticles;

        public SmartSound tweetSuccessSound;

        public SmartSound soundCannotTweet;

        public float progressBarValueWhileUploading = 0.5f;

        void PlayCannotTweetSound()
        {
            // gif not ready in memory. play BBOOOOP sound.
            if (soundCannotTweet != null)
            {
                soundCannotTweet.Play();
            }
        }

        /// <summary>
        /// Set every frame by SelfieStick controller. Handles all interface related to progress bar based on tweet status.
        /// </summary>
        /// <param name="progress"></param>
        public void HandleProgressBar(float progress)
        {
            // show progress "bar"
            // if tweeting, show rotating bar because we don't know how long it takes
            if (IsTweeting())
            {
                // if saving file, do increasing tweet progress.
                if (saver.savingFile)
                {
                    buttonSetter.SetRotation(false);
                    buttonSetter.SetTweetProgress(progress);
                }
                else
                {
                    // else rotate that half progress bar
                    buttonSetter.SetRotation(true);
                    buttonSetter.SetTweetProgress(progressBarValueWhileUploading);
                }
            }
            else
            {
                buttonSetter.SetRotation(false);
                buttonSetter.SetTweetProgress(0);
            }
        }

        #endregion

        #region Debug

        void DebugLog(string text)
        {
            if (showDebugLogs)
            {
                Debug.Log("[Tweeter] " + text);
            }
        }

        #endregion

        void Update()
        {
            var progress = saver.preProcessProgress * 0.2f + saver.fileSaveProgress * 0.8f;
            HandleProgressBar(progress);

        }

        [DebugButton]
        void AddDefaultTweet(string tweet)
        {
            if (!tweet.ToLower().Contains(gameName.ToLower()))
            {
                tweet += " " + gameName;
            }
            defaultTextsForTweet.Add(tweet);
        }
    }
}