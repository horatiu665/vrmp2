using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using VRCore;
using VRUnicorns.SelfieStick;

public class SelfieStickRecycleSpawnBehaviour : VRCustomBehaviourBase
{
    private static List<IVRSpawnableEquippable> selfieSticks = new List<IVRSpawnableEquippable>();

    [SerializeField]
    private int _maxSelfieSticks = 4;

    [SerializeField]
    private SmartSound soundNotAllowedToSpawn;

    public override VRBehaviourCapabilities capabilities
    {
        get
        {
            return VRBehaviourCapabilities.Spawn;
        }
    }

    public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
    {
        // cannot spawn selfie stick if ANY hand already has one equipped (doesn't matter if spawned or from the ground)
        if (input.player.rightHand.GetEquipped(input.spawnable.prefabType) != null || input.player.leftHand.GetEquipped(input.spawnable.prefabType) != null)
        {
            output.stopExecution = true;
            FeedbackForCannotSpawn();
            return;    
        }

        for (int i = selfieSticks.Count - 1; i >= 0; i--)
        {
            // remove nulls
            if (selfieSticks[i] == null || selfieSticks[i].Equals(null) || !selfieSticks[i].gameObject.activeSelf)
            {
                selfieSticks.RemoveAt(i);
                continue;
            }

            // only returns non-equipped sticks if any
            if (!selfieSticks[i].isEquipped)
            {
                output.overrideSpawnable = selfieSticks[i];
                return;
            }
        }

        // if _MAX_ selfiesticks already exist, do not allow more to spawn - with some kind of feedback.
        if (selfieSticks.Count >= _maxSelfieSticks)
        {
            //////// COMPLETELY STOP EXECUTION
            output.stopExecution = true;
            FeedbackForCannotSpawn();
            return;
        }
        
        // add selfiestick to the list if it has not been added yet
        var equippable = (IVRSpawnableEquippable)input.spawnable;
        if (!selfieSticks.Contains(equippable))
        {
            selfieSticks.Add(equippable);
        }

    }

    private void FeedbackForCannotSpawn()
    {
        // sound or visuals for not allowing selfiestick to be spawned
        if (soundNotAllowedToSpawn != null)
        {
            soundNotAllowedToSpawn.Play();
        }
    }

    private void Reset()
    {
        _priority = 200;
    }
}