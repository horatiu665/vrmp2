using UnityEngine;
using System.Collections;
using UnityEngine.Audio;
using VRCore;
//using Atomtwist.SoundClip;
//using Kaae;

public class SelfieStickSounds : MonoBehaviour
{

    public int countdownLength = 3;
    public float snapShotLength = 3;
    public SmartSound countDownClip;
    public SmartSound recording;
    public SmartSound photoDone;
    public SmartSound recordingConfirm;

    [DebugButton]
    public void SnapshotWithCountdown()
    {
        StopAllSounds();
        StopAllCoroutines();
        StartCoroutine(CountDownLoop(countdownLength, snapShotLength));
    }

    [DebugButton]
    public void SnapshotWithCountdown(int countDown, float snapshotTime)
    {
        StopAllCoroutines();
        StartCoroutine(CountDownLoop(countDown, snapshotTime));
    }

    IEnumerator CountDownLoop(int countDown, float snapShot)
    {
        for (int i = 0; i < countDown; i++) {
            countDownClip.Play();
            yield return new WaitForSeconds(1);
        }

        recording.Play();
        yield return new WaitForSeconds(snapShot);
        recording.Stop();
        photoDone.Play();
        recordingConfirm.Play();
    }

    void StopAllSounds()
    {
        countDownClip.Stop();
        recording.Stop();
        photoDone.Stop();
        recordingConfirm.Stop();
    }
}
