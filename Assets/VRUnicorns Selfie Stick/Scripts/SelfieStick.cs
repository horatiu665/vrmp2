namespace VRUnicorns.SelfieStick
{
    using UnityEngine;
    using VRCore;
    using VRCore.VRNetwork;
    using Apex.Services;
    using System;
    using UnityEngine.EventSystems;

    public class SelfieStick : NetEquippableSpawnableBase, IHandleMessage<VRInputPressDown>, IHandleMessage<VRInputPressUp>, IHandleMessage<VRInputTouchUp>, IHandleMessage<VRInputTouch>
    {
        private SelfieStickRecordPreviewController controller;

        protected override void Awake()
        {
            base.Awake();
            controller = GetComponent<SelfieStickRecordPreviewController>();

        }

        protected override void OnEnable()
        {
            base.OnEnable();

            // gonna wait a frame so the other shit can initialize before calling this
            StartCoroutine(pTween.WaitFrames(1, () =>
            {
                // set render texture to next available from rendertexture manager
                if (SelfieStickRenderTextureManager.inst != null)
                {
                    var rt = SelfieStickRenderTextureManager.inst.GetNewRenderTexture();
                    controller.SetCameraRenderTexture(rt);
                    controller.ScrapRecordingAndShowCamera();
                }
            }));

            // check if there is an overlay and event system in the scene. if not, throw error.
            if (TwitterController.inst == null)
            {
                Debug.LogWarning("[SelfieStick] Warning: You probably forgot the #VRUnicorns Overlay!!! No TwitterController found. Tweets cannot be posted from SelfieStick without the overlay because of settings on the overlay itself.");
            }
            if (EventSystem.current == null)
            {
                Debug.LogWarning("[SelfieStick] Warning: no EventSystem found.");
            }

            GameServices.messageBus.Subscribe<VRInputPressUp>(this);
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputTouchUp>(this);
            GameServices.messageBus.Subscribe<VRInputTouch>(this);
        }

        void OnDisable()
        {
            GameServices.messageBus.Unsubscribe<VRInputPressUp>(this);
            GameServices.messageBus.Unsubscribe<VRInputPressDown>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouchUp>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouch>(this);
        }

        public void Handle(VRInputTouch message)
        {
            if (message.hand == _hand)
            {
                if (message.input == VRInput.Touchpad)
                {
                    var axis = message.hand.controller.GetAxis();
                    controller.OnTouchpadTouch(axis);
                }
            }
        }

        public void Handle(VRInputTouchUp message)
        {
            if (message.hand == _hand)
            {
                if (message.input == VRInput.Touchpad)
                {
                    controller.OnTouchpadTouchUp();
                }
            }
        }

        public void Handle(VRInputPressUp message)
        {
            if (message.hand == _hand)
            {
                if (message.input == VRInput.Touchpad)
                {
                    controller.OnTouchpadPressUp();
                }
            }
        }

        public void Handle(VRInputPressDown message)
        {
            if (message.hand == _hand)
            {
                if (message.input == VRInput.Touchpad)
                {
                    var axis = message.hand.controller.GetAxis();
                    controller.OnTouchpadPressDown(axis);
                }
            }
        }

        public void StartRecording()
        {
            controller.RecordAndPreviewOnSameScreen();
        }

        public void DeleteRecording()
        {
            controller.ScrapRecordingAndShowCamera();
        }

        // ///////////// debug buttons
        [DebugButton]
        void PretendWakeup()
        {
            var sb = GetComponent<SelfieStickStandbyMode>();
            sb.Grab();
            sb.Ungrab();

        }

        [DebugButton]
        void RecordAGif()
        {
            var rp = GetComponent<SelfieStickRecordPreviewController>();
            rp.RecordAndPreviewOnSameScreen();
        }

        [DebugButton]
        void TweetIt()
        {
            var t = GetComponent<SelfieStickTweeter>();
            t.OnTweetButtonClicked();

        }
    }
}