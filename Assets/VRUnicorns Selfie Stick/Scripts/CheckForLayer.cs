﻿using UnityEngine;
using System.Collections;

namespace VRUnicorns.SelfieStick
{

    public class CheckForLayer : MonoBehaviour
    {
        public string requiredLayer = "OnlyVisibleInSelfie";

        void Start()
        {
            if (LayerMask.LayerToName(gameObject.layer) != requiredLayer) {
                Debug.LogError(gameObject.name + " gameObject must be under "
                    + requiredLayer + " layer, but is currently under " + LayerMask.LayerToName(gameObject.layer)
                    + ". Remember to set camera layers too, after setting the layer", gameObject);
            }
        }
    }
}