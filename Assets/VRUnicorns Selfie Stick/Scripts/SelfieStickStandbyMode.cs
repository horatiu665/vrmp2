namespace VRUnicorns.SelfieStick
{

    using UnityEngine;
    using System.Collections;

    public class SelfieStickStandbyMode : MonoBehaviour
    {
        SelfieStick equippable;

        public enum StandbyState
        {
            Inactive,
            WaitingToBeActive,
            Active

        }
        public StandbyState state;
        float thrownTime;
        public float delayAfterThrow = 10f;

        bool isGrabbed = false;

        public void Grab()
        {
            isGrabbed = true;
            // grabbed by tool
            state = StandbyState.Inactive;
            SetStandby(false);
        }

        public void Ungrab()
        {
            isGrabbed = false;
            // thrown
            state = StandbyState.WaitingToBeActive;
            thrownTime = Time.time;

        }

        void Update()
        {
            if (equippable.isEquipped && !isGrabbed)
            {
                Grab();
            }
            else if (!equippable.isEquipped && isGrabbed)
            {
                Ungrab();
            }

            if (state == StandbyState.WaitingToBeActive)
            {
                if (Time.time - thrownTime >= delayAfterThrow && !IsRecordingOrTweeting())
                {
                    state = StandbyState.Active;
                    SetStandby(true);
                }
            }
        }

        public GameObject[] turnOff;
        public GameObject[] turnOn;

        void SetStandby(bool active)
        {
            // turn off camera and preview
            foreach (var g in turnOff)
            {
                g.SetActive(!active);
            }
            // turn on screen saver preview
            foreach (var g in turnOn)
            {
                g.SetActive(active);
            }
        }


        SelfieStickRecordPreviewController recordPreviewController;
        SelfieStickTweeter tweeter;

        void Awake()
        {
            equippable = GetComponentInParent<SelfieStick>();
            tweeter = GetComponent<SelfieStickTweeter>();
            recordPreviewController = GetComponent<SelfieStickRecordPreviewController>();

        }

        bool IsRecordingOrTweeting()
        {
            return recordPreviewController.Recording || tweeter.state == SelfieStickTweeter.TweeterStates.Tweeting;
        }

    }

}