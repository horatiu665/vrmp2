﻿using UnityEngine;
using System.Collections;

namespace VRUnicorns.SelfieStick
{

    public class SelfieStickProgressRotate : MonoBehaviour
    {

        public float speed = 135f;
        public bool rotate;


        void Update()
        {
            if (rotate) {
                transform.Rotate(Vector3.forward * speed * Time.deltaTime);
            }
        }
    }
}