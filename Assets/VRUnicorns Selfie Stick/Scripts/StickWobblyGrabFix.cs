﻿using UnityEngine;
using System.Collections;
using System;

namespace VRUnicorns.SelfieStick
{

    public class StickWobblyGrabFix : MonoBehaviour
    {

        public Transform flap;
        public float flapUpAngle = 45f;
        public float flapDuration = 0.2f;

        ConfigurableJoint joint;
        bool flapUp = false;

        // Use this for initialization
        void Start()
        {
            joint = GetComponent<ConfigurableJoint>();
        }

        // Update is called once per frame
        void Update()
        {
            if (joint.connectedBody != null) {
                if (!flapUp) {
                    SetFlap(true);
                }

            } else {
                if (flapUp)
                    SetFlap(false);

            }
        }

        private void SetFlap(bool flapUp)
        {
            this.flapUp = flapUp;
            var init = flapUp ? 0 : flapUpAngle;
            var final = flapUp ? flapUpAngle : 0;
            StartCoroutine(pTween.To(flapDuration, t => {
                flap.localEulerAngles = new Vector3(Mathf.SmoothStep(init, final, t), 0, 0);

            }));

        }

    }
}