using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SelfieStickRenderTextureManager : MonoBehaviour
{
    public static SelfieStickRenderTextureManager inst;
    void Awake()
    {
        inst = this;

    }

    public List<RenderTexture> _renderTextures = new List<RenderTexture>();

    int _lastRenderTexture = 0;

    public RenderTexture GetNewRenderTexture()
    {
        if (_lastRenderTexture >= _renderTextures.Count)
        {
            Debug.LogWarning("[SelfieStickRenderTextureManager] We have reached the max. amount of selfie sticks for this game");
            return null;
        }
        var rt = _renderTextures[_lastRenderTexture];
        _lastRenderTexture++;

        return rt;
    }

}