﻿using UnityEngine;
using System.Collections;

namespace VRUnicorns.SelfieStick
{

    public class SelfieStickBallSpawnerWhenActive : MonoBehaviour
    {
        //public ToolSpawnMulti ballSpawner;
        public Transform originalBallGrabber, stickBallGrabber;

        void Start()
        {

        }

        // Use this for initialization
        void BeingGrabbedByTool_Start()
        {
            if (!enabled) return;
            //originalBallGrabber = ballSpawner.grabPoint;

            //// set to new
            //ballSpawner.FullDeselectObj();
            //ballSpawner.SetGrabPoint(stickBallGrabber.GetComponent<ConfigurableJoint>());

        }

        void BeingGrabbedByTool_OnDestroy()
        {
            if (!enabled) return;
            // set to old
            //ballSpawner.SetGrabPoint(originalBallGrabber.GetComponent<ConfigurableJoint>());

        }

    }
}