﻿using UnityEngine;
using System.Collections;

namespace VRUnicorns.SelfieStick
{

    public class SetAlphaCutoffOnMaterial : MonoBehaviour
    {

        public bool useSharedMaterial = true;
        public Material mat;

        // Use this for initialization
        void Start()
        {
            if (mat == null) {
                mat = GetComponent<Renderer>().sharedMaterial;
            }
        }


        /// <summary>
        /// between 0 and 1.
        /// </summary>
        /// <param name="value"></param>
        public void SetAlphaCutoff(float value)
        {
            mat.SetFloat("_AlphaCutoff", Mathf.Clamp01(value));
        }
    }
}