using System;
using UnityEngine;
using UnityEngine.UI;
using VRUnicorns.SelfieStick;

public class CustomTweetHandling : MonoBehaviour
{
    public static CustomTweetHandling inst;

    InputField customTweet;
    public InputField CustomTweetInputField
    {
        get
        {
            return customTweet;
        }
    }
    public Text charCountText;
    [Header("Max characters allowed in the interface. Should be <103 due to gameName.Length() and GIF link")]
    public int maxCharacters = 55;
    public int redFontLimit = 0;

    Color regularColor;
    public Color redFontColor = Color.red;

    // Use this for initialization
    void Awake()
    {
        inst = this;
        customTweet = GetComponent<InputField>();
    }

    void Start()
    {
        regularColor = charCountText.color;
        UpdateCharacterCount("");
    }

    void OnEnable()
    {
        customTweet.onValueChanged.AddListener(CustomTweetChanged);

    }

    void OnDisable()
    {
        customTweet.onValueChanged.RemoveListener(CustomTweetChanged);

    }

    // Update is called once per frame
    void Update()
    {
        if (customTweet.isFocused)
        {
            // we are currently typing in the interface
            UpdateCharacterCount(customTweet.text);
        }
    }

    public bool preventTypingBelowLimit = false;

    string oldText;

    public void CustomTweetChanged(string newText)
    {
        if (preventTypingBelowLimit && GetRemainingCharacters(newText) < 0)
        {
            customTweet.text = oldText;
            // play BOOP sound
        }
        else
        {
            oldText = newText;
        }

    }

    private void UpdateCharacterCount(string currentText)
    {
        int remaining = GetRemainingCharacters(currentText);

        charCountText.text = remaining.ToString();
        if (remaining < redFontLimit)
        {
            charCountText.color = redFontColor;
        }
        else
        {
            charCountText.color = regularColor;
        }

    }

    private int GetRemainingCharacters(string status)
    {
        var chars = maxCharacters;
        //    // allow 13 more characters because we don't have to add #selfietennis / GameName
        var gn = SelfieStickTweeter.gameName;
        if (!status.ToLower().Contains(gn.ToLower()))
        {
            chars -= gn.Length;
        }

        var remaining = chars - status.Length;
        return remaining;
    }
}
