
using System;
using System.IO;
using System.Net;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using System.Text.RegularExpressions;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;

using UnityEngine;
using SimpleJSON;
using UnityEngine.Networking;
#if UNITY_5_3
//using UnityEngine.Experimental.Networking;
#endif
#if UNITY_5_4
using UnityEngine.Networking;
#endif

namespace Twitter
{
    public class RequestTokenResponse
    {
        public string Token { get; set; }
        public string TokenSecret { get; set; }
    }

    public class AccessTokenResponse
    {
        public string Token { get; set; }
        public string TokenSecret { get; set; }
        public string UserId { get; set; }
        public string ScreenName { get; set; }
    }

    public delegate void RequestTokenCallback(bool success, RequestTokenResponse response);
    public delegate void AccessTokenCallback(bool success, AccessTokenResponse response);
    public delegate void PostTweetCallback(bool success);
    public delegate void MediaUploadCallback(bool success, string mediaId);

    public class API
    {
#region OAuth Token Methods
        // 1. Get Request-Token From Twitter
        // 2. Get PIN from User
        // 3. Get Access-Token from Twitter
        // 4. Use Accss-Token for APIs requriring OAuth 
        // Accss-Token will be always valid until the user revokes the access to your application.

        // Twitter APIs for OAuth process
        private static readonly string RequestTokenURL = "https://api.twitter.com/oauth/request_token";
        private static readonly string AuthorizationURL = "https://api.twitter.com/oauth/authenticate?oauth_token={0}";
        private static readonly string AccessTokenURL = "https://api.twitter.com/oauth/access_token";

        public static IEnumerator GetRequestToken(string consumerKey, string consumerSecret, RequestTokenCallback callback)
        {
            WWW web = WWWRequestToken(consumerKey, consumerSecret);

            yield return web;

            if (!string.IsNullOrEmpty(web.error)) {
                Debug.Log(string.Format("GetRequestToken - failed. error : {0}", web.error));
                callback(false, null);
            } else {
                RequestTokenResponse response = new RequestTokenResponse {
                    Token = Regex.Match(web.text, @"oauth_token=([^&]+)").Groups[1].Value,
                    TokenSecret = Regex.Match(web.text, @"oauth_token_secret=([^&]+)").Groups[1].Value,
                };

                if (!string.IsNullOrEmpty(response.Token) &&
                    !string.IsNullOrEmpty(response.TokenSecret)) {
                    callback(true, response);
                } else {
                    Debug.Log(string.Format("GetRequestToken - failed. response : {0}", web.text));

                    callback(false, null);
                }
            }
        }

        public static void OpenAuthorizationPage(string requestToken)
        {
            Application.OpenURL(string.Format(AuthorizationURL, requestToken));
        }

        public static IEnumerator GetAccessToken(string consumerKey, string consumerSecret, string requestToken, string pin, AccessTokenCallback callback)
        {
            WWW web = WWWAccessToken(consumerKey, consumerSecret, requestToken, pin);

            yield return web;

            if (!string.IsNullOrEmpty(web.error)) {
                Debug.Log(string.Format("GetAccessToken - failed. error : {0}", web.error));
                callback(false, null);
            } else {
                AccessTokenResponse response = new AccessTokenResponse {
                    Token = Regex.Match(web.text, @"oauth_token=([^&]+)").Groups[1].Value,
                    TokenSecret = Regex.Match(web.text, @"oauth_token_secret=([^&]+)").Groups[1].Value,
                    UserId = Regex.Match(web.text, @"user_id=([^&]+)").Groups[1].Value,
                    ScreenName = Regex.Match(web.text, @"screen_name=([^&]+)").Groups[1].Value
                };

                if (!string.IsNullOrEmpty(response.Token) &&
                    !string.IsNullOrEmpty(response.TokenSecret) &&
                    !string.IsNullOrEmpty(response.UserId) &&
                    !string.IsNullOrEmpty(response.ScreenName)) {
                    callback(true, response);
                } else {
                    Debug.Log(string.Format("GetAccessToken - failed. response : {0}", web.text));

                    callback(false, null);
                }
            }
        }

        private static WWW WWWRequestToken(string consumerKey, string consumerSecret)
        {
            // Add data to the form to post.
            WWWForm form = new WWWForm();
            form.AddField("oauth_callback", "oob");

            // HTTP header
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            AddDefaultOAuthParams(parameters, consumerKey, consumerSecret);
            parameters.Add("oauth_callback", "oob");

            var headers = new Dictionary<string, string>();
            headers["Authorization"] = GetFinalOAuthHeader("POST", RequestTokenURL, parameters);

            return new WWW(RequestTokenURL, form.data, headers);
        }

        private static WWW WWWAccessToken(string consumerKey, string consumerSecret, string requestToken, string pin)
        {
            // Need to fill body since Unity doesn't like an empty request body.
            byte[] dummmy = new byte[1];
            dummmy[0] = 0;

            // HTTP header
            var headers = new Dictionary<string, string>();
            Dictionary<string, string> parameters = new Dictionary<string, string>();
            AddDefaultOAuthParams(parameters, consumerKey, consumerSecret);
            parameters.Add("oauth_token", requestToken);
            parameters.Add("oauth_verifier", pin);

            headers["Authorization"] = GetFinalOAuthHeader("POST", AccessTokenURL, parameters);

            return new WWW(AccessTokenURL, dummmy, headers);
        }

        private static string GetHeaderWithAccessToken(string httpRequestType, string apiURL, string consumerKey, string consumerSecret, AccessTokenResponse response, Dictionary<string, string> parameters)
        {
            AddDefaultOAuthParams(parameters, consumerKey, consumerSecret);

            parameters.Add("oauth_token", response.Token);
            parameters.Add("oauth_token_secret", response.TokenSecret);

            return GetFinalOAuthHeader(httpRequestType, apiURL, parameters);
        }

#endregion

#region Twitter API Methods

        private const string PostTweetURL = "https://api.twitter.com/1.1/statuses/update.json";

        public static IEnumerator PostTweet(string text, string consumerKey, string consumerSecret, AccessTokenResponse response, PostTweetCallback callback)
        {
            if (string.IsNullOrEmpty(text) || text.Length > 140) {
                Debug.Log(string.Format("PostTweet - text[{0}] is empty or too long.", text));

                callback(false);
            } else {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("status", text);

                // Add data to the form to post.
                WWWForm form = new WWWForm();
                form.AddField("status", text);

                // HTTP header
                var headers = new Dictionary<string, string>();
                headers["Authorization"] = GetHeaderWithAccessToken("POST", PostTweetURL, consumerKey, consumerSecret, response, parameters);

                WWW web = new WWW(PostTweetURL, form.data, headers);
                yield return web;

                if (!string.IsNullOrEmpty(web.error)) {
                    Debug.Log(string.Format("PostTweet - failed. {0}\n{1}", web.error, web.text));
                    callback(false);
                } else {
                    string error = Regex.Match(web.text, @"<error>([^&]+)</error>").Groups[1].Value;

                    if (!string.IsNullOrEmpty(error)) {
                        Debug.Log(string.Format("PostTweet - failed. {0}", error));
                        callback(false);
                    } else {
                        callback(true);
                    }
                }
            }
        }

        // http://stackoverflow.com/questions/29024050/c-upload-photo-to-twitter-from-unity
        private static string EscapeString(string originalString)
        {
            int limit = 2000;

            StringBuilder sb = new StringBuilder();
            int loops = originalString.Length / limit;

            for (int i = 0; i <= loops; i++) {
                if (i < loops) {
                    sb.Append(Uri.EscapeDataString(originalString.Substring(limit * i, limit)));
                } else {
                    sb.Append(Uri.EscapeDataString(originalString.Substring(limit * i)));
                }
            }
            return sb.ToString();
        }

        // http://stackoverflow.com/questions/29024050/c-upload-photo-to-twitter-from-unity
        private const string UploadMediaURL = "https://upload.twitter.com/1.1/media/upload.json";//"https://localhost/unityhack/index.php";

        // hiding some code here
        void NotGoodShit()
        {
            //var n = 1000;
            //var index = 0;
            //string encoded64ImageData = "";
            //byte[] chunk = new byte[n * 64];
            //while (index < imageData.Length) {
            //    for (int i = 0; i < chunk.Length; i++) {
            //        if (index + i < imageData.Length) {
            //            chunk[i] = imageData[index + i];
            //        }
            //    }
            //    encoded64ImageData += ImageToBase64(chunk);
            //    index += n * 64;
            //    //yield return 0;
            //}





            //Debug.Log("adding encoded string of " + encoded64ImageData.Length + " chars into parameters dictionary");
            //var step = 10000;
            //index = 0;
            //while (index < encoded64ImageData.Length) {
            //    parameters["media_data"] += encoded64ImageData.Substring(index, Mathf.Min(step, encoded64ImageData.Length - index - 1));
            //    index += step;
            //    yield return 0;
            //}

        }

        public static event Action OnStartUploadingMedia;

        // GOES TO TweetGifCallback() WHICH SENDS A TWEET POST WITH THE MEDIA_ID FROM THIS REQUEST
        public static IEnumerator UploadMedia(byte[] imageData, string consumerKey, string consumerSecret, AccessTokenResponse response, MediaUploadCallback callback)
        {

            WWW web = null;

            Debug.Log("Started Thread");

            var timeAtStart = DateTime.Now;
            Debug.Log("timeAtStart " + DateTime.Now);


            Dictionary<string, string> parameters = new Dictionary<string, string>();

            // split imageData into small chunks of 64n bytes, where n is not too big. convert each and concat
            string encoded64ImageData = ImageToBase64(imageData);
            var timeAfterEncodedData = DateTime.Now;
            Debug.Log("timeAfterEncodedData " + timeAfterEncodedData.Subtract(timeAtStart));


            //string encoded64ImageData = ImageToBase64(imageData);
            parameters.Add("media_data", encoded64ImageData);
            var timeAfterAddedParams = DateTime.Now;
            Debug.Log("timeAfterAddedParams " + timeAfterAddedParams.Subtract(timeAfterEncodedData));

            // Add data to the form to post.
            WWWForm form = new WWWForm();
            form.AddField("media_data", encoded64ImageData);
            var timeAfterAddedFieldForm = DateTime.Now;
            Debug.Log("timeAfterAddedParams " + timeAfterAddedFieldForm.Subtract(timeAfterAddedParams));

            // HTTP header
            Dictionary<string, string> headers = new Dictionary<string, string>();
            string url = UploadMediaURL;
            string auth = GetHeaderWithAccessToken("POST", url, consumerKey, consumerSecret, response, parameters);
            headers["Authorization"] = auth;
            headers["Content-Transfer-Encoding"] = "base64";

            var timeAfterGetHeaders = DateTime.Now;
            Debug.Log("timeAfterGetHeaders " + timeAfterGetHeaders.Subtract(timeAfterAddedFieldForm));

            web = new WWW(url, form.data, headers);

            var timeAfterWWW = DateTime.Now;
            Debug.Log("timeAfterWWW " + timeAfterWWW.Subtract(timeAfterGetHeaders));

            Debug.Log("Sending WWW request. Waiting for reply");

            if (OnStartUploadingMedia != null) {
                OnStartUploadingMedia();
            }

            yield return web;

            // use web (the response from the twitter upload site) to get the media id/url to use in the tweet
            Debug.Log("Posted!!!!");
            Debug.Log("Errors: " + web.error);
            Debug.Log("Text: " + web.text);

            if (!string.IsNullOrEmpty(web.error)) {
                Debug.Log(string.Format("PostTweet - failed. {0}\n{1}", web.error, web.text));
                callback(false, "");
            } else {
                string error = Regex.Match(web.text, @"<error>([^&]+)</error>").Groups[1].Value;

                if (!string.IsNullOrEmpty(error)) {
                    Debug.Log(string.Format("PostTweet - failed. {0}", error));
                    callback(false, "");
                } else {
                    var json = JSON.Parse(web.text);
                    if (json != null) {
                        var mediaId = json["media_id_string"];
                        if (mediaId != null) {
                            callback(true, mediaId);
                        }
                    } else {
                        callback(false, "");
                    }
                }
            }

            /*
            response in the format: 
            Text: 
            {
                "media_id":712855190093869056,
                "media_id_string":"712855190093869056",
                "size":38699,
                "expires_after_secs":86400,
                "image":
                {
                    "image_type":"image\/gif",
                    "w":279,
                    "h":328
                }
            }
    */


        }


        public static UnityWebRequest SetupUnityWebRequest_Post(UnityWebRequest requestWithURIAndPost, string urlEncodedData)
        {
            //string urlencoded = WWWTranscoder.URLEncode(postData, System.Text.Encoding.UTF8);

            requestWithURIAndPost.uploadHandler = new UploadHandlerRaw(System.Text.Encoding.UTF8.GetBytes(urlEncodedData));
            requestWithURIAndPost.uploadHandler.contentType = "application/x-www-form-urlencoded";
            requestWithURIAndPost.downloadHandler = new DownloadHandlerBuffer();

            return requestWithURIAndPost;
        }

        static DateTime lastCall = DateTime.Now;
        static TimeSpan TimeSinceLastCall()
        {
            var tempLastCall = lastCall;
            lastCall = DateTime.Now;
            return DateTime.Now.Subtract(tempLastCall);
        }

        // GOES TO TwitterDemo.TweetGifCallback() WHICH SENDS A TWEET POST WITH THE MEDIA_ID FROM THIS REQUEST
        public static IEnumerator UploadMediaNew(byte[] imageData, string consumerKey, string consumerSecret, AccessTokenResponse response, MediaUploadCallback callback, bool debugLog = false)
        {
            string url = UploadMediaURL;

            Dictionary<string, string> parameters = new Dictionary<string, string>();

            string encoded64ImageData = "";

            if (debugLog) {
                Debug.Log("Started UploadMediaNew thread for encoding image data");
                TimeSinceLastCall();
            }
            bool threadDone = false;
            new System.Threading.Thread(() => {
                encoded64ImageData = ImageToBase64(imageData);
                threadDone = true;
            }).Start();

            while (!threadDone) {
                yield return 0;
            }

#region //encoding in chunks and using yield statements
            //var n = 1000;
            //var index = 0;
            //string encoded64ImageData = "";
            //byte[] chunk = new byte[n * 64];
            //while (index < imageData.Length) {
            //    for (int i = 0; i < chunk.Length; i++) {
            //        if (index + i < imageData.Length) {
            //            chunk[i] = imageData[index + i];
            //        }
            //    }
            //    encoded64ImageData += ImageToBase64(chunk);
            //    index += n * 64;
            //    yield return 0;
            //}
#endregion

            if (debugLog) {
                Debug.Log("Encoded byte[] imageData in " + TimeSinceLastCall());
            }
            parameters.Add("media_data", encoded64ImageData);

            // Add data to the form to post.
            //WWWForm form = new WWWForm();
            //form.AddField("media_data", encoded64ImageData);
            //List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
            //formData.Add(new MultipartFormDataSection("media_data", encoded64ImageData));

            Dictionary<string, string> headers = new Dictionary<string, string>();
            UnityWebRequest webReq = new UnityWebRequest(url, UnityWebRequest.kHttpVerbPOST);
            string urlencodedData = "";
            threadDone = false;
            new System.Threading.Thread(() => {
                // HTTP header
                string auth = GetHeaderWithAccessToken("POST", url, consumerKey, consumerSecret, response, parameters);
                headers["Authorization"] = auth;
                headers["Content-Transfer-Encoding"] = "base64";

                string postData = UrlEncode("media_data") + "=" + UrlEncode(encoded64ImageData);
                urlencodedData = postData;

                threadDone = true;

            }).Start();

            while (!threadDone) {
                yield return 0;
            }

            if (debugLog) {
                Debug.Log("Created headers and UrlEncoded encoded64ImageData in "
                    + TimeSinceLastCall() + ". Starting setup of UnityWebRequest");
            }
            webReq = SetupUnityWebRequest_Post(webReq, urlencodedData);

            foreach (var h in headers) {
                webReq.SetRequestHeader(h.Key, h.Value);
            }

            if (debugLog) {
                Debug.Log("Finished setup of UnityWebRequest in " + TimeSinceLastCall() + ". Starting webRequest.Send()");
            }

            if (OnStartUploadingMedia != null) {
                OnStartUploadingMedia();
            }

            yield return webReq.Send();

            if (debugLog) {
                Debug.Log("Finished webReq.Send() (a.k.a. received response) after " + TimeSinceLastCall());
                Debug.Log("Errors: " + webReq.error);
                Debug.Log("Text: " + webReq.downloadHandler.text);
            }

            // use web (the response from the twitter upload site) to get the media id/url to use in the tweet
            if (webReq.isNetworkError) {
                if (debugLog) {
                    Debug.Log(string.Format("PostTweet - failed. {0}\n{1}", webReq.responseCode, webReq.error));
                }
                callback(false, "");
            } else {
                var json = JSON.Parse(webReq.downloadHandler.text);
                if (json != null) {
                    var mediaId = json["media_id_string"];
                    if (mediaId != null) {
                        callback(true, mediaId);
                    } else {
                        if (debugLog) {
                            Debug.Log("PostTweet - failed. Received json but not mediaId");
                        }
                        callback(false, "");
                    }
                } else {
                    callback(false, "");
                }
            }

#region // formatted response example
            /*
            response in the format: 
            Text: 
            {
                "media_id":712855190093869056,
                "media_id_string":"712855190093869056",
                "size":38699,
                "expires_after_secs":86400,
                "image":
                {
                    "image_type":"image\/gif",
                    "w":279,
                    "h":328
                }
            }
            */
#endregion

        }

        public static string ImageToBase64(byte[] imageData)
        {
            // encode to base 64
            return Convert.ToBase64String(imageData);

        }

        //public void AddField(string fieldName, string value, [uei.DefaultValue("System.Text.Encoding.UTF8")]  Encoding e)
        //{
        //    fieldNames.Add(fieldName);
        //    fileNames.Add(null);
        //    formData.Add(e.GetBytes(value));
        //    types.Add("text/plain; charset=\"" + e.WebName + "\"");
        //}

        public static IEnumerator PostTweetWithMedia(string text, string media_id, string consumerKey, string consumerSecret, AccessTokenResponse response, PostTweetCallback callback, bool debugLog = false)
        {
            if (string.IsNullOrEmpty(text) || text.Length > 140) {
                if (debugLog) {
                    Debug.Log(string.Format("PostTweet - text[{0}] is empty or too long.", text));
                }
                callback(false);
            } else {
                Dictionary<string, string> parameters = new Dictionary<string, string>();
                parameters.Add("status", text);
                parameters.Add("media_ids", media_id);

                // Add data to the form to post.
                WWWForm form = new WWWForm();
                form.AddField("status", text);
                form.AddField("media_ids", media_id);

                // HTTP header
                var headers = new Dictionary<string, string>();
                headers["Authorization"] = GetHeaderWithAccessToken("POST", PostTweetURL, consumerKey, consumerSecret, response, parameters);

                WWW web = new WWW(PostTweetURL, form.data, headers);
                yield return web;

                if (!string.IsNullOrEmpty(web.error)) {
                    if (debugLog) Debug.Log(string.Format("PostTweet with media - failed. {0}\n{1}", web.error, web.text));
                    callback(false);
                } else {
                    string error = Regex.Match(web.text, @"<error>([^&]+)</error>").Groups[1].Value;

                    if (!string.IsNullOrEmpty(error)) {
                        if (debugLog) Debug.Log(string.Format("PostTweet with media - failed. {0}", error));
                        callback(false);
                    } else {
                        callback(true);
                    }
                }
            }
        }

#endregion

#region OAuth Help Methods
        // The below help methods are modified from "WebRequestBuilder.cs" in Twitterizer(http://www.twitterizer.net/).
        // Here is its license.

        //-----------------------------------------------------------------------
        // <copyright file="WebRequestBuilder.cs" company="Patrick 'Ricky' Smith">
        //  This file is part of the Twitterizer library (http://www.twitterizer.net/)
        // 
        //  Copyright (c) 2010, Patrick "Ricky" Smith (ricky@digitally-born.com)
        //  All rights reserved.
        //  
        //  Redistribution and use in source and binary forms, with or without modification, are 
        //  permitted provided that the following conditions are met:
        // 
        //  - Redistributions of source code must retain the above copyright notice, this list 
        //    of conditions and the following disclaimer.
        //  - Redistributions in binary form must reproduce the above copyright notice, this list 
        //    of conditions and the following disclaimer in the documentation and/or other 
        //    materials provided with the distribution.
        //  - Neither the name of the Twitterizer nor the names of its contributors may be 
        //    used to endorse or promote products derived from this software without specific 
        //    prior written permission.
        // 
        //  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
        //  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
        //  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. 
        //  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, 
        //  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT 
        //  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR 
        //  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, 
        //  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
        //  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE 
        //  POSSIBILITY OF SUCH DAMAGE.
        // </copyright>
        // <author>Ricky Smith</author>
        // <summary>Provides the means of preparing and executing Anonymous and OAuth signed web requests.</summary>
        //-----------------------------------------------------------------------

        private static readonly string[] OAuthParametersToIncludeInHeader = new[]
                                                          {
                                                              "oauth_version",
                                                              "oauth_nonce",
                                                              "oauth_timestamp",
                                                              "oauth_signature_method",
                                                              "oauth_consumer_key",
                                                              "oauth_token",
                                                              "oauth_verifier"
                                                              // Leave signature omitted from the list, it is added manually
                                                              // "oauth_signature",
                                                          };

        private static readonly string[] SecretParameters = new[]
                                                                {
                                                                    "oauth_consumer_secret",
                                                                    "oauth_token_secret",
                                                                    "oauth_signature"
                                                                };

        private static void AddDefaultOAuthParams(Dictionary<string, string> parameters, string consumerKey, string consumerSecret)
        {
            parameters.Add("oauth_version", "1.0");
            parameters.Add("oauth_nonce", GenerateNonce());
            parameters.Add("oauth_timestamp", GenerateTimeStamp());
            parameters.Add("oauth_signature_method", "HMAC-SHA1");
            parameters.Add("oauth_consumer_key", consumerKey);
            parameters.Add("oauth_consumer_secret", consumerSecret);
        }

        private static string GetFinalOAuthHeader(string HTTPRequestType, string URL, Dictionary<string, string> parameters)
        {
            // Add the signature to the oauth parameters
            string signature = GenerateSignature(HTTPRequestType, URL, parameters);

            parameters.Add("oauth_signature", signature);

            StringBuilder authHeaderBuilder = new StringBuilder();
            authHeaderBuilder.AppendFormat("OAuth realm=\"{0}\"", "Twitter API");

            var sortedParameters = from p in parameters
                                   where OAuthParametersToIncludeInHeader.Contains(p.Key)
                                   orderby p.Key, UrlEncode(p.Value)
                                   select p;

            foreach (var item in sortedParameters) {
                authHeaderBuilder.AppendFormat(",{0}=\"{1}\"", UrlEncode(item.Key), UrlEncode(item.Value));
            }

            authHeaderBuilder.AppendFormat(",oauth_signature=\"{0}\"", UrlEncode(parameters["oauth_signature"]));

            return authHeaderBuilder.ToString();
        }

        private static string GenerateSignature(string httpMethod, string url, Dictionary<string, string> parameters)
        {
            var nonSecretParameters = (from p in parameters
                                       where !SecretParameters.Contains(p.Key)
                                       select p);

            // Create the base string. This is the string that will be hashed for the signature.
            string signatureBaseString = string.Format(CultureInfo.InvariantCulture,
                                                       "{0}&{1}&{2}",
                                                       httpMethod,
                                                       UrlEncode(NormalizeUrl(new Uri(url))),
                                                       UrlEncode(nonSecretParameters));

            // Create our hash key (you might say this is a password)
            string key = string.Format(CultureInfo.InvariantCulture,
                                       "{0}&{1}",
                                       UrlEncode(parameters["oauth_consumer_secret"]),
                                       parameters.ContainsKey("oauth_token_secret") ? UrlEncode(parameters["oauth_token_secret"]) : string.Empty);


            // Generate the hash
            HMACSHA1 hmacsha1 = new HMACSHA1(Encoding.ASCII.GetBytes(key));
            byte[] signatureBytes = hmacsha1.ComputeHash(Encoding.ASCII.GetBytes(signatureBaseString));
            return Convert.ToBase64String(signatureBytes);
        }

        private static string GenerateTimeStamp()
        {
            // Default implementation of UNIX time of the current UTC time
            TimeSpan ts = DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0);
            return Convert.ToInt64(ts.TotalSeconds, CultureInfo.CurrentCulture).ToString(CultureInfo.CurrentCulture);
        }

        private static string GenerateNonce()
        {
            // Just a simple implementation of a random number between 123400 and 9999999
            return new System.Random().Next(123400, int.MaxValue).ToString("X", CultureInfo.InvariantCulture);
        }

        private static string NormalizeUrl(Uri url)
        {
            string normalizedUrl = string.Format(CultureInfo.InvariantCulture, "{0}://{1}", url.Scheme, url.Host);
            if (!((url.Scheme == "http" && url.Port == 80) || (url.Scheme == "https" && url.Port == 443))) {
                normalizedUrl += ":" + url.Port;
            }

            normalizedUrl += url.AbsolutePath;
            return normalizedUrl;
        }

        private static string UrlEncode(string value)
        {
            if (string.IsNullOrEmpty(value)) {
                return string.Empty;
            }

            // replaced Uri.EscapeDataString with the EscapeString function because there was a limit of characters that it allowed to encode
            value = EscapeString(value);

            // UrlEncode escapes with lowercase characters (e.g. %2f) but oAuth needs %2F
            value = Regex.Replace(value, "(%[0-9a-f][0-9a-f])", c => c.Value.ToUpper());

            // these characters are not escaped by UrlEncode() but needed to be escaped
            value = value
                .Replace("(", "%28")
                .Replace(")", "%29")
                .Replace("$", "%24")
                .Replace("!", "%21")
                .Replace("*", "%2A")
                .Replace("'", "%27");

            // these characters are escaped by UrlEncode() but will fail if unescaped!
            value = value.Replace("%7E", "~");

            return value;
        }

        private static string UrlEncode(IEnumerable<KeyValuePair<string, string>> parameters)
        {
            StringBuilder parameterString = new StringBuilder();

            var paramsSorted = from p in parameters
                               orderby p.Key, p.Value
                               select p;

            foreach (var item in paramsSorted) {
                if (parameterString.Length > 0) {
                    parameterString.Append("&");
                }

                parameterString.Append(
                    string.Format(
                        CultureInfo.InvariantCulture,
                        "{0}={1}",
                        UrlEncode(item.Key),
                        UrlEncode(item.Value)));
            }

            return UrlEncode(parameterString.ToString());
        }

#endregion
    }
}