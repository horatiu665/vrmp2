using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class TwitterUsernameText : MonoBehaviour 
{
    public static TwitterUsernameText inst;
    void Awake()
    {
        inst = this;
        text = GetComponent<Text>();
    }

    public Text text { get; private set; }

    public void SetUsernameText(string username)
    {
        text.text = username;
    }

}