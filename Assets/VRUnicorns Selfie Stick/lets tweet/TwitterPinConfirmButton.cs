namespace VRUnicorns.SelfieStick
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;

    public class TwitterPinConfirmButton : MonoBehaviour
    {
        private Button _button;

        public InputField pinInput;
        
        void Awake()
        {
            _button = GetComponent<Button>();
        }

        void OnEnable()
        {
            _button.onClick.AddListener(OnClick);
        }

        void OnDisable()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            TwitterController.inst.InterfaceLoginUsingPin(pinInput);

        }

    }

}