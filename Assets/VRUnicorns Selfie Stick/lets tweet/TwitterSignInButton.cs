namespace VRUnicorns.SelfieStick
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using UnityEngine.UI;

    public class TwitterSignInButton : MonoBehaviour
    {
        private Button _button;

        public GameObject enterPinPanel;

        public GameObject usernameText;

        public InputField enterPinInputField;

        void Awake()
        {
            _button = GetComponent<Button>();
        }

        void OnEnable()
        {
            _button.onClick.AddListener(OnClick);
        }

        void OnDisable()
        {
            _button.onClick.RemoveListener(OnClick);
        }

        private void OnClick()
        {
            TwitterController.inst.GoToTwitterForLogInCode();
            // enable enter pin panel
            enterPinPanel.SetActive(true);

            // disable username text
            usernameText.SetActive(false);

            // set pin text to nothing
            enterPinInputField.text = "";

        }

    }

}