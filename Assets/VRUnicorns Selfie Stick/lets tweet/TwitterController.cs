using UnityEngine;
using System.Collections;
using System;
using UnityEngine.UI;

namespace VRUnicorns.SelfieStick
{
    public class TwitterController : MonoBehaviour
    {
        public static TwitterController inst;

        // You need to save access token and secret for later use.
        // You can keep using them whenever you need to access the user's Twitter account. 
        // They will be always valid until the user revokes the access to your application.
        const string PLAYER_PREFS_TWITTER_USER_ID = "TwitterUserID";
        const string PLAYER_PREFS_TWITTER_USER_SCREEN_NAME = "TwitterUserScreenName";
        const string PLAYER_PREFS_TWITTER_USER_TOKEN = "TwitterUserToken";
        const string PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET = "TwitterUserTokenSecret";

        // You need to register your game or application in Twitter to get cosumer key and secret.
        // Go to this page for registration: http://dev.twitter.com/apps/new
        public string CONSUMER_KEY;
        public string CONSUMER_SECRET;

        public string LOGIN_ERROR_MESSAGE = "wrong PIN";

        Twitter.RequestTokenResponse m_RequestTokenResponse;
        Twitter.AccessTokenResponse m_AccessTokenResponse;

        public bool showTweetUploadDebug = false;

        string textForTweet;

        /// <summary>
        /// was tweet posting procedure completed successfully? or unsuccessfully?
        /// </summary>
        public event Action<bool> TweetPosted;
        public event Action<bool> TweetMediaUploadPosted;

        void Awake()
        {
            inst = this;

        }

        // Use this for initialization
        void Start()
        {
            LoadTwitterUserInfo();
        }

        #region Login stuff!!!

        void LoadTwitterUserInfo()
        {
            m_AccessTokenResponse = new Twitter.AccessTokenResponse();

            m_AccessTokenResponse.UserId = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_ID);
            m_AccessTokenResponse.ScreenName = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME);
            m_AccessTokenResponse.Token = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN);
            m_AccessTokenResponse.TokenSecret = PlayerPrefs.GetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET);

            if (!string.IsNullOrEmpty(m_AccessTokenResponse.Token) &&
                !string.IsNullOrEmpty(m_AccessTokenResponse.ScreenName) &&
                !string.IsNullOrEmpty(m_AccessTokenResponse.Token) &&
                !string.IsNullOrEmpty(m_AccessTokenResponse.TokenSecret))
            {
                if (showTweetUploadDebug)
                {
                    string log = "LoadTwitterUserInfo - succeeded";
                    log += "\n    UserId : " + m_AccessTokenResponse.UserId;
                    log += "\n    ScreenName : " + m_AccessTokenResponse.ScreenName;
                    log += "\n    Token : " + m_AccessTokenResponse.Token;
                    log += "\n    TokenSecret : " + m_AccessTokenResponse.TokenSecret;
                    print(log);

                }
            }

            if (IsLoggedIn())
            {
                // put username on the interface
                SetUsername(m_AccessTokenResponse.ScreenName);
            }
            else
            {
                TwitterEnterPinPanel.inst.gameObject.SetActive(false);
            }

        }

        public bool IsLoggedIn()
        {
            return !string.IsNullOrEmpty(m_AccessTokenResponse.ScreenName);
        }

        // called when logging in on any account
        public void GoToTwitterForLogInCode()
        {
            StartCoroutine(Twitter.API.GetRequestToken(CONSUMER_KEY, CONSUMER_SECRET,
                new Twitter.RequestTokenCallback(this.OnRequestTokenCallback)));

        }

        void OnRequestTokenCallback(bool success, Twitter.RequestTokenResponse response)
        {
            if (success)
            {

                if (showTweetUploadDebug)
                {
                    string log = "OnRequestTokenCallback - succeeded";
                    log += "\n    Token : " + response.Token;
                    log += "\n    TokenSecret : " + response.TokenSecret;
                    print(log);
                }
                m_RequestTokenResponse = response;

                Twitter.API.OpenAuthorizationPage(response.Token);
            }
            else
            {
                if (showTweetUploadDebug)
                    print("OnRequestTokenCallback - failed.");
            }
        }

        public void InterfaceLoginUsingPin(InputField pinInput)
        {
            if (pinInput != null)
                LogInUsingPinFromTwitter(pinInput.text);
            else
                Debug.LogError("TwitterController: PinInput cannot be found! Cannot log in");
        }

        public void LogInUsingPinFromTwitter(string pin)
        {
            StartCoroutine(Twitter.API.GetAccessToken(CONSUMER_KEY, CONSUMER_SECRET, m_RequestTokenResponse.Token, pin,
                new Twitter.AccessTokenCallback(this.OnAccessTokenCallback)));

        }

        void OnAccessTokenCallback(bool success, Twitter.AccessTokenResponse response)
        {
            if (success)
            {
                string log = "OnAccessTokenCallback - succeeded";
                log += "\n    UserId : " + response.UserId;
                log += "\n    ScreenName : " + response.ScreenName;
                log += "\n    Token : " + response.Token;
                log += "\n    TokenSecret : " + response.TokenSecret;
                if (showTweetUploadDebug)
                {
                    print(log);
                }
                m_AccessTokenResponse = response;

                PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_ID, response.UserId);
                PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_SCREEN_NAME, response.ScreenName);
                PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_TOKEN, response.Token);
                PlayerPrefs.SetString(PLAYER_PREFS_TWITTER_USER_TOKEN_SECRET, response.TokenSecret);

                // put username on the button
                SetUsername(response.ScreenName);

            }
            else
            {
                if (showTweetUploadDebug)
                    print("OnAccessTokenCallback - failed.");
                // put "/fail" on the button
                SetUsername(LOGIN_ERROR_MESSAGE);

            }
        }

        public void SetUsername(string username)
        {
            if (TwitterUsernameText.inst != null && TwitterUsernameText.inst.text != null)
            {
                if (username == LOGIN_ERROR_MESSAGE)
                {
                    TwitterUsernameText.inst.text.text = LOGIN_ERROR_MESSAGE;
                }
                else
                {
                    TwitterUsernameText.inst.text.text = "@" + username;
                }

                // set which set of buttons/interface elems should be active.
                TwitterUsernameText.inst.text.gameObject.SetActive(true);
                TwitterEnterPinPanel.inst.gameObject.SetActive(false);

            }
            else
            {
                Debug.LogError("TwitterController: UsernameText not set, cannot set username!");
            }
        }

        #endregion

        #region Tweeting stuff!!!

        /// <summary>
        /// Tweets out the file at filePath with the status textForTweet with an embedded media link.
        /// Fires events when failed for various reasons.
        /// </summary>
        /// <param name="filePath">If this does not exist, does not tweet and fails</param>
        /// <param name="textForTweet">This can be anything except empty string in which case it fails</param>
        public void TweetGif(string filePath, string textForTweet)
        {
            this.textForTweet = textForTweet;

            if (System.IO.File.Exists(filePath))
            {

                if (showTweetUploadDebug)
                {
                    print("Just starting to read GIF from file at " + filePath);
                }
                var startReadingGifTime = DateTime.Now;

                // PERHAPS CONSIDER DOING THIS IN A THREAD???
                // load img from path
                byte[] imageData = System.IO.File.ReadAllBytes(filePath);

                if (showTweetUploadDebug)
                {
                    print("Finished reading GIF after " + DateTime.Now.Subtract(startReadingGifTime).ToString() + ". Now starting to upload media");
                }

                Twitter.AccessTokenResponse response = m_AccessTokenResponse;
                StartCoroutine(Twitter.API.UploadMediaNew(imageData,
                    CONSUMER_KEY, CONSUMER_SECRET, response,
                    TweetGifCallback, showTweetUploadDebug));

            }
            else
            {
                if (showTweetUploadDebug)
                    print("TweetGif error: File \"" + filePath + "\" does not exist!");

                TweetGifCallback(false, "");
            }
        }

        /// <summary>
        /// callback from MediaUpload 
        /// </summary>
        /// <param name="success">successful tweet? (media upload)</param>
        /// <param name="mediaId">media ID returned by twitter API</param>
        private void TweetGifCallback(bool success, string mediaId)
        {
            OnPostMediaForTweet(success);
            if (success)
            {
                // GOES TO SelfieStickTweeter.OnTweetPosted (bool success)
                StartCoroutine(Twitter.API.PostTweetWithMedia(textForTweet, mediaId,
                    CONSUMER_KEY, CONSUMER_SECRET, m_AccessTokenResponse,
                    OnPostTweet, showTweetUploadDebug));
            }
        }

        void OnPostMediaForTweet(bool success)
        {
            if (showTweetUploadDebug)
                print("Tweet Media/upload post - " + (success ? "succedded." : "failed."));
            if (TweetMediaUploadPosted != null)
            {
                TweetMediaUploadPosted(success);
            }
        }

        void OnPostTweet(bool success)
        {
            if (showTweetUploadDebug)
                print("OnPostTweet - " + (success ? "succedded." : "failed."));
            if (TweetPosted != null)
            {
                TweetPosted(success);
            }
        }

        #endregion

    }
}