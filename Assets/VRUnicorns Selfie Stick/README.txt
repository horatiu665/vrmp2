VRUnicorns Selfie Stick
====
by Horatiu Roman / VRUnicorns (http://www.vrunicorns.com)

Thanks for checking out this asset package.

The package is based on:
- Let's Tweet (http://u3d.as/1Dq) which implements OAuth and a simple POST request for posting tweets using the "POST statuses/update" Twitter API
- Gif Creator by PygmyMonkey (http://u3d.as/icC) which offers a solution for recording GIF images in the Unity Editor

Changes to those packages are:
- added the ability to post media to Twitter
- implemented async framedrop-free methods for posting tweets, for use in runtime VR scenarios, using the UnityEngine.Experimental.Networking systems (in 5.3; UnityEngine.Networking in 5.4b)
- implemented runtime GIF saving functionality, framedrop-free. Uses coroutines and threads. Jammed over ~48 hours, therefore there is room for improvement

Further implementation includes:
- GIF previewing functionality connected to the recording functionality (can be used with generic images as well)
- Wrapper for triggering a gif-recording and previewing cycle, and a tweeting cycle, with callback methods for success/failure. Must be hooked up with user input. Designed for motion-tracked VR
- customizable twitter log-in user interface
- selfie stick model and functionality aimed for use in VR
- optional usage in regular games, by ripping apart the selfie stick and taking the camera and functionality parts (needs tweaking and more custom implementation for good results)

For questions, write to play at vrunicorns dot com