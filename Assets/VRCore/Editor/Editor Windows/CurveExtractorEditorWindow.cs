namespace VRCore.Editor
{
    using System.Text;
    using UnityEditor;
    using UnityEngine;

    public sealed class CurveExtractorEditorWindow : EditorWindow
    {
        private static string _csharpCurve;
        private static Vector2 _scroll;

        [MenuItem("Build/Curve Extractor Window", false, 99)]
        public static void OpenWindow()
        {
            GetWindow<CurveExtractorEditorWindow>("Curve Extractor");
        }

        public static void OpenWindow(AnimationCurve curve)
        {
            _csharpCurve = ToCode(curve);
            OpenWindow();
        }

        private void OnGUI()
        {
            _scroll = EditorGUILayout.BeginScrollView(_scroll);

            EditorGUILayout.Separator();

            EditorGUILayout.LabelField("C# (Read Only)", EditorStyles.centeredGreyMiniLabel);
            EditorGUILayout.TextArea(_csharpCurve);

            if (GUILayout.Button("Copy to Clipboard"))
            {
                EditorGUIUtility.systemCopyBuffer = _csharpCurve;
            }

            EditorGUILayout.Separator();

            GUILayout.Box("Click the 'C' button next to any AnimationCurve to generate the C# code to represent it in this window.", GUILayout.Width(this.position.width - 10f));

            EditorGUILayout.Separator();

            EditorGUILayout.EndScrollView();
        }

        private static string ToCode(AnimationCurve curve)
        {
            var sb = new StringBuilder();
            sb.AppendLine("AnimationCurve curve = new AnimationCurve(");
            sb.AppendLine("    new Keyframe[]");
            sb.AppendLine("    {");

            foreach (var key in curve.keys)
            {
                var keyStr = string.Concat("new Keyframe(", key.time.ToString(), "f, ", key.value.ToString(), "f, ", key.inTangent.ToString(), "f, ", key.outTangent.ToString(), "f),");
                sb.AppendLine("        " + keyStr);
            }

            sb.AppendLine("    }");
            sb.AppendLine(");");

            return sb.ToString();
        }
    }
}