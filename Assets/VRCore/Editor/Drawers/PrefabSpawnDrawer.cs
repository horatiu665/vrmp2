using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using VRCore;
using Random = UnityEngine.Random;

[CustomPropertyDrawer(typeof(PrefabTypeAndSpawnCount))]
public class PrefabSpawnDrawer : PropertyDrawer
{
    private const float _intFieldWidth = 60;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);

        position.width -= _intFieldWidth + 1f;
        EditorGUI.PropertyField(position, property.FindPropertyRelative("prefabType"));
        position.x += position.width + 2f;
        position.width = _intFieldWidth;

        EditorGUI.PropertyField(position, property.FindPropertyRelative("spawnCount"), GUIContent.none);

        EditorGUI.EndProperty();
    }

}