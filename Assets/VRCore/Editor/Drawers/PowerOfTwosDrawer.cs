namespace VRCore.Editor
{
    using System;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(PowerOfTwosAttribute))]
    public sealed class PowerOfTwosDrawer : PropertyDrawer
    {
        private MethodInfo _floatFieldMethod;
        private int _intValue;
        private float _floatValue;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.Float && property.propertyType != SerializedPropertyType.Integer)
            {
                label.text = string.Concat(label.text, " *PowerOfTwosAttribute N/A (only float/integer)!");

                // Not correct field type => no special handling
                EditorGUI.PropertyField(position, property, label, true);
                return;
            }

            var attrib = this.attribute as PowerOfTwosAttribute;

            if (property.propertyType == SerializedPropertyType.Integer)
            {
                _intValue = property.intValue;
            }
            else if (property.propertyType == SerializedPropertyType.Float)
            {
                _floatValue = property.floatValue;
            }

            label = EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();

            if (property.propertyType == SerializedPropertyType.Integer)
            {
                if (attrib.min != attrib.max)
                {
                    _intValue = EditorGUI.IntSlider(position, label, _intValue, Mathf.RoundToInt(attrib.min), Mathf.RoundToInt(attrib.max));
                    // failed attempt to make a logarithmic scale on the slider, so the same distance is dragged between 1 and 2, and between 2 and 4, as between 128 and 256
                    //_intValue = (int)Mathf.Pow(10, EditorGUI.IntSlider(position, label, (int)Mathf.Log(_intValue, 2), Mathf.RoundToInt(Mathf.Log(attrib.min, 2)), Mathf.RoundToInt(Mathf.Log(attrib.max, 2))));
                }
                else
                {
                    _intValue = EditorGUI.IntField(position, label, _intValue);
                }
            }
            else if (property.propertyType == SerializedPropertyType.Float)
            {
                if (attrib.min != attrib.max)
                {
                    _floatValue = EditorGUI.Slider(position, label, _floatValue, attrib.min, attrib.max);
                }
                else
                {
                    position = EditorGUI.PrefixLabel(position, label);
                    var labelRect = new Rect(position);
                    labelRect.width = EditorGUIUtility.labelWidth;
                    _floatValue = FloatFieldInternal(position, labelRect, _floatValue, EditorStyles.numberField);
                }
            }

            if (EditorGUI.EndChangeCheck() || GUI.changed)
            {
                if (property.propertyType == SerializedPropertyType.Integer)
                {
                    property.intValue = Mathf.ClosestPowerOfTwo(_intValue);
                }
                else if (property.propertyType == SerializedPropertyType.Float)
                {
                    property.floatValue = Mathf.ClosestPowerOfTwo(Mathf.RoundToInt(_floatValue));
                }
            }

            EditorGUI.EndProperty();
        }

        private float FloatFieldInternal(Rect position, Rect dragHotZone, float value, GUIStyle style)
        {
            Type editorGUIType = typeof(EditorGUI);
            int controlID = GUIUtility.GetControlID("EditorTextField".GetHashCode(), FocusType.Keyboard, position);

            if (_floatFieldMethod == null)
            {
                Type RecycledTextEditorType = Assembly.GetAssembly(editorGUIType).GetType("UnityEditor.EditorGUI+RecycledTextEditor");
                Type[] argumentTypes = new Type[] { RecycledTextEditorType, typeof(Rect), typeof(Rect), typeof(int), typeof(float), typeof(string), typeof(GUIStyle), typeof(bool) };
                _floatFieldMethod = editorGUIType.GetMethod("DoFloatField", BindingFlags.NonPublic | BindingFlags.Static, null, argumentTypes, null);
            }

            FieldInfo fieldInfo = editorGUIType.GetField("s_RecycledEditor", BindingFlags.NonPublic | BindingFlags.Static);
            object recycledEditor = fieldInfo.GetValue(null);

            object[] parameters = new object[] { recycledEditor, position, dragHotZone, controlID, value, "g7", style, true };

            return (float)_floatFieldMethod.Invoke(null, parameters);
        }
    }
}