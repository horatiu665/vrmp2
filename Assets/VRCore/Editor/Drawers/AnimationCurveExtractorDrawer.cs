namespace VRCore.Editor
{
    using UnityEditor;
    using UnityEngine;

    //[CustomPropertyDrawer(typeof(AnimationCurve))]
    public class AnimationCurveExtractorDrawer : PropertyDrawer
    {
        private const float _width = 20f;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            position.width -= _width + 1f;
            EditorGUI.PropertyField(position, property);
            position.x += position.width + 2f;
            position.width = _width;

            if (GUI.Button(position, "C"))
            {
                CurveExtractorEditorWindow.OpenWindow(property.animationCurveValue);
            }
        }
    }
}