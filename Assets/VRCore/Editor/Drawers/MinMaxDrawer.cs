namespace VRCore.Editor
{
    using System;
    using System.Reflection;
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(MinMaxAttribute))]
    public sealed class MinMaxDrawer : PropertyDrawer
    {
        private const float _prefixLabelWidth = 28f;

        private readonly GUIContent[] _subLabels = new GUIContent[]
        {
            new GUIContent("Min", "Minimum Value"),
            new GUIContent("Max", "Maximum Value")
        };

        private float _minValue, _maxValue;
        private MethodInfo _floatFieldMethod;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            if (property.propertyType != SerializedPropertyType.Vector2)
            {
                label.text = string.Concat(label.text, " *MinMaxAttribute N/A (only Vector2)!");

                // Not a vector2 field => no special formatting
                EditorGUI.PropertyField(position, property, label, true);
                return;
            }

            // For some reason using the passed in label directly results in other properties that have no tooltip to inherit the last tooltip set.
            label = new GUIContent(label.text, label.tooltip);

            var attrib = this.attribute as MinMaxAttribute;
            if (!string.IsNullOrEmpty(attrib.label))
            {
                label.text = string.Concat(label.text, " (", attrib.label, ")");
            }

            _minValue = property.vector2Value.x;
            _maxValue = property.vector2Value.y;

            var rect = new Rect(position.position, position.size);
            var labelWidth = EditorGUIUtility.labelWidth;
            var width = rect.width;

            EditorGUI.BeginProperty(rect, GUIContent.none, property);
            EditorGUI.BeginChangeCheck();

            // prefix label
            rect.width = labelWidth;
            rect = EditorGUI.PrefixLabel(rect, label);

            if (attrib.min != attrib.max)
            {
                var fieldWidth = Mathf.Max(_prefixLabelWidth, EditorGUIUtility.fieldWidth);
                var padding = 2f;

                rect.width = fieldWidth;
                _minValue = EditorGUI.FloatField(rect, (float)Math.Round(_minValue, 3));
                rect.x += rect.width + padding;

                rect.width = width - (fieldWidth * 2f) - labelWidth - (padding * 2f);
                EditorGUI.MinMaxSlider(rect, ref _minValue, ref _maxValue, attrib.min, attrib.max);
                rect.x += rect.width + padding;

                rect.width = fieldWidth;
                _maxValue = EditorGUI.FloatField(rect, (float)Math.Round(_maxValue, 3));
            }
            else
            {
                var fieldWidth = (width - labelWidth - (_prefixLabelWidth * 2f)) * 0.5f;
                var prefixLabelWidth = (width - labelWidth - (Mathf.Abs(fieldWidth) * 2f)) * 0.5f;

                // minimum
                rect.width = prefixLabelWidth;
                EditorGUI.LabelField(rect, _subLabels[0], EditorStyles.label);
                var labelRect = rect;
                rect.x += rect.width;

                rect.width = fieldWidth;
                //_minValue = EditorGUI.FloatField(position, _minValue, EditorStyles.numberField);
                _minValue = FloatFieldInternal(rect, labelRect, _minValue, EditorStyles.numberField);
                rect.x += rect.width;

                // maximum
                rect.width = prefixLabelWidth;

                EditorGUI.LabelField(rect, _subLabels[1], EditorStyles.label);
                labelRect = rect;
                rect.x += rect.width;

                rect.width = fieldWidth;
                //_maxValue = EditorGUI.FloatField(position, _maxValue, EditorStyles.numberField);
                _maxValue = FloatFieldInternal(rect, labelRect, _maxValue, EditorStyles.numberField);
            }

            if (EditorGUI.EndChangeCheck() || GUI.changed)
            {
                // Min value must not increase above max value
                //if (_minValue >= _maxValue)
                //{
                //    _minValue = _maxValue;
                //}

                property.vector2Value = new Vector2(_minValue, _maxValue);
            }

            EditorGUI.EndProperty();
        }

        private float FloatFieldInternal(Rect position, Rect dragHotZone, float value, GUIStyle style)
        {
            Type editorGUIType = typeof(EditorGUI);
            int controlID = GUIUtility.GetControlID("EditorTextField".GetHashCode(), FocusType.Keyboard, position);

            if (_floatFieldMethod == null)
            {
                Type RecycledTextEditorType = Assembly.GetAssembly(editorGUIType).GetType("UnityEditor.EditorGUI+RecycledTextEditor");
                Type[] argumentTypes = new Type[] { RecycledTextEditorType, typeof(Rect), typeof(Rect), typeof(int), typeof(float), typeof(string), typeof(GUIStyle), typeof(bool) };
                _floatFieldMethod = editorGUIType.GetMethod("DoFloatField", BindingFlags.NonPublic | BindingFlags.Static, null, argumentTypes, null);
            }

            FieldInfo fieldInfo = editorGUIType.GetField("s_RecycledEditor", BindingFlags.NonPublic | BindingFlags.Static);
            object recycledEditor = fieldInfo.GetValue(null);

            object[] parameters = new object[] { recycledEditor, position, dragHotZone, controlID, value, "g7", style, true };

            return (float)_floatFieldMethod.Invoke(null, parameters);
        }
    }
}