namespace VRCore.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(HelpBoxAttribute))]
    public sealed class HelpBoxDrawer : DecoratorDrawer
    {
        private HelpBoxAttribute attr
        {
            get { return (HelpBoxAttribute)this.attribute; }
        }

        public override float GetHeight()
        {
            var content = new GUIContent(this.attr.text);
            var rect = GUILayoutUtility.GetRect(content, EditorStyles.helpBox);
            return base.GetHeight() + rect.height;
        }

        public override void OnGUI(Rect position)
        {
            var attribute = this.attr;
            EditorGUI.HelpBox(position, attribute.text, (MessageType)attribute.type);
        }
    }
}