namespace VRCore.VRNetwork.Editor
{
    using UnityEditor;
    using UnityEditor.SceneManagement;
    using UnityEngine;

    public static class RigidbodySyncHelper
    {
        [MenuItem("Build/Synchronize All Rigidbodies", false, 899)]
        public static void SyncAllRigidbodies()
        {
            var counter = 0;
            var rigidbodies = Object.FindObjectsOfType<Rigidbody>();
            for (int i = 0; i < rigidbodies.Length; i++)
            {
                var rb = rigidbodies[i];
                if (rb.GetComponent<RigidbodySyncComponent>() != null)
                {
                    continue;
                }

                Undo.RecordObject(rb, "Added RigidbodySyncComponent");
                rb.AddComponent<RigidbodySyncComponent>();
                counter++;
            }

            Debug.Log("Rigidbody Synchronization Helper:: Added RigidbodySyncComponent to " + counter.ToString() + " rigidbodies in the current scene(s).");
            SetAllSyncedRigidbodies();
        }

        [MenuItem("Build/Synchronize All Rigidbodies", true)]
        public static bool SyncAllRigidbodiesValidate()
        {
            return Object.FindObjectOfType<Rigidbody>() != null;
        }

        [MenuItem("Build/Set All Synced Rigidbody IDs", false, 898)]
        public static void SetAllSyncedRigidbodies()
        {
            var objects = Object.FindObjectsOfType<RigidbodySyncComponent>();
            for (int i = 0; i < objects.Length; i++)
            {
                Undo.RecordObject(objects[i], "Set RigidbodySyncComponent SyncId");
                objects[i].SetSyncId(i + 1);
            }

            Debug.Log("Rigidbody Synchronization Helper:: Set sequential Sync IDs on " + objects.Length.ToString() + " objects in the current scene(s).");
            EditorSceneManager.MarkAllScenesDirty();
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        }

        [MenuItem("Build/Set All Synced Rigidbody IDs", true)]
        public static bool SetAllSyncedRigidbodiesValidate()
        {
            return Object.FindObjectOfType<RigidbodySyncComponent>() != null;
        }
    }
}