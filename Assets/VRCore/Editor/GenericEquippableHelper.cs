namespace VRCore.VRNetwork.Editor
{
    using System.Linq;
    using UnityEditor;
    using UnityEditor.SceneManagement;
    using UnityEngine;

    public static class GenericEquippableHelper
    {
        [MenuItem("Build/Set All Generic Equippable Net IDs", false, 99)]
        public static void Run()
        {
            var ge = Object.FindObjectsOfType<GenericEquippable>();

            Undo.RecordObjects(ge, "GenericEquippables Set Net IDs");

            for (int i = 0; i < ge.Length; i++)
            {
                ge[i].SetNetId(i + 1);
            }

            var gse = Object.FindObjectsOfType<GenericSpawnableEquippable>();
            Undo.RecordObjects(gse, "GenericSpawnableEquippables Set Net IDs");

            for (int i = 0; i < gse.Length; i++)
            {
                gse[i].SetNetId(i + 1);
            }

            Debug.Log("Generic Equippable Helper:: Set sequential Net IDs on " + (gse.Length + ge.Length).ToString() + " objects in the current scene(s).");
            EditorSceneManager.MarkAllScenesDirty();
            EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo();
        }

        [MenuItem("Build/Set All Generic Equippable Net IDs", true)]
        public static bool Validate()
        {
            return Object.FindObjectOfType<GenericEquippable>() != null || Object.FindObjectOfType<GenericSpawnableEquippable>() != null;
        }
    }
}