namespace VRCore.Editor
{
    using UnityEditor;

    [CustomEditor(typeof(VRPrefabManager))]
    public sealed class VRPrefabManagerEditor : VRPrefabManagerBaseEditor
    {
        private SerializedProperty _previewSetup;

        protected override void OnEnable()
        {
            base.OnEnable();
            _previewSetup = this.FindProperty("_previewSetup");
        }

        protected override void DrawExtraFields()
        {
            EditorGUILayout.PropertyField(_previewSetup, true);
            EditorGUILayout.Separator();
        }
    }
}