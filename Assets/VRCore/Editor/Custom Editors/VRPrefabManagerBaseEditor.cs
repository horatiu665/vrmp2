namespace VRCore.Editor
{
	using System;
	using System.Collections.Generic;
	using System.IO;
	using System.Text;
	using Apex.Editor;
	using UnityEditor;
	using UnityEngine;
	using VRCore;

	[CustomEditor(typeof(VRPrefabManagerBase<>))]
	public class VRPrefabManagerBaseEditor : Editor
	{
		protected SerializedProperty _prefabTypeMode, _initialInstanceCount, _initialCapacity, _prefabs, _maxEquipDistance;
		protected FileInfo _prefabFile;
        private MonoScript _script;

        protected virtual void OnEnable()
		{
            _script = MonoScript.FromMonoBehaviour(this.target as MonoBehaviour);

            _prefabTypeMode = this.serializedObject.FindProperty("_prefabTypeMode");
			_initialInstanceCount = this.serializedObject.FindProperty("_initialInstanceCount");
			_initialCapacity = this.serializedObject.FindProperty("_initialCapacity");
			_prefabs = this.serializedObject.FindProperty("_prefabs");
			_maxEquipDistance = this.serializedObject.FindProperty("_maxEquipDistance");
		}

		public override void OnInspectorGUI()
		{
            GUI.enabled = false;
            EditorGUILayout.ObjectField("Script", _script, typeof(MonoScript), false);
            GUI.enabled = true;

            if (Application.isPlaying)
			{
				EditorGUILayout.Separator();
				EditorGUILayout.HelpBox("The VRPrefabType enum is NOT generated during runtime, since that would trigger a recompilation of the project and thus break the execution. Therefore, the VR Prefab manager cannot be edited at runtime.", MessageType.Info);
				GUI.enabled = false;
			}

			this.serializedObject.Update();

			EditorGUILayout.Separator();
			if (!Application.isPlaying)
			{
				EditorGUILayout.PropertyField(_prefabTypeMode);
			}
			else
			{
				GUILayout.BeginHorizontal();
				EditorGUILayout.PrefixLabel(new GUIContent(_prefabTypeMode.displayName, _prefabTypeMode.tooltip));
				EditorGUILayout.LabelField(new GUIContent(_prefabTypeMode.enumDisplayNames[_prefabTypeMode.enumValueIndex], _prefabTypeMode.tooltip));
				GUILayout.EndHorizontal();
			}
			EditorGUILayout.Separator();

			EditorGUILayout.PropertyField(_initialInstanceCount);
			EditorGUILayout.PropertyField(_initialCapacity);
			EditorGUILayout.PropertyField(_prefabs, true);
			EditorGUILayout.Separator();

			DrawExtraFields();

			EditorGUILayout.LabelField("Auto Computed", EditorStyles.boldLabel);
			EditorGUI.indentLevel += 1;

			GUILayout.BeginHorizontal();
			EditorGUILayout.PrefixLabel(new GUIContent(_maxEquipDistance.displayName, _maxEquipDistance.tooltip));
			EditorGUILayout.LabelField(new GUIContent(_maxEquipDistance.floatValue.ToString(), _maxEquipDistance.tooltip));
			GUILayout.EndHorizontal();

			EditorGUI.indentLevel -= 1;
			EditorGUILayout.Separator();

			this.serializedObject.ApplyModifiedProperties();

			if (!Application.isPlaying)
			{
				if (GUI.changed || GUILayout.Button(new GUIContent("Generate Enum", "Generate the VRPrefabType enum on command. The file is also automatically generated when a change is detected.")))
				{
					GenerateVRPrefabTypeEnum();
				}
			}
			else
			{
				GUI.enabled = true;
			}

		}

		protected virtual void DrawExtraFields()
		{
		}

		protected virtual void GenerateVRPrefabTypeEnum()
		{
			var prefabTypeMode = ((VRPrefabTypeMode[])Enum.GetValues(typeof(VRPrefabTypeMode)))[_prefabTypeMode.enumValueIndex];
			EnsureMaxPrefabsLength(prefabTypeMode);

			var prefabFile = LocatePrefabTypeFile();
			if (prefabFile != null)
			{
				var types = GeneratePrefabTypeContents();
				WritePrefabTypeFile(prefabTypeMode, prefabFile, types);
			}
		}

		protected virtual void EnsureMaxPrefabsLength(VRPrefabTypeMode mode)
		{
			// do not allow prefabs to surpass max value
			var max = 0;
			switch (mode)
			{
			case VRPrefabTypeMode.Byte:
				{
					max = byte.MaxValue;
					break;
				}

			case VRPrefabTypeMode.Short:
				{
					max = short.MaxValue;
					break;
				}

			case VRPrefabTypeMode.Int:
				{
					max = int.MaxValue;
					break;
				}

			default:
				{
					throw new NotImplementedException(this.ToString() + " VRPrefabTypeMode unhandled type == " + mode.ToString());
				}
			}

			if (_prefabs.arraySize > max)
			{
				Debug.LogWarning(this.ToString() + " does not support more than " + max.ToString() + " prefabs, due to the currently selected prefab type mode (" + mode.ToString() + "). Please change the VRPrefab Type Mode first if you need more prefabs than the current maximum. Removing the last " + (_prefabs.arraySize - max).ToString() + " prefabs from array.");

				for (int i = _prefabs.arraySize - 1; i >= max; i--)
				{
					_prefabs.DeleteArrayElementAtIndex(i);
				}
			}
		}

		protected virtual string GeneratePrefabTypeContents()
		{
			var prefabEnumEntries = new List<string>();
			var prefabIntValues = new List<int>();
			for (int i = 0; i < _prefabs.arraySize; i++)
			{
				var prefab = _prefabs.GetArrayElementAtIndex(i).objectReferenceValue as GameObject;
				if (prefab == null)
				{
					continue;
				}

				// Remove whitespace at start and end of string, as well as a range of illegal characters
				var name = prefab.name.Trim()
					.Replace(" ", string.Empty)
					.Replace("-", string.Empty)
					.Replace("(", string.Empty)
					.Replace(")", string.Empty)
					.Replace("}", string.Empty)
					.Replace("{", string.Empty)
					.Replace("[", string.Empty)
					.Replace("]", string.Empty)
					.Replace(".", string.Empty)
					.Replace(",", string.Empty)
					.Replace(";", string.Empty)
					.Replace(":", string.Empty);

				if (string.IsNullOrEmpty(name))
				{
					Debug.LogWarning(this.ToString() + " the generated prefab name, for prefab == " + prefab.ToString() + ", is empty, and thus cannot be used.");
					continue;
				}

				if (EditorUtilities.IsReservedWord(name))
				{
					Debug.LogWarning(this.ToString() + " the generated prefab name (\"" + name + "\") is a reserved word, and thus cannot be used.");
					continue;
				}

				if (!System.CodeDom.Compiler.CodeGenerator.IsValidLanguageIndependentIdentifier(name))
				{
					Debug.LogWarning(this.ToString() + " the generated prefab name (\"" + name + "\") is not a valid language independent identifier, and thus cannot be used.");
					continue;
				}

				if (prefabEnumEntries.Contains(name))
				{
					Debug.LogWarning(this.ToString() + " the generated prefab name (\"" + name + "\"), for prefab == " + prefab.ToString() + ", is duplicated, another prefab already uses the same name. Please change the prefab's name.");
					continue;
				}

				prefabEnumEntries.Add(name);
				prefabIntValues.Add(i);
			}

			var count = prefabEnumEntries.Count;
			var sb = new StringBuilder(count * sizeof(char)); // not nearly enough, but better than nothing probably
			for (int i = 0; i < count; i++)
			{
				sb.AppendFormat(Template.ItemTemplate, prefabEnumEntries[i], prefabIntValues[i]);
				sb.AppendLine();
			}

			return sb.ToString();
		}

		protected virtual FileInfo LocatePrefabTypeFile()
		{
			if (_prefabFile == null)
			{
				// only search for prefab file if we don't have a cached version
				var di = new DirectoryInfo(Application.dataPath);
				var results = di.GetFiles("VRPrefabType.cs", SearchOption.AllDirectories);
				if (results.Length == 0)
				{
					Debug.LogError(this.ToString() + " could not find any VRPrefabType.cs file.");
					return null;
				}

				if (results.Length > 1)
				{
					Debug.LogWarning(this.ToString() + " more than one VRPrefabType.cs file found in project. Ensure that there is only one!");
				}

				_prefabFile = results[0];
			}

			return _prefabFile;
		}

		protected virtual void WritePrefabTypeFile(VRPrefabTypeMode prefabTypeMode, FileInfo file, string rawContents)
		{
			try
			{
				var prefabTypeModeStr = prefabTypeMode.ToString().ToLower();
				var contents = string.Format(Template.FileTemplate, rawContents, prefabTypeModeStr).Replace("{{", "{").Replace("}}", "}");
				using (var stream = file.Open(FileMode.Create))
				using (var writer = new StreamWriter(stream))
				{
					writer.Write(contents);
				}

				AssetDatabase.Refresh();
				Debug.Log(this.ToString() + " Updated VRPrefabType.cs");
			}
			catch (Exception e)
			{
				Debug.LogError(this.ToString() + " error in attempting to update VRPrefabType.cs: " + e.Message.ToString() + "\n" + e.StackTrace.ToString());
			}
		}

		protected class Template
		{
			public const string ItemTemplate = "\t\t{0} = {1},";
			public const string FileTemplate = @"//------------------------------------------------------------------------------
// <auto-generated>
// This enum was auto generated by VRCore.VRPrefabManager.
// </auto-generated>
//------------------------------------------------------------------------------
namespace VRCore
{{
    /// <summary>
    /// Auto-generated enum representing all VRCore prefabs.
    /// </summary>
    [Apex.PooledTypeIdentifier]
    public enum VRPrefabType : {1}
    {{
{0}
    }}
}}";
		}
	}
}