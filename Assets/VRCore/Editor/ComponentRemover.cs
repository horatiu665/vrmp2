namespace VRCore.Editor
{
    using System;
    using System.Collections.Generic;
    using UnityEditor;
    using UnityEngine;
    using UnityEngine.EventSystems;
    using UnityEngine.Networking;

    public static class ComponentRemover
    {
        private static readonly HashSet<Type> _nonRemovableTypes = new HashSet<Type>()
        {
            // Unity component types
            typeof(Transform),
            typeof(Collider),
            typeof(Collider2D),
            typeof(Rigidbody),
            typeof(Rigidbody2D),
            typeof(Animator),
            typeof(Animation),
            typeof(MeshFilter),
            typeof(Renderer),
            typeof(ParticleSystem),
            typeof(AudioSource),
            typeof(AudioListener),
            typeof(Camera),
            typeof(Joint),
            typeof(Joint2D),
            typeof(Light),
            typeof(LODGroup),
            typeof(UIBehaviour),
            typeof(Canvas),
            typeof(TextMesh),
            typeof(GUILayer),
            typeof(FlareLayer),

            // Unity network
            typeof(NetworkTransform),
            typeof(NetworkTransformChild),
            typeof(NetworkIdentity),
            typeof(NetworkManager),
            typeof(NetworkAnimator),
            typeof(NetworkLobbyPlayer),

            //// Pro builder => removing these ruins the mesh
            //typeof(pb_Entity),
            //typeof(pb_Object)
        };

        [MenuItem("GameObject/Remove All Scripts")]
        public static void RemoveAllScripts()
        {
            var selected = Selection.gameObjects;
            if (selected == null || selected.Length == 0)
            {
                Debug.LogWarning("Could not remove all scripts when there are no game objects selected. Please select at least one game object.");
                return;
            }

            for (int i = 0; i < selected.Length; i++)
            {
                RemoveScripts(selected[i]);
            }
        }

        private static void RemoveScripts(GameObject gameObject)
        {
            var counter = 0;

            var components = gameObject.GetComponents<Component>();
            if (components.Length > 0)
            {
                for (int i = components.Length - 1; i >= 0; i--)
                {
                    var comp = components[i];
                    if (!CanTypeBeRemoved(comp.GetType()))
                    {
                        continue;
                    }

                    counter++;
                    UnityEngine.Object.DestroyImmediate(comp);
                }
            }

            var childComponents = gameObject.GetComponentsInChildren<Component>(true);
            if (childComponents.Length > 0)
            {
                for (int i = childComponents.Length - 1; i >= 0; i--)
                {
                    var comp = childComponents[i];
                    if (!CanTypeBeRemoved(comp.GetType()))
                    {
                        continue;
                    }

                    counter++;
                    UnityEngine.Object.DestroyImmediate(comp);
                }
            }

            Debug.Log("Removed " + counter.ToString() + " components from " + gameObject.ToString());
        }

        private static bool CanTypeBeRemoved(Type type)
        {
            if (_nonRemovableTypes.Contains(type))
            {
                return false;
            }

            foreach (var t in _nonRemovableTypes)
            {
                if (type.IsSubclassOf(t))
                {
                    return false;
                }
            }

            return true;
        }
    }
}