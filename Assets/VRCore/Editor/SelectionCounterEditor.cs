﻿namespace VRCore.Editor
{
    using UnityEditor;
    using UnityEngine;

    public class SelectionCounterEditor
    {
        [MenuItem("Build/Count Selection", false, 96)]
        public static void CountSelection()
        {
            Debug.Log(Selection.gameObjects.Length.ToString() + " object(s) selected currently.");
        }

        [MenuItem("Build/Count Selection", true)]
        public static bool CountSelectionValidation()
        {
            return Selection.gameObjects != null && Selection.gameObjects.Length > 0;
        }
    }
}
