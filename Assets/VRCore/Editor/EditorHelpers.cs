namespace VRCore.Editor
{
    using System;
    using UnityEditor;

    public static class EditorHelpers
    {
        public static SerializedProperty FindProperty(this Editor editor, string str)
        {
            return editor.serializedObject.FindProperty(str);
        }

        public static void Section(string label)
        {
            if (EditorGUI.indentLevel >= 1)
            {
                EditorGUI.indentLevel -= 1;
            }

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField(label, EditorStyles.boldLabel);
            EditorGUI.indentLevel += 1;
        }

        public static void CreateOrUpdateAsset<T>(T obj, string assetName = "", string defaultAssetSubPath = "") where T : UnityEngine.Object
        {
            if (AssetDatabase.Contains(obj))
            {
                EditorUtility.SetDirty(obj);
            }
            else
            {
                //Have to do this rather cumbersome path construction due to the workings of the AssetDatabase methods
                var path = "Assets";
                var subPath = GetSubPath(defaultAssetSubPath);
                if (subPath.Contains("Assets"))
                {
                    path = subPath;
                }
                else if (!string.IsNullOrEmpty(subPath))
                {
                    path = string.Concat(path, "/", subPath);
                }

                var folderId = AssetDatabase.AssetPathToGUID(path);
                if (string.IsNullOrEmpty(folderId))
                {
                    folderId = AssetDatabase.CreateFolder("Assets", subPath);
                    path = AssetDatabase.GUIDToAssetPath(folderId);
                }

                if (string.IsNullOrEmpty(assetName))
                {
                    assetName = typeof(T).Name;
                }

                path = string.Concat(path, "/", assetName, ".asset");
                path = AssetDatabase.GenerateUniqueAssetPath(path);

                AssetDatabase.CreateAsset(obj, path);
            }

            EditorGUIUtility.PingObject(obj);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }

        private static string GetSubPath(string path)
        {
            if (string.IsNullOrEmpty(path) || path.Equals("Assets", StringComparison.OrdinalIgnoreCase))
            {
                return string.Empty;
            }

            return path.Trim('/', '\\');
        }
    }
}