Shader "Outlined/Silhouette Only Additive"
{
    Properties
    {
        _OutlineColor ("Outline Color", Color) = (0, 0, 0, 1)
        _Outline ("Outline width", Float) = .005
        _LogDiv ("Distance div", Float) = 5
    }

    CGINCLUDE
    #include "UnityCG.cginc"

    struct appdata
    {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
    };

    struct v2f
    {
        float4 pos : POSITION;
        float4 color : COLOR;
    };

    uniform float _Outline;
    uniform float4 _OutlineColor;
    uniform float _LogDiv;

    v2f vert(appdata v)
    {
        // just make a copy of incoming vertex data but scaled according to normal direction
        v2f o;
        o.pos = UnityObjectToClipPos(v.vertex);

        float3 camPos = _WorldSpaceCameraPos;
        float3 worldPos = mul(unity_ObjectToWorld, v.vertex);
        float dist = distance(camPos, worldPos);
        float logDist = log2(dist / _LogDiv);
        float3 norm = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
        float2 offset = TransformViewToProjection(norm.xy);

        o.pos.xy += normalize(offset) * o.pos.z * _Outline * logDist;
        o.color = _OutlineColor;
        return o;
    }
    ENDCG

    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
        }

        Pass
        {
            Name "BASE"
            Cull Back
            Blend Zero One
            // uncomment this to hide inner details:

            //Offset -8, -8
            SetTexture [_OutlineColor]
            { 
                constantColor (0, 0, 0, 0)
                combine constant
            }
        }

        // note that a vertex shader is specified here but its using the one above
        Pass
        {
            Name "OUTLINE"
            Tags
            {
                "LightMode" = "Always"
            }
            Cull Front

            // you can choose what kind of blending mode you want for the outline
            Blend SrcAlpha OneMinusSrcAlpha
            // Normal

            //Blend One One // Additive
            //Blend One OneMinusDstColor // Soft Additive
            //Blend DstColor Zero // Multiplicative
            //Blend DstColor SrcColor // 2x Multiplicative
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            half4 frag(v2f i) : COLOR
            {
                return i.color;
            }
            ENDCG
        }

    }

    FallBack "Diffuse"
}