namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;
    using VRNetwork;

    public static class PlayerManager
    {
        private static List<Rigidbody> _allPlayers = new List<Rigidbody>();
        private static IDictionary<Rigidbody, IVRPlayer> _dict = new Dictionary<Rigidbody, IVRPlayer>();

        // pls do not change list through this prop.
        public static List<Rigidbody> allPlayers
        {
            get
            {
                return _allPlayers;
            }
        }

        public static void LinkPlayer(this IVRPlayer pooled, Rigidbody rb)
        {
            _allPlayers.Add(rb);
            _dict.Add(rb, pooled);
        }

        public static bool UnlinkPlayer(this IVRPlayer pooled, Rigidbody rb)
        {
            _allPlayers.Remove(rb);
            return _dict.Remove(rb);
        }

        public static IVRPlayer GetPlayer(this Rigidbody rb)
        {
            return _dict.GetValueOrDefault(rb);
        }

        public static T GetPlayer<T>(this Rigidbody rb) where T : class, IVRPlayer
        {
            return GetPlayer(rb) as T;
        }

        public static T GetPlayer<T>(this Collider c) where T : class, IVRPlayer
        {
            if (c.attachedRigidbody == null)
            {
                return null;
            }
            else if (c.isTrigger)
            {
                return null;
            }
            else
            {
                return GetPlayer<T>(c.attachedRigidbody);
            }
        }

        public static T GetLocalPlayer<T>()
        {
            for (int i = 0; i < _allPlayers.Count; i++)
            {
                INetPlayer np = (INetPlayer)_dict[_allPlayers[i]];// _allPlayers[i].GetComponent<INetPlayer>();
                if (np != null)
                {
                    if (np.isLocal)
                    {
                        return (T)np;
                    }
                }
            }
            // only one player => singleplayer, or only one client player (local), or only one remote player because local has not connected yet.
            if (_allPlayers.Count == 1)
            {
                INetPlayer np = (INetPlayer)_dict[_allPlayers[0]];// _allPlayers[i].GetComponent<INetPlayer>();
                if (np != null)
                {
                    if (np.isLocal)
                    {
                        return (T)np;
                    }
                }
            }
            return default(T);
        }
    }
}