namespace VRCore
{
    [System.Serializable]
    public struct PreviewSetup
    {
        public VRPrefabType prefabType;
        public UnityEngine.GameObject previewPrefab;
    }
}