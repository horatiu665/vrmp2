namespace VRCore
{
    public class VRInputConfig
    {
        public float velocityMultiplier
        {
            get;
            set;
        }

        public float angularVelocityMultiplier
        {
            get;
            set;
        }
    }
}