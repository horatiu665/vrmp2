namespace VRCore
{
    using Helpers;
    using UnityEngine;

    public abstract class PrioritizedComponentBase : MonoBehaviour, IHavePriority
    {
        [SerializeField, ReadOnly(runtimeOnly = true)]
        protected int _priority = 1;

        public int priority
        {
            get { return _priority; }
        }
    }
}