namespace VRCore
{
    using UnityEngine;

    public sealed class GrabPointComponent : MonoBehaviour
    {
        private const string _viveControllerMeshPath = "ViveController";

#if UNITY_EDITOR

        [DebugButton]
        private void SpawnVisualController()
        {
            var prefab = Resources.Load<GameObject>(_viveControllerMeshPath);
            if (prefab != null)
            {
                this.InstantiateSafe(prefab, this.transform.position, this.transform.rotation, this.transform);
            }
            else
            {
                Debug.LogWarning(this.ToString() + " could not find game object at path == " + _viveControllerMeshPath);
            }
        }

        [DebugButton]
        private void RemoveVisualController()
        {
            var child = this.transform.Find(_viveControllerMeshPath);
            if (child != null)
            {
                this.DestroySafe(child.gameObject);
            }
        }

#endif
    }
}