namespace VRCore
{
    public interface IHavePriority
    {
        int priority { get; }
    }
}
