namespace Core
{
    using UnityEngine;
    using UnityEngine.Serialization;
    using VRCore;

    public class PlayerColorChooserManager : SingletonMonoBehaviour<PlayerColorChooserManager>
    {
        [SerializeField, FormerlySerializedAs("acceptableColors")]
        private Color[] _acceptableColors = new Color[1];

        private int _colorTurn;

        public static Color GetPlayerColor()
        {
            if (instance != null)
            {
                return instance.GetAcceptablePlayerColor();
            }

            return new Color(Random.value, Random.value, Random.value, 1f);
        }

        public Color GetAcceptablePlayerColor()
        {
            var color = _acceptableColors[_colorTurn++];
            if (_colorTurn >= _acceptableColors.Length)
            {
                _colorTurn = 0;
            }

            return color;
        }
    }
}