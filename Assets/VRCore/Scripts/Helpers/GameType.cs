namespace VRCore
{
    public enum GameType : byte
    {
        Local = 0,
        Remote = 1,
        Server = 2
    }
}