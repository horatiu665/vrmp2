﻿namespace VRCore
{
    public interface IVRPlayerStartListener
    {
        void OnPlayerStart(IVRPlayer player);
    }
}