﻿namespace VRCore
{
    using System;
    using UnityEngine;
    using Valve.VR;

    public static class VRHelper
    {
        private static GameObject _dummy;

        public static GameObject dummy
        {
            get
            {
                if (_dummy == null)
                {
                    _dummy = new GameObject("[Generated] Empty Dummy");
                }
                else if (!_dummy.activeSelf)
                {
                    _dummy.SetActive(true);
                }

                return _dummy;
            }
        }

        /// <summary>
        /// Gets the <see cref="EVRButtonId"/> associated with the given <see cref="VRInput"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">GetSteamVRButtonID: The given VRInput type, " + input.ToString() + ", is unhandled!</exception>
        public static EVRButtonId GetSteamVRButtonID(VRInput input)
        {
            switch (input)
            {
                case VRInput.System:
                {
                    return EVRButtonId.k_EButton_System;
                }

                case VRInput.ApplicationMenu:
                {
                    return EVRButtonId.k_EButton_ApplicationMenu;
                }

                case VRInput.Grip:
                {
                    return EVRButtonId.k_EButton_Grip;
                }

                case VRInput.Touchpad:
                {
                    return EVRButtonId.k_EButton_SteamVR_Touchpad;
                }

                case VRInput.Trigger:
                {
                    return EVRButtonId.k_EButton_SteamVR_Trigger;
                }

                //case VRInput.Axis0:
                //{
                //    return EVRButtonId.k_EButton_Axis0;
                //}

                //case VRInput.Axis1:
                //{
                //    return EVRButtonId.k_EButton_Axis1;
                //}

                case VRInput.Axis2:
                {
                    return EVRButtonId.k_EButton_Axis2;
                }

                case VRInput.Axis3:
                {
                    return EVRButtonId.k_EButton_Axis3;
                }

                case VRInput.Axis4:
                {
                    return EVRButtonId.k_EButton_Axis4;
                }

                default:
                {
                    throw new NotImplementedException("GetSteamVRButtonID: The given VRInput type, " + input.ToString() + ", is unhandled!");
                }
            }
        }

        /// <summary>
        /// Gets the <see cref="SteamVR_Controller.ButtonMask"/> value corresponding to the given <see cref="VRInput"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        /// <exception cref="System.NotImplementedException">GetSteamVRButtonMask: The given VRInput type, " + input.ToString() + ", is unhandled!</exception>
        public static ulong GetSteamVRButtonMask(VRInput input)
        {
            switch (input)
            {
                case VRInput.System:
                {
                    return SteamVR_Controller.ButtonMask.System;
                }

                case VRInput.ApplicationMenu:
                {
                    return SteamVR_Controller.ButtonMask.ApplicationMenu;
                }

                case VRInput.Grip:
                {
                    return SteamVR_Controller.ButtonMask.Grip;
                }

                case VRInput.Touchpad:
                {
                    return SteamVR_Controller.ButtonMask.Touchpad;
                }

                case VRInput.Trigger:
                {
                    return SteamVR_Controller.ButtonMask.Trigger;
                }

                //case VRInput.Axis0:
                //{
                //    return SteamVR_Controller.ButtonMask.Axis0;
                //}

                //case VRInput.Axis1:
                //{
                //    return SteamVR_Controller.ButtonMask.Axis1;
                //}

                case VRInput.Axis2:
                {
                    return SteamVR_Controller.ButtonMask.Axis2;
                }

                case VRInput.Axis3:
                {
                    return SteamVR_Controller.ButtonMask.Axis3;
                }

                case VRInput.Axis4:
                {
                    return SteamVR_Controller.ButtonMask.Axis4;
                }

                default:
                {
                    throw new NotImplementedException("GetSteamVRButtonMask: The given VRInput type, " + input.ToString() + ", is unhandled!");
                }
            }
        }
    }
}