namespace VRCore
{
    using UnityEngine;

    public interface IHaveThrowMultipliers
    {
        float velocityMultiplier { get; }

        float angularVelocityMultiplier { get; }
    }
}