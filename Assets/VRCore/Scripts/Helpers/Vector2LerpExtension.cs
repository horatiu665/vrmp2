using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class Vector2LerpExtension
{
	/// <summary>
	/// Equivalent of Mathf.Lerp(vector.x, vector.y, t)
	/// </summary>
	public static float Lerp(this Vector2 vector, float t)
	{
		return Mathf.Lerp(vector.x, vector.y, t);
	}

	/// <summary>
	/// Equivalent of Mathf.InverseLerp(vector.x, vector.y, t)
	/// </summary>
	public static float InverseLerp(this Vector2 vector, float t)
	{
		return Mathf.InverseLerp(vector.x, vector.y, t);
	}
}
