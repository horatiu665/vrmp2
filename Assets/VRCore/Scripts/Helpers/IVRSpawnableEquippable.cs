namespace VRCore
{
    public interface IVRSpawnableEquippable : IVRSpawnable, IVREquippable
    {
    }
}