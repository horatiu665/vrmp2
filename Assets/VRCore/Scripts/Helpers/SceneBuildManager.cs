namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    [ExecuteInEditMode]
    public sealed class SceneBuildManager : SingletonMonoBehaviour<SceneBuildManager>
    {
        [HelpBox("This component is automatically populated with the list of scenes from the custom Build Window. Should usually not be modified manually.", HelpBoxType.Info)]
        [SerializeField]
        private List<SceneBuildItem> _scenes = new List<SceneBuildItem>();

        public List<SceneBuildItem> scenes
        {
            get { return _scenes; }
        }
    }
}