namespace VRCore
{
    public abstract class VREquippableSpawnableBase : VREquippableBase, IVRSpawnableEquippable
    {
        public virtual void OnPreSpawn(IVRPlayer spawner)
        {
            _player = spawner;
        }

        public virtual void OnPostSpawn()
        {
            /* NOOP */
        }

    }
}