namespace VRCore.Debugging
{
    using System;
    using Apex.Utilities;
    using Core;
    using UnityEngine;
    using UnityEngine.SceneManagement;
    using UnityEngine.Serialization;

    [Apex.ApexComponent("Player")]
    [RequireComponent(typeof(IVRPlayer))]
    public sealed class CheatSpawning : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private KeyCodeMapping[] _mapping = new KeyCodeMapping[0];
        
        [SerializeField, FormerlySerializedAs("resetLevel")]
        private KeyCode _resetLevel = KeyCode.T;

        [Space]
        [SerializeField]
        private KeyCode _leftHandModifier = KeyCode.LeftShift;

        [SerializeField]
        private KeyCode _rightHandModifier = KeyCode.RightShift;

        [Space]
        [SerializeField]
        private Vector3 _leftHandLocalPos = new Vector3(-1f, 1f, 0f);

        [SerializeField]
        private Vector3 _rightHandLocalPos = new Vector3(1f, 1f, 0f);

        private IVRPlayer _player;
        private bool _isLeft;

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;

            _player.leftHand.gameObject.SetActive(true);
            _player.leftHand.localPosition = _leftHandLocalPos;

            _player.rightHand.gameObject.SetActive(true);
            _player.rightHand.localPosition = _rightHandLocalPos;
        }

        private void Update()
        {
            if (_player == null || (Event.current != null && !Event.current.isKey))
            {
                return;
            }

            _isLeft = Input.GetKey(_leftHandModifier) || !Input.GetKey(_rightHandModifier);

            for (int i = 0; i < _mapping.Length; i++)
            {
                var key = _mapping[i].keyCode;
                var input = _mapping[i].vrInput;
                if (Input.GetKeyDown(key))
                {
                    OnPressDown(_isLeft, input);
                }
                else if (Input.GetKeyUp(key))
                {
                    OnPressUp(_isLeft, input);
                }
            }

            if (Input.GetKeyUp(_resetLevel))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }

            if (Input.GetKeyUp(KeyCode.Keypad1))
            {
                SceneManager.LoadScene(0);
            }

            if (Input.GetKeyUp(KeyCode.Keypad2))
            {
                SceneManager.LoadScene(1);
            }

            if (Input.GetKeyUp(KeyCode.Keypad3))
            {
                SceneManager.LoadScene(2);
            }

            if (Input.GetKeyUp(KeyCode.Keypad4))
            {
                SceneManager.LoadScene(3);
            }
        }

        private void OnPressDown(bool isLeft, VRInput input)
        {
            var manager = SpawnManagerPreviewable.instance;
            var mapping = ListBufferPool.GetBuffer<SpawnSetup>(1);
            manager.GetMapped(isLeft, input, mapping);

            if (mapping.Count > 0)
            {
                var hand = isLeft ? _player.leftHand : _player.rightHand;
                var prefabs = ListBufferPool.GetBuffer<IVRSpawnable>(mapping.Count);
                manager.Spawn(hand, input, prefabs);

                var count = prefabs.Count;
                for (int i = 0; i < count; i++)
                {
                    if (prefabs[i] == null)
                    {
                        continue;
                    }

                    var equippable = prefabs[i] as IVRSpawnableEquippable;
                    if (equippable != null)
                    {
                        hand.Equip(equippable);
                    }
                }

                ListBufferPool.ReturnBuffer(prefabs);
            }

            ListBufferPool.ReturnBuffer(mapping);
        }

        private void OnPressUp(bool isLeft, VRInput input)
        {
            var hand = isLeft ? _player.leftHand : _player.rightHand;
            var equipped = hand.equipped;
            var count = equipped.Count;
            if (count == 0)
            {
                return;
            }

            // Otherwise, figure out whether we are releasing a spawn button and that we have such a spawned object in hand
            var manager = SpawnManagerPreviewable.instance;
            var mapping = ListBufferPool.GetBuffer<SpawnSetup>(1);
            manager.GetMapped(isLeft, input, mapping);

            var mappedCount = mapping.Count;
            if (mappedCount > 0)
            {
                for (int i = 0; i < mappedCount; i++)
                {
                    for (int j = count - 1; j >= 0; j--)
                    {
                        if (mapping[i].prefabType == equipped[j].prefabType)
                        {
                            // a mapped prefab is in hand
                            Unequip(equipped[j], isLeft);
                        }
                    }
                }
            }

            ListBufferPool.ReturnBuffer(mapping);
        }

        private void Unequip(IVREquippable equippable, bool isLeft)
        {
            if (equippable == null)
            {
                return;
            }

            var hand = isLeft ? _player.leftHand : _player.rightHand;
            var equipped = hand.equipped;
            var count = equipped.Count;
            if (count == 0)
            {
                Debug.LogWarning(this.ToString() + " Cannot Unequip() when the hand has nothing equipped");
                return;
            }

            hand.Unequip(equippable, _player.velocity + Vector3.zero, Vector3.zero);
        }

        [Serializable]
        private class KeyCodeMapping
        {
            public VRInput vrInput = 0;
            public KeyCode keyCode = KeyCode.None;
        }
    }
}