namespace Helpers.Editor
{
    using UnityEngine;

    public sealed class TransformGizmoVisualizer : MonoBehaviour
    {
        [SerializeField, Tooltip("If true, will draw always and not just when this game object is selected.")]
        private bool _drawAlways = true;
        
        [SerializeField]
        private float _sphereSize = 0.5f;

        [SerializeField]
        private Color _sphereColor = Color.green;

        [SerializeField]
        private bool _useWireframe = false;

        private void OnDrawGizmosSelected()
        {
            if (!_drawAlways)
            {
                Draw();
            }
        }

        private void OnDrawGizmos()
        {
            if (_drawAlways)
            {
                Draw();
            }
        }

        private void Draw()
        {
            Gizmos.color = _sphereColor;
            if (_useWireframe)
            {
                Gizmos.DrawWireSphere(this.transform.position, _sphereSize);
            }
            else
            {
                Gizmos.DrawSphere(this.transform.position, _sphereSize);
            }
        }
    }
}