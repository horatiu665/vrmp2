namespace Core.Testing
{
    using System;
    using Helpers;
    using UnityEngine;
    using VRCore;

    public sealed class ReuseSameInstanceHelper : SingletonMonoBehaviour<ReuseSameInstanceHelper>
    {
        [SerializeField]
        private SingleInstanceSetup[] _setup = new SingleInstanceSetup[0];

        public T GetInstance<T>(VRPrefabType type)
        {
            return GetInstance(type).GetComponent<T>();
        }

        public GameObject GetInstance(VRPrefabType type)
        {
            for (int i = 0; i < _setup.Length; i++)
            {
                if (_setup[i].prefabType == type)
                {
                    return _setup[i].gameObject;
                }
            }

            return null;
        }

        [Serializable]
        private class SingleInstanceSetup
        {
            public VRPrefabType prefabType = 0;
            public GameObject gameObject = null;
        }
    }
}