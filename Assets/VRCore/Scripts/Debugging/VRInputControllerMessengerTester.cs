namespace VRCore.Debugging
{
    using Apex.Services;
    using UnityEngine;

    public sealed class VRInputControllerMessengerTester : MonoBehaviour, IHandleMessage<VRInputPress>, IHandleMessage<VRInputPressDown>, IHandleMessage<VRInputPressUp>, IHandleMessage<VRInputTouch>, IHandleMessage<VRInputTouchDown>, IHandleMessage<VRInputTouchUp>
    {
        public VRInput inputFilter = VRInput.System;

        private void OnEnable()
        {
            GameServices.messageBus.Subscribe<VRInputPress>(this);
            GameServices.messageBus.Subscribe<VRInputPressDown>(this);
            GameServices.messageBus.Subscribe<VRInputPressUp>(this);
            GameServices.messageBus.Subscribe<VRInputTouch>(this);
            GameServices.messageBus.Subscribe<VRInputTouchDown>(this);
            GameServices.messageBus.Subscribe<VRInputTouchUp>(this);
        }

        private void OnDisable()
        {
            GameServices.messageBus.Unsubscribe<VRInputPress>(this);
            GameServices.messageBus.Unsubscribe<VRInputPressDown>(this);
            GameServices.messageBus.Unsubscribe<VRInputPressUp>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouch>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouchDown>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouchUp>(this);
        }

        public void Handle(VRInputPress message)
        {
            if (this.inputFilter != VRInput.System && message.input != this.inputFilter)
            {
                return;
            }

            Debug.Log(this.ToString() + " Handle VRInputPress message == " + message.ToString());
        }

        public void Handle(VRInputPressDown message)
        {
            if (this.inputFilter != VRInput.System && message.input != this.inputFilter)
            {
                return;
            }

            Debug.Log(this.ToString() + " Handle VRInputPressDown message == " + message.ToString());
        }

        public void Handle(VRInputPressUp message)
        {
            if (this.inputFilter != VRInput.System && message.input != this.inputFilter)
            {
                return;
            }

            Debug.Log(this.ToString() + " Handle VRInputPressUp message == " + message.ToString());
        }

        public void Handle(VRInputTouch message)
        {
            if (this.inputFilter != VRInput.System && message.input != this.inputFilter)
            {
                return;
            }

            Debug.Log(this.ToString() + " Handle VRInputTouch message == " + message.ToString());
        }

        public void Handle(VRInputTouchDown message)
        {
            if (this.inputFilter != VRInput.System && message.input != this.inputFilter)
            {
                return;
            }

            Debug.Log(this.ToString() + " Handle VRInputTouchDown message == " + message.ToString());
        }

        public void Handle(VRInputTouchUp message)
        {
            if (this.inputFilter != VRInput.System && message.input != this.inputFilter)
            {
                return;
            }

            Debug.Log(this.ToString() + " Handle VRInputTouchUp message == " + message.ToString());
        }
    }
}