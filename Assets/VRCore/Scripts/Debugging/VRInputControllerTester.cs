namespace VRCore.Debugging
{
    using UnityEngine;

    public abstract class VRInputControllerTester<TInputReceiver> : VRInputControllerBase<IVRPlayer, TInputReceiver> where TInputReceiver : IVRInputReceiver, new()
    {
        public override void OnPress(bool isLeft, VRInput input)
        {
            Debug.Log(this.ToString() + " OnPress. Is left == " + isLeft.ToString() + ", input == " + input.ToString());
        }

        public override void OnPressDown(bool isLeft, VRInput input)
        {
            Debug.Log(this.ToString() + " OnPressDown. Is left == " + isLeft.ToString() + ", input == " + input.ToString());
        }

        public override void OnPressUp(bool isLeft, VRInput input)
        {
            Debug.Log(this.ToString() + " OnPressUp. Is left == " + isLeft.ToString() + ", input == " + input.ToString());
        }

        public override void OnTouch(bool isLeft, VRInput input)
        {
            Debug.Log(this.ToString() + " OnTouch. Is left == " + isLeft.ToString() + ", input == " + input.ToString());
        }

        public override void OnTouchDown(bool isLeft, VRInput input)
        {
            Debug.Log(this.ToString() + " OnTouchDown. Is left == " + isLeft.ToString() + ", input == " + input.ToString());
        }

        public override void OnTouchUp(bool isLeft, VRInput input)
        {
            Debug.Log(this.ToString() + " OnTouchUp. Is left == " + isLeft.ToString() + ", input == " + input.ToString());
        }
    }
}