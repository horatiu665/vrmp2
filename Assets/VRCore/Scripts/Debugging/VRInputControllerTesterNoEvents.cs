namespace VRCore.Debugging
{
    using UnityEngine;

    [RequireComponent(typeof(IVRPlayer))]
    public sealed class VRInputControllerTesterNoEvents : MonoBehaviour, IVRPlayerStartListener
    {
        [SerializeField]
        private bool _useLeft = false;

        [SerializeField]
        private VRInput _input = VRInput.Touchpad;

        private IVRPlayer _player;

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;
        }

        private void Update()
        {
            if (_player == null)
            {
                return;
            }

            var hand = _useLeft ? _player.leftHand : _player.rightHand;
            var controller = hand.controller;
            if (controller == null)
            {
                return;
            }

            if (controller.GetPressDown(_input))
            {
                Debug.Log(this.ToString() + " GetPressDown input == " + _input.ToString());
            }

            if (controller.GetPressUp(_input))
            {
                Debug.Log(this.ToString() + " GetPressUp input == " + _input.ToString());
            }

            if (controller.GetTouchDown(_input))
            {
                Debug.Log(this.ToString() + " GetTouchDown input == " + _input.ToString());
            }

            if (controller.GetTouchUp(_input))
            {
                Debug.Log(this.ToString() + " GetTouchUp input == " + _input.ToString());
            }
        }
    }
}