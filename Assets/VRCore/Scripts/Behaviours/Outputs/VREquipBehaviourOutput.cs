namespace VRCore
{
    public sealed class VREquipBehaviourOutput : VRBehaviourOutputBase
    {
        public IVREquippable overrideEquippable;

        internal override void Clear()
        {
            base.Clear();
            this.overrideEquippable = null;
        }
    }
}