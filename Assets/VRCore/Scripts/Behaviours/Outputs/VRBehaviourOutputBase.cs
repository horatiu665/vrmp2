namespace VRCore
{
    public abstract class VRBehaviourOutputBase
    {
        public bool stopExecution;

        internal virtual void Clear()
        {
            this.stopExecution = false;
        }
    }
}