namespace VRCore
{
    public sealed class VRSpawnBehaviourOutput : VRBehaviourOutputBase
    {
        public IVRSpawnable overrideSpawnable;

        internal override void Clear()
        {
            base.Clear();
            this.overrideSpawnable = null;
        }
    }
}