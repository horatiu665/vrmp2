namespace VRCore
{
    public sealed class VRSpawnBehaviourInput : VRBehaviourInputBase
    {
        public IVRSpawnable spawnable;
    }
}