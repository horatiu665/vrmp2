namespace VRCore
{
    public abstract class VREquippableBehaviourInputBase : VRBehaviourInputBase
    {
        public IVREquippable equippable;

        internal override void Clear()
        {
            base.Clear();
            this.equippable = null;
        }
    }
}