namespace VRCore
{
    public abstract class VRBehaviourInputBase
    {
        public IVRHand hand;
        public IVRPlayer player;

        internal virtual void Clear()
        {
            this.hand = null;
            this.player = null;
        }
    }
}