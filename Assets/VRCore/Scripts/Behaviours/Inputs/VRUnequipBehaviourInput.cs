namespace VRCore
{
    using UnityEngine;

    public sealed class VRUnequipBehaviourInput : VREquippableBehaviourInputBase
    {
        public Vector3 velocity;
        public Vector3 angularVelocity;
    }
}