namespace VRCore
{
    using System.Collections.Generic;

    public sealed class VRCompositeBehaviours : IVRCustomBehaviour
    {
        private readonly IList<IVRCustomBehaviour> _behaviours;
        private readonly int _priority;
        private VRBehaviourCapabilities _capabilities;

        public VRCompositeBehaviours(int priority)
        {
            _priority = priority;
            _behaviours = new List<IVRCustomBehaviour>();
        }

        public bool enabled
        {
            get { return true; }
        }

        public IList<IVRCustomBehaviour> behaviours
        {
            get { return _behaviours; }
        }

        public VRBehaviourCapabilities capabilities
        {
            get { return _capabilities; }
        }

        public bool isSpawnBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Spawn) != 0; }
        }

        public bool isHighlightBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Highlight) != 0; }
        }

        public bool isEquipBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Equip) != 0; }
        }

        public bool isUnequipBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Unequip) != 0; }
        }

        public int priority
        {
            get { return _priority; }
        }

        public void Add(IVRCustomBehaviour behaviour)
        {
            _capabilities |= behaviour.capabilities;
            _behaviours.Add(behaviour);
        }

        public bool Remove(IVRCustomBehaviour behaviour)
        {
            _capabilities &= ~behaviour.capabilities;
            return _behaviours.Remove(behaviour);
        }

        public void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var count = _behaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!_behaviours[i].isSpawnBehaviour)
                {
                    continue;
                }

                _behaviours[i].OnSpawn(input, output);
            }
        }

        public void OnHighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output)
        {
            var count = _behaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!_behaviours[i].isHighlightBehaviour)
                {
                    continue;
                }

                _behaviours[i].OnHighlight(input, output);
            }
        }

        public void OnUnhighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output)
        {
            var count = _behaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!_behaviours[i].isHighlightBehaviour)
                {
                    continue;
                }

                _behaviours[i].OnUnhighlight(input, output);
            }
        }

        public void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var count = _behaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!_behaviours[i].isEquipBehaviour)
                {
                    continue;
                }

                _behaviours[i].OnEquip(input, output);
            }
        }

        public void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            var count = _behaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!_behaviours[i].isUnequipBehaviour)
                {
                    continue;
                }

                _behaviours[i].OnUnequip(input, output);
            }
        }
    }
}