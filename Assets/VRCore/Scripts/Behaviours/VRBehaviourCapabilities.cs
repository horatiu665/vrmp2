namespace VRCore
{
    using System;

    [Flags]
    public enum VRBehaviourCapabilities
    {
        Spawn = 1,
        Highlight = 2,
        Equip = 4,
        Unequip = 8,
        All = Spawn | Highlight | Equip | Unequip
    }
}