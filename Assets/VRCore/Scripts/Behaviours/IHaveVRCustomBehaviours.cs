namespace VRCore
{
    using System.Collections.Generic;

    public interface IHaveVRCustomBehaviours
    {
        /// <summary>
        /// Gets the list of custom behaviours currently registered for this instance.
        /// </summary>
        /// <value>
        /// The behaviours.
        /// </value>
        IList<IVRCustomBehaviour> behaviours { get; }
    }
}