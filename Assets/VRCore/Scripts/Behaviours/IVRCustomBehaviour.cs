namespace VRCore
{
    public interface IVRCustomBehaviour : IHavePriority
    {
        bool enabled { get; }

        VRBehaviourCapabilities capabilities { get; }

        bool isSpawnBehaviour { get; }

        bool isHighlightBehaviour { get; }

        bool isEquipBehaviour { get; }

        bool isUnequipBehaviour { get; }

        void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output);

        void OnHighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output);

        void OnUnhighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output);

        void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output);

        void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output);
    }
}