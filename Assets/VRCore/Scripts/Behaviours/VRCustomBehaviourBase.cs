namespace VRCore
{
    public abstract class VRCustomBehaviourBase : PrioritizedComponentBase, IVRCustomBehaviour
    {
        protected IHaveVRCustomBehaviours _owner;

        public abstract VRBehaviourCapabilities capabilities { get; }

        public bool isSpawnBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Spawn) != 0; }
        }

        public bool isHighlightBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Highlight) != 0; }
        }

        public bool isEquipBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Equip) != 0; }
        }

        public bool isUnequipBehaviour
        {
            get { return (this.capabilities & VRBehaviourCapabilities.Unequip) != 0; }
        }

        protected virtual void OnEnable()
        {
            _owner = this.GetComponentInParent<IHaveVRCustomBehaviours>();
            _owner.RegisterBehaviour(this);
        }

        protected virtual void OnDisable()
        {
            _owner.UnregisterBehaviour(this);
            _owner = null;
        }

        /// <summary>
        /// Base call not needed
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public virtual void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            /* NOOP */
        }

        /// <summary>
        /// Base call not needed
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public virtual void OnHighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output)
        {
            /* NOOP */
        }

        /// <summary>
        /// Base call not needed
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public virtual void OnUnhighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output)
        {
            /* NOOP */
        }

        /// <summary>
        /// Base call not needed
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public virtual void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            /* NOOP */
        }

        /// <summary>
        /// Base call not needed
        /// </summary>
        /// <param name="input"></param>
        /// <param name="output"></param>
        public virtual void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            /* NOOP */
        }
    }
}