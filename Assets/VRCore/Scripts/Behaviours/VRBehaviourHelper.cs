namespace VRCore
{
    public static class VRBehaviourHelper
    {
        public static void RegisterBehaviour(this IHaveVRCustomBehaviours comp, IVRCustomBehaviour behaviour)
        {
            var list = comp.behaviours;
            var count = list.Count;
            for (int i = 0; i < count; i++)
            {
                var c = list[i];
                if (c.priority == behaviour.priority)
                {
                    var grp = c as VRCompositeBehaviours;
                    if (grp == null)
                    {
                        grp = new VRCompositeBehaviours(c.priority);
                        list[i] = grp;
                        grp.Add(c);
                    }

                    grp.Add(behaviour);
                    return;
                }
            }

            list.Add(behaviour);
            list.Sort((a, b) => b.priority.CompareTo(a.priority));
        }

        public static void UnregisterBehaviour(this IHaveVRCustomBehaviours comp, IVRCustomBehaviour behaviour)
        {
            var list = comp.behaviours;
            var start = list.Count - 1;
            for (int i = start; i >= 0; i--)
            {
                var c = list[i];
                if (c.priority == behaviour.priority)
                {
                    var grp = c as VRCompositeBehaviours;
                    if (grp != null)
                    {
                        grp.Remove(behaviour);
                    }
                    else
                    {
                        list.RemoveAt(i);
                    }
                }
            }
        }
    }
}