namespace VRCore
{
    using Core;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEngine;
    using VRNetwork;

    public sealed class ReuseInstancesListSpawnBehaviour : VRCustomBehaviourBase
    {
        // outer-most key is player net ID, nested dictionary key is prefab type mapping to the value which is a list of instances that were spawned previously
        private static Dictionary<short, Dictionary<VRPrefabType, List<IVRSpawnable>>> _dict = new Dictionary<short, Dictionary<VRPrefabType, List<IVRSpawnable>>>();

        private readonly VRPrefabTypeComparer _comparer = new VRPrefabTypeComparer();

        [SerializeField]
        private int _maxInstances = 10;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Spawn; }
        }

        public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var player = input.player as INetPlayer;
            if (player == null)
            {
                return;
            }

            if (!_dict.ContainsKey(player.netId))
            {
                _dict.Add(player.netId, new Dictionary<VRPrefabType, List<IVRSpawnable>>(_comparer));
            }

            var spawnable = input.spawnable;
            if (spawnable == null)
            {
                Debug.LogError("[ReuseInstancesListSpawnBehaviour] tried to add an IVRSpawnable which is not a VRPrefabBase into the respawn list."
                    + "This is not supported because of Unity's retardation regarding null checks.");
            }

            var playerDict = _dict.GetValueOrDefault(player.netId);
            if (!playerDict.ContainsKey(spawnable.prefabType))
            {
                playerDict.Add(spawnable.prefabType, new List<IVRSpawnable>());
            }

            var list = playerDict.GetValueOrDefault(spawnable.prefabType);

            // BEFORE checking count
            // remove null elements and inactive self ones (they could be in the pool too)
            for (int i = list.Count - 1; i >= 0; i--)
            {
                var s = list[i];
                if (s == null)
                {
                    list.RemoveAt(i);
                    continue;
                }

                if (((Object)s).IsDestroyed())
                {
                    list.RemoveAt(i);
                    continue;
                }

                if (!s.gameObject.activeSelf)
                {
                    list.RemoveAt(i);
                }
            }

            if (list.Count < _maxInstances)
            {
                // Player is allowed to spawn more new ones
                list.Add(spawnable);
                return;
            } // else, player should reuse the existing ones in the list

            // find the first element in the list (oldest object) which is not equipped, reset its syncing, unimpale and equip that one instead of the current.
            // remove from the list and add to the front.
            for (int i = 0; i < list.Count; i++)
            {
                var s = list[i];
                var e = s as IVRSpawnableEquippable;
                if (e != null && e.isEquipped)
                {
                    continue;
                }

                var sync = s.GetComponent<EquippableSyncComponent>();
                if (sync != null)
                {
                    sync.ResetSync();
                }

                var impaler = s as ICanImpale;
                if (impaler != null)
                {
                    ImpalementHandler.Unimpale(impaler, true);
                }

                // the first non-null non-equipped spawnable is used
                output.overrideSpawnable = s as IVRSpawnable;
                list.RemoveAt(i);
                list.Add(s);
                break;
            }
        }

        private void Reset()
        {
            _priority = 500;
        }
    }
}