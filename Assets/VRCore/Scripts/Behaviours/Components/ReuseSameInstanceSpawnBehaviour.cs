namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// This behaviour stores when a new equippable is spawned, and then re-uses the spawned instance of that type for that hand on subsequent spawns.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class ReuseSameInstanceSpawnBehaviour : VRCustomBehaviourBase
    {
        private static readonly IDictionary<IVRHand, IDictionary<VRPrefabType, IVRSpawnable>> _instances = new Dictionary<IVRHand, IDictionary<VRPrefabType, IVRSpawnable>>();
        private static readonly VRPrefabTypeComparer _comparer = new VRPrefabTypeComparer();

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Spawn; }
        }

        private void Reset()
        {
            _priority = 75;
        }

        public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var hand = input.hand;
            var spawnable = input.spawnable;

            IDictionary<VRPrefabType, IVRSpawnable> dict;
            if (!_instances.TryGetValue(hand, out dict))
            {
                // hand has not had anything equipped before
                dict = new Dictionary<VRPrefabType, IVRSpawnable>(_comparer);
                _instances.Add(hand, dict);
            }

            IVRSpawnable existing = null;
            if (!dict.TryGetValue(spawnable.prefabType, out existing))
            {
                //// the type of prefab has not been spawned before, thus spawn it and store it

#if UNITY_EDITOR

                var helper = Core.Testing.ReuseSameInstanceHelper.instance;
                if (helper != null)
                {
                    var existingSpawnable = helper.GetInstance<IVRSpawnable>(spawnable.prefabType);
                    if (existingSpawnable != null)
                    {
                        spawnable = existingSpawnable;
                        output.overrideSpawnable = spawnable;
                    }
                }

#endif

                dict.Add(spawnable.prefabType, spawnable);
                return;
            }

            var existingPrefab = existing as VRPrefabBase;
            if (existingPrefab == null || existingPrefab.IsDestroyed())
            {
                //// the existing spawnable that was stored has been destroyed, so allow the spawning of a new one and store the new instance

#if UNITY_EDITOR

                var helper = Core.Testing.ReuseSameInstanceHelper.instance;
                if (helper != null)
                {
                    var existingSpawnable = helper.GetInstance<IVRSpawnable>(spawnable.prefabType);
                    if (existingSpawnable != null)
                    {
                        spawnable = existingSpawnable;
                        output.overrideSpawnable = spawnable;
                    }
                }

#endif

                dict[spawnable.prefabType] = spawnable;
                return;
            }

            var equippable = existing as IVREquippable;
            if (equippable != null && equippable.isEquipped)
            {
                // if previously/currently equipped, make sure it is unequipped
                equippable.hand.Unequip(equippable, Vector3.zero, Vector3.zero);
                output.stopExecution = true;
            }

            // There is an existing instance, move that to hand and disallow spawning of new one
            existing.position = hand.position;
            existing.rotation = hand.rotation;
            output.overrideSpawnable = existing;
        }
    }
}