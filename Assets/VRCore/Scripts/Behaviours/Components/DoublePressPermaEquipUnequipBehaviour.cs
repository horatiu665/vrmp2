namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// This behaviour will turn double-pressing an input into perma spawn-equipping.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class DoublePressPermaEquipUnequipBehaviour : VRCustomBehaviourBase
    {
        private static readonly IDictionary<IVRHand, IDictionary<VRPrefabType, PermaData>> _permaDict = new Dictionary<IVRHand, IDictionary<VRPrefabType, PermaData>>();
        private static readonly VRPrefabTypeComparer _comparer = new VRPrefabTypeComparer();

        [SerializeField, Range(0.1f, 10f)]
        private float _permaEquipThreshold = 0.4f;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip | VRBehaviourCapabilities.Unequip; }
        }

        private void Reset()
        {
            _priority = 50;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var hand = input.hand;
            var equippable = input.equippable as VREquippableBase;

            IDictionary<VRPrefabType, PermaData> dict;
            if (!_permaDict.TryGetValue(hand, out dict))
            {
                dict = new Dictionary<VRPrefabType, PermaData>(_comparer);
                _permaDict.Add(hand, dict);
            }

            var time = Time.timeSinceLevelLoad;
            var permaData = dict.GetValueOrDefault(equippable.prefabType);
            if (permaData.equippable == null || (time - permaData.lastEquip) > _permaEquipThreshold)
            {
                // First time the equippable is equipped in a while or ever
                dict[equippable.prefabType] = new PermaData()
                {
                    equippable = equippable,
                    lastEquip = time
                };

                return;
            }

            if ((time - permaData.lastEquip) < _permaEquipThreshold)
            {
                // the equippable was just spawned or was just equipped => thus, store the perma time as "last perma" so that it is not unequipped
                dict[equippable.prefabType] = new PermaData()
                {
                    equippable = permaData.equippable,
                    lastEquip = permaData.lastEquip,
                    lastPerma = time
                };
            }
        }

        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            var dict = _permaDict.GetValueOrDefault(input.hand);
            if (dict == null)
            {
                // no data for the hand in question
                return;
            }

            VREquippableBase equippable = input.equippable as VREquippableBase;
            if (equippable == null)
            {
                // no data stored about the equippable
                return;
            }

            var time = Time.timeSinceLevelLoad;
            var permaData = dict.GetValueOrDefault(equippable.prefabType);
            var timeGood = (time - permaData.lastPerma) < _permaEquipThreshold;
            output.stopExecution = permaData.equippable == equippable && timeGood;
        }

        private struct PermaData
        {
            public float lastEquip;
            public float lastPerma;
            public VREquippableBase equippable;
        }
    }
}