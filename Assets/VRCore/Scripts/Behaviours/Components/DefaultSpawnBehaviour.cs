namespace VRCore
{
    /// <summary>
    /// The default spawning behaviour simply places the spawnable in the hand.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class DefaultSpawnBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Spawn; }
        }

        private void Reset()
        {
            _priority = 1;
        }

        public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var spawnable = input.spawnable;
            var hand = input.hand;

            spawnable.position = hand.position;
            spawnable.rotation = hand.rotation;
        }
    }
}