namespace VRCore
{
    using UnityEngine;

    /// <summary>
    /// This behaviour checks whether an equippable of the same prefab type is already equipped, in which case it will disallow the equipping of a new one.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class OnePerTypeEquipBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip; }
        }

        private void Reset()
        {
            _priority = 100;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            if (input.hand.GetEquipped(input.equippable.prefabType) != null)
            {
                // Do not allow two equipped prefabs of the same type
                Debug.LogWarning(this.ToString() + " cannot equip a second equippable of the same type, type == " + input.equippable.prefabType.ToString());
                output.stopExecution = true;
            }
        }
    }
}