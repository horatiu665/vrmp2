namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// The default behaviour for highlighting equippables with an ouline shader for showing what would be equipped in any given situation.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class DefaultHighlightBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private Material _outlineMaterial = null;

        [SerializeField, Range(0.1f, 60f), Tooltip("How many times per second this behaviour checks for adding outline to the nearest equippable.")]
        private float _checkFrequency = 10f;

        private KeyValuePair<IVRHand, IVREquippable> _lastNearestLeft, _lastNearestRight;
        private float _lastCheck;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Highlight; }
        }

        private void Reset()
        {
            _priority = 1;
        }

        public override void OnHighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output)
        {
            var hand = input.hand;
            if (!((VRNetwork.INetPlayer)hand.player).isLocal)
            {
                // only highlight for local players
                return;
            }

            if (hand.equipped.Count > 0)
            {
                // Do not highlight if the hand already has something equipped
                output.stopExecution = true;
                return;
            }

            if (hand.isLeft && _lastNearestLeft.Value == null)
            {
                _lastNearestLeft = new KeyValuePair<IVRHand, IVREquippable>(hand, null);
            }
            else if (!hand.isLeft && _lastNearestRight.Value == null)
            {
                _lastNearestRight = new KeyValuePair<IVRHand, IVREquippable>(hand, null);
            }

            //Debug.Log(this.ToString() + " OnHighlight == " + equippable.ToString() + " @ " + hand.ToString());
        }

        private void Update()
        {
            var time = Time.timeSinceLevelLoad;
            if (time < _lastCheck)
            {
                return;
            }

            if (_lastNearestLeft.Key != null)
            {
                // something was outlined at some point in left hand
                UpdateOutline(_lastNearestLeft);
            }

            if (_lastNearestRight.Key != null)
            {
                // something was outlined at some point in right hand
                UpdateOutline(_lastNearestRight);
            }

            // only time throttle if nothing changed, since we may need to check (the other hand) again next frame otherwise
            _lastCheck = time + (1f / _checkFrequency);
        }

        private void UpdateOutline(KeyValuePair<IVRHand, IVREquippable> pair)
        {
            var hand = pair.Key;
            var lastNearest = pair.Value;

            if (hand.equipped.Count > 0 && lastNearest != null)
            {
                // hand has something equipped, and something was previously highlighted => outline it
                RemoveOutline(lastNearest);
                SetLast(hand.isLeft, null, null);

                return;
            }

            var nearest = hand.GetNearestHighlighted();
            if (nearest != lastNearest)
            {
                // The now nearest does not match the previous (even if either of them is null)
                if (lastNearest != null)
                {
                    // The previously outlined was null => since we are changing what should be outlined now, remove outline
                    RemoveOutline(lastNearest);
                }

                if (nearest != null)
                {
                    // If the now-nearest is not null, add outline to it
                    AddOutline(nearest);
                }
            }

            SetLast(hand.isLeft, hand, nearest);
        }

        private void SetLast(bool isLeft, IVRHand hand, IVREquippable nearest)
        {
            if (isLeft)
            {
                _lastNearestLeft = new KeyValuePair<IVRHand, IVREquippable>(hand, nearest);
            }
            else
            {
                _lastNearestRight = new KeyValuePair<IVRHand, IVREquippable>(hand, nearest);
            }
        }

        private void AddOutline(IVREquippable equippable)
        {
            var renderers = equippable.renderers;
            for (int i = 0; i < renderers.Length; i++)
            {
                //renderers[i].materials = renderers[i].materials.Concat(new Material[] { _outlineMaterial });
                renderers[i].sharedMaterials = renderers[i].sharedMaterials.Concat(new Material[] { _outlineMaterial });
            }
        }

        private void RemoveOutline(IVREquippable equippable)
        {
            var renderers = equippable.renderers;
            for (int i = 0; i < renderers.Length; i++)
            {
                //var materials = renderers[i].materials;
                var materials = renderers[i].sharedMaterials;
                var index = -1;
                for (int j = 0; j < materials.Length; j++)
                {
                    if (materials[j] != null)
                    {
                        if (materials[j].shader == _outlineMaterial.shader)
                        {
                            index = j;
                            break;
                        }
                    }
                }

                if (index != -1)
                {
                    renderers[i].sharedMaterials = materials.RemoveAt(index);
                    //renderers[i].materials = materials.RemoveAt(index);
                }
            }
        }

#if UNITY_EDITOR

        [DebugButton]
        private void PrintLastNearest()
        {
            if (_lastNearestLeft.Key != null && _lastNearestLeft.Value != null)
            {
                Debug.Log(this.ToString() + " last nearest left == " + _lastNearestLeft.Value.ToString());
            }
            else
            {
                Debug.Log(this.ToString() + " nothing in left hand");
            }

            if (_lastNearestRight.Key != null && _lastNearestRight.Value != null)
            {
                Debug.Log(this.ToString() + " last nearest right == " + _lastNearestRight.Value.ToString());
            }
            else
            {
                Debug.Log(this.ToString() + " nothing in right hand");
            }
        }

#endif
    }
}