namespace VRCore
{
    using VRNetwork;

    /// <summary>
    /// This behaviour allows only the local player to spawn or equip.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class OnlyLocalPlayerEquipSpawnBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip | VRBehaviourCapabilities.Spawn; }
        }

        private void Reset()
        {
            _priority = 200;
        }

        public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var player = input.player as INetPlayer;
            if (player == null)
            {
                // the player is not a net player, may not be a networked game
                return;
            }

            if (NetServices.isClient && !player.isLocal)
            {
                output.stopExecution = true;
            }
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var player = input.player as INetPlayer;
            if (player == null)
            {
                // the player is not a net player, may not be a networked game
                return;
            }

            if (NetServices.isClient && !player.isLocal)
            {
                output.stopExecution = true;
            }
        }
    }
}