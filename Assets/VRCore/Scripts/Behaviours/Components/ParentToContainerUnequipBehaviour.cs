namespace VRCore
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;

    [Apex.ApexComponent("Behaviours")]
    public class ParentToContainerUnequipBehaviour : VRCustomBehaviourBase
    {
        static Transform _container;
        static Transform container
        {
            get
            {
                if (_container == null)
                {
                    _container = new GameObject("[generated] Unequip container", typeof(MoveChildrenOnOriginShift)).transform;
                }
                return _container;
            }
        }

        private void Reset()
        {
            _priority = 10;
        }

        public override VRBehaviourCapabilities capabilities
        {
            get
            {
                return VRBehaviourCapabilities.Unequip;
            }
        }

        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            // set parent to a static object container that also moves with the origin shift
            input.equippable.transform.SetParent(container);

        }
    }
}