namespace VRCore
{
    using UnityEngine;

    /// <summary>
    /// The default unequipping behaviour - it assumes a rigidbody on the equippable, and it assumes that the equippable is equipped through a joint on the hand.
    /// This behaviour will disconnect and deactivate the joint used for the equipment, it sets velocities for throwing the equippable + it shows the controller model.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class DefaultUnequipBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private bool _showControllerModel = true;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Unequip; }
        }

        private void Reset()
        {
            _priority = 1;
        }

        // happens on update, which means that the physics stuff happens for the previous physics frame, which may be a bit behind.
        // that's why we fake the forces and shit to happen for the "next" frame, by adding the player's velocity to the mix.
        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            var hand = input.hand;
            var equippable = input.equippable;
            var rb = equippable.rigidbody;

            // deactivate joint, but first save throw data
            var joint = hand.GetConnectedJoint(equippable.rigidbody);
            var relPos = rb.position - joint.transform.position;
            var relRot = Quaternion.Inverse(joint.transform.rotation) * rb.transform.rotation;
            if (joint != null)
            {
                // disconnect the joint and deactivate its game object
                joint.connectedBody = null;
                joint.gameObject.SetActive(false);
            }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            else
            {
                Debug.LogWarning(this.ToString() + " did not find a Joint for the equipped == " + equippable.ToString());
            }
#endif

            // set rb position to hand (if we leave it to the last joint position and player is moving, the joint is lagging behind)
            // also add another frame of physics cause this function runs at Update() but the physics system will be behind with about 1 frame.
            // this seems to fix it after testing in singleplayer
            var futurePosOffset = input.velocity * Time.fixedDeltaTime;
            rb.transform.position = hand.transform.position + relPos + futurePosOffset;
            rb.transform.rotation = hand.transform.rotation * relRot;

            // apply velocities
            rb.velocity = input.velocity;
            rb.angularVelocity = input.angularVelocity;
            var newAV = rb.angularVelocity.magnitude;
            if (rb.maxAngularVelocity < newAV)
            {
                rb.maxAngularVelocity = newAV;
            }

            if (_showControllerModel && hand.equipped.Count == 1)
            {
                // if this is the last equipped object being unequipped
                if (hand.controller != null)
                {
                    hand.controller.ShowControllerModel();
                }
            }

            //Debug.Log(this.ToString() + " OnUnequip == " + equippable.ToString() + ", rb == " + equippable.rigidbody.ToString() + ", hand equipped count == " + hand.equipped.Count.ToString());
        }
    }
}