namespace Core
{
    using UnityEngine;
    using VRCore;

    public sealed class NotForPlayerTypeEquipBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private PlayerType _playerType = 0;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip; }
        }

        private void Reset()
        {
            _priority = 200;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var player = input.player as IPlayer;
            if (player == null)
            {
                return;
            }

            if (player.playerType == _playerType)
            {
                output.stopExecution = true;
            }
        }
    }
}