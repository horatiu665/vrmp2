namespace VRCore
{
    using UnityEngine;

    public sealed class NoStealingHighlightBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Highlight; }
        }

        private void Reset()
        {
            _priority = 200;
        }

        public override void OnHighlight(VRHighlightBehaviourInput input, VRHighlightBehaviourOutput output)
        {
            base.OnHighlight(input, output);

            // if already grabbed, unhighlight right away
            if (input.equippable.isEquipped)
            {
                output.stopExecution = true;
                input.equippable.hand.Unhighlight(input.equippable);

            }
        }
    }
}