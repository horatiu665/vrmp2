namespace VRCore
{
    using UnityEngine;

    /// <summary>
    /// The default equip behaviour - it assumes that the equippable has a rigidbody and it uses a joint on the hand to connect it. Also hides the controller model.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class DefaultEquipBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private bool _hideControllerModel = true;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip; }
        }

        private void Reset()
        {
            _priority = 2;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var hand = input.hand;
            var equippable = input.equippable;

            // need to reset rigidbody velocities
            var rb = equippable.rigidbody;
            if (rb != null)
            {
                rb.velocity = rb.angularVelocity = Vector3.zero;
                rb.isKinematic = false;
            }

            // Get or create a joint to attach the equippable to
            var joint = hand.GetOrCreateJoint();
            if (joint != null)
            {
                equippable.position = joint.transform.position;
                equippable.rotation = joint.transform.rotation;
                joint.connectedBody = equippable.rigidbody;
            }

#if UNITY_EDITOR || DEVELOPMENT_BUILD
            else
            {
                Debug.LogWarning(this.ToString() + "  did not find a vacant Joint for the newly equipped == " + equippable.ToString());
            }
#endif

            if (_hideControllerModel && hand.controller != null)
            {
                hand.controller.HideControllerModel();
            }
        }
    }
}