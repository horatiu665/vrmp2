namespace VRCore
{
    using UnityEngine;

    /// <summary>
    /// This behaviour checks whether an equippable of the same type as the now equipped is already present in the hand, in which case it will unequip the existing.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class UnequipSameTypeEquipBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip; }
        }

        private void Reset()
        {
            _priority = 25;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var hand = input.hand;
            var existing = hand.GetEquipped(input.equippable.prefabType);
            if (existing != null)
            {
                // There is an existing equipped of the same prefab type => Unequip it!
                hand.Unequip(existing, Vector3.zero, Vector3.zero);
            }
        }
    }
}