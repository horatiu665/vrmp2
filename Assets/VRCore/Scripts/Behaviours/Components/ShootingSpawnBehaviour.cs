﻿namespace VRCore
{
    using UnityEngine;

    public sealed class ShootingSpawnBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private Vector3 _shootModifier = new Vector3(0f, 1.5f, 0f);

        [SerializeField]
        private float _shootPower = 1.25f;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Spawn; }
        }

        private void Reset()
        {
            _priority = 0;
        }

        public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var rb = input.spawnable.transform.GetComponent<Rigidbody>(); // TODO: may want to optimize this out
            if (rb != null)
            {
                var direction = input.hand.transform.forward;
                var force = (direction + _shootModifier) * _shootPower;
                rb.AddForce(force, ForceMode.Impulse);
            }
        }
    }
}