namespace VRCore
{
    /// <summary>
    /// This behaviour will set the parent of the equippable to the player or the player's equippable container on equip, and de-parent it on unequip.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class ParentToPlayerEquipUnequipBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip | VRBehaviourCapabilities.Unequip; }
        }

        private void Reset()
        {
            _priority = 5;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var player = input.player;
            var parent = player.equippablesContainer != null ? player.equippablesContainer.transform : player.transform;
            input.equippable.transform.SetParent(parent);
        }

        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            input.equippable.transform.SetParent(null);
        }
    }
}