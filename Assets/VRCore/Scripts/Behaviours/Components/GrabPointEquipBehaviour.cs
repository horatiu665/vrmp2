namespace VRCore
{
    using UnityEngine;

    [Apex.ApexComponent("Behaviours")]
    public sealed class GrabPointEquipBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private float _positionWeight = 1f;

        [SerializeField]
        private float _rotationWeight = 1f;

        [SerializeField]
        private bool _hideControllerModel = true;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip; }
        }

        private void Reset()
        {
            _priority = 2;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            var hand = input.hand;
            var equippable = input.equippable;

            // find the connected joint, if one is connected
            var joint = hand.GetConnectedJoint(equippable.rigidbody);
            if (joint != null)
            {
                // disconnect the joint
                joint.connectedBody = null;
            }
            else
            {
                // No joint found, so create a new one
                joint = hand.GetOrCreateJoint();
            }

            // TODO: Might want to optimize this lookup away
            var grabPoints = equippable.transform.GetComponentsInChildren<GrabPointComponent>();
            if (grabPoints.Length > 0)
            {
                var handPos = hand.position;
                var handRot = hand.rotation.eulerAngles;
                var shortest = float.MaxValue;
                GrabPointComponent nearestGrabPoint = null;
                for (int i = 0; i < grabPoints.Length; i++)
                {
                    var gp = grabPoints[i];
                    var distanceSqr = (handPos - gp.transform.position).sqrMagnitude * _positionWeight;
                    var deltaRotSqr = (handRot - gp.transform.rotation.eulerAngles).sqrMagnitude * _rotationWeight;

                    var combinedDelta = distanceSqr + deltaRotSqr;
                    if (combinedDelta < shortest)
                    {
                        nearestGrabPoint = gp;
                        shortest = combinedDelta;
                    }
                }

                // RotateTheRightWay(hand, equippable, grabPoint);
                RotateTheRightWaySelfieTennisMethod(hand, equippable, nearestGrabPoint.transform);
            }

            equippable.rigidbody.velocity = equippable.rigidbody.angularVelocity = Vector3.zero;

            joint.connectedBody = equippable.rigidbody;
            equippable.rigidbody.isKinematic = false;

            if (_hideControllerModel && hand.controller != null)
            {
                hand.controller.HideControllerModel();
            }
        }

        private void RotateTheRightWaySelfieTennisMethod(IVRHand hand, IVREquippable equippable, Transform grabPointObj)
        {
            var objectGrabPoint = grabPointObj;
            var grabPoint = hand.transform;

            Vector3 selOffset;
            selOffset = grabPoint.position - objectGrabPoint.position;
            equippable.transform.position += selOffset;

            Quaternion selOffsetRot;
            selOffsetRot = Quaternion.Inverse(objectGrabPoint.rotation) * grabPoint.rotation;

            //var newObj = new GameObject();
            var newObj = VRHelper.dummy;
            newObj.transform.position = grabPoint.position;
            newObj.transform.rotation = objectGrabPoint.rotation;
            var oldSelectedObjParent = equippable.transform.parent;
            equippable.transform.SetParent(newObj.transform);
            newObj.transform.rotation *= selOffsetRot;
            equippable.transform.SetParent(oldSelectedObjParent);
            //Destroy(newObj);
            newObj.SetActive(false);
        }

        //private void RotateTheRightWay(IVRHand hand, IVREquippable equippable, Transform grabPoint)
        //{
        //    // equippable.transform must be rotated and translated in such a way that grabPoint.transform and hand.transform are aligned perfectly.

        //}
    }
}