namespace VRCore
{
    using UnityEngine;
    using VRNetwork;

    /// <summary>
    /// This behaviour will re-write the spawn manager mapping in order to keep spawning a prefab of the same type.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class RemapSpawnMappingEquipSpawnBehaviour : VRCustomBehaviourBase
    {
        [SerializeField]
        private VRInput _equipButtonToAffect = VRInput.Grip;

        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Equip | VRBehaviourCapabilities.Spawn; }
        }

        private void Reset()
        {
            _priority = 0;
        }

        public override void OnEquip(VREquipBehaviourInput input, VREquipBehaviourOutput output)
        {
            if (!((INetPlayer)input.hand.player).isLocal)
            {
                return;
            }

            SpawnManagerPreviewable.instance.Remap(input.hand.isLeft, _equipButtonToAffect, input.equippable.prefabType);
        }
    }
}