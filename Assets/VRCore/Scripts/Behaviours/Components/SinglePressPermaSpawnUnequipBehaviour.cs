namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    /// <summary>
    /// This behaviour will perma equip an equippable when it is single-click-spawned.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class SinglePressPermaSpawnUnequipBehaviour : VRCustomBehaviourBase
    {
        private static readonly IDictionary<IVRHand, IDictionary<VRPrefabType, PermaData>> _dict = new Dictionary<IVRHand, IDictionary<VRPrefabType, PermaData>>();
        private static readonly VRPrefabTypeComparer _comparer = new VRPrefabTypeComparer();

        [SerializeField, Range(0.1f, 10f)]
        private float _permaEquipThreshold = 0.4f;

        public override VRBehaviourCapabilities capabilities
        {
            get
            {
                return VRBehaviourCapabilities.Spawn | VRBehaviourCapabilities.Unequip;
            }
        }

        private void Reset()
        {
            _priority = 50;
        }

        public override void OnSpawn(VRSpawnBehaviourInput input, VRSpawnBehaviourOutput output)
        {
            var equippable = input.spawnable as IVREquippable;
            if (equippable == null)
            {
                // the spawnable cannot be equipped, thus this behaviour does not apply
                return;
            }

            var hand = input.hand;
            IDictionary<VRPrefabType, PermaData> dict;
            if (!_dict.TryGetValue(hand, out dict))
            {
                dict = new Dictionary<VRPrefabType, PermaData>(_comparer);
                _dict.Add(hand, dict);
            }

            var oldPermaData = dict.GetValueOrDefault(equippable.prefabType);
            if (oldPermaData.equippable != null && hand.GetConnectedJoint(oldPermaData.equippable.rigidbody) != null && oldPermaData.equippable != equippable)
            {
                output.overrideSpawnable = (IVRSpawnable)oldPermaData.equippable;
                return;
            }

            dict[equippable.prefabType] = new PermaData()
            {
                timestamp = Time.timeSinceLevelLoad + _permaEquipThreshold,
                equippable = equippable,
            };
        }

        public override void OnUnequip(VRUnequipBehaviourInput input, VRUnequipBehaviourOutput output)
        {
            var dict = _dict.GetValueOrDefault(input.hand);
            if (dict == null)
            {
                return;
            }

            var data = dict.GetValueOrDefault(input.equippable.prefabType);
            var timestamp = data.timestamp;
            output.stopExecution = input.equippable == data.equippable && (timestamp > 0f && Time.timeSinceLevelLoad < timestamp);
        }

        private struct PermaData
        {
            public float timestamp;
            public IVREquippable equippable;
        }
    }
}