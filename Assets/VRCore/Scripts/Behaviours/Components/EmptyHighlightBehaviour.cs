namespace VRCore
{
    /// <summary>
    /// A highlight behaviour that does nothing. Good for remote players over network.
    /// </summary>
    /// <seealso cref="VRCore.VRCustomBehaviourBase" />
    [Apex.ApexComponent("Behaviours")]
    public sealed class EmptyHighlightBehaviour : VRCustomBehaviourBase
    {
        public override VRBehaviourCapabilities capabilities
        {
            get { return VRBehaviourCapabilities.Highlight; }
        }

        private void Reset()
        {
            _priority = -1;
        }
    }
}