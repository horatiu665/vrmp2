﻿namespace VRCore
{
    /// <summary>
    /// Interface that (optionally) integrates internally with <see cref="IVRInputReceiver"/>.
    /// Can be added as component to VR players (camera rig).
    /// </summary>
    public interface IVRInputController
    {
        /// <summary>
        /// Called while a VR input is touched, meaning a halfway press.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        void OnTouch(bool isLeft, VRInput input);

        /// <summary>
        /// Called when a VR input is touched down (and was previously up), meaning a halfway press down.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        void OnTouchDown(bool isLeft, VRInput input);

        /// <summary>
        /// Called when a VR input is touched up (and was previously down), meaning a halfway press up.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        void OnTouchUp(bool isLeft, VRInput input);

        /// <summary>
        /// Called while a VR input is pressed.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        void OnPress(bool isLeft, VRInput input);

        /// <summary>
        /// Called when a VR input is pressed down (and was previously up).
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        void OnPressDown(bool isLeft, VRInput input);

        /// <summary>
        /// Called when a VR input is pressed up (and was previously down).
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        void OnPressUp(bool isLeft, VRInput input);

        /// <summary>
        /// Re-initializes the controllers using each other's data, effectively swapping the hands.
        /// </summary>
        void SwapHands();
    }
}