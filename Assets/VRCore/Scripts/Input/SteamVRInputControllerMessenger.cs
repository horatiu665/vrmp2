namespace VRCore
{
    using UnityEngine;

    [RequireComponent(typeof(SteamVR_ControllerManager))]
    public class SteamVRInputControllerMessenger<TPlayer> : VRInputControllerMessenger<TPlayer, SteamVRInputReceiver> where TPlayer : IVRPlayer
    {
        protected SteamVR_ControllerManager _controllerManager;

        protected virtual void Start()
        {
            _controllerManager = this.GetComponent<SteamVR_ControllerManager>();

            _inputReceiver.Initialize(this,
                _player.leftHand, _controllerManager.left.GetComponent<SteamVR_TrackedObject>(),
                _player.rightHand, _controllerManager.right.GetComponent<SteamVR_TrackedObject>(),
                _config);
        }
    }
}