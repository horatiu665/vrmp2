﻿namespace VRCore
{
    /// <summary>
    /// Represents <see cref="SteamVR_Controller.ButtonMask"/> and <see cref="Valve.VR.EVRButtonId"/> in one common implementation.
    /// </summary>
    public enum VRInput
    {
        System = 0,
        ApplicationMenu = 1,
        Grip = 2,
        Touchpad = 32,
        Trigger = 33,
        Axis0 = 32,
        Axis1 = 33,
        Axis2 = 34,
        Axis3 = 35,
        Axis4 = 36
    }

}