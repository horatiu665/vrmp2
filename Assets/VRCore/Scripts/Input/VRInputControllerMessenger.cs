﻿namespace VRCore
{
    using Apex.Services;

    public abstract class VRInputControllerMessenger<T, TInputReceiver> : VRInputControllerBase<T, TInputReceiver> where T : IVRPlayer where TInputReceiver : IVRInputReceiver, new()
    {
        private readonly VRInputPressUp _pressUp = new VRInputPressUp();
        private readonly VRInputPressDown _pressDown = new VRInputPressDown();
        private readonly VRInputPress _press = new VRInputPress();

        private readonly VRInputTouchUp _touchUp = new VRInputTouchUp();
        private readonly VRInputTouchDown _touchDown = new VRInputTouchDown();
        private readonly VRInputTouch _touch = new VRInputTouch();

        /// <summary>
        /// Called when a VR input is pressed up (and was previously down).
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public override void OnPressUp(bool isLeft, VRInput input)
        {
            PostMessage(_pressUp, isLeft, input);
        }

        /// <summary>
        /// Called when a VR input is pressed down (and was previously up).
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public override void OnPressDown(bool isLeft, VRInput input)
        {
            PostMessage(_pressDown, isLeft, input);
        }

        /// <summary>
        /// Called while a VR input is pressed.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public override void OnPress(bool isLeft, VRInput input)
        {
            PostMessage(_press, isLeft, input);
        }

        /// <summary>
        /// Called when a VR input is touched up (and was previously down), meaning a halfway press up.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public override void OnTouchUp(bool isLeft, VRInput input)
        {
            PostMessage(_touchUp, isLeft, input);
        }

        /// <summary>
        /// Called when a VR input is touched down (and was previously up), meaning a halfway press down.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public override void OnTouchDown(bool isLeft, VRInput input)
        {
            PostMessage(_touchDown, isLeft, input);
        }

        /// <summary>
        /// Called while a VR input is touched, meaning a halfway press.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public override void OnTouch(bool isLeft, VRInput input)
        {
            PostMessage(_touch, isLeft, input);
        }

        /// <summary>
        /// Posts the message.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="msg">The message.</param>
        /// <param name="isLeft">if set to <c>true</c> [is left].</param>
        /// <param name="input">The input.</param>
        private void PostMessage<TMessage>(TMessage msg, bool isLeft, VRInput input) where TMessage : VRInputMessageBase
        {
            msg.player = _player;
            msg.hand = isLeft ? _player.leftHand : _player.rightHand;
            msg.input = input;
            msg.isLeft = isLeft;
            msg.isRight = !isLeft;
            GameServices.messageBus.Post(msg);
        }
    }
}