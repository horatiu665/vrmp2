﻿namespace VRCore
{
    using UnityEngine;

    [RequireComponent(typeof(IVRPlayer))]
    public abstract class VRInputControllerBase<TPlayer, TInputReceiver> : MonoBehaviour, IVRInputController where TPlayer : IVRPlayer where TInputReceiver : IVRInputReceiver, new()
    {
        protected readonly TInputReceiver _inputReceiver = new TInputReceiver();

        [Header("Throwing")]
        [SerializeField, Range(0.1f, 100f), Tooltip("A multiplier applied to the velocity of thrown objects.")]
        protected float _velocityMultiplier = 1f;

        [SerializeField, Range(0.1f, 100f), Tooltip("A multiplier applied to the angular velocity of thrown objects.")]
        protected float _angularVelocityMultiplier = 1f;

        protected VRInputConfig _config;
        protected TPlayer _player;

        /// <summary>
        /// Gets the left controller.
        /// </summary>
        /// <value>
        /// The left controller.
        /// </value>
        public IVRController leftController
        {
            get
            {
                if (!_inputReceiver.initialized)
                {
                    Debug.LogError(this.ToString() + " cannot query left VRController before the input receiver has been initialized");
                    return null;
                }

                return _inputReceiver.leftController;
            }
        }

        /// <summary>
        /// Gets the right controller.
        /// </summary>
        /// <value>
        /// The right controller.
        /// </value>
        public IVRController rightController
        {
            get
            {
                if (!_inputReceiver.initialized)
                {
                    Debug.LogError(this.ToString() + " cannot query right VRController before the input receiver has been initialized");
                    return null;
                }

                return _inputReceiver.rightController;
            }
        }

        /// <summary>
        /// Called by Unity when enabled.
        /// </summary>
        protected virtual void OnEnable()
        {
            _player = this.GetComponent<TPlayer>();
            _config = new VRInputConfig()
            {
                velocityMultiplier = _velocityMultiplier,
                angularVelocityMultiplier = _angularVelocityMultiplier
            };
        }

#if UNITY_EDITOR

        protected virtual void OnValidate()
        {
            if (_config == null)
            {
                _config = new VRInputConfig()
                {
                    velocityMultiplier = _velocityMultiplier,
                    angularVelocityMultiplier = _angularVelocityMultiplier
                };

                return;
            }

            if (_velocityMultiplier != _config.velocityMultiplier)
            {
                _config.velocityMultiplier = _velocityMultiplier;
            }

            if (_angularVelocityMultiplier != _config.angularVelocityMultiplier)
            {
                _config.angularVelocityMultiplier = _angularVelocityMultiplier;
            }
        }

#endif

        /// <summary>
        /// Updates this VR input controller, must be called every frame for maximum accuracy.
        /// </summary>
        protected virtual void Update()
        {
            _inputReceiver.Update();
        }

        /// <summary>
        /// Called while a VR input is pressed.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public virtual void OnPress(bool isLeft, VRInput input)
        {
        }

        /// <summary>
        /// Called when a VR input is pressed down (and was previously up).
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public virtual void OnPressDown(bool isLeft, VRInput input)
        {
        }

        /// <summary>
        /// Called when a VR input is pressed up (and was previously down).
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public virtual void OnPressUp(bool isLeft, VRInput input)
        {
        }

        /// <summary>
        /// Called while a VR input is touched, meaning a halfway press.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public virtual void OnTouch(bool isLeft, VRInput input)
        {
        }

        /// <summary>
        /// Called when a VR input is touched down (and was previously up), meaning a halfway press down.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public virtual void OnTouchDown(bool isLeft, VRInput input)
        {
        }

        /// <summary>
        /// Called when a VR input is touched up (and was previously down), meaning a halfway press up.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> the touch came from the left hand, otherwise right.</param>
        /// <param name="input">The VR input.</param>
        public virtual void OnTouchUp(bool isLeft, VRInput input)
        {
        }

        /// <summary>
        /// Re-initializes the controllers using each other's data, effectively swapping the hands.
        /// </summary>
        public void SwapHands()
        {
            _inputReceiver.SwapHands();
        }
    }
}