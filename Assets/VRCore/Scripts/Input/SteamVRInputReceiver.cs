﻿namespace VRCore
{
    public sealed class SteamVRInputReceiver : VRInputReceiverBase<SteamVRController>
    {
        /// <summary>
        /// Initializes this input receiver, using the specified input controller.
        /// </summary>
        /// <param name="inputController">The input controller.</param>
        /// <param name="player">The player.</param>
        /// <param name="leftController">The left controller.</param>
        /// <param name="rightController">The right controller.</param>
        /// <param name="velocityMultiplier">The velocity multiplier.</param>
        /// <param name="angularVelocityMultiplier">The angular velocity multiplier.</param>
        public void Initialize(IVRInputController inputController, IVRHand leftHand, SteamVR_TrackedObject leftController, IVRHand rightHand, SteamVR_TrackedObject rightController, VRInputConfig config)
        {
            _inputController = inputController;
            _leftController.Initialize(leftHand, leftController, config);
            _rightController.Initialize(rightHand, rightController, config);

            this.initialized = true;
        }

        /// <summary>
        /// Re-initializes the controllers using each other's data, effectively swapping the hands.
        /// </summary>
        public override void SwapHands()
        {
            var hand = _leftController.hand;
            var trackedObj = _leftController.trackedObject;
            var config = _leftController.inputConfig;

            _leftController.Initialize(_rightController.hand, _rightController.trackedObject, _rightController.inputConfig);
            _rightController.Initialize(hand, trackedObj, config);
        }
    }
}