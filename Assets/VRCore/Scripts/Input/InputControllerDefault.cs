namespace VRCore
{
    using System.Collections.Generic;
    using Apex.Utilities;
    using UnityEngine;

    [Apex.ApexComponent("Player")]
    [RequireComponent(typeof(IVRPlayer))]
    public class InputControllerDefault : SteamVRInputControllerMessenger<IVRPlayer>
    {
        [Header("Button Mapping")]
        [SerializeField]
        private bool _allowEquippingFromWorld = true;

        [SerializeField]
        private VRInput _equipButton = VRInput.Trigger;

        [SerializeField]
        private VRInput _storeButton = VRInput.Grip;

        [Header("Debugging")]
        [SerializeField, ReadOnly]
        private bool _leftEquipped = false;

        [SerializeField, ReadOnly]
        private bool _rightEquipped = false;

        // TODO: UGLY IMPLEMENTATION; REVISIT!
        public static InputControllerDefault instance { get; private set; }

        private void Awake()
        {
            if (instance != null)
            {
                Debug.LogError(this.ToString() + " another instance of a InputControllerDefault is already registered, destroying this one!");
                Destroy(this);
                return;
            }

            instance = this;
        }

        // equips while the button is pressed, if something comes within range.
        public override void OnPress(bool isLeft, VRInput input)
        {
            base.OnPress(isLeft, input);

            if (_player.disallowEquipping || !_allowEquippingFromWorld)
            {
                // player is not allowed to equip
                return;
            }

            if ((isLeft && _leftEquipped) || (!isLeft && _rightEquipped))
            {
                // Limit to one equipment per press down
                return;
            }

            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;
            if (hand.equipped.Count > 0)
            {
                // that hand already has something equipped, thus disallow equipping something new
                return;
            }

            if (hand.highlighted.Count == 0)
            {
                // hand has no things currently highlighted, nothing to equip
                return;
            }

            var nearestHighlighted = hand.GetNearestHighlighted();
            if (nearestHighlighted == null)
            {
                // the nearest highlighted is for some reason null - it shouldn't be though!
                return;
            }

            var result = false;
            var manager = SpawnManagerPreviewable.instance;
            if (manager.HasMapping(isLeft, input, nearestHighlighted.prefabType))
            {
                // If the nearest highlighted object has a mapping corresponding to the current input, equip it
                result = Equip(nearestHighlighted, isLeft);
            }
            else if (!manager.HasMapping(isLeft, nearestHighlighted.prefabType) && input == _equipButton)
            {
                // if the nearest highlighted object has no mapping, only equip it if the input is the intended equip button
                result = Equip(nearestHighlighted, isLeft);
            }
            else if (input == _storeButton)
            {
                // store the object by incrementing spawn count and removing it (in singleplayer, at least)
                HandleStorage(nearestHighlighted, input, isLeft);
                result = true;
            }

            if (result)
            {
                if (isLeft)
                {
                    _leftEquipped = result;
                }
                else
                {
                    _rightEquipped = result;
                }
            }
        }

        // on press down, tries to equip highlighted item if possible, otherwise try to spawn using this input and the spawn manager.
        public override void OnPressDown(bool isLeft, VRInput input)
        {
            base.OnPressDown(isLeft, input);

            if (_player.disallowSpawning)
            {
                // player is not allowed to spawn
                return;
            }

            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;
            var manager = SpawnManagerPreviewable.instance;

            // Ensure that no objects can be equipped, before allowing the spawning of a new object
            if (hand.highlighted.Count > 0)
            {
                if (input == _equipButton && _allowEquippingFromWorld)
                {
                    // if we are pressing the equip button, and something is highlighted, definitely disallow spawning!
                    return;
                }

                // this loop checks each highlighted object, and if it is part of the spawn manager mapping, it doesn't allow spawning. that is foolish and fucked. the cockblocking should be left for the individual behaviours, not cockblocked by the system
                //var count = hand.highlighted.Count;
                //for (int i = 0; i < count; i++)
                //{
                //    var highlighted = hand.highlighted[i];
                //    if (manager.HasMapping(isLeft, highlighted.prefabType))
                //    {
                //        // at least one of the highlighted objects HAS a mapping on the spawn manager => so disallow spawning!
                //        // but only if the highlighted obj is not equipped, f.x. it was just dropped or thrown at us
                //        if (!highlighted.isEquipped)
                //        {
                //            return;
                //        }
                //    }
                //}
            }

            var mapping = ListBufferPool.GetBuffer<SpawnSetup>(1);
            manager.GetMapped(isLeft, input, mapping);
            if (mapping.Count > 0)
            {
                // This input has some mapping on the spawn manager
                HandleSpawning(mapping, input, isLeft);
            }

            ListBufferPool.ReturnBuffer(mapping);
        }

        // if anything is equipped using the default equip or one of the spawn manager mappings, drops that item from the hand.
        public override void OnPressUp(bool isLeft, VRInput input)
        {
            base.OnPressUp(isLeft, input);

            if (isLeft)
            {
                _leftEquipped = false;
            }
            else
            {
                _rightEquipped = false;
            }

            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;
            var equipped = hand.equipped;
            var count = equipped.Count;
            if (count == 0)
            {
                // hand has nothing equipped, thus nothing to unequip
                return;
            }

            var manager = SpawnManagerPreviewable.instance;

            // First check whether the current input is the equip button
            if (_allowEquippingFromWorld)
            {
                if (input == _equipButton)
                {
                    // If so, find all currently equipped objects which are not mapped - these must be unequipped with the equip button
                    for (int i = count - 1; i >= 0; i--)
                    {
                        var equip = equipped[i];
                        if (manager.HasMapping(isLeft, equip.prefabType))
                        {
                            // If the prefab type has ANY mapping (any input) - then it cannot be unequipped with equip button
                            continue;
                        }

                        // Found an equippable which has no mapping in the spawn manager - thus, unequip it
                        Unequip(equip, isLeft);
                    }
                }
            }

            var mapping = ListBufferPool.GetBuffer<SpawnSetup>(1);
            manager.GetMapped(isLeft, input, mapping);
            var mcount = mapping.Count;
            if (mcount > 0)
            {
                // For all other inputs than the equip button, check if they have a mapping on the spawn manager
                count = equipped.Count;
                for (int i = count - 1; i >= 0; i--)
                {
                    // For all currently equipped that do have a mapping matching the current input => unequip them!
                    var equip = equipped[i];

                    for (int j = 0; j < mcount; j++)
                    {
                        if (mapping[j].prefabType == equip.prefabType)
                        {
                            // found an equippable with a matching mapping
                            Unequip(equip, isLeft);
                            break;
                        }
                    }
                }
            }

            ListBufferPool.ReturnBuffer(mapping);
        }

        protected virtual void HandleStorage(IVREquippable equippable, VRInput input, bool isLeft)
        {
            ((SpawnManagerPreviewable)SpawnManagerPreviewable.instance).AdjustSpawnCount(isLeft, equippable.prefabType, 1);
            VRPrefabManager.instance.Return(equippable);
        }

        protected virtual void HandleSpawning(IList<SpawnSetup> mapping, VRInput input, bool isLeft)
        {
            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;

            var prefabs = ListBufferPool.GetBuffer<IVRSpawnable>(mapping.Count);
            var manager = (SpawnManagerPreviewable)SpawnManagerPreviewable.instance;
            manager.Spawn(hand, input, prefabs); // Spawn a list of the mapped prefabs depending on the input

            var count = prefabs.Count;
            for (int i = 0; i < count; i++)
            {
                if (prefabs[i] == null)
                {
                    continue;
                }

                // check for inventory count
                if (!manager.CanSpawn(isLeft, prefabs[i].prefabType))
                {
                    VRPrefabManager.instance.Return(prefabs[i]);
                    continue;
                }

                var equippable = prefabs[i] as IVRSpawnableEquippable;
                if (equippable != null)
                {
                    // if the spawnable can be equipped, spawn and equip it
                    SpawnAndEquip(equippable, isLeft);
                }
                else
                {
                    // if the spawnable cannot be equipped, just spawn it
                    Spawn(prefabs[i], isLeft);
                }

                manager.AdjustSpawnCount(isLeft, prefabs[i].prefabType, -1);
            }

            ListBufferPool.ReturnBuffer(prefabs);
        }

        public virtual bool Spawn(IVRSpawnable spawnable, bool isLeft)
        {
            return spawnable != null;
        }

        public virtual bool SpawnAndEquip(IVRSpawnableEquippable equippable, bool isLeft)
        {
            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;
            var result = hand.Equip(equippable);
            return result != null;
        }

        public virtual bool Equip(IVREquippable equippable, bool isLeft)
        {
            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;
            var result = hand.Equip(equippable);
            return result != null;
        }

        public virtual bool Unequip(IVREquippable equippable, bool isLeft)
        {
            if (equippable == null)
            {
                return false;
            }

            var controller = isLeft ? this.leftController : this.rightController;
            var hand = controller.hand;
            var equipped = hand.equipped;
            var count = equipped.Count;
            if (count == 0)
            {
                Debug.LogWarning(this.ToString() + " Cannot Unequip() when the hand has nothing equipped");
                return false;
            }

            var haveVelocities = equippable as IHaveThrowMultipliers;
            var velocityMultiplier = (haveVelocities != null ? haveVelocities.velocityMultiplier : _config.velocityMultiplier);
            var angularVelocityMultiplier = (haveVelocities != null ? haveVelocities.angularVelocityMultiplier : _config.angularVelocityMultiplier);

            var result = hand.Unequip(equippable, _player.velocity + (controller.velocity * velocityMultiplier), controller.angularVelocity * angularVelocityMultiplier);

            //Debug.Log(this.ToString() + " " + (isLeft ? "left" : "right") + " hand unequipped " + equippable.ToString());
            return result != null;
        }

        public void UnequipAll()
        {
            // actually iterate through hands' equipped objects and unequip them
            var leftHand = _player.leftHand;
            var equipped = leftHand.equipped;
            var count = equipped.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                Unequip(equipped[i], true);
            }

            var rightHand = _player.rightHand;
            equipped = rightHand.equipped;
            count = equipped.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                Unequip(equipped[i], false);
            }
        }

#if UNITY_EDITOR

        [DebugButton]
        private void PrintNearestHighlighted()
        {
            if (_player == null)
            {
                return;
            }

            var nearestLeft = _player.leftHand.GetNearestHighlighted();
            Debug.Log(this.ToString() + " LEFT HAND current nearest highlighted == " + (nearestLeft != null ? nearestLeft.ToString() : "null"));

            var nearestRight = _player.rightHand.GetNearestHighlighted();
            Debug.Log(this.ToString() + " RIGHT HAND current nearest highlighted == " + (nearestRight != null ? nearestRight.ToString() : "null"));
        }

        [DebugButton]
        private void PrintCurrentlyEquipped()
        {
            if (_player == null)
            {
                return;
            }

            var leftHand = _player.leftHand;

            var sb = new System.Text.StringBuilder();
            sb.AppendLine(this.ToString() + " Left hand has " + leftHand.equipped.Count.ToString() + " currenly equipped items:");
            foreach (var equipped in leftHand.equipped)
            {
                sb.AppendLine(equipped.ToString() + "(" + equipped.prefabType.ToString() + ")");
            }

            Debug.Log(sb.ToString());

            var rightHand = _player.rightHand;

            sb = new System.Text.StringBuilder();
            sb.AppendLine(this.ToString() + " Right hand has " + rightHand.equipped.Count.ToString() + " currenly equipped items:");
            foreach (var equipped in rightHand.equipped)
            {
                sb.AppendLine(equipped.ToString() + " (" + equipped.prefabType.ToString() + ")");
            }

            Debug.Log(sb.ToString(), this);
        }

        [DebugButton]
        private void PrintCurrentlyHighlighted()
        {
            if (_player == null)
            {
                return;
            }

            var leftHand = _player.leftHand;

            var sb = new System.Text.StringBuilder();
            sb.AppendLine(this.ToString() + " Left hand has " + leftHand.highlighted.Count.ToString() + " currenly highlighted items:");
            foreach (var equipped in leftHand.highlighted)
            {
                sb.AppendLine(equipped.ToString() + " (" + equipped.prefabType.ToString() + ")");
            }

            Debug.Log(sb.ToString());

            var rightHand = _player.rightHand;

            sb = new System.Text.StringBuilder();
            sb.AppendLine(this.ToString() + " Right hand has " + rightHand.highlighted.Count.ToString() + " currenly highlighted items:");
            foreach (var equipped in rightHand.highlighted)
            {
                sb.AppendLine(equipped.ToString() + " (" + equipped.prefabType.ToString() + ")");
            }

            Debug.Log(sb.ToString(), this);
        }

        [DebugButton]
        private void UnequipAllDebug()
        {
            if (_player == null)
            {
                return;
            }

            Debug.Log(this.ToString() + " unequipping " + (_player.leftHand.equipped.Count + _player.rightHand.equipped.Count).ToString() + " items.", this);
            UnequipAll();
        }

        [DebugButton]
        private void TriggerClickOnHand(bool left)
        {
            OnPressDown(left, VRInput.Trigger);
            OnPressUp(left, VRInput.Trigger);
        }

#endif
    }
}