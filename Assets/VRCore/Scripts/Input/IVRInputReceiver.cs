namespace VRCore
{
    /// <summary>
    /// The Input Receiver is basically a wrapper for the two <see cref="IVRController"/>s, representing the VR capabilities for each hand.
    /// The Input Receiver listens for VR input sends the input on to the given <see cref="IVRInputController"/>, if one such is given.
    /// </summary>
    public interface IVRInputReceiver
    {
        /// <summary>
        /// Gets the left controller.
        /// </summary>
        /// <value>
        /// The left controller.
        /// </value>
        IVRController leftController { get; }

        /// <summary>
        /// Gets the right controller.
        /// </summary>
        /// <value>
        /// The right controller.
        /// </value>
        IVRController rightController { get; }

        /// <summary>
        /// Gets or sets the input controller associated with this input receiver.
        /// </summary>
        /// <value>
        /// The input controller.
        /// </value>
        IVRInputController inputController { get; }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IVRInputReceiver"/> is initialized.
        /// </summary>
        /// <value>
        ///   <c>true</c> if initialized; otherwise, <c>false</c>.
        /// </value>
        bool initialized { get; }

        /// <summary>
        /// Re-initializes the controllers using each other's data, effectively swapping the hands.
        /// </summary>
        void SwapHands();

        /// <summary>
        /// Updates the input receiver. Must be called every frame for maximum accuracy.
        /// If an <see cref="IVRInputController"/> is set, its methods will be called in this update method as well.
        /// </summary>
        void Update();
    }
}