namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface IVRController
    {
        VRInputConfig inputConfig { get; }

        /// <summary>
        /// Gets the <see cref="IVRHand"/> associated with this controller.
        /// </summary>
        /// <value>
        /// The hand.
        /// </value>
        IVRHand hand { get; }

        /// <summary>
        /// Gets the currently equipped objects in the hand.
        /// </summary>
        /// <value>
        /// The equipped.
        /// </value>
        IList<IVREquippable> equipped { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is the left hand.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is the left hand; otherwise, <c>false</c>.
        /// </value>
        bool isLeft { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is the right hand.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this instance is the right hand; otherwise, <c>false</c>.
        /// </value>
        bool isRight { get; }

        /// <summary>
        /// Gets the position of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        Vector3 position { get; }

        /// <summary>
        /// Gets the rotation of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        Quaternion rotation { get; }

        /// <summary>
        /// Gets the velocity of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The velocity.
        /// </value>
        Vector3 velocity { get; }

        /// <summary>
        /// Gets the angular velocity of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The angular velocity.
        /// </value>
        Vector3 angularVelocity { get; }

        /// <summary>
        /// Gets a value indicating whether this controller is currently showing the render model.
        /// </summary>
        /// <value>
        ///   <c>true</c> if showing the render model; otherwise, <c>false</c>.
        /// </value>
        bool showingModel { get; }

        /// <summary>
        /// Returns true if touch started for <see cref="VRInput.Touchpad"/> or <see cref="VRInput.Trigger"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        bool GetTouchDown(VRInput input);

        /// <summary>
        /// Returns true if a touch ended for <see cref="VRInput.Touchpad"/> or <see cref="VRInput.Trigger"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        bool GetTouchUp(VRInput input);

        /// <summary>
        /// Returns true while a touch is registered for <see cref="VRInput.Touchpad"/> or <see cref="VRInput.Trigger"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        bool GetTouch(VRInput input);

        /// <summary>
        /// Returns true if a press started on the given <see cref="VRInput"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        bool GetPressDown(VRInput input);

        /// <summary>
        /// Returns true if a press ended on the given <see cref="VRInput"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        bool GetPressUp(VRInput input);

        /// <summary>
        /// Returns true while a press is registered on the given <see cref="VRInput"/>.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        bool GetPress(VRInput input);

        /// <summary>
        /// Gets the axis state for Axis 0.
        /// </summary>
        /// <returns></returns>
        Vector2 GetAxis();

        /// <summary>
        /// Gets the axis state for the given <see cref="VRInput"/> axis.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        Vector2 GetAxis(VRInput input);

        /// <summary>
        /// Initializes this <see cref="IVRController"/>.
        /// </summary>
        void Initialize(VRInputConfig inputConfig);

        /// <summary>
        /// Updates this <see cref="IVRController"/>.
        /// </summary>
        void Update();

        /// <summary>
        /// Vibrates this <see cref="IVRController"/> with 0.5 used as intensity.
        /// </summary>
        void Vibrate();

        /// <summary>
        /// Vibrates this <see cref="IVRController"/> at the specified intensity (0-1) on the default <see cref="VRInput.Axis0"/>.
        /// </summary>
        /// <param name="intensity">The intensity.</param>
        void Vibrate(float intensity);

        /// <summary>
        /// Vibrates this <see cref="IVRController"/> at the specified intensity (0-1) on the given <see cref="VRInput"/> axis.
        /// </summary>
        /// <param name="intensity">The intensity. Is clamped to the range between 0 and 1 automatically.</param>
        /// <param name="input">The input.</param>
        void Vibrate(float intensity, VRInput input);

        /// <summary>
        /// Hides the controller model.
        /// </summary>
        void HideControllerModel();

        /// <summary>
        /// Shows the controller model.
        /// </summary>
        void ShowControllerModel();

        /// <summary>
        /// Toggles the controller model's visibility status.
        /// </summary>
        /// <param name="active">if set to <c>true</c> activates the render model, <c>false</c> deactivates.</param>
        void ToggleControllerModel(bool active);
    }
}