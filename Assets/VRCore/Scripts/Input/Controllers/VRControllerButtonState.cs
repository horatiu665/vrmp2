namespace VRCore
{
    public enum VRControllerButtonState
    {
        None = 0,
        Down,
        Up,
    }
}