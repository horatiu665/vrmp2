namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class VRControllerBase : IVRController
    {
        protected IVRHand _hand;
        protected VRInputConfig _inputConfig;

        public VRInputConfig inputConfig
        {
            get { return _inputConfig; }
        }

        /// <summary>
        /// Gets the <see cref="IVRHand"/> associated with this controller.
        /// </summary>
        /// <value>
        /// The hand.
        /// </value>
        public IVRHand hand
        {
            get { return _hand; }
        }

        /// <summary>
        /// Gets the currently equipped object in the hand.
        /// </summary>
        /// <value>
        /// The equipped.
        /// </value>
        public IList<IVREquippable> equipped
        {
            get { return _hand.equipped; }
        }

        /// <summary>
        /// Gets a value indicating whether this instance is the left hand.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is the left hand; otherwise, <c>false</c>.
        /// </value>
        public abstract bool isLeft { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is the right hand.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is the right hand; otherwise, <c>false</c>.
        /// </value>
        public abstract bool isRight { get; }

        /// <summary>
        /// Gets the position of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public abstract Vector3 position { get; }

        /// <summary>
        /// Gets the rotation of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        public abstract Quaternion rotation { get; }

        /// <summary>
        /// Gets the velocity of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The velocity.
        /// </value>
        public abstract Vector3 velocity { get; }

        /// <summary>
        /// Gets the angular velocity of this <see cref="IVRController"/>.
        /// </summary>
        /// <value>
        /// The angular velocity.
        /// </value>
        public abstract Vector3 angularVelocity { get; }

        /// <summary>
        /// Gets a value indicating whether this controller is currently showing the render model.
        /// </summary>
        /// <value>
        /// <c>true</c> if showing the render model; otherwise, <c>false</c>.
        /// </value>
        public abstract bool showingModel { get; }

        public virtual void Initialize(VRInputConfig config)
        {
            _inputConfig = config;

            // Setup hand so that it can listen to trigger events and facilitate highlighting equippables
            var tr = _hand.transform;
            var handCollider = tr.GetOrAddComponent<SphereCollider>();
            handCollider.radius = VRPrefabManager.instance.maxEquipDistance;
            handCollider.isTrigger = true;

            var handComponent = tr.GetOrAddComponent<VRHandComponent>();
            handComponent.SetHand(_hand);
        }

        /// <summary>
        /// Updates this <see cref="IVRController"/>.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Returns true if touch started on the given <see cref="VRInput" />.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract bool GetTouchDown(VRInput input);

        /// <summary>
        /// Returns true if a touch ended on the given <see cref="VRInput" />.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract bool GetTouchUp(VRInput input);

        /// <summary>
        /// Returns true while a touch is registered on the given <see cref="VRInput" />..
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract bool GetTouch(VRInput input);

        /// <summary>
        /// Returns true if a press started on the given <see cref="VRInput" />.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract bool GetPressDown(VRInput input);

        /// <summary>
        /// Returns true if a press ended on the given <see cref="VRInput" />.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract bool GetPressUp(VRInput input);

        /// <summary>
        /// Returns true while a press is registered on the given <see cref="VRInput" />.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract bool GetPress(VRInput input);

        /// <summary>
        /// Gets the axis state for Axis 0.
        /// </summary>
        /// <returns></returns>
        public Vector2 GetAxis()
        {
            return GetAxis(VRInput.Axis0);
        }

        /// <summary>
        /// Gets the axis state for the given <see cref="VRInput" /> axis.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public abstract Vector2 GetAxis(VRInput input);

        /// <summary>
        /// Vibrates this <see cref="IVRController"/> with 0.5 used as intensity.
        /// </summary>
        public void Vibrate()
        {
            Vibrate(0.5f);
        }

        /// <summary>
        /// Vibrates this <see cref="IVRController"/> at the specified intensity (0-1) on the default <see cref="VRInput.Axis0"/>.
        /// </summary>
        /// <param name="intensity">The intensity.</param>
        public void Vibrate(float intensity)
        {
            Vibrate(intensity, VRInput.Axis0);
        }

        public abstract void Vibrate(float intensity, VRInput input);

        /// <summary>
        /// Hides the controller model.
        /// </summary>
        public void HideControllerModel()
        {
            ToggleControllerModel(false);
        }

        /// <summary>
        /// Shows the controller model.
        /// </summary>
        public void ShowControllerModel()
        {
            ToggleControllerModel(true);
        }

        /// <summary>
        /// Toggles the controller model's visibility status.
        /// </summary>
        /// <param name="active">if set to <c>true</c> activates the render model, <c>false</c> deactivates.</param>
        public abstract void ToggleControllerModel(bool active);
    }
}