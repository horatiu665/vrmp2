namespace VRCore
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using UnityEngine;
	using Valve.VR;

	/// <summary>
	/// The VR controller is a wrapper for the <see cref="SteamVR_TrackedObject"/>, making it easier to listen for and react to <see cref="SteamVR"/> input.
	/// Each VR controller class represents a single controller.
	/// Also includes functionality for toggling the visibility of <see cref="SteamVR_RenderModel"/>, as well as equipping, dropping and throwing objects.
	/// </summary>
	public sealed class SteamVRController : VRControllerBase
	{
		/// The magic number, 3999, is found through testing. The device simply stops vibrating when it reaches the value 4000, so the de-facto maximum is 3999.
		private const int _vibrateMaxValue = 3999;

		private readonly VRInput[] _inputValues = ((VRInput[])Enum.GetValues(typeof(VRInput))).GroupBy(e => (int)e).Select(e => e.First()).ToArray();

		private IDictionary<VRInput, bool> _touchedInput;
		private IDictionary<VRInput, bool> _pressedInput;
		private IDictionary<VRInput, VRControllerButtonState> _pressedButtonStates;
		private IDictionary<VRInput, VRControllerButtonState> _touchedButtonStates;
		private VRControllerState_t _controllerState;
		private SteamVR_TrackedObject _trackedObj;
		private SteamVR_RenderModel _model;
		private bool _showingModel;
		private bool _isLeft;
		private bool _isRight;

		/// <summary>
		/// Initializes a new instance of the <see cref="SteamVRController"/> class.
		/// </summary>
		public SteamVRController()
		{
			// setup pressed/touched dicts
			var comparer = new VRInputTypeComparer();
			var length = _inputValues.Length;

			_touchedInput = new Dictionary<VRInput, bool>(length, comparer);
			_pressedInput = new Dictionary<VRInput, bool>(length, comparer);
			_pressedButtonStates = new Dictionary<VRInput, VRControllerButtonState>(length, comparer);
			_touchedButtonStates = new Dictionary<VRInput, VRControllerButtonState>(length, comparer);

			for (int i = 0; i < length; i++)
			{
				_touchedInput.Add(_inputValues[i], false);
				_pressedInput.Add(_inputValues[i], false);
				_pressedButtonStates.Add(_inputValues[i], VRControllerButtonState.None);
				_touchedButtonStates.Add(_inputValues[i], VRControllerButtonState.None);
			}
		}

		#region PROPERTIES

		/// <summary>
		/// Gets the <see cref="SteamVR_Controller.Device"/> associated with the <see cref="SteamVR_TrackedObject"/>.
		/// Please note that this performs an evaluation. Cache locally when possible.
		/// </summary>
		/// <value>
		/// The <see cref="SteamVR_Controller.Device"/>.
		/// </value>
		private SteamVR_Controller.Device device
		{
			get
			{
				if (!_trackedObj.isValid)
				{
					return null;
				}

				return SteamVR_Controller.Input((int)_trackedObj.index);
			}
		}

		/// <summary>
		/// Gets the <see cref="SteamVR_TrackedObject"/>.
		/// </summary>
		/// <value>
		/// The tracked object.
		/// </value>
		internal SteamVR_TrackedObject trackedObject
		{
			get { return _trackedObj; }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is the left hand.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is the left hand; otherwise, <c>false</c>.
		/// </value>
		public override bool isLeft
		{
			get { return _isLeft; }
		}

		/// <summary>
		/// Gets a value indicating whether this instance is the right hand.
		/// </summary>
		/// <value>
		///   <c>true</c> if this instance is the right hand; otherwise, <c>false</c>.
		/// </value>
		public override bool isRight
		{
			get { return _isRight; }
		}

		/// <summary>
		/// Gets the position of the <see cref="SteamVR_Controller.Device"/>, if available. Returns the <see cref="SteamVR_TrackedObject"/>'s position as a fallback.
		/// </summary>
		/// <value>
		/// The position.
		/// </value>
		public override Vector3 position
		{
			get
			{
				var device = this.device;
				if (device == null)
				{
					return _trackedObj.transform.localPosition;
				}

				return device.transform.pos;
			}
		}

		/// <summary>
		/// Gets the rotation of the <see cref="SteamVR_Controller.Device"/>, if available. Returns the <see cref="SteamVR_TrackedObject"/>'s rotation as a fallback.
		/// </summary>
		/// <value>
		/// The rotation.
		/// </value>
		public override Quaternion rotation
		{
			get
			{
				var device = this.device;
				if (device == null)
				{
					return _trackedObj.transform.localRotation;
				}

				return device.transform.rot;
			}
		}

		/// <summary>
		/// Gets the velocity for this controller.
		/// </summary>
		/// <value>
		/// The velocity.
		/// </value>
		public override Vector3 velocity
		{
			get
			{
				var device = this.device;
				if (device == null)
				{
					return Vector3.zero;
				}

				return device.velocity;
			}
		}

		/// <summary>
		/// Gets the angular velocity for this controller.
		/// </summary>
		/// <value>
		/// The angular velocity.
		/// </value>
		public override Vector3 angularVelocity
		{
			get
			{
				var device = this.device;
				if (device == null)
				{
					return Vector3.zero;
				}

				return device.angularVelocity;
			}
		}

		/// <summary>
		/// Gets a value indicating whether this controller is currently showing the <see cref="SteamVR_RenderModel"/>.
		/// </summary>
		/// <value>
		///   <c>true</c> if showing the render model; otherwise, <c>false</c>.
		/// </value>
		public override bool showingModel
		{
			get { return _showingModel; }
		}

		#endregion PROPERTIES

		#region METHODS

		/// <summary>
		/// Initializes this VR input controller. If the <see cref="SteamVR_RenderModel"/> argument is passed as non-null, the VR controller will show and hide the rendered model.
		/// </summary>
		/// <param name="hand">The hand.</param>
		/// <param name="trackedObj">The <see cref="SteamVR_TrackedObject"/> object.</param>
		/// <param name="velocityMultiplier">The velocity multiplier used to multiply the velocity of thrown objects.</param>
		/// <param name="angularVelocityMultiplier">The angular velocity multiplier used to multiply the angular velocity of thrown objects.</param>
		/// <param name="defaultLifespan">The default lifespan of thrown objects.</param>
		public void Initialize(IVRHand hand, SteamVR_TrackedObject trackedObj, VRInputConfig inputConfig)
		{
			if (hand == null)
			{
				throw new ArgumentNullException("hand", "The supplied Hand is null");
			}

			if (trackedObj == null)
			{
				throw new ArgumentNullException("trackedObj", "The supplied SteamVR_TrackedObject is null");
			}

			// set references & values
			_hand = hand;
			_hand.SetVRController(this);

			_trackedObj = trackedObj;

			// figure out whether this controller is the left or right one
			_isLeft = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Leftmost) == (int)_trackedObj.index;
			_isRight = SteamVR_Controller.GetDeviceIndex(SteamVR_Controller.DeviceRelation.Rightmost) == (int)_trackedObj.index;

			// The model can be null and this is valid. Warnings are not thrown until ToggleControllerModel is called. Automatically search for an enabled render model.
			_model = _trackedObj.GetComponent<SteamVR_RenderModel>();
			if (_model == null)
			{
				_model = _trackedObj.GetComponentInChildren<SteamVR_RenderModel>();
			}

			_showingModel = _model != null && _model.enabled && _model.gameObject.activeSelf;

			base.Initialize(inputConfig);
		}

		/// <summary>
		/// Updates this controller's state. Must be called every frame for maximum accuracy.
		/// </summary>
		public override void Update()
		{
			var system = OpenVR.System;
			if (system != null)
			{
				if (_trackedObj != null)
				{
					system.GetControllerState((uint)_trackedObj.index, ref _controllerState, (uint)System.Runtime.InteropServices.Marshal.SizeOf(typeof(VRControllerState_t)));
					UpdateButtonStates();
				}
				else
				{
					Debug.LogWarning(this.ToString() + " cannot update button states, since the SteamVR tracked object is null");
				}
			}
		}

		private ulong GetTouched(VRInput input)
		{
			return _controllerState.ulButtonTouched & VRHelper.GetSteamVRButtonMask(input);
		}

		private ulong GetPressed(VRInput input)
		{
			return _controllerState.ulButtonPressed & VRHelper.GetSteamVRButtonMask(input);
		}

		private void UpdateButtonStates()
		{
			for (int i = 0; i < _inputValues.Length; i++)
			{
				var input = _inputValues[i];
				UpdateTouchState(input);
				UpdatePressState(input);
			}
		}

		private void UpdateTouchState(VRInput input)
		{
			var touch = GetTouched(input);
			if (touch > 0L && !_touchedInput[input])
			{
				_touchedInput[input] = true;
				_touchedButtonStates[input] = VRControllerButtonState.Down;
			}
			else if (touch == 0L && _touchedInput[input])
			{
				_touchedInput[input] = false;
				_touchedButtonStates[input] = VRControllerButtonState.Up;
			}
			else
			{
				_touchedButtonStates[input] = VRControllerButtonState.None;
			}
		}

		private void UpdatePressState(VRInput input)
		{
			var press = GetPressed(input);
			if (press > 0L && !_pressedInput[input])
			{
				_pressedInput[input] = true;
				_pressedButtonStates[input] = VRControllerButtonState.Down;
			}
			else if (press == 0L && _pressedInput[input])
			{
				_pressedInput[input] = false;
				_pressedButtonStates[input] = VRControllerButtonState.Up;
			}
			else
			{
				_pressedButtonStates[input] = VRControllerButtonState.None;
			}
		}

		#endregion METHODS

		#region TOUCH

		/// <summary>
		/// Returns true if touch started on the given <see cref="VRInput" />.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override bool GetTouchDown(VRInput input)
		{
			return _touchedButtonStates[input] == VRControllerButtonState.Down;
		}

		/// <summary>
		/// Returns true if a touch ended on the given <see cref="VRInput" />.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override bool GetTouchUp(VRInput input)
		{
			return _touchedButtonStates[input] == VRControllerButtonState.Up;
		}

		/// <summary>
		/// Returns true while a touch is registered on the given <see cref="VRInput" />.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override bool GetTouch(VRInput input)
		{
			return GetTouched(input) > 0L;
		}

		#endregion TOUCH

		#region PRESS

		/// <summary>
		/// Returns true if a press started on the given <see cref="VRInput"/>.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override bool GetPressDown(VRInput input)
		{
			return _pressedButtonStates[input] == VRControllerButtonState.Down;
		}

		/// <summary>
		/// Returns true if a press ended on the given <see cref="VRInput"/>.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override bool GetPressUp(VRInput input)
		{
			return _pressedButtonStates[input] == VRControllerButtonState.Up;
		}

		/// <summary>
		/// Returns true while a press is registered on the given <see cref="VRInput"/>.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override bool GetPress(VRInput input)
		{
			return GetPressed(input) > 0L;
		}

		#endregion PRESS

		#region AXIS

		/// <summary>
		/// Gets the axis state for the given <see cref="VRInput"/> axis.
		/// </summary>
		/// <param name="input">The input.</param>
		/// <returns></returns>
		public override Vector2 GetAxis(VRInput input)
		{
			switch (input)
			{
			case VRInput.Axis0:
				{
					return new Vector2(_controllerState.rAxis0.x, _controllerState.rAxis0.y);
				}

			case VRInput.Axis1:
				{
					return new Vector2(_controllerState.rAxis1.x, _controllerState.rAxis1.y);
				}

			case VRInput.Axis2:
				{
					return new Vector2(_controllerState.rAxis2.x, _controllerState.rAxis2.y);
				}

			case VRInput.Axis3:
				{
					return new Vector2(_controllerState.rAxis3.x, _controllerState.rAxis3.y);
				}

			case VRInput.Axis4:
				{
					return new Vector2(_controllerState.rAxis4.x, _controllerState.rAxis4.y);
				}
			}

			// if the request was not for one of the axes available in the controller state, try quering the device
			var device = this.device;
			if (device == null)
			{
				return Vector2.zero;
			}

			return device.GetAxis(VRHelper.GetSteamVRButtonID(input));
		}

		#endregion AXIS

		#region VIBRATE

		/// <summary>
		/// Vibrates this <see cref="IVRController"/> at the specified intensity (0-1) on the given <see cref="VRInput"/> axis.
		/// </summary>
		/// <param name="intensity">The intensity. Is clamped to the range between 0 and 1 automatically.</param>
		/// <param name="input">The input.</param>
		public override void Vibrate(float intensity, VRInput input)
		{
			var device = this.device;
			if (device == null)
			{
				return;
			}

			device.TriggerHapticPulse((ushort)Mathf.RoundToInt(Mathf.Clamp01(intensity) * _vibrateMaxValue), VRHelper.GetSteamVRButtonID(input));
		}

		#endregion VIBRATE

		#region RENDER_MODEL

		/// <summary>
		/// Toggles the controller model's visibility status, <seealso cref="SteamVR_RenderModel"/>.
		/// </summary>
		/// <param name="active">if set to <c>true</c> activates the SteamVR render model, <c>false</c> deactivates.</param>
		public override void ToggleControllerModel(bool active)
		{
			if (_model == null)
			{
				Debug.LogWarning(this.ToString() + " cannot ToggleControllerModel for unknown SteamVR_RenderModel");
				return;
			}

			if (_showingModel != active)
			{
				_showingModel = active;
				_model.gameObject.SetActive(active);
			}
		}

		#endregion RENDER_MODEL
	}
}