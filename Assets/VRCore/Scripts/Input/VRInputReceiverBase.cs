namespace VRCore
{
    using System;
	using System.Linq;

	/// <summary>
	/// The Input Receiver is basically a wrapper for the two <see cref="IVRController"/>s, representing the VR capabilities for each hand.
	/// The Input Receiver listens for VR input sends the input on to the given <see cref="IVRInputController"/>, if one such is given.
	/// </summary>
	public abstract class VRInputReceiverBase<T> : IVRInputReceiver where T : IVRController, new()
    {
        protected readonly VRInput[] _inputs = ((VRInput[])Enum.GetValues(typeof(VRInput))).GroupBy(e => (int)e).Select(e => e.First()).ToArray();

		protected readonly T _leftController = new T();
        protected readonly T _rightController = new T();

        protected IVRInputController _inputController;

        /// <summary>
        /// Initializes a new instance of the <see cref="VRInputReceiverBase{T}"/> class.
        /// </summary>
        public VRInputReceiverBase()
        {
        }

        /// <summary>
        /// Gets the left controller.
        /// </summary>
        /// <value>
        /// The left controller.
        /// </value>
        public IVRController leftController
        {
            get { return _leftController; }
        }

        /// <summary>
        /// Gets the right controller.
        /// </summary>
        /// <value>
        /// The right controller.
        /// </value>
        public IVRController rightController
        {
            get { return _rightController; }
        }

        /// <summary>
        /// Gets or sets the input controller associated with this input receiver.
        /// </summary>
        /// <value>
        /// The input controller.
        /// </value>
        public IVRInputController inputController
        {
            get { return _inputController; }
            set { _inputController = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether this <see cref="IVRInputReceiver"/> is initialized.
        /// </summary>
        /// <value>
        ///   <c>true</c> if initialized; otherwise, <c>false</c>.
        /// </value>
        public bool initialized
        {
            get;
            protected set;
        }

        /// <summary>
        /// Re-initializes the controllers using each other's data, effectively swapping the hands.
        /// </summary>
        public abstract void SwapHands();

        /// <summary>
        /// Updates the input receiver. Must be called every frame for maximum accuracy.
        /// If an <see cref="IVRInputController"/> is set, its methods will be called in this update method as well.
        /// </summary>
        public virtual void Update()
        {
            _leftController.Update();
            _rightController.Update();

            if (_inputController != null)
            {
                // To allow for maximum flexibility, only run the input controller events if the input controller is not null.
                // This allows for alternate implementations not using the input controller
                for (int i = 0; i < _inputs.Length; i++)
                {
                    HandleEvents(_leftController, true, _inputs[i]);
                    HandleEvents(_rightController, false, _inputs[i]);
                }
            }
        }

        /// <summary>
        /// Handles the invocation of events for the given controller.
        /// </summary>
        /// <param name="controller">The controller.</param>
        /// <param name="isLeft">if set to <c>true</c> is left hand, otherwise right hand.</param>
        /// <param name="input">The VR input.</param>
        protected virtual void HandleEvents(IVRController controller, bool isLeft, VRInput input)
        {
            if (controller.GetTouchUp(input))
            {
                _inputController.OnTouchUp(isLeft, input);
            }

            if (controller.GetTouchDown(input))
            {
                _inputController.OnTouchDown(isLeft, input);
            }

            if (controller.GetTouch(input))
            {
                _inputController.OnTouch(isLeft, input);
            }

            if (controller.GetPressUp(input))
            {
                _inputController.OnPressUp(isLeft, input);
            }

            if (controller.GetPressDown(input))
            {
                _inputController.OnPressDown(isLeft, input);
            }

            if (controller.GetPress(input))
            {
                _inputController.OnPress(isLeft, input);
            }
        }
    }
}