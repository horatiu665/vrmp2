﻿namespace VRCore
{
    using System.Collections.Generic;

    /// <summary>
    /// A type comparer for comparing VRInput enum types without incurring memory allocations.
    /// </summary>
    /// <seealso cref="System.Collections.Generic.IEqualityComparer{VRCore.VRInput}" />
    public sealed class VRInputTypeComparer : IEqualityComparer<VRInput>
    {
        public bool Equals(VRInput x, VRInput y)
        {
            return x == y;
        }

        public int GetHashCode(VRInput obj)
        {
            return (int)obj;
        }
    }
}