﻿namespace VRCore
{
    /// <summary>
    /// Base class representing all VR Input messages.
    /// </summary>
    public abstract class VRInputMessageBase
    {
        public IVRPlayer player;
        public IVRHand hand;
        public VRInput input;
        public bool isLeft;
        public bool isRight;

        public override string ToString()
        {
            return string.Concat("VRInputMessage (", this.GetType().ToString(), "). Player == ", this.player.ToString(), ", hand == ", this.hand.ToString(), ", input == ", this.input.ToString(), ", isLeft == ", this.isLeft.ToString(), "\n");
        }
    }
}