namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class VREquippableBase : VRPrefabBase, IVREquippable
    {
        protected readonly IList<IVRCustomBehaviour> _behaviours = new List<IVRCustomBehaviour>();

        [Header("Equippable")]
        [SerializeField, Tooltip("Whether this equippable mixes its behaviour setup with that on the player.")]
        protected bool _mixWithPlayerBehaviours = true;

        [SerializeField, Range(0.001f, 2f), Tooltip("The maximum distance at which this equippable can be equipped.")]
        protected float _equipDistance = 0.15f;

        [SerializeField, ReadOnly]
        protected bool _isEquipped;

        protected IList<Collider> _colliders;
        protected Rigidbody _rb;
        protected IVRPlayer _player;
        protected IVRHand _hand;

        public event System.Action<IVRHand> OnEquipEvent, OnUnequipEvent;

        /// <summary>
        /// Gets the player wielding this equippable.
        /// </summary>
        /// <value>
        /// The player.
        /// </value>
        public IVRPlayer player
        {
            get { return _player; }
        }

        /// <summary>
        /// The Collider attached to this GameObject (null if there is none attached).
        /// </summary>
        public IList<Collider> colliders
        {
            get { return _colliders; }
        }

        /// <summary>
        /// The Rigidbody attached to this GameObject (is required, so cannot be null).
        /// </summary>
        /// <value>
        /// The rigidbody.
        /// </value>
        public new Rigidbody rigidbody
        {
            get { return _rb; }
        }

        /// <summary>
        /// Gets the hand wielding this equippable.
        /// </summary>
        /// <value>
        /// The hand.
        /// </value>
        public IVRHand hand
        {
            get { return _hand; }
        }

        public IList<IVRCustomBehaviour> behaviours
        {
            get { return _behaviours; }
        }

        public Renderer[] renderers
        {
            get;
            private set;
        }

        public float equipDistance
        {
            get { return _equipDistance; }
        }

        public bool isEquipped
        {
            get { return _isEquipped; }
        }

        public bool mixWithPlayerBehaviours
        {
            get { return _mixWithPlayerBehaviours; }
        }

        /// <summary>
        /// Called by Unity when awoken - before OnEnable.
        /// </summary>
        protected virtual void Awake()
        {
            _rb = this.GetComponent<Rigidbody>();
            if (_rb == null)
            {
                _rb = this.GetComponentInChildren<Rigidbody>();
            }

            var allcols = this.GetComponentsInChildren<Collider>();
            _colliders = new List<Collider>(allcols.Length);
            for (int i = 0; i < allcols.Length; i++)
            {
                if (allcols[i].attachedRigidbody == _rb)
                {
                    _colliders.Add(allcols[i]);
                }
            }

            this.renderers = this.GetAllRenderers();
        }

        /// <summary>
        /// Called when equipped.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="hand">The hand.</param>
        public virtual void OnEquipped(IVRHand hand)
        {
            _player = hand.player;
            _hand = hand;
            _isEquipped = true;

            if (OnEquipEvent != null)
            {
                OnEquipEvent(hand);
            }
        }

        /// <summary>
        /// Called when unequipped.
        /// </summary>
        /// <param name="hand">The hand.</param>
        public virtual void OnUnequipped(IVRHand hand)
        {
            _isEquipped = false;
            // should we null hand and player? no because that leads to issues with evaluating whether e.g. the thing was previously equipped in the same hand (for perma equip)

            if (OnUnequipEvent != null)
            {
                OnUnequipEvent(hand);
            }
        }
    }
}