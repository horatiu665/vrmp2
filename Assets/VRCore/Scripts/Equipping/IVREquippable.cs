namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface IVREquippable : IVRPrefab, IHaveVRCustomBehaviours
    {
        /// <summary>
        /// Gets the renderers.
        /// </summary>
        /// <value>
        /// The renderers.
        /// </value>
        Renderer[] renderers { get; }

        /// <summary>
        /// Gets the rigidbody.
        /// </summary>
        /// <value>
        /// The rigidbody.
        /// </value>
        Rigidbody rigidbody { get; }

        /// <summary>
        /// List of colliders attached to this object's rigidbody. Can represent a single collider, or even none.
        /// </summary>
        IList<Collider> colliders { get; }

        /// <summary>
        /// Gets the player wielding this equippable.
        /// </summary>
        /// <value>
        /// The player.
        /// </value>
        IVRPlayer player { get; }

        /// <summary>
        /// Gets the hand currently wielding this equippable.
        /// </summary>
        /// <value>
        /// The hand.
        /// </value>
        IVRHand hand { get; }

        /// <summary>
        /// Gets the equip distance - the distance at which this equippable can be equipped.
        /// </summary>
        /// <value>
        /// The equip distance.
        /// </value>
        float equipDistance { get; }

        /// <summary>
        /// Gets a value indicating whether this instance is equipped.
        /// </summary>
        /// <value>
        /// <c>true</c> if this instance is equipped; otherwise, <c>false</c>.
        /// </value>
        bool isEquipped { get; }

        /// <summary>
        /// Gets a value indicating whether this equippable mixes its behaviour setup with that on the player.
        /// </summary>
        /// <value>
        /// <c>true</c> if mixing with player behaviours; otherwise, <c>false</c>.
        /// </value>
        bool mixWithPlayerBehaviours { get; }

        /// <summary>
        /// Called when equipped.
        /// </summary>
        /// <param name="hand">The hand.</param>
        void OnEquipped(IVRHand hand);

        /// <summary>
        /// Called when unequipped.
        /// </summary>
        /// <param name="hand">The hand.</param>
        void OnUnequipped(IVRHand hand);

        event System.Action<IVRHand> OnEquipEvent, OnUnequipEvent;

    }
}