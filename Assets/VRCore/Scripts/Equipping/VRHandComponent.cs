namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    [RequireComponent(typeof(Collider))]
    public sealed class VRHandComponent : MonoBehaviour
    {
        private IVRHand _hand;

        public enum HighlightMethods
        {
            Triggers,
            OverlapSphere,
        }
        [SerializeField]
        private HighlightMethods _method = HighlightMethods.OverlapSphere;

        public HighlightMethods method
        {
            get
            {
                return _method;
            }

            set
            {
                _method = value;
            }
        }

        [Header("Overlap Sphere method parameters")]
        [SerializeField]
        private float _overlapSphereUpdatesPerSecond = 12;

        float overlapSphereDuration
        {
            get
            {
                return 1f / _overlapSphereUpdatesPerSecond;
            }
        }

        private float _nextOverlapSphereTime = 0;

        private Collider[] _collidersCache;
        private List<IVREquippable> _equippablesFoundCache = new List<IVREquippable>();

        private void OnTriggerEnter(Collider other)
        {
            if (method == HighlightMethods.Triggers)
            {
                // TODO: consider optimizing the GetComponent away
                var equippable = other.GetComponentInParent<IVREquippable>();
                if (equippable == null/* || equippable.isEquipped*/)
                {
                    // the thing is not equippable or already equipped
                    return;
                }

                _hand.Highlight(equippable);

            }
        }

        private void OnTriggerExit(Collider other)
        {
            if (method == HighlightMethods.Triggers)
            {
                // TODO: consider optimizing the GetComponent away
                var equippable = other.GetComponentInParent<IVREquippable>();
                if (equippable == null)
                {
                    // the thing is not equippable or already equipped
                    return;
                }

                _hand.Unhighlight(equippable);

            }
        }

        private void Update()
        {
            if (method == HighlightMethods.OverlapSphere)
            {
                // infrequent updating
                if (_nextOverlapSphereTime < Time.time)
                {
                    _nextOverlapSphereTime = Time.time + overlapSphereDuration;
                }
                else
                {
                    return;
                }

                // highlight new stuff first, then unhighlight all the stuff that isn't highlighted anymore
                _collidersCache = VRPhysics.OverlapSphere(transform.position, VRPrefabManager.instance.maxEquipDistance);
                _equippablesFoundCache.Clear();
                for (int i = 0; i < _collidersCache.Length; i++)
                {
                    if (_collidersCache[i] != null)
                    {
                        var rb = _collidersCache[i].attachedRigidbody;
                        if (rb != null)
                        {
                            var eq = rb.GetComponent<IVREquippable>();
                            if (eq != null)
                            {
                                _equippablesFoundCache.Add(eq);
                            }

                        }
                    }
                }
                // highlight equippables in the equippables found cache, unhighlight all other equippables in the hand highlighted list
                for (int i = 0; i < _equippablesFoundCache.Count; i++)
                {
                    _hand.Highlight(_equippablesFoundCache[i]);
                }

                for (int i = 0; i < _hand.highlighted.Count; i++)
                {
                    if (!_equippablesFoundCache.Contains(_hand.highlighted[i]))
                    {
                        _hand.Unhighlight(_hand.highlighted[i]);
                    }
                }
            }
        }

        internal void SetHand(IVRHand hand)
        {
            _hand = hand;
        }
    }
}