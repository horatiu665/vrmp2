namespace VRCore
{
    using UnityEngine;

    /// <summary>
    /// A default equippable class that can be used while prototyping to enable an object to be equippable, without adding any functionality.
    /// </summary>
    /// <seealso cref="VRCore.VREquippableBase" />
    [RequireComponent(typeof(Rigidbody))]
    public sealed class VREquippableDefault : VREquippableBase
    {
    }
}