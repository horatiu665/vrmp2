namespace VRCore.VRNetwork
{
    using VRCore;

    public abstract class PrefabTypeMessageBase : DataMessage
    {
        public PrefabTypeMessageBase(NetMessageType type) : base(type)
        {
        }

        public PrefabTypeMessageBase(byte[] buffer) : base(buffer)
        {
        }

        public VRPrefabType prefabType
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return this.prefabType.GetByteSize();
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write(this.prefabType);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.prefabType = s.ReadPrefabType();
        }
    }
}