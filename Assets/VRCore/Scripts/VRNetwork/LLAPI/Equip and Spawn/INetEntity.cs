namespace VRCore.VRNetwork
{
    public interface INetEntity : IVRPrefab
    {
        /// <summary>
        /// Gets the net identifier - used for uniquely identifying entities on the network.
        /// </summary>
        /// <value>
        /// The net identifier.
        /// </value>
        int netId { get; }

        /// <summary>
        /// Gets or sets the time of the last interaction (spawn, equip or unequip).
        /// </summary>
        /// <value>
        /// The last interaction.
        /// </value>
        float lastInteraction { get; set; }

        /// <summary>
        /// Sets the specified net identifier.
        /// </summary>
        /// <param name="netId">The net identifier.</param>
        void SetNetId(int netId);
    }
}