namespace VRCore.VRNetwork
{
    public interface INetSpawnable : IVRSpawnable, INetEntity
    {
    }
}