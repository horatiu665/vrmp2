namespace VRCore.VRNetwork
{
    using Core;
    using Core.Server;
    using Server;
    using UnityEngine;
    using UnityEngine.Networking;

    public sealed class NetEntityDestructionManager : SingletonMonoBehaviour<NetEntityDestructionManager>
    {
        private readonly DestroyNetEntityMessage _msg = new DestroyNetEntityMessage();

        private void Start()
        {
            // We expect the network to have initialized in OnEnable, so that we can expect it to be ready in Start.
            if (!NetServices.isServer)
            {
                Debug.LogWarning(this.ToString() + " the NetEntityDestructionManager is only meant to be used on the Server! Destroying it now.");
                Destroy(this);
                return;
            }
        }

        public void DestroyNetEntity(INetEntity entity)
        {
            if (!ServerNetReceiver.instance.RemoveNetEntity(entity))
            {
                // entity is no longer registered, must have been deleted already
                return;
            }

            _msg.netEntityId = entity.netId;
            ServerNetSender.instance.SendToAll(_msg, QosType.ReliableSequenced);

            //entity.SetNetId(0);
            VRPrefabManager.instance.Return(entity);
        }
    }
}