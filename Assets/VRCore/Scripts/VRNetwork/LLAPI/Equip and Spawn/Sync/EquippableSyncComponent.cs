namespace VRCore.VRNetwork
{
    using Core;
    using UnityEngine;

    [RequireComponent(typeof(INetEquippable))]
    public class EquippableSyncComponent : MonoBehaviour, IOriginShifter
    {
        [SerializeField]
        private bool _interpolate = true;

        [SerializeField]
        private Vector2 _syncDeltaTimeLimits = new Vector2(0.001f, 0.2f);

        [SerializeField]
        private float _minDeltaPositionThreshold = 0.005f;

        [SerializeField, ReadOnly]
        private bool _hasBeenUpdated;

        private INetEquippable _equippable;
        private Vector3 _lastSyncPos;
        private Quaternion _lastSyncRot;
        private Vector3 _targetPosition;
        private Quaternion _targetRotation;
        private Vector3 _lastSyncVel;
        private Vector3 _lastSyncAV;
        private Vector3 _targetVel;
        private Vector3 _targetAV;
        private float _lerpTime;

        // 2 network frames ago
        private float _prevSyncTime;

        // 1 network frame ago
        private float _lastSyncTime;

        private Rigidbody rb
        {
            get
            {
                return _equippable.rigidbody;
            }
        }

        Joint _j;
        private Joint joint
        {
            get
            {
                if (_j == null)
                {
                    _j = rb.GetComponent<Joint>();
                }
                return _j;
            }
        }
        
        [SerializeField]
        private Vector2 playerSpeedToLerpParaem = new Vector2(5f, 25f);

        [SerializeField]
        private Vector2 _syncLerpRange = new Vector2(0.5f, 0.005f);

        [SerializeField]
        private float _maxUpdateRatio = 0.25f;

        [Range(0, 2)]
        public int extrapolation = 1;

        /// <summary>
        /// Gets a value indicating whether this <see cref="RigidbodySyncComponent"/> has changed position or rotation since last update. Only used by the server.
        /// </summary>
        public bool changed
        {
            get { return (_lastSyncPos != this.transform.position) || (_lastSyncRot != this.transform.rotation); }
        }

        public bool isEquipped
        {
            get { return _equippable.isEquipped; }
        }

        public PlayerBase player
        {
            get
            {
                if (_equippable != null)
                {
                    return _equippable.player as PlayerBase;
                }
                return null;
            }
        }

        private void Awake()
        {
            _equippable = this.GetComponent<INetEquippable>();
        }

        private void OnEnable()
        {
            OriginShiftManager.OriginShiftersAdd(this);

        }

        private void OnDisable()
        {
            OriginShiftManager.OriginShiftersRemove(this);
            ResetSync();
        }

        public void OnWorldMove(Vector3 originShiftDelta)
        {
            _lastSyncPos -= originShiftDelta;
            _targetPosition -= originShiftDelta;
            
        }

        public void ResetSync()
        {
            _lastSyncRot = _targetRotation = new Quaternion(0f, 0f, 0f, 0f);
            _lastSyncPos = _targetPosition = Vector3.zero;
            _lastSyncVel = _targetVel = Vector3.zero;
            _lastSyncAV = _targetAV = Vector3.zero;
            _hasBeenUpdated = false;
            _prevSyncTime = 0f;
            _lastSyncTime = 0f;
            _lerpTime = 0;
        }

        // Only run on the clients
        private void FixedUpdate()
        {
            if (!_interpolate || !_hasBeenUpdated || NetServices.isServer)
            {
                return;
            }

            if (_equippable.isEquipped)
            {
                // the first frame after having been equipped (meaning no updates), we must get a new update before moving to it so that it does not move momentarily back to a previous old location
                _hasBeenUpdated = false;
                return;
            }

            float playerLerpParam = 0f;
            if (player != null)
            {
                playerLerpParam = Mathf.InverseLerp(player.velocity.magnitude, this.playerSpeedToLerpParaem.x, this.playerSpeedToLerpParaem.y);
            }
            
            if ((transform.position - _targetPosition).sqrMagnitude < (_minDeltaPositionThreshold * _minDeltaPositionThreshold))
            {
                // if the position change is super small then stop lerping to it
                return;
            }

            UpdatePos(playerLerpParam);
        }

        private void UpdatePos(float playerLerpParam)
        {
            var lastTimeDelta = Mathf.Clamp(_lastSyncTime - _prevSyncTime, _syncDeltaTimeLimits.x, _syncDeltaTimeLimits.y);
            var t = Mathf.Clamp01(_lerpTime / lastTimeDelta);

            bool updateIt = t <= _maxUpdateRatio;
            // only update while we're within the last sync frames. if we exceed, we let the simulation work 100% locally
            if (updateIt)
            {
                // what is the most likely next frame pos for the server, according to the change in velocity of the previous two frames?
                // lastSyncPos goes with lastSyncVel
                // targetPos goes with targetVel
                // nextPos can be (targetPos - lastSyncPos) * (targetVel / lastSyncVel)
                // so we take into account linear acceleration.

                var lerpAmount = Mathf.Lerp(_syncLerpRange.x, _syncLerpRange.y, playerLerpParam);
                rb.velocity = Vector3.Lerp(rb.velocity, Vector3.LerpUnclamped(_lastSyncVel, _targetVel, t), lerpAmount);
                rb.angularVelocity = Vector3.Lerp(rb.angularVelocity, Vector3.LerpUnclamped(_lastSyncAV, _targetAV, t), lerpAmount);

                if (extrapolation == 0)
                {
                    // no extrapolation
                    transform.position = Vector3.Lerp(transform.position, Vector3.LerpUnclamped(_lastSyncPos, _targetPosition, t), lerpAmount);
                }
                else if (extrapolation == 1)
                {
                    // extrapolate to nextPos
                    var nextPos = _targetPosition + (_targetPosition - _lastSyncPos) * (_targetVel.magnitude / _lastSyncVel.magnitude);
                    transform.position = Vector3.Lerp(transform.position, Vector3.LerpUnclamped(_targetPosition, nextPos, t), lerpAmount);
                }
                // tested extrapolation = 2 and it's too much in VR. works fairly well in the non-vr test scene tho. weird.
                else if (extrapolation == 2)
                {
                    // extrapolate twice to nextNextPos
                    var nextPos = _targetPosition + (_targetPosition - _lastSyncPos) * (_targetVel.magnitude / _lastSyncVel.magnitude);
                    var nextVel = _targetVel + (_targetVel - _lastSyncVel);
                    var nextNextPos = nextPos + (nextPos - _targetPosition) * (nextVel.magnitude / _targetVel.magnitude);
                    transform.position = Vector3.Lerp(transform.position, Vector3.LerpUnclamped(nextPos, nextNextPos, t), lerpAmount);
                }

                transform.rotation = Quaternion.SlerpUnclamped(transform.rotation, Quaternion.SlerpUnclamped(_lastSyncRot, _targetRotation, t), lerpAmount);
            }

            _lerpTime += Time.fixedDeltaTime;
        }

        /// <summary>
        /// Handles an <see cref="EquippableSyncData"/> update. Only called on clients.
        /// </summary>
        /// <param name="syncData">The synchronized data.</param>
        /// <param name="initialSetup">If true, applies the position and rotation immediately. Otherwise, sets targets used in interpolation.</param>
        public void HandleUpdate(EquippableSyncData syncData, bool initialSetup = false)
        {
            if (!_interpolate || initialSetup)
            {
                if (_equippable.isEquipped)
                {
                    // we will never allow setting position and rotation on an equipped equippable
                    return;
                }

                rb.velocity = syncData.velocity;
                rb.angularVelocity = syncData.angularVelocity;
                this.transform.position = syncData.position;
                this.transform.rotation = syncData.rotation;
            }
            else
            {
                if (_targetPosition == Vector3.zero)
                {
                    _lastSyncPos = syncData.position;
                }
                else
                {
                    _lastSyncPos = _targetPosition;
                }

                if (_targetRotation.IsZero())
                {
                    _lastSyncRot = syncData.rotation;
                }
                else
                {
                    _lastSyncRot = _targetRotation;
                }

                if (_targetVel == Vector3.zero)
                {
                    _lastSyncVel = syncData.velocity;
                }
                else
                {
                    _lastSyncVel = _targetVel;
                }

                if (_targetAV == Vector3.zero)
                {
                    _lastSyncAV = syncData.angularVelocity;
                }
                else
                {
                    _lastSyncAV = _targetAV;
                }

                _targetPosition = syncData.position;
                _targetRotation = syncData.rotation;
                _targetVel = syncData.velocity;
                _targetAV = syncData.angularVelocity;

                if (!_hasBeenUpdated)
                {
                    // we are updating after a hiatus, so the old positions are no longer valid
                    _lastSyncPos = _targetPosition;
                    _lastSyncRot = _targetRotation;
                    _lastSyncVel = _targetVel;
                    _lastSyncAV = _targetAV;
                }

                _hasBeenUpdated = true;

                _prevSyncTime = _lastSyncTime;
                _lastSyncTime = Time.timeSinceLevelLoad;
                _lerpTime = 0;
            }
        }

        /// <summary>
        /// Gets the synchronize data. Also records the position and rotation for use in changed check. Only called on server.
        /// </summary>
        /// <returns></returns>
        public EquippableSyncData GetSyncData()
        {
            var pos = this.transform.position;
            var rot = this.transform.rotation;
            var vel = rb.velocity;
            var av = rb.angularVelocity;

            _lastSyncPos = pos;
            _lastSyncRot = rot;
            _lastSyncVel = vel;
            _lastSyncAV = av;

            return new EquippableSyncData()
            {
                netId = _equippable.netId,
                prefabType = _equippable.prefabType,
                position = pos,
                rotation = rot,
                velocity = vel,
                angularVelocity = av
            };
        }
    }
}