namespace VRCore.VRNetwork
{
    using System.Collections.Generic;

    public class EquippableSyncMessage : DataMessage
    {
        public EquippableSyncMessage(byte[] buffer) : base(buffer)
        {
        }

        public EquippableSyncMessage() : base(NetMessageType.EquippableSyncUpdate)
        {
        }

        public static int bytesPerData
        {
            get
            {
                // net id (4) + precise position (12) + imprecise vel and AV (6 + 6) + imprecise quaternion (8) + VR Prefab Type
                return 36 + ((VRPrefabType)0).GetByteSize();
            }
        }

        public List<EquippableSyncData> data
        {
            get;
            private set;
        }

        /// <summary>
        /// Whether this message is an initial setup message for a new player joining.
        /// </summary>
        public bool initial
        {
            get;
            set;
        }

        public void SetData(EquippableSyncData[] data)
        {
            if (this.data == null)
            {
                this.data = new List<EquippableSyncData>(data.Length);
            }
            else
            {
                this.data.Clear();
            }

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i].IsNull())
                {
                    // the first "null" (default valued) element we encounter means the end of the actual data
                    break;
                }

                this.data.Add(data[i]);
            }
        }

        protected override int GetByteSize()
        {
            return 2 + bytesPerData * data.Count; // initial (1) + count (1) + data
        }

        protected override void Serialize(NetSerializer s)
        {
            s.Write(this.initial);

            var count = this.data.Count;
            s.Write((byte)count);

            for (int i = 0; i < count; i++)
            {
                var d = this.data[i];
                s.Write(d.netId);
                s.Write(d.prefabType);
                s.WriteExact(d.position);
                s.Write(d.rotation);
                s.Write(d.velocity);
                s.Write(d.angularVelocity);
            }
        }

        protected override void Deserialize(NetDeserializer s)
        {
            this.initial = s.ReadBool();

            var count = s.ReadByte();
            if (this.data == null)
            {
                this.data = new List<EquippableSyncData>(count);
            }
            else
            {
                this.data.Clear();
            }

            for (int i = 0; i < count; i++)
            {
                this.data.Add(new EquippableSyncData()
                {
                    netId = s.ReadInt(),
                    prefabType = s.ReadPrefabType(),
                    position = s.ReadExactVector3(),
                    rotation = s.ReadQuaternion(),
                    velocity = s.ReadVector3(),
                    angularVelocity = s.ReadVector3()
                });
            }
        }
    }
}