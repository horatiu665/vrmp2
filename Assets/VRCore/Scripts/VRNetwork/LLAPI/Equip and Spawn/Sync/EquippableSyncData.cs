namespace VRCore.VRNetwork
{
    using UnityEngine;

    public struct EquippableSyncData
    {
        public int netId;
        public VRPrefabType prefabType;
        public Vector3 position;
        public Quaternion rotation;
        public Vector3 velocity;
        public Vector3 angularVelocity;

        public bool IsNull()
        {
            return netId == 0 &&
                   prefabType == 0 &&
                   position == Vector3.zero &&
                   rotation.IsZero() &&
                   velocity == Vector3.zero &&
                   angularVelocity == Vector3.zero;
        }

        public override string ToString()
        {
            return string.Concat("EquippableSyncData:: net id == ", netId.ToString(), 
                ", prefab type == ", prefabType.ToString(),
                ", position == ", position.ToString(),
                ", rotation == ", rotation.ToString(),
                ", velocity == ", velocity.ToString(),
                ", angularVelocity == ", angularVelocity.ToString()
                );
        }
    }
}