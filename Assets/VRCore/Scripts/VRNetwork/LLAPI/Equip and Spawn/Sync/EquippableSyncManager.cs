namespace VRCore.VRNetwork
{
    using System.Collections.Generic;
    using Core.Server;
    using Server;
    using UnityEngine;
    using UnityEngine.Networking;

    public class EquippableSyncManager : SingletonMonoBehaviour<EquippableSyncManager>
    {
        [SerializeField, Range(0.1f, 90f), Tooltip("How many times per second the rigidbody sync manager may send sync package updates over the network.")]
        private float _sendRate = 10f;

        [SerializeField]
        private int _maxEquippablesPerMessage = 35;

        [SerializeField, Range(30, 1471)]
        private int _maxMessageSize = 1400;

        private readonly EquippableSyncMessage _msg = new EquippableSyncMessage();

        private List<INetEntity> _tempEntities;
        private EquippableSyncData[] _syncs;
        private int _maxSyncsPerMessage;
        private float _lastSend;
        // when there are too many syncs for sending in one frame, the loop tries to send the rest in the next frame.
        // To do this, it skips this amount of objects in the enumeration in the new frame (because the enumeration can change between frames so we cannot keep a reference)
        private int _skipSyncs = 0;

        protected override void Awake()
        {
            base.Awake();

            _maxSyncsPerMessage = Mathf.Min(_maxEquippablesPerMessage, _maxMessageSize / EquippableSyncMessage.bytesPerData);
            _syncs = new EquippableSyncData[_maxSyncsPerMessage];
        }

        private void OnEnable()
        {
            // wait 'one update' before starting to send (to ensure proper initialization has had time)
            _lastSend = Time.timeSinceLevelLoad + (1f / _sendRate);
        }

        private void Start()
        {
            // We expect the network to have initialized in OnEnable, so that we can expect it to be ready in Start.
            if (!NetServices.isServer)
            {
                Debug.LogWarning(this.ToString() + " the EquippableSyncManager is only meant to be used on the Server! Destroying it now.");
                Destroy(this);
                return;
            }
        }

        private void Update()
        {
            var time = Time.timeSinceLevelLoad;
            if (time < _lastSend)
            {
                return;
            }

            if (ServerNetSender.instance.players.Count == 0)
            {
                return;
            }

            _lastSend = time + (1f / _sendRate);
            OnSend();
        }

        private void OnSend()
        {
            var netEntities = ServerNetReceiver.instance.netEntities;
            if (netEntities.Count == 0)
            {
                // nothing to synchronize
                return;
            }

            _syncs.Clear();

            var idx = 0;
            var enumerator = netEntities.GetEnumerator();
            try
            {
                var syncsSkipper = _skipSyncs;
                while (enumerator.MoveNext())
                {
                    // skips elements because of the multi-frame sending. like it would continue with the next id in a list.
                    if (syncsSkipper-- > 0)
                    {
                        continue;
                    }

                    var ne = enumerator.Current.Value;
                    if (ne == null || ne.Equals(null))
                    {
                        continue;
                    }

                    var sync = ne.GetComponent<EquippableSyncComponent>();
                    if (sync == null)
                    {
                        continue;
                    }

                    if (sync.isEquipped)
                    {
                        // do NOT update equipped equippables, since they are updated through prediction
                        continue;
                    }

                    if (!sync.changed)
                    {
                        continue;
                    }

                    _syncs[idx++] = sync.GetSyncData();

                    if (idx >= _maxSyncsPerMessage)
                    {
                        // if we cannot send any more syncs in one message, we stop adding more and instead continue sending in the next frame (rather than on next interval tick)
                        _lastSend = 0f;
                        _skipSyncs += idx;
                        break;
                    }
                }
                if (_lastSend != 0)
                {
                    _skipSyncs = 0;
                }
            }
            finally
            {
                enumerator.Dispose();
            }

            if (idx == 0)
            {
                // no changes this time
                return;
            }

            _msg.initial = false;
            _msg.SetData(_syncs);
            ServerNetSender.instance.SendToAll(_msg, QosType.UnreliableSequenced);
            
        }

        public void HandleNewPlayer(int connectionId)
        {
            var netEntities = ServerNetReceiver.instance.netEntities;
            var count = netEntities.Count;
            if (count == 0)
            {
                // nothing to synchronize
                return;
            }

            var serverNetSender = ServerNetSender.instance;
            _syncs.Clear();
            var idx = 0;

            _msg.initial = true;

            if (count < _maxSyncsPerMessage)
            {
                // there are not more net entities than what can be sent in one message
                var enumerator = netEntities.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        var ne = enumerator.Current.Value;
                        if (ne == null || ne.Equals(null))
                        {
                            continue;
                        }

                        var sync = ne.GetComponent<EquippableSyncComponent>();
                        if (sync == null)
                        {
                            continue;
                        }

                        _syncs[idx++] = sync.GetSyncData();
                    }
                }
                finally
                {
                    enumerator.Dispose();
                }

                _msg.SetData(_syncs);
                serverNetSender.Send(connectionId, _msg, QosType.Reliable);
                return;
            }

            // for the cases where we must split up into multiple messages, we need a list so that we can index directly in at any index
            if (_tempEntities == null)
            {
                _tempEntities = new List<INetEntity>(netEntities.Values);
            }
            else
            {
                _tempEntities.Clear();
                _tempEntities.AddRange(netEntities.Values);
            }

            // there are more net entities than what can be contained in one message
            var messageCount = Mathf.CeilToInt((float)count / _maxSyncsPerMessage);
            for (int i = 0; i < messageCount; i++)
            {
                var step = _maxSyncsPerMessage * i;
                var end = step + Mathf.Min(_maxSyncsPerMessage, count - step);
                for (int j = step; j < end; j++)
                {
                    var ne = _tempEntities[i];
                    if (ne == null || ne.Equals(null))
                    {
                        continue;
                    }

                    var sync = ne.GetComponent<EquippableSyncComponent>();
                    if (sync == null)
                    {
                        continue;
                    }

                    _syncs[idx++] = sync.GetSyncData();
                }

                idx = 0;
                _msg.SetData(_syncs);
                serverNetSender.Send(connectionId, _msg, QosType.Reliable);
                _syncs.Clear();
            }
        }
    }
}