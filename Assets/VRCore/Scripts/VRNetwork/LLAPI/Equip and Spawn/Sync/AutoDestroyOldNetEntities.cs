namespace VRCore.VRNetwork
{
    using System.Collections.Generic;
    using Apex;
    using Core.Server;
    using UnityEngine;

    [RequireComponent(typeof(NetEntityDestructionManager))]
    public sealed class AutoDestroyOldNetEntities : MonoBehaviour
    {
        [SerializeField]
        private float _checkFrequency = 6f;

        [SerializeField]
        private float _minLastInteractionDeltaTime = 30f;

        [SerializeField]
        private int _maxNetEntityCapacity = 200;

        private readonly List<INetEntity> _entities = new List<INetEntity>();

        private NetEntityDestructionManager _manager;
        private float _lastCheck;

        private void Awake()
        {
            _manager = this.GetComponent<NetEntityDestructionManager>();
            _lastCheck = Time.timeSinceLevelLoad + _checkFrequency;
        }

        private void Update()
        {
            var time = Time.timeSinceLevelLoad;
            if (time < _lastCheck)
            {
                return;
            }

            _lastCheck = time + _checkFrequency;

            var netReceiver = ServerNetReceiver.instance;
            var netEntities = netReceiver.netEntities;
            if (netEntities.Count <= _maxNetEntityCapacity)
            {
                // there are not too many net entities yet
                return;
            }

            var deleteCount = netEntities.Count - _maxNetEntityCapacity;
            _entities.EnsureCapacity(deleteCount);

            var enumerator = netEntities.GetEnumerator();
            try
            {
                while (enumerator.MoveNext())
                {
                    var netEntity = enumerator.Current.Value;
                    if (netEntity == null || netEntity.Equals(null))
                    {
                        continue;
                    }

                    var equippable = enumerator.Current.Value as INetEquippable;
                    if (equippable != null && equippable.isEquipped)
                    {
                        // NEVER remove equipped net entities
                        continue;
                    }

                    if ((time - netEntity.lastInteraction) < _minLastInteractionDeltaTime)
                    {
                        // the net entity was recently interacted with, thus we cannot delete it yet
                        continue;
                    }

                    _entities.Add(netEntity);
                    if (_entities.Count >= deleteCount)
                    {
                        break;
                    }
                }
            }
            finally
            {
                enumerator.Dispose();
            }

            deleteCount = _entities.Count;
            if (deleteCount == 0)
            {
                return;
            }

            for (int i = deleteCount - 1; i >= 0; i--)
            {
                _manager.DestroyNetEntity(_entities[i]);
            }

            _entities.Clear();
        }
    }
}