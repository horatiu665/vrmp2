namespace VRCore.VRNetwork
{
    public interface INetSpawnableEquippable : IVRSpawnableEquippable, INetSpawnable, INetEquippable
    {
    }
}