namespace VRCore.VRNetwork
{
    using UnityEngine;

    public abstract class NetSpawnableBase : VRSpawnableBase, INetSpawnable
    {
        [SerializeField, ReadOnly]
        private int _netId;

        /// <summary>
        /// Gets the net identifier.
        /// </summary>
        /// <value>
        /// The net identifier.
        /// </value>
        public int netId
        {
            get { return _netId; }
        }

        /// <summary>
        /// Gets or sets the last interaction.
        /// </summary>
        /// <value>
        /// The last interaction.
        /// </value>
        public float lastInteraction
        {
            get;
            set;
        }

        protected virtual void OnEnable()
        {
            this.RecordInteractionTime();
        }

        /// <summary>
        /// Sets the net identifier.
        /// </summary>
        /// <param name="netId">The net identifier.</param>
        public void SetNetId(int netId)
        {
            _netId = netId;

#if UNITY_EDITOR
            var originalName = this.name;
            var idx = originalName.IndexOf("(ID:");
            if (idx > 0)
            {
                originalName = originalName.Substring(0, idx - 1);
            }

            this.name = string.Concat(originalName, " (ID: ", _netId.ToString(), ")");
#endif
        }
    }
}