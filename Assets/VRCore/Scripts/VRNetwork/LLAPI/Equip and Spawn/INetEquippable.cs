namespace VRCore.VRNetwork
{
    public interface INetEquippable : IVREquippable, INetEntity
    {
    }
}