namespace VRCore.VRNetwork
{
    using Apex;

    public static class NetDeserializerPool
    {
        static NetDeserializerPool()
        {
            PoolManager.Configure(new PoolConfig[]
            {
                new PoolConfig()
                {
                    entityType = typeof(NetDeserializer),
                    initialInstanceCount = 2,
                }
            });
        }

        public static NetDeserializer Get(byte[] buffer)
        {
            var deserializer = PoolManager.Get<NetDeserializer>();
            deserializer.SetBuffer(buffer);
            return deserializer;
        }

        public static void Return(NetDeserializer deserializer)
        {
            deserializer.Recycle();
        }
    }
}