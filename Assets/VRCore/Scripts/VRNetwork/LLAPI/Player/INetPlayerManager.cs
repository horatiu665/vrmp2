namespace VRCore.VRNetwork
{
    using System.Collections.Generic;

    public interface INetPlayerManager
    {
        IDictionary<short, INetPlayer> players { get; }

        int GetConnectionId(short netId);
    }
}