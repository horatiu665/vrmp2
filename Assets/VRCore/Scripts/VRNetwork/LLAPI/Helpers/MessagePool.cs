namespace VRCore.VRNetwork
{
    using System;
    using Apex;
    using UnityEngine;

    /// <summary>
    /// A message pool for easily pooling any <see cref="DataMessage"/>-derived network messages.
    /// </summary>
    public static class MessagePool
    {
        //public static void Initialize(params Type[] messageTypes)
        //{
        //    //PoolManager.Conf
        //}

        /// <summary>
        /// Gets a pooled <see cref="DataMessage"/>, deserialized from the given buffer.
        /// </summary>
        /// <typeparam name="T">The type of <see cref="DataMessage"/> to get.</typeparam>
        /// <param name="buffer">The buffer to deserialize from.</param>
        /// <returns>A DataMessage of the specified type, serialized from the given buffer.</returns>
        public static T Get<T>(byte[] buffer) where T : DataMessage, new()
        {
            var message = Get<T>();
            message.Deserialize(buffer);
            return message;
        }

        /// <summary>
        /// Gets a pooled <see cref="NetIdMessageBase"/>, with the given net id already set.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="netId">The net identifier.</param>
        /// <returns>A PlayerMessageBase of the specified type, with the netId already set.</returns>
        public static T Get<T>(short netId) where T : NetIdMessageBase, new()
        {
            var message = Get<T>();
            message.netId = netId;
            return message;
        }

        /// <summary>
        /// Gets a pooled <see cref="DataMessage"/>, in a clean state.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns>A DataMessage of the specified type, in a clean state.</returns>
        public static T Get<T>() where T : DataMessage, new()
        {
            return GetAndCreatePoolIfMissing<T>();
            //return PoolManager.Get<T>();
        }

        private static T GetAndCreatePoolIfMissing<T>() where T : DataMessage, new()
        {
            // TODO: REFACTOR!!!!
            T msg;
            try
            {
                msg = PoolManager.Get<T>();
            }
            catch (System.ArgumentException e)
            {
                Debug.LogWarning("MessagePool warning == " + e.Message + "\n" + e.StackTrace);

                PoolManager.RegisterPool(new BasicPool<T>(4));
                msg = PoolManager.Get<T>();
            }

            return msg;
        }

        /// <summary>
        /// Returns the specified <see cref="DataMessage"/> to the message pool.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="message">The message.</param>
        public static void Return<T>(T message) where T : DataMessage
        {
            message.Recycle();
        }
    }
}