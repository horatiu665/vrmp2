using Apex.Services;

namespace VRCore.VRNetwork
{
    public abstract class RigidbodySyncManager : SingletonMonoBehaviour<RigidbodySyncManager>
    {
        protected virtual void Start()
        {
            GameServices.messageBus.Post(new RigidbodySyncManagerInitializationMessage(this));
        }

        public abstract void Register(RigidbodySyncComponent sync);

        public abstract void Unregister(RigidbodySyncComponent sync);
    }
}