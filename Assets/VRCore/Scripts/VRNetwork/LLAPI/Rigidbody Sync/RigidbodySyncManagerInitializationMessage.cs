namespace VRCore.VRNetwork
{
    public class RigidbodySyncManagerInitializationMessage
    {
        public RigidbodySyncManager manager;

        public RigidbodySyncManagerInitializationMessage(RigidbodySyncManager manager)
        {
            this.manager = manager;
        }
    }
}