namespace VRCore.VRNetwork
{
    using Client;
    using UnityEngine;
    using UnityEngine.Networking;

    [RequireComponent(typeof(Rigidbody))]
    public sealed class ApplyForceToPlayerComponent : MonoBehaviour
    {
        [SerializeField]
        private float _forceMultiplier = 0.3f;

        [SerializeField]
        private Vector3 _victimPositionOffset = Vector3.up;

        private void OnCollisionEnter(Collision collision)
        {
            if (!NetServices.isClient)
            {
                // Only run this on clients
                return;
            }

            if (collision.rigidbody == null)
            {
                // no rigidbody => cannot apply forces
                return;
            }

            var playerVictim = collision.collider.GetPlayer<INetPlayer>();
            if (playerVictim == null || playerVictim.isLocal)
            {
                // if the hit entity was not a player, or was the local player, we cannot apply forces to it
                return;
            }

            var force = ((playerVictim.position + _victimPositionOffset) - collision.contacts[0].point) * _forceMultiplier;

            var msg = MessagePool.Get<ApplyForceToPlayerMessage>(playerVictim.netId);
            msg.force = force;

            ClientNetSender.instance.Send(msg, QosType.ReliableSequenced);
            MessagePool.Return(msg);
        }
    }
}