namespace VRCore.VRNetwork.Client
{
    using Core;
    using System;
    using UnityEngine;
    using UnityEngine.Networking;

    public class ClientNetReceiverDefault : ClientNetReceiverBase<ClientNetReceiverDefault>
    {
        [SerializeField]
        protected GameObject _localPlayerPrefab = null;

        protected override void Awake()
        {
            base.Awake();
            AddAllNetEntities<GenericEquippable>();
        }

        public override int GetConnectionId(short netId)
        {
            throw new NotImplementedException();
        }

        public override void OnConnect(int connectionId, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                // an error occured !
                Debug.LogError(this.ToString() + " OnConnect could not establish a connection to server, error == " + error.ToString());
                return;
            }

            // Connection to server established, send a name message
            var localConnectMsg = MessagePool.Get<PlayerLocalConnectMessageDefault>();
            var pt = PlayerTypeManager.instance != null ? PlayerTypeManager.instance.currentGamePlayerType : PlayerType.Normal;
            localConnectMsg.playerType = pt;//            PlayerType.Normal; // TODO: Get player type elsewhere ?
            _network.Send(localConnectMsg, QosType.Reliable);
            MessagePool.Return(localConnectMsg);

            Debug.Log(this.ToString() + " OnConnect. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString());
        }

        public override void OnData(int connectionId, byte[] buffer, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                Debug.LogWarning(this.ToString() + " OnData from connection ID == " + connectionId.ToString() + ", buffer size == " + buffer.Length.ToString() + ", encountered NetworkError == " + error.ToString());
                return;
            }

            var messageType = buffer.PeekType();
            ////Debug.Log(this.ToString() + " OnData. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString() + ", message type == " + messageType.ToString() + ", buffer size == " + buffer.Length.ToString() + ", contents=\n" + buffer.DebugLogContents());

            switch (messageType)
            {
                case NetMessageType.LocalStart:
                {
                    HandlePlayerLocalStart(buffer);
                    break;
                }

                case NetMessageType.RemoteConnect:
                {
                    HandlePlayerRemoteConnect(buffer);
                    break;
                }

                case NetMessageType.Disconnect:
                {
                    HandlePlayerDisconnect(buffer);
                    break;
                }

                case NetMessageType.VRBodyUpdate:
                {
                    HandleVRBodyUpdate(buffer);
                    break;
                }

                case NetMessageType.SpawnResponse:
                {
                    HandleSpawn(buffer);
                    break;
                }

                case NetMessageType.SpawnEquipResponse:
                {
                    HandleSpawnEquip(buffer);
                    break;
                }

                case NetMessageType.Equip:
                {
                    HandleEquip(buffer);
                    break;
                }

                case NetMessageType.Unequip:
                {
                    HandleUnequip(buffer);
                    break;
                }

                case NetMessageType.ApplyForceToPlayer:
                {
                    HandleApplyForceToPlayer(buffer);
                    break;
                }

                default:
                {
                    Debug.LogError(this.ToString() + " OnData unhandled MessageType == " + messageType.ToString());
                    break;
                }
            }
        }

        public override void OnDisconnect(int connectionId, NetworkError error)
        {
            Debug.Log(this.ToString() + " OnDisconnect. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString());
        }

        protected virtual void HandlePlayerLocalStart(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerLocalStartMessageDefault>(buffer);
            var id = msg.netId;
            if (GetPlayer(id) != null)
            {
                Debug.LogError(this.ToString() + " HandlePlayerStart another player is already registered with net id == " + id.ToString());
                MessagePool.Return(msg);
                return;
            }

            var pos = msg.position;

            // do not use a pool for the local player, since there will only ever be one of those
            var localPlayer = PlayerTypeManager.instance.InstantiatePlayer<INetPlayer>(msg.playerType, GameType.Local, pos);
            localPlayer.SetIsLocal();

            AddPlayer(localPlayer, id);
            MessagePool.Return(msg);

            Debug.Log(this.ToString() + " HandlePlayerStart created local player by id == " + localPlayer.netId.ToString());
        }

        protected virtual void HandlePlayerRemoteConnect(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerRemoteConnectMessageDefault>(buffer);
            var id = msg.netId;
            if (GetPlayer(id) != null)
            {
                Debug.LogError(this.ToString() + " HandlePlayerConnect another player is already registered with net id == " + id.ToString());
                MessagePool.Return(msg);
                return;
            }

            ////Debug.Log(this.ToString() + " HandlePlayerConnect create remote player by id == " + id.ToString() + ", name == " + msg.name.ToString() + ", at position == " + msg.position.ToString());

            var player = PlayerTypeManager.instance.InstantiatePlayer<INetPlayer>(msg.playerType, GameType.Remote, msg.position);
            AddPlayer(player, id);
            MessagePool.Return(msg);

            Debug.Log(this.ToString() + " HandlePlayerConnect created remote player by id == " + player.netId.ToString());
        }

        protected virtual void HandlePlayerDisconnect(byte[] buffer)
        {
            var msg = MessagePool.Get<PlayerDisconnectMessageDefault>(buffer);
            var player = GetPlayer<INetPlayer>(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandlePlayerDisconnect received disconnect message for unknown player with net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            Debug.Log(this.ToString() + " HandlePlayerDisconnect for player by netId == " + msg.netId.ToString());

            if (!RemovePlayer(msg.netId))
            {
                Debug.LogWarning(this.ToString() + " found player by id == " + msg.netId.ToString() + ", removal was unsuccessful");
            }

            PlayerTypeManager.instance.Return(player);
            MessagePool.Return(msg);
        }

        protected virtual void HandleVRBodyUpdate(byte[] buffer)
        {
            var msg = MessagePool.Get<VRBodyUpdateMessage>(buffer);
            var player = GetPlayer<INetPlayer>(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleVRBodyUpdate no player found through net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            if (player.isLocal)
            {
                Debug.LogWarning(this.ToString() + " HandleVRBodyUpdate player by net id == " + msg.netId.ToString() + " is a local player, and thus cannot be updated");
                MessagePool.Return(msg);
                return;
            }

            var data = new VRBodyUpdateData()
            {
                position = msg.position,

                headPosition = msg.headPosition,
                headRotation = msg.headRotation,

                leftHandPosition = msg.leftHandPosition,
                leftHandRotation = msg.leftHandRotation,

                rightHandPosition = msg.rightHandPosition,
                rightHandRotation = msg.rightHandRotation
            };

            player.HandleVRBodyUpdate(data);
            //Debug.Log(this.ToString() + " HandleVRBodyUpdate for player by net id == " + player.netId.ToString());
            MessagePool.Return(msg);
        }

        protected virtual void HandleSpawn(byte[] buffer)
        {
            var msg = MessagePool.Get<SpawnMessageResponse>(buffer);
            var player = GetPlayer(msg.playerNetId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleSpawn() no player found by net id == " + msg.playerNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            SpawnManagerPreviewable.instance.Spawn<INetSpawnable>(hand, msg.prefabType);
            MessagePool.Return(msg);
        }

        protected virtual void HandleSpawnEquip(byte[] buffer)
        {
            var msg = MessagePool.Get<SpawnEquipMessageResponse>(buffer);
            var player = GetPlayer(msg.playerNetId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleSpawnEquip() no player found by net id == " + msg.playerNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            var prefab = SpawnManagerPreviewable.instance.Spawn<INetSpawnableEquippable>(hand, msg.prefabType);
            if (prefab == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip received spawn equip message for non-INetSpawnableEquippable prefab of type == " + msg.prefabType.ToString());
                MessagePool.Return(msg);
                return;
            }

            AddNetEntity(prefab, msg.equippableNetId);
            hand.Equip(prefab);

            //Debug.Log(this.ToString() + " HandleSpawnEquip() spawned prefab == " + prefab.ToString() + ", and equipped on player == " + player.ToString());
            MessagePool.Return(msg);
        }

        protected virtual void HandleEquip(byte[] buffer)
        {
            var msg = MessagePool.Get<EquipMessage>(buffer);
            var player = GetPlayer<INetPlayer>(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleEquip no player found by net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var equippable = GetEntity<INetEquippable>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() player == " + player.ToString() + ", cannot equip unknown equippable by id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            hand.Equip(equippable);
            //Debug.Log(this.ToString() + " HandleEquip for net id == " + msg.netId.ToString() + ", type == " + equippable.prefabType.ToString() + ", is left == " + msg.isLeft.ToString());
            MessagePool.Return(msg);
        }

        protected virtual void HandleUnequip(byte[] buffer)
        {
            var msg = MessagePool.Get<UnequipMessage>(buffer);
            var player = GetPlayer<INetPlayer>(msg.netId);
            if (player == null)
            {
                Debug.LogError(this.ToString() + " HandleUnequip no player found through net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var equipped = GetEntity<INetEquippable>(msg.netEquippableId);
            if (equipped == null)
            {
                Debug.LogWarning(this.ToString() + " HandleUnequip() player == " + player.ToString() + ", cannot unequip unknown equippable by id == " + msg.netEquippableId.ToString());
                MessagePool.Return(msg);
                return;
            }

            equipped.position = msg.position;
            equipped.rotation = msg.rotation;
            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            hand.Unequip(equipped, msg.velocity, msg.angularVelocity);

            //Debug.Log(this.ToString() + " HandleUnequip for player by net id == " + msg.netId.ToString() + ", is left == " + msg.isLeft.ToString());
            MessagePool.Return(msg);
        }

        protected virtual void HandleApplyForceToPlayer(byte[] buffer)
        {
            var msg = MessagePool.Get<ApplyForceToPlayerMessage>(buffer);
            var playerVictim = GetPlayer(msg.netId);
            if (playerVictim == null)
            {
                Debug.LogWarning(this.ToString() + " HandleApplyForceToPlayer() could not find player by net id == " + msg.netId.ToString());
                MessagePool.Return(msg);
                return;
            }

            if (playerVictim.isLocal)
            {
                // tell (local player) victim to get hit
                ((IPlayer)playerVictim).AddForce(this, msg.force);
            }

            MessagePool.Return(msg);
        }
    }
}