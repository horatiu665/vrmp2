namespace VRCore.VRNetwork
{
    using UnityEngine;

    public class UnequipMessage : NetIdMessageBase
    {
        public UnequipMessage()
            : this(NetMessageType.Unequip)
        {
        }

        public UnequipMessage(NetMessageType messageType)
            : base(messageType)
        {
        }

        public UnequipMessage(byte[] buffer)
            : base(buffer)
        {
        }

        public int netEquippableId
        {
            get;
            set;
        }

        public bool isLeft
        {
            get;
            set;
        }

        public Vector3 position
        {
            get;
            set;
        }

        public Quaternion rotation
        {
            get;
            set;
        }

        public Vector3 velocity
        {
            get;
            set;
        }

        public Vector3 angularVelocity
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 31; // equippable net id (4) + is left (1) + position (6) + rotation (8) + velocity (6) + angular velocity (6)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.netEquippableId);
            s.Write(this.position);
            s.Write(this.rotation);
            s.Write(this.isLeft);
            s.Write(this.velocity);
            s.Write(this.angularVelocity);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.netEquippableId = s.ReadInt();
            this.position = s.ReadVector3();
            this.rotation = s.ReadQuaternion();
            this.isLeft = s.ReadBool();
            this.velocity = s.ReadVector3();
            this.angularVelocity = s.ReadVector3();
        }
    }
}