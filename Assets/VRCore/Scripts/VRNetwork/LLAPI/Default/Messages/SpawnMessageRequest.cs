namespace VRCore.VRNetwork
{
    public class SpawnMessageRequest : PrefabTypeMessageBase
    {
        public SpawnMessageRequest(byte[] buffer) : base(buffer)
        {
        }

        public SpawnMessageRequest() : base(NetMessageType.SpawnRequest)
        {
        }

        public SpawnMessageRequest(NetMessageType type) : base(type)
        {
        }
        
        public bool isLeft
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 1; // is left (1)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.isLeft);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.isLeft = s.ReadBool();
        }
    }
}