namespace VRCore.VRNetwork
{
    public class SpawnMessageResponse : PrefabTypeMessageBase
    {
        public SpawnMessageResponse()
            : this(NetMessageType.SpawnResponse)
        {
        }

        public SpawnMessageResponse(NetMessageType type)
            : base(type)
        {
        }

        public SpawnMessageResponse(byte[] buffer)
            : base(buffer)
        {
        }

        public short playerNetId
        {
            get;
            set;
        }

        public bool isLeft
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 3; // player net id (2) + is left (1)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.playerNetId);
            s.Write(this.isLeft);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.playerNetId = s.ReadShort();
            this.isLeft = s.ReadBool();
        }
    }
}