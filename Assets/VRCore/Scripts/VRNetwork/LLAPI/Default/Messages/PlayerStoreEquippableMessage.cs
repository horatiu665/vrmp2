namespace VRCore.VRNetwork
{
    public sealed class PlayerStoreEquippableMessage : NetIdMessageBase
    {
        public PlayerStoreEquippableMessage() : base(NetMessageType.PlayerStorage)
        {
        }

        public PlayerStoreEquippableMessage(byte[] buffer) : base(buffer)
        {
        }

        public int equippableNetId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 4;
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.equippableNetId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.equippableNetId = s.ReadInt();
        }
    }
}