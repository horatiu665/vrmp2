namespace VRCore.VRNetwork
{
    public class SpawnEquipMessageRequest : SpawnMessageRequest
    {
        public SpawnEquipMessageRequest() : base(NetMessageType.SpawnEquipRequest)
        {
        }

        public SpawnEquipMessageRequest(byte[] buffer) : base(buffer)
        {
        }
    }
}