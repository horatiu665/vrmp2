namespace VRCore.VRNetwork
{
    using UnityEngine;

    public class ApplyForceToPlayerMessage : NetIdMessageBase
    {
        public ApplyForceToPlayerMessage() : base(NetMessageType.ApplyForceToPlayer)
        {
        }

        public ApplyForceToPlayerMessage(byte[] buffer) : base(buffer)
        {
        }

        public Vector3 force
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            // half-precision position (6 bytes = (3 * 4) / 2)
            return base.GetByteSize() + 6;
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.force);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            force = s.ReadVector3();
        }
    }
}