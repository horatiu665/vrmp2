namespace VRCore.VRNetwork
{
    public class EquipMessage : NetIdMessageBase
    {
        public EquipMessage(byte[] buffer)
            : base(buffer)
        {
        }

        public EquipMessage()
            : base(NetMessageType.Equip)
        {
        }

        public int equippableNetId
        {
            get;
            set;
        }

        public bool isLeft
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 5; // is left (1) + equippable net id (4)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.isLeft);
            s.Write(this.equippableNetId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.isLeft = s.ReadBool();
            this.equippableNetId = s.ReadInt();
        }
    }
}