namespace VRCore.VRNetwork
{
    public class VRBodyUpdateMessage : VRBodyInputMessage
    {
        public VRBodyUpdateMessage()
            : this(NetMessageType.VRBodyUpdate)
        {
        }

        public VRBodyUpdateMessage(NetMessageType messageType)
            : base(messageType)
        {
        }

        public VRBodyUpdateMessage(byte[] buffer)
            : base(buffer)
        {
        }

        public short netId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 2; // player net id (2)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.netId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.netId = s.ReadShort();
        }
    }
}