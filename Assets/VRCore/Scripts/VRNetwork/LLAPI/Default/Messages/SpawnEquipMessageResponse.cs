namespace VRCore.VRNetwork
{
    public class SpawnEquipMessageResponse : SpawnMessageResponse
    {
        public SpawnEquipMessageResponse()
            : this(NetMessageType.SpawnEquipResponse)
        {
        }

        public SpawnEquipMessageResponse(NetMessageType messageType)
            : base(messageType)
        {
        }

        public SpawnEquipMessageResponse(byte[] buffer) : base(buffer)
        {
        }

        public int equippableNetId
        {
            get;
            set;
        }

        protected override int GetByteSize()
        {
            return base.GetByteSize() + 4; // equippable net id (4)
        }

        protected override void Serialize(NetSerializer s)
        {
            base.Serialize(s);
            s.Write(this.equippableNetId);
        }

        protected override void Deserialize(NetDeserializer s)
        {
            base.Deserialize(s);
            this.equippableNetId = s.ReadInt();
        }
    }
}