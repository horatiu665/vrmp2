namespace VRCore.VRNetwork
{
    public class GenericSpawnableEquippable : NetEquippableSpawnableBase
    {
        private void Reset()
        {
            _prefabType = VRPrefabType.GenericEquippable;
            _mixWithPlayerBehaviours = true;
        }
    }
}