namespace VRCore.VRNetwork.Server
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class ServerNetReceiverDefault : ServerNetReceiverBase<ServerNetReceiverDefault>
    {
        protected NetStartPosition[] _startPositions;

        protected override void Awake()
        {
            base.Awake();
            AddAllNetEntities<GenericEquippable>();
        }

        protected virtual void OnEnable()
        {
            _startPositions = FindObjectsOfType<NetStartPosition>();
            if (_startPositions.Length == 0)
            {
                Debug.LogError(this.ToString() + " cannot operate without at least one NetStartPosition in the scene!");
            }
        }

        public override void OnConnect(int connectionId, NetworkError error)
        {
            Debug.Log(this.ToString() + " OnConnect. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString());
        }

        public override void OnData(int connectionId, byte[] buffer, NetworkError error)
        {
            if (error != NetworkError.Ok)
            {
                Debug.LogWarning(this.ToString() + " OnData from connection ID == " + connectionId.ToString() + ", buffer size == " + buffer.Length.ToString() + ", encountered NetworkError == " + error.ToString());
                return;
            }

            var messageType = buffer.PeekType();
            ////Debug.Log(this.ToString() + " OnData. Connection Id == " + connectionId.ToString() + ", error == " + error.ToString() + ", message type == " + messageType.ToString() + ", buffer size == " + buffer.Length.ToString() + ", contents:\n" + buffer.DebugLogContents());

            switch (messageType)
            {
                case NetMessageType.LocalConnect:
                {
                    HandlePlayerLocalConnect(connectionId, buffer);
                    break;
                }

                case NetMessageType.VRBodyInput:
                {
                    HandlePlayerVRBodyInput(connectionId, buffer);
                    break;
                }

                case NetMessageType.SpawnRequest:
                {
                    HandleSpawn(connectionId, buffer);
                    break;
                }

                case NetMessageType.SpawnEquipRequest:
                {
                    HandleSpawnEquip(connectionId, buffer);
                    break;
                }

                case NetMessageType.Equip:
                {
                    HandleEquip(connectionId, buffer);
                    break;
                }

                case NetMessageType.Unequip:
                {
                    HandleUnequip(connectionId, buffer);
                    break;
                }

                case NetMessageType.ApplyForceToPlayer:
                {
                    HandleApplyForceToPlayer(connectionId, buffer);
                    break;
                }

                default:
                {
                    Debug.LogError(this.ToString() + " OnData unhandled MessageType == " + messageType.ToString());
                    break;
                }
            }
        }

        public override void OnDisconnect(int connectionId, NetworkError error)
        {
            var player = GetPlayer<INetPlayer>(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " OnDisconnect received disconnect from unknown player with connection ID == " + connectionId.ToString());
                return;
            }

            Debug.Log(this.ToString() + " OnDisconnect. Removing player (" + player.ToString() + ") by net id == " + player.netId.ToString() + ". Connection  Id == " + connectionId.ToString() + ", error == " + error.ToString());

            // Remove disconnected players - before sending out message, to avoid sending to the leaver
            var netId = player.netId;
            RemovePlayer(player);
            PlayerTypeManager.instance.Return(player);

            // inform all other clients of the leaver, except the one actually leaving (since he is already disconnected)
            var msg = MessagePool.Get<PlayerDisconnectMessageDefault>(netId);
            _network.SendToAll(msg, QosType.Reliable);
            MessagePool.Return(msg);
        }

        protected virtual void HandlePlayerLocalConnect(int connectionId, byte[] buffer)
        {
            var nameMessage = MessagePool.Get<PlayerLocalConnectMessageDefault>(buffer);
            var playerType = nameMessage.playerType;
            MessagePool.Return(nameMessage);

            var player = GetPlayer<INetPlayer>(connectionId);
            if (player != null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerLocalConnect another player is already registered with connection id == " + connectionId.ToString());
                return;
            }

            if (string.IsNullOrEmpty(name))
            {
                name = "Random Name"; // TODO: what to do about players with no name? (can it even happen?)
            }

            var netId = GetNextPlayerId();
            var pos = _startPositions[netId % _startPositions.Length].transform.position;

            // a new client has connected - inform all other clients of the new player - do this before adding the new player to avoid sending the connect message to the new player
            var connectMsg = MessagePool.Get<PlayerRemoteConnectMessageDefault>(netId);
            connectMsg.position = pos;
            connectMsg.originShift = NetOriginShiftManager.instance.GetPlayerOriginShift(netId);
            connectMsg.playerType = playerType;
            _network.SendToAll(connectMsg, QosType.Reliable);

            // Inform the new player of all the existing client players (if there are any)
            if (_players.Count > 0)
            {
                var enumerator = _players.GetEnumerator();
                try
                {
                    while (enumerator.MoveNext())
                    {
                        // send to the new connecting player, but send a connect message contaning details of all the other existing clients
                        var p = enumerator.Current.Value;
                        connectMsg.netId = p.netId;
                        connectMsg.position = p.position;
                        connectMsg.originShift = NetOriginShiftManager.instance.GetPlayerOriginShift(netId);
                        connectMsg.playerType = p.playerType;
                        _network.Send(connectionId, connectMsg, QosType.Reliable);
                    }
                }
                finally
                {
                    enumerator.Dispose();
                }
            }

            MessagePool.Return(connectMsg);

            // send a special 'start message' to the new player so that the local player can be set up
            var startMsg = MessagePool.Get<PlayerLocalStartMessageDefault>(netId);
            startMsg.position = pos;
            startMsg.playerType = playerType;
            _network.Send(connectionId, startMsg, QosType.Reliable);
            MessagePool.Return(startMsg);

            // actually create and add the new player
            var newPlayer = PlayerTypeManager.instance.InstantiatePlayer<INetPlayer>(playerType, GameType.Server, pos);
            AddPlayer(newPlayer, connectionId, netId);
            Debug.Log(this.ToString() + " HandlePlayerName() - added new player by net id == " + netId.ToString() + " and name == " + name + " for connection id == " + connectionId.ToString());
        }

        protected virtual void HandlePlayerVRBodyInput(int connectionId, byte[] buffer)
        {
            var player = GetPlayer<INetPlayer>(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandlePlayerVRBodyInput() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            // get the incoming message and update the head and hands of the correct player
            var msg = MessagePool.Get<VRBodyInputMessage>(buffer);
            var data = new VRBodyUpdateData()
            {
                position = msg.position,

                headPosition = msg.headPosition,
                headRotation = msg.headRotation,

                leftHandPosition = msg.leftHandPosition,
                leftHandRotation = msg.leftHandRotation,

                rightHandPosition = msg.rightHandPosition,
                rightHandRotation = msg.rightHandRotation
            };

            player.HandleVRBodyUpdate(data);
            MessagePool.Return(msg);
        }

        protected virtual void HandleSpawn(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawn() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var requestMsg = MessagePool.Get<SpawnMessageRequest>(buffer);
            var hand = requestMsg.isLeft ? player.leftHand : player.rightHand;
            SpawnManagerPreviewable.instance.Spawn<INetSpawnable>(hand, requestMsg.prefabType);

            var responseMsg = MessagePool.Get<SpawnMessageResponse>();
            responseMsg.playerNetId = player.netId;
            responseMsg.isLeft = requestMsg.isLeft;
            responseMsg.prefabType = requestMsg.prefabType;
            _network.SendToAll(responseMsg, QosType.ReliableSequenced);

            //Debug.Log(this.ToString() + " HandleSpawn() spawned new prefab of type == " + prefab.prefabType.ToString());
            MessagePool.Return(requestMsg);
            MessagePool.Return(responseMsg);
        }

        protected virtual void HandleSpawnEquip(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var requestMsg = MessagePool.Get<SpawnEquipMessageRequest>(buffer);
            var hand = requestMsg.isLeft ? player.leftHand : player.rightHand;
            var prefab = SpawnManagerPreviewable.instance.Spawn<INetSpawnableEquippable>(hand, requestMsg.prefabType);
            if (prefab == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip() spawned prefab for player == " + player.ToString() + ", is null. Prefab type == " + requestMsg.prefabType.ToString());
                MessagePool.Return(requestMsg);
                return;
            }

            if (hand.Equip(prefab) == null)
            {
                Debug.LogWarning(this.ToString() + " HandleSpawnEquip() failed to equip == " + prefab.ToString() + " on player == " + player.ToString());
                VRPrefabManager.instance.Return(prefab);
                MessagePool.Return(requestMsg);
                return;
            }

            var serverNetId = GetNextEntityId();
            AddNetEntity(prefab, serverNetId);

            var responseMsg = MessagePool.Get<SpawnEquipMessageResponse>();
            responseMsg.playerNetId = player.netId;
            responseMsg.isLeft = requestMsg.isLeft;
            responseMsg.prefabType = requestMsg.prefabType;
            responseMsg.equippableNetId = serverNetId;
            _network.SendToAll(responseMsg, QosType.ReliableSequenced);

            //Debug.Log(this.ToString() + " HandleSpawnEquip() spawned prefab == " + prefab.ToString() + " and equipped to player == " + player.ToString());
            MessagePool.Return(requestMsg);
            MessagePool.Return(responseMsg);
        }

        protected virtual void HandleEquip(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<EquipMessage>(buffer);
            var equippable = GetEntity<INetEquippable>(msg.equippableNetId);
            if (equippable == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() player == " + player.ToString() + ", cannot equip unknown equippable by id == " + msg.equippableNetId.ToString());
                MessagePool.Return(msg);
                return;
            }

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            if (hand.Equip(equippable) == null)
            {
                Debug.LogWarning(this.ToString() + " HandleEquip() failed to equip == " + equippable.ToString() + " on player == " + player.ToString());
                MessagePool.Return(msg);
                return;
            }

            msg.netId = player.netId;
            _network.SendToAll(msg, QosType.ReliableSequenced, player.netId);

            //Debug.Log(this.ToString() + " HandleEquip() player == " + player.ToString() + ", equipped == " + equippable.ToString());
            MessagePool.Return(msg);
        }

        protected virtual void HandleUnequip(int connectionId, byte[] buffer)
        {
            var player = GetPlayer(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleUnequip() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<UnequipMessage>(buffer);
            var equipped = GetEntity<INetEquippable>(msg.netEquippableId);
            if (equipped == null)
            {
                Debug.LogWarning(this.ToString() + " HandleUnequip() player == " + player.ToString() + ", cannot unequip unknown equippable by id == " + msg.netEquippableId.ToString());
                MessagePool.Return(msg);
                return;
            }

            equipped.position = msg.position;
            equipped.rotation = msg.rotation;

            var hand = msg.isLeft ? player.leftHand : player.rightHand;
            hand.Unequip(equipped, msg.velocity, msg.angularVelocity);

            msg.netId = player.netId;
            _network.SendToAll(msg, QosType.ReliableSequenced, player.netId);

            //Debug.Log(this.ToString() + " HandleUnequip() player == " + player.ToString());
            MessagePool.Return(msg);
        }

        protected virtual void HandleApplyForceToPlayer(int connectionId, byte[] buffer)
        {
            var player = GetPlayer<INetPlayer>(connectionId);
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " HandleApplyForceToPlayer() could not find player by connection id == " + connectionId.ToString());
                return;
            }

            var msg = MessagePool.Get<ApplyForceToPlayerMessage>(buffer);

            // this is the id of the player that should get hit
            var playerVictim = GetPlayer(GetConnectionId(msg.netId));
            if (playerVictim != null)
            {
                // apply force to the victim (not on the server, it would do nothing. it has to be done on the playerVictim) => tell victim to get hit
                _network.Send(playerVictim.netId, msg, QosType.ReliableSequenced);
            }

            MessagePool.Return(msg);
        }
    }
}