namespace VRCore.VRNetwork
{
    public class GenericEquippable : NetEquippableBase
    {
        private void Reset()
        {
            _prefabType = VRPrefabType.GenericEquippable;
            _mixWithPlayerBehaviours = true;
        }
    }
}