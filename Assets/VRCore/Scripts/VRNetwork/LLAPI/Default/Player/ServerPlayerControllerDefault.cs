namespace VRCore.VRNetwork
{
    using Server;
    using UnityEngine;
    using UnityEngine.Networking;
    using VRCore;

    /// <summary>
    /// Component for server to broadcast updates concerning this player.
    /// </summary>
    /// <seealso cref="VRNetwork.NetBehaviourBase" />
    /// <seealso cref="VRCore.IVRPlayerStartListener" />
    [Apex.ApexComponent("Network")]
    [RequireComponent(typeof(INetPlayer))]
    public class ServerPlayerControllerDefault : NetBehaviourBase, IVRPlayerStartListener
    {
        private readonly VRBodyUpdateMessage _msg = new VRBodyUpdateMessage();

        protected ServerNetSender _network;
        protected INetPlayer _player;

        protected override void OnEnable()
        {
            base.OnEnable();
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<INetPlayer>());
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _network = ServerNetSender.instance;
            _player = (INetPlayer)player;

            if (_network == null)
            {
                this.enabled = false;
                Debug.LogWarning(this.ToString() + " cannot function without a ServerNetReceiver in the scene. Disabling automatically.");
            }
        }

        protected override void OnSend()
        {
            if (_player == null)
            {
                return;
            }

            if (_msg.position == _player.position &&
                _msg.headPosition == _player.head.localPosition &&
                _msg.leftHandPosition == _player.leftHand.localPosition &&
                _msg.rightHandPosition == _player.rightHand.localPosition)
            {
                // There are no positional changes, no need to update
                return;
            }

            _msg.netId = _player.netId;
            _msg.position = _player.position;

            _msg.headPosition = _player.head.localPosition;
            _msg.headRotation = _player.head.localRotation;

            _msg.leftHandPosition = _player.leftHand.localPosition;
            _msg.leftHandRotation = _player.leftHand.localRotation;

            _msg.rightHandPosition = _player.rightHand.localPosition;
            _msg.rightHandRotation = _player.rightHand.localRotation;

            // send out to all other players, than the one for whom this concerns - since he has authority over his own position & rotation state
            _network.SendToAll(_msg, QosType.UnreliableSequenced, _player.netId);
        }
    }
}