namespace VRCore.VRNetwork
{
    using UnityEngine;

    /// <summary>
    /// For singleplayer use only, to "fake" that the single player is local (has authority).
    /// </summary>
    /// <seealso cref="UnityEngine.MonoBehaviour" />
    /// <seealso cref="VRCore.IVRPlayerStartListener" />
    [Apex.ApexComponent("Player")]
    [RequireComponent(typeof(INetPlayer))]
    public class SinglePlayerControllerDefault : SingletonMonoBehaviour<SinglePlayerControllerDefault>, IVRPlayerStartListener
    {
        protected INetPlayer _player;

        public INetPlayer player
        {
            get { return _player; }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = (INetPlayer)player;
            _player.SetIsLocal();
        }

        protected virtual void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<INetPlayer>());
            }
        }
    }
}