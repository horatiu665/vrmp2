namespace VRCore.VRNetwork
{
    using Client;
    using UnityEngine;
    using UnityEngine.Networking;

    /// <summary>
    /// Component for local players, which sends updates of positions and rotations to server, which in turn broadcasts to remotes.
    /// </summary>
    /// <seealso cref="VRNetwork.NetBehaviourBase" />
    /// <seealso cref="VRCore.IVRPlayerStartListener" />
    [Apex.ApexComponent("Network")]
    [RequireComponent(typeof(INetPlayer))]
    public class LocalPlayerControllerDefault : SingletonMonoBehaviour<LocalPlayerControllerDefault>, IVRPlayerStartListener
    {
        [SerializeField, Range(0.1f, 30f), Tooltip("How many times per second this component may send updates over the network.")]
        private float _sendRate = 10f;

        private readonly VRBodyInputMessage _msg = new VRBodyInputMessage();
        protected ClientNetSender _network;
        protected INetPlayer _player;
        private float _lastSend;

        public INetPlayer player
        {
            get { return _player; }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _network = ClientNetSender.instance;
            _player = (INetPlayer)player;
        }

        protected virtual void OnEnable()
        {
            // wait 'one update' before starting to send (to ensure proper initialization has had time)
            _lastSend = Time.timeSinceLevelLoad + (1f / _sendRate);

            if (_player == null)
            {
                OnPlayerStart(this.GetComponent<INetPlayer>());
            }
        }

        protected virtual void Update()
        {
            if (_player == null)
            {
                return;
            }

            var time = Time.timeSinceLevelLoad;
            if (time < _lastSend)
            {
                return;
            }

            _lastSend = time + (1f / _sendRate);

            if (_msg.position == _player.position &&
                _msg.headPosition == _player.head.localPosition &&
                _msg.leftHandPosition == _player.leftHand.localPosition &&
                _msg.rightHandPosition == _player.rightHand.localPosition)
            {
                // There are no positional changes, no need to update
                return;
            }

            _msg.position = _player.position;

            _msg.headPosition = _player.head.localPosition;
            _msg.headRotation = _player.head.localRotation;

            _msg.leftHandPosition = _player.leftHand.localPosition;
            _msg.leftHandRotation = _player.leftHand.localRotation;

            _msg.rightHandPosition = _player.rightHand.localPosition;
            _msg.rightHandRotation = _player.rightHand.localRotation;

            _network.Send(_msg, QosType.UnreliableSequenced);
        }
    }
}