namespace VRCore.VRNetwork
{
    using System.Collections.Generic;
    using Client;
    using UnityEngine;
    using UnityEngine.Networking;

    [Apex.ApexComponent("Network")]
    [RequireComponent(typeof(INetPlayer))]
    public class NetInputControllerDefault : InputControllerDefault
    {
        [Header("Extrapolation throw params")]
        [Range(0, 2)]
        public int extrapolationFrames = 0;


        public delegate void UnequipMessageDelegate(INetEquippable equippable, UnequipMessage message);

        public static event UnequipMessageDelegate OnUnequipMessageHook;

        protected override void HandleStorage(IVREquippable equippable, VRInput input, bool isLeft)
        {
            ((SpawnManagerPreviewable)SpawnManagerPreviewable.instance).AdjustSpawnCount(isLeft, equippable.prefabType, 1);

            var msg = MessagePool.Get<PlayerStoreEquippableMessage>(((INetPlayer)_player).netId);
            msg.equippableNetId = ((INetEquippable)equippable).netId;
            ClientNetSender.instance.Send(msg, QosType.ReliableSequenced);
            MessagePool.Return(msg);
        }

        protected override void HandleSpawning(IList<SpawnSetup> mapping, VRInput input, bool isLeft)
        {
            var prefabManager = VRPrefabManager.instance;
            var spawnManager = (SpawnManagerPreviewable)SpawnManagerPreviewable.instance;

            var count = mapping.Count;
            for (int i = 0; i < count; i++)
            {
                var spawnable = prefabManager.GetPrefab<INetSpawnable>(mapping[i].prefabType);
                if (spawnable == null)
                {
                    continue;
                }

                // check for inventory count
                if (!spawnManager.CanSpawn(isLeft, spawnable.prefabType))
                {
                    //Debug.Log(this.ToString() + " disallowing spawning of a new spawnable of type == " + spawnable.prefabType.ToString() + ", because the spawn count is zero");
                    continue;
                }

                var equippable = spawnable as INetSpawnableEquippable;
                if (equippable != null)
                {
                    // if the spawnable can be equipped, spawn and equip it
                    SpawnAndEquip(equippable, isLeft);
                }
                else
                {
                    // if the spawnable cannot be equipped, just spawn it
                    Spawn(spawnable, isLeft);
                }

                spawnManager.AdjustSpawnCount(isLeft, spawnable.prefabType, -1);
            }
        }

        public override bool Spawn(IVRSpawnable spawnable, bool isLeft)
        {
            var netSpawnable = spawnable as INetSpawnable;
            if (netSpawnable == null)
            {
                Debug.LogError(this.ToString() + " Spawn() cannot spawn == " + spawnable.ToString() + ", because it is not an INetSpawnable");
                return false;
            }

            var msg = MessagePool.Get<SpawnMessageRequest>();
            msg.prefabType = netSpawnable.prefabType;
            msg.isLeft = isLeft;
            ClientNetSender.instance.Send(msg, QosType.UnreliableSequenced);
            //Debug.Log(this.ToString() + " Spawn() send message for spawning prefab type == " + msg.prefabType.ToString());
            MessagePool.Return(msg);

            return true;
        }

        public override bool SpawnAndEquip(IVRSpawnableEquippable equippable, bool isLeft)
        {
            var netEquippable = equippable as INetSpawnableEquippable;
            if (netEquippable == null)
            {
                Debug.LogError(this.ToString() + " SpawnAndEquip() cannot spawn == " + equippable.ToString() + ", because it is not an INetSpawnableEquippable");
                return false;
            }

            var msg = MessagePool.Get<SpawnEquipMessageRequest>();
            msg.prefabType = netEquippable.prefabType;
            msg.isLeft = isLeft;
            ClientNetSender.instance.Send(msg, QosType.UnreliableSequenced);
            //Debug.Log(this.ToString() + " SpawnAndEquip() send message for spawning and equipping prefab type == " + msg.prefabType.ToString());
            MessagePool.Return(msg);

            return true;
        }

        public override bool Equip(IVREquippable equippable, bool isLeft)
        {
            if (!base.Equip(equippable, isLeft))
            {
                return false;
            }

            var netEquippable = equippable as INetEquippable;
            if (netEquippable == null)
            {
                Debug.LogError(this.ToString() + " Equip() cannot equip == " + equippable.ToString() + ", because it is not an INetEquippable");
                return false;
            }

            var msg = MessagePool.Get<EquipMessage>(((INetPlayer)_player).netId);
            msg.isLeft = isLeft;
            msg.equippableNetId = netEquippable.netId;
            ClientNetSender.instance.Send(msg, QosType.ReliableSequenced);
            //Debug.Log(this.ToString() + " Equip() type == " + equippable.prefabType.ToString() + ", isLeft == " + isLeft.ToString() + ", equippable net ID == " + msg.equippableNetId.ToString());
            MessagePool.Return(msg);

            return true;
        }

        public override bool Unequip(IVREquippable equippable, bool isLeft)
        {
            if (!base.Unequip(equippable, isLeft))
            {
                return false;
            }

            var netEquippable = equippable as INetEquippable;
            if (netEquippable == null)
            {
                Debug.LogError(this.ToString() + " Unequip() cannot unequip == " + equippable.ToString() + ", because it is not an INetEquippable");
                return false;
            }

            var haveVelocities = equippable as IHaveThrowMultipliers;
            var velocityMultiplier = (haveVelocities != null ? haveVelocities.velocityMultiplier : _config.velocityMultiplier);
            var angularVelocityMultiplier = (haveVelocities != null ? haveVelocities.angularVelocityMultiplier : _config.angularVelocityMultiplier);

            var controller = isLeft ? this.leftController : this.rightController;
            var msg = MessagePool.Get<UnequipMessage>(((INetPlayer)_player).netId);
            msg.isLeft = isLeft;
            msg.netEquippableId = netEquippable.netId;

            msg.position = netEquippable.position;
            var vel = _player.velocity + (controller.velocity * velocityMultiplier);
            if (extrapolationFrames > 0)
            {
                var serverOffsetDistance = GhettoTimeSync.instance.serverLag * extrapolationFrames * vel.magnitude;

                // if nothing is in the way, start the throw on the server with some offset 
                // so that when it switches to server sync position, it will match the values simulated here.
                RaycastHit hitInfo;
                if (!Physics.Raycast(netEquippable.position, vel, out hitInfo, serverOffsetDistance))
                {
                    msg.position += vel * GhettoTimeSync.instance.serverLag * extrapolationFrames;
                }
                else
                {
                    // if found rb is actually just the throwed object, do the same thing.
                    if (hitInfo.collider.attachedRigidbody != null)
                    {
                        if (hitInfo.collider.attachedRigidbody == netEquippable.rigidbody)
                        {
                            msg.position += vel * GhettoTimeSync.instance.serverLag * extrapolationFrames;
                        }
                    }
                }
            }
            msg.rotation = netEquippable.rotation;
            msg.velocity = vel;
            msg.angularVelocity = controller.angularVelocity * angularVelocityMultiplier;

            if (OnUnequipMessageHook != null)
            {
                OnUnequipMessageHook(netEquippable, msg);
            }

            ClientNetSender.instance.Send(msg, QosType.ReliableSequenced);
            //Debug.Log(this.ToString() + " Unequip() isLeft == " + isLeft.ToString());
            MessagePool.Return(msg);

            return true;
        }
    }
}