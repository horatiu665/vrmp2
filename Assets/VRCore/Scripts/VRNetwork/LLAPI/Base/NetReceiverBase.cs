namespace VRCore.VRNetwork
{
    using System.Collections.Generic;
    using UnityEngine;
    using UnityEngine.Networking;
    using VRCore;

    public abstract class NetReceiverBase<TSelf, T> : SingletonMonoBehaviour<TSelf>, INetReceiver<T> where T : NetSenderBase<T> where TSelf : NetReceiverBase<TSelf, T>
    {
        [SerializeField, Range(0, 100)]
        protected int _playerPreallocation = 4;

        [SerializeField, Range(0, short.MaxValue)]
        protected int _netEntitiesPreallocation = 200;

        protected IDictionary<short, INetPlayer> _players;
        protected IDictionary<int, INetEntity> _netEntities;
        protected T _network;

        /// <summary>
        /// Gets all the players collected in a dictionary with their netIds as keys.
        /// </summary>
        /// <value>
        /// The players.
        /// </value>
        public IDictionary<short, INetPlayer> players
        {
            get { return _players; }
        }

        public IDictionary<int, INetEntity> netEntities
        {
            get { return _netEntities; }
        }

        /// <summary>
        /// Called by Unity when enabled.
        /// </summary>
        protected override void Awake()
        {
            base.Awake();
            _players = new Dictionary<short, INetPlayer>(_playerPreallocation);
            _netEntities = new Dictionary<int, INetEntity>(_netEntitiesPreallocation);
        }

        /// <summary>
        /// Called when the network is initialized. Passes a reference of the <see cref="T:VRNetwork.NetworkBase`1" /> instance.
        /// </summary>
        /// <param name="network">The network.</param>
        public virtual void OnInitialized(T network)
        {
            _network = network;
        }

        /// <summary>
        /// Finds and stores a reference to all entities of the designated type, so that they can be equipped with network support.
        /// </summary>
        /// <typeparam name="TEntity">The type of the entity.</typeparam>
        public void AddAllNetEntities<TEntity>() where TEntity : MonoBehaviour, INetEquippable
        {
            var objects = FindObjectsOfType<TEntity>();
            for (int i = 0; i < objects.Length; i++)
            {
                var obj = objects[i];
                if (obj == null)
                {
                    continue;
                }

                AddNetEntity(obj, obj.netId);
            }

            Debug.Log(this.ToString() + " stored " + objects.Length + " equippables (" + typeof(TEntity).ToString() + ") in lookup table for networked equipping.");
        }

        public void AddNetEntity(INetEntity entity, int netId)
        {
            if (!_netEntities.ContainsKey(netId))
            {
                if (entity.netId != netId)
                {
                    entity.SetNetId(netId);
                }

                _netEntities.Add(netId, entity);
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            else
            {
                Debug.LogWarning(this.ToString() + " trying to register an entity (" + entity.ToString() + ") whose Net ID is already registered, net id == " + netId.ToString());
            }
#endif
        }

        public INetEntity GetEntity(int netId)
        {
            //Debug.LogWarning("Trying to get entity by id " + netId + " and contains is " + _netEntities.ContainsKey(netId));
            return _netEntities.GetValueOrDefault(netId);
        }

        public TEntity GetEntity<TEntity>(int netID) where TEntity : class, INetEntity
        {
            return GetEntity(netID) as TEntity;
        }

        public virtual bool RemoveNetEntity(INetEntity entity)
        {
            return RemoveNetEntity(entity.netId);
        }

        public virtual bool RemoveNetEntity(int netId)
        {
            return _netEntities.Remove(netId);
        }

        /// <summary>
        /// Adds the given player to a dictionary using the player's netId as the key.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <param name="netId">The net identifier to set on the player.</param>
        /// <seealso cref="GetPlayer(short)"/>
        /// <seealso cref="RemovePlayer(short)"/>
        protected void AddPlayer(INetPlayer player, short netId)
        {
            player.SetNetId(netId);
            _players.Add(netId, player);
        }

        /// <summary>
        /// Gets the player going by the specified netId.
        /// </summary>
        /// <param name="netId">The net identifier.</param>
        /// <returns></returns>
        /// <seealso cref="AddPlayer(INetPlayer)"/>
        /// <seealso cref="RemovePlayer(short)"/>
        protected INetPlayer GetPlayer(short netId)
        {
            return _players.GetValueOrDefault(netId);
        }

        /// <summary>
        /// Gets the player going by the specified netId cast to the given type.
        /// </summary>
        /// <typeparam name="TPlayer"></typeparam>
        /// <param name="netId">The net identifier.</param>
        /// <returns></returns>
        protected TPlayer GetPlayer<TPlayer>(short netId) where TPlayer : class, INetPlayer
        {
            return GetPlayer(netId) as TPlayer;
        }

        /// <summary>
        /// Removes the given player from the players' dictionary.
        /// </summary>
        /// <param name="player">The player.</param>
        /// <returns></returns>
        /// <seealso cref="AddPlayer(INetPlayer)"/>
        /// <seealso cref="GetPlayer(short)"/>
        protected virtual bool RemovePlayer(INetPlayer player)
        {
            return RemovePlayer(player.netId);
        }

        /// <summary>
        /// Removes the player by the given netId from the players' dictionary.
        /// </summary>
        /// <param name="netId">The net identifier.</param>
        /// <returns></returns>
        /// <seealso cref="AddPlayer(INetPlayer)"/>
        /// <seealso cref="GetPlayer(short)"/>
        protected virtual bool RemovePlayer(short netId)
        {
            return _players.Remove(netId);
        }

        public abstract int GetConnectionId(short netId);

        /// <summary>
        /// Called when a new connection is established.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="error">The error.</param>
        public abstract void OnConnect(int connectionId, NetworkError error);

        /// <summary>
        /// Called when a new data message is received.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="buffer">The buffer.</param>
        /// <param name="error">The error.</param>
        public abstract void OnData(int connectionId, byte[] buffer, NetworkError error);

        /// <summary>
        /// Called when a connection is disconnected.
        /// </summary>
        /// <param name="connectionId">The connection identifier.</param>
        /// <param name="error">The error.</param>
        public abstract void OnDisconnect(int connectionId, NetworkError error);
    }
}