﻿namespace VRCore.VRNetwork.HLAPI
{
    using System.Collections;
    using UnityEngine;
    using UnityEngine.Networking;

    public sealed class VRNetPlayerWrapper : NetworkBehaviour
    {
        [SerializeField]
        private GameObject _localPlayer = null;

        [SerializeField]
        private GameObject _remotePlayer = null;

        [SerializeField, Range(0.1f, 10f)]
        private float _startDelay = 0.25f;

        private void Awake()
        {
            if (_localPlayer == null)
            {
                Debug.LogError(this.ToString() + " is missing a reference to the LocalPlayer");
            }

            if (_remotePlayer == null)
            {
                Debug.LogError(this.ToString() + " is missing a reference to the RemotePlayer");
            }

            StartCoroutine(DelayedStart());
        }

        private IEnumerator DelayedStart()
        {
            yield return new WaitForSeconds(_startDelay);

            if (this.isLocalPlayer)
            {
                _localPlayer.SetActive(true);
            }
            else
            {
                _remotePlayer.SetActive(true);
            }
        }
    }
}