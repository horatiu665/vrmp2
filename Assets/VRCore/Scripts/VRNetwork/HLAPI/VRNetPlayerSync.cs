﻿namespace VRCore.VRNetwork.HLAPI
{
    using UnityEngine;
    using UnityEngine.Networking;

    public class VRNetPlayerSync : NetworkBehaviour, IVRPlayerStartListener
    {
        [SerializeField, Range(0.1f, 30f), Tooltip("How many times per second this component sends an update through RPCs.")]
        public float _sendRate = 20f;

        [Header("Player Thresholds")]
        [SerializeField, Range(0.1f, 10f)]
        private float _positionThreshold = 0.5f;

        [SerializeField, Range(0.1f, 10f)]
        private float _rotationThreshold = 2f;

        [SerializeField, Range(0.1f, 100f)]
        private float _playerMaxSpeed = 10f;

        [SerializeField, Range(0.1f, 100f)]
        private float _playerMaxTorque = 20f;

        [Header("Head Thresholds")]
        [SerializeField, Range(0.01f, 10f)]
        private float _headPositionThreshold = 0.25f;

        [SerializeField, Range(0.01f, 10f)]
        private float _headRotationThreshold = 1f;

        [SerializeField, Range(0.1f, 100f)]
        private float _headMaxSpeed = 15f;

        [SerializeField, Range(0.1f, 100f)]
        private float _headMaxTorque = 30f;

        [Header("Hands' Thresholds")]
        [SerializeField, Range(0.001f, 10f)]
        private float _handsPositionThreshold = 0.1f;

        [SerializeField, Range(0.001f, 10f)]
        private float _handsRotationThreshold = 1f;

        [SerializeField, Range(0.1f, 100f)]
        private float _handsMaxSpeed = 20f;

        [SerializeField, Range(0.1f, 100f)]
        private float _handsMaxTorque = 40f;

        [Header("Debugging")]
        [SerializeField, ReadOnly]
        private bool _ready;

        private IVRPlayer _player;
        private float _lastSend;

        private Vector3 _lastPos;
        private Quaternion _lastRot;
        private Vector3 _targetPos;
        private Quaternion _targetRot;

        private Vector3 _headPos;
        private Quaternion _headRot;
        private Vector3 _headTargetPos;
        private Quaternion _headTargetRot;

        private Vector3 _leftHandPos;
        private Quaternion _leftHandRot;
        private Vector3 _leftHandTargetPos;
        private Quaternion _leftHandTargetRot;

        private Vector3 _rightHandPos;
        private Quaternion _rightHandRot;
        private Vector3 _rightHandTargetPos;
        private Quaternion _rightHandTargetRot;

        private void OnEnable()
        {
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;

            _lastPos = _targetPos = _player.position;
            _lastRot = _targetRot = _player.rotation;

            _headPos = _headTargetPos = _player.head.position;
            _headRot = _headTargetRot = _player.head.rotation;

            _leftHandPos = _leftHandTargetPos = _player.leftHand.position;
            _leftHandRot = _leftHandTargetRot = _player.leftHand.rotation;

            _rightHandPos = _rightHandTargetPos = _player.rightHand.position;
            _rightHandRot = _rightHandTargetRot = _player.rightHand.rotation;

            _ready = true;
        }

        /// <summary>
        /// On update all authority objects send their movements and rotations to all the non-authoritative objects.
        /// </summary>
        private void Update()
        {
            if (!this.hasAuthority || !_ready)
            {
                return;
            }

            var time = Time.timeSinceLevelLoad;
            if (time < _lastSend)
            {
                return;
            }

            _lastSend = time + (1f / _sendRate);

            // Player position and rotation
            var deltaPosSqr = (_lastPos - _player.position).sqrMagnitude;
            if (deltaPosSqr > (_positionThreshold * _positionThreshold))
            {
                UpdatePlayerPosition(_player.position);
                _lastPos = _player.position;
            }

            var deltaRotSqr = (_lastRot.eulerAngles - _player.rotation.eulerAngles).sqrMagnitude;
            if (deltaRotSqr > (_rotationThreshold * _rotationThreshold))
            {
                UpdatePlayerRotation(_player.rotation);
                _lastRot = _player.rotation;
            }

            // Head position and rotation
            var headDeltaPosSqr = (_headPos - _player.head.position).sqrMagnitude;
            if (headDeltaPosSqr > (_headPositionThreshold * _headPositionThreshold))
            {
                UpdateHeadPosition(_player.head.position);
                _headPos = _player.head.position;
            }

            var headDeltaRotSqr = (_headRot.eulerAngles - _player.head.rotation.eulerAngles).sqrMagnitude;
            if (headDeltaRotSqr > (_headRotationThreshold * _headRotationThreshold))
            {
                UpdateHeadRotation(_player.head.rotation);
                _headRot = _player.head.rotation;
            }

            // Hands
            var handPosThresholdSqr = _handsPositionThreshold * _handsPositionThreshold;
            var handRotThresholdSqr = _handsRotationThreshold * _handsRotationThreshold;

            // Left hand position and rotation
            var leftHandPos = _player.leftHand.position;
            var lhDeltaPosSqr = (_leftHandPos - leftHandPos).sqrMagnitude;
            if (lhDeltaPosSqr > handPosThresholdSqr)
            {
                UpdateLeftHandPosition(leftHandPos);
                _leftHandPos = leftHandPos;
            }

            var leftHandRot = _player.leftHand.rotation;
            var lhDeltaRotSqr = (_leftHandRot.eulerAngles - leftHandRot.eulerAngles).sqrMagnitude;
            if (lhDeltaRotSqr > handRotThresholdSqr)
            {
                UpdateLeftHandRotation(leftHandRot);
                _leftHandRot = leftHandRot;
            }

            // Right hand position and rotation
            var rightHandPos = _player.rightHand.position;
            var rhDeltaPosSqr = (_rightHandPos - rightHandPos).sqrMagnitude;
            if (rhDeltaPosSqr > handPosThresholdSqr)
            {
                UpdateRightHandPosition(rightHandPos);
                _rightHandPos = rightHandPos;
            }

            var rightHandRot = _player.rightHand.rotation;
            var rhDeltaRotSqr = (_rightHandRot.eulerAngles - rightHandRot.eulerAngles).sqrMagnitude;
            if (rhDeltaRotSqr > handRotThresholdSqr)
            {
                UpdateRightHandRotation(rightHandRot);
                _rightHandRot = rightHandRot;
            }
        }

        /// <summary>
        /// On fixed update non-authority objects follow whatever targets they receive from the one that has authority.
        /// </summary>
        private void FixedUpdate()
        {
            if (this.hasAuthority || !_ready)
            {
                return;
            }

            var deltaTime = Time.deltaTime;
            _player.position = Vector3.Lerp(_player.position, _targetPos, deltaTime * _playerMaxSpeed);
            _player.rotation = Quaternion.Slerp(_player.rotation, _targetRot, deltaTime * _playerMaxTorque);

            _player.head.position = Vector3.Lerp(_player.head.position, _headTargetPos, deltaTime * _headMaxSpeed);
            _player.head.rotation = Quaternion.Slerp(_player.head.rotation, _headTargetRot, deltaTime * _headMaxTorque);

            _player.leftHand.position = Vector3.Lerp(_player.leftHand.position, _leftHandTargetPos, deltaTime * _handsMaxSpeed);
            _player.leftHand.rotation = Quaternion.Slerp(_player.leftHand.rotation, _leftHandTargetRot, deltaTime * _handsMaxTorque);

            _player.rightHand.position = Vector3.Lerp(_player.rightHand.position, _rightHandTargetPos, deltaTime * _handsMaxSpeed);
            _player.rightHand.rotation = Quaternion.Slerp(_player.rightHand.rotation, _rightHandTargetRot, deltaTime * _handsMaxTorque);
        }

        #region PLAYER_SYNC

        private void UpdatePlayerPosition(Vector3 position)
        {
            if (NetworkClient.active)
            {
                CmdUpdatePlayerPos(position);
            }
            else if (NetworkServer.active)
            {
                RpcUpdatePlayerPos(position);
            }
        }

        [Command]
        private void CmdUpdatePlayerPos(Vector3 position)
        {
            RpcUpdatePlayerPos(position);
        }

        [ClientRpc]
        private void RpcUpdatePlayerPos(Vector3 position)
        {
            _targetPos = position;
        }

        private void UpdatePlayerRotation(Quaternion rotation)
        {
            if (NetworkClient.active)
            {
                CmdUpdatePlayerRot(rotation.eulerAngles);
            }
            else if (NetworkServer.active)
            {
                RpcUpdatePlayerRot(rotation.eulerAngles);
            }
        }

        [Command]
        private void CmdUpdatePlayerRot(Vector3 rotation)
        {
            RpcUpdatePlayerRot(rotation);
        }

        [ClientRpc]
        private void RpcUpdatePlayerRot(Vector3 rotation)
        {
            _targetRot = Quaternion.Euler(rotation);
        }

        #endregion PLAYER_SYNC

        #region HEAD_SYNC

        private void UpdateHeadPosition(Vector3 position)
        {
            if (NetworkClient.active)
            {
                CmdUpdateHeadPos(position);
            }
            else if (NetworkServer.active)
            {
                RpcUpdateHeadPos(position);
            }
        }

        [Command]
        private void CmdUpdateHeadPos(Vector3 position)
        {
            RpcUpdateHeadPos(position);
        }

        [ClientRpc]
        private void RpcUpdateHeadPos(Vector3 position)
        {
            _headTargetPos = position;
        }

        private void UpdateHeadRotation(Quaternion rotation)
        {
            if (NetworkClient.active)
            {
                CmdUpdateHeadRot(rotation.eulerAngles);
            }
            else if (NetworkServer.active)
            {
                RpcUpdateHeadRot(rotation.eulerAngles);
            }
        }

        [Command]
        private void CmdUpdateHeadRot(Vector3 rotation)
        {
            RpcUpdateHeadRot(rotation);
        }

        [ClientRpc]
        private void RpcUpdateHeadRot(Vector3 rotation)
        {
            _headTargetRot = Quaternion.Euler(rotation);
        }

        #endregion HEAD_SYNC

        #region LEFT_HAND_SYNC

        private void UpdateLeftHandPosition(Vector3 position)
        {
            if (NetworkClient.active)
            {
                CmdUpdateLeftHandPos(position);
            }
            else if (NetworkServer.active)
            {
                RpcUpdateLeftHandPos(position);
            }
        }

        [Command]
        private void CmdUpdateLeftHandPos(Vector3 position)
        {
            RpcUpdateLeftHandPos(position);
        }

        [ClientRpc]
        private void RpcUpdateLeftHandPos(Vector3 position)
        {
            _leftHandTargetPos = position;
        }

        private void UpdateLeftHandRotation(Quaternion rotation)
        {
            if (NetworkClient.active)
            {
                CmdUpdateLeftHandRot(rotation.eulerAngles);
            }
            else if (NetworkServer.active)
            {
                RpcUpdateLeftHandRot(rotation.eulerAngles);
            }
        }

        [Command]
        private void CmdUpdateLeftHandRot(Vector3 rotation)
        {
            RpcUpdateLeftHandRot(rotation);
        }

        [ClientRpc]
        private void RpcUpdateLeftHandRot(Vector3 rotation)
        {
            _leftHandTargetRot = Quaternion.Euler(rotation);
        }

        #endregion LEFT_HAND_SYNC

        #region RIGHT_HAND_SYNC

        private void UpdateRightHandPosition(Vector3 position)
        {
            if (NetworkClient.active)
            {
                CmdUpdateRightHandPos(position);
            }
            else if (NetworkServer.active)
            {
                RpcUpdateRightHandPos(position);
            }
        }

        [Command]
        private void CmdUpdateRightHandPos(Vector3 position)
        {
            RpcUpdateRightHandPos(position);
        }

        [ClientRpc]
        private void RpcUpdateRightHandPos(Vector3 position)
        {
            _rightHandTargetPos = position;
        }

        private void UpdateRightHandRotation(Quaternion rotation)
        {
            if (NetworkClient.active)
            {
                CmdUpdateRightHandRot(rotation.eulerAngles);
            }
            else if (NetworkServer.active)
            {
                RpcUpdateRightHandRot(rotation.eulerAngles);
            }
        }

        [Command]
        private void CmdUpdateRightHandRot(Vector3 rotation)
        {
            RpcUpdateRightHandRot(rotation);
        }

        [ClientRpc]
        private void RpcUpdateRightHandRot(Vector3 rotation)
        {
            _rightHandTargetRot = Quaternion.Euler(rotation);
        }

        #endregion RIGHT_HAND_SYNC
    }
}