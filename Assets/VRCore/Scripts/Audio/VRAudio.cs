namespace VRCore
{
	using System;
	using UnityEngine;

	[Serializable]
	public class VRAudio : object
	{
		[SerializeField, Tooltip("The audio source to use for this audio")]
		private AudioSource _source = null;

		[SerializeField, Tooltip("Delay in number of samples, assuming a 44100Hz sample rate (meaning that 44100 will delay the playing by exactly 1 sec).")]
		private ulong _delay = 0;

		[SerializeField, Tooltip("Whether to force the given AudioSource to spatialize in 3D.")]
		private bool _forcedSpatialize3D = true;

		[SerializeField]
		private bool _loop = false;

		[SerializeField, Tooltip("A singular audio clip to play.")]
		private AudioClip _clip = null;

		[SerializeField, Tooltip("A random audio clip to play from this list.")]
		private AudioClip[] _clips = new AudioClip[0];

		public bool CanPlay()
		{
			return _source != null && (_clip != null || _clips.Length > 0);
		}

		public bool IsPlaying()
		{
			return _source != null && _source.isPlaying;
		}

		public void SetAudioSource(AudioSource source)
		{
			if (source == null)
			{
				Debug.LogWarning(this.ToString() + " supplied AudioSource is null!");
				return;
			}

			_source = source;
		}

		public AudioSource GetAudioSource()
		{
			return _source;
		}

		public void Play()
		{
			if (_source == null)
			{
				Debug.LogError(this.ToString() + " could not play audio, since the AudioSource reference is null");
				return;
			}

			var clip = _clip ?? _clips.Random();
			if (clip == null)
			{
				Debug.LogWarning(this.ToString() + " (" + _source.gameObject.ToString() + ") could not play audio, since the AudioClip is null", _source);
				return;
			}

			if (_forcedSpatialize3D)
			{
				_source.spatialize = true;
				_source.spatialBlend = 1f;
			}

			_source.clip = clip;
			_source.loop = _loop;

			if (_delay > 0)
			{
				_source.Play(_delay);
			}
			else
			{
				_source.Play();
			}
		}

		public void Stop()
		{
			if (_source == null)
			{
				Debug.LogError(this.ToString() + " could not stop audio, since the AudioSource reference is null");
				return;
			}

			_source.Stop();
		}

		public void Pause()
		{
			if (_source == null)
			{
				Debug.LogError(this.ToString() + " could not pause audio, since the AudioSource reference is null");
				return;
			}

			_source.Pause();
		}

		public void Resume()
		{
			if (_source == null)
			{
				Debug.LogError(this.ToString() + " could not resume audio, since the AudioSource reference is null");
				return;
			}

			_source.UnPause();
		}

		/* FROM ATOMTWIST / SOUNDCLIP / AUDIOCLIP: */
		//bool fadingIn;
		//void StartFadeIn(float fadeIn, float delayTime = 0)
		//{
		//    timeInSamples = AudioSettings.outputSampleRate * 2 * fadeIn;
		//    delayTimeInSamples = AudioSettings.outputSampleRate * 2 * delayTime;
		//    delayCounter = 0;
		//    gain = 0;
		//    counter = 0;
		//    fadingOut = false;
		//    fadingIn = true;
		//}

		//bool fadingOut;
		//void StartFadeOut(float fadeOut)
		//{
		//    timeInSamples = AudioSettings.outputSampleRate * 2 * fadeOut;
		//    //gain = 1;
		//    counter = 0;
		//    fadingIn = false;
		//    fadingOut = true;
		//}

		//float timeInSamples;
		//float delayTimeInSamples;
		//double delayCounter;
		//double gain;
		//int counter;
		//void OnAudioFilterRead(float[] data, int channels)
		//{
		//    for (var i = 0; i < data.Length; ++i)
		//    {
		//        if (fadingIn)
		//        {
		//            delayCounter += 1 / delayTimeInSamples;
		//            if (delayCounter > 1)
		//            {
		//                gain += 1 / timeInSamples;
		//            }
		//            counter++;
		//            if (gain > 1)
		//            {
		//                gain = 1;
		//                fadingIn = false;
		//            }
		//        }

		//        if (fadingOut)
		//        {
		//            gain -= 1 / timeInSamples;
		//            counter++;
		//            if (gain < 0)
		//            {
		//                gain = 0;
		//                fadingOut = false;
		//            }
		//        }

		//        data[i] = Mathf.Clamp(data[i] * (float)gain, -1, 1);
		//    }
		//}
	}
}