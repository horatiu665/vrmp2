namespace VRCore
{
    public interface IVRSpawnable : IVRPrefab, IHaveVRCustomBehaviours
    {
        /// <summary>
        /// Gets the player who spawned this spawnable.
        /// </summary>
        /// <value>
        /// The player.
        /// </value>
        IVRPlayer player { get; }

        /// <summary>
        /// Gets a value indicating whether this equippable mixes its behaviour setup with that on the player.
        /// </summary>
        /// <value>
        /// <c>true</c> if mixing with player behaviours; otherwise, <c>false</c>.
        /// </value>
        bool mixWithPlayerBehaviours { get; }

        /// <summary>
        /// Called before being spawned.
        /// </summary>
        /// <param name="spawner">The spawner.</param>
        void OnPreSpawn(IVRPlayer spawner);

        /// <summary>
        /// Called after being spawned.
        /// </summary>
        void OnPostSpawn();
    }
}