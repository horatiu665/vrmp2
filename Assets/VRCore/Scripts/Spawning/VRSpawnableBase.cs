namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class VRSpawnableBase : VRPrefabBase, IVRSpawnable
    {
        [SerializeField, Tooltip("Whether this spawnable mixes its behaviour setup with that on the player.")]
        private bool _mixWithPlayerBehaviours = true;

        protected readonly IList<IVRCustomBehaviour> _behaviours = new List<IVRCustomBehaviour>();

        public IVRPlayer player
        {
            get;
            protected set;
        }

        public IList<IVRCustomBehaviour> behaviours
        {
            get { return _behaviours; }
        }

        public bool mixWithPlayerBehaviours
        {
            get { return _mixWithPlayerBehaviours; }
        }

        public virtual void OnPreSpawn(IVRPlayer player)
        {
            this.player = player;
        }

        public virtual void OnPostSpawn()
        {
            /* NOOP */
        }
    }
}