namespace VRCore
{
    using System;

    [Serializable]
    public class SpawnSetup
    {
        public VRPrefabType prefabType;
        public VRInput inputButton;
    }
}