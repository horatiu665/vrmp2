namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public abstract class SpawnManagerBase<T> : SingletonMonoBehaviour<SpawnManagerBase<T>> where T : SpawnSetup
    {
        protected readonly VRSpawnBehaviourInput _input = new VRSpawnBehaviourInput();
        protected readonly VRSpawnBehaviourOutput _output = new VRSpawnBehaviourOutput();

        [Header("Left Hand Setup")]
        [SerializeField]
        protected T[] _leftSetup = new T[0];

        [Header("Right Hand Setup")]
        [SerializeField]
        protected T[] _rightSetup = new T[0];

        /// <summary>
        /// Gets the current setup for the left hand.
        /// </summary>
        /// <value>
        /// The setup for the left hand.
        /// </value>
        public T[] leftSetup
        {
            get { return _leftSetup; }
        }

        /// <summary>
        /// Gets current setup for the the right hand.
        /// </summary>
        /// <value>
        /// The setup for the right hand.
        /// </value>
        public T[] rightSetup
        {
            get { return _rightSetup; }
        }

        /// <summary>
        /// Determines whether the specified hand has setup mapping matching the indicated input and prefab.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> checks left hand setup.</param>
        /// <param name="input">The input.</param>
        /// <param name="prefabType">Type of the prefab.</param>
        /// <returns>
        ///   <c>true</c> if the specified hand has setup mapping; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool HasMapping(bool isLeft, VRInput input, VRPrefabType prefabType)
        {
            var setup = GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton == input && setup[i].prefabType == prefabType)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified hand has any setup mapping for the indicated prefab type.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> checks left hand setup.</param>
        /// <param name="type">The type.</param>
        /// <returns>
        ///   <c>true</c> if the specified hand has setup mapping; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool HasMapping(bool isLeft, VRPrefabType type)
        {
            var setup = GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].prefabType == type)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Determines whether the specified hand has setup mapping matching the indicated input.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> checks left hand setup.</param>
        /// <param name="input">The input.</param>
        /// <returns>
        ///   <c>true</c> if the specified hand has setup mapping; otherwise, <c>false</c>.
        /// </returns>
        public virtual bool HasMapping(bool isLeft, VRInput input)
        {
            var setup = GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return false;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton == input)
                {
                    return true;
                }
            }

            return false;
        }

        /// <summary>
        /// Gets the setup mapping for the indicated hand matching the given input. However, this method has runtime memory allocations due to the creation of new list. Consider using <seealso cref="GetMapped(bool, VRInput, IList{T})"/> instead.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> checks left hand setup.</param>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public virtual IList<T> GetMapped(bool isLeft, VRInput input)
        {
            var list = new List<T>(1);
            GetMapped(isLeft, input, list);
            return list;
        }

        /// <summary>
        /// Gets the setup mapping for the indicated hand matching the given input.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c> checks left hand setup.</param>
        /// <param name="input">The input.</param>
        /// <returns></returns>
        public virtual void GetMapped(bool isLeft, VRInput input, IList<T> outList)
        {
            var setup = GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton == input)
                {
                    outList.Add(setup[i]);
                }
            }
        }

        /// <summary>
        /// Remaps the specified hand from the old prefab type to the new prefab type. Changes the first matched element.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c>, remaps left hand.</param>
        /// <param name="oldPrefabType">Old type of the prefab.</param>
        /// <param name="newPrefabType">Type of the prefab.</param>
        public virtual void Remap(bool isLeft, VRPrefabType oldPrefabType, VRPrefabType newPrefabType)
        {
            var setup = this.GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].prefabType == oldPrefabType)
                {
                    setup[i].prefabType = newPrefabType;
                    break;
                }
            }
        }

        /// <summary>
        /// Remaps the specified hand on the given input to use the specified new prefab type. Changes the first matched element.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c>, remaps left hand.</param>
        /// <param name="input">The input.</param>
        /// <param name="newPrefabType">Type of the prefab.</param>
        public virtual void Remap(bool isLeft, VRInput input, VRPrefabType newPrefabType)
        {
            var setup = this.GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton == input)
                {
                    setup[i].prefabType = newPrefabType;
                    break;
                }
            }
        }

        /// <summary>
        /// Remaps the specified hand from the old input type to the new input type. Changes the first matched element.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c>, remaps left hand.</param>
        /// <param name="oldInput">The old input.</param>
        /// <param name="newInput">The new input.</param>
        public virtual void Remap(bool isLeft, VRInput oldInput, VRInput newInput)
        {
            var setup = this.GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton == oldInput)
                {
                    setup[i].inputButton = newInput;
                    break;
                }
            }
        }

        /// <summary>
        /// Remaps the specified hand for the given input type, from the specified old prefab type to the new prefab type.
        /// </summary>
        /// <param name="isLeft">if set to <c>true</c>, remaps left hand.</param>
        /// <param name="input">The input.</param>
        /// <param name="oldPrefabType">Old type of the prefab.</param>
        /// <param name="newPrefabType">New type of the prefab.</param>
        public virtual void Remap(bool isLeft, VRInput input, VRPrefabType oldPrefabType, VRPrefabType newPrefabType)
        {
            var setup = this.GetSetup(isLeft);
            if (setup.Length == 0)
            {
                return;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton == input && setup[i].prefabType == oldPrefabType)
                {
                    setup[i].prefabType = newPrefabType;
                    break;
                }
            }
        }

        ///// <summary>
        ///// Returns the first SpawnSetup with the specified prefab type. Null if not found.
        ///// </summary>
        ///// <param name="prefabType"></param>
        ///// <returns></returns>
        //public virtual SpawnSetup GetFirstSpawnable(bool isLeft, VRPrefabType prefabType)
        //{
        //    var setup = this.GetSetup(isLeft);
        //    if (setup.Length == 0)
        //    {
        //        return null;
        //    }

        //    for (int i = 0; i < setup.Length; i++)
        //    {
        //        if (setup[i].prefabType == prefabType)
        //        {
        //            return setup[i];
        //        }
        //    }

        //    return null;
        //}

        ///// <summary>
        ///// Returns the first SpawnSetup that uses the specified input button. Null if not found.
        ///// </summary>
        ///// <param name="prefabType"></param>
        ///// <returns></returns>
        //public virtual SpawnSetup GetFirstSpawnable(bool isLeft, VRInput inputButton)
        //{
        //    var setup = this.GetSetup(isLeft);
        //    if (setup.Length == 0)
        //    {
        //        return null;
        //    }

        //    for (int i = 0; i < setup.Length; i++)
        //    {
        //        if (setup[i].inputButton == inputButton)
        //        {
        //            return setup[i];
        //        }
        //    }

        //    return null;
        //}

        /// <summary>
        /// used by single/local player
        /// </summary>
        /// <typeparam name="TPrefab"></typeparam>
        /// <param name="hand"></param>
        /// <param name="input"></param>
        /// <param name="outList"></param>
        public virtual void Spawn<TPrefab>(IVRHand hand, VRInput input, IList<TPrefab> outList) where TPrefab : class, IVRSpawnable
        {
            var setup = GetSetup(hand.isLeft);
            if (setup.Length == 0)
            {
                return;
            }

            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].inputButton != input)
                {
                    // input does not match
                    continue;
                }

                // TODO: THIS IS A TEMPORARY FIX
                if (setup[i].prefabType == VRPrefabType.FlyWings || setup[i].prefabType == VRPrefabType.GiantMode)
                {
                    Debug.LogWarning(this.ToString() + " was asked to spawn " + setup[i].prefabType.ToString() + ", but it is not allowed to spawn that!");
                    continue;
                }

                var prefab = Spawn<TPrefab>(hand, setup[i].prefabType);
                outList.Add(prefab);
            }
        }

        /// <summary>
        /// can be used directly by network
        /// </summary>
        /// <typeparam name="TPrefab"></typeparam>
        /// <param name="hand"></param>
        /// <param name="prefabType"></param>
        /// <returns></returns>
        public virtual TPrefab Spawn<TPrefab>(IVRHand hand, VRPrefabType prefabType) where TPrefab : class, IVRSpawnable
        {
            var prefab = VRPrefabManager.instance.Spawn<TPrefab>(prefabType);
            if (prefab == null)
            {
                return null;
            }

            return Spawn(hand, prefab);
        }

        /// <summary>
        /// the big one - runs all the behaviours.
        /// </summary>
        /// <typeparam name="TPrefab"></typeparam>
        /// <param name="hand"></param>
        /// <param name="prefab"></param>
        /// <returns></returns>
        public virtual TPrefab Spawn<TPrefab>(IVRHand hand, TPrefab prefab) where TPrefab : class, IVRSpawnable
        {
            var prefabManager = VRPrefabManager.instance;

            prefab.OnPreSpawn(hand.player);
            var behaviours = prefab.GetBehaviours(hand.player);

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (behaviours == null || behaviours.Count == 0)
            {
                Debug.LogWarning(this.ToString() + " no Spawn Behaviours are defined for == " + prefab.ToString() + ", and no defaults exist on the player == " + hand.player.ToString());
            }

#endif
            _input.hand = hand;
            _input.player = hand.player;
            _input.spawnable = prefab;
            _output.Clear();

            var overridenSpawnable = false;
            var count = behaviours.Count;
            for (int i = 0; i < count; i++)
            {
                var behaviour = behaviours[i];
                if (!behaviour.isSpawnBehaviour)
                {
                    continue;
                }

                behaviour.OnSpawn(_input, _output);

                if (_output.overrideSpawnable != null)
                {
                    // The spawnable is being overriden
                    if (!overridenSpawnable)
                    {
                        // If the spawnable was overriden, and it hadn't been overriden before, we are supposed to stop the spawning of a new instance, thus we return the new instance back to its pool
                        overridenSpawnable = true;
                        prefabManager.Return(prefab);
                    }

                    // Update prefab reference to the overriden spawnable
                    prefab = (TPrefab)_output.overrideSpawnable;
                    _output.overrideSpawnable = null;

                    // must update the input object so that the next behaviour gets the overriden spawnable, rather than the original-now-returned one
                    _input.spawnable = prefab;

                    // since the original prefab is returned to the pool, we must use the behaviours of the overriden spawnable
                    behaviours = prefab.GetBehaviours(hand.player);
                    count = behaviours.Count;

                    // Finally, we need to identify where to continue the iteration from.
                    // We iterate through all the new behaviours, comparing their priority with the last executed priority, in order to continue iteration from the highest priority that does not exceed the current priority.
                    // Since the behaviours are already sorted based on priority, we just need to find the lowest indexed behaviour with less priority than the current priority.
                    var newIndex = 0;
                    var currentPriority = behaviour.priority;
                    for (int j = 0; j < count; j++)
                    {
                        if (behaviours[j].priority < currentPriority)
                        {
                            newIndex = j;
                            break;
                        }
                    }

                    i = newIndex - 1;
                }

                if (_output.stopExecution)
                {
                    // A behaviour has flagged that execution should stop
                    if (!overridenSpawnable)
                    {
                        // If no behaviours have overriden, the prefab is expected to NOT be spawned and thus we return it back to its pool
                        prefabManager.Return(prefab);
                    }

                    return null;
                }
            }

            prefab.OnPostSpawn();
            return prefab;
        }

        public T[] GetSetup(bool isLeft)
        {
            return isLeft ? _leftSetup : _rightSetup;
        }

#if UNITY_EDITOR

        [DebugButton]
        private void DuplicateSetup()
        {
            if (_leftSetup.Length == 0 && _rightSetup.Length == 0)
            {
                Debug.LogWarning(this.ToString() + " cannot duplicate setup - no setup exists!", this);
                return;
            }

            if (_leftSetup.Length > 0 && _rightSetup.Length == 0)
            {
                _rightSetup = _leftSetup;
                UnityEditor.EditorUtility.SetDirty(this);
            }
            else if (_rightSetup.Length > 0 && _leftSetup.Length == 0)
            {
                _leftSetup = _rightSetup;
                UnityEditor.EditorUtility.SetDirty(this);
            }
            else
            {
                Debug.LogWarning(this.ToString() + " both setups have elements, cannot resolve which setup list to duplicate. Empty one of them first!", this);
            }
        }

        [DebugButton]
        private void ClearSetup()
        {
            _leftSetup = _rightSetup = new T[0];
            UnityEditor.EditorUtility.SetDirty(this);
        }

#endif
    }
}