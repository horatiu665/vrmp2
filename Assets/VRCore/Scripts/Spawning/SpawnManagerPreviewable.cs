namespace VRCore
{
    using System;
    using UnityEngine;

    [Apex.ApexComponent("Player")]
    public sealed class SpawnManagerPreviewable : SpawnManagerBase<SpawnSetup>
    {
        [Header("Spawn Previews")]
        [SerializeField]
        private PrefabTypeAndSpawnCount[] _leftPreviews = new PrefabTypeAndSpawnCount[1];

        [SerializeField]
        private PrefabTypeAndSpawnCount[] _rightPreviews = new PrefabTypeAndSpawnCount[1];

        [Space]
        [SerializeField]
        private Vector3 _previewOffset = new Vector3(0f, 0f, 0.174f);

        private void OnEnable()
        {
            SpawnPreviewPrefabs();
        }

        public void SwapPreviews()
        {
            int leftIndex = 0;
            int rightIndex = 0;
            var spawnPreviewBosses = this.GetComponentsInChildren<SpawnPreviewBoss>(true);
            for (int i = 0; i < spawnPreviewBosses.Length; i++)
            {
                var boss = spawnPreviewBosses[i];
                if (boss.isRight)
                {
                    rightIndex = boss.currentIndex;
                }
                else
                {
                    leftIndex = boss.currentIndex;
                }
            }
            var ptemp = (PrefabTypeAndSpawnCount[])_leftPreviews.Clone();
            _leftPreviews = _rightPreviews;
            _rightPreviews = ptemp;

            var stemp = (SpawnSetup[])_leftSetup.Clone();
            _leftSetup = _rightSetup;
            _rightSetup = stemp;

            SpawnPreviewPrefabs(leftIndex, rightIndex);

        }

        public bool CanSpawn(bool isLeft, VRPrefabType type)
        {
            var setup = GetPreviews(isLeft);
            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].prefabType != type)
                {
                    continue;
                }

                return setup[i].spawnCount != 0;
            }

            return false;
        }

        public int GetSpawnCount(bool isLeft, VRPrefabType type)
        {
            var setup = GetPreviews(isLeft);
            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].prefabType != type)
                {
                    continue;
                }

                return setup[i].spawnCount;
            }

            return 0;
        }

        public void AdjustSpawnCount(bool isLeft, VRPrefabType type, int delta)
        {
            var setup = GetPreviews(isLeft);
            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].prefabType != type)
                {
                    continue;
                }

                if (setup[i].spawnCount == -1)
                {
                    // the thing has inifinite spawning, thus it cannot be adjusted
                    return;
                }

                setup[i].spawnCount += delta;
                if (setup[i].spawnCount < 0)
                {
                    setup[i].spawnCount = 0;
                }

                //Debug.Log(this.ToString() + " adjusting spawn count for prefab type == " + type.ToString() + ", new count == " + setup[i].spawnCount.ToString());
                return;
            }

            Debug.LogWarning(this.ToString() + " AdjustSpawnCount failed to find a matching setup for prefab type == " + type.ToString() + ", thus cannot adjust spawn count for it.");
        }

        public void SetSpawnCountInifinite(bool isLeft, VRPrefabType type)
        {
            SetSpawnCount(isLeft, type, -1);
        }

        public void SetSpawnCount(bool isLeft, VRPrefabType type, int count)
        {
            var setup = GetPreviews(isLeft);
            for (int i = 0; i < setup.Length; i++)
            {
                if (setup[i].prefabType != type)
                {
                    continue;
                }

                setup[i].spawnCount = count;
                return;
            }

            Debug.LogWarning(this.ToString() + " SetSpawnCount failed to find a matching setup for prefab type == " + type.ToString() + ", thus cannot set spawn count for it.");
        }

        private PrefabTypeAndSpawnCount[] GetPreviews(bool isLeft)
        {
            return isLeft ? _leftPreviews : _rightPreviews;
        }

        [DebugButton]
        private void SpawnPreviewPrefabs(int leftIndex = -1, int rightIndex = -1)
        {
            var prefabManager = VRPrefabManager.instance;
            var spawnPreviewBosses = this.GetComponentsInChildren<SpawnPreviewBoss>(true);
            for (int i = 0; i < spawnPreviewBosses.Length; i++)
            {
                var boss = spawnPreviewBosses[i];
                boss.transform.ClearChildren();

                var previews = boss.isRight ? _rightPreviews : _leftPreviews;
                for (int j = 0; j < previews.Length; j++)
                {
                    var setup = prefabManager.GetSetup(previews[j].prefabType);
                    var go = Instantiate(setup.previewPrefab, boss.transform.position + boss.transform.TransformDirection(_previewOffset), boss.transform.rotation, boss.transform);
                    var item = go.GetOrAddComponent<SpawnPreviewItem>();
                    item.prefabType = setup.prefabType;
                }

                boss.Initialize(boss.isRight ? rightIndex : leftIndex);
            }
        }

        // implemented due to shitty inspector that prevents right clicks on list elements
        [DebugButton]
        private void InspectorRemovePreviewGhetto(bool left, int index)
        {
            if (left)
            {
                _leftPreviews = _leftPreviews.RemoveAt(index);
            }
            else
            {
                _rightPreviews = _rightPreviews.RemoveAt(index);
            }
        }
    }

    [Serializable]
    public class PrefabTypeAndSpawnCount
    {
        public VRPrefabType prefabType;
        public int spawnCount = -1;
    }
}