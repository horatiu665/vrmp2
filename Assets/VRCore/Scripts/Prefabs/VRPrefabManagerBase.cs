namespace VRCore
{
	using Apex;
	using System.Linq;
	using UnityEngine;

	public abstract class VRPrefabManagerBase<T> : SingletonMonoBehaviour<T> where T : VRPrefabManagerBase<T>
	{
		public static event System.Action<IVRPrefab> OnSpawn;

		[SerializeField, Tooltip("Decides what the constraining type is for the auto-generated VRPrefabType enum. Basically controls how many different prefabs there may be registered.")]
		protected VRPrefabTypeMode _prefabTypeMode = VRPrefabTypeMode.Byte;

		[SerializeField, Range(0, 1000), Tooltip("The initial preallocation count for prefab pools.")]
		protected int _initialInstanceCount = 10;

		[SerializeField, Range(0, 2000), Tooltip("The initial capacity for prefab pools.")]
		protected int _initialCapacity = 20;

		[SerializeField, Tooltip("Drag all prefabs into this field. The VRPrefabType enum is auto-generated based on this array of prefabs.")]
		protected GameObject[] _prefabs = new GameObject[0];

		[SerializeField, Tooltip("The maximum equip distance for all the registered prefabs.")]
		protected float _maxEquipDistance;


		/// <summary>
		/// Gets the computed maximum equip distance - based on all registered prefabs.
		/// </summary>
		/// <value>
		/// The maximum equip distance.
		/// </value>
		public float maxEquipDistance
		{
			get { return _maxEquipDistance; }
		}

		/// <summary>
		/// Gets the currently selected prefab type mode, determining the base type of the auto-generated VRPrefabType enum.
		/// </summary>
		/// <value>
		/// The prefab type mode.
		/// </value>
		public VRPrefabTypeMode prefabTypeMode
		{
			get { return _prefabTypeMode; }
		}

#if UNITY_EDITOR

		/// <summary>
		/// Called by Unity when an inspector change occurs or when scripts recompile - only called in the editor.
		/// </summary>
		protected virtual void OnValidate()
		{
			_maxEquipDistance = ComputeMaxEquipDistance();

			SetPrefabTypes();
		}

#endif

		protected override void Awake()
		{
			base.Awake();

			_maxEquipDistance = ComputeMaxEquipDistance();

			var values = (VRPrefabType[])System.Enum.GetValues(typeof(VRPrefabType));
			var config = new PoolConfig[values.Length];
			for (int i = 0; i < values.Length; i++)
			{
				config[i] = new PoolConfig()
				{
					prefab = _prefabs[(int)values[i]],
					host = this.gameObject,
					entityIdentifier = values[i],
					entityType = typeof(IVRPrefab),
					initialInstanceCount = _initialInstanceCount,
					initialCapacity = _initialCapacity,
					isComponentPool = true,
					maxCapacity = int.MaxValue
				};

			}

			PoolManager.Configure(config);
		}

		public virtual TPrefab GetPrefab<TPrefab>(VRPrefabType type) where TPrefab : class, IVRPrefab
		{
			for (int i = 0; i < _prefabs.Length; i++)
			{
				var prefab = _prefabs[i];
				if (_prefabs[i] == null)
				{
					continue;
				}
				var comp = prefab.GetComponent<TPrefab>();
				if (comp == null)
				{
					continue;
				}

				if (comp.prefabType != type)
				{
					continue;
				}

				return comp;
			}

			return null;
		}

		public virtual TPrefab Spawn<TPrefab>(VRPrefabType type) where TPrefab : class, IVRPrefab
		{
			return Spawn<TPrefab>(type, Vector3.zero, Quaternion.identity);
		}

		public virtual TPrefab Spawn<TPrefab>(VRPrefabType type, Vector3 position, Quaternion rotation) where TPrefab : class, IVRPrefab
		{
			var prefab = Spawn(type, position, rotation);
			if (prefab != null)
			{
				var cast = prefab as TPrefab;
				if (cast != null)
				{
					return cast;
				}

				return prefab.transform.GetComponent<TPrefab>();
			}

			return null;
		}

		public virtual IVRPrefab Spawn(VRPrefabType type)
		{
			return Spawn(type, Vector3.zero, Quaternion.identity);
		}

		public virtual IVRPrefab Spawn(VRPrefabType type, Vector3 position, Quaternion rotation)
		{
			IVRPrefab prefab;

#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				var prefabgo = this.InstantiateSafe(_prefabs[(int)type], position, rotation);
				prefab = prefabgo.GetComponent<IVRPrefab>();
			}
			else
			{
#endif
				prefab = PoolManager.Get<IVRPrefab>(type, position, rotation);

#if UNITY_EDITOR
			}
#endif
			prefab.SetPrefabType(type);

			if (OnSpawn != null)
			{
				OnSpawn(prefab);
			}

			return prefab;
		}

		public virtual void Return(IVRPrefab prefab)
		{
			prefab.Recycle(prefab.entityIdentifier);
		}

		protected virtual float ComputeMaxEquipDistance()
		{
			var distance = 0f;
			for (int i = 0; i < _prefabs.Length; i++)
			{
				if (_prefabs[i] == null)
				{
					continue;
				}

				var equippable = _prefabs[i].GetComponent<IVREquippable>();
				if (equippable == null)
				{
					continue;
				}

				if (equippable.equipDistance > distance)
				{
					distance = equippable.equipDistance;
				}
			}

			return distance;
		}

		private void SetPrefabTypes()
		{
			for (int i = 0; i < _prefabs.Length; i++)
			{
				if (_prefabs[i] != null)
				{
					_prefabs[i].GetComponent<IVRPrefab>().SetPrefabType((VRPrefabType)i);
				}
			}
		}
	}
}