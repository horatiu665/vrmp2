namespace VRCore
{
    using Apex;
    using UnityEngine;

    public interface IVRPrefab : IPooledComponent, IEnumTyped
    {
        /// <summary>
        /// Gets the type of the prefab - used to identify prefabs over the network, as well as by <see cref="SpawnManagerBase{T}"/> and <see cref="VRPrefabManager"/>.
        /// </summary>
        /// <value>
        /// The type of the prefab.
        /// </value>
        VRPrefabType prefabType { get; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        Vector3 position { get; set; }

        /// <summary>
        /// Gets or sets the local position.
        /// </summary>
        /// <value>
        /// The local position.
        /// </value>
        Vector3 localPosition { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        Quaternion rotation { get; set; }

        /// <summary>
        /// Gets or sets the local rotation.
        /// </summary>
        /// <value>
        /// The local rotation.
        /// </value>
        Quaternion localRotation { get; set; }

        /// <summary>
        /// Sets the type of this prefab. Not meant to be called outside of the <see cref="VRPrefabManager"/>.
        /// </summary>
        /// <param name="type">The type.</param>
        void SetPrefabType(VRPrefabType type);
    }
}