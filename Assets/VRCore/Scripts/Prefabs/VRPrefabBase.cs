namespace VRCore
{
    using System;
    using Apex;
    using UnityEngine;

    public class VRPrefabBase : PooledComponentBase, IVRPrefab
    {
        [Header("Prefab")]
        [SerializeField]
        protected VRPrefabType _prefabType;

        public VRPrefabType prefabType
        {
            get { return _prefabType; }
        }

        public Enum entityIdentifier
        {
            get { return _prefabType; }
        }

        public Vector3 localPosition
        {
            get { return this.transform.localPosition; }
            set { this.transform.localPosition = value; }
        }

        public Vector3 position
        {
            get { return this.transform.position; }
            set { this.transform.position = value; }
        }

        public Quaternion localRotation
        {
            get { return this.transform.localRotation; }
            set { this.transform.localRotation = value; }
        }

        public Quaternion rotation
        {
            get { return this.transform.rotation; }
            set { this.transform.rotation = value; }
        }

        void IVRPrefab.SetPrefabType(VRPrefabType type)
        {
            _prefabType = type;
        }
    }
}