namespace VRCore
{
    using UnityEngine;

    public sealed class VRPrefabManager : VRPrefabManagerBase<VRPrefabManager>
    {
        [Header("Previews")]
        [SerializeField]
        private PreviewSetup[] _previewSetup = new PreviewSetup[0];

        public PreviewSetup[] previews
        {
            get { return _previewSetup; }
        }

        public PreviewSetup GetSetup(VRPrefabType prefabType)
        {
            for (int i = 0; i < _previewSetup.Length; i++)
            {
                if (_previewSetup[i].prefabType == prefabType)
                {
                    return _previewSetup[i];
                }
            }

            return default(PreviewSetup);
        }
    }
}