namespace VRCore
{
    using System.Collections.Generic;

    public sealed class VRPrefabTypeComparer : IEqualityComparer<VRPrefabType>
    {
        public bool Equals(VRPrefabType x, VRPrefabType y)
        {
            return x == y;
        }

        public int GetHashCode(VRPrefabType obj)
        {
            return (int)obj;
        }
    }
}