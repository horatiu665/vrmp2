namespace VRCore
{
    using System;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.Field)]
    public class HelpBoxAttribute : PropertyAttribute
    {
        public string text = string.Empty;
        public HelpBoxType type = 0;

        public HelpBoxAttribute(string text, HelpBoxType type)
        {
            this.text = text;
            this.type = type;
        }
    }
}