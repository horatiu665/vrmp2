namespace VRCore
{
    using System;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class MinMaxAttribute : PropertyAttribute
    {
        public string label { get; set; }

        public float min { get; set; }

        public float max { get; set; }
    }
}