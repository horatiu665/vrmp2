namespace VRCore
{
    /// <summary>
    /// Types of editor GUI help boxes, see <seealso cref="UnityEditor.EditorGUILayout.HelpBox(string, UnityEditor.MessageType)"/> and <seealso cref="UnityEditor.MessageType"/>.
    /// </summary>
    public enum HelpBoxType
    {
        /// <summary>
        /// Neutral message.
        /// </summary>
        None = 0,

        /// <summary>
        /// Info message.
        /// </summary>
        Info = 1,

        /// <summary>
        /// Warning message.
        /// </summary>
        Warning = 2,

        /// <summary>
        /// Error message.
        /// </summary>
        Error = 3
    }
}