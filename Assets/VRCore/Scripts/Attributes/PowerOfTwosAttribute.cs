namespace VRCore
{
    using System;
    using UnityEngine;

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class PowerOfTwosAttribute : PropertyAttribute
    {
        public float min { get; set; }

        public float max { get; set; }
    }
}