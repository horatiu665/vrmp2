namespace VRCore
{
    public interface IVRPreviewBehaviour
    {
        bool enabled { get; }

        VRPrefabType prefabType { get; }

        void OnPreviewEnter(VRPreviewChangeInput input);

        void OnPreviewExit(VRPreviewChangeInput input);
    }
}