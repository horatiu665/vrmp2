namespace VRCore
{
    using System.Collections.Generic;
    using System.Linq;
    using Apex.Services;
    using UnityEngine;

    /// <summary>
    /// provides the graphical interface to switch what is spawned in each hand
    /// but for each hand
    /// </summary>
    public sealed class SpawnPreviewBoss : MonoBehaviour, IVRPlayerStartListener, IHandleMessage<VRInputTouchDown>, IHandleMessage<VRInputTouch>, IHandleMessage<VRInputTouchUp>
    {
        [SerializeField]
        private bool _isRight = false;

        [SerializeField]
        [EnumButtons]
        private VRInput _zbutton = VRInput.Trigger;

        [SerializeField, Range(0, 1f)]
        private float _returnToSnapPosSpeed = 0.25f;

        [SerializeField]
        private bool _loopList = false;

        [Header("Graphics parameters")]
        [SerializeField]
        private AnimationCurve _scaleButtonCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [SerializeField]
        private AnimationCurve _positionButtonCurve = AnimationCurve.EaseInOut(0, 0, 1, 1);

        [SerializeField]
        private float _positionButtonCurveScale = 1f;

        private readonly IList<SpawnPreviewItem> _items = new List<SpawnPreviewItem>();
        private Vector2 _oldTouchInput = Vector2.zero;
        private float _indexContinuous = 0f;
        private bool _returnToSnapPos = true;
        private bool _rendering = true;
        private VRPrefabType _prevPrefabType;
        private IVRPlayer _player;
        private int _oldCurrentIndex;
        private bool _isTouching;

        public int currentIndex
        {
            get
            {
                return Mathf.Clamp(Mathf.RoundToInt(_indexContinuous), 0, items.Count - 1);
            }
        }

        public bool isRight
        {
            get
            {
                return _isRight;
            }
        }

        public IList<SpawnPreviewItem> items
        {
            get
            {
                return _items;
            }
        }

        public AnimationCurve positionButtonCurve
        {
            get
            {
                return _positionButtonCurve;
            }
        }

        public AnimationCurve scaleButtonCurve
        {
            get
            {
                return _scaleButtonCurve;
            }
        }

        public float positionButtonCurveScale
        {
            get
            {
                return _positionButtonCurveScale;
            }
        }

        public int maxIndex
        {
            get
            {
                return _items.Count;
            }
        }

        public void OnPlayerStart(IVRPlayer player)
        {
            _player = player;
        }

        private void OnEnable()
        {
            if (_player == null)
            {
                OnPlayerStart(this.GetComponentInParent<IVRPlayer>());
            }

            GameServices.messageBus.Subscribe<VRInputTouch>(this);
            GameServices.messageBus.Subscribe<VRInputTouchDown>(this);
            GameServices.messageBus.Subscribe<VRInputTouchUp>(this);
        }

        private void OnDisable()
        {
            GameServices.messageBus.Unsubscribe<VRInputTouch>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouchDown>(this);
            GameServices.messageBus.Unsubscribe<VRInputTouchUp>(this);
        }

        private void Start()
        {
            ChangePreview();
            Initialize();
        }

        public void Initialize(int index = -1)
        {
            if (index == -1)
            {
                // initialize to the first mapped item's index
                var manager = SpawnManagerPreviewable.instance;
                var firstSetup = manager.GetMapped(!_isRight, _zbutton).FirstOrDefault();
                _prevPrefabType = firstSetup.prefabType;
                _indexContinuous = manager.GetSetup(!_isRight).IndexOf(firstSetup);
            }
            else
            {
                _indexContinuous = index;
            }
        }

        public void Handle(VRInputTouchDown message)
        {
            _isTouching = true;
            if (HasEquipped())
            {
                return;
            }

            // if same hand as this class refers to
            if (message.isLeft ^ _isRight)
            {
                _oldTouchInput = message.hand.controller.GetAxis();
				_returnToSnapPos = false;
            }
        }

        private void ToggleRendering(bool enabled)
        {
            var count = _items.Count;
            for (int i = 0; i < count; i++)
            {
                _items[i].ToggleRendering(enabled);
            }

            _rendering = enabled;
        }

        public void Handle(VRInputTouch message)
        {
            if (HasEquipped())
            {
                if (_rendering)
                {
                    // if something is equipped and we are rendering, stop rendering now
                    ToggleRendering(false);
                }

                return;
            }

            if (!_rendering)
            {
                // if nothing is equipped and we are not rendering, start rendering now
                ToggleRendering(true);
            }

            if (!_isTouching)
            {
                return;
            }

            // if same hand as this class refers to
            if (message.isLeft ^ _isRight)
            {
                var swipeDelta = message.hand.controller.GetAxis() - _oldTouchInput;
				_indexContinuous -= swipeDelta.x;
                if (_indexContinuous > maxIndex - 0.51f)
                {
                    if (_loopList)
                    {
                        _indexContinuous -= maxIndex;
                    }
                    else
                    {
                        _indexContinuous = maxIndex - 0.51f;
                    }
                }
                else if (_indexContinuous < -0.49f)
                {
                    if (_loopList)
                    {
                        _indexContinuous += maxIndex;
                    }
                    else
                    {
                        _indexContinuous = -0.49f;
                    }
                }

                _oldTouchInput = message.hand.controller.GetAxis();
            }
        }

        public void Handle(VRInputTouchUp message)
        {
            _isTouching = false;
            if (HasEquipped())
            {
                return;
            }

            // if same hand as this class refers to
            if (message.isLeft ^ _isRight)
            {
                _returnToSnapPos = true;
            }
        }

        private void Update()
        {
            if (maxIndex == 0)
            {
                return;
            }

            if (HasEquipped())
            {
                return;
            }

            // snap list to int (a button is always in the center)
            if (_returnToSnapPos)
            {
                _indexContinuous = Mathf.Lerp(_indexContinuous, currentIndex, _returnToSnapPosSpeed);
                // if very small difference, set to the same value so it doesn't jitter
                if (Mathf.Abs(_indexContinuous - currentIndex) < 0.001f)
                {
                    _indexContinuous = currentIndex;
                }
            }

            if (_oldCurrentIndex != currentIndex)
            {
                _oldCurrentIndex = currentIndex;
                ChangePreview();
            }

            // set visibility of all buttons based on indexContinuous
            SetItemVisibility(_indexContinuous);
        }

        private void ChangePreview()
        {
            // Execute preview change on player - this is where the functionality occurs
            var newType = _items[this.currentIndex].prefabType;
            _player.OnChangePreview(!_isRight, _prevPrefabType, newType);
            _prevPrefabType = newType;
        }

        private void SetItemVisibility(float indexContinuous)
        {
            var count = _items.Count;
            for (int i = 0; i < count; i++)
            {
                _items[i].SetVisibility(i, indexContinuous);
            }
        }

        private bool HasEquipped()
        {
            if (_player == null)
            {
                return false;
            }

            var hand = _isRight ? _player.rightHand : _player.leftHand;
            return hand.equipped.Count > 0;
        }

#if UNITY_EDITOR

        [DebugButton]
        private void ChangePreviewTo(VRPrefabType prefabType)
        {
            _player.OnChangePreview(!_isRight, _items[this.currentIndex].prefabType, prefabType);
        }

#endif
    }
}