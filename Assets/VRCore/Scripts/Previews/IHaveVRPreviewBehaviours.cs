namespace VRCore
{
    using System.Collections.Generic;

    public interface IHaveVRPreviewBehaviours
    {
        IList<IVRPreviewBehaviour> previewBehaviours { get; }

        void OnChangePreview(bool isLeft, VRPrefabType oldType, VRPrefabType newType);
    }
}