namespace VRCore
{
	using UnityEngine;
	using UnityEngine.Serialization;

	public sealed class SpawnPreviewItem : MonoBehaviour
	{
		[SerializeField, FormerlySerializedAs("prefabType")]
		private VRPrefabType _prefabType = 0;

		[SerializeField, Tooltip("If null, will automatically look up SpawnPreviewBoss in parent hierarchy")]
		private SpawnPreviewBoss _boss;

		private Vector3 _initScale;
		private Renderer[] _renderers;
		private ParticleSystem[] _ps;
		private bool _rendering;

		public VRPrefabType prefabType
		{
			get { return _prefabType; }
			set { _prefabType = value; }
		}

		private void Awake()
		{
			_boss = _boss ?? this.GetComponentInParent<SpawnPreviewBoss>();
			_renderers = this.GetComponentsInChildren<Renderer>();
			_ps = GetComponentsInChildren<ParticleSystem>();
			_initScale = this.transform.localScale;
		}

		private void OnEnable()
		{
			_boss.items.Add(this);
			ToggleRendering(false);
		}

		private void OnDisable()
		{
			if (_boss != null)
			{
				_boss.items.Remove(this);
			}
		}

		public void ToggleRendering(bool enabled)
		{
			for (int i = 0; i < _renderers.Length; i++)
			{
				_renderers[i].enabled = enabled;
			}
			for (int i = 0; i < _ps.Length; i++)
			{
				if (enabled)
				{
					if (!_ps[i].isPlaying)
						_ps[i].Play();
				}
				else
				{
					_ps[i].Stop();
				}
			}

			_rendering = enabled;
		}

		public void SetVisibility(int itemIndex, float indexContinuous)
		{
			float relIndex = itemIndex - indexContinuous;
			var scaleFactor = _boss.scaleButtonCurve.Evaluate(relIndex);
			var posFactor = _boss.positionButtonCurve.Evaluate(relIndex);

			// to support looping:
			// relative button index goes from 0 to maxIndex, and we want to show button 0 looped with button maxIndex.
			// so we evaluate relativeButtonIndex as well as relativeButtonIndex + maxIndex and -maxIndex.
			// special case: buttonIndex == 0 or maxIndex-1
			var maxIndex = _boss.maxIndex;
			if (itemIndex == 0 && indexContinuous > 1)
			{
				scaleFactor = _boss.scaleButtonCurve.Evaluate(relIndex + maxIndex);
				posFactor = _boss.positionButtonCurve.Evaluate(relIndex + maxIndex);
			}
			else if (itemIndex == maxIndex - 1 && indexContinuous < maxIndex - 2)
			{
				scaleFactor = _boss.scaleButtonCurve.Evaluate(relIndex - maxIndex);
				posFactor = _boss.positionButtonCurve.Evaluate(relIndex - maxIndex);
			}

			if (scaleFactor <= 0f)
			{
				if (_rendering)
				{
					// if the scale is zero or less, disable rendering
					ToggleRendering(false);
				}
				return;
			}
			else if (!_rendering)
			{
				ToggleRendering(true);
			}

			// local scale and move.
			this.transform.localScale = _initScale * (scaleFactor);
			var pos = transform.localPosition;
			pos.x = posFactor * _boss.positionButtonCurveScale;
			this.transform.localPosition = pos;
		}
	}
}