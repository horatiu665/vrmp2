namespace VRCore
{
    using UnityEngine;
    using VRNetwork;

    [Apex.ApexComponent("Preview Behaviour")]
    [RequireComponent(typeof(IVRPlayer))]
    public abstract class VRPreviewBehaviourBase : MonoBehaviour, IVRPreviewBehaviour
    {
        [SerializeField, Tooltip("The prefab type for which this preview behaviour applies to.")]
        protected VRPrefabType _prefabType = 0;

        protected IVRPlayer _player;

        public virtual VRPrefabType prefabType
        {
            get { return _prefabType; }
        }

        protected virtual void OnEnable()
        {
            _player = this.GetComponentInParent<IVRPlayer>();
            if (_player != null)
            {
                _player.previewBehaviours.Add(this);
            }
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            else
            {
                Debug.LogWarning(this.ToString() + " could not find an IHaveVRPreviewBehaviours (e.g. a player) in the parent hierarchy");
            }
#endif
        }

        protected virtual void OnDisable()
        {
            if (_player != null)
            {
                _player.previewBehaviours.Remove(this);
            }
        }

        protected void UnequipFromOppositeHand(bool isLeft)
        {
            if (NetServices.isServer)
            {
                return;
            }

            // get opposite hand and check whether it has anything equipped
            var hand = isLeft ? _player.rightHand : _player.leftHand;
            var equipped = hand.equipped;
            var count = equipped.Count;
            if (count == 0)
            {
                return;
            }

            // actually iterate through other hand's equipped objects and unequip them
            var inputController = InputControllerDefault.instance;
            for (int i = 0; i < count; i++)
            {
                inputController.Unequip(equipped[i], hand.isLeft);
            }
        }

        public abstract void OnPreviewEnter(VRPreviewChangeInput input);

        public abstract void OnPreviewExit(VRPreviewChangeInput input);
    }
}