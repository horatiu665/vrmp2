namespace VRCore
{
    public class VRPreviewChangeInput
    {
        public IVRPlayer player = null;
        public IVRHand hand = null;
        public VRPrefabType oldType = 0;
        public VRPrefabType newType = 0;
    }
}