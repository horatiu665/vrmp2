namespace VRCore
{
    using UnityEngine;

    public struct ClosestPoint
    {
        public Vector3 position;
        public float sqrDistance;
    }
}
