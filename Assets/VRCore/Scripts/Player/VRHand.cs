namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public sealed class VRHand : IVRHand
    {
        private readonly bool _isLeft;
        private readonly IVRPlayer _player;
        private readonly Transform _transform;
        private readonly GameObject _handJointPrefab;
        private readonly IList<IVREquippable> _equipped;
        private readonly IList<IVREquippable> _highlighted;
        private readonly IList<ConfigurableJoint> _joints;

        private readonly VRHighlightBehaviourInput _highlightInput = new VRHighlightBehaviourInput();
        private readonly VRHighlightBehaviourOutput _highlightOutput = new VRHighlightBehaviourOutput();
        private readonly VREquipBehaviourInput _equipInput = new VREquipBehaviourInput();
        private readonly VREquipBehaviourOutput _equipOutput = new VREquipBehaviourOutput();
        private readonly VRUnequipBehaviourInput _unequipInput = new VRUnequipBehaviourInput();
        private readonly VRUnequipBehaviourOutput _unequipOutput = new VRUnequipBehaviourOutput();

        public VRHand(Transform hand, IVRPlayer player, bool isLeft, GameObject handJointPrefab)
        {
            _transform = hand;
            _player = player;
            _isLeft = isLeft;
            _handJointPrefab = handJointPrefab;

            _highlighted = new List<IVREquippable>();
            _equipped = new List<IVREquippable>();
            _joints = new List<ConfigurableJoint>();
        }

        /// <summary>
        /// Gets the transform for this hand.
        /// </summary>
        /// <value>
        /// The transform.
        /// </value>
        public Transform transform
        {
            get { return _transform; }
        }

        /// <summary>
        /// Gets the game object.
        /// </summary>
        /// <value>
        /// The game object.
        /// </value>
        public GameObject gameObject
        {
            get { return _transform.gameObject; }
        }

        /// <summary>
        /// Gets a value indicating whether this hand is left.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this hand is left; otherwise, <c>false</c>.
        /// </value>
        public bool isLeft
        {
            get { return _isLeft; }
        }

        /// <summary>
        /// Gets the currently equipped <see cref="IVREquippable"/>s, or null if none is equipped.
        /// </summary>
        /// <value>
        /// The equipped object (null if none).
        /// </value>
        public IList<IVREquippable> equipped
        {
            get { return _equipped; }
        }

        /// <summary>
        /// Gets the list of currently created joints, both active and inactive ones.
        /// </summary>
        /// <value>
        /// The joints.
        /// </value>
        public IList<ConfigurableJoint> joints
        {
            get { return _joints; }
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public Vector3 position
        {
            get { return _transform.position; }
            set { _transform.position = value; }
        }

        /// <summary>
        /// Gets or sets the local position.
        /// </summary>
        /// <value>
        /// The local position.
        /// </value>
        public Vector3 localPosition
        {
            get { return _transform.localPosition; }
            set { _transform.localPosition = value; }
        }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        public Quaternion rotation
        {
            get { return _transform.rotation; }
            set { _transform.rotation = value; }
        }

        /// <summary>
        /// Gets or sets the local rotation.
        /// </summary>
        /// <value>
        /// The local rotation.
        /// </value>
        public Quaternion localRotation
        {
            get { return _transform.localRotation; }
            set { _transform.localRotation = value; }
        }

        /// <summary>
        /// Gets the player whom this hand belongs to.
        /// </summary>
        /// <value>
        /// The player.
        /// </value>
        public IVRPlayer player
        {
            get { return _player; }
        }

        /// <summary>
        /// Gets or sets the <see cref="IVRController"/>, if it is available. This will return null for remote players and on the server - basically only usable for local players.
        /// </summary>
        /// <value>
        /// The controller.
        /// </value>
        public IVRController controller
        {
            get;
            private set;
        }

        /// <summary>
        /// Gets the currently highlighted equippables - the ones that are within this hand's colliders.
        /// </summary>
        /// <value>
        /// The highlighted.
        /// </value>
        public IList<IVREquippable> highlighted
        {
            get { return _highlighted; }
        }

        /// <summary>
        /// Sets the <see cref="IVRController" /> reference for this hand.
        /// </summary>
        /// <param name="controller">The controller.</param>
        void IVRHand.SetVRController(IVRController controller)
        {
            this.controller = controller;
        }

        /// <summary>
        /// Highlights the specified <see cref="IVREquippable" />.
        /// </summary>
        /// <param name="equippable">The equippable.</param>
        /// <returns></returns>
        public bool Highlight(IVREquippable equippable)
        {
            if (equippable == null)
            {
                Debug.LogError(this.ToString() + " Highlight() a null equippable was passed");
                return false;
            }

            if (_highlighted.Contains(equippable))
            {
                // the equippable is already highlighted
                return false;
            }

            var highlightBehaviours = equippable.GetBehaviours(_player);

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (highlightBehaviours == null || highlightBehaviours.Count == 0)
            {
                Debug.LogWarning(this.ToString() + " no Highlight Behaviours are defined for == " + equippable.ToString() + ", and no defaults exist on the player == " + _player.ToString());
            }

#endif

            _highlightInput.hand = this;
            _highlightInput.player = _player;
            _highlightInput.equippable = equippable;
            _highlightOutput.Clear();

            var count = highlightBehaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!highlightBehaviours[i].isHighlightBehaviour)
                {
                    continue;
                }

                highlightBehaviours[i].OnHighlight(_highlightInput, _highlightOutput);
                if (_highlightOutput.stopExecution)
                {
                    return false;
                }
            }

            _highlighted.Add(equippable);
            return true;
        }

        /// <summary>
        /// Unhighlights the specified <see cref="IVREquippable" />.
        /// </summary>
        /// <param name="equippable">The equippable.</param>
        /// <returns></returns>
        public bool Unhighlight(IVREquippable equippable)
        {
            if (equippable == null)
            {
                Debug.LogError(this.ToString() + " Highlight() a null equippable was passed");
                return false;
            }

            if (!_highlighted.Contains(equippable))
            {
                // the equippable is not highlighted
                return false;
            }

            var unhighlightBehaviours = equippable.GetBehaviours(_player);

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (unhighlightBehaviours == null || unhighlightBehaviours.Count == 0)
            {
                Debug.LogWarning(this.ToString() + " no Highlight Behaviours are defined for == " + equippable.ToString() + ", and no defaults exist on the player == " + _player.ToString());
            }

#endif

            _highlightInput.hand = this;
            _highlightInput.player = _player;
            _highlightInput.equippable = equippable;
            _highlightOutput.Clear();

            var count = unhighlightBehaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!unhighlightBehaviours[i].isHighlightBehaviour)
                {
                    continue;
                }

                unhighlightBehaviours[i].OnUnhighlight(_highlightInput, _highlightOutput);
                if (_highlightOutput.stopExecution)
                {
                    return false;
                }
            }

            _highlighted.Remove(equippable);
            return true;
        }

        /// <summary>
        /// Equips the given <see cref="IVREquippable" />, if possible. Returns <c>true</c> if successful, <c>false</c> otherwise.
        /// </summary>
        /// <param name="equippable">The object to equip.</param>
        /// <returns>
        /// Returns <c>true</c> if successful, <c>false</c> otherwise.
        /// </returns>
        public IVREquippable Equip(IVREquippable equippable)
        {
            if (equippable == null)
            {
                Debug.LogError(this.ToString() + " Equip() a null equippable was passed");
                return null;
            }

            if (_equipped.Contains(equippable))
            {
                // equippable is already equipped!
                Debug.LogWarning(this.ToString() + " Equip() equippable (" + equippable.ToString() + ") already equipped");
                return null;
            }

            var equipBehaviours = equippable.GetBehaviours(_player);

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (equipBehaviours == null || equipBehaviours.Count == 0)
            {
                Debug.LogWarning(this.ToString() + " no Equip Behaviours are defined for == " + equippable.ToString() + ", and no defaults exist on the player == " + _player.ToString());
            }

#endif

            _equipInput.hand = this;
            _equipInput.player = _player;
            _equipInput.equippable = equippable;
            _equipOutput.Clear();

            var count = equipBehaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!equipBehaviours[i].isEquipBehaviour)
                {
                    continue;
                }

                equipBehaviours[i].OnEquip(_equipInput, _equipOutput);

                if (_equipOutput.overrideEquippable != null)
                {
                    equippable = _equipOutput.overrideEquippable;
                    _equipOutput.overrideEquippable = null;

                    _equipInput.equippable = equippable;
                }

                if (_equipOutput.stopExecution)
                {
                    // if a single behaviour reports false, then stop
                    return null;
                }
            }

            // if the equippable is actually already equipped elsewhere, make sure it is unequipped from there first
            if (equippable.isEquipped)
            {
                equippable.hand.Unequip(equippable, Vector3.zero, Vector3.zero);
            }

            equipped.Add(equippable);
            equippable.OnEquipped(this);
            //Debug.Log(this.ToString() + " OnEquipped == " + equippable.ToString());

            if (_highlighted.Contains(equippable))
            {
                // if the equippable was previously highlighted, we must make sure that Unhighlight is called
                Unhighlight(equippable);
            }

            return equippable;
        }

        /// <summary>
        /// Unequips the specified equippable, if possible.
        /// Returns the unequipped <see cref="IVREquippable" />, or null in case of error.
        /// </summary>
        /// <param name="equippable"></param>
        /// <param name="velocity"></param>
        /// <param name="angularVelocity"></param>
        /// <returns>
        /// The dropped <see cref="IVREquippable" />, or null in case of error.
        /// </returns>
        public IVREquippable Unequip(IVREquippable equippable, Vector3 velocity, Vector3 angularVelocity)
        {
            if (equippable == null)
            {
                Debug.LogError(this.ToString() + " Unequip() a null equippable was passed");
                return null;
            }

            if (!_equipped.Contains(equippable))
            {
                // equippable is already equipped!
                Debug.LogWarning(this.ToString() + " Unequip() equippable (" + equippable.ToString() + ") already unequipped");
                return null;
            }

            var unequipBehaviours = equippable.GetBehaviours(_player);

#if UNITY_EDITOR || DEVELOPMENT_BUILD

            if (unequipBehaviours == null || unequipBehaviours.Count == 0)
            {
                Debug.LogWarning(this.ToString() + " no Unequip Behaviours are defined for == " + equippable.ToString() + ", and no defaults exist on the player == " + _player.ToString());
            }

#endif

            _unequipInput.hand = this;
            _unequipInput.player = _player;
            _unequipInput.equippable = equippable;
            _unequipInput.velocity = velocity;
            _unequipInput.angularVelocity = angularVelocity;
            _unequipOutput.Clear();

            var count = unequipBehaviours.Count;
            for (int i = 0; i < count; i++)
            {
                if (!unequipBehaviours[i].isUnequipBehaviour)
                {
                    continue;
                }

                unequipBehaviours[i].OnUnequip(_unequipInput, _unequipOutput);
                if (_unequipOutput.stopExecution)
                {
                    // if a single behaviour reports false, then stop
                    //Debug.Log(this.ToString() + " stopping execution due to == " + unequipBehaviours[i].ToString());
                    return null;
                }
            }

            equipped.Remove(equippable);
            equippable.OnUnequipped(this);
            //Debug.Log(this.ToString() + " OnUnequipped == " + equippable.ToString());

            // Need to fix Unity issue that OnTriggerEnter is not called for objects spawned in hand, apparently
            if (!_highlighted.Contains(equippable))
            {
                Highlight(equippable);
            }

            var otherHand = _isLeft ? _player.rightHand : _player.leftHand;
            var closest = GetClosestPosition(equippable, otherHand);
            if (closest.sqrDistance < (equippable.equipDistance * equippable.equipDistance))
            {
                // The other hand is within range of the now unequipped equippable => highlight it
                otherHand.Highlight(equippable);
            }

            return equippable;
        }

        /// <summary>
        /// Gets or creates a "hand" joint.
        /// </summary>
        /// <returns></returns>
        public ConfigurableJoint GetOrCreateJoint()
        {
            ConfigurableJoint joint = null;
            if (_equipped.Count >= _joints.Count)
            {
                if (_handJointPrefab == null)
                {
                    var go = new GameObject("Joint");
                    go.transform.SetParent(_transform);
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localRotation = Quaternion.identity;

                    var rb = go.AddComponent<Rigidbody>();
                    rb.constraints = RigidbodyConstraints.FreezeAll;
                    rb.useGravity = false;

                    joint = go.AddComponent<ConfigurableJoint>();
                    joint.SetDefaultHandJointSettings();
                }
                else
                {
                    var go = GameObject.Instantiate(_handJointPrefab);
                    go.transform.SetParent(_transform);
                    go.transform.localPosition = Vector3.zero;
                    go.transform.localRotation = Quaternion.identity;

                    joint = go.GetOrAddComponent<ConfigurableJoint>();
                }

                _joints.Add(joint);
            }
            else
            {
                joint = _joints[_equipped.Count];
                joint.gameObject.SetActive(true);
            }

            return joint;
        }

        /// <summary>
        /// Gets the joint currently connected to the given <see cref="Rigidbody" />, or null if none.
        /// </summary>
        /// <param name="rb">The rb.</param>
        /// <returns></returns>
        public ConfigurableJoint GetConnectedJoint(Rigidbody rb)
        {
            var count = _joints.Count;
            for (int i = 0; i < count; i++)
            {
                var j = _joints[i];
                if (j.connectedBody == null)
                {
                    continue;
                }

                if (ReferenceEquals(j.connectedBody, rb))
                {
                    return j;
                }
            }

            return null;
        }

        /// <summary>
        /// Gets an equippable of the given prefab type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        public IVREquippable GetEquipped(VRPrefabType type)
        {
            var count = _equipped.Count;
            for (int i = 0; i < count; i++)
            {
                if (_equipped[i].prefabType == type)
                {
                    return _equipped[i];
                }
            }

            return null;
        }

        /// <summary>
        /// Gets the nearest currently highlighted equippable. Performs an evaluation. Cache when possible.
        /// </summary>
        /// <returns></returns>
        public IVREquippable GetNearestHighlighted()
        {
            IVREquippable nearest = null;
            var shortest = float.MaxValue;
            var count = _highlighted.Count;
            for (int i = count - 1; i >= 0; i--)
            {
                var highlighted = _highlighted[i];
                // something can be equipped by another hand, so keep the isEquipped irrelevant
                if (highlighted == null || highlighted.Equals(null)/* || highlighted.isEquipped*/)
                {
                    _highlighted.RemoveAt(i);
                    continue;
                }

                var closest = GetClosestPosition(highlighted, this);
                if (closest.sqrDistance > (highlighted.equipDistance * highlighted.equipDistance))
                {
                    continue;
                }

                if (closest.sqrDistance < shortest)
                {
                    shortest = closest.sqrDistance;
                    nearest = highlighted;
                }
            }

            return nearest;
        }

        /// <summary>
        /// Returns the position on any of the colliders of the equippable, that is closest to the hand.
        /// Also returns the square distance in case anybody wants to use it (usually it's needed right after).
        /// </summary>
        /// <param name="equippable">obj to analyze</param>
        /// <param name="hand">Ref to the hand used in position comparison</param>
        private ClosestPoint GetClosestPosition(IVREquippable equippable, IVRHand hand)
        {
            var handPos = hand.position;
            var nearestSqrDist = float.MaxValue;
            var position = equippable.position;

            var colliders = equippable.colliders;
            var count = colliders.Count;
            for (int i = 0; i < count; i++)
            {
                var closest = equippable.colliders[i].ClosestPointOnSurface(handPos);
                var sqrDistance = (closest - handPos).sqrMagnitude;
                if (sqrDistance < nearestSqrDist)
                {
                    position = closest;
                    nearestSqrDist = sqrDistance;
                }
            }

            if (nearestSqrDist == float.MaxValue)
            {
                nearestSqrDist = (position - handPos).sqrMagnitude;
            }

            return new ClosestPoint()
            {
                position = position,
                sqrDistance = nearestSqrDist
            };
        }
    }
}