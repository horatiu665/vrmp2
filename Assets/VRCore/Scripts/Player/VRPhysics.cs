namespace VRCore
{
    using UnityEngine;

    /// <summary>
    /// Static wrapper for Unity physics methods, ensured for maximum performance.
    /// </summary>
    public static class VRPhysics
    {
        private const int _bufferSize = 100;
        private static readonly Collider[] _buffer = new Collider[_bufferSize];

        /// <summary>
        ///   <para>Computes and stores colliders touching or inside the sphere, and returns an array populated with the hits. Note that there WILL most often be <see cref="null"/> colliders in the array, due to the buffer being fixed-size.</para>
        /// </summary>
        /// <param name="position">Center of the sphere.</param>
        /// <param name="radius">Radius of the sphere.</param>
        /// <returns>
        ///   <para>The results buffer with hit colliders, AND most likely null entries.</para>
        /// </returns>
        public static Collider[] OverlapSphere(Vector3 position, float radius)
        {
            return OverlapSphere(position, radius, -1);
        }

        /// <summary>
        ///   <para>Computes and stores colliders touching or inside the sphere, and returns an array populated with the hits. Note that there WILL most often be <see cref="null"/> colliders in the array, due to the buffer being fixed-size.</para>
        /// </summary>
        /// <param name="position">Center of the sphere.</param>
        /// <param name="radius">Radius of the sphere.</param>
        /// <param name="layerMask">A that is used to selectively ignore colliders when casting a ray.</param>
        /// <returns>
        ///   <para>The results buffer with hit colliders, AND most likely null entries.</para>
        /// </returns>
        public static Collider[] OverlapSphere(Vector3 position, float radius, LayerMask layer)
        {
            _buffer.Clear();
            Physics.OverlapSphereNonAlloc(position, radius, _buffer, layer);
            return _buffer;
        }
    }
}