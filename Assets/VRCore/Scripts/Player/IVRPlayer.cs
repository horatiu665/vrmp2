namespace VRCore
{
    using Apex;
    using UnityEngine;

    /// <summary>
    /// Basic interface representing all (VR) players.
    /// </summary>
    public interface IVRPlayer : IPooledComponent, IHaveVRCustomBehaviours, IHaveVRPreviewBehaviours
    {
        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        Vector3 position { get; set; }

        /// <summary>
        /// Gets or sets the local position.
        /// </summary>
        /// <value>
        /// The local position.
        /// </value>
        Vector3 localPosition { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        Quaternion rotation { get; set; }

        /// <summary>
        /// Gets or sets the local rotation.
        /// </summary>
        /// <value>
        /// The local rotation.
        /// </value>
        Quaternion localRotation { get; set; }

        /// <summary>
        /// Gets the type of the player.
        /// </summary>
        /// <value>
        /// The type of the player.
        /// </value>
        PlayerType playerType
        {
            get;
        }

        /// <summary>
        /// Gets the Rigidbody.
        /// </summary>
        /// <value>
        /// The rigidbody.
        /// </value>
        Rigidbody rb
        {
            get;
        }

        /// <summary>
        /// Gets or sets the velocity.
        /// </summary>
        /// <value>
        /// The velocity.
        /// </value>
        Vector3 velocity
        {
            get;
            set;
        }

        /// <summary>
        /// Gets the head.
        /// </summary>
        /// <value>
        /// The head.
        /// </value>
        Transform head
        {
            get;
        }

        /// <summary>
        /// Gets the left hand.
        /// </summary>
        /// <value>
        /// The left hand.
        /// </value>
        IVRHand leftHand
        {
            get;
        }

        /// <summary>
        /// Gets the right hand.
        /// </summary>
        /// <value>
        /// The right hand.
        /// </value>
        IVRHand rightHand
        {
            get;
        }

        /// <summary>
        /// The spring strength on the grab joint. used by objects when manipulating joint strength such as when impaling arrows (sometimes we want to set strength to 0 for a while)
        /// </summary>
        float handJointSpringStrength { get; }

        /// <summary>
        /// Gets the equippables container, where equipped equippables are parented to, if using <see cref="ParentToPlayerEquipUnequipBehaviour"/>.
        /// </summary>
        /// <value>
        /// The equippables container.
        /// </value>
        PlayerEquippablesContainer equippablesContainer
        {
            get;
        }

        /// <summary>
        /// Gets the velocity controller script, which controls rigidbody forces, velocity and iskinematic.
        /// </summary>
        SkiVelocityController velocityController { get; }
        
        /// <summary>
        /// Gets or sets a value indicating whether to disallow spawning.
        /// </summary>
        /// <value>
        ///   <c>true</c> if disallowing spawning; otherwise, <c>false</c>.
        /// </value>
        bool disallowSpawning { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to disallow equipping.
        /// </summary>
        /// <value>
        ///   <c>true</c> if disallowing equipping; otherwise, <c>false</c>.
        /// </value>
        bool disallowEquipping { get; set; }

        /// <summary>
        /// Sets the head's local position.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        void SetHeadPosition(float x, float y);

        /// <summary>
        /// Sets the local scale uniformly and updates connected anchors.
        /// </summary>
        /// <param name="scale">The scale.</param>
        void SetLocalScale(float scale);

        /// <summary>
        /// Updates all configurable joint connected anchors for this player.
        /// </summary>
        void UpdateConnectedAnchors();

    }
}