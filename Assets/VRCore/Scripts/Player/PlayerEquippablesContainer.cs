namespace VRCore
{
    using Apex;
    using UnityEngine;

    public sealed class PlayerEquippablesContainer : PooledComponentBase, IVRPlayerStartListener
    {
        private IVRPlayer _player;

        public void OnPlayerStart(IVRPlayer player)
        {
            if (player == null)
            {
                Debug.LogWarning(this.ToString() + " OnPlayerStart() passed player is null!");
                return;
            }

            _player = player;
            this.name = string.Concat("Equippables Container (", _player.transform.name, ")");
            this.transform.SetParent(null);
        }

        private void Update()
        {
            Sync();
        }

        private void FixedUpdate()
        {
            Sync();
        }

        private void LateUpdate()
        {
            Sync();
        }

        private void Sync()
        {
            if (_player == null)
            {
                return;
            }

            this.transform.position = _player.position;
        }
    }
}