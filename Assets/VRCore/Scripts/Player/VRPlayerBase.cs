namespace VRCore
{
    using System.Collections.Generic;
    using Apex;
    using Core;
    using UnityEngine;
    using VRNetwork;
    using System.Linq;

    /// <summary>
    /// Base class for all players. To be extended for game-specific purposes. Should not exist in multiple game-specific versions, instead, both AI, remotes (networked) and locals should be able to use the same instance of a derivative of this class.
    /// </summary>
    /// <seealso cref="UnityEngine.MonoBehaviour" />
    /// <seealso cref="VRCore.IVRPlayer" />
    public abstract class VRPlayerBase : PooledComponentBase, IVRPlayer
    {
        protected readonly IList<IVRCustomBehaviour> _behaviours = new List<IVRCustomBehaviour>();
        protected readonly IList<IVRPreviewBehaviour> _previewBehaviours = new List<IVRPreviewBehaviour>();
        protected readonly VRPreviewChangeInput _previewChangeInput = new VRPreviewChangeInput();

        [Header("Player Type")]
        [SerializeField]
        protected PlayerType _playerType = 0;

        [Header("References")]
        [SerializeField]
        protected Transform _head;

        [SerializeField]
        protected Transform _leftHand;

        [SerializeField]
        protected Transform _rightHand;

        [SerializeField]
        protected GameObject _handJointPrefab;

        [SerializeField, ReadOnly]
        private float _handJointSpringStrength = 200000;
        public float handJointSpringStrength
        {
            get
            {
                return _handJointSpringStrength;
            }
        }

        [Header("Capabilities")]
        [SerializeField]
        protected bool _disallowSpawning;

        [SerializeField]
        protected bool _disallowEquipping;

        protected Rigidbody _rb;
        protected List<ConfigurableJoint> _joints;

        protected PlayerEquippablesContainer _equippablesContainer;

        private SkiVelocityController _velocityController;
        public SkiVelocityController velocityController
        {
            get
            {
                if (_velocityController == null)
                {
                    _velocityController = GetComponent<SkiVelocityController>();
                }
                return _velocityController;
            }
        }

        /// <summary>
        /// Gets or sets the local position.
        /// </summary>
        /// <value>
        /// The local position.
        /// </value>
        public virtual Vector3 localPosition
        {
            get { return this.transform.localPosition; }
            set { this.transform.localPosition = value; }
        }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        public virtual Vector3 position
        {
            get { return this.transform.position; }
            set { this.transform.position = value; }
        }

        /// <summary>
        /// Gets or sets the local rotation.
        /// </summary>
        /// <value>
        /// The local rotation.
        /// </value>
        public virtual Quaternion localRotation
        {
            get { return this.transform.localRotation; }
            set { this.transform.localRotation = value; }
        }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        public virtual Quaternion rotation
        {
            get { return this.transform.rotation; }
            set { this.transform.rotation = value; }
        }

        /// <summary>
        /// Gets or sets the <see cref="Rigidbody"/> velocity.
        /// </summary>
        /// <value>
        /// The velocity.
        /// </value>
        public virtual Vector3 velocity
        {
            get
            {
                try
                {
                    return _rb.velocity;
                }
                catch
                {
                    return Vector3.zero;
                }
            }
            set
            {
                try
                {
                    _rb.velocity = value;
                }
                catch
                {
                    // do shit
                }
            }
        }

        /// <summary>
        /// Gets the left hand.
        /// </summary>
        /// <value>
        /// The left hand.
        /// </value>
        public virtual IVRHand leftHand
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the right hand.
        /// </summary>
        /// <value>
        /// The right hand.
        /// </value>
        public virtual IVRHand rightHand
        {
            get;
            protected set;
        }

        /// <summary>
        /// Gets the head <see cref="Transform"/>.
        /// </summary>
        /// <value>
        /// The head.
        /// </value>
        public virtual Transform head
        {
            get { return _head; }
        }

        /// <summary>
        /// Gets the <see cref="Rigidbody"/> associated with this player.
        /// </summary>
        /// <value>
        /// The rigidbody.
        /// </value>
        public virtual Rigidbody rb
        {
            get { return _rb; }
        }

        /// <summary>
        /// Gets the list of custom behaviours currently registered for this instance.
        /// </summary>
        /// <value>
        /// The behaviours.
        /// </value>
        public virtual IList<IVRCustomBehaviour> behaviours
        {
            get { return _behaviours; }
        }

        /// <summary>
        /// Gets the preview behaviours.
        /// </summary>
        /// <value>
        /// The preview behaviours.
        /// </value>
        public virtual IList<IVRPreviewBehaviour> previewBehaviours
        {
            get { return _previewBehaviours; }
        }

        /// <summary>
        /// Gets the equippables container, where equipped equippables are parented to, if using <see cref="ParentToPlayerEquipUnequipBehaviour" />.
        /// </summary>
        /// <value>
        /// The equippables container.
        /// </value>
        public virtual PlayerEquippablesContainer equippablesContainer
        {
            get { return _equippablesContainer; }
        }

        /// <summary>
        /// Gets the type of the player.
        /// </summary>
        /// <value>
        /// The type of the player.
        /// </value>
        public virtual PlayerType playerType
        {
            get { return _playerType; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to disallow spawning.
        /// </summary>
        /// <value>
        ///   <c>true</c> if disallowing spawning; otherwise, <c>false</c>.
        /// </value>
        public bool disallowSpawning
        {
            get { return _disallowSpawning; }
            set { _disallowSpawning = value; }
        }

        /// <summary>
        /// Gets or sets a value indicating whether to disallow equipping.
        /// </summary>
        /// <value>
        ///   <c>true</c> if disallowing equipping; otherwise, <c>false</c>.
        /// </value>
        public bool disallowEquipping
        {
            get { return _disallowEquipping; }
            set { _disallowEquipping = value; }
        }

        protected virtual void OnValidate()
        {
            if (_handJointPrefab != null)
            {
                _handJointSpringStrength = _handJointPrefab.GetComponent<ConfigurableJoint>().slerpDrive.positionSpring;
            }
        }

        /// <summary>
        /// Called by Unity when awaking - right before OnEnable.
        /// </summary>
        protected virtual void Awake()
        {
            if (_head == null)
            {
                Debug.LogError(this.ToString() + " is missing a Head reference");
            }

            if (_leftHand == null)
            {
                Debug.LogError(this.ToString() + " is missing a Left Hand reference");
            }

            if (_rightHand == null)
            {
                Debug.LogError(this.ToString() + " is missing a Right Hand reference");
            }

            _rb = this.GetComponent<Rigidbody>();
            this.leftHand = new VRHand(_leftHand, this, true, _handJointPrefab);
            this.rightHand = new VRHand(_rightHand, this, false, _handJointPrefab);

            _velocityController = this.GetComponent<SkiVelocityController>();
        }

        /// <summary>
        /// Called by Unity when enabled - after Awake, but before Start.
        /// </summary>
        protected virtual void OnEnable()
        {
            _joints = this.GetComponentsInChildren<ConfigurableJoint>().ToList();
        }

        /// <summary>
        /// Called by Unity when starting - after Awake and OnEnable.
        /// </summary>
        protected virtual void Start()
        {
            var listeners = this.GetComponentsInChildren<IVRPlayerStartListener>();
            for (int i = 0; i < listeners.Length; i++)
            {
                listeners[i].OnPlayerStart(this);
            }

            _equippablesContainer = PoolManager.Get<PlayerEquippablesContainer>(this.position, this.rotation);
            _equippablesContainer.OnPlayerStart(this);
        }

        /// <summary>
        /// Called by Unity when disabled - before Destroy (usually).
        /// </summary>
        protected virtual void OnDisable()
        {
            if (!_equippablesContainer || _equippablesContainer == null || _equippablesContainer.Equals(null))
            {
                return;
            }

            _equippablesContainer.Recycle();
            _equippablesContainer = null;
        }

        /// <summary>
        /// Sets the head's local position.
        /// </summary>
        /// <param name="x">The x.</param>
        /// <param name="y">The y.</param>
        public virtual void SetHeadPosition(float x, float y)
        {
            _head.transform.localPosition = new Vector3(x, y, _head.transform.localPosition.z);
        }

        /// <summary>
        /// Updates all configurable joint connected anchors for this player.
        /// </summary>
        public virtual void UpdateConnectedAnchors()
        {
            // Update connected anchors for this game object and its children.
            _joints.UpdateConnectedAnchors();
        }

        /// <summary>
        /// Called when a new preview tool selection has been made.
        /// </summary>
        /// <param name="hand">The hand.</param>
        /// <param name="oldType">The old type.</param>
        /// <param name="newType">The new type.</param>
        public virtual void OnChangePreview(bool isLeft, VRPrefabType oldType, VRPrefabType newType)
        {
            if (oldType == newType)
            {
                Debug.LogWarning(this.ToString() + " OnChangePreview() old type matches new type, ignoring!");
                return;
            }

            _previewChangeInput.player = this;
            _previewChangeInput.hand = isLeft ? this.leftHand : this.rightHand;
            _previewChangeInput.oldType = oldType;
            _previewChangeInput.newType = newType;

            // Process exit events
            var count = _previewBehaviours.Count;
            for (int i = 0; i < count; i++)
            {
                var b = _previewBehaviours[i];
                if (!b.enabled)
                {
                    // ignore disabled behaviours
                    continue;
                }

                if (b.prefabType == oldType)
                {
                    // all behaviours matching old type have 'preview exit' invoked
                    b.OnPreviewExit(_previewChangeInput);
                }
            }

            // Process enter events
            for (int i = 0; i < count; i++)
            {
                var b = _previewBehaviours[i];
                if (!b.enabled)
                {
                    // ignore disabled behaviours
                    continue;
                }

                if (b.prefabType == newType)
                {
                    // all behaviours matching new type have 'preview enter' invoked
                    b.OnPreviewEnter(_previewChangeInput);
                }
            }

            // TODO: Ugly implementation, refactor!!!
            var netPlayer = this as INetPlayer;
            if (netPlayer == null || netPlayer.isLocal)
            {
                // Only run this for the single or local player
                SpawnManagerPreviewable.instance.Remap(isLeft, oldType, newType);

                if (NetServices.isClient)
                {
                    // net player could be used in a single player game, so we have to check whether we are actually a client to ensure that ClientNetSender is available
                    var msg = MessagePool.Get<ChangePreviewMessage>();
                    msg.isLeft = isLeft;
                    msg.oldPrefabType = oldType;
                    msg.prefabType = newType;

                    VRNetwork.Client.ClientNetSender.instance.Send(msg, UnityEngine.Networking.QosType.ReliableSequenced);
                    MessagePool.Return(msg);
                }
            }
        }

        /// <summary>
        /// Sets the local scale uniformly and updates connected anchors.
        /// </summary>
        /// <param name="scale">The scale.</param>
        public void SetLocalScale(float scale)
        {
            this.transform.localScale = Vector3.one * scale;
            UpdateConnectedAnchors();
        }
    }
}