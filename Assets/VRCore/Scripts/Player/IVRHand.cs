namespace VRCore
{
    using System.Collections.Generic;
    using UnityEngine;

    public interface IVRHand
    {
        /// <summary>
        /// Gets a value indicating whether this hand is left.
        /// </summary>
        /// <value>
        ///   <c>true</c> if this hand is left; otherwise, <c>false</c>.
        /// </value>
        bool isLeft { get; }

        /// <summary>
        /// Gets the currently highlighted equippables - the ones that are within this hand's colliders.
        /// </summary>
        /// <value>
        /// The highlighted.
        /// </value>
        IList<IVREquippable> highlighted { get; }

        /// <summary>
        /// Gets the currently equipped <see cref="IVREquippable"/>s for this hand.
        /// </summary>
        /// <value>
        /// The equipped objects.
        /// </value>
        IList<IVREquippable> equipped { get; }

        /// <summary>
        /// Gets the list of currently created joints, both active and inactive ones.
        /// </summary>
        /// <value>
        /// The joints.
        /// </value>
        IList<ConfigurableJoint> joints { get; }

        /// <summary>
        /// Gets the transform.
        /// </summary>
        /// <value>
        /// The transform.
        /// </value>
        Transform transform { get; }

        /// <summary>
        /// Gets the game object.
        /// </summary>
        /// <value>
        /// The game object.
        /// </value>
        GameObject gameObject { get; }

        /// <summary>
        /// Gets or sets the position.
        /// </summary>
        /// <value>
        /// The position.
        /// </value>
        Vector3 position { get; set; }

        /// <summary>
        /// Gets or sets the local position.
        /// </summary>
        /// <value>
        /// The local position.
        /// </value>
        Vector3 localPosition { get; set; }

        /// <summary>
        /// Gets or sets the rotation.
        /// </summary>
        /// <value>
        /// The rotation.
        /// </value>
        Quaternion rotation { get; set; }

        /// <summary>
        /// Gets or sets the local rotation.
        /// </summary>
        /// <value>
        /// The local rotation.
        /// </value>
        Quaternion localRotation { get; set; }

        /// <summary>
        /// Gets the player whom this hand belongs to.
        /// </summary>
        /// <value>
        /// The player.
        /// </value>
        IVRPlayer player { get; }

        /// <summary>
        /// Gets the <see cref="IVRController"/>, if it is available. This will return null for remote players and on the server - basically only usable for local players.
        /// </summary>
        /// <value>
        /// The controller.
        /// </value>
        IVRController controller { get; }

        /// <summary>
        /// Sets the <see cref="IVRController"/> reference for this hand.
        /// </summary>
        /// <param name="controller">The controller.</param>
        void SetVRController(IVRController controller);

        /// <summary>
        /// Gets or creates a "hand" joint.
        /// </summary>
        /// <returns></returns>
        ConfigurableJoint GetOrCreateJoint();

        /// <summary>
        /// Gets the joint currently connected to the given <see cref="Rigidbody"/>, or null if none.
        /// </summary>
        /// <param name="rb">The rb.</param>
        /// <returns></returns>
        ConfigurableJoint GetConnectedJoint(Rigidbody rb);

        /// <summary>
        /// Gets an equippable of the given prefab type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns></returns>
        IVREquippable GetEquipped(VRPrefabType type);

        /// <summary>
        /// Gets the nearest currently highlighted equippable. Performs an evaluation. Cache when possible.
        /// </summary>
        /// <returns></returns>
        IVREquippable GetNearestHighlighted();

        /// <summary>
        /// Highlights the specified <see cref="IVREquippable"/>.
        /// </summary>
        /// <param name="equippable">The equippable.</param>
        bool Highlight(IVREquippable equippable);

        /// <summary>
        /// Unhighlights the specified <see cref="IVREquippable"/>.
        /// </summary>
        /// <param name="equippable">The equippable.</param>
        /// <returns></returns>
        bool Unhighlight(IVREquippable equippable);

        /// <summary>
        /// Equips the given <see cref="IVREquippable"/>, if possible. Returns <c>true</c> if successful, <c>false</c> otherwise.
        /// </summary>
        /// <param name="equippable">The object to equip.</param>
        /// <returns>Returns <c>true</c> if successful, <c>false</c> otherwise.</returns>
        IVREquippable Equip(IVREquippable equippable);

        /// <summary>
        /// Unequips the specified equippable, if possible.
        /// Returns the unequipped <see cref="IVREquippable"/>, or null in case of error.
        /// </summary>
        /// <param name="equipped">The equipped object to unequip.</param>
        /// <returns>The dropped <see cref="IVREquippable"/>, or null in case of error.</returns>
        IVREquippable Unequip(IVREquippable equippable, Vector3 velocity, Vector3 angularVelocity);
    }
}