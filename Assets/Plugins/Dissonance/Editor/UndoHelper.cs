﻿using System;
using UnityEditor;

namespace Dissonance.Editor
{
    internal static class UndoHelper
    {
        public static void ChangeWithUndo<T, TO>(this TO obj, string message, T newValue, T value, Action<T> set)
            where TO : UnityEngine.Object
        {
            if (!Equals(value, newValue))
            {
                Undo.RecordObject(obj, message);
                set(newValue);
            }
        }
    }
}
