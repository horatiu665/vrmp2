﻿using System;
using System.Collections.Generic;
using System.Threading;
using Dissonance.Datastructures;
using Dissonance.Threading;
using Dissonance.VAD;
using NAudio.Wave;

namespace Dissonance.Audio.Capture
{
    internal class PreprocessingPipeline
        : IDisposable
    {
        #region fields and properties
        private static readonly Log Log = Logs.Create(LogCategory.Recording, typeof(PreprocessingPipeline).Name);

        private readonly ConcurrentPool<float[]> _inputBufferSource;
        private readonly TransferBuffer<float[]> _inputQueue;

        private static float[] _emptyInputFrame;
        private int _droppedFrames;

        private bool _resetApplied;
        private int _resetRequested = 1;

        private bool _subActive;
        private readonly List<IMicrophoneHandler> _micSubscriptions = new List<IMicrophoneHandler>();

        private bool _vadActive;
        private readonly List<IVoiceActivationListener> _vadSubscriptions = new List<IVoiceActivationListener>();

        private bool _vadDirty;
        private readonly IVoiceDetector _vad;
        public IVoiceDetector VAD { get { return _vad; } }

        private readonly Preprocessor _speex;

        private readonly short[] _int16Frame;
        private readonly float[] _float32Frame;

        private readonly BufferedSampleProvider _resamplerInput;
        private readonly Resampler _resampler;
        private readonly SampleToFrameProvider _resampledOutput;

        private volatile bool _runThread;
        private readonly DThread _thread;
        private readonly AutoResetEvent _threadEvent;

        public WaveFormat OutputFormat
        {
            get { return _resampler.WaveFormat; }
        }

        public bool RequiresInput
        {
            get { return _vadActive || _subActive; }
        }

        private ArvCalculator _arvCalculator = new ArvCalculator();
        public float ARV { get { return _arvCalculator.ARV; } }
        #endregion

        #region constructor
        public PreprocessingPipeline(uint inputFrameSize, WaveFormat inputFormat)
        {
            //Create two buffers for transferring buffers between the two thread
            // - _bufferSource is a pool of empty buffers, ready for the main thread to use
            // - _inputQueue is a queue of samples, waiting to be processed off the main thread
            _inputBufferSource = new ConcurrentPool<float[]>(24, () => new float[inputFrameSize]);
            _inputQueue = new TransferBuffer<float[]>(12);

            _emptyInputFrame = new float[inputFrameSize];

            const uint outputFrameSize = (uint)(48000 * 0.02);

            _resamplerInput = new BufferedSampleProvider(inputFormat, (int)inputFrameSize * 4);
            _resampler = new Resampler(_resamplerInput, 48000);
            _resampledOutput = new SampleToFrameProvider(_resampler, outputFrameSize);

            _speex = new Preprocessor((int)outputFrameSize, 48000);
            _vad = new WebRtcVoiceDetector((int)outputFrameSize, 48000);
            _int16Frame = new short[outputFrameSize];
            _float32Frame = new float[outputFrameSize];

            _threadEvent = new AutoResetEvent(false);
            _thread = new DThread(ThreadEntry);
        }
        #endregion

        #region disposal
        private bool _disposed;
        public void Dispose()
        {
            if (_disposed)
                return;
            _disposed = true;

            _runThread = false;
            _threadEvent.Set();
            _thread.Join();
            
            _speex.Dispose();

            Log.Info("Stopped microphone capture");
        }
        #endregion

        public void Start()
        {
            _runThread = true;
            _thread.Start();
        }

        public void Reset()
        {
            Interlocked.Exchange(ref _resetRequested, 1);
        }

        #region pooling
        /// <summary>
        /// Get an empty buffer which can be filled with audi data and passed into `Send`
        /// </summary>
        /// <returns></returns>
        public float[] GetFrameBuffer()
        {
            return _inputBufferSource.Get();
        }

        /// <summary>
        /// Deliver a new frame of audio (with the input format/size configured in the constructor)
        /// </summary>
        /// <param name="frame"></param>
        /// <remarks>Frame passed in here should have come from a call to `GetFrameBuffer`</remarks>
        public void Send(float[] frame)
        {
            if (!_inputQueue.Write(frame))
            {
                Log.Warn("Failed to write microphone samples into input queue");

                //Increment a counter to inform the pipeline that a frame was lost
                Interlocked.Increment(ref _droppedFrames);

                //We failed to process this frame, recycle the buffer
                _inputBufferSource.Put(frame);
            }
            _threadEvent.Set();
        }
        #endregion

        private void ThreadEntry()
        {
            try
            {
                while (_runThread)
                {
                    //Wait for an event(s) to arrive which we need to process.
                    //Max wait time is half the size of the smallest frame size so it should wake up with no work to do
                    // a lot of the time (ensuring minimal processing latency when there is work to do).
                    if (_inputQueue.EstimatedUnreadCount == 0)
                        _threadEvent.WaitOne(10);

                    //Apply a pipeline reset if one has been requested, but one has not yet been applied
                    if (Interlocked.Exchange(ref _resetRequested, 0) == 1 && !_resetApplied)
                        ApplyReset();

                    //If there are no items in the buffer skip back to sleep
                    var countInBuffer = _inputQueue.EstimatedUnreadCount;
                    if (countInBuffer == 0)
                        continue;

                    //We're about to process some audio, meaning the pipeline is no longer in a reset state
                    _resetApplied = false;

                    //Reset the external dropped frames counter and store it locally, we're about to process the packet before the drop
                    var missed = Interlocked.Exchange(ref _droppedFrames, 0);

                    for (var i = 0; i < countInBuffer + missed; i++)
                    {
                        var recycle = true;
                        float[] buffer;
                        if (!_inputQueue.Read(out buffer))
                        {
                            //We can't get a buffer from the input queue, submit silent frames to make up for lost data
                            if (missed > 0)
                            {
                                recycle = false;
                                buffer = _emptyInputFrame;
                                missed--;
                            }
                        }

                        //Push frame through the resampler and process the resampled audio one frame at a time
                        var offset = 0;
                        while (offset != buffer.Length)
                        {
                            offset += _resamplerInput.Write(buffer, offset, buffer.Length - offset);

                            //Read resampled data and push it through the pipeline
                            while (_resampledOutput.Read(new ArraySegment<float>(_float32Frame, 0, _float32Frame.Length)))
                                ProcessAudioFrame(_float32Frame);
                        }

                        //Recycle this buffer if necessary
                        if (recycle)
                            _inputBufferSource.Put(buffer);
                    }

                    //Now that we've pulled the right amount of items out of the queue send empty frames to compensate for dropped audio
                    //If we didn't do this there would be a permanent desync between sender and receiver
                    if (missed > 0)
                    {
                        Log.Debug("Sending {0} silent frames to compensate for lost audio", missed);

                        Array.Clear(_float32Frame, 0, _float32Frame.Length);
                        for (var i = 0; i < missed; i++)
                            SendSamplesToSubscribers(_float32Frame);
                    }

                }
            }
            catch (Exception e)
            {
                Log.Error(Log.PossibleBugMessage("Unhandled exception killed the microphone capture thread: " + e, "E1A24AFE-0456-4922-A2D6-3BE22D17DA0C"));
            }
        }

        private void ApplyReset()
        {
            Log.Debug("Resetting preprocessing pipeline");

            _vad.Reset();
            _speex.Reset();

            _resamplerInput.Reset();
            _resampler.Reset();
            _resampledOutput.Reset();

            _arvCalculator.Reset();
            _droppedFrames = 0;

            _resetApplied = true;
        }

        private void ProcessAudioFrame(float[] buffer)
        {
            //Convert the audio to int16 format for VAD and preprocessing
            FormatConverter.ConvertFloat32ToInt16(new ArraySegment<float>(buffer), new ArraySegment<short>(_int16Frame));

            //Process the buffer with VAD, sets the IsSpeaking flag on the VAD
            ProcessVAD(_int16Frame);

            //Preprocess the audio (AGC, noise removal)
            _speex.Process(new ArraySegment<short>(_int16Frame));

            //Convert back to float format
            FormatConverter.ConvertInt16ToFloat32(new ArraySegment<short>(_int16Frame), new ArraySegment<float>(buffer));

            //Update the ARV calculation
            _arvCalculator.Update(new ArraySegment<float>(buffer));

            //Send the processed audio to subscribers
            SendSamplesToSubscribers(buffer);
        }

        /// <summary>
        /// Preprocess a buffer of audio. Updates the VAD (if necessary).
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns>true, if the audio should be delayed by a frame. Otherwise false</returns>
        private void ProcessVAD(short[] buffer)
        {
            //Run voice detector on samples (only if there are any listeners)
            lock (_vadSubscriptions)
            {
                if (_vadDirty && _vadSubscriptions.Count > 0)
                {
                    _vadDirty = false;
                    _vad.Reset();

                    Log.Trace("Reset VAD");
                }

                //If we are running VAD we want to delay 
                if (_vadSubscriptions.Count > 0)
                {
                    var talk = _vad.IsSpeaking;
                    _vad.Handle(new ArraySegment<short>(buffer));
                    if (talk != _vad.IsSpeaking)
                    {
                        for (var i = 0; i < _vadSubscriptions.Count; i++)
                        {
                            if (_vad.IsSpeaking)
                                _vadSubscriptions[i].VoiceActivationStart();
                            else
                                _vadSubscriptions[i].VoiceActivationStop();
                        }
                    }
                }
            }
        }

        private void SendSamplesToSubscribers(float[] buffer)
        {
            lock (_micSubscriptions)
            {
                for (var i = 0; i < _micSubscriptions.Count; i++)
                {
                    try
                    {
                        _micSubscriptions[i].Handle(new ArraySegment<float>(buffer), _resampler.WaveFormat);
                    }
                    catch (Exception ex)
                    {
                        Log.Error("Microphone subscriber '{0}' threw: {1}", _micSubscriptions[i].GetType().Name, ex);
                    }
                }
            }
        }

        #region subscriptions
        public void Subscribe(IMicrophoneHandler listener)
        {
            lock (_micSubscriptions)
            {
                _micSubscriptions.Add(listener);
                _subActive = true;
            }
        }

        public bool Unsubscribe(IMicrophoneHandler listener)
        {
            lock (_micSubscriptions)
            {
                var removed = _micSubscriptions.Remove(listener);
                _subActive = _micSubscriptions.Count > 0;
                return removed;
            }
        }

        public void Subscribe(IVoiceActivationListener listener)
        {
            lock (_vadSubscriptions)
            {
                _vadSubscriptions.Add(listener);
                _vadActive = true;
            }
            _vadDirty = true;
        }

        public bool Unsubscribe(IVoiceActivationListener listener)
        {
            _vadDirty = true;
            lock (_vadSubscriptions)
            {
                var removed = _vadSubscriptions.Remove(listener);
                _vadActive = _vadSubscriptions.Count > 0;
                return removed;
            }
        }
        #endregion
    }
}
