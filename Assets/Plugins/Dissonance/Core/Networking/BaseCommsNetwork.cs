﻿using System;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Dissonance.Networking
{
    internal struct TextPacket
    {
        public readonly ushort Sender;
        public readonly ChannelType RecipientType;
        public readonly ushort Recipient;
        public readonly string Text;

        public TextPacket(ushort sender, ChannelType recipientType, ushort recipient, string text)
        {
            Sender = sender;
            RecipientType = recipientType;
            Recipient = recipient;
            Text = text;
        }
    }

    [RequireComponent(typeof(DissonanceComms))]
    public abstract class BaseCommsNetwork<TServer, TClient, TPeer>
        : MonoBehaviour, ICommsNetwork
        where TPeer : IEquatable<TPeer>
        where TServer : BaseServer<TServer, TClient, TPeer>
        where TClient : BaseClient<TServer, TClient, TPeer>
    {
        #region fields and properties
        protected readonly Log Log;

        protected TServer Server { get; private set; }
        protected TClient Client { get; private set; }

        public string PlayerName { get; private set; }

        public bool ServerEnabled { get; private set; }
        public bool ClientEnabled { get; private set; }

        private DateTime _lastTriedClientConnect;

        public DissonanceComms Comms { get; private set; }

        public Rooms Rooms { get; private set; }
        public PlayerChannels PlayerChannels { get; private set; }
        public RoomChannels RoomChannels { get; private set; }
        #endregion

        protected BaseCommsNetwork()
        {
            Log = Logs.Create(LogCategory.Network, GetType().Name);
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global (Justification: Public API)
        public virtual void Awake()
        {
            Comms = GetComponent<DissonanceComms>();
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global (Justification: Public API)
        protected virtual void InitializeAsDedicatedServer()
        {
            Disconnect();

            ServerEnabled = true;
            ClientEnabled = false;
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global (Justification: Public API)
        protected virtual void InitializeAsServer()
        {
            Disconnect();

            ServerEnabled = true;
            ClientEnabled = true;
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global (Justification: Public API)
        protected virtual void InitializeAsClient()
        {
            Disconnect();

            ServerEnabled = false;
            ClientEnabled = true;
        }

        private Action<bool> _connectedCallback;

        private void Disconnect()
        {
            StopClient();
            StopServer();
        }

        // ReSharper disable once UnusedMemberHierarchy.Global (Justification: Used implicitly by unity)
        protected virtual void Update()
        {
            // start the server and client up if we have all required info and they are not started
            if (PlayerName != null)
            {
                if (Server == null && ServerEnabled)
                    StartServer();

                if (Client == null && ClientEnabled)
                    StartClient();
            }

            //Call the "connected" callback if we've successfuly created a client
            if (_connectedCallback != null && Client != null)
            {
                if (Client.IsConnected)
                {
                    _connectedCallback(true);
                    _connectedCallback = null;
                }
            }

            // update server
            if (Server != null)
                Server.Update();

            // update client
            if (Client != null)
                Client.Update();
        }

        protected abstract TServer CreateServer();

        private void StartServer()
        {
            Server = CreateServer();
            Server.Connect();
        }

        protected abstract TClient CreateClient();

        private void StartClient()
        {
            if (DateTime.Now - _lastTriedClientConnect <= TimeSpan.FromSeconds(1))
                return;

            _lastTriedClientConnect = DateTime.Now;

            Client = CreateClient();
            Client.Connect();

            if (Client != null)
            {
                Log.Trace("Subscribing to network events");

                Client.PlayerJoined += OnPlayerJoined;
                Client.PlayerLeft += OnPlayerLeft;
                Client.VoicePacketReceived += OnVoicePacketReceived;
                Client.TextMessageReceived += OnTextPacketReceived;
                Client.PlayerStartedSpeaking += OnPlayerStartedSpeaking;
                Client.PlayerStoppedSpeaking += OnPlayerStoppedSpeaking;

                Client.Disconnected += Client_Disconnected;
            }
        }

        private void Client_Disconnected()
        {
            Client.PlayerJoined -= OnPlayerJoined;
            Client.PlayerLeft -= OnPlayerLeft;
            Client.VoicePacketReceived -= VoicePacketReceived;
            Client.TextMessageReceived -= TextPacketReceived;
            Client.PlayerStartedSpeaking -= OnPlayerStartedSpeaking;
            Client.PlayerStoppedSpeaking -= OnPlayerStoppedSpeaking;
            Client.Disconnected -= Client_Disconnected;

            Client = null;
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global (Justification: Public API)
        public virtual void StopClient()
        {
            ClientEnabled = false;

            if (Client != null)
            {
                Client.Disconnect();
                Client = null;
            }
        }

        // ReSharper disable once VirtualMemberNeverOverridden.Global (Justification: Public API)
        public virtual void StopServer()
        {
            ServerEnabled = false;

            if (Server != null)
            {
                StopClient();

                Server.Disconnect();
                Server = null;
            }
        }

        protected abstract void Initialize();

        protected virtual void Shutdown()
        {
            Disconnect();
        }

        // ReSharper disable once UnusedMember.Local (Justification: Used implicitly by unity)
        private void OnDisable()
        {
            Shutdown();
        }

        #region IVoiceNetwork
        public void Initialize(string playerName, Rooms rooms, PlayerChannels playerChannels, RoomChannels roomChannels, Action<bool> connectionCallback)
        {
            if (playerName == null)
                throw new ArgumentNullException("playerName");
            if (rooms == null)
                throw new ArgumentNullException("rooms");
            if (playerChannels == null)
                throw new ArgumentNullException("playerChannels");
            if (roomChannels == null)
                throw new ArgumentNullException("roomChannels");
            if (connectionCallback == null)
                throw new ArgumentNullException("connectionCallback");

            // disconnect if any details have changed
            if (PlayerName != playerName || Rooms != rooms || PlayerChannels != playerChannels || RoomChannels != roomChannels)
            {
                PlayerName = playerName;
                Rooms = rooms;
                PlayerChannels = playerChannels;
                RoomChannels = roomChannels;

                Disconnect();
            }

            Initialize();

            _connectedCallback = connectionCallback;
        }

        public event Action<string> PlayerJoined;
        public event Action<string> PlayerLeft;
        public event Action<VoicePacket> VoicePacketReceived;
        public event Action<TextMessage> TextPacketReceived;
        public event Action<string> PlayerStartedSpeaking;
        public event Action<string> PlayerStoppedSpeaking;

        public void SendVoice(ArraySegment<byte> data)
        {
            if (Client != null)
                Client.SendVoiceData(data);
        }

        public void SendText(string data, ChannelType recipientType, string recipientId)
        {
            if (Client != null)
                Client.SendTextData(data, recipientType, recipientId);
        }
        #endregion

        #region event invokers
        private void OnPlayerJoined(string obj)
        {
            var handler = PlayerJoined;
            if (handler != null) handler(obj);
        }

        private void OnPlayerLeft(string obj)
        {
            var handler = PlayerLeft;
            if (handler != null) handler(obj);
        }

        private void OnVoicePacketReceived(VoicePacket obj)
        {
            var handler = VoicePacketReceived;
            if (handler != null) handler(obj);
        }

        private void OnTextPacketReceived(TextMessage obj)
        {
            var handler = TextPacketReceived;
            if (handler != null) handler(obj);
        }

        private void OnPlayerStartedSpeaking(string obj)
        {
            var handler = PlayerStartedSpeaking;
            if (handler != null) handler(obj);
        }

        private void OnPlayerStoppedSpeaking(string obj)
        {
            var handler = PlayerStoppedSpeaking;
            if (handler != null) handler(obj);
        }
        #endregion

        public void OnInspectorGUI()
        {
#if UNITY_EDITOR
            var mode = ServerEnabled && ClientEnabled ? "Server & Client" : ServerEnabled ? "Server" : ClientEnabled ? "Client" : "None";
            EditorGUILayout.LabelField("Mode", mode);

            if (!ServerEnabled && !ClientEnabled)
                return;

            if (ClientEnabled && Client != null)
                EditorGUILayout.LabelField("Client Status", Client.IsConnected ? "Connected" : "Disconnected");

            EditorGUILayout.LabelField("Received");
            EditorGUI.indentLevel++;
            try
            {
                if (Client != null)
                {
                    EditorGUILayout.LabelField("Client");
                    EditorGUI.indentLevel++;
                    try
                    {
                        EditorGUILayout.LabelField("Handshake", Client.RecvHandshakeResponse.ToString());
                        EditorGUILayout.LabelField("Routing", Client.RecvRoutingUpdate.ToString());
                        EditorGUILayout.LabelField("Text", Client.RecvTextData.ToString());
                        EditorGUILayout.LabelField("Voice", Client.RecvVoiceData.ToString());

                        int totalPackets, totalBytes, totalBytesPerSecond;
                        TrafficCounter.Combine(out totalPackets, out totalBytes, out totalBytesPerSecond, Client.RecvHandshakeResponse, Client.RecvTextData, Client.RecvRoutingUpdate, Client.RecvVoiceData);
                        EditorGUILayout.LabelField("TOTAL", TrafficCounter.Format(totalPackets, totalBytes, totalBytesPerSecond));
                    }
                    finally
                    {
                        EditorGUI.indentLevel--;
                    }
                }

                if (Server != null)
                {
                    EditorGUILayout.LabelField("Server");
                    EditorGUI.indentLevel++;
                    try
                    {
                        EditorGUILayout.LabelField("Handshake", Server.RecvHandshakeRequest.ToString());
                        EditorGUILayout.LabelField("CState", Server.RecvClientState.ToString());
                        EditorGUILayout.LabelField("Text", Server.RecvTextData.ToString());
                        EditorGUILayout.LabelField("Voice", Server.RecvVoiceData.ToString());

                        int totalPackets, totalBytes, totalBytesPerSecond;
                        TrafficCounter.Combine(out totalPackets, out totalBytes, out totalBytesPerSecond, Server.RecvHandshakeRequest, Server.RecvTextData, Server.RecvClientState, Server.RecvVoiceData);
                        EditorGUILayout.LabelField("TOTAL", TrafficCounter.Format(totalPackets, totalBytes, totalBytesPerSecond));
                    }
                    finally
                    {
                        EditorGUI.indentLevel--;
                    }
                }
            }
            finally
            {
                EditorGUI.indentLevel--;
            }

            EditorGUILayout.LabelField("Sent");
            EditorGUI.indentLevel++;
            try
            {
                if (Server != null)
                    EditorGUILayout.LabelField("Server", Server.SentTraffic.ToString());
                if (Client != null)
                    EditorGUILayout.LabelField("Client", Client.SentTraffic.ToString());
            }
            finally
            {
                EditorGUI.indentLevel--;
            }
#endif
        }
    }
}
