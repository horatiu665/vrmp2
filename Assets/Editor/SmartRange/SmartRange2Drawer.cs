namespace Helpers.Editor
{
    using UnityEditor;
    using UnityEngine;

    [CustomPropertyDrawer(typeof(SmartRange2))]
    public sealed class SmartRange2Drawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            label = EditorGUI.BeginProperty(position, label, property);

            // Calculate rects
            var enumWidth = 26;
            var enumRect = new Rect(position.xMax - enumWidth, position.y, enumWidth, position.height);

            // Don't make child fields be indented
            var indent = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            // depending on Mode, show a different propertyfield, and the enum switch dropdown button
            var modeProp = property.FindPropertyRelative("_mode");
            var mode = (SmartRange2.Mode)modeProp.intValue;

            switch (mode)
            {
            case SmartRange2.Mode.Constant:
                {
                    position.width -= enumWidth;
                    EditorGUI.PropertyField(position, property.FindPropertyRelative("_constValue"), label);
                    break;
                }

            case SmartRange2.Mode.RandomRange:
                {
                    // Draw label and save position rect for other values
                    position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Keyboard), label);
                    // copied this part from the animationcurve inspector below........
                    var rangeRect = position;
                    rangeRect.x -= 1;
                    var animCurveWidth = position.width * 0.33f;
                    rangeRect.width -= animCurveWidth + 2;

                    EditorGUI.PropertyField(rangeRect, property.FindPropertyRelative("_range"), GUIContent.none);

                    break;
                }

            case SmartRange2.Mode.DistributionCurve:
                {
                    // Draw label and save position rect for other values
                    position = EditorGUI.PrefixLabel(position, GUIUtility.GetControlID(FocusType.Keyboard), label);

                    var rangeRect = position;
                    rangeRect.x -= 1;
                    var animCurveWidth = position.width * 0.33f;
                    rangeRect.width -= animCurveWidth + 2;

                    var animCurveRect = position;
                    animCurveRect.x = position.xMax - animCurveWidth;
                    animCurveRect.width = animCurveWidth - enumWidth;

                    EditorGUI.PropertyField(rangeRect, property.FindPropertyRelative("_range"), GUIContent.none);
                    EditorGUI.PropertyField(animCurveRect, property.FindPropertyRelative("_distributionCurve"), GUIContent.none);

                    break;
                }

            default:
                {
                    break;
                }
            }

            // always show the enum dropdown button
            EditorGUI.PropertyField(enumRect, modeProp, GUIContent.none);

            // Set indent back to what it was
            EditorGUI.indentLevel = indent;

            EditorGUI.EndProperty();
        }
    }
}