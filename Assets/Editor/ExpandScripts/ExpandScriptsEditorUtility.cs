﻿namespace ExpandScriptsEditor
{
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using UnityEngine;
    using Random = UnityEngine.Random;
#if UNITY_EDITOR
    using UnityEditor;
#endif

    public static class ExpandScriptsEditorUtility
    {
#if UNITY_EDITOR
        [MenuItem("Tools/Toggle Components Expanded &m")]
        public static void ToggleComponentsExpanded()
        {
            // default, if any are expanded, consider all to be expanded, so we contract all. if all are contracted, expand all.
            var expanded = GetComponents(Selection.activeGameObject).Any(c => UnityEditorInternal.InternalEditorUtility.GetIsInspectorExpanded(c));
            SetComponentsExpanded(Selection.activeGameObject, !expanded);
        }

        public static void SetComponentsExpanded(GameObject target, bool expanded)
        {
            foreach (var comp in GetComponents(target))
            {
                UnityEditorInternal.InternalEditorUtility.SetIsInspectorExpanded(comp, expanded);
            }

            var oldSel = target;
            Selection.activeGameObject = null;
            EditorCoroutine.Start(pTween.WaitFrames(1, () =>
            {
                Selection.activeGameObject = oldSel;
            }));
        }

        private static IEnumerable<Component> GetComponents(GameObject target)
        {
            return target.GetComponents<Component>().Where(c =>
                c.GetType() != typeof(Transform)
            );
        }
        
#endif

    }
}