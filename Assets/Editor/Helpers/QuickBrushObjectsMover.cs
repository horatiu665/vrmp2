using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class QuickBrushObjectsMover : MonoBehaviour
{
    public bool doIt = false;

    public Transform target;
    public Transform[] quickBrushGroups;

    void Reset()
    {
        target = transform;

    }

    void Update()
    {
        if (doIt) {
            doIt = false;
            MoveIt();
        }
    }

    public void MoveIt()
    {
        for (int i = 0; i < quickBrushGroups.Length; i++) {
            //var curName = quickBrushGroups[i].name;
            //var prefix = "QB_Group_";
            //var newName = curName.Substring(prefix.Length, curName.Length - prefix.Length);
            var newName = quickBrushGroups[i].GetComponent<qb_Group>().groupName;
            var newGroup = new GameObject(newName).transform;
            newGroup.SetParent(target);
            newGroup.localPosition = Vector3.zero;
            newGroup.localRotation = Quaternion.identity;
            newGroup.localScale = Vector3.one;

            var n = quickBrushGroups[i].childCount;
            for (int j = 0; j < n; j++) {
                quickBrushGroups[i].GetChild(0).SetParent(newGroup);
            }

        }

    }

}
