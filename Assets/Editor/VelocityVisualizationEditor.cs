﻿namespace Apex.Editor
{
    using Analytics;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using UnityEditor;
    using UnityEngine;

    [CustomEditor(typeof(VelocityVisualizer))]
    public class VelocityVisualizationEditor : Editor
    {
        private VelocityVisualizer _v;
        private Vector3 _maxRightHandVelocity = Vector3.zero;

        public override void OnInspectorGUI()
        {
            if(!EditorApplication.isPlaying) 
            {
                EditorGUILayout.HelpBox("This only shows data at run time.", MessageType.Info);
                return;
            }

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Right Hand");
            EditorGUILayout.LabelField("Velocity", _v.rightHandVelocity.ToString());
            EditorGUILayout.LabelField("Angular Velocity", _v.rightHandAngularRotation.ToString());
            EditorGUILayout.LabelField("Rotation", _v.rightHandRotation.ToString());

            if(_v.rightHandVelocity.sqrMagnitude > _maxRightHandVelocity.sqrMagnitude)
            {
                _maxRightHandVelocity = _v.rightHandVelocity;
            }

            EditorGUILayout.LabelField("Max Velocity", _maxRightHandVelocity.ToString());

            EditorGUILayout.Separator();
            EditorGUILayout.LabelField("Left Hand");
            EditorGUILayout.LabelField("Velocity", _v.leftHandVelocity.ToString());
            EditorGUILayout.LabelField("Angular Velocity", _v.leftHandAngularRotation.ToString());
            EditorGUILayout.LabelField("Rotation", _v.leftHandRotation.ToString());
        }

        private void OnEnable()
        {
            _v = this.target as VelocityVisualizer;
        }
    }
}
